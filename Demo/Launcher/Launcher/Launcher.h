//
//  Launcher.h
//  Launcher
//
//  Created by iphtech-28 on 6/5/18.
//  Copyright © 2018 iphtech-28. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for Launcher.
FOUNDATION_EXPORT double LauncherVersionNumber;

//! Project version string for Launcher.
FOUNDATION_EXPORT const unsigned char LauncherVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <Launcher/PublicHeader.h>

#import <Launcher/Launch.h>
