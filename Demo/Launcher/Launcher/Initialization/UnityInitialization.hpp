//
//  UnityInitialization.hpp
//  Launcher
//
//  Created by iphtech-28 on 6/5/18.
//  Copyright © 2018 iphtech-28. All rights reserved.
//

#ifndef UnityInitialization_hpp
#define UnityInitialization_hpp

#include <stdio.h>
int UNITY(int argc, char* argv[], const char* appDelegateName);
#endif /* UnityInitialization_hpp */
