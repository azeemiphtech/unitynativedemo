//
//  Launch.m
//  Launcher
//
//  Created by iphtech-28 on 6/5/18.
//  Copyright © 2018 iphtech-28. All rights reserved.
//

#import "Launch.h"
#import "UnityInitialization.hpp"
#import "UnityAppController.h"
@interface Launch()
@property (nonatomic,assign) bool isRunning;
@end
@implementation Launch
+ (int)initializeUnity:(int)argc argv:(char**)argv appDelegateName:(NSString *)appDelegateName {
    return UNITY(argc, argv, appDelegateName.UTF8String);
}
+ (UnityAppController *)controller {
    static UnityAppController *instance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[UnityAppController alloc] init];
    });
    
    return instance;
}
- (void)start {
    if (!self.isRunning) {
        self.isRunning = true;
        [self applicationDidBecomeActive:UIApplication.sharedApplication];
    }
}
- (void)stop {
    if (self.isRunning) {
        [self applicationWillResignActive:UIApplication.sharedApplication];
        self.isRunning = false;
    }
}

- (UIView *)view {
    return UnityGetGLView();
}


- (NSUInteger)application:(UIApplication*)application supportedInterfaceOrientationsForWindow:(UIWindow*)window
{
    return [[Launch controller] application:application supportedInterfaceOrientationsForWindow:window];
}

- (void)application:(UIApplication*)application didReceiveLocalNotification:(UILocalNotification*)notification
{
    [[Launch controller] application:application didReceiveLocalNotification:notification];
}


- (void)application:(UIApplication*)application didReceiveRemoteNotification:(NSDictionary*)userInfo
{
    [[Launch controller] application:application didReceiveRemoteNotification:userInfo];
}
- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
{
    [[Launch controller] application:application didRegisterForRemoteNotificationsWithDeviceToken:deviceToken];
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult result))handler
{
    [[Launch controller] application:application didReceiveRemoteNotification:userInfo fetchCompletionHandler:handler];
}

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error
{
    [[Launch controller] application:application didFailToRegisterForRemoteNotificationsWithError:error];
}

- (BOOL)application:(UIApplication*)application openURL:(NSURL*)url sourceApplication:(NSString*)sourceApplication annotation:(id)annotation
{
    return [[Launch controller] application:application openURL:url sourceApplication:sourceApplication annotation:annotation];
}
- (BOOL)application:(UIApplication*)application willFinishLaunchingWithOptions:(NSDictionary*)launchOptions
{
    [[Launch controller] application:application willFinishLaunchingWithOptions:launchOptions];
    return YES;
}

- (BOOL)application:(UIApplication*)application didFinishLaunchingWithOptions:(NSDictionary*)launchOptions
{
    [[Launch controller] application:application didFinishLaunchingWithOptions:launchOptions];
    
    return YES;
}

- (void)applicationDidEnterBackground:(UIApplication*)application
{
    if (self.isRunning) {
        [[Launch controller] applicationDidEnterBackground:application];
    }
}

- (void)applicationWillEnterForeground:(UIApplication*)application
{
    if (self.isRunning) {
        [[Launch controller] applicationWillEnterForeground:application];
    }
}
- (void)applicationDidBecomeActive:(UIApplication*)application
{
    if (self.isRunning) {
        [[Launch controller] applicationDidBecomeActive:application];
    }
}

- (void)applicationWillResignActive:(UIApplication*)application
{
    if (self.isRunning) {
        [[Launch controller] applicationWillResignActive:application];
    }
}

- (void)applicationDidReceiveMemoryWarning:(UIApplication*)application
{
    [[Launch controller] applicationDidReceiveMemoryWarning:application];
}

- (void)applicationWillTerminate:(UIApplication*)application
{
    [[Launch controller] applicationWillTerminate:application];
}

@end
