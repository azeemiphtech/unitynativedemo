﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// UnityEngine.AddComponentMenu
struct AddComponentMenu_t3529181215;
// System.String
struct String_t;
// System.Attribute
struct Attribute_t1659210826;
// UnityEngine.AnimationCurve
struct AnimationCurve_t1726276845;
// UnityEngine.Keyframe[]
struct KeyframeU5BU5D_t1322505427;
// UnityEngine.Application/LowMemoryCallback
struct LowMemoryCallback_t454721290;
// UnityEngine.Application/LogCallback
struct LogCallback_t3381938013;
// UnityEngine.Events.UnityAction
struct UnityAction_t35329723;
// System.IAsyncResult
struct IAsyncResult_t2277996523;
// System.AsyncCallback
struct AsyncCallback_t1046330627;
// UnityEngine.AsyncOperation
struct AsyncOperation_t4233810744;
// System.Action`1<UnityEngine.AsyncOperation>
struct Action_1_t3243625498;
// System.Action`1<System.Object>
struct Action_1_t2305831757;
// System.Type
struct Type_t;
// System.Collections.Generic.Stack`1<System.Type>
struct Stack_1_t2864815562;
// System.Collections.Generic.Stack`1<System.Object>
struct Stack_1_t3886787695;
// System.Type[]
struct TypeU5BU5D_t1830665827;
// System.Collections.Generic.List`1<System.Type>
struct List_1_t152613186;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t1174585319;
// System.Object[]
struct ObjectU5BU5D_t4290686858;
// UnityEngine.RequireComponent
struct RequireComponent_t597737523;
// UnityEngine.DefaultExecutionOrder
struct DefaultExecutionOrder_t3862449190;
// UnityEngine.DisallowMultipleComponent
struct DisallowMultipleComponent_t2957315911;
// UnityEngine.ExecuteInEditMode
struct ExecuteInEditMode_t1836144016;
// UnityEngine.Behaviour
struct Behaviour_t4110946901;
// UnityEngine.Component
struct Component_t2160255836;
// UnityEngine.Bindings.UnmarshalledAttribute
struct UnmarshalledAttribute_t2695814082;
// UnityEngine.Camera
struct Camera_t101201881;
// UnityEngine.RenderTexture
struct RenderTexture_t2667189599;
// UnityEngine.Camera[]
struct CameraU5BU5D_t2722699428;
// UnityEngine.Camera/CameraCallback
struct CameraCallback_t2065492188;
// UnityEngine.GameObject
struct GameObject_t3732643618;
// UnityEngine.Object
struct Object_t2461733472;
// UnityEngine.Coroutine
struct Coroutine_t3619068392;
// UnityEngine.CSSLayout.CSSMeasureFunc
struct CSSMeasureFunc_t2162796896;
// System.Collections.Generic.Dictionary`2<System.IntPtr,System.WeakReference>
struct Dictionary_2_t990142682;
// System.WeakReference
struct WeakReference_t3998924468;
// System.Collections.Generic.Dictionary`2<System.IntPtr,System.Object>
struct Dictionary_2_t287235217;
// UnityEngine.CullingGroup
struct CullingGroup_t1255328198;
// UnityEngine.CullingGroup/StateChanged
struct StateChanged_t2514413929;
// UnityEngine.ILogger
struct ILogger_t2992480098;
// System.Exception
struct Exception_t3361176243;
// UnityEngine.DebugLogHandler
struct DebugLogHandler_t1401667417;
// UnityEngine.Logger
struct Logger_t605719344;
// UnityEngine.ILogHandler
struct ILogHandler_t2217299658;
// UnityEngine.Display
struct Display_t4002355288;
// System.IntPtr[]
struct IntPtrU5BU5D_t2893013242;
// UnityEngine.Display/DisplaysUpdatedDelegate
struct DisplaysUpdatedDelegate_t2805775822;
// UnityEngine.Events.ArgumentCache
struct ArgumentCache_t3682645796;
// UnityEngine.Events.BaseInvokableCall
struct BaseInvokableCall_t2920707967;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.ArgumentNullException
struct ArgumentNullException_t517973450;
// System.Delegate
struct Delegate_t96267039;
// UnityEngine.Events.InvokableCall
struct InvokableCall_t2876658856;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t1673367030;
// System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>
struct List_1_t799276283;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Events.BaseInvokableCall>
struct IEnumerable_1_t1037543098;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t1412852134;
// UnityEngine.Events.PersistentCall
struct PersistentCall_t2687057912;
// UnityEngine.Events.UnityEventBase
struct UnityEventBase_t3171537072;
// UnityEngine.Events.CachedInvokableCall`1<System.Single>
struct CachedInvokableCall_1_t1592702797;
// UnityEngine.Events.CachedInvokableCall`1<System.Int32>
struct CachedInvokableCall_1_t3666972114;
// UnityEngine.Events.CachedInvokableCall`1<System.String>
struct CachedInvokableCall_1_t4079887551;
// UnityEngine.Events.CachedInvokableCall`1<System.Object>
struct CachedInvokableCall_1_t96886622;
// UnityEngine.Events.CachedInvokableCall`1<System.Boolean>
struct CachedInvokableCall_1_t3155509814;
// System.Reflection.ConstructorInfo
struct ConstructorInfo_t3360520918;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t586105043;
// System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>
struct List_1_t565626228;
// UnityEngine.Events.UnityEvent
struct UnityEvent_t1464431504;
// System.Reflection.Binder
struct Binder_t2856902408;
// System.Reflection.ParameterModifier[]
struct ParameterModifierU5BU5D_t3522550039;
// System.Reflection.ParameterInfo
struct ParameterInfo_t3893964560;
// UnityEngine.Experimental.Rendering.IRenderPipeline
struct IRenderPipeline_t2460632778;
// UnityEngine.Experimental.Rendering.IRenderPipelineAsset
struct IRenderPipelineAsset_t171638552;
// UnityEngine.Gradient
struct Gradient_t130030130;
// UnityEngine.GUIElement
struct GUIElement_t1292848738;
// UnityEngine.GUILayer
struct GUILayer_t1929752731;
// UnityEngine.Internal.DefaultValueAttribute
struct DefaultValueAttribute_t2693326634;
// UnityEngine.iOS.LocalNotification
struct LocalNotification_t2133447199;
// UnityEngine.iOS.RemoteNotification
struct RemoteNotification_t414806740;
// System.IO.Stream
struct Stream_t2349536632;
// System.ArgumentException
struct ArgumentException_t1372035057;
// System.Byte[]
struct ByteU5BU5D_t1014014593;
// System.IndexOutOfRangeException
struct IndexOutOfRangeException_t3041194587;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t2904604993;
// UnityEngine.NativeClassAttribute
struct NativeClassAttribute_t3085471410;
// UnityEngine.Networking.PlayerConnection.MessageEventArgs
struct MessageEventArgs_t947408028;
// UnityEngine.Networking.PlayerConnection.PlayerConnection
struct PlayerConnection_t3559457672;
// UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents
struct PlayerEditorConnectionEvents_t2151463461;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t449703515;
// UnityEngine.ScriptableObject
struct ScriptableObject_t476655885;
// UnityEngine.Events.UnityEvent`1<System.Int32>
struct UnityEvent_1_t3088907843;
// System.Collections.Generic.List`1<UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers>
struct List_1_t3831528118;
// UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/ConnectionChangeEvent
struct ConnectionChangeEvent_t2308234365;
// UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/<InvokeMessageIdSubscribers>c__AnonStorey0
struct U3CInvokeMessageIdSubscribersU3Ec__AnonStorey0_t1928087868;
// System.Func`2<UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers,System.Boolean>
struct Func_2_t874238247;
// System.Func`2<System.Object,System.Boolean>
struct Func_2_t1469056018;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers>
struct IEnumerable_1_t4069794933;
// UnityEngine.Events.UnityEvent`1<UnityEngine.Networking.PlayerConnection.MessageEventArgs>
struct UnityEvent_1_t1465180672;
// UnityEngine.Events.UnityEvent`1<System.Object>
struct UnityEvent_1_t3813789647;
// UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers
struct MessageTypeSubscribers_t1657992506;
// UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageEvent
struct MessageEvent_t2016149491;
// UnityEngine.Playables.PlayableAsset
struct PlayableAsset_t2436284446;
// UnityEngine.Playables.PlayableBehaviour
struct PlayableBehaviour_t4011616635;
// UnityEngine.RectTransform
struct RectTransform_t3950565364;
// UnityEngine.RectTransform/ReapplyDrivenProperties
struct ReapplyDrivenProperties_t3880910420;
// UnityEngine.Gyroscope
struct Gyroscope_t1916239707;
// UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers[]
struct MessageTypeSubscribersU5BU5D_t3695869087;
// System.Int32[]
struct Int32U5BU5D_t2856113350;
// System.Collections.Generic.Link[]
struct LinkU5BU5D_t1176066778;
// System.WeakReference[]
struct WeakReferenceU5BU5D_t1075252221;
// System.Collections.Generic.IEqualityComparer`1<System.IntPtr>
struct IEqualityComparer_1_t2703523616;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t3667883434;
// System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.WeakReference,System.Collections.DictionaryEntry>
struct Transform_1_t395897088;
// System.Collections.IDictionary
struct IDictionary_t1911576662;
// UnityEngine.Events.BaseInvokableCall[]
struct BaseInvokableCallU5BU5D_t3135018918;
// UnityEngine.Events.PersistentCall[]
struct PersistentCallU5BU5D_t3052710185;
// System.Char[]
struct CharU5BU5D_t2864810077;
// UnityEngine.DisallowMultipleComponent[]
struct DisallowMultipleComponentU5BU5D_t773128126;
// UnityEngine.ExecuteInEditMode[]
struct ExecuteInEditModeU5BU5D_t2797163057;
// UnityEngine.RequireComponent[]
struct RequireComponentU5BU5D_t2045948322;
// UnityEngine.Events.UnityAction`1<System.Boolean>
struct UnityAction_1_t3693684563;
// UnityEngine.Events.UnityAction`1<System.String>
struct UnityAction_1_t323095004;
// UnityEngine.Events.UnityAction`1<System.Int32>
struct UnityAction_1_t4205146863;
// System.Void
struct Void_t1078098495;
// UnityEngine.Events.UnityAction`1<System.Single>
struct UnityAction_1_t2130877546;
// System.Boolean[]
struct BooleanU5BU5D_t129461842;
// System.Security.Cryptography.RandomNumberGenerator
struct RandomNumberGenerator_t4107616314;
// UnityEngine.Display[]
struct DisplayU5BU5D_t130122569;
// System.DelegateData
struct DelegateData_t2624639528;
// UnityEngine.Playables.PlayableBinding[]
struct PlayableBindingU5BU5D_t207642577;
// System.Reflection.MemberFilter
struct MemberFilter_t1197335030;
// System.Reflection.MemberInfo
struct MemberInfo_t;
// System.Reflection.Emit.UnmanagedMarshal
struct UnmanagedMarshal_t1503927065;
// System.String[]
struct StringU5BU5D_t522578821;

extern RuntimeClass* Application_t2657524047_il2cpp_TypeInfo_var;
extern const uint32_t Application_CallLowMemory_m1626726542_MetadataUsageId;
extern const uint32_t Application_CallLogCallback_m3808806632_MetadataUsageId;
extern const uint32_t Application_InvokeOnBeforeRender_m818804610_MetadataUsageId;
extern RuntimeClass* LogType_t1259498875_il2cpp_TypeInfo_var;
extern const uint32_t LogCallback_BeginInvoke_m1314223040_MetadataUsageId;
extern RuntimeClass* Action_1_t3243625498_il2cpp_TypeInfo_var;
extern const uint32_t AssetBundleCreateRequest_t3985900355_pinvoke_FromNativeMethodDefinition_MetadataUsageId;
extern const uint32_t AssetBundleCreateRequest_t3985900355_com_FromNativeMethodDefinition_MetadataUsageId;
extern const uint32_t AssetBundleRequest_t2873072919_pinvoke_FromNativeMethodDefinition_MetadataUsageId;
extern const uint32_t AssetBundleRequest_t2873072919_com_FromNativeMethodDefinition_MetadataUsageId;
extern const uint32_t AsyncOperation_t4233810744_pinvoke_FromNativeMethodDefinition_MetadataUsageId;
extern const uint32_t AsyncOperation_t4233810744_com_FromNativeMethodDefinition_MetadataUsageId;
extern const RuntimeMethod* Action_1_Invoke_m258234264_RuntimeMethod_var;
extern const uint32_t AsyncOperation_InvokeCompletionEvent_m737953261_MetadataUsageId;
extern const RuntimeType* MonoBehaviour_t2904604993_0_0_0_var;
extern const RuntimeType* DisallowMultipleComponent_t2957315911_0_0_0_var;
extern RuntimeClass* Stack_1_t2864815562_il2cpp_TypeInfo_var;
extern RuntimeClass* Type_t_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Stack_1__ctor_m3382330311_RuntimeMethod_var;
extern const RuntimeMethod* Stack_1_Push_m2062010219_RuntimeMethod_var;
extern const RuntimeMethod* Stack_1_Pop_m2423581918_RuntimeMethod_var;
extern const RuntimeMethod* Stack_1_get_Count_m865256008_RuntimeMethod_var;
extern const uint32_t AttributeHelperEngine_GetParentTypeDisallowingMultipleInclusion_m1021838127_MetadataUsageId;
extern const RuntimeType* RequireComponent_t597737523_0_0_0_var;
extern RuntimeClass* RequireComponentU5BU5D_t2045948322_il2cpp_TypeInfo_var;
extern RuntimeClass* TypeU5BU5D_t1830665827_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_t152613186_il2cpp_TypeInfo_var;
extern const RuntimeMethod* List_1__ctor_m1793766529_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_m2364263606_RuntimeMethod_var;
extern const RuntimeMethod* List_1_ToArray_m289903370_RuntimeMethod_var;
extern const uint32_t AttributeHelperEngine_GetRequiredComponents_m2091293497_MetadataUsageId;
extern const RuntimeType* ExecuteInEditMode_t1836144016_0_0_0_var;
extern const uint32_t AttributeHelperEngine_CheckIsEditorScript_m2212222995_MetadataUsageId;
extern RuntimeClass* AttributeHelperEngine_t3367628457_il2cpp_TypeInfo_var;
extern const RuntimeMethod* AttributeHelperEngine_GetCustomAttributeOfType_TisDefaultExecutionOrder_t3862449190_m4154997828_RuntimeMethod_var;
extern const uint32_t AttributeHelperEngine_GetDefaultExecutionOrderFor_m1362990272_MetadataUsageId;
extern RuntimeClass* DisallowMultipleComponentU5BU5D_t773128126_il2cpp_TypeInfo_var;
extern RuntimeClass* ExecuteInEditModeU5BU5D_t2797163057_il2cpp_TypeInfo_var;
extern const uint32_t AttributeHelperEngine__cctor_m3752452713_MetadataUsageId;
extern RuntimeClass* Camera_t101201881_il2cpp_TypeInfo_var;
extern const uint32_t Camera_FireOnPreCull_m2186829015_MetadataUsageId;
extern const uint32_t Camera_FireOnPreRender_m1563217163_MetadataUsageId;
extern const uint32_t Camera_FireOnPostRender_m4290449375_MetadataUsageId;
extern RuntimeClass* ObjectU5BU5D_t4290686858_il2cpp_TypeInfo_var;
extern RuntimeClass* Single_t496865882_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2330298027;
extern const uint32_t Color_ToString_m4292820139_MetadataUsageId;
extern RuntimeClass* Color_t4024113822_il2cpp_TypeInfo_var;
extern const uint32_t Color_Equals_m3350585184_MetadataUsageId;
extern RuntimeClass* Object_t2461733472_il2cpp_TypeInfo_var;
extern const uint32_t Component__ctor_m2592563656_MetadataUsageId;
extern RuntimeClass* IntPtr_t_il2cpp_TypeInfo_var;
extern RuntimeClass* CSSMeasureMode_t3615638984_il2cpp_TypeInfo_var;
extern const uint32_t CSSMeasureFunc_BeginInvoke_m3083536890_MetadataUsageId;
extern RuntimeClass* Native_t4028515775_il2cpp_TypeInfo_var;
extern RuntimeClass* CSSMeasureFunc_t2162796896_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Dictionary_2_TryGetValue_m3843825608_RuntimeMethod_var;
extern const uint32_t Native_CSSNodeGetMeasureFunc_m4025476427_MetadataUsageId;
extern const uint32_t Native_CSSNodeMeasureInvoke_m1142609737_MetadataUsageId;
extern RuntimeClass* Dictionary_2_t990142682_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Dictionary_2__ctor_m4269798443_RuntimeMethod_var;
extern const uint32_t Native__cctor_m2631422355_MetadataUsageId;
extern RuntimeClass* StateChanged_t2514413929_il2cpp_TypeInfo_var;
extern const uint32_t CullingGroup_t1255328198_pinvoke_FromNativeMethodDefinition_MetadataUsageId;
extern const uint32_t CullingGroup_t1255328198_com_FromNativeMethodDefinition_MetadataUsageId;
extern const uint32_t CullingGroup_Finalize_m3186244642_MetadataUsageId;
extern RuntimeClass* CullingGroupEvent_t157813866_il2cpp_TypeInfo_var;
extern const uint32_t CullingGroup_SendEvents_m3781512400_MetadataUsageId;
extern const uint32_t StateChanged_BeginInvoke_m1155489289_MetadataUsageId;
extern RuntimeClass* Debug_t2165388762_il2cpp_TypeInfo_var;
extern const uint32_t Debug_get_unityLogger_m2700972930_MetadataUsageId;
extern RuntimeClass* ILogger_t2992480098_il2cpp_TypeInfo_var;
extern const uint32_t Debug_Log_m2305178311_MetadataUsageId;
extern const uint32_t Debug_LogError_m1070074371_MetadataUsageId;
extern RuntimeClass* ILogHandler_t2217299658_il2cpp_TypeInfo_var;
extern const uint32_t Debug_LogException_m57812199_MetadataUsageId;
extern RuntimeClass* DebugLogHandler_t1401667417_il2cpp_TypeInfo_var;
extern RuntimeClass* Logger_t605719344_il2cpp_TypeInfo_var;
extern const uint32_t Debug__cctor_m1871084662_MetadataUsageId;
extern RuntimeClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t DebugLogHandler_LogFormat_m3907003566_MetadataUsageId;
extern RuntimeClass* DisplayU5BU5D_t130122569_il2cpp_TypeInfo_var;
extern RuntimeClass* Display_t4002355288_il2cpp_TypeInfo_var;
extern const uint32_t Display_RecreateDisplayList_m3512407007_MetadataUsageId;
extern const uint32_t Display_FireDisplaysUpdated_m3194723814_MetadataUsageId;
extern const uint32_t Display__cctor_m3585651147_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2088885139;
extern Il2CppCodeGenString* _stringLiteral3374150706;
extern Il2CppCodeGenString* _stringLiteral4013295064;
extern Il2CppCodeGenString* _stringLiteral1114441295;
extern Il2CppCodeGenString* _stringLiteral992642132;
extern Il2CppCodeGenString* _stringLiteral3749740686;
extern const uint32_t ArgumentCache_TidyAssemblyTypeName_m2531759392_MetadataUsageId;
extern RuntimeClass* ArgumentNullException_t517973450_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2440078237;
extern Il2CppCodeGenString* _stringLiteral3966407617;
extern const uint32_t BaseInvokableCall__ctor_m3419792134_MetadataUsageId;
extern const uint32_t BaseInvokableCall_AllowInvoke_m1751538175_MetadataUsageId;
extern const RuntimeType* UnityAction_t35329723_0_0_0_var;
extern RuntimeClass* UnityAction_t35329723_il2cpp_TypeInfo_var;
extern const uint32_t InvokableCall__ctor_m2974797033_MetadataUsageId;
extern const uint32_t InvokableCall_add_Delegate_m2772852427_MetadataUsageId;
extern const uint32_t InvokableCall_remove_Delegate_m599836031_MetadataUsageId;
extern RuntimeClass* List_1_t799276283_il2cpp_TypeInfo_var;
extern const RuntimeMethod* List_1__ctor_m2027075001_RuntimeMethod_var;
extern const uint32_t InvokableCallList__ctor_m386930243_MetadataUsageId;
extern const RuntimeMethod* List_1_Add_m3979596576_RuntimeMethod_var;
extern const uint32_t InvokableCallList_AddPersistentInvokableCall_m3306354928_MetadataUsageId;
extern const RuntimeMethod* List_1_Clear_m550264580_RuntimeMethod_var;
extern const uint32_t InvokableCallList_ClearPersistent_m1797712781_MetadataUsageId;
extern const RuntimeMethod* List_1_AddRange_m3917463589_RuntimeMethod_var;
extern const uint32_t InvokableCallList_PrepareInvoke_m2613780459_MetadataUsageId;
extern RuntimeClass* ArgumentCache_t3682645796_il2cpp_TypeInfo_var;
extern const uint32_t PersistentCall__ctor_m2649643936_MetadataUsageId;
extern const uint32_t PersistentCall_IsValid_m2172430097_MetadataUsageId;
extern RuntimeClass* CachedInvokableCall_1_t1592702797_il2cpp_TypeInfo_var;
extern RuntimeClass* CachedInvokableCall_1_t3666972114_il2cpp_TypeInfo_var;
extern RuntimeClass* CachedInvokableCall_1_t4079887551_il2cpp_TypeInfo_var;
extern RuntimeClass* CachedInvokableCall_1_t3155509814_il2cpp_TypeInfo_var;
extern RuntimeClass* InvokableCall_t2876658856_il2cpp_TypeInfo_var;
extern const RuntimeMethod* CachedInvokableCall_1__ctor_m2316491615_RuntimeMethod_var;
extern const RuntimeMethod* CachedInvokableCall_1__ctor_m245935429_RuntimeMethod_var;
extern const RuntimeMethod* CachedInvokableCall_1__ctor_m2544757654_RuntimeMethod_var;
extern const RuntimeMethod* CachedInvokableCall_1__ctor_m498507028_RuntimeMethod_var;
extern const uint32_t PersistentCall_GetRuntimeCall_m1057898321_MetadataUsageId;
extern const RuntimeType* Object_t2461733472_0_0_0_var;
extern const RuntimeType* CachedInvokableCall_1_t4137603102_0_0_0_var;
extern const RuntimeType* MethodInfo_t_0_0_0_var;
extern RuntimeClass* BaseInvokableCall_t2920707967_il2cpp_TypeInfo_var;
extern const uint32_t PersistentCall_GetObjectCall_m2893916170_MetadataUsageId;
extern RuntimeClass* List_1_t565626228_il2cpp_TypeInfo_var;
extern const RuntimeMethod* List_1__ctor_m43236579_RuntimeMethod_var;
extern const uint32_t PersistentCallGroup__ctor_m2062212555_MetadataUsageId;
extern const RuntimeMethod* List_1_GetEnumerator_m1074128493_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_get_Current_m4156657100_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_MoveNext_m2090417815_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_Dispose_m2940250647_RuntimeMethod_var;
extern const uint32_t PersistentCallGroup_Initialize_m644878480_MetadataUsageId;
extern const uint32_t UnityEvent_FindMethod_Impl_m3636630720_MetadataUsageId;
extern const uint32_t UnityEvent_GetDelegate_m452312904_MetadataUsageId;
extern RuntimeClass* InvokableCallList_t1673367030_il2cpp_TypeInfo_var;
extern RuntimeClass* PersistentCallGroup_t586105043_il2cpp_TypeInfo_var;
extern const uint32_t UnityEventBase__ctor_m1419821417_MetadataUsageId;
extern const uint32_t UnityEventBase_FindMethod_m595753448_MetadataUsageId;
extern const RuntimeType* Single_t496865882_0_0_0_var;
extern const RuntimeType* Int32_t2571135199_0_0_0_var;
extern const RuntimeType* Boolean_t2059672899_0_0_0_var;
extern const RuntimeType* String_t_0_0_0_var;
extern const uint32_t UnityEventBase_FindMethod_m3931843611_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral578152108;
extern const uint32_t UnityEventBase_ToString_m2520642732_MetadataUsageId;
extern const RuntimeType* RuntimeObject_0_0_0_var;
extern const uint32_t UnityEventBase_GetValidMethodInfo_m1004474682_MetadataUsageId;
extern RuntimeClass* RenderPipelineManager_t3681178706_il2cpp_TypeInfo_var;
extern const uint32_t RenderPipelineManager_get_currentPipeline_m3103495424_MetadataUsageId;
extern const uint32_t RenderPipelineManager_set_currentPipeline_m3602131277_MetadataUsageId;
extern RuntimeClass* IRenderPipelineAsset_t171638552_il2cpp_TypeInfo_var;
extern const uint32_t RenderPipelineManager_CleanupRenderPipeline_m309528094_MetadataUsageId;
extern RuntimeClass* IRenderPipeline_t2460632778_il2cpp_TypeInfo_var;
extern const uint32_t RenderPipelineManager_DoRenderLoop_Internal_m124400281_MetadataUsageId;
extern const uint32_t RenderPipelineManager_PrepareRenderPipeline_m896043942_MetadataUsageId;
extern const uint32_t GameObject__ctor_m717643193_MetadataUsageId;
extern RuntimeClass* Input_t2650329352_il2cpp_TypeInfo_var;
extern const uint32_t Input_get_mousePosition_m3311395491_MetadataUsageId;
extern const uint32_t Input__cctor_m865649194_MetadataUsageId;
extern RuntimeClass* DefaultValueAttribute_t2693326634_il2cpp_TypeInfo_var;
extern const uint32_t DefaultValueAttribute_Equals_m3424877481_MetadataUsageId;
extern RuntimeClass* LocalNotification_t2133447199_il2cpp_TypeInfo_var;
extern const uint32_t LocalNotification__cctor_m140754005_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral614899718;
extern const uint32_t Logger_GetString_m2492438982_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1047698264;
extern const uint32_t Logger_Log_m2914072330_MetadataUsageId;
extern const uint32_t Logger_LogFormat_m1581286573_MetadataUsageId;
extern const uint32_t Logger_LogException_m1366382550_MetadataUsageId;
extern RuntimeClass* ArgumentException_t1372035057_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1321367729;
extern Il2CppCodeGenString* _stringLiteral2562582605;
extern Il2CppCodeGenString* _stringLiteral1188424634;
extern Il2CppCodeGenString* _stringLiteral2966094564;
extern const uint32_t ManagedStreamHelpers_ValidateLoadFromStream_m2537091486_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral904118955;
extern Il2CppCodeGenString* _stringLiteral310943400;
extern const uint32_t ManagedStreamHelpers_ManagedStreamRead_m4024070239_MetadataUsageId;
extern const uint32_t ManagedStreamHelpers_ManagedStreamSeek_m504681536_MetadataUsageId;
extern const uint32_t ManagedStreamHelpers_ManagedStreamLength_m1632106524_MetadataUsageId;
extern RuntimeClass* Mathf_t1822574071_il2cpp_TypeInfo_var;
extern const uint32_t Mathf_Approximately_m1455183363_MetadataUsageId;
extern RuntimeClass* MathfInternal_t1431629077_il2cpp_TypeInfo_var;
extern const uint32_t Mathf__cctor_m3961786392_MetadataUsageId;
extern RuntimeClass* Matrix4x4_t3534180298_il2cpp_TypeInfo_var;
extern RuntimeClass* Vector4_t4052901136_il2cpp_TypeInfo_var;
extern const uint32_t Matrix4x4_Equals_m1131202087_MetadataUsageId;
extern RuntimeClass* IndexOutOfRangeException_t3041194587_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2500430000;
extern const uint32_t Matrix4x4_GetColumn_m1315812967_MetadataUsageId;
extern const uint32_t Matrix4x4_get_identity_m3045349954_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2341087010;
extern const uint32_t Matrix4x4_ToString_m861927258_MetadataUsageId;
extern const uint32_t Matrix4x4__cctor_m770025791_MetadataUsageId;
extern RuntimeClass* PlayerEditorConnectionEvents_t2151463461_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_t449703515_il2cpp_TypeInfo_var;
extern const RuntimeMethod* List_1__ctor_m2518762847_RuntimeMethod_var;
extern const uint32_t PlayerConnection__ctor_m527712293_MetadataUsageId;
extern RuntimeClass* PlayerConnection_t3559457672_il2cpp_TypeInfo_var;
extern const uint32_t PlayerConnection_get_instance_m2190863265_MetadataUsageId;
extern const RuntimeMethod* ScriptableObject_CreateInstance_TisPlayerConnection_t3559457672_m1179650981_RuntimeMethod_var;
extern const uint32_t PlayerConnection_CreateInstance_m4274468596_MetadataUsageId;
extern RuntimeClass* ByteU5BU5D_t1014014593_il2cpp_TypeInfo_var;
extern RuntimeClass* Marshal_t4207306895_il2cpp_TypeInfo_var;
extern const uint32_t PlayerConnection_MessageCallbackInternal_m469132778_MetadataUsageId;
extern const RuntimeMethod* List_1_Add_m3295068314_RuntimeMethod_var;
extern const RuntimeMethod* UnityEvent_1_Invoke_m4202823606_RuntimeMethod_var;
extern const uint32_t PlayerConnection_ConnectedCallbackInternal_m1135070508_MetadataUsageId;
extern const uint32_t PlayerConnection_DisconnectedCallback_m2631702132_MetadataUsageId;
extern RuntimeClass* List_1_t3831528118_il2cpp_TypeInfo_var;
extern RuntimeClass* ConnectionChangeEvent_t2308234365_il2cpp_TypeInfo_var;
extern const RuntimeMethod* List_1__ctor_m539526798_RuntimeMethod_var;
extern const uint32_t PlayerEditorConnectionEvents__ctor_m1455086094_MetadataUsageId;
extern RuntimeClass* U3CInvokeMessageIdSubscribersU3Ec__AnonStorey0_t1928087868_il2cpp_TypeInfo_var;
extern RuntimeClass* Func_2_t874238247_il2cpp_TypeInfo_var;
extern RuntimeClass* Guid_t_il2cpp_TypeInfo_var;
extern RuntimeClass* MessageEventArgs_t947408028_il2cpp_TypeInfo_var;
extern RuntimeClass* IEnumerable_1_t4069794933_il2cpp_TypeInfo_var;
extern RuntimeClass* IEnumerator_1_t603347181_il2cpp_TypeInfo_var;
extern RuntimeClass* IEnumerator_t4262330110_il2cpp_TypeInfo_var;
extern RuntimeClass* IDisposable_t318870991_il2cpp_TypeInfo_var;
extern const RuntimeMethod* U3CInvokeMessageIdSubscribersU3Ec__AnonStorey0_U3CU3Em__0_m974971376_RuntimeMethod_var;
extern const RuntimeMethod* Func_2__ctor_m1624349331_RuntimeMethod_var;
extern const RuntimeMethod* Enumerable_Where_TisMessageTypeSubscribers_t1657992506_m190799551_RuntimeMethod_var;
extern const RuntimeMethod* Enumerable_Any_TisMessageTypeSubscribers_t1657992506_m1081467775_RuntimeMethod_var;
extern const RuntimeMethod* UnityEvent_1_Invoke_m523984493_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral3857039302;
extern const uint32_t PlayerEditorConnectionEvents_InvokeMessageIdSubscribers_m2032614817_MetadataUsageId;
extern const uint32_t U3CInvokeMessageIdSubscribersU3Ec__AnonStorey0_U3CU3Em__0_m974971376_MetadataUsageId;
extern const RuntimeMethod* UnityEvent_1__ctor_m1279621320_RuntimeMethod_var;
extern const uint32_t ConnectionChangeEvent__ctor_m1785426546_MetadataUsageId;
extern const RuntimeMethod* UnityEvent_1__ctor_m183532629_RuntimeMethod_var;
extern const uint32_t MessageEvent__ctor_m3751876958_MetadataUsageId;
extern RuntimeClass* MessageEvent_t2016149491_il2cpp_TypeInfo_var;
extern const uint32_t MessageTypeSubscribers__ctor_m2303372877_MetadataUsageId;
extern const uint32_t Object_Equals_m4058157391_MetadataUsageId;
extern const uint32_t Object_op_Implicit_m3975891434_MetadataUsageId;
extern const uint32_t Object_CompareBaseObjects_m3289384931_MetadataUsageId;
extern const uint32_t Object_IsNativeObjectAlive_m2916611270_MetadataUsageId;
extern const uint32_t Object_op_Equality_m2743582905_MetadataUsageId;
extern const uint32_t Object_op_Inequality_m3819358377_MetadataUsageId;
extern const uint32_t Object__cctor_m3598847078_MetadataUsageId;
extern RuntimeClass* Playable_t1760619938_il2cpp_TypeInfo_var;
extern const uint32_t Playable_get_Null_m1034340665_MetadataUsageId;
extern const uint32_t Playable__cctor_m2479433417_MetadataUsageId;
extern RuntimeClass* PlayableBinding_t136667248_il2cpp_TypeInfo_var;
extern const uint32_t PlayableAsset_get_duration_m2883676437_MetadataUsageId;
extern const uint32_t PlayableAsset_Internal_CreatePlayable_m4149673157_MetadataUsageId;
struct Object_t2461733472_marshaled_pinvoke;
struct Object_t2461733472;;
struct Object_t2461733472_marshaled_pinvoke;;
struct Object_t2461733472_marshaled_com;
struct Object_t2461733472_marshaled_com;;
extern RuntimeClass* PlayableBindingU5BU5D_t207642577_il2cpp_TypeInfo_var;
extern const uint32_t PlayableBinding__cctor_m1831276805_MetadataUsageId;
extern RuntimeClass* PlayableHandle_t3938927722_il2cpp_TypeInfo_var;
extern const uint32_t PlayableHandle_get_Null_m2516265857_MetadataUsageId;
extern const uint32_t PlayableHandle_Equals_m771590314_MetadataUsageId;
extern RuntimeClass* PlayableOutput_t3840687608_il2cpp_TypeInfo_var;
extern const uint32_t PlayableOutput__cctor_m43115862_MetadataUsageId;
extern RuntimeClass* PlayableOutputHandle_t2719521870_il2cpp_TypeInfo_var;
extern const uint32_t PlayableOutputHandle_get_Null_m3044568124_MetadataUsageId;
extern const uint32_t PlayableOutputHandle_Equals_m669007655_MetadataUsageId;
extern RuntimeClass* PropertyName_t666104458_il2cpp_TypeInfo_var;
extern const uint32_t PropertyName_Equals_m3895751744_MetadataUsageId;
extern RuntimeClass* Int32_t2571135199_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral909797061;
extern const uint32_t PropertyName_ToString_m1969781279_MetadataUsageId;
extern RuntimeClass* Vector3_t3239555143_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1596573497;
extern const uint32_t Ray_ToString_m3743478486_MetadataUsageId;
extern RuntimeClass* Rect_t1111852907_il2cpp_TypeInfo_var;
extern const uint32_t Rect_Equals_m2317586560_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral4037084165;
extern const uint32_t Rect_ToString_m2557162916_MetadataUsageId;
extern RuntimeClass* RectTransform_t3950565364_il2cpp_TypeInfo_var;
extern const uint32_t RectTransform_SendReapplyDrivenProperties_m2257690066_MetadataUsageId;

struct KeyframeU5BU5D_t1322505427;
struct ObjectU5BU5D_t4290686858;
struct TypeU5BU5D_t1830665827;
struct RequireComponentU5BU5D_t2045948322;
struct DisallowMultipleComponentU5BU5D_t773128126;
struct ExecuteInEditModeU5BU5D_t2797163057;
struct CameraU5BU5D_t2722699428;
struct IntPtrU5BU5D_t2893013242;
struct DisplayU5BU5D_t130122569;
struct ParameterInfoU5BU5D_t854811313;
struct ParameterModifierU5BU5D_t3522550039;
struct ByteU5BU5D_t1014014593;
struct PlayableBindingU5BU5D_t207642577;


#ifndef U3CMODULEU3E_T1256061007_H
#define U3CMODULEU3E_T1256061007_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t1256061007 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T1256061007_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef CLASSLIBRARYINITIALIZER_T4091167741_H
#define CLASSLIBRARYINITIALIZER_T4091167741_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ClassLibraryInitializer
struct  ClassLibraryInitializer_t4091167741  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLASSLIBRARYINITIALIZER_T4091167741_H
#ifndef INPUT_T2650329352_H
#define INPUT_T2650329352_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Input
struct  Input_t2650329352  : public RuntimeObject
{
public:

public:
};

struct Input_t2650329352_StaticFields
{
public:
	// UnityEngine.Gyroscope UnityEngine.Input::m_MainGyro
	Gyroscope_t1916239707 * ___m_MainGyro_0;

public:
	inline static int32_t get_offset_of_m_MainGyro_0() { return static_cast<int32_t>(offsetof(Input_t2650329352_StaticFields, ___m_MainGyro_0)); }
	inline Gyroscope_t1916239707 * get_m_MainGyro_0() const { return ___m_MainGyro_0; }
	inline Gyroscope_t1916239707 ** get_address_of_m_MainGyro_0() { return &___m_MainGyro_0; }
	inline void set_m_MainGyro_0(Gyroscope_t1916239707 * value)
	{
		___m_MainGyro_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_MainGyro_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUT_T2650329352_H
#ifndef MESSAGETYPESUBSCRIBERS_T1657992506_H
#define MESSAGETYPESUBSCRIBERS_T1657992506_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers
struct  MessageTypeSubscribers_t1657992506  : public RuntimeObject
{
public:
	// System.String UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers::m_messageTypeId
	String_t* ___m_messageTypeId_0;
	// System.Int32 UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers::subscriberCount
	int32_t ___subscriberCount_1;
	// UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageEvent UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers::messageCallback
	MessageEvent_t2016149491 * ___messageCallback_2;

public:
	inline static int32_t get_offset_of_m_messageTypeId_0() { return static_cast<int32_t>(offsetof(MessageTypeSubscribers_t1657992506, ___m_messageTypeId_0)); }
	inline String_t* get_m_messageTypeId_0() const { return ___m_messageTypeId_0; }
	inline String_t** get_address_of_m_messageTypeId_0() { return &___m_messageTypeId_0; }
	inline void set_m_messageTypeId_0(String_t* value)
	{
		___m_messageTypeId_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_messageTypeId_0), value);
	}

	inline static int32_t get_offset_of_subscriberCount_1() { return static_cast<int32_t>(offsetof(MessageTypeSubscribers_t1657992506, ___subscriberCount_1)); }
	inline int32_t get_subscriberCount_1() const { return ___subscriberCount_1; }
	inline int32_t* get_address_of_subscriberCount_1() { return &___subscriberCount_1; }
	inline void set_subscriberCount_1(int32_t value)
	{
		___subscriberCount_1 = value;
	}

	inline static int32_t get_offset_of_messageCallback_2() { return static_cast<int32_t>(offsetof(MessageTypeSubscribers_t1657992506, ___messageCallback_2)); }
	inline MessageEvent_t2016149491 * get_messageCallback_2() const { return ___messageCallback_2; }
	inline MessageEvent_t2016149491 ** get_address_of_messageCallback_2() { return &___messageCallback_2; }
	inline void set_messageCallback_2(MessageEvent_t2016149491 * value)
	{
		___messageCallback_2 = value;
		Il2CppCodeGenWriteBarrier((&___messageCallback_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESSAGETYPESUBSCRIBERS_T1657992506_H
#ifndef LIST_1_T3831528118_H
#define LIST_1_T3831528118_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers>
struct  List_1_t3831528118  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	MessageTypeSubscribersU5BU5D_t3695869087* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t3831528118, ____items_1)); }
	inline MessageTypeSubscribersU5BU5D_t3695869087* get__items_1() const { return ____items_1; }
	inline MessageTypeSubscribersU5BU5D_t3695869087** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(MessageTypeSubscribersU5BU5D_t3695869087* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t3831528118, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t3831528118, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t3831528118_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	MessageTypeSubscribersU5BU5D_t3695869087* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t3831528118_StaticFields, ___EmptyArray_4)); }
	inline MessageTypeSubscribersU5BU5D_t3695869087* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline MessageTypeSubscribersU5BU5D_t3695869087** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(MessageTypeSubscribersU5BU5D_t3695869087* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T3831528118_H
#ifndef NATIVE_T4028515775_H
#define NATIVE_T4028515775_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CSSLayout.Native
struct  Native_t4028515775  : public RuntimeObject
{
public:

public:
};

struct Native_t4028515775_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.IntPtr,System.WeakReference> UnityEngine.CSSLayout.Native::s_MeasureFunctions
	Dictionary_2_t990142682 * ___s_MeasureFunctions_0;

public:
	inline static int32_t get_offset_of_s_MeasureFunctions_0() { return static_cast<int32_t>(offsetof(Native_t4028515775_StaticFields, ___s_MeasureFunctions_0)); }
	inline Dictionary_2_t990142682 * get_s_MeasureFunctions_0() const { return ___s_MeasureFunctions_0; }
	inline Dictionary_2_t990142682 ** get_address_of_s_MeasureFunctions_0() { return &___s_MeasureFunctions_0; }
	inline void set_s_MeasureFunctions_0(Dictionary_2_t990142682 * value)
	{
		___s_MeasureFunctions_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_MeasureFunctions_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NATIVE_T4028515775_H
#ifndef DICTIONARY_2_T990142682_H
#define DICTIONARY_2_T990142682_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2<System.IntPtr,System.WeakReference>
struct  Dictionary_2_t990142682  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::table
	Int32U5BU5D_t2856113350* ___table_4;
	// System.Collections.Generic.Link[] System.Collections.Generic.Dictionary`2::linkSlots
	LinkU5BU5D_t1176066778* ___linkSlots_5;
	// TKey[] System.Collections.Generic.Dictionary`2::keySlots
	IntPtrU5BU5D_t2893013242* ___keySlots_6;
	// TValue[] System.Collections.Generic.Dictionary`2::valueSlots
	WeakReferenceU5BU5D_t1075252221* ___valueSlots_7;
	// System.Int32 System.Collections.Generic.Dictionary`2::touchedSlots
	int32_t ___touchedSlots_8;
	// System.Int32 System.Collections.Generic.Dictionary`2::emptySlot
	int32_t ___emptySlot_9;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_10;
	// System.Int32 System.Collections.Generic.Dictionary`2::threshold
	int32_t ___threshold_11;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::hcp
	RuntimeObject* ___hcp_12;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.Dictionary`2::serialization_info
	SerializationInfo_t3667883434 * ___serialization_info_13;
	// System.Int32 System.Collections.Generic.Dictionary`2::generation
	int32_t ___generation_14;

public:
	inline static int32_t get_offset_of_table_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t990142682, ___table_4)); }
	inline Int32U5BU5D_t2856113350* get_table_4() const { return ___table_4; }
	inline Int32U5BU5D_t2856113350** get_address_of_table_4() { return &___table_4; }
	inline void set_table_4(Int32U5BU5D_t2856113350* value)
	{
		___table_4 = value;
		Il2CppCodeGenWriteBarrier((&___table_4), value);
	}

	inline static int32_t get_offset_of_linkSlots_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t990142682, ___linkSlots_5)); }
	inline LinkU5BU5D_t1176066778* get_linkSlots_5() const { return ___linkSlots_5; }
	inline LinkU5BU5D_t1176066778** get_address_of_linkSlots_5() { return &___linkSlots_5; }
	inline void set_linkSlots_5(LinkU5BU5D_t1176066778* value)
	{
		___linkSlots_5 = value;
		Il2CppCodeGenWriteBarrier((&___linkSlots_5), value);
	}

	inline static int32_t get_offset_of_keySlots_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t990142682, ___keySlots_6)); }
	inline IntPtrU5BU5D_t2893013242* get_keySlots_6() const { return ___keySlots_6; }
	inline IntPtrU5BU5D_t2893013242** get_address_of_keySlots_6() { return &___keySlots_6; }
	inline void set_keySlots_6(IntPtrU5BU5D_t2893013242* value)
	{
		___keySlots_6 = value;
		Il2CppCodeGenWriteBarrier((&___keySlots_6), value);
	}

	inline static int32_t get_offset_of_valueSlots_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t990142682, ___valueSlots_7)); }
	inline WeakReferenceU5BU5D_t1075252221* get_valueSlots_7() const { return ___valueSlots_7; }
	inline WeakReferenceU5BU5D_t1075252221** get_address_of_valueSlots_7() { return &___valueSlots_7; }
	inline void set_valueSlots_7(WeakReferenceU5BU5D_t1075252221* value)
	{
		___valueSlots_7 = value;
		Il2CppCodeGenWriteBarrier((&___valueSlots_7), value);
	}

	inline static int32_t get_offset_of_touchedSlots_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t990142682, ___touchedSlots_8)); }
	inline int32_t get_touchedSlots_8() const { return ___touchedSlots_8; }
	inline int32_t* get_address_of_touchedSlots_8() { return &___touchedSlots_8; }
	inline void set_touchedSlots_8(int32_t value)
	{
		___touchedSlots_8 = value;
	}

	inline static int32_t get_offset_of_emptySlot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t990142682, ___emptySlot_9)); }
	inline int32_t get_emptySlot_9() const { return ___emptySlot_9; }
	inline int32_t* get_address_of_emptySlot_9() { return &___emptySlot_9; }
	inline void set_emptySlot_9(int32_t value)
	{
		___emptySlot_9 = value;
	}

	inline static int32_t get_offset_of_count_10() { return static_cast<int32_t>(offsetof(Dictionary_2_t990142682, ___count_10)); }
	inline int32_t get_count_10() const { return ___count_10; }
	inline int32_t* get_address_of_count_10() { return &___count_10; }
	inline void set_count_10(int32_t value)
	{
		___count_10 = value;
	}

	inline static int32_t get_offset_of_threshold_11() { return static_cast<int32_t>(offsetof(Dictionary_2_t990142682, ___threshold_11)); }
	inline int32_t get_threshold_11() const { return ___threshold_11; }
	inline int32_t* get_address_of_threshold_11() { return &___threshold_11; }
	inline void set_threshold_11(int32_t value)
	{
		___threshold_11 = value;
	}

	inline static int32_t get_offset_of_hcp_12() { return static_cast<int32_t>(offsetof(Dictionary_2_t990142682, ___hcp_12)); }
	inline RuntimeObject* get_hcp_12() const { return ___hcp_12; }
	inline RuntimeObject** get_address_of_hcp_12() { return &___hcp_12; }
	inline void set_hcp_12(RuntimeObject* value)
	{
		___hcp_12 = value;
		Il2CppCodeGenWriteBarrier((&___hcp_12), value);
	}

	inline static int32_t get_offset_of_serialization_info_13() { return static_cast<int32_t>(offsetof(Dictionary_2_t990142682, ___serialization_info_13)); }
	inline SerializationInfo_t3667883434 * get_serialization_info_13() const { return ___serialization_info_13; }
	inline SerializationInfo_t3667883434 ** get_address_of_serialization_info_13() { return &___serialization_info_13; }
	inline void set_serialization_info_13(SerializationInfo_t3667883434 * value)
	{
		___serialization_info_13 = value;
		Il2CppCodeGenWriteBarrier((&___serialization_info_13), value);
	}

	inline static int32_t get_offset_of_generation_14() { return static_cast<int32_t>(offsetof(Dictionary_2_t990142682, ___generation_14)); }
	inline int32_t get_generation_14() const { return ___generation_14; }
	inline int32_t* get_address_of_generation_14() { return &___generation_14; }
	inline void set_generation_14(int32_t value)
	{
		___generation_14 = value;
	}
};

struct Dictionary_2_t990142682_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,System.Collections.DictionaryEntry> System.Collections.Generic.Dictionary`2::<>f__am$cacheB
	Transform_1_t395897088 * ___U3CU3Ef__amU24cacheB_15;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheB_15() { return static_cast<int32_t>(offsetof(Dictionary_2_t990142682_StaticFields, ___U3CU3Ef__amU24cacheB_15)); }
	inline Transform_1_t395897088 * get_U3CU3Ef__amU24cacheB_15() const { return ___U3CU3Ef__amU24cacheB_15; }
	inline Transform_1_t395897088 ** get_address_of_U3CU3Ef__amU24cacheB_15() { return &___U3CU3Ef__amU24cacheB_15; }
	inline void set_U3CU3Ef__amU24cacheB_15(Transform_1_t395897088 * value)
	{
		___U3CU3Ef__amU24cacheB_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cacheB_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARY_2_T990142682_H
#ifndef DEBUG_T2165388762_H
#define DEBUG_T2165388762_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Debug
struct  Debug_t2165388762  : public RuntimeObject
{
public:

public:
};

struct Debug_t2165388762_StaticFields
{
public:
	// UnityEngine.ILogger UnityEngine.Debug::s_Logger
	RuntimeObject* ___s_Logger_0;

public:
	inline static int32_t get_offset_of_s_Logger_0() { return static_cast<int32_t>(offsetof(Debug_t2165388762_StaticFields, ___s_Logger_0)); }
	inline RuntimeObject* get_s_Logger_0() const { return ___s_Logger_0; }
	inline RuntimeObject** get_address_of_s_Logger_0() { return &___s_Logger_0; }
	inline void set_s_Logger_0(RuntimeObject* value)
	{
		___s_Logger_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_Logger_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUG_T2165388762_H
#ifndef EXCEPTION_T3361176243_H
#define EXCEPTION_T3361176243_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t3361176243  : public RuntimeObject
{
public:
	// System.IntPtr[] System.Exception::trace_ips
	IntPtrU5BU5D_t2893013242* ___trace_ips_0;
	// System.Exception System.Exception::inner_exception
	Exception_t3361176243 * ___inner_exception_1;
	// System.String System.Exception::message
	String_t* ___message_2;
	// System.String System.Exception::help_link
	String_t* ___help_link_3;
	// System.String System.Exception::class_name
	String_t* ___class_name_4;
	// System.String System.Exception::stack_trace
	String_t* ___stack_trace_5;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_6;
	// System.Int32 System.Exception::remote_stack_index
	int32_t ___remote_stack_index_7;
	// System.Int32 System.Exception::hresult
	int32_t ___hresult_8;
	// System.String System.Exception::source
	String_t* ___source_9;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_10;

public:
	inline static int32_t get_offset_of_trace_ips_0() { return static_cast<int32_t>(offsetof(Exception_t3361176243, ___trace_ips_0)); }
	inline IntPtrU5BU5D_t2893013242* get_trace_ips_0() const { return ___trace_ips_0; }
	inline IntPtrU5BU5D_t2893013242** get_address_of_trace_ips_0() { return &___trace_ips_0; }
	inline void set_trace_ips_0(IntPtrU5BU5D_t2893013242* value)
	{
		___trace_ips_0 = value;
		Il2CppCodeGenWriteBarrier((&___trace_ips_0), value);
	}

	inline static int32_t get_offset_of_inner_exception_1() { return static_cast<int32_t>(offsetof(Exception_t3361176243, ___inner_exception_1)); }
	inline Exception_t3361176243 * get_inner_exception_1() const { return ___inner_exception_1; }
	inline Exception_t3361176243 ** get_address_of_inner_exception_1() { return &___inner_exception_1; }
	inline void set_inner_exception_1(Exception_t3361176243 * value)
	{
		___inner_exception_1 = value;
		Il2CppCodeGenWriteBarrier((&___inner_exception_1), value);
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(Exception_t3361176243, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier((&___message_2), value);
	}

	inline static int32_t get_offset_of_help_link_3() { return static_cast<int32_t>(offsetof(Exception_t3361176243, ___help_link_3)); }
	inline String_t* get_help_link_3() const { return ___help_link_3; }
	inline String_t** get_address_of_help_link_3() { return &___help_link_3; }
	inline void set_help_link_3(String_t* value)
	{
		___help_link_3 = value;
		Il2CppCodeGenWriteBarrier((&___help_link_3), value);
	}

	inline static int32_t get_offset_of_class_name_4() { return static_cast<int32_t>(offsetof(Exception_t3361176243, ___class_name_4)); }
	inline String_t* get_class_name_4() const { return ___class_name_4; }
	inline String_t** get_address_of_class_name_4() { return &___class_name_4; }
	inline void set_class_name_4(String_t* value)
	{
		___class_name_4 = value;
		Il2CppCodeGenWriteBarrier((&___class_name_4), value);
	}

	inline static int32_t get_offset_of_stack_trace_5() { return static_cast<int32_t>(offsetof(Exception_t3361176243, ___stack_trace_5)); }
	inline String_t* get_stack_trace_5() const { return ___stack_trace_5; }
	inline String_t** get_address_of_stack_trace_5() { return &___stack_trace_5; }
	inline void set_stack_trace_5(String_t* value)
	{
		___stack_trace_5 = value;
		Il2CppCodeGenWriteBarrier((&___stack_trace_5), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_6() { return static_cast<int32_t>(offsetof(Exception_t3361176243, ____remoteStackTraceString_6)); }
	inline String_t* get__remoteStackTraceString_6() const { return ____remoteStackTraceString_6; }
	inline String_t** get_address_of__remoteStackTraceString_6() { return &____remoteStackTraceString_6; }
	inline void set__remoteStackTraceString_6(String_t* value)
	{
		____remoteStackTraceString_6 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_6), value);
	}

	inline static int32_t get_offset_of_remote_stack_index_7() { return static_cast<int32_t>(offsetof(Exception_t3361176243, ___remote_stack_index_7)); }
	inline int32_t get_remote_stack_index_7() const { return ___remote_stack_index_7; }
	inline int32_t* get_address_of_remote_stack_index_7() { return &___remote_stack_index_7; }
	inline void set_remote_stack_index_7(int32_t value)
	{
		___remote_stack_index_7 = value;
	}

	inline static int32_t get_offset_of_hresult_8() { return static_cast<int32_t>(offsetof(Exception_t3361176243, ___hresult_8)); }
	inline int32_t get_hresult_8() const { return ___hresult_8; }
	inline int32_t* get_address_of_hresult_8() { return &___hresult_8; }
	inline void set_hresult_8(int32_t value)
	{
		___hresult_8 = value;
	}

	inline static int32_t get_offset_of_source_9() { return static_cast<int32_t>(offsetof(Exception_t3361176243, ___source_9)); }
	inline String_t* get_source_9() const { return ___source_9; }
	inline String_t** get_address_of_source_9() { return &___source_9; }
	inline void set_source_9(String_t* value)
	{
		___source_9 = value;
		Il2CppCodeGenWriteBarrier((&___source_9), value);
	}

	inline static int32_t get_offset_of__data_10() { return static_cast<int32_t>(offsetof(Exception_t3361176243, ____data_10)); }
	inline RuntimeObject* get__data_10() const { return ____data_10; }
	inline RuntimeObject** get_address_of__data_10() { return &____data_10; }
	inline void set__data_10(RuntimeObject* value)
	{
		____data_10 = value;
		Il2CppCodeGenWriteBarrier((&____data_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTION_T3361176243_H
#ifndef DEBUGLOGHANDLER_T1401667417_H
#define DEBUGLOGHANDLER_T1401667417_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.DebugLogHandler
struct  DebugLogHandler_t1401667417  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGLOGHANDLER_T1401667417_H
#ifndef LIST_1_T449703515_H
#define LIST_1_T449703515_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<System.Int32>
struct  List_1_t449703515  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	Int32U5BU5D_t2856113350* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t449703515, ____items_1)); }
	inline Int32U5BU5D_t2856113350* get__items_1() const { return ____items_1; }
	inline Int32U5BU5D_t2856113350** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(Int32U5BU5D_t2856113350* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t449703515, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t449703515, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t449703515_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	Int32U5BU5D_t2856113350* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t449703515_StaticFields, ___EmptyArray_4)); }
	inline Int32U5BU5D_t2856113350* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline Int32U5BU5D_t2856113350** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(Int32U5BU5D_t2856113350* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T449703515_H
#ifndef PLAYEREDITORCONNECTIONEVENTS_T2151463461_H
#define PLAYEREDITORCONNECTIONEVENTS_T2151463461_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents
struct  PlayerEditorConnectionEvents_t2151463461  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers> UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents::messageTypeSubscribers
	List_1_t3831528118 * ___messageTypeSubscribers_0;
	// UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/ConnectionChangeEvent UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents::connectionEvent
	ConnectionChangeEvent_t2308234365 * ___connectionEvent_1;
	// UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/ConnectionChangeEvent UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents::disconnectionEvent
	ConnectionChangeEvent_t2308234365 * ___disconnectionEvent_2;

public:
	inline static int32_t get_offset_of_messageTypeSubscribers_0() { return static_cast<int32_t>(offsetof(PlayerEditorConnectionEvents_t2151463461, ___messageTypeSubscribers_0)); }
	inline List_1_t3831528118 * get_messageTypeSubscribers_0() const { return ___messageTypeSubscribers_0; }
	inline List_1_t3831528118 ** get_address_of_messageTypeSubscribers_0() { return &___messageTypeSubscribers_0; }
	inline void set_messageTypeSubscribers_0(List_1_t3831528118 * value)
	{
		___messageTypeSubscribers_0 = value;
		Il2CppCodeGenWriteBarrier((&___messageTypeSubscribers_0), value);
	}

	inline static int32_t get_offset_of_connectionEvent_1() { return static_cast<int32_t>(offsetof(PlayerEditorConnectionEvents_t2151463461, ___connectionEvent_1)); }
	inline ConnectionChangeEvent_t2308234365 * get_connectionEvent_1() const { return ___connectionEvent_1; }
	inline ConnectionChangeEvent_t2308234365 ** get_address_of_connectionEvent_1() { return &___connectionEvent_1; }
	inline void set_connectionEvent_1(ConnectionChangeEvent_t2308234365 * value)
	{
		___connectionEvent_1 = value;
		Il2CppCodeGenWriteBarrier((&___connectionEvent_1), value);
	}

	inline static int32_t get_offset_of_disconnectionEvent_2() { return static_cast<int32_t>(offsetof(PlayerEditorConnectionEvents_t2151463461, ___disconnectionEvent_2)); }
	inline ConnectionChangeEvent_t2308234365 * get_disconnectionEvent_2() const { return ___disconnectionEvent_2; }
	inline ConnectionChangeEvent_t2308234365 ** get_address_of_disconnectionEvent_2() { return &___disconnectionEvent_2; }
	inline void set_disconnectionEvent_2(ConnectionChangeEvent_t2308234365 * value)
	{
		___disconnectionEvent_2 = value;
		Il2CppCodeGenWriteBarrier((&___disconnectionEvent_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYEREDITORCONNECTIONEVENTS_T2151463461_H
#ifndef BASEINVOKABLECALL_T2920707967_H
#define BASEINVOKABLECALL_T2920707967_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.BaseInvokableCall
struct  BaseInvokableCall_t2920707967  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEINVOKABLECALL_T2920707967_H
#ifndef PLAYABLEBEHAVIOUR_T4011616635_H
#define PLAYABLEBEHAVIOUR_T4011616635_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.PlayableBehaviour
struct  PlayableBehaviour_t4011616635  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYABLEBEHAVIOUR_T4011616635_H
#ifndef MESSAGEEVENTARGS_T947408028_H
#define MESSAGEEVENTARGS_T947408028_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.PlayerConnection.MessageEventArgs
struct  MessageEventArgs_t947408028  : public RuntimeObject
{
public:
	// System.Int32 UnityEngine.Networking.PlayerConnection.MessageEventArgs::playerId
	int32_t ___playerId_0;
	// System.Byte[] UnityEngine.Networking.PlayerConnection.MessageEventArgs::data
	ByteU5BU5D_t1014014593* ___data_1;

public:
	inline static int32_t get_offset_of_playerId_0() { return static_cast<int32_t>(offsetof(MessageEventArgs_t947408028, ___playerId_0)); }
	inline int32_t get_playerId_0() const { return ___playerId_0; }
	inline int32_t* get_address_of_playerId_0() { return &___playerId_0; }
	inline void set_playerId_0(int32_t value)
	{
		___playerId_0 = value;
	}

	inline static int32_t get_offset_of_data_1() { return static_cast<int32_t>(offsetof(MessageEventArgs_t947408028, ___data_1)); }
	inline ByteU5BU5D_t1014014593* get_data_1() const { return ___data_1; }
	inline ByteU5BU5D_t1014014593** get_address_of_data_1() { return &___data_1; }
	inline void set_data_1(ByteU5BU5D_t1014014593* value)
	{
		___data_1 = value;
		Il2CppCodeGenWriteBarrier((&___data_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESSAGEEVENTARGS_T947408028_H
#ifndef INVOKABLECALLLIST_T1673367030_H
#define INVOKABLECALLLIST_T1673367030_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.InvokableCallList
struct  InvokableCallList_t1673367030  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall> UnityEngine.Events.InvokableCallList::m_PersistentCalls
	List_1_t799276283 * ___m_PersistentCalls_0;
	// System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall> UnityEngine.Events.InvokableCallList::m_RuntimeCalls
	List_1_t799276283 * ___m_RuntimeCalls_1;
	// System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall> UnityEngine.Events.InvokableCallList::m_ExecutingCalls
	List_1_t799276283 * ___m_ExecutingCalls_2;
	// System.Boolean UnityEngine.Events.InvokableCallList::m_NeedsUpdate
	bool ___m_NeedsUpdate_3;

public:
	inline static int32_t get_offset_of_m_PersistentCalls_0() { return static_cast<int32_t>(offsetof(InvokableCallList_t1673367030, ___m_PersistentCalls_0)); }
	inline List_1_t799276283 * get_m_PersistentCalls_0() const { return ___m_PersistentCalls_0; }
	inline List_1_t799276283 ** get_address_of_m_PersistentCalls_0() { return &___m_PersistentCalls_0; }
	inline void set_m_PersistentCalls_0(List_1_t799276283 * value)
	{
		___m_PersistentCalls_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_PersistentCalls_0), value);
	}

	inline static int32_t get_offset_of_m_RuntimeCalls_1() { return static_cast<int32_t>(offsetof(InvokableCallList_t1673367030, ___m_RuntimeCalls_1)); }
	inline List_1_t799276283 * get_m_RuntimeCalls_1() const { return ___m_RuntimeCalls_1; }
	inline List_1_t799276283 ** get_address_of_m_RuntimeCalls_1() { return &___m_RuntimeCalls_1; }
	inline void set_m_RuntimeCalls_1(List_1_t799276283 * value)
	{
		___m_RuntimeCalls_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_RuntimeCalls_1), value);
	}

	inline static int32_t get_offset_of_m_ExecutingCalls_2() { return static_cast<int32_t>(offsetof(InvokableCallList_t1673367030, ___m_ExecutingCalls_2)); }
	inline List_1_t799276283 * get_m_ExecutingCalls_2() const { return ___m_ExecutingCalls_2; }
	inline List_1_t799276283 ** get_address_of_m_ExecutingCalls_2() { return &___m_ExecutingCalls_2; }
	inline void set_m_ExecutingCalls_2(List_1_t799276283 * value)
	{
		___m_ExecutingCalls_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_ExecutingCalls_2), value);
	}

	inline static int32_t get_offset_of_m_NeedsUpdate_3() { return static_cast<int32_t>(offsetof(InvokableCallList_t1673367030, ___m_NeedsUpdate_3)); }
	inline bool get_m_NeedsUpdate_3() const { return ___m_NeedsUpdate_3; }
	inline bool* get_address_of_m_NeedsUpdate_3() { return &___m_NeedsUpdate_3; }
	inline void set_m_NeedsUpdate_3(bool value)
	{
		___m_NeedsUpdate_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVOKABLECALLLIST_T1673367030_H
#ifndef LIST_1_T799276283_H
#define LIST_1_T799276283_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>
struct  List_1_t799276283  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	BaseInvokableCallU5BU5D_t3135018918* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t799276283, ____items_1)); }
	inline BaseInvokableCallU5BU5D_t3135018918* get__items_1() const { return ____items_1; }
	inline BaseInvokableCallU5BU5D_t3135018918** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(BaseInvokableCallU5BU5D_t3135018918* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t799276283, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t799276283, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t799276283_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	BaseInvokableCallU5BU5D_t3135018918* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t799276283_StaticFields, ___EmptyArray_4)); }
	inline BaseInvokableCallU5BU5D_t3135018918* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline BaseInvokableCallU5BU5D_t3135018918** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(BaseInvokableCallU5BU5D_t3135018918* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T799276283_H
#ifndef UNITYEVENTBASE_T3171537072_H
#define UNITYEVENTBASE_T3171537072_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEventBase
struct  UnityEventBase_t3171537072  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_t1673367030 * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t586105043 * ___m_PersistentCalls_1;
	// System.String UnityEngine.Events.UnityEventBase::m_TypeName
	String_t* ___m_TypeName_2;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_3;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_t3171537072, ___m_Calls_0)); }
	inline InvokableCallList_t1673367030 * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_t1673367030 ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_t1673367030 * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Calls_0), value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_t3171537072, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t586105043 * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t586105043 ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t586105043 * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PersistentCalls_1), value);
	}

	inline static int32_t get_offset_of_m_TypeName_2() { return static_cast<int32_t>(offsetof(UnityEventBase_t3171537072, ___m_TypeName_2)); }
	inline String_t* get_m_TypeName_2() const { return ___m_TypeName_2; }
	inline String_t** get_address_of_m_TypeName_2() { return &___m_TypeName_2; }
	inline void set_m_TypeName_2(String_t* value)
	{
		___m_TypeName_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_TypeName_2), value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_3() { return static_cast<int32_t>(offsetof(UnityEventBase_t3171537072, ___m_CallsDirty_3)); }
	inline bool get_m_CallsDirty_3() const { return ___m_CallsDirty_3; }
	inline bool* get_address_of_m_CallsDirty_3() { return &___m_CallsDirty_3; }
	inline void set_m_CallsDirty_3(bool value)
	{
		___m_CallsDirty_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENTBASE_T3171537072_H
#ifndef PERSISTENTCALLGROUP_T586105043_H
#define PERSISTENTCALLGROUP_T586105043_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.PersistentCallGroup
struct  PersistentCallGroup_t586105043  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall> UnityEngine.Events.PersistentCallGroup::m_Calls
	List_1_t565626228 * ___m_Calls_0;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(PersistentCallGroup_t586105043, ___m_Calls_0)); }
	inline List_1_t565626228 * get_m_Calls_0() const { return ___m_Calls_0; }
	inline List_1_t565626228 ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(List_1_t565626228 * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Calls_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PERSISTENTCALLGROUP_T586105043_H
#ifndef LIST_1_T565626228_H
#define LIST_1_T565626228_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>
struct  List_1_t565626228  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	PersistentCallU5BU5D_t3052710185* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t565626228, ____items_1)); }
	inline PersistentCallU5BU5D_t3052710185* get__items_1() const { return ____items_1; }
	inline PersistentCallU5BU5D_t3052710185** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(PersistentCallU5BU5D_t3052710185* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t565626228, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t565626228, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t565626228_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	PersistentCallU5BU5D_t3052710185* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t565626228_StaticFields, ___EmptyArray_4)); }
	inline PersistentCallU5BU5D_t3052710185* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline PersistentCallU5BU5D_t3052710185** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(PersistentCallU5BU5D_t3052710185* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T565626228_H
#ifndef STREAM_T2349536632_H
#define STREAM_T2349536632_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.Stream
struct  Stream_t2349536632  : public RuntimeObject
{
public:

public:
};

struct Stream_t2349536632_StaticFields
{
public:
	// System.IO.Stream System.IO.Stream::Null
	Stream_t2349536632 * ___Null_0;

public:
	inline static int32_t get_offset_of_Null_0() { return static_cast<int32_t>(offsetof(Stream_t2349536632_StaticFields, ___Null_0)); }
	inline Stream_t2349536632 * get_Null_0() const { return ___Null_0; }
	inline Stream_t2349536632 ** get_address_of_Null_0() { return &___Null_0; }
	inline void set_Null_0(Stream_t2349536632 * value)
	{
		___Null_0 = value;
		Il2CppCodeGenWriteBarrier((&___Null_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAM_T2349536632_H
#ifndef BINDER_T2856902408_H
#define BINDER_T2856902408_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.Binder
struct  Binder_t2856902408  : public RuntimeObject
{
public:

public:
};

struct Binder_t2856902408_StaticFields
{
public:
	// System.Reflection.Binder System.Reflection.Binder::default_binder
	Binder_t2856902408 * ___default_binder_0;

public:
	inline static int32_t get_offset_of_default_binder_0() { return static_cast<int32_t>(offsetof(Binder_t2856902408_StaticFields, ___default_binder_0)); }
	inline Binder_t2856902408 * get_default_binder_0() const { return ___default_binder_0; }
	inline Binder_t2856902408 ** get_address_of_default_binder_0() { return &___default_binder_0; }
	inline void set_default_binder_0(Binder_t2856902408 * value)
	{
		___default_binder_0 = value;
		Il2CppCodeGenWriteBarrier((&___default_binder_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDER_T2856902408_H
#ifndef MANAGEDSTREAMHELPERS_T1729740455_H
#define MANAGEDSTREAMHELPERS_T1729740455_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ManagedStreamHelpers
struct  ManagedStreamHelpers_t1729740455  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MANAGEDSTREAMHELPERS_T1729740455_H
#ifndef RENDERPIPELINEMANAGER_T3681178706_H
#define RENDERPIPELINEMANAGER_T3681178706_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.RenderPipelineManager
struct  RenderPipelineManager_t3681178706  : public RuntimeObject
{
public:

public:
};

struct RenderPipelineManager_t3681178706_StaticFields
{
public:
	// UnityEngine.Experimental.Rendering.IRenderPipelineAsset UnityEngine.Experimental.Rendering.RenderPipelineManager::s_CurrentPipelineAsset
	RuntimeObject* ___s_CurrentPipelineAsset_0;
	// UnityEngine.Experimental.Rendering.IRenderPipeline UnityEngine.Experimental.Rendering.RenderPipelineManager::<currentPipeline>k__BackingField
	RuntimeObject* ___U3CcurrentPipelineU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_s_CurrentPipelineAsset_0() { return static_cast<int32_t>(offsetof(RenderPipelineManager_t3681178706_StaticFields, ___s_CurrentPipelineAsset_0)); }
	inline RuntimeObject* get_s_CurrentPipelineAsset_0() const { return ___s_CurrentPipelineAsset_0; }
	inline RuntimeObject** get_address_of_s_CurrentPipelineAsset_0() { return &___s_CurrentPipelineAsset_0; }
	inline void set_s_CurrentPipelineAsset_0(RuntimeObject* value)
	{
		___s_CurrentPipelineAsset_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_CurrentPipelineAsset_0), value);
	}

	inline static int32_t get_offset_of_U3CcurrentPipelineU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(RenderPipelineManager_t3681178706_StaticFields, ___U3CcurrentPipelineU3Ek__BackingField_1)); }
	inline RuntimeObject* get_U3CcurrentPipelineU3Ek__BackingField_1() const { return ___U3CcurrentPipelineU3Ek__BackingField_1; }
	inline RuntimeObject** get_address_of_U3CcurrentPipelineU3Ek__BackingField_1() { return &___U3CcurrentPipelineU3Ek__BackingField_1; }
	inline void set_U3CcurrentPipelineU3Ek__BackingField_1(RuntimeObject* value)
	{
		___U3CcurrentPipelineU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcurrentPipelineU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERPIPELINEMANAGER_T3681178706_H
#ifndef ARGUMENTCACHE_T3682645796_H
#define ARGUMENTCACHE_T3682645796_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.ArgumentCache
struct  ArgumentCache_t3682645796  : public RuntimeObject
{
public:
	// UnityEngine.Object UnityEngine.Events.ArgumentCache::m_ObjectArgument
	Object_t2461733472 * ___m_ObjectArgument_0;
	// System.String UnityEngine.Events.ArgumentCache::m_ObjectArgumentAssemblyTypeName
	String_t* ___m_ObjectArgumentAssemblyTypeName_1;
	// System.Int32 UnityEngine.Events.ArgumentCache::m_IntArgument
	int32_t ___m_IntArgument_2;
	// System.Single UnityEngine.Events.ArgumentCache::m_FloatArgument
	float ___m_FloatArgument_3;
	// System.String UnityEngine.Events.ArgumentCache::m_StringArgument
	String_t* ___m_StringArgument_4;
	// System.Boolean UnityEngine.Events.ArgumentCache::m_BoolArgument
	bool ___m_BoolArgument_5;

public:
	inline static int32_t get_offset_of_m_ObjectArgument_0() { return static_cast<int32_t>(offsetof(ArgumentCache_t3682645796, ___m_ObjectArgument_0)); }
	inline Object_t2461733472 * get_m_ObjectArgument_0() const { return ___m_ObjectArgument_0; }
	inline Object_t2461733472 ** get_address_of_m_ObjectArgument_0() { return &___m_ObjectArgument_0; }
	inline void set_m_ObjectArgument_0(Object_t2461733472 * value)
	{
		___m_ObjectArgument_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ObjectArgument_0), value);
	}

	inline static int32_t get_offset_of_m_ObjectArgumentAssemblyTypeName_1() { return static_cast<int32_t>(offsetof(ArgumentCache_t3682645796, ___m_ObjectArgumentAssemblyTypeName_1)); }
	inline String_t* get_m_ObjectArgumentAssemblyTypeName_1() const { return ___m_ObjectArgumentAssemblyTypeName_1; }
	inline String_t** get_address_of_m_ObjectArgumentAssemblyTypeName_1() { return &___m_ObjectArgumentAssemblyTypeName_1; }
	inline void set_m_ObjectArgumentAssemblyTypeName_1(String_t* value)
	{
		___m_ObjectArgumentAssemblyTypeName_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_ObjectArgumentAssemblyTypeName_1), value);
	}

	inline static int32_t get_offset_of_m_IntArgument_2() { return static_cast<int32_t>(offsetof(ArgumentCache_t3682645796, ___m_IntArgument_2)); }
	inline int32_t get_m_IntArgument_2() const { return ___m_IntArgument_2; }
	inline int32_t* get_address_of_m_IntArgument_2() { return &___m_IntArgument_2; }
	inline void set_m_IntArgument_2(int32_t value)
	{
		___m_IntArgument_2 = value;
	}

	inline static int32_t get_offset_of_m_FloatArgument_3() { return static_cast<int32_t>(offsetof(ArgumentCache_t3682645796, ___m_FloatArgument_3)); }
	inline float get_m_FloatArgument_3() const { return ___m_FloatArgument_3; }
	inline float* get_address_of_m_FloatArgument_3() { return &___m_FloatArgument_3; }
	inline void set_m_FloatArgument_3(float value)
	{
		___m_FloatArgument_3 = value;
	}

	inline static int32_t get_offset_of_m_StringArgument_4() { return static_cast<int32_t>(offsetof(ArgumentCache_t3682645796, ___m_StringArgument_4)); }
	inline String_t* get_m_StringArgument_4() const { return ___m_StringArgument_4; }
	inline String_t** get_address_of_m_StringArgument_4() { return &___m_StringArgument_4; }
	inline void set_m_StringArgument_4(String_t* value)
	{
		___m_StringArgument_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_StringArgument_4), value);
	}

	inline static int32_t get_offset_of_m_BoolArgument_5() { return static_cast<int32_t>(offsetof(ArgumentCache_t3682645796, ___m_BoolArgument_5)); }
	inline bool get_m_BoolArgument_5() const { return ___m_BoolArgument_5; }
	inline bool* get_address_of_m_BoolArgument_5() { return &___m_BoolArgument_5; }
	inline void set_m_BoolArgument_5(bool value)
	{
		___m_BoolArgument_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARGUMENTCACHE_T3682645796_H
#ifndef LIST_1_T152613186_H
#define LIST_1_T152613186_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<System.Type>
struct  List_1_t152613186  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	TypeU5BU5D_t1830665827* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t152613186, ____items_1)); }
	inline TypeU5BU5D_t1830665827* get__items_1() const { return ____items_1; }
	inline TypeU5BU5D_t1830665827** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(TypeU5BU5D_t1830665827* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t152613186, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t152613186, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t152613186_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	TypeU5BU5D_t1830665827* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t152613186_StaticFields, ___EmptyArray_4)); }
	inline TypeU5BU5D_t1830665827* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline TypeU5BU5D_t1830665827** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(TypeU5BU5D_t1830665827* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T152613186_H
#ifndef GYROSCOPE_T1916239707_H
#define GYROSCOPE_T1916239707_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Gyroscope
struct  Gyroscope_t1916239707  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GYROSCOPE_T1916239707_H
#ifndef ATTRIBUTE_T1659210826_H
#define ATTRIBUTE_T1659210826_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_t1659210826  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_T1659210826_H
#ifndef YIELDINSTRUCTION_T3999321895_H
#define YIELDINSTRUCTION_T3999321895_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.YieldInstruction
struct  YieldInstruction_t3999321895  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_t3999321895_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_t3999321895_marshaled_com
{
};
#endif // YIELDINSTRUCTION_T3999321895_H
#ifndef APPLICATION_T2657524047_H
#define APPLICATION_T2657524047_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Application
struct  Application_t2657524047  : public RuntimeObject
{
public:

public:
};

struct Application_t2657524047_StaticFields
{
public:
	// UnityEngine.Application/LowMemoryCallback UnityEngine.Application::lowMemory
	LowMemoryCallback_t454721290 * ___lowMemory_0;
	// UnityEngine.Application/LogCallback UnityEngine.Application::s_LogCallbackHandler
	LogCallback_t3381938013 * ___s_LogCallbackHandler_1;
	// UnityEngine.Application/LogCallback UnityEngine.Application::s_LogCallbackHandlerThreaded
	LogCallback_t3381938013 * ___s_LogCallbackHandlerThreaded_2;
	// UnityEngine.Events.UnityAction UnityEngine.Application::onBeforeRender
	UnityAction_t35329723 * ___onBeforeRender_3;

public:
	inline static int32_t get_offset_of_lowMemory_0() { return static_cast<int32_t>(offsetof(Application_t2657524047_StaticFields, ___lowMemory_0)); }
	inline LowMemoryCallback_t454721290 * get_lowMemory_0() const { return ___lowMemory_0; }
	inline LowMemoryCallback_t454721290 ** get_address_of_lowMemory_0() { return &___lowMemory_0; }
	inline void set_lowMemory_0(LowMemoryCallback_t454721290 * value)
	{
		___lowMemory_0 = value;
		Il2CppCodeGenWriteBarrier((&___lowMemory_0), value);
	}

	inline static int32_t get_offset_of_s_LogCallbackHandler_1() { return static_cast<int32_t>(offsetof(Application_t2657524047_StaticFields, ___s_LogCallbackHandler_1)); }
	inline LogCallback_t3381938013 * get_s_LogCallbackHandler_1() const { return ___s_LogCallbackHandler_1; }
	inline LogCallback_t3381938013 ** get_address_of_s_LogCallbackHandler_1() { return &___s_LogCallbackHandler_1; }
	inline void set_s_LogCallbackHandler_1(LogCallback_t3381938013 * value)
	{
		___s_LogCallbackHandler_1 = value;
		Il2CppCodeGenWriteBarrier((&___s_LogCallbackHandler_1), value);
	}

	inline static int32_t get_offset_of_s_LogCallbackHandlerThreaded_2() { return static_cast<int32_t>(offsetof(Application_t2657524047_StaticFields, ___s_LogCallbackHandlerThreaded_2)); }
	inline LogCallback_t3381938013 * get_s_LogCallbackHandlerThreaded_2() const { return ___s_LogCallbackHandlerThreaded_2; }
	inline LogCallback_t3381938013 ** get_address_of_s_LogCallbackHandlerThreaded_2() { return &___s_LogCallbackHandlerThreaded_2; }
	inline void set_s_LogCallbackHandlerThreaded_2(LogCallback_t3381938013 * value)
	{
		___s_LogCallbackHandlerThreaded_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_LogCallbackHandlerThreaded_2), value);
	}

	inline static int32_t get_offset_of_onBeforeRender_3() { return static_cast<int32_t>(offsetof(Application_t2657524047_StaticFields, ___onBeforeRender_3)); }
	inline UnityAction_t35329723 * get_onBeforeRender_3() const { return ___onBeforeRender_3; }
	inline UnityAction_t35329723 ** get_address_of_onBeforeRender_3() { return &___onBeforeRender_3; }
	inline void set_onBeforeRender_3(UnityAction_t35329723 * value)
	{
		___onBeforeRender_3 = value;
		Il2CppCodeGenWriteBarrier((&___onBeforeRender_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPLICATION_T2657524047_H
#ifndef VALUETYPE_T1920584095_H
#define VALUETYPE_T1920584095_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t1920584095  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t1920584095_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t1920584095_marshaled_com
{
};
#endif // VALUETYPE_T1920584095_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::length
	int32_t ___length_0;
	// System.Char System.String::start_char
	Il2CppChar ___start_char_1;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(String_t, ___length_0)); }
	inline int32_t get_length_0() const { return ___length_0; }
	inline int32_t* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(int32_t value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_start_char_1() { return static_cast<int32_t>(offsetof(String_t, ___start_char_1)); }
	inline Il2CppChar get_start_char_1() const { return ___start_char_1; }
	inline Il2CppChar* get_address_of_start_char_1() { return &___start_char_1; }
	inline void set_start_char_1(Il2CppChar value)
	{
		___start_char_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_2;
	// System.Char[] System.String::WhiteChars
	CharU5BU5D_t2864810077* ___WhiteChars_3;

public:
	inline static int32_t get_offset_of_Empty_2() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_2)); }
	inline String_t* get_Empty_2() const { return ___Empty_2; }
	inline String_t** get_address_of_Empty_2() { return &___Empty_2; }
	inline void set_Empty_2(String_t* value)
	{
		___Empty_2 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_2), value);
	}

	inline static int32_t get_offset_of_WhiteChars_3() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___WhiteChars_3)); }
	inline CharU5BU5D_t2864810077* get_WhiteChars_3() const { return ___WhiteChars_3; }
	inline CharU5BU5D_t2864810077** get_address_of_WhiteChars_3() { return &___WhiteChars_3; }
	inline void set_WhiteChars_3(CharU5BU5D_t2864810077* value)
	{
		___WhiteChars_3 = value;
		Il2CppCodeGenWriteBarrier((&___WhiteChars_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef STACK_1_T2864815562_H
#define STACK_1_T2864815562_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Stack`1<System.Type>
struct  Stack_1_t2864815562  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.Stack`1::_array
	TypeU5BU5D_t1830665827* ____array_0;
	// System.Int32 System.Collections.Generic.Stack`1::_size
	int32_t ____size_1;
	// System.Int32 System.Collections.Generic.Stack`1::_version
	int32_t ____version_2;

public:
	inline static int32_t get_offset_of__array_0() { return static_cast<int32_t>(offsetof(Stack_1_t2864815562, ____array_0)); }
	inline TypeU5BU5D_t1830665827* get__array_0() const { return ____array_0; }
	inline TypeU5BU5D_t1830665827** get_address_of__array_0() { return &____array_0; }
	inline void set__array_0(TypeU5BU5D_t1830665827* value)
	{
		____array_0 = value;
		Il2CppCodeGenWriteBarrier((&____array_0), value);
	}

	inline static int32_t get_offset_of__size_1() { return static_cast<int32_t>(offsetof(Stack_1_t2864815562, ____size_1)); }
	inline int32_t get__size_1() const { return ____size_1; }
	inline int32_t* get_address_of__size_1() { return &____size_1; }
	inline void set__size_1(int32_t value)
	{
		____size_1 = value;
	}

	inline static int32_t get_offset_of__version_2() { return static_cast<int32_t>(offsetof(Stack_1_t2864815562, ____version_2)); }
	inline int32_t get__version_2() const { return ____version_2; }
	inline int32_t* get_address_of__version_2() { return &____version_2; }
	inline void set__version_2(int32_t value)
	{
		____version_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STACK_1_T2864815562_H
#ifndef PROPERTYNAMEUTILS_T1618685890_H
#define PROPERTYNAMEUTILS_T1618685890_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PropertyNameUtils
struct  PropertyNameUtils_t1618685890  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYNAMEUTILS_T1618685890_H
#ifndef MEMBERINFO_T_H
#define MEMBERINFO_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MemberInfo
struct  MemberInfo_t  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERINFO_T_H
#ifndef ATTRIBUTEHELPERENGINE_T3367628457_H
#define ATTRIBUTEHELPERENGINE_T3367628457_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AttributeHelperEngine
struct  AttributeHelperEngine_t3367628457  : public RuntimeObject
{
public:

public:
};

struct AttributeHelperEngine_t3367628457_StaticFields
{
public:
	// UnityEngine.DisallowMultipleComponent[] UnityEngine.AttributeHelperEngine::_disallowMultipleComponentArray
	DisallowMultipleComponentU5BU5D_t773128126* ____disallowMultipleComponentArray_0;
	// UnityEngine.ExecuteInEditMode[] UnityEngine.AttributeHelperEngine::_executeInEditModeArray
	ExecuteInEditModeU5BU5D_t2797163057* ____executeInEditModeArray_1;
	// UnityEngine.RequireComponent[] UnityEngine.AttributeHelperEngine::_requireComponentArray
	RequireComponentU5BU5D_t2045948322* ____requireComponentArray_2;

public:
	inline static int32_t get_offset_of__disallowMultipleComponentArray_0() { return static_cast<int32_t>(offsetof(AttributeHelperEngine_t3367628457_StaticFields, ____disallowMultipleComponentArray_0)); }
	inline DisallowMultipleComponentU5BU5D_t773128126* get__disallowMultipleComponentArray_0() const { return ____disallowMultipleComponentArray_0; }
	inline DisallowMultipleComponentU5BU5D_t773128126** get_address_of__disallowMultipleComponentArray_0() { return &____disallowMultipleComponentArray_0; }
	inline void set__disallowMultipleComponentArray_0(DisallowMultipleComponentU5BU5D_t773128126* value)
	{
		____disallowMultipleComponentArray_0 = value;
		Il2CppCodeGenWriteBarrier((&____disallowMultipleComponentArray_0), value);
	}

	inline static int32_t get_offset_of__executeInEditModeArray_1() { return static_cast<int32_t>(offsetof(AttributeHelperEngine_t3367628457_StaticFields, ____executeInEditModeArray_1)); }
	inline ExecuteInEditModeU5BU5D_t2797163057* get__executeInEditModeArray_1() const { return ____executeInEditModeArray_1; }
	inline ExecuteInEditModeU5BU5D_t2797163057** get_address_of__executeInEditModeArray_1() { return &____executeInEditModeArray_1; }
	inline void set__executeInEditModeArray_1(ExecuteInEditModeU5BU5D_t2797163057* value)
	{
		____executeInEditModeArray_1 = value;
		Il2CppCodeGenWriteBarrier((&____executeInEditModeArray_1), value);
	}

	inline static int32_t get_offset_of__requireComponentArray_2() { return static_cast<int32_t>(offsetof(AttributeHelperEngine_t3367628457_StaticFields, ____requireComponentArray_2)); }
	inline RequireComponentU5BU5D_t2045948322* get__requireComponentArray_2() const { return ____requireComponentArray_2; }
	inline RequireComponentU5BU5D_t2045948322** get_address_of__requireComponentArray_2() { return &____requireComponentArray_2; }
	inline void set__requireComponentArray_2(RequireComponentU5BU5D_t2045948322* value)
	{
		____requireComponentArray_2 = value;
		Il2CppCodeGenWriteBarrier((&____requireComponentArray_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTEHELPERENGINE_T3367628457_H
#ifndef INVOKABLECALL_T2876658856_H
#define INVOKABLECALL_T2876658856_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.InvokableCall
struct  InvokableCall_t2876658856  : public BaseInvokableCall_t2920707967
{
public:
	// UnityEngine.Events.UnityAction UnityEngine.Events.InvokableCall::Delegate
	UnityAction_t35329723 * ___Delegate_0;

public:
	inline static int32_t get_offset_of_Delegate_0() { return static_cast<int32_t>(offsetof(InvokableCall_t2876658856, ___Delegate_0)); }
	inline UnityAction_t35329723 * get_Delegate_0() const { return ___Delegate_0; }
	inline UnityAction_t35329723 ** get_address_of_Delegate_0() { return &___Delegate_0; }
	inline void set_Delegate_0(UnityAction_t35329723 * value)
	{
		___Delegate_0 = value;
		Il2CppCodeGenWriteBarrier((&___Delegate_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVOKABLECALL_T2876658856_H
#ifndef NATIVECLASSATTRIBUTE_T3085471410_H
#define NATIVECLASSATTRIBUTE_T3085471410_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.NativeClassAttribute
struct  NativeClassAttribute_t3085471410  : public Attribute_t1659210826
{
public:
	// System.String UnityEngine.NativeClassAttribute::<QualifiedNativeName>k__BackingField
	String_t* ___U3CQualifiedNativeNameU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CQualifiedNativeNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(NativeClassAttribute_t3085471410, ___U3CQualifiedNativeNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CQualifiedNativeNameU3Ek__BackingField_0() const { return ___U3CQualifiedNativeNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CQualifiedNativeNameU3Ek__BackingField_0() { return &___U3CQualifiedNativeNameU3Ek__BackingField_0; }
	inline void set_U3CQualifiedNativeNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CQualifiedNativeNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CQualifiedNativeNameU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NATIVECLASSATTRIBUTE_T3085471410_H
#ifndef INVOKABLECALL_1_T2119843275_H
#define INVOKABLECALL_1_T2119843275_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.InvokableCall`1<System.Boolean>
struct  InvokableCall_1_t2119843275  : public BaseInvokableCall_t2920707967
{
public:
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1::Delegate
	UnityAction_1_t3693684563 * ___Delegate_0;

public:
	inline static int32_t get_offset_of_Delegate_0() { return static_cast<int32_t>(offsetof(InvokableCall_1_t2119843275, ___Delegate_0)); }
	inline UnityAction_1_t3693684563 * get_Delegate_0() const { return ___Delegate_0; }
	inline UnityAction_1_t3693684563 ** get_address_of_Delegate_0() { return &___Delegate_0; }
	inline void set_Delegate_0(UnityAction_1_t3693684563 * value)
	{
		___Delegate_0 = value;
		Il2CppCodeGenWriteBarrier((&___Delegate_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVOKABLECALL_1_T2119843275_H
#ifndef REQUIRECOMPONENT_T597737523_H
#define REQUIRECOMPONENT_T597737523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RequireComponent
struct  RequireComponent_t597737523  : public Attribute_t1659210826
{
public:
	// System.Type UnityEngine.RequireComponent::m_Type0
	Type_t * ___m_Type0_0;
	// System.Type UnityEngine.RequireComponent::m_Type1
	Type_t * ___m_Type1_1;
	// System.Type UnityEngine.RequireComponent::m_Type2
	Type_t * ___m_Type2_2;

public:
	inline static int32_t get_offset_of_m_Type0_0() { return static_cast<int32_t>(offsetof(RequireComponent_t597737523, ___m_Type0_0)); }
	inline Type_t * get_m_Type0_0() const { return ___m_Type0_0; }
	inline Type_t ** get_address_of_m_Type0_0() { return &___m_Type0_0; }
	inline void set_m_Type0_0(Type_t * value)
	{
		___m_Type0_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Type0_0), value);
	}

	inline static int32_t get_offset_of_m_Type1_1() { return static_cast<int32_t>(offsetof(RequireComponent_t597737523, ___m_Type1_1)); }
	inline Type_t * get_m_Type1_1() const { return ___m_Type1_1; }
	inline Type_t ** get_address_of_m_Type1_1() { return &___m_Type1_1; }
	inline void set_m_Type1_1(Type_t * value)
	{
		___m_Type1_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Type1_1), value);
	}

	inline static int32_t get_offset_of_m_Type2_2() { return static_cast<int32_t>(offsetof(RequireComponent_t597737523, ___m_Type2_2)); }
	inline Type_t * get_m_Type2_2() const { return ___m_Type2_2; }
	inline Type_t ** get_address_of_m_Type2_2() { return &___m_Type2_2; }
	inline void set_m_Type2_2(Type_t * value)
	{
		___m_Type2_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Type2_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REQUIRECOMPONENT_T597737523_H
#ifndef INVOKABLECALL_1_T3044221012_H
#define INVOKABLECALL_1_T3044221012_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.InvokableCall`1<System.String>
struct  InvokableCall_1_t3044221012  : public BaseInvokableCall_t2920707967
{
public:
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1::Delegate
	UnityAction_1_t323095004 * ___Delegate_0;

public:
	inline static int32_t get_offset_of_Delegate_0() { return static_cast<int32_t>(offsetof(InvokableCall_1_t3044221012, ___Delegate_0)); }
	inline UnityAction_1_t323095004 * get_Delegate_0() const { return ___Delegate_0; }
	inline UnityAction_1_t323095004 ** get_address_of_Delegate_0() { return &___Delegate_0; }
	inline void set_Delegate_0(UnityAction_1_t323095004 * value)
	{
		___Delegate_0 = value;
		Il2CppCodeGenWriteBarrier((&___Delegate_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVOKABLECALL_1_T3044221012_H
#ifndef INVOKABLECALL_1_T2631305575_H
#define INVOKABLECALL_1_T2631305575_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.InvokableCall`1<System.Int32>
struct  InvokableCall_1_t2631305575  : public BaseInvokableCall_t2920707967
{
public:
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1::Delegate
	UnityAction_1_t4205146863 * ___Delegate_0;

public:
	inline static int32_t get_offset_of_Delegate_0() { return static_cast<int32_t>(offsetof(InvokableCall_1_t2631305575, ___Delegate_0)); }
	inline UnityAction_1_t4205146863 * get_Delegate_0() const { return ___Delegate_0; }
	inline UnityAction_1_t4205146863 ** get_address_of_Delegate_0() { return &___Delegate_0; }
	inline void set_Delegate_0(UnityAction_1_t4205146863 * value)
	{
		___Delegate_0 = value;
		Il2CppCodeGenWriteBarrier((&___Delegate_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVOKABLECALL_1_T2631305575_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef UINT64_T2265270229_H
#define UINT64_T2265270229_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UInt64
struct  UInt64_t2265270229 
{
public:
	// System.UInt64 System.UInt64::m_value
	uint64_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(UInt64_t2265270229, ___m_value_0)); }
	inline uint64_t get_m_value_0() const { return ___m_value_0; }
	inline uint64_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint64_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINT64_T2265270229_H
#ifndef INVOKABLECALL_1_T557036258_H
#define INVOKABLECALL_1_T557036258_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.InvokableCall`1<System.Single>
struct  InvokableCall_1_t557036258  : public BaseInvokableCall_t2920707967
{
public:
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1::Delegate
	UnityAction_1_t2130877546 * ___Delegate_0;

public:
	inline static int32_t get_offset_of_Delegate_0() { return static_cast<int32_t>(offsetof(InvokableCall_1_t557036258, ___Delegate_0)); }
	inline UnityAction_1_t2130877546 * get_Delegate_0() const { return ___Delegate_0; }
	inline UnityAction_1_t2130877546 ** get_address_of_Delegate_0() { return &___Delegate_0; }
	inline void set_Delegate_0(UnityAction_1_t2130877546 * value)
	{
		___Delegate_0 = value;
		Il2CppCodeGenWriteBarrier((&___Delegate_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVOKABLECALL_1_T557036258_H
#ifndef BOOLEAN_T2059672899_H
#define BOOLEAN_T2059672899_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t2059672899 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t2059672899, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t2059672899_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t2059672899_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t2059672899_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T2059672899_H
#ifndef MATRIX4X4_T3534180298_H
#define MATRIX4X4_T3534180298_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Matrix4x4
struct  Matrix4x4_t3534180298 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_t3534180298, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_t3534180298, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_t3534180298, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_t3534180298, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_t3534180298, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_t3534180298, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_t3534180298, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_t3534180298, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_t3534180298, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_t3534180298, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_t3534180298, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_t3534180298, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_t3534180298, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_t3534180298, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_t3534180298, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_t3534180298, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_t3534180298_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_t3534180298  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_t3534180298  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_t3534180298_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_t3534180298  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_t3534180298 * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_t3534180298  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_t3534180298_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_t3534180298  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_t3534180298 * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_t3534180298  value)
	{
		___identityMatrix_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIX4X4_T3534180298_H
#ifndef KEYFRAME_T3396755990_H
#define KEYFRAME_T3396755990_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Keyframe
struct  Keyframe_t3396755990 
{
public:
	// System.Single UnityEngine.Keyframe::m_Time
	float ___m_Time_0;
	// System.Single UnityEngine.Keyframe::m_Value
	float ___m_Value_1;
	// System.Single UnityEngine.Keyframe::m_InTangent
	float ___m_InTangent_2;
	// System.Single UnityEngine.Keyframe::m_OutTangent
	float ___m_OutTangent_3;

public:
	inline static int32_t get_offset_of_m_Time_0() { return static_cast<int32_t>(offsetof(Keyframe_t3396755990, ___m_Time_0)); }
	inline float get_m_Time_0() const { return ___m_Time_0; }
	inline float* get_address_of_m_Time_0() { return &___m_Time_0; }
	inline void set_m_Time_0(float value)
	{
		___m_Time_0 = value;
	}

	inline static int32_t get_offset_of_m_Value_1() { return static_cast<int32_t>(offsetof(Keyframe_t3396755990, ___m_Value_1)); }
	inline float get_m_Value_1() const { return ___m_Value_1; }
	inline float* get_address_of_m_Value_1() { return &___m_Value_1; }
	inline void set_m_Value_1(float value)
	{
		___m_Value_1 = value;
	}

	inline static int32_t get_offset_of_m_InTangent_2() { return static_cast<int32_t>(offsetof(Keyframe_t3396755990, ___m_InTangent_2)); }
	inline float get_m_InTangent_2() const { return ___m_InTangent_2; }
	inline float* get_address_of_m_InTangent_2() { return &___m_InTangent_2; }
	inline void set_m_InTangent_2(float value)
	{
		___m_InTangent_2 = value;
	}

	inline static int32_t get_offset_of_m_OutTangent_3() { return static_cast<int32_t>(offsetof(Keyframe_t3396755990, ___m_OutTangent_3)); }
	inline float get_m_OutTangent_3() const { return ___m_OutTangent_3; }
	inline float* get_address_of_m_OutTangent_3() { return &___m_OutTangent_3; }
	inline void set_m_OutTangent_3(float value)
	{
		___m_OutTangent_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYFRAME_T3396755990_H
#ifndef SYSTEMEXCEPTION_T3635077273_H
#define SYSTEMEXCEPTION_T3635077273_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t3635077273  : public Exception_t3361176243
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T3635077273_H
#ifndef UINT32_T2739056616_H
#define UINT32_T2739056616_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UInt32
struct  UInt32_t2739056616 
{
public:
	// System.UInt32 System.UInt32::m_value
	uint32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(UInt32_t2739056616, ___m_value_0)); }
	inline uint32_t get_m_value_0() const { return ___m_value_0; }
	inline uint32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINT32_T2739056616_H
#ifndef BYTE_T880488320_H
#define BYTE_T880488320_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Byte
struct  Byte_t880488320 
{
public:
	// System.Byte System.Byte::m_value
	uint8_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Byte_t880488320, ___m_value_2)); }
	inline uint8_t get_m_value_2() const { return ___m_value_2; }
	inline uint8_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(uint8_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BYTE_T880488320_H
#ifndef TIMESPAN_T2986675223_H
#define TIMESPAN_T2986675223_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TimeSpan
struct  TimeSpan_t2986675223 
{
public:
	// System.Int64 System.TimeSpan::_ticks
	int64_t ____ticks_3;

public:
	inline static int32_t get_offset_of__ticks_3() { return static_cast<int32_t>(offsetof(TimeSpan_t2986675223, ____ticks_3)); }
	inline int64_t get__ticks_3() const { return ____ticks_3; }
	inline int64_t* get_address_of__ticks_3() { return &____ticks_3; }
	inline void set__ticks_3(int64_t value)
	{
		____ticks_3 = value;
	}
};

struct TimeSpan_t2986675223_StaticFields
{
public:
	// System.TimeSpan System.TimeSpan::MaxValue
	TimeSpan_t2986675223  ___MaxValue_0;
	// System.TimeSpan System.TimeSpan::MinValue
	TimeSpan_t2986675223  ___MinValue_1;
	// System.TimeSpan System.TimeSpan::Zero
	TimeSpan_t2986675223  ___Zero_2;

public:
	inline static int32_t get_offset_of_MaxValue_0() { return static_cast<int32_t>(offsetof(TimeSpan_t2986675223_StaticFields, ___MaxValue_0)); }
	inline TimeSpan_t2986675223  get_MaxValue_0() const { return ___MaxValue_0; }
	inline TimeSpan_t2986675223 * get_address_of_MaxValue_0() { return &___MaxValue_0; }
	inline void set_MaxValue_0(TimeSpan_t2986675223  value)
	{
		___MaxValue_0 = value;
	}

	inline static int32_t get_offset_of_MinValue_1() { return static_cast<int32_t>(offsetof(TimeSpan_t2986675223_StaticFields, ___MinValue_1)); }
	inline TimeSpan_t2986675223  get_MinValue_1() const { return ___MinValue_1; }
	inline TimeSpan_t2986675223 * get_address_of_MinValue_1() { return &___MinValue_1; }
	inline void set_MinValue_1(TimeSpan_t2986675223  value)
	{
		___MinValue_1 = value;
	}

	inline static int32_t get_offset_of_Zero_2() { return static_cast<int32_t>(offsetof(TimeSpan_t2986675223_StaticFields, ___Zero_2)); }
	inline TimeSpan_t2986675223  get_Zero_2() const { return ___Zero_2; }
	inline TimeSpan_t2986675223 * get_address_of_Zero_2() { return &___Zero_2; }
	inline void set_Zero_2(TimeSpan_t2986675223  value)
	{
		___Zero_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESPAN_T2986675223_H
#ifndef INT32_T2571135199_H
#define INT32_T2571135199_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2571135199 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t2571135199, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2571135199_H
#ifndef ENUMERATOR_T2325441285_H
#define ENUMERATOR_T2325441285_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<UnityEngine.Events.PersistentCall>
struct  Enumerator_t2325441285 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t565626228 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	PersistentCall_t2687057912 * ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_t2325441285, ___l_0)); }
	inline List_1_t565626228 * get_l_0() const { return ___l_0; }
	inline List_1_t565626228 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t565626228 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t2325441285, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t2325441285, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t2325441285, ___current_3)); }
	inline PersistentCall_t2687057912 * get_current_3() const { return ___current_3; }
	inline PersistentCall_t2687057912 ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(PersistentCall_t2687057912 * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T2325441285_H
#ifndef ENUMERATOR_T2934400376_H
#define ENUMERATOR_T2934400376_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<System.Object>
struct  Enumerator_t2934400376 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t1174585319 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	RuntimeObject * ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_t2934400376, ___l_0)); }
	inline List_1_t1174585319 * get_l_0() const { return ___l_0; }
	inline List_1_t1174585319 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t1174585319 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t2934400376, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t2934400376, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t2934400376, ___current_3)); }
	inline RuntimeObject * get_current_3() const { return ___current_3; }
	inline RuntimeObject ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(RuntimeObject * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T2934400376_H
#ifndef UNITYEVENT_T1464431504_H
#define UNITYEVENT_T1464431504_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent
struct  UnityEvent_t1464431504  : public UnityEventBase_t3171537072
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent::m_InvokeArray
	ObjectU5BU5D_t4290686858* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_t1464431504, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t4290686858* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t4290686858** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t4290686858* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_T1464431504_H
#ifndef VOID_T1078098495_H
#define VOID_T1078098495_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1078098495 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1078098495_H
#ifndef PARAMETERMODIFIER_T969812002_H
#define PARAMETERMODIFIER_T969812002_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.ParameterModifier
struct  ParameterModifier_t969812002 
{
public:
	// System.Boolean[] System.Reflection.ParameterModifier::_byref
	BooleanU5BU5D_t129461842* ____byref_0;

public:
	inline static int32_t get_offset_of__byref_0() { return static_cast<int32_t>(offsetof(ParameterModifier_t969812002, ____byref_0)); }
	inline BooleanU5BU5D_t129461842* get__byref_0() const { return ____byref_0; }
	inline BooleanU5BU5D_t129461842** get_address_of__byref_0() { return &____byref_0; }
	inline void set__byref_0(BooleanU5BU5D_t129461842* value)
	{
		____byref_0 = value;
		Il2CppCodeGenWriteBarrier((&____byref_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Reflection.ParameterModifier
struct ParameterModifier_t969812002_marshaled_pinvoke
{
	int32_t* ____byref_0;
};
// Native definition for COM marshalling of System.Reflection.ParameterModifier
struct ParameterModifier_t969812002_marshaled_com
{
	int32_t* ____byref_0;
};
#endif // PARAMETERMODIFIER_T969812002_H
#ifndef METHODBASE_T674153939_H
#define METHODBASE_T674153939_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MethodBase
struct  MethodBase_t674153939  : public MemberInfo_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // METHODBASE_T674153939_H
#ifndef ADDCOMPONENTMENU_T3529181215_H
#define ADDCOMPONENTMENU_T3529181215_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AddComponentMenu
struct  AddComponentMenu_t3529181215  : public Attribute_t1659210826
{
public:
	// System.String UnityEngine.AddComponentMenu::m_AddComponentMenu
	String_t* ___m_AddComponentMenu_0;
	// System.Int32 UnityEngine.AddComponentMenu::m_Ordering
	int32_t ___m_Ordering_1;

public:
	inline static int32_t get_offset_of_m_AddComponentMenu_0() { return static_cast<int32_t>(offsetof(AddComponentMenu_t3529181215, ___m_AddComponentMenu_0)); }
	inline String_t* get_m_AddComponentMenu_0() const { return ___m_AddComponentMenu_0; }
	inline String_t** get_address_of_m_AddComponentMenu_0() { return &___m_AddComponentMenu_0; }
	inline void set_m_AddComponentMenu_0(String_t* value)
	{
		___m_AddComponentMenu_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_AddComponentMenu_0), value);
	}

	inline static int32_t get_offset_of_m_Ordering_1() { return static_cast<int32_t>(offsetof(AddComponentMenu_t3529181215, ___m_Ordering_1)); }
	inline int32_t get_m_Ordering_1() const { return ___m_Ordering_1; }
	inline int32_t* get_address_of_m_Ordering_1() { return &___m_Ordering_1; }
	inline void set_m_Ordering_1(int32_t value)
	{
		___m_Ordering_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADDCOMPONENTMENU_T3529181215_H
#ifndef INT64_T384086013_H
#define INT64_T384086013_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int64
struct  Int64_t384086013 
{
public:
	// System.Int64 System.Int64::m_value
	int64_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int64_t384086013, ___m_value_0)); }
	inline int64_t get_m_value_0() const { return ___m_value_0; }
	inline int64_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int64_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT64_T384086013_H
#ifndef DEFAULTVALUEATTRIBUTE_T2693326634_H
#define DEFAULTVALUEATTRIBUTE_T2693326634_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Internal.DefaultValueAttribute
struct  DefaultValueAttribute_t2693326634  : public Attribute_t1659210826
{
public:
	// System.Object UnityEngine.Internal.DefaultValueAttribute::DefaultValue
	RuntimeObject * ___DefaultValue_0;

public:
	inline static int32_t get_offset_of_DefaultValue_0() { return static_cast<int32_t>(offsetof(DefaultValueAttribute_t2693326634, ___DefaultValue_0)); }
	inline RuntimeObject * get_DefaultValue_0() const { return ___DefaultValue_0; }
	inline RuntimeObject ** get_address_of_DefaultValue_0() { return &___DefaultValue_0; }
	inline void set_DefaultValue_0(RuntimeObject * value)
	{
		___DefaultValue_0 = value;
		Il2CppCodeGenWriteBarrier((&___DefaultValue_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTVALUEATTRIBUTE_T2693326634_H
#ifndef MATHF_T1822574071_H
#define MATHF_T1822574071_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Mathf
struct  Mathf_t1822574071 
{
public:

public:
};

struct Mathf_t1822574071_StaticFields
{
public:
	// System.Single UnityEngine.Mathf::Epsilon
	float ___Epsilon_0;

public:
	inline static int32_t get_offset_of_Epsilon_0() { return static_cast<int32_t>(offsetof(Mathf_t1822574071_StaticFields, ___Epsilon_0)); }
	inline float get_Epsilon_0() const { return ___Epsilon_0; }
	inline float* get_address_of_Epsilon_0() { return &___Epsilon_0; }
	inline void set_Epsilon_0(float value)
	{
		___Epsilon_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATHF_T1822574071_H
#ifndef GCHANDLE_T4215464244_H
#define GCHANDLE_T4215464244_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.GCHandle
struct  GCHandle_t4215464244 
{
public:
	// System.Int32 System.Runtime.InteropServices.GCHandle::handle
	int32_t ___handle_0;

public:
	inline static int32_t get_offset_of_handle_0() { return static_cast<int32_t>(offsetof(GCHandle_t4215464244, ___handle_0)); }
	inline int32_t get_handle_0() const { return ___handle_0; }
	inline int32_t* get_address_of_handle_0() { return &___handle_0; }
	inline void set_handle_0(int32_t value)
	{
		___handle_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GCHANDLE_T4215464244_H
#ifndef ASSEMBLYISEDITORASSEMBLY_T958181164_H
#define ASSEMBLYISEDITORASSEMBLY_T958181164_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AssemblyIsEditorAssembly
struct  AssemblyIsEditorAssembly_t958181164  : public Attribute_t1659210826
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSEMBLYISEDITORASSEMBLY_T958181164_H
#ifndef GUID_T_H
#define GUID_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Guid
struct  Guid_t 
{
public:
	// System.Int32 System.Guid::_a
	int32_t ____a_0;
	// System.Int16 System.Guid::_b
	int16_t ____b_1;
	// System.Int16 System.Guid::_c
	int16_t ____c_2;
	// System.Byte System.Guid::_d
	uint8_t ____d_3;
	// System.Byte System.Guid::_e
	uint8_t ____e_4;
	// System.Byte System.Guid::_f
	uint8_t ____f_5;
	// System.Byte System.Guid::_g
	uint8_t ____g_6;
	// System.Byte System.Guid::_h
	uint8_t ____h_7;
	// System.Byte System.Guid::_i
	uint8_t ____i_8;
	// System.Byte System.Guid::_j
	uint8_t ____j_9;
	// System.Byte System.Guid::_k
	uint8_t ____k_10;

public:
	inline static int32_t get_offset_of__a_0() { return static_cast<int32_t>(offsetof(Guid_t, ____a_0)); }
	inline int32_t get__a_0() const { return ____a_0; }
	inline int32_t* get_address_of__a_0() { return &____a_0; }
	inline void set__a_0(int32_t value)
	{
		____a_0 = value;
	}

	inline static int32_t get_offset_of__b_1() { return static_cast<int32_t>(offsetof(Guid_t, ____b_1)); }
	inline int16_t get__b_1() const { return ____b_1; }
	inline int16_t* get_address_of__b_1() { return &____b_1; }
	inline void set__b_1(int16_t value)
	{
		____b_1 = value;
	}

	inline static int32_t get_offset_of__c_2() { return static_cast<int32_t>(offsetof(Guid_t, ____c_2)); }
	inline int16_t get__c_2() const { return ____c_2; }
	inline int16_t* get_address_of__c_2() { return &____c_2; }
	inline void set__c_2(int16_t value)
	{
		____c_2 = value;
	}

	inline static int32_t get_offset_of__d_3() { return static_cast<int32_t>(offsetof(Guid_t, ____d_3)); }
	inline uint8_t get__d_3() const { return ____d_3; }
	inline uint8_t* get_address_of__d_3() { return &____d_3; }
	inline void set__d_3(uint8_t value)
	{
		____d_3 = value;
	}

	inline static int32_t get_offset_of__e_4() { return static_cast<int32_t>(offsetof(Guid_t, ____e_4)); }
	inline uint8_t get__e_4() const { return ____e_4; }
	inline uint8_t* get_address_of__e_4() { return &____e_4; }
	inline void set__e_4(uint8_t value)
	{
		____e_4 = value;
	}

	inline static int32_t get_offset_of__f_5() { return static_cast<int32_t>(offsetof(Guid_t, ____f_5)); }
	inline uint8_t get__f_5() const { return ____f_5; }
	inline uint8_t* get_address_of__f_5() { return &____f_5; }
	inline void set__f_5(uint8_t value)
	{
		____f_5 = value;
	}

	inline static int32_t get_offset_of__g_6() { return static_cast<int32_t>(offsetof(Guid_t, ____g_6)); }
	inline uint8_t get__g_6() const { return ____g_6; }
	inline uint8_t* get_address_of__g_6() { return &____g_6; }
	inline void set__g_6(uint8_t value)
	{
		____g_6 = value;
	}

	inline static int32_t get_offset_of__h_7() { return static_cast<int32_t>(offsetof(Guid_t, ____h_7)); }
	inline uint8_t get__h_7() const { return ____h_7; }
	inline uint8_t* get_address_of__h_7() { return &____h_7; }
	inline void set__h_7(uint8_t value)
	{
		____h_7 = value;
	}

	inline static int32_t get_offset_of__i_8() { return static_cast<int32_t>(offsetof(Guid_t, ____i_8)); }
	inline uint8_t get__i_8() const { return ____i_8; }
	inline uint8_t* get_address_of__i_8() { return &____i_8; }
	inline void set__i_8(uint8_t value)
	{
		____i_8 = value;
	}

	inline static int32_t get_offset_of__j_9() { return static_cast<int32_t>(offsetof(Guid_t, ____j_9)); }
	inline uint8_t get__j_9() const { return ____j_9; }
	inline uint8_t* get_address_of__j_9() { return &____j_9; }
	inline void set__j_9(uint8_t value)
	{
		____j_9 = value;
	}

	inline static int32_t get_offset_of__k_10() { return static_cast<int32_t>(offsetof(Guid_t, ____k_10)); }
	inline uint8_t get__k_10() const { return ____k_10; }
	inline uint8_t* get_address_of__k_10() { return &____k_10; }
	inline void set__k_10(uint8_t value)
	{
		____k_10 = value;
	}
};

struct Guid_t_StaticFields
{
public:
	// System.Guid System.Guid::Empty
	Guid_t  ___Empty_11;
	// System.Object System.Guid::_rngAccess
	RuntimeObject * ____rngAccess_12;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_rng
	RandomNumberGenerator_t4107616314 * ____rng_13;

public:
	inline static int32_t get_offset_of_Empty_11() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ___Empty_11)); }
	inline Guid_t  get_Empty_11() const { return ___Empty_11; }
	inline Guid_t * get_address_of_Empty_11() { return &___Empty_11; }
	inline void set_Empty_11(Guid_t  value)
	{
		___Empty_11 = value;
	}

	inline static int32_t get_offset_of__rngAccess_12() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rngAccess_12)); }
	inline RuntimeObject * get__rngAccess_12() const { return ____rngAccess_12; }
	inline RuntimeObject ** get_address_of__rngAccess_12() { return &____rngAccess_12; }
	inline void set__rngAccess_12(RuntimeObject * value)
	{
		____rngAccess_12 = value;
		Il2CppCodeGenWriteBarrier((&____rngAccess_12), value);
	}

	inline static int32_t get_offset_of__rng_13() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rng_13)); }
	inline RandomNumberGenerator_t4107616314 * get__rng_13() const { return ____rng_13; }
	inline RandomNumberGenerator_t4107616314 ** get_address_of__rng_13() { return &____rng_13; }
	inline void set__rng_13(RandomNumberGenerator_t4107616314 * value)
	{
		____rng_13 = value;
		Il2CppCodeGenWriteBarrier((&____rng_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUID_T_H
#ifndef DEFAULTEXECUTIONORDER_T3862449190_H
#define DEFAULTEXECUTIONORDER_T3862449190_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.DefaultExecutionOrder
struct  DefaultExecutionOrder_t3862449190  : public Attribute_t1659210826
{
public:
	// System.Int32 UnityEngine.DefaultExecutionOrder::<order>k__BackingField
	int32_t ___U3CorderU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CorderU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(DefaultExecutionOrder_t3862449190, ___U3CorderU3Ek__BackingField_0)); }
	inline int32_t get_U3CorderU3Ek__BackingField_0() const { return ___U3CorderU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CorderU3Ek__BackingField_0() { return &___U3CorderU3Ek__BackingField_0; }
	inline void set_U3CorderU3Ek__BackingField_0(int32_t value)
	{
		___U3CorderU3Ek__BackingField_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTEXECUTIONORDER_T3862449190_H
#ifndef DISALLOWMULTIPLECOMPONENT_T2957315911_H
#define DISALLOWMULTIPLECOMPONENT_T2957315911_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.DisallowMultipleComponent
struct  DisallowMultipleComponent_t2957315911  : public Attribute_t1659210826
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISALLOWMULTIPLECOMPONENT_T2957315911_H
#ifndef EXECUTEINEDITMODE_T1836144016_H
#define EXECUTEINEDITMODE_T1836144016_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ExecuteInEditMode
struct  ExecuteInEditMode_t1836144016  : public Attribute_t1659210826
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXECUTEINEDITMODE_T1836144016_H
#ifndef PREFERBINARYSERIALIZATION_T2730910840_H
#define PREFERBINARYSERIALIZATION_T2730910840_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PreferBinarySerialization
struct  PreferBinarySerialization_t2730910840  : public Attribute_t1659210826
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PREFERBINARYSERIALIZATION_T2730910840_H
#ifndef UNMARSHALLEDATTRIBUTE_T2695814082_H
#define UNMARSHALLEDATTRIBUTE_T2695814082_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Bindings.UnmarshalledAttribute
struct  UnmarshalledAttribute_t2695814082  : public Attribute_t1659210826
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNMARSHALLEDATTRIBUTE_T2695814082_H
#ifndef SINGLE_T496865882_H
#define SINGLE_T496865882_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t496865882 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t496865882, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T496865882_H
#ifndef RECT_T1111852907_H
#define RECT_T1111852907_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rect
struct  Rect_t1111852907 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t1111852907, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t1111852907, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t1111852907, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t1111852907, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T1111852907_H
#ifndef DOUBLE_T1454265465_H
#define DOUBLE_T1454265465_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Double
struct  Double_t1454265465 
{
public:
	// System.Double System.Double::m_value
	double ___m_value_13;

public:
	inline static int32_t get_offset_of_m_value_13() { return static_cast<int32_t>(offsetof(Double_t1454265465, ___m_value_13)); }
	inline double get_m_value_13() const { return ___m_value_13; }
	inline double* get_address_of_m_value_13() { return &___m_value_13; }
	inline void set_m_value_13(double value)
	{
		___m_value_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOUBLE_T1454265465_H
#ifndef VECTOR3_T3239555143_H
#define VECTOR3_T3239555143_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3239555143 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_0;
	// System.Single UnityEngine.Vector3::y
	float ___y_1;
	// System.Single UnityEngine.Vector3::z
	float ___z_2;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector3_t3239555143, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector3_t3239555143, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Vector3_t3239555143, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}
};

struct Vector3_t3239555143_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3239555143  ___zeroVector_3;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3239555143  ___oneVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3239555143  ___upVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3239555143  ___downVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3239555143  ___leftVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3239555143  ___rightVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3239555143  ___forwardVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3239555143  ___backVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3239555143  ___positiveInfinityVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3239555143  ___negativeInfinityVector_12;

public:
	inline static int32_t get_offset_of_zeroVector_3() { return static_cast<int32_t>(offsetof(Vector3_t3239555143_StaticFields, ___zeroVector_3)); }
	inline Vector3_t3239555143  get_zeroVector_3() const { return ___zeroVector_3; }
	inline Vector3_t3239555143 * get_address_of_zeroVector_3() { return &___zeroVector_3; }
	inline void set_zeroVector_3(Vector3_t3239555143  value)
	{
		___zeroVector_3 = value;
	}

	inline static int32_t get_offset_of_oneVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3239555143_StaticFields, ___oneVector_4)); }
	inline Vector3_t3239555143  get_oneVector_4() const { return ___oneVector_4; }
	inline Vector3_t3239555143 * get_address_of_oneVector_4() { return &___oneVector_4; }
	inline void set_oneVector_4(Vector3_t3239555143  value)
	{
		___oneVector_4 = value;
	}

	inline static int32_t get_offset_of_upVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3239555143_StaticFields, ___upVector_5)); }
	inline Vector3_t3239555143  get_upVector_5() const { return ___upVector_5; }
	inline Vector3_t3239555143 * get_address_of_upVector_5() { return &___upVector_5; }
	inline void set_upVector_5(Vector3_t3239555143  value)
	{
		___upVector_5 = value;
	}

	inline static int32_t get_offset_of_downVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3239555143_StaticFields, ___downVector_6)); }
	inline Vector3_t3239555143  get_downVector_6() const { return ___downVector_6; }
	inline Vector3_t3239555143 * get_address_of_downVector_6() { return &___downVector_6; }
	inline void set_downVector_6(Vector3_t3239555143  value)
	{
		___downVector_6 = value;
	}

	inline static int32_t get_offset_of_leftVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3239555143_StaticFields, ___leftVector_7)); }
	inline Vector3_t3239555143  get_leftVector_7() const { return ___leftVector_7; }
	inline Vector3_t3239555143 * get_address_of_leftVector_7() { return &___leftVector_7; }
	inline void set_leftVector_7(Vector3_t3239555143  value)
	{
		___leftVector_7 = value;
	}

	inline static int32_t get_offset_of_rightVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3239555143_StaticFields, ___rightVector_8)); }
	inline Vector3_t3239555143  get_rightVector_8() const { return ___rightVector_8; }
	inline Vector3_t3239555143 * get_address_of_rightVector_8() { return &___rightVector_8; }
	inline void set_rightVector_8(Vector3_t3239555143  value)
	{
		___rightVector_8 = value;
	}

	inline static int32_t get_offset_of_forwardVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3239555143_StaticFields, ___forwardVector_9)); }
	inline Vector3_t3239555143  get_forwardVector_9() const { return ___forwardVector_9; }
	inline Vector3_t3239555143 * get_address_of_forwardVector_9() { return &___forwardVector_9; }
	inline void set_forwardVector_9(Vector3_t3239555143  value)
	{
		___forwardVector_9 = value;
	}

	inline static int32_t get_offset_of_backVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3239555143_StaticFields, ___backVector_10)); }
	inline Vector3_t3239555143  get_backVector_10() const { return ___backVector_10; }
	inline Vector3_t3239555143 * get_address_of_backVector_10() { return &___backVector_10; }
	inline void set_backVector_10(Vector3_t3239555143  value)
	{
		___backVector_10 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3239555143_StaticFields, ___positiveInfinityVector_11)); }
	inline Vector3_t3239555143  get_positiveInfinityVector_11() const { return ___positiveInfinityVector_11; }
	inline Vector3_t3239555143 * get_address_of_positiveInfinityVector_11() { return &___positiveInfinityVector_11; }
	inline void set_positiveInfinityVector_11(Vector3_t3239555143  value)
	{
		___positiveInfinityVector_11 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3239555143_StaticFields, ___negativeInfinityVector_12)); }
	inline Vector3_t3239555143  get_negativeInfinityVector_12() const { return ___negativeInfinityVector_12; }
	inline Vector3_t3239555143 * get_address_of_negativeInfinityVector_12() { return &___negativeInfinityVector_12; }
	inline void set_negativeInfinityVector_12(Vector3_t3239555143  value)
	{
		___negativeInfinityVector_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3239555143_H
#ifndef UNITYEVENT_1_T1465180672_H
#define UNITYEVENT_1_T1465180672_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<UnityEngine.Networking.PlayerConnection.MessageEventArgs>
struct  UnityEvent_1_t1465180672  : public UnityEventBase_t3171537072
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t4290686858* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t1465180672, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t4290686858* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t4290686858** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t4290686858* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T1465180672_H
#ifndef CULLINGGROUPEVENT_T157813866_H
#define CULLINGGROUPEVENT_T157813866_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CullingGroupEvent
struct  CullingGroupEvent_t157813866 
{
public:
	// System.Int32 UnityEngine.CullingGroupEvent::m_Index
	int32_t ___m_Index_0;
	// System.Byte UnityEngine.CullingGroupEvent::m_PrevState
	uint8_t ___m_PrevState_1;
	// System.Byte UnityEngine.CullingGroupEvent::m_ThisState
	uint8_t ___m_ThisState_2;

public:
	inline static int32_t get_offset_of_m_Index_0() { return static_cast<int32_t>(offsetof(CullingGroupEvent_t157813866, ___m_Index_0)); }
	inline int32_t get_m_Index_0() const { return ___m_Index_0; }
	inline int32_t* get_address_of_m_Index_0() { return &___m_Index_0; }
	inline void set_m_Index_0(int32_t value)
	{
		___m_Index_0 = value;
	}

	inline static int32_t get_offset_of_m_PrevState_1() { return static_cast<int32_t>(offsetof(CullingGroupEvent_t157813866, ___m_PrevState_1)); }
	inline uint8_t get_m_PrevState_1() const { return ___m_PrevState_1; }
	inline uint8_t* get_address_of_m_PrevState_1() { return &___m_PrevState_1; }
	inline void set_m_PrevState_1(uint8_t value)
	{
		___m_PrevState_1 = value;
	}

	inline static int32_t get_offset_of_m_ThisState_2() { return static_cast<int32_t>(offsetof(CullingGroupEvent_t157813866, ___m_ThisState_2)); }
	inline uint8_t get_m_ThisState_2() const { return ___m_ThisState_2; }
	inline uint8_t* get_address_of_m_ThisState_2() { return &___m_ThisState_2; }
	inline void set_m_ThisState_2(uint8_t value)
	{
		___m_ThisState_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CULLINGGROUPEVENT_T157813866_H
#ifndef DEALLOCATEONJOBCOMPLETIONATTRIBUTE_T2226375310_H
#define DEALLOCATEONJOBCOMPLETIONATTRIBUTE_T2226375310_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Collections.DeallocateOnJobCompletionAttribute
struct  DeallocateOnJobCompletionAttribute_t2226375310  : public Attribute_t1659210826
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEALLOCATEONJOBCOMPLETIONATTRIBUTE_T2226375310_H
#ifndef NATIVECONTAINERATTRIBUTE_T1940066792_H
#define NATIVECONTAINERATTRIBUTE_T1940066792_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Collections.NativeContainerAttribute
struct  NativeContainerAttribute_t1940066792  : public Attribute_t1659210826
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NATIVECONTAINERATTRIBUTE_T1940066792_H
#ifndef NATIVECONTAINERSUPPORTSATOMICWRITEATTRIBUTE_T1199220324_H
#define NATIVECONTAINERSUPPORTSATOMICWRITEATTRIBUTE_T1199220324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Collections.NativeContainerSupportsAtomicWriteAttribute
struct  NativeContainerSupportsAtomicWriteAttribute_t1199220324  : public Attribute_t1659210826
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NATIVECONTAINERSUPPORTSATOMICWRITEATTRIBUTE_T1199220324_H
#ifndef PROPERTYNAME_T666104458_H
#define PROPERTYNAME_T666104458_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PropertyName
struct  PropertyName_t666104458 
{
public:
	// System.Int32 UnityEngine.PropertyName::id
	int32_t ___id_0;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(PropertyName_t666104458, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYNAME_T666104458_H
#ifndef NATIVECONTAINERSUPPORTSMINMAXWRITERESTRICTIONATTRIBUTE_T3950250497_H
#define NATIVECONTAINERSUPPORTSMINMAXWRITERESTRICTIONATTRIBUTE_T3950250497_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Collections.NativeContainerSupportsMinMaxWriteRestrictionAttribute
struct  NativeContainerSupportsMinMaxWriteRestrictionAttribute_t3950250497  : public Attribute_t1659210826
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NATIVECONTAINERSUPPORTSMINMAXWRITERESTRICTIONATTRIBUTE_T3950250497_H
#ifndef UNITYEVENT_1_T3088907843_H
#define UNITYEVENT_1_T3088907843_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<System.Int32>
struct  UnityEvent_1_t3088907843  : public UnityEventBase_t3171537072
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t4290686858* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t3088907843, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t4290686858* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t4290686858** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t4290686858* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T3088907843_H
#ifndef ENUM_T750633987_H
#define ENUM_T750633987_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t750633987  : public ValueType_t1920584095
{
public:

public:
};

struct Enum_t750633987_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t2864810077* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t750633987_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t2864810077* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t2864810077** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t2864810077* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t750633987_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t750633987_marshaled_com
{
};
#endif // ENUM_T750633987_H
#ifndef CSSSIZE_T2436738465_H
#define CSSSIZE_T2436738465_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CSSLayout.CSSSize
struct  CSSSize_t2436738465 
{
public:
	// System.Single UnityEngine.CSSLayout.CSSSize::width
	float ___width_0;
	// System.Single UnityEngine.CSSLayout.CSSSize::height
	float ___height_1;

public:
	inline static int32_t get_offset_of_width_0() { return static_cast<int32_t>(offsetof(CSSSize_t2436738465, ___width_0)); }
	inline float get_width_0() const { return ___width_0; }
	inline float* get_address_of_width_0() { return &___width_0; }
	inline void set_width_0(float value)
	{
		___width_0 = value;
	}

	inline static int32_t get_offset_of_height_1() { return static_cast<int32_t>(offsetof(CSSSize_t2436738465, ___height_1)); }
	inline float get_height_1() const { return ___height_1; }
	inline float* get_address_of_height_1() { return &___height_1; }
	inline void set_height_1(float value)
	{
		___height_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CSSSIZE_T2436738465_H
#ifndef CONTEXTMENU_T3503312229_H
#define CONTEXTMENU_T3503312229_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ContextMenu
struct  ContextMenu_t3503312229  : public Attribute_t1659210826
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTEXTMENU_T3503312229_H
#ifndef VECTOR4_T4052901136_H
#define VECTOR4_T4052901136_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector4
struct  Vector4_t4052901136 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_0;
	// System.Single UnityEngine.Vector4::y
	float ___y_1;
	// System.Single UnityEngine.Vector4::z
	float ___z_2;
	// System.Single UnityEngine.Vector4::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector4_t4052901136, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector4_t4052901136, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Vector4_t4052901136, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Vector4_t4052901136, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Vector4_t4052901136_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_t4052901136  ___zeroVector_4;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_t4052901136  ___oneVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_t4052901136  ___positiveInfinityVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_t4052901136  ___negativeInfinityVector_7;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector4_t4052901136_StaticFields, ___zeroVector_4)); }
	inline Vector4_t4052901136  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector4_t4052901136 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector4_t4052901136  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector4_t4052901136_StaticFields, ___oneVector_5)); }
	inline Vector4_t4052901136  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector4_t4052901136 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector4_t4052901136  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_6() { return static_cast<int32_t>(offsetof(Vector4_t4052901136_StaticFields, ___positiveInfinityVector_6)); }
	inline Vector4_t4052901136  get_positiveInfinityVector_6() const { return ___positiveInfinityVector_6; }
	inline Vector4_t4052901136 * get_address_of_positiveInfinityVector_6() { return &___positiveInfinityVector_6; }
	inline void set_positiveInfinityVector_6(Vector4_t4052901136  value)
	{
		___positiveInfinityVector_6 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_t4052901136_StaticFields, ___negativeInfinityVector_7)); }
	inline Vector4_t4052901136  get_negativeInfinityVector_7() const { return ___negativeInfinityVector_7; }
	inline Vector4_t4052901136 * get_address_of_negativeInfinityVector_7() { return &___negativeInfinityVector_7; }
	inline void set_negativeInfinityVector_7(Vector4_t4052901136  value)
	{
		___negativeInfinityVector_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4_T4052901136_H
#ifndef WRITEONLYATTRIBUTE_T1011568951_H
#define WRITEONLYATTRIBUTE_T1011568951_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Collections.WriteOnlyAttribute
struct  WriteOnlyAttribute_t1011568951  : public Attribute_t1659210826
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WRITEONLYATTRIBUTE_T1011568951_H
#ifndef COLOR_T4024113822_H
#define COLOR_T4024113822_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t4024113822 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t4024113822, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t4024113822, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t4024113822, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t4024113822, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T4024113822_H
#ifndef READONLYATTRIBUTE_T3185780063_H
#define READONLYATTRIBUTE_T3185780063_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Collections.ReadOnlyAttribute
struct  ReadOnlyAttribute_t3185780063  : public Attribute_t1659210826
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // READONLYATTRIBUTE_T3185780063_H
#ifndef READWRITEATTRIBUTE_T1843161347_H
#define READWRITEATTRIBUTE_T1843161347_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Collections.ReadWriteAttribute
struct  ReadWriteAttribute_t1843161347  : public Attribute_t1659210826
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // READWRITEATTRIBUTE_T1843161347_H
#ifndef ARGUMENTEXCEPTION_T1372035057_H
#define ARGUMENTEXCEPTION_T1372035057_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ArgumentException
struct  ArgumentException_t1372035057  : public SystemException_t3635077273
{
public:
	// System.String System.ArgumentException::param_name
	String_t* ___param_name_12;

public:
	inline static int32_t get_offset_of_param_name_12() { return static_cast<int32_t>(offsetof(ArgumentException_t1372035057, ___param_name_12)); }
	inline String_t* get_param_name_12() const { return ___param_name_12; }
	inline String_t** get_address_of_param_name_12() { return &___param_name_12; }
	inline void set_param_name_12(String_t* value)
	{
		___param_name_12 = value;
		Il2CppCodeGenWriteBarrier((&___param_name_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARGUMENTEXCEPTION_T1372035057_H
#ifndef CONNECTIONCHANGEEVENT_T2308234365_H
#define CONNECTIONCHANGEEVENT_T2308234365_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/ConnectionChangeEvent
struct  ConnectionChangeEvent_t2308234365  : public UnityEvent_1_t3088907843
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONNECTIONCHANGEEVENT_T2308234365_H
#ifndef LOCALNOTIFICATION_T2133447199_H
#define LOCALNOTIFICATION_T2133447199_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.iOS.LocalNotification
struct  LocalNotification_t2133447199  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.iOS.LocalNotification::notificationWrapper
	intptr_t ___notificationWrapper_0;

public:
	inline static int32_t get_offset_of_notificationWrapper_0() { return static_cast<int32_t>(offsetof(LocalNotification_t2133447199, ___notificationWrapper_0)); }
	inline intptr_t get_notificationWrapper_0() const { return ___notificationWrapper_0; }
	inline intptr_t* get_address_of_notificationWrapper_0() { return &___notificationWrapper_0; }
	inline void set_notificationWrapper_0(intptr_t value)
	{
		___notificationWrapper_0 = value;
	}
};

struct LocalNotification_t2133447199_StaticFields
{
public:
	// System.Int64 UnityEngine.iOS.LocalNotification::m_NSReferenceDateTicks
	int64_t ___m_NSReferenceDateTicks_1;

public:
	inline static int32_t get_offset_of_m_NSReferenceDateTicks_1() { return static_cast<int32_t>(offsetof(LocalNotification_t2133447199_StaticFields, ___m_NSReferenceDateTicks_1)); }
	inline int64_t get_m_NSReferenceDateTicks_1() const { return ___m_NSReferenceDateTicks_1; }
	inline int64_t* get_address_of_m_NSReferenceDateTicks_1() { return &___m_NSReferenceDateTicks_1; }
	inline void set_m_NSReferenceDateTicks_1(int64_t value)
	{
		___m_NSReferenceDateTicks_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOCALNOTIFICATION_T2133447199_H
#ifndef DATETIMEKIND_T3511769276_H
#define DATETIMEKIND_T3511769276_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTimeKind
struct  DateTimeKind_t3511769276 
{
public:
	// System.Int32 System.DateTimeKind::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DateTimeKind_t3511769276, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMEKIND_T3511769276_H
#ifndef REMOTENOTIFICATION_T414806740_H
#define REMOTENOTIFICATION_T414806740_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.iOS.RemoteNotification
struct  RemoteNotification_t414806740  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.iOS.RemoteNotification::notificationWrapper
	intptr_t ___notificationWrapper_0;

public:
	inline static int32_t get_offset_of_notificationWrapper_0() { return static_cast<int32_t>(offsetof(RemoteNotification_t414806740, ___notificationWrapper_0)); }
	inline intptr_t get_notificationWrapper_0() const { return ___notificationWrapper_0; }
	inline intptr_t* get_address_of_notificationWrapper_0() { return &___notificationWrapper_0; }
	inline void set_notificationWrapper_0(intptr_t value)
	{
		___notificationWrapper_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REMOTENOTIFICATION_T414806740_H
#ifndef SEEKORIGIN_T2616381620_H
#define SEEKORIGIN_T2616381620_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.SeekOrigin
struct  SeekOrigin_t2616381620 
{
public:
	// System.Int32 System.IO.SeekOrigin::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SeekOrigin_t2616381620, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SEEKORIGIN_T2616381620_H
#ifndef PARAMETERATTRIBUTES_T3383706087_H
#define PARAMETERATTRIBUTES_T3383706087_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.ParameterAttributes
struct  ParameterAttributes_t3383706087 
{
public:
	// System.Int32 System.Reflection.ParameterAttributes::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ParameterAttributes_t3383706087, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARAMETERATTRIBUTES_T3383706087_H
#ifndef PLAYABLEOUTPUTHANDLE_T2719521870_H
#define PLAYABLEOUTPUTHANDLE_T2719521870_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.PlayableOutputHandle
struct  PlayableOutputHandle_t2719521870 
{
public:
	// System.IntPtr UnityEngine.Playables.PlayableOutputHandle::m_Handle
	intptr_t ___m_Handle_0;
	// System.Int32 UnityEngine.Playables.PlayableOutputHandle::m_Version
	int32_t ___m_Version_1;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(PlayableOutputHandle_t2719521870, ___m_Handle_0)); }
	inline intptr_t get_m_Handle_0() const { return ___m_Handle_0; }
	inline intptr_t* get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(intptr_t value)
	{
		___m_Handle_0 = value;
	}

	inline static int32_t get_offset_of_m_Version_1() { return static_cast<int32_t>(offsetof(PlayableOutputHandle_t2719521870, ___m_Version_1)); }
	inline int32_t get_m_Version_1() const { return ___m_Version_1; }
	inline int32_t* get_address_of_m_Version_1() { return &___m_Version_1; }
	inline void set_m_Version_1(int32_t value)
	{
		___m_Version_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYABLEOUTPUTHANDLE_T2719521870_H
#ifndef U3CINVOKEMESSAGEIDSUBSCRIBERSU3EC__ANONSTOREY0_T1928087868_H
#define U3CINVOKEMESSAGEIDSUBSCRIBERSU3EC__ANONSTOREY0_T1928087868_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/<InvokeMessageIdSubscribers>c__AnonStorey0
struct  U3CInvokeMessageIdSubscribersU3Ec__AnonStorey0_t1928087868  : public RuntimeObject
{
public:
	// System.Guid UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/<InvokeMessageIdSubscribers>c__AnonStorey0::messageId
	Guid_t  ___messageId_0;

public:
	inline static int32_t get_offset_of_messageId_0() { return static_cast<int32_t>(offsetof(U3CInvokeMessageIdSubscribersU3Ec__AnonStorey0_t1928087868, ___messageId_0)); }
	inline Guid_t  get_messageId_0() const { return ___messageId_0; }
	inline Guid_t * get_address_of_messageId_0() { return &___messageId_0; }
	inline void set_messageId_0(Guid_t  value)
	{
		___messageId_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CINVOKEMESSAGEIDSUBSCRIBERSU3EC__ANONSTOREY0_T1928087868_H
#ifndef MATHFINTERNAL_T1431629077_H
#define MATHFINTERNAL_T1431629077_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngineInternal.MathfInternal
struct  MathfInternal_t1431629077 
{
public:
	union
	{
		struct
		{
		};
		uint8_t MathfInternal_t1431629077__padding[1];
	};

public:
};

struct MathfInternal_t1431629077_StaticFields
{
public:
	// System.Single modreq(System.Runtime.CompilerServices.IsVolatile) UnityEngineInternal.MathfInternal::FloatMinNormal
	float ___FloatMinNormal_0;
	// System.Single modreq(System.Runtime.CompilerServices.IsVolatile) UnityEngineInternal.MathfInternal::FloatMinDenormal
	float ___FloatMinDenormal_1;
	// System.Boolean UnityEngineInternal.MathfInternal::IsFlushToZeroEnabled
	bool ___IsFlushToZeroEnabled_2;

public:
	inline static int32_t get_offset_of_FloatMinNormal_0() { return static_cast<int32_t>(offsetof(MathfInternal_t1431629077_StaticFields, ___FloatMinNormal_0)); }
	inline float get_FloatMinNormal_0() const { return ___FloatMinNormal_0; }
	inline float* get_address_of_FloatMinNormal_0() { return &___FloatMinNormal_0; }
	inline void set_FloatMinNormal_0(float value)
	{
		___FloatMinNormal_0 = value;
	}

	inline static int32_t get_offset_of_FloatMinDenormal_1() { return static_cast<int32_t>(offsetof(MathfInternal_t1431629077_StaticFields, ___FloatMinDenormal_1)); }
	inline float get_FloatMinDenormal_1() const { return ___FloatMinDenormal_1; }
	inline float* get_address_of_FloatMinDenormal_1() { return &___FloatMinDenormal_1; }
	inline void set_FloatMinDenormal_1(float value)
	{
		___FloatMinDenormal_1 = value;
	}

	inline static int32_t get_offset_of_IsFlushToZeroEnabled_2() { return static_cast<int32_t>(offsetof(MathfInternal_t1431629077_StaticFields, ___IsFlushToZeroEnabled_2)); }
	inline bool get_IsFlushToZeroEnabled_2() const { return ___IsFlushToZeroEnabled_2; }
	inline bool* get_address_of_IsFlushToZeroEnabled_2() { return &___IsFlushToZeroEnabled_2; }
	inline void set_IsFlushToZeroEnabled_2(bool value)
	{
		___IsFlushToZeroEnabled_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATHFINTERNAL_T1431629077_H
#ifndef PLAYABLEGRAPH_T1897216728_H
#define PLAYABLEGRAPH_T1897216728_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.PlayableGraph
struct  PlayableGraph_t1897216728 
{
public:
	// System.IntPtr UnityEngine.Playables.PlayableGraph::m_Handle
	intptr_t ___m_Handle_0;
	// System.Int32 UnityEngine.Playables.PlayableGraph::m_Version
	int32_t ___m_Version_1;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(PlayableGraph_t1897216728, ___m_Handle_0)); }
	inline intptr_t get_m_Handle_0() const { return ___m_Handle_0; }
	inline intptr_t* get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(intptr_t value)
	{
		___m_Handle_0 = value;
	}

	inline static int32_t get_offset_of_m_Version_1() { return static_cast<int32_t>(offsetof(PlayableGraph_t1897216728, ___m_Version_1)); }
	inline int32_t get_m_Version_1() const { return ___m_Version_1; }
	inline int32_t* get_address_of_m_Version_1() { return &___m_Version_1; }
	inline void set_m_Version_1(int32_t value)
	{
		___m_Version_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYABLEGRAPH_T1897216728_H
#ifndef INDEXOUTOFRANGEEXCEPTION_T3041194587_H
#define INDEXOUTOFRANGEEXCEPTION_T3041194587_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IndexOutOfRangeException
struct  IndexOutOfRangeException_t3041194587  : public SystemException_t3635077273
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INDEXOUTOFRANGEEXCEPTION_T3041194587_H
#ifndef PLAYABLEHANDLE_T3938927722_H
#define PLAYABLEHANDLE_T3938927722_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.PlayableHandle
struct  PlayableHandle_t3938927722 
{
public:
	// System.IntPtr UnityEngine.Playables.PlayableHandle::m_Handle
	intptr_t ___m_Handle_0;
	// System.Int32 UnityEngine.Playables.PlayableHandle::m_Version
	int32_t ___m_Version_1;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(PlayableHandle_t3938927722, ___m_Handle_0)); }
	inline intptr_t get_m_Handle_0() const { return ___m_Handle_0; }
	inline intptr_t* get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(intptr_t value)
	{
		___m_Handle_0 = value;
	}

	inline static int32_t get_offset_of_m_Version_1() { return static_cast<int32_t>(offsetof(PlayableHandle_t3938927722, ___m_Version_1)); }
	inline int32_t get_m_Version_1() const { return ___m_Version_1; }
	inline int32_t* get_address_of_m_Version_1() { return &___m_Version_1; }
	inline void set_m_Version_1(int32_t value)
	{
		___m_Version_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYABLEHANDLE_T3938927722_H
#ifndef DATASTREAMTYPE_T1550112629_H
#define DATASTREAMTYPE_T1550112629_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.DataStreamType
struct  DataStreamType_t1550112629 
{
public:
	// System.Int32 UnityEngine.Playables.DataStreamType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DataStreamType_t1550112629, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATASTREAMTYPE_T1550112629_H
#ifndef MESSAGEEVENT_T2016149491_H
#define MESSAGEEVENT_T2016149491_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageEvent
struct  MessageEvent_t2016149491  : public UnityEvent_1_t1465180672
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESSAGEEVENT_T2016149491_H
#ifndef CACHEDINVOKABLECALL_1_T3666972114_H
#define CACHEDINVOKABLECALL_1_T3666972114_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.CachedInvokableCall`1<System.Int32>
struct  CachedInvokableCall_1_t3666972114  : public InvokableCall_1_t2631305575
{
public:
	// T UnityEngine.Events.CachedInvokableCall`1::m_Arg1
	int32_t ___m_Arg1_1;

public:
	inline static int32_t get_offset_of_m_Arg1_1() { return static_cast<int32_t>(offsetof(CachedInvokableCall_1_t3666972114, ___m_Arg1_1)); }
	inline int32_t get_m_Arg1_1() const { return ___m_Arg1_1; }
	inline int32_t* get_address_of_m_Arg1_1() { return &___m_Arg1_1; }
	inline void set_m_Arg1_1(int32_t value)
	{
		___m_Arg1_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CACHEDINVOKABLECALL_1_T3666972114_H
#ifndef ASYNCOPERATION_T4233810744_H
#define ASYNCOPERATION_T4233810744_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AsyncOperation
struct  AsyncOperation_t4233810744  : public YieldInstruction_t3999321895
{
public:
	// System.IntPtr UnityEngine.AsyncOperation::m_Ptr
	intptr_t ___m_Ptr_0;
	// System.Action`1<UnityEngine.AsyncOperation> UnityEngine.AsyncOperation::m_completeCallback
	Action_1_t3243625498 * ___m_completeCallback_1;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(AsyncOperation_t4233810744, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}

	inline static int32_t get_offset_of_m_completeCallback_1() { return static_cast<int32_t>(offsetof(AsyncOperation_t4233810744, ___m_completeCallback_1)); }
	inline Action_1_t3243625498 * get_m_completeCallback_1() const { return ___m_completeCallback_1; }
	inline Action_1_t3243625498 ** get_address_of_m_completeCallback_1() { return &___m_completeCallback_1; }
	inline void set_m_completeCallback_1(Action_1_t3243625498 * value)
	{
		___m_completeCallback_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_completeCallback_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.AsyncOperation
struct AsyncOperation_t4233810744_marshaled_pinvoke : public YieldInstruction_t3999321895_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
	Il2CppMethodPointer ___m_completeCallback_1;
};
// Native definition for COM marshalling of UnityEngine.AsyncOperation
struct AsyncOperation_t4233810744_marshaled_com : public YieldInstruction_t3999321895_marshaled_com
{
	intptr_t ___m_Ptr_0;
	Il2CppMethodPointer ___m_completeCallback_1;
};
#endif // ASYNCOPERATION_T4233810744_H
#ifndef HIDEFLAGS_T1155951039_H
#define HIDEFLAGS_T1155951039_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.HideFlags
struct  HideFlags_t1155951039 
{
public:
	// System.Int32 UnityEngine.HideFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(HideFlags_t1155951039, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HIDEFLAGS_T1155951039_H
#ifndef COROUTINE_T3619068392_H
#define COROUTINE_T3619068392_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Coroutine
struct  Coroutine_t3619068392  : public YieldInstruction_t3999321895
{
public:
	// System.IntPtr UnityEngine.Coroutine::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(Coroutine_t3619068392, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Coroutine
struct Coroutine_t3619068392_marshaled_pinvoke : public YieldInstruction_t3999321895_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Coroutine
struct Coroutine_t3619068392_marshaled_com : public YieldInstruction_t3999321895_marshaled_com
{
	intptr_t ___m_Ptr_0;
};
#endif // COROUTINE_T3619068392_H
#ifndef CSSMEASUREMODE_T3615638984_H
#define CSSMEASUREMODE_T3615638984_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CSSLayout.CSSMeasureMode
struct  CSSMeasureMode_t3615638984 
{
public:
	// System.Int32 UnityEngine.CSSLayout.CSSMeasureMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CSSMeasureMode_t3615638984, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CSSMEASUREMODE_T3615638984_H
#ifndef WEAKREFERENCE_T3998924468_H
#define WEAKREFERENCE_T3998924468_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.WeakReference
struct  WeakReference_t3998924468  : public RuntimeObject
{
public:
	// System.Boolean System.WeakReference::isLongReference
	bool ___isLongReference_0;
	// System.Runtime.InteropServices.GCHandle System.WeakReference::gcHandle
	GCHandle_t4215464244  ___gcHandle_1;

public:
	inline static int32_t get_offset_of_isLongReference_0() { return static_cast<int32_t>(offsetof(WeakReference_t3998924468, ___isLongReference_0)); }
	inline bool get_isLongReference_0() const { return ___isLongReference_0; }
	inline bool* get_address_of_isLongReference_0() { return &___isLongReference_0; }
	inline void set_isLongReference_0(bool value)
	{
		___isLongReference_0 = value;
	}

	inline static int32_t get_offset_of_gcHandle_1() { return static_cast<int32_t>(offsetof(WeakReference_t3998924468, ___gcHandle_1)); }
	inline GCHandle_t4215464244  get_gcHandle_1() const { return ___gcHandle_1; }
	inline GCHandle_t4215464244 * get_address_of_gcHandle_1() { return &___gcHandle_1; }
	inline void set_gcHandle_1(GCHandle_t4215464244  value)
	{
		___gcHandle_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEAKREFERENCE_T3998924468_H
#ifndef CULLINGGROUP_T1255328198_H
#define CULLINGGROUP_T1255328198_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CullingGroup
struct  CullingGroup_t1255328198  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.CullingGroup::m_Ptr
	intptr_t ___m_Ptr_0;
	// UnityEngine.CullingGroup/StateChanged UnityEngine.CullingGroup::m_OnStateChanged
	StateChanged_t2514413929 * ___m_OnStateChanged_1;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(CullingGroup_t1255328198, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}

	inline static int32_t get_offset_of_m_OnStateChanged_1() { return static_cast<int32_t>(offsetof(CullingGroup_t1255328198, ___m_OnStateChanged_1)); }
	inline StateChanged_t2514413929 * get_m_OnStateChanged_1() const { return ___m_OnStateChanged_1; }
	inline StateChanged_t2514413929 ** get_address_of_m_OnStateChanged_1() { return &___m_OnStateChanged_1; }
	inline void set_m_OnStateChanged_1(StateChanged_t2514413929 * value)
	{
		___m_OnStateChanged_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnStateChanged_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.CullingGroup
struct CullingGroup_t1255328198_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
	Il2CppMethodPointer ___m_OnStateChanged_1;
};
// Native definition for COM marshalling of UnityEngine.CullingGroup
struct CullingGroup_t1255328198_marshaled_com
{
	intptr_t ___m_Ptr_0;
	Il2CppMethodPointer ___m_OnStateChanged_1;
};
#endif // CULLINGGROUP_T1255328198_H
#ifndef RAY_T1113537957_H
#define RAY_T1113537957_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Ray
struct  Ray_t1113537957 
{
public:
	// UnityEngine.Vector3 UnityEngine.Ray::m_Origin
	Vector3_t3239555143  ___m_Origin_0;
	// UnityEngine.Vector3 UnityEngine.Ray::m_Direction
	Vector3_t3239555143  ___m_Direction_1;

public:
	inline static int32_t get_offset_of_m_Origin_0() { return static_cast<int32_t>(offsetof(Ray_t1113537957, ___m_Origin_0)); }
	inline Vector3_t3239555143  get_m_Origin_0() const { return ___m_Origin_0; }
	inline Vector3_t3239555143 * get_address_of_m_Origin_0() { return &___m_Origin_0; }
	inline void set_m_Origin_0(Vector3_t3239555143  value)
	{
		___m_Origin_0 = value;
	}

	inline static int32_t get_offset_of_m_Direction_1() { return static_cast<int32_t>(offsetof(Ray_t1113537957, ___m_Direction_1)); }
	inline Vector3_t3239555143  get_m_Direction_1() const { return ___m_Direction_1; }
	inline Vector3_t3239555143 * get_address_of_m_Direction_1() { return &___m_Direction_1; }
	inline void set_m_Direction_1(Vector3_t3239555143  value)
	{
		___m_Direction_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAY_T1113537957_H
#ifndef CAMERACLEARFLAGS_T4003071365_H
#define CAMERACLEARFLAGS_T4003071365_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CameraClearFlags
struct  CameraClearFlags_t4003071365 
{
public:
	// System.Int32 UnityEngine.CameraClearFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CameraClearFlags_t4003071365, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERACLEARFLAGS_T4003071365_H
#ifndef DISPLAY_T4002355288_H
#define DISPLAY_T4002355288_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Display
struct  Display_t4002355288  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Display::nativeDisplay
	intptr_t ___nativeDisplay_0;

public:
	inline static int32_t get_offset_of_nativeDisplay_0() { return static_cast<int32_t>(offsetof(Display_t4002355288, ___nativeDisplay_0)); }
	inline intptr_t get_nativeDisplay_0() const { return ___nativeDisplay_0; }
	inline intptr_t* get_address_of_nativeDisplay_0() { return &___nativeDisplay_0; }
	inline void set_nativeDisplay_0(intptr_t value)
	{
		___nativeDisplay_0 = value;
	}
};

struct Display_t4002355288_StaticFields
{
public:
	// UnityEngine.Display[] UnityEngine.Display::displays
	DisplayU5BU5D_t130122569* ___displays_1;
	// UnityEngine.Display UnityEngine.Display::_mainDisplay
	Display_t4002355288 * ____mainDisplay_2;
	// UnityEngine.Display/DisplaysUpdatedDelegate UnityEngine.Display::onDisplaysUpdated
	DisplaysUpdatedDelegate_t2805775822 * ___onDisplaysUpdated_3;

public:
	inline static int32_t get_offset_of_displays_1() { return static_cast<int32_t>(offsetof(Display_t4002355288_StaticFields, ___displays_1)); }
	inline DisplayU5BU5D_t130122569* get_displays_1() const { return ___displays_1; }
	inline DisplayU5BU5D_t130122569** get_address_of_displays_1() { return &___displays_1; }
	inline void set_displays_1(DisplayU5BU5D_t130122569* value)
	{
		___displays_1 = value;
		Il2CppCodeGenWriteBarrier((&___displays_1), value);
	}

	inline static int32_t get_offset_of__mainDisplay_2() { return static_cast<int32_t>(offsetof(Display_t4002355288_StaticFields, ____mainDisplay_2)); }
	inline Display_t4002355288 * get__mainDisplay_2() const { return ____mainDisplay_2; }
	inline Display_t4002355288 ** get_address_of__mainDisplay_2() { return &____mainDisplay_2; }
	inline void set__mainDisplay_2(Display_t4002355288 * value)
	{
		____mainDisplay_2 = value;
		Il2CppCodeGenWriteBarrier((&____mainDisplay_2), value);
	}

	inline static int32_t get_offset_of_onDisplaysUpdated_3() { return static_cast<int32_t>(offsetof(Display_t4002355288_StaticFields, ___onDisplaysUpdated_3)); }
	inline DisplaysUpdatedDelegate_t2805775822 * get_onDisplaysUpdated_3() const { return ___onDisplaysUpdated_3; }
	inline DisplaysUpdatedDelegate_t2805775822 ** get_address_of_onDisplaysUpdated_3() { return &___onDisplaysUpdated_3; }
	inline void set_onDisplaysUpdated_3(DisplaysUpdatedDelegate_t2805775822 * value)
	{
		___onDisplaysUpdated_3 = value;
		Il2CppCodeGenWriteBarrier((&___onDisplaysUpdated_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISPLAY_T4002355288_H
#ifndef LOGTYPE_T1259498875_H
#define LOGTYPE_T1259498875_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.LogType
struct  LogType_t1259498875 
{
public:
	// System.Int32 UnityEngine.LogType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LogType_t1259498875, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGTYPE_T1259498875_H
#ifndef METHODINFO_T_H
#define METHODINFO_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MethodInfo
struct  MethodInfo_t  : public MethodBase_t674153939
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // METHODINFO_T_H
#ifndef OBJECT_T2461733472_H
#define OBJECT_T2461733472_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t2461733472  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t2461733472, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t2461733472_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t2461733472_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t2461733472_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t2461733472_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T2461733472_H
#ifndef PERSISTENTLISTENERMODE_T677115295_H
#define PERSISTENTLISTENERMODE_T677115295_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.PersistentListenerMode
struct  PersistentListenerMode_t677115295 
{
public:
	// System.Int32 UnityEngine.Events.PersistentListenerMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PersistentListenerMode_t677115295, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PERSISTENTLISTENERMODE_T677115295_H
#ifndef DELEGATE_T96267039_H
#define DELEGATE_T96267039_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t96267039  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t2624639528 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t96267039, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t96267039, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t96267039, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t96267039, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t96267039, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t96267039, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t96267039, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t96267039, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t96267039, ___data_8)); }
	inline DelegateData_t2624639528 * get_data_8() const { return ___data_8; }
	inline DelegateData_t2624639528 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t2624639528 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T96267039_H
#ifndef CACHEDINVOKABLECALL_1_T1592702797_H
#define CACHEDINVOKABLECALL_1_T1592702797_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.CachedInvokableCall`1<System.Single>
struct  CachedInvokableCall_1_t1592702797  : public InvokableCall_1_t557036258
{
public:
	// T UnityEngine.Events.CachedInvokableCall`1::m_Arg1
	float ___m_Arg1_1;

public:
	inline static int32_t get_offset_of_m_Arg1_1() { return static_cast<int32_t>(offsetof(CachedInvokableCall_1_t1592702797, ___m_Arg1_1)); }
	inline float get_m_Arg1_1() const { return ___m_Arg1_1; }
	inline float* get_address_of_m_Arg1_1() { return &___m_Arg1_1; }
	inline void set_m_Arg1_1(float value)
	{
		___m_Arg1_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CACHEDINVOKABLECALL_1_T1592702797_H
#ifndef ANIMATIONCURVE_T1726276845_H
#define ANIMATIONCURVE_T1726276845_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AnimationCurve
struct  AnimationCurve_t1726276845  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.AnimationCurve::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(AnimationCurve_t1726276845, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.AnimationCurve
struct AnimationCurve_t1726276845_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.AnimationCurve
struct AnimationCurve_t1726276845_marshaled_com
{
	intptr_t ___m_Ptr_0;
};
#endif // ANIMATIONCURVE_T1726276845_H
#ifndef CACHEDINVOKABLECALL_1_T4079887551_H
#define CACHEDINVOKABLECALL_1_T4079887551_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.CachedInvokableCall`1<System.String>
struct  CachedInvokableCall_1_t4079887551  : public InvokableCall_1_t3044221012
{
public:
	// T UnityEngine.Events.CachedInvokableCall`1::m_Arg1
	String_t* ___m_Arg1_1;

public:
	inline static int32_t get_offset_of_m_Arg1_1() { return static_cast<int32_t>(offsetof(CachedInvokableCall_1_t4079887551, ___m_Arg1_1)); }
	inline String_t* get_m_Arg1_1() const { return ___m_Arg1_1; }
	inline String_t** get_address_of_m_Arg1_1() { return &___m_Arg1_1; }
	inline void set_m_Arg1_1(String_t* value)
	{
		___m_Arg1_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Arg1_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CACHEDINVOKABLECALL_1_T4079887551_H
#ifndef GRADIENT_T130030130_H
#define GRADIENT_T130030130_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Gradient
struct  Gradient_t130030130  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Gradient::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(Gradient_t130030130, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Gradient
struct Gradient_t130030130_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Gradient
struct Gradient_t130030130_marshaled_com
{
	intptr_t ___m_Ptr_0;
};
#endif // GRADIENT_T130030130_H
#ifndef CACHEDINVOKABLECALL_1_T3155509814_H
#define CACHEDINVOKABLECALL_1_T3155509814_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.CachedInvokableCall`1<System.Boolean>
struct  CachedInvokableCall_1_t3155509814  : public InvokableCall_1_t2119843275
{
public:
	// T UnityEngine.Events.CachedInvokableCall`1::m_Arg1
	bool ___m_Arg1_1;

public:
	inline static int32_t get_offset_of_m_Arg1_1() { return static_cast<int32_t>(offsetof(CachedInvokableCall_1_t3155509814, ___m_Arg1_1)); }
	inline bool get_m_Arg1_1() const { return ___m_Arg1_1; }
	inline bool* get_address_of_m_Arg1_1() { return &___m_Arg1_1; }
	inline void set_m_Arg1_1(bool value)
	{
		___m_Arg1_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CACHEDINVOKABLECALL_1_T3155509814_H
#ifndef CONSTRUCTORINFO_T3360520918_H
#define CONSTRUCTORINFO_T3360520918_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.ConstructorInfo
struct  ConstructorInfo_t3360520918  : public MethodBase_t674153939
{
public:

public:
};

struct ConstructorInfo_t3360520918_StaticFields
{
public:
	// System.String System.Reflection.ConstructorInfo::ConstructorName
	String_t* ___ConstructorName_0;
	// System.String System.Reflection.ConstructorInfo::TypeConstructorName
	String_t* ___TypeConstructorName_1;

public:
	inline static int32_t get_offset_of_ConstructorName_0() { return static_cast<int32_t>(offsetof(ConstructorInfo_t3360520918_StaticFields, ___ConstructorName_0)); }
	inline String_t* get_ConstructorName_0() const { return ___ConstructorName_0; }
	inline String_t** get_address_of_ConstructorName_0() { return &___ConstructorName_0; }
	inline void set_ConstructorName_0(String_t* value)
	{
		___ConstructorName_0 = value;
		Il2CppCodeGenWriteBarrier((&___ConstructorName_0), value);
	}

	inline static int32_t get_offset_of_TypeConstructorName_1() { return static_cast<int32_t>(offsetof(ConstructorInfo_t3360520918_StaticFields, ___TypeConstructorName_1)); }
	inline String_t* get_TypeConstructorName_1() const { return ___TypeConstructorName_1; }
	inline String_t** get_address_of_TypeConstructorName_1() { return &___TypeConstructorName_1; }
	inline void set_TypeConstructorName_1(String_t* value)
	{
		___TypeConstructorName_1 = value;
		Il2CppCodeGenWriteBarrier((&___TypeConstructorName_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONSTRUCTORINFO_T3360520918_H
#ifndef RUNTIMETYPEHANDLE_T637624852_H
#define RUNTIMETYPEHANDLE_T637624852_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.RuntimeTypeHandle
struct  RuntimeTypeHandle_t637624852 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_t637624852, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMETYPEHANDLE_T637624852_H
#ifndef BINDINGFLAGS_T3268090012_H
#define BINDINGFLAGS_T3268090012_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.BindingFlags
struct  BindingFlags_t3268090012 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BindingFlags_t3268090012, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDINGFLAGS_T3268090012_H
#ifndef UNITYEVENTCALLSTATE_T4278301202_H
#define UNITYEVENTCALLSTATE_T4278301202_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEventCallState
struct  UnityEventCallState_t4278301202 
{
public:
	// System.Int32 UnityEngine.Events.UnityEventCallState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(UnityEventCallState_t4278301202, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENTCALLSTATE_T4278301202_H
#ifndef SENDMESSAGEOPTIONS_T539082817_H
#define SENDMESSAGEOPTIONS_T539082817_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SendMessageOptions
struct  SendMessageOptions_t539082817 
{
public:
	// System.Int32 UnityEngine.SendMessageOptions::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SendMessageOptions_t539082817, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SENDMESSAGEOPTIONS_T539082817_H
#ifndef SCRIPTABLERENDERCONTEXT_T1486166028_H
#define SCRIPTABLERENDERCONTEXT_T1486166028_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.ScriptableRenderContext
struct  ScriptableRenderContext_t1486166028 
{
public:
	// System.IntPtr UnityEngine.Experimental.Rendering.ScriptableRenderContext::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(ScriptableRenderContext_t1486166028, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCRIPTABLERENDERCONTEXT_T1486166028_H
#ifndef PLAYABLE_T1760619938_H
#define PLAYABLE_T1760619938_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.Playable
struct  Playable_t1760619938 
{
public:
	// UnityEngine.Playables.PlayableHandle UnityEngine.Playables.Playable::m_Handle
	PlayableHandle_t3938927722  ___m_Handle_0;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(Playable_t1760619938, ___m_Handle_0)); }
	inline PlayableHandle_t3938927722  get_m_Handle_0() const { return ___m_Handle_0; }
	inline PlayableHandle_t3938927722 * get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(PlayableHandle_t3938927722  value)
	{
		___m_Handle_0 = value;
	}
};

struct Playable_t1760619938_StaticFields
{
public:
	// UnityEngine.Playables.Playable UnityEngine.Playables.Playable::m_NullPlayable
	Playable_t1760619938  ___m_NullPlayable_1;

public:
	inline static int32_t get_offset_of_m_NullPlayable_1() { return static_cast<int32_t>(offsetof(Playable_t1760619938_StaticFields, ___m_NullPlayable_1)); }
	inline Playable_t1760619938  get_m_NullPlayable_1() const { return ___m_NullPlayable_1; }
	inline Playable_t1760619938 * get_address_of_m_NullPlayable_1() { return &___m_NullPlayable_1; }
	inline void set_m_NullPlayable_1(Playable_t1760619938  value)
	{
		___m_NullPlayable_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYABLE_T1760619938_H
#ifndef SCRIPTPLAYABLEOUTPUT_T149882980_H
#define SCRIPTPLAYABLEOUTPUT_T149882980_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.ScriptPlayableOutput
struct  ScriptPlayableOutput_t149882980 
{
public:
	// UnityEngine.Playables.PlayableOutputHandle UnityEngine.Playables.ScriptPlayableOutput::m_Handle
	PlayableOutputHandle_t2719521870  ___m_Handle_0;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(ScriptPlayableOutput_t149882980, ___m_Handle_0)); }
	inline PlayableOutputHandle_t2719521870  get_m_Handle_0() const { return ___m_Handle_0; }
	inline PlayableOutputHandle_t2719521870 * get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(PlayableOutputHandle_t2719521870  value)
	{
		___m_Handle_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCRIPTPLAYABLEOUTPUT_T149882980_H
#ifndef TEXTURE_T238546388_H
#define TEXTURE_T238546388_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Texture
struct  Texture_t238546388  : public Object_t2461733472
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTURE_T238546388_H
#ifndef PLAYABLEBINDING_T136667248_H
#define PLAYABLEBINDING_T136667248_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.PlayableBinding
struct  PlayableBinding_t136667248 
{
public:
	union
	{
		struct
		{
			// System.String UnityEngine.Playables.PlayableBinding::<streamName>k__BackingField
			String_t* ___U3CstreamNameU3Ek__BackingField_2;
			// UnityEngine.Playables.DataStreamType UnityEngine.Playables.PlayableBinding::<streamType>k__BackingField
			int32_t ___U3CstreamTypeU3Ek__BackingField_3;
			// UnityEngine.Object UnityEngine.Playables.PlayableBinding::<sourceObject>k__BackingField
			Object_t2461733472 * ___U3CsourceObjectU3Ek__BackingField_4;
			// System.Type UnityEngine.Playables.PlayableBinding::<sourceBindingType>k__BackingField
			Type_t * ___U3CsourceBindingTypeU3Ek__BackingField_5;
		};
		uint8_t PlayableBinding_t136667248__padding[1];
	};

public:
	inline static int32_t get_offset_of_U3CstreamNameU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(PlayableBinding_t136667248, ___U3CstreamNameU3Ek__BackingField_2)); }
	inline String_t* get_U3CstreamNameU3Ek__BackingField_2() const { return ___U3CstreamNameU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CstreamNameU3Ek__BackingField_2() { return &___U3CstreamNameU3Ek__BackingField_2; }
	inline void set_U3CstreamNameU3Ek__BackingField_2(String_t* value)
	{
		___U3CstreamNameU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CstreamNameU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CstreamTypeU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(PlayableBinding_t136667248, ___U3CstreamTypeU3Ek__BackingField_3)); }
	inline int32_t get_U3CstreamTypeU3Ek__BackingField_3() const { return ___U3CstreamTypeU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CstreamTypeU3Ek__BackingField_3() { return &___U3CstreamTypeU3Ek__BackingField_3; }
	inline void set_U3CstreamTypeU3Ek__BackingField_3(int32_t value)
	{
		___U3CstreamTypeU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CsourceObjectU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(PlayableBinding_t136667248, ___U3CsourceObjectU3Ek__BackingField_4)); }
	inline Object_t2461733472 * get_U3CsourceObjectU3Ek__BackingField_4() const { return ___U3CsourceObjectU3Ek__BackingField_4; }
	inline Object_t2461733472 ** get_address_of_U3CsourceObjectU3Ek__BackingField_4() { return &___U3CsourceObjectU3Ek__BackingField_4; }
	inline void set_U3CsourceObjectU3Ek__BackingField_4(Object_t2461733472 * value)
	{
		___U3CsourceObjectU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsourceObjectU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CsourceBindingTypeU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(PlayableBinding_t136667248, ___U3CsourceBindingTypeU3Ek__BackingField_5)); }
	inline Type_t * get_U3CsourceBindingTypeU3Ek__BackingField_5() const { return ___U3CsourceBindingTypeU3Ek__BackingField_5; }
	inline Type_t ** get_address_of_U3CsourceBindingTypeU3Ek__BackingField_5() { return &___U3CsourceBindingTypeU3Ek__BackingField_5; }
	inline void set_U3CsourceBindingTypeU3Ek__BackingField_5(Type_t * value)
	{
		___U3CsourceBindingTypeU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsourceBindingTypeU3Ek__BackingField_5), value);
	}
};

struct PlayableBinding_t136667248_StaticFields
{
public:
	// UnityEngine.Playables.PlayableBinding[] UnityEngine.Playables.PlayableBinding::None
	PlayableBindingU5BU5D_t207642577* ___None_0;
	// System.Double UnityEngine.Playables.PlayableBinding::DefaultDuration
	double ___DefaultDuration_1;

public:
	inline static int32_t get_offset_of_None_0() { return static_cast<int32_t>(offsetof(PlayableBinding_t136667248_StaticFields, ___None_0)); }
	inline PlayableBindingU5BU5D_t207642577* get_None_0() const { return ___None_0; }
	inline PlayableBindingU5BU5D_t207642577** get_address_of_None_0() { return &___None_0; }
	inline void set_None_0(PlayableBindingU5BU5D_t207642577* value)
	{
		___None_0 = value;
		Il2CppCodeGenWriteBarrier((&___None_0), value);
	}

	inline static int32_t get_offset_of_DefaultDuration_1() { return static_cast<int32_t>(offsetof(PlayableBinding_t136667248_StaticFields, ___DefaultDuration_1)); }
	inline double get_DefaultDuration_1() const { return ___DefaultDuration_1; }
	inline double* get_address_of_DefaultDuration_1() { return &___DefaultDuration_1; }
	inline void set_DefaultDuration_1(double value)
	{
		___DefaultDuration_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Playables.PlayableBinding
struct PlayableBinding_t136667248_marshaled_pinvoke
{
	union
	{
		struct
		{
			char* ___U3CstreamNameU3Ek__BackingField_2;
			int32_t ___U3CstreamTypeU3Ek__BackingField_3;
			Object_t2461733472_marshaled_pinvoke ___U3CsourceObjectU3Ek__BackingField_4;
			Type_t * ___U3CsourceBindingTypeU3Ek__BackingField_5;
		};
		uint8_t PlayableBinding_t136667248__padding[1];
	};
};
// Native definition for COM marshalling of UnityEngine.Playables.PlayableBinding
struct PlayableBinding_t136667248_marshaled_com
{
	union
	{
		struct
		{
			Il2CppChar* ___U3CstreamNameU3Ek__BackingField_2;
			int32_t ___U3CstreamTypeU3Ek__BackingField_3;
			Object_t2461733472_marshaled_com* ___U3CsourceObjectU3Ek__BackingField_4;
			Type_t * ___U3CsourceBindingTypeU3Ek__BackingField_5;
		};
		uint8_t PlayableBinding_t136667248__padding[1];
	};
};
#endif // PLAYABLEBINDING_T136667248_H
#ifndef ASSETBUNDLEREQUEST_T2873072919_H
#define ASSETBUNDLEREQUEST_T2873072919_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AssetBundleRequest
struct  AssetBundleRequest_t2873072919  : public AsyncOperation_t4233810744
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.AssetBundleRequest
struct AssetBundleRequest_t2873072919_marshaled_pinvoke : public AsyncOperation_t4233810744_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.AssetBundleRequest
struct AssetBundleRequest_t2873072919_marshaled_com : public AsyncOperation_t4233810744_marshaled_com
{
};
#endif // ASSETBUNDLEREQUEST_T2873072919_H
#ifndef PLAYABLEOUTPUT_T3840687608_H
#define PLAYABLEOUTPUT_T3840687608_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.PlayableOutput
struct  PlayableOutput_t3840687608 
{
public:
	// UnityEngine.Playables.PlayableOutputHandle UnityEngine.Playables.PlayableOutput::m_Handle
	PlayableOutputHandle_t2719521870  ___m_Handle_0;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(PlayableOutput_t3840687608, ___m_Handle_0)); }
	inline PlayableOutputHandle_t2719521870  get_m_Handle_0() const { return ___m_Handle_0; }
	inline PlayableOutputHandle_t2719521870 * get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(PlayableOutputHandle_t2719521870  value)
	{
		___m_Handle_0 = value;
	}
};

struct PlayableOutput_t3840687608_StaticFields
{
public:
	// UnityEngine.Playables.PlayableOutput UnityEngine.Playables.PlayableOutput::m_NullPlayableOutput
	PlayableOutput_t3840687608  ___m_NullPlayableOutput_1;

public:
	inline static int32_t get_offset_of_m_NullPlayableOutput_1() { return static_cast<int32_t>(offsetof(PlayableOutput_t3840687608_StaticFields, ___m_NullPlayableOutput_1)); }
	inline PlayableOutput_t3840687608  get_m_NullPlayableOutput_1() const { return ___m_NullPlayableOutput_1; }
	inline PlayableOutput_t3840687608 * get_address_of_m_NullPlayableOutput_1() { return &___m_NullPlayableOutput_1; }
	inline void set_m_NullPlayableOutput_1(PlayableOutput_t3840687608  value)
	{
		___m_NullPlayableOutput_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYABLEOUTPUT_T3840687608_H
#ifndef COMPONENT_T2160255836_H
#define COMPONENT_T2160255836_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t2160255836  : public Object_t2461733472
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T2160255836_H
#ifndef MULTICASTDELEGATE_T69367374_H
#define MULTICASTDELEGATE_T69367374_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t69367374  : public Delegate_t96267039
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t69367374 * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t69367374 * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t69367374, ___prev_9)); }
	inline MulticastDelegate_t69367374 * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t69367374 ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t69367374 * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t69367374, ___kpm_next_10)); }
	inline MulticastDelegate_t69367374 * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t69367374 ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t69367374 * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T69367374_H
#ifndef ASSETBUNDLECREATEREQUEST_T3985900355_H
#define ASSETBUNDLECREATEREQUEST_T3985900355_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AssetBundleCreateRequest
struct  AssetBundleCreateRequest_t3985900355  : public AsyncOperation_t4233810744
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.AssetBundleCreateRequest
struct AssetBundleCreateRequest_t3985900355_marshaled_pinvoke : public AsyncOperation_t4233810744_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.AssetBundleCreateRequest
struct AssetBundleCreateRequest_t3985900355_marshaled_com : public AsyncOperation_t4233810744_marshaled_com
{
};
#endif // ASSETBUNDLECREATEREQUEST_T3985900355_H
#ifndef LOGGER_T605719344_H
#define LOGGER_T605719344_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Logger
struct  Logger_t605719344  : public RuntimeObject
{
public:
	// UnityEngine.ILogHandler UnityEngine.Logger::<logHandler>k__BackingField
	RuntimeObject* ___U3ClogHandlerU3Ek__BackingField_0;
	// System.Boolean UnityEngine.Logger::<logEnabled>k__BackingField
	bool ___U3ClogEnabledU3Ek__BackingField_1;
	// UnityEngine.LogType UnityEngine.Logger::<filterLogType>k__BackingField
	int32_t ___U3CfilterLogTypeU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3ClogHandlerU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Logger_t605719344, ___U3ClogHandlerU3Ek__BackingField_0)); }
	inline RuntimeObject* get_U3ClogHandlerU3Ek__BackingField_0() const { return ___U3ClogHandlerU3Ek__BackingField_0; }
	inline RuntimeObject** get_address_of_U3ClogHandlerU3Ek__BackingField_0() { return &___U3ClogHandlerU3Ek__BackingField_0; }
	inline void set_U3ClogHandlerU3Ek__BackingField_0(RuntimeObject* value)
	{
		___U3ClogHandlerU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClogHandlerU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3ClogEnabledU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Logger_t605719344, ___U3ClogEnabledU3Ek__BackingField_1)); }
	inline bool get_U3ClogEnabledU3Ek__BackingField_1() const { return ___U3ClogEnabledU3Ek__BackingField_1; }
	inline bool* get_address_of_U3ClogEnabledU3Ek__BackingField_1() { return &___U3ClogEnabledU3Ek__BackingField_1; }
	inline void set_U3ClogEnabledU3Ek__BackingField_1(bool value)
	{
		___U3ClogEnabledU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CfilterLogTypeU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Logger_t605719344, ___U3CfilterLogTypeU3Ek__BackingField_2)); }
	inline int32_t get_U3CfilterLogTypeU3Ek__BackingField_2() const { return ___U3CfilterLogTypeU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CfilterLogTypeU3Ek__BackingField_2() { return &___U3CfilterLogTypeU3Ek__BackingField_2; }
	inline void set_U3CfilterLogTypeU3Ek__BackingField_2(int32_t value)
	{
		___U3CfilterLogTypeU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGGER_T605719344_H
#ifndef TYPE_T_H
#define TYPE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Type
struct  Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_t637624852  ____impl_1;

public:
	inline static int32_t get_offset_of__impl_1() { return static_cast<int32_t>(offsetof(Type_t, ____impl_1)); }
	inline RuntimeTypeHandle_t637624852  get__impl_1() const { return ____impl_1; }
	inline RuntimeTypeHandle_t637624852 * get_address_of__impl_1() { return &____impl_1; }
	inline void set__impl_1(RuntimeTypeHandle_t637624852  value)
	{
		____impl_1 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_2;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t1830665827* ___EmptyTypes_3;
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t1197335030 * ___FilterAttribute_4;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t1197335030 * ___FilterName_5;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t1197335030 * ___FilterNameIgnoreCase_6;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_7;

public:
	inline static int32_t get_offset_of_Delimiter_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_2)); }
	inline Il2CppChar get_Delimiter_2() const { return ___Delimiter_2; }
	inline Il2CppChar* get_address_of_Delimiter_2() { return &___Delimiter_2; }
	inline void set_Delimiter_2(Il2CppChar value)
	{
		___Delimiter_2 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_3)); }
	inline TypeU5BU5D_t1830665827* get_EmptyTypes_3() const { return ___EmptyTypes_3; }
	inline TypeU5BU5D_t1830665827** get_address_of_EmptyTypes_3() { return &___EmptyTypes_3; }
	inline void set_EmptyTypes_3(TypeU5BU5D_t1830665827* value)
	{
		___EmptyTypes_3 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyTypes_3), value);
	}

	inline static int32_t get_offset_of_FilterAttribute_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_4)); }
	inline MemberFilter_t1197335030 * get_FilterAttribute_4() const { return ___FilterAttribute_4; }
	inline MemberFilter_t1197335030 ** get_address_of_FilterAttribute_4() { return &___FilterAttribute_4; }
	inline void set_FilterAttribute_4(MemberFilter_t1197335030 * value)
	{
		___FilterAttribute_4 = value;
		Il2CppCodeGenWriteBarrier((&___FilterAttribute_4), value);
	}

	inline static int32_t get_offset_of_FilterName_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_5)); }
	inline MemberFilter_t1197335030 * get_FilterName_5() const { return ___FilterName_5; }
	inline MemberFilter_t1197335030 ** get_address_of_FilterName_5() { return &___FilterName_5; }
	inline void set_FilterName_5(MemberFilter_t1197335030 * value)
	{
		___FilterName_5 = value;
		Il2CppCodeGenWriteBarrier((&___FilterName_5), value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_6)); }
	inline MemberFilter_t1197335030 * get_FilterNameIgnoreCase_6() const { return ___FilterNameIgnoreCase_6; }
	inline MemberFilter_t1197335030 ** get_address_of_FilterNameIgnoreCase_6() { return &___FilterNameIgnoreCase_6; }
	inline void set_FilterNameIgnoreCase_6(MemberFilter_t1197335030 * value)
	{
		___FilterNameIgnoreCase_6 = value;
		Il2CppCodeGenWriteBarrier((&___FilterNameIgnoreCase_6), value);
	}

	inline static int32_t get_offset_of_Missing_7() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_7)); }
	inline RuntimeObject * get_Missing_7() const { return ___Missing_7; }
	inline RuntimeObject ** get_address_of_Missing_7() { return &___Missing_7; }
	inline void set_Missing_7(RuntimeObject * value)
	{
		___Missing_7 = value;
		Il2CppCodeGenWriteBarrier((&___Missing_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T_H
#ifndef SCRIPTABLEOBJECT_T476655885_H
#define SCRIPTABLEOBJECT_T476655885_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_t476655885  : public Object_t2461733472
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t476655885_marshaled_pinvoke : public Object_t2461733472_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t476655885_marshaled_com : public Object_t2461733472_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_T476655885_H
#ifndef ARGUMENTNULLEXCEPTION_T517973450_H
#define ARGUMENTNULLEXCEPTION_T517973450_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ArgumentNullException
struct  ArgumentNullException_t517973450  : public ArgumentException_t1372035057
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARGUMENTNULLEXCEPTION_T517973450_H
#ifndef PERSISTENTCALL_T2687057912_H
#define PERSISTENTCALL_T2687057912_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.PersistentCall
struct  PersistentCall_t2687057912  : public RuntimeObject
{
public:
	// UnityEngine.Object UnityEngine.Events.PersistentCall::m_Target
	Object_t2461733472 * ___m_Target_0;
	// System.String UnityEngine.Events.PersistentCall::m_MethodName
	String_t* ___m_MethodName_1;
	// UnityEngine.Events.PersistentListenerMode UnityEngine.Events.PersistentCall::m_Mode
	int32_t ___m_Mode_2;
	// UnityEngine.Events.ArgumentCache UnityEngine.Events.PersistentCall::m_Arguments
	ArgumentCache_t3682645796 * ___m_Arguments_3;
	// UnityEngine.Events.UnityEventCallState UnityEngine.Events.PersistentCall::m_CallState
	int32_t ___m_CallState_4;

public:
	inline static int32_t get_offset_of_m_Target_0() { return static_cast<int32_t>(offsetof(PersistentCall_t2687057912, ___m_Target_0)); }
	inline Object_t2461733472 * get_m_Target_0() const { return ___m_Target_0; }
	inline Object_t2461733472 ** get_address_of_m_Target_0() { return &___m_Target_0; }
	inline void set_m_Target_0(Object_t2461733472 * value)
	{
		___m_Target_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Target_0), value);
	}

	inline static int32_t get_offset_of_m_MethodName_1() { return static_cast<int32_t>(offsetof(PersistentCall_t2687057912, ___m_MethodName_1)); }
	inline String_t* get_m_MethodName_1() const { return ___m_MethodName_1; }
	inline String_t** get_address_of_m_MethodName_1() { return &___m_MethodName_1; }
	inline void set_m_MethodName_1(String_t* value)
	{
		___m_MethodName_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_MethodName_1), value);
	}

	inline static int32_t get_offset_of_m_Mode_2() { return static_cast<int32_t>(offsetof(PersistentCall_t2687057912, ___m_Mode_2)); }
	inline int32_t get_m_Mode_2() const { return ___m_Mode_2; }
	inline int32_t* get_address_of_m_Mode_2() { return &___m_Mode_2; }
	inline void set_m_Mode_2(int32_t value)
	{
		___m_Mode_2 = value;
	}

	inline static int32_t get_offset_of_m_Arguments_3() { return static_cast<int32_t>(offsetof(PersistentCall_t2687057912, ___m_Arguments_3)); }
	inline ArgumentCache_t3682645796 * get_m_Arguments_3() const { return ___m_Arguments_3; }
	inline ArgumentCache_t3682645796 ** get_address_of_m_Arguments_3() { return &___m_Arguments_3; }
	inline void set_m_Arguments_3(ArgumentCache_t3682645796 * value)
	{
		___m_Arguments_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Arguments_3), value);
	}

	inline static int32_t get_offset_of_m_CallState_4() { return static_cast<int32_t>(offsetof(PersistentCall_t2687057912, ___m_CallState_4)); }
	inline int32_t get_m_CallState_4() const { return ___m_CallState_4; }
	inline int32_t* get_address_of_m_CallState_4() { return &___m_CallState_4; }
	inline void set_m_CallState_4(int32_t value)
	{
		___m_CallState_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PERSISTENTCALL_T2687057912_H
#ifndef PARAMETERINFO_T3893964560_H
#define PARAMETERINFO_T3893964560_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.ParameterInfo
struct  ParameterInfo_t3893964560  : public RuntimeObject
{
public:
	// System.Type System.Reflection.ParameterInfo::ClassImpl
	Type_t * ___ClassImpl_0;
	// System.Object System.Reflection.ParameterInfo::DefaultValueImpl
	RuntimeObject * ___DefaultValueImpl_1;
	// System.Reflection.MemberInfo System.Reflection.ParameterInfo::MemberImpl
	MemberInfo_t * ___MemberImpl_2;
	// System.String System.Reflection.ParameterInfo::NameImpl
	String_t* ___NameImpl_3;
	// System.Int32 System.Reflection.ParameterInfo::PositionImpl
	int32_t ___PositionImpl_4;
	// System.Reflection.ParameterAttributes System.Reflection.ParameterInfo::AttrsImpl
	int32_t ___AttrsImpl_5;
	// System.Reflection.Emit.UnmanagedMarshal System.Reflection.ParameterInfo::marshalAs
	UnmanagedMarshal_t1503927065 * ___marshalAs_6;

public:
	inline static int32_t get_offset_of_ClassImpl_0() { return static_cast<int32_t>(offsetof(ParameterInfo_t3893964560, ___ClassImpl_0)); }
	inline Type_t * get_ClassImpl_0() const { return ___ClassImpl_0; }
	inline Type_t ** get_address_of_ClassImpl_0() { return &___ClassImpl_0; }
	inline void set_ClassImpl_0(Type_t * value)
	{
		___ClassImpl_0 = value;
		Il2CppCodeGenWriteBarrier((&___ClassImpl_0), value);
	}

	inline static int32_t get_offset_of_DefaultValueImpl_1() { return static_cast<int32_t>(offsetof(ParameterInfo_t3893964560, ___DefaultValueImpl_1)); }
	inline RuntimeObject * get_DefaultValueImpl_1() const { return ___DefaultValueImpl_1; }
	inline RuntimeObject ** get_address_of_DefaultValueImpl_1() { return &___DefaultValueImpl_1; }
	inline void set_DefaultValueImpl_1(RuntimeObject * value)
	{
		___DefaultValueImpl_1 = value;
		Il2CppCodeGenWriteBarrier((&___DefaultValueImpl_1), value);
	}

	inline static int32_t get_offset_of_MemberImpl_2() { return static_cast<int32_t>(offsetof(ParameterInfo_t3893964560, ___MemberImpl_2)); }
	inline MemberInfo_t * get_MemberImpl_2() const { return ___MemberImpl_2; }
	inline MemberInfo_t ** get_address_of_MemberImpl_2() { return &___MemberImpl_2; }
	inline void set_MemberImpl_2(MemberInfo_t * value)
	{
		___MemberImpl_2 = value;
		Il2CppCodeGenWriteBarrier((&___MemberImpl_2), value);
	}

	inline static int32_t get_offset_of_NameImpl_3() { return static_cast<int32_t>(offsetof(ParameterInfo_t3893964560, ___NameImpl_3)); }
	inline String_t* get_NameImpl_3() const { return ___NameImpl_3; }
	inline String_t** get_address_of_NameImpl_3() { return &___NameImpl_3; }
	inline void set_NameImpl_3(String_t* value)
	{
		___NameImpl_3 = value;
		Il2CppCodeGenWriteBarrier((&___NameImpl_3), value);
	}

	inline static int32_t get_offset_of_PositionImpl_4() { return static_cast<int32_t>(offsetof(ParameterInfo_t3893964560, ___PositionImpl_4)); }
	inline int32_t get_PositionImpl_4() const { return ___PositionImpl_4; }
	inline int32_t* get_address_of_PositionImpl_4() { return &___PositionImpl_4; }
	inline void set_PositionImpl_4(int32_t value)
	{
		___PositionImpl_4 = value;
	}

	inline static int32_t get_offset_of_AttrsImpl_5() { return static_cast<int32_t>(offsetof(ParameterInfo_t3893964560, ___AttrsImpl_5)); }
	inline int32_t get_AttrsImpl_5() const { return ___AttrsImpl_5; }
	inline int32_t* get_address_of_AttrsImpl_5() { return &___AttrsImpl_5; }
	inline void set_AttrsImpl_5(int32_t value)
	{
		___AttrsImpl_5 = value;
	}

	inline static int32_t get_offset_of_marshalAs_6() { return static_cast<int32_t>(offsetof(ParameterInfo_t3893964560, ___marshalAs_6)); }
	inline UnmanagedMarshal_t1503927065 * get_marshalAs_6() const { return ___marshalAs_6; }
	inline UnmanagedMarshal_t1503927065 ** get_address_of_marshalAs_6() { return &___marshalAs_6; }
	inline void set_marshalAs_6(UnmanagedMarshal_t1503927065 * value)
	{
		___marshalAs_6 = value;
		Il2CppCodeGenWriteBarrier((&___marshalAs_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARAMETERINFO_T3893964560_H
#ifndef FAILEDTOLOADSCRIPTOBJECT_T2431108707_H
#define FAILEDTOLOADSCRIPTOBJECT_T2431108707_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.FailedToLoadScriptObject
struct  FailedToLoadScriptObject_t2431108707  : public Object_t2461733472
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.FailedToLoadScriptObject
struct FailedToLoadScriptObject_t2431108707_marshaled_pinvoke : public Object_t2461733472_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.FailedToLoadScriptObject
struct FailedToLoadScriptObject_t2431108707_marshaled_com : public Object_t2461733472_marshaled_com
{
};
#endif // FAILEDTOLOADSCRIPTOBJECT_T2431108707_H
#ifndef DATETIME_T507798353_H
#define DATETIME_T507798353_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t507798353 
{
public:
	// System.TimeSpan System.DateTime::ticks
	TimeSpan_t2986675223  ___ticks_0;
	// System.DateTimeKind System.DateTime::kind
	int32_t ___kind_1;

public:
	inline static int32_t get_offset_of_ticks_0() { return static_cast<int32_t>(offsetof(DateTime_t507798353, ___ticks_0)); }
	inline TimeSpan_t2986675223  get_ticks_0() const { return ___ticks_0; }
	inline TimeSpan_t2986675223 * get_address_of_ticks_0() { return &___ticks_0; }
	inline void set_ticks_0(TimeSpan_t2986675223  value)
	{
		___ticks_0 = value;
	}

	inline static int32_t get_offset_of_kind_1() { return static_cast<int32_t>(offsetof(DateTime_t507798353, ___kind_1)); }
	inline int32_t get_kind_1() const { return ___kind_1; }
	inline int32_t* get_address_of_kind_1() { return &___kind_1; }
	inline void set_kind_1(int32_t value)
	{
		___kind_1 = value;
	}
};

struct DateTime_t507798353_StaticFields
{
public:
	// System.DateTime System.DateTime::MaxValue
	DateTime_t507798353  ___MaxValue_2;
	// System.DateTime System.DateTime::MinValue
	DateTime_t507798353  ___MinValue_3;
	// System.String[] System.DateTime::ParseTimeFormats
	StringU5BU5D_t522578821* ___ParseTimeFormats_4;
	// System.String[] System.DateTime::ParseYearDayMonthFormats
	StringU5BU5D_t522578821* ___ParseYearDayMonthFormats_5;
	// System.String[] System.DateTime::ParseYearMonthDayFormats
	StringU5BU5D_t522578821* ___ParseYearMonthDayFormats_6;
	// System.String[] System.DateTime::ParseDayMonthYearFormats
	StringU5BU5D_t522578821* ___ParseDayMonthYearFormats_7;
	// System.String[] System.DateTime::ParseMonthDayYearFormats
	StringU5BU5D_t522578821* ___ParseMonthDayYearFormats_8;
	// System.String[] System.DateTime::MonthDayShortFormats
	StringU5BU5D_t522578821* ___MonthDayShortFormats_9;
	// System.String[] System.DateTime::DayMonthShortFormats
	StringU5BU5D_t522578821* ___DayMonthShortFormats_10;
	// System.Int32[] System.DateTime::daysmonth
	Int32U5BU5D_t2856113350* ___daysmonth_11;
	// System.Int32[] System.DateTime::daysmonthleap
	Int32U5BU5D_t2856113350* ___daysmonthleap_12;
	// System.Object System.DateTime::to_local_time_span_object
	RuntimeObject * ___to_local_time_span_object_13;
	// System.Int64 System.DateTime::last_now
	int64_t ___last_now_14;

public:
	inline static int32_t get_offset_of_MaxValue_2() { return static_cast<int32_t>(offsetof(DateTime_t507798353_StaticFields, ___MaxValue_2)); }
	inline DateTime_t507798353  get_MaxValue_2() const { return ___MaxValue_2; }
	inline DateTime_t507798353 * get_address_of_MaxValue_2() { return &___MaxValue_2; }
	inline void set_MaxValue_2(DateTime_t507798353  value)
	{
		___MaxValue_2 = value;
	}

	inline static int32_t get_offset_of_MinValue_3() { return static_cast<int32_t>(offsetof(DateTime_t507798353_StaticFields, ___MinValue_3)); }
	inline DateTime_t507798353  get_MinValue_3() const { return ___MinValue_3; }
	inline DateTime_t507798353 * get_address_of_MinValue_3() { return &___MinValue_3; }
	inline void set_MinValue_3(DateTime_t507798353  value)
	{
		___MinValue_3 = value;
	}

	inline static int32_t get_offset_of_ParseTimeFormats_4() { return static_cast<int32_t>(offsetof(DateTime_t507798353_StaticFields, ___ParseTimeFormats_4)); }
	inline StringU5BU5D_t522578821* get_ParseTimeFormats_4() const { return ___ParseTimeFormats_4; }
	inline StringU5BU5D_t522578821** get_address_of_ParseTimeFormats_4() { return &___ParseTimeFormats_4; }
	inline void set_ParseTimeFormats_4(StringU5BU5D_t522578821* value)
	{
		___ParseTimeFormats_4 = value;
		Il2CppCodeGenWriteBarrier((&___ParseTimeFormats_4), value);
	}

	inline static int32_t get_offset_of_ParseYearDayMonthFormats_5() { return static_cast<int32_t>(offsetof(DateTime_t507798353_StaticFields, ___ParseYearDayMonthFormats_5)); }
	inline StringU5BU5D_t522578821* get_ParseYearDayMonthFormats_5() const { return ___ParseYearDayMonthFormats_5; }
	inline StringU5BU5D_t522578821** get_address_of_ParseYearDayMonthFormats_5() { return &___ParseYearDayMonthFormats_5; }
	inline void set_ParseYearDayMonthFormats_5(StringU5BU5D_t522578821* value)
	{
		___ParseYearDayMonthFormats_5 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearDayMonthFormats_5), value);
	}

	inline static int32_t get_offset_of_ParseYearMonthDayFormats_6() { return static_cast<int32_t>(offsetof(DateTime_t507798353_StaticFields, ___ParseYearMonthDayFormats_6)); }
	inline StringU5BU5D_t522578821* get_ParseYearMonthDayFormats_6() const { return ___ParseYearMonthDayFormats_6; }
	inline StringU5BU5D_t522578821** get_address_of_ParseYearMonthDayFormats_6() { return &___ParseYearMonthDayFormats_6; }
	inline void set_ParseYearMonthDayFormats_6(StringU5BU5D_t522578821* value)
	{
		___ParseYearMonthDayFormats_6 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearMonthDayFormats_6), value);
	}

	inline static int32_t get_offset_of_ParseDayMonthYearFormats_7() { return static_cast<int32_t>(offsetof(DateTime_t507798353_StaticFields, ___ParseDayMonthYearFormats_7)); }
	inline StringU5BU5D_t522578821* get_ParseDayMonthYearFormats_7() const { return ___ParseDayMonthYearFormats_7; }
	inline StringU5BU5D_t522578821** get_address_of_ParseDayMonthYearFormats_7() { return &___ParseDayMonthYearFormats_7; }
	inline void set_ParseDayMonthYearFormats_7(StringU5BU5D_t522578821* value)
	{
		___ParseDayMonthYearFormats_7 = value;
		Il2CppCodeGenWriteBarrier((&___ParseDayMonthYearFormats_7), value);
	}

	inline static int32_t get_offset_of_ParseMonthDayYearFormats_8() { return static_cast<int32_t>(offsetof(DateTime_t507798353_StaticFields, ___ParseMonthDayYearFormats_8)); }
	inline StringU5BU5D_t522578821* get_ParseMonthDayYearFormats_8() const { return ___ParseMonthDayYearFormats_8; }
	inline StringU5BU5D_t522578821** get_address_of_ParseMonthDayYearFormats_8() { return &___ParseMonthDayYearFormats_8; }
	inline void set_ParseMonthDayYearFormats_8(StringU5BU5D_t522578821* value)
	{
		___ParseMonthDayYearFormats_8 = value;
		Il2CppCodeGenWriteBarrier((&___ParseMonthDayYearFormats_8), value);
	}

	inline static int32_t get_offset_of_MonthDayShortFormats_9() { return static_cast<int32_t>(offsetof(DateTime_t507798353_StaticFields, ___MonthDayShortFormats_9)); }
	inline StringU5BU5D_t522578821* get_MonthDayShortFormats_9() const { return ___MonthDayShortFormats_9; }
	inline StringU5BU5D_t522578821** get_address_of_MonthDayShortFormats_9() { return &___MonthDayShortFormats_9; }
	inline void set_MonthDayShortFormats_9(StringU5BU5D_t522578821* value)
	{
		___MonthDayShortFormats_9 = value;
		Il2CppCodeGenWriteBarrier((&___MonthDayShortFormats_9), value);
	}

	inline static int32_t get_offset_of_DayMonthShortFormats_10() { return static_cast<int32_t>(offsetof(DateTime_t507798353_StaticFields, ___DayMonthShortFormats_10)); }
	inline StringU5BU5D_t522578821* get_DayMonthShortFormats_10() const { return ___DayMonthShortFormats_10; }
	inline StringU5BU5D_t522578821** get_address_of_DayMonthShortFormats_10() { return &___DayMonthShortFormats_10; }
	inline void set_DayMonthShortFormats_10(StringU5BU5D_t522578821* value)
	{
		___DayMonthShortFormats_10 = value;
		Il2CppCodeGenWriteBarrier((&___DayMonthShortFormats_10), value);
	}

	inline static int32_t get_offset_of_daysmonth_11() { return static_cast<int32_t>(offsetof(DateTime_t507798353_StaticFields, ___daysmonth_11)); }
	inline Int32U5BU5D_t2856113350* get_daysmonth_11() const { return ___daysmonth_11; }
	inline Int32U5BU5D_t2856113350** get_address_of_daysmonth_11() { return &___daysmonth_11; }
	inline void set_daysmonth_11(Int32U5BU5D_t2856113350* value)
	{
		___daysmonth_11 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonth_11), value);
	}

	inline static int32_t get_offset_of_daysmonthleap_12() { return static_cast<int32_t>(offsetof(DateTime_t507798353_StaticFields, ___daysmonthleap_12)); }
	inline Int32U5BU5D_t2856113350* get_daysmonthleap_12() const { return ___daysmonthleap_12; }
	inline Int32U5BU5D_t2856113350** get_address_of_daysmonthleap_12() { return &___daysmonthleap_12; }
	inline void set_daysmonthleap_12(Int32U5BU5D_t2856113350* value)
	{
		___daysmonthleap_12 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonthleap_12), value);
	}

	inline static int32_t get_offset_of_to_local_time_span_object_13() { return static_cast<int32_t>(offsetof(DateTime_t507798353_StaticFields, ___to_local_time_span_object_13)); }
	inline RuntimeObject * get_to_local_time_span_object_13() const { return ___to_local_time_span_object_13; }
	inline RuntimeObject ** get_address_of_to_local_time_span_object_13() { return &___to_local_time_span_object_13; }
	inline void set_to_local_time_span_object_13(RuntimeObject * value)
	{
		___to_local_time_span_object_13 = value;
		Il2CppCodeGenWriteBarrier((&___to_local_time_span_object_13), value);
	}

	inline static int32_t get_offset_of_last_now_14() { return static_cast<int32_t>(offsetof(DateTime_t507798353_StaticFields, ___last_now_14)); }
	inline int64_t get_last_now_14() const { return ___last_now_14; }
	inline int64_t* get_address_of_last_now_14() { return &___last_now_14; }
	inline void set_last_now_14(int64_t value)
	{
		___last_now_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T507798353_H
#ifndef GAMEOBJECT_T3732643618_H
#define GAMEOBJECT_T3732643618_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GameObject
struct  GameObject_t3732643618  : public Object_t2461733472
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECT_T3732643618_H
#ifndef ASYNCCALLBACK_T1046330627_H
#define ASYNCCALLBACK_T1046330627_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.AsyncCallback
struct  AsyncCallback_t1046330627  : public MulticastDelegate_t69367374
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCCALLBACK_T1046330627_H
#ifndef UNITYACTION_T35329723_H
#define UNITYACTION_T35329723_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityAction
struct  UnityAction_t35329723  : public MulticastDelegate_t69367374
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYACTION_T35329723_H
#ifndef LOGCALLBACK_T3381938013_H
#define LOGCALLBACK_T3381938013_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Application/LogCallback
struct  LogCallback_t3381938013  : public MulticastDelegate_t69367374
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGCALLBACK_T3381938013_H
#ifndef ACTION_1_T3243625498_H
#define ACTION_1_T3243625498_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Action`1<UnityEngine.AsyncOperation>
struct  Action_1_t3243625498  : public MulticastDelegate_t69367374
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTION_1_T3243625498_H
#ifndef LOWMEMORYCALLBACK_T454721290_H
#define LOWMEMORYCALLBACK_T454721290_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Application/LowMemoryCallback
struct  LowMemoryCallback_t454721290  : public MulticastDelegate_t69367374
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOWMEMORYCALLBACK_T454721290_H
#ifndef FUNC_2_T874238247_H
#define FUNC_2_T874238247_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Func`2<UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers,System.Boolean>
struct  Func_2_t874238247  : public MulticastDelegate_t69367374
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FUNC_2_T874238247_H
#ifndef TRANSFORM_T94239816_H
#define TRANSFORM_T94239816_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Transform
struct  Transform_t94239816  : public Component_t2160255836
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORM_T94239816_H
#ifndef BEHAVIOUR_T4110946901_H
#define BEHAVIOUR_T4110946901_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t4110946901  : public Component_t2160255836
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T4110946901_H
#ifndef RENDERTEXTURE_T2667189599_H
#define RENDERTEXTURE_T2667189599_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RenderTexture
struct  RenderTexture_t2667189599  : public Texture_t238546388
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERTEXTURE_T2667189599_H
#ifndef PLAYABLEASSET_T2436284446_H
#define PLAYABLEASSET_T2436284446_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.PlayableAsset
struct  PlayableAsset_t2436284446  : public ScriptableObject_t476655885
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYABLEASSET_T2436284446_H
#ifndef CAMERACALLBACK_T2065492188_H
#define CAMERACALLBACK_T2065492188_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Camera/CameraCallback
struct  CameraCallback_t2065492188  : public MulticastDelegate_t69367374
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERACALLBACK_T2065492188_H
#ifndef CSSMEASUREFUNC_T2162796896_H
#define CSSMEASUREFUNC_T2162796896_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CSSLayout.CSSMeasureFunc
struct  CSSMeasureFunc_t2162796896  : public MulticastDelegate_t69367374
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CSSMEASUREFUNC_T2162796896_H
#ifndef STATECHANGED_T2514413929_H
#define STATECHANGED_T2514413929_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CullingGroup/StateChanged
struct  StateChanged_t2514413929  : public MulticastDelegate_t69367374
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATECHANGED_T2514413929_H
#ifndef DISPLAYSUPDATEDDELEGATE_T2805775822_H
#define DISPLAYSUPDATEDDELEGATE_T2805775822_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Display/DisplaysUpdatedDelegate
struct  DisplaysUpdatedDelegate_t2805775822  : public MulticastDelegate_t69367374
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISPLAYSUPDATEDDELEGATE_T2805775822_H
#ifndef PLAYERCONNECTION_T3559457672_H
#define PLAYERCONNECTION_T3559457672_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.PlayerConnection.PlayerConnection
struct  PlayerConnection_t3559457672  : public ScriptableObject_t476655885
{
public:
	// UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents UnityEngine.Networking.PlayerConnection.PlayerConnection::m_PlayerEditorConnectionEvents
	PlayerEditorConnectionEvents_t2151463461 * ___m_PlayerEditorConnectionEvents_2;
	// System.Collections.Generic.List`1<System.Int32> UnityEngine.Networking.PlayerConnection.PlayerConnection::m_connectedPlayers
	List_1_t449703515 * ___m_connectedPlayers_3;

public:
	inline static int32_t get_offset_of_m_PlayerEditorConnectionEvents_2() { return static_cast<int32_t>(offsetof(PlayerConnection_t3559457672, ___m_PlayerEditorConnectionEvents_2)); }
	inline PlayerEditorConnectionEvents_t2151463461 * get_m_PlayerEditorConnectionEvents_2() const { return ___m_PlayerEditorConnectionEvents_2; }
	inline PlayerEditorConnectionEvents_t2151463461 ** get_address_of_m_PlayerEditorConnectionEvents_2() { return &___m_PlayerEditorConnectionEvents_2; }
	inline void set_m_PlayerEditorConnectionEvents_2(PlayerEditorConnectionEvents_t2151463461 * value)
	{
		___m_PlayerEditorConnectionEvents_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_PlayerEditorConnectionEvents_2), value);
	}

	inline static int32_t get_offset_of_m_connectedPlayers_3() { return static_cast<int32_t>(offsetof(PlayerConnection_t3559457672, ___m_connectedPlayers_3)); }
	inline List_1_t449703515 * get_m_connectedPlayers_3() const { return ___m_connectedPlayers_3; }
	inline List_1_t449703515 ** get_address_of_m_connectedPlayers_3() { return &___m_connectedPlayers_3; }
	inline void set_m_connectedPlayers_3(List_1_t449703515 * value)
	{
		___m_connectedPlayers_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_connectedPlayers_3), value);
	}
};

struct PlayerConnection_t3559457672_StaticFields
{
public:
	// UnityEngine.Networking.PlayerConnection.PlayerConnection UnityEngine.Networking.PlayerConnection.PlayerConnection::s_Instance
	PlayerConnection_t3559457672 * ___s_Instance_4;

public:
	inline static int32_t get_offset_of_s_Instance_4() { return static_cast<int32_t>(offsetof(PlayerConnection_t3559457672_StaticFields, ___s_Instance_4)); }
	inline PlayerConnection_t3559457672 * get_s_Instance_4() const { return ___s_Instance_4; }
	inline PlayerConnection_t3559457672 ** get_address_of_s_Instance_4() { return &___s_Instance_4; }
	inline void set_s_Instance_4(PlayerConnection_t3559457672 * value)
	{
		___s_Instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERCONNECTION_T3559457672_H
#ifndef REAPPLYDRIVENPROPERTIES_T3880910420_H
#define REAPPLYDRIVENPROPERTIES_T3880910420_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RectTransform/ReapplyDrivenProperties
struct  ReapplyDrivenProperties_t3880910420  : public MulticastDelegate_t69367374
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REAPPLYDRIVENPROPERTIES_T3880910420_H
#ifndef RECTTRANSFORM_T3950565364_H
#define RECTTRANSFORM_T3950565364_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RectTransform
struct  RectTransform_t3950565364  : public Transform_t94239816
{
public:

public:
};

struct RectTransform_t3950565364_StaticFields
{
public:
	// UnityEngine.RectTransform/ReapplyDrivenProperties UnityEngine.RectTransform::reapplyDrivenProperties
	ReapplyDrivenProperties_t3880910420 * ___reapplyDrivenProperties_2;

public:
	inline static int32_t get_offset_of_reapplyDrivenProperties_2() { return static_cast<int32_t>(offsetof(RectTransform_t3950565364_StaticFields, ___reapplyDrivenProperties_2)); }
	inline ReapplyDrivenProperties_t3880910420 * get_reapplyDrivenProperties_2() const { return ___reapplyDrivenProperties_2; }
	inline ReapplyDrivenProperties_t3880910420 ** get_address_of_reapplyDrivenProperties_2() { return &___reapplyDrivenProperties_2; }
	inline void set_reapplyDrivenProperties_2(ReapplyDrivenProperties_t3880910420 * value)
	{
		___reapplyDrivenProperties_2 = value;
		Il2CppCodeGenWriteBarrier((&___reapplyDrivenProperties_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTTRANSFORM_T3950565364_H
#ifndef CAMERA_T101201881_H
#define CAMERA_T101201881_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Camera
struct  Camera_t101201881  : public Behaviour_t4110946901
{
public:

public:
};

struct Camera_t101201881_StaticFields
{
public:
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreCull
	CameraCallback_t2065492188 * ___onPreCull_2;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreRender
	CameraCallback_t2065492188 * ___onPreRender_3;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPostRender
	CameraCallback_t2065492188 * ___onPostRender_4;

public:
	inline static int32_t get_offset_of_onPreCull_2() { return static_cast<int32_t>(offsetof(Camera_t101201881_StaticFields, ___onPreCull_2)); }
	inline CameraCallback_t2065492188 * get_onPreCull_2() const { return ___onPreCull_2; }
	inline CameraCallback_t2065492188 ** get_address_of_onPreCull_2() { return &___onPreCull_2; }
	inline void set_onPreCull_2(CameraCallback_t2065492188 * value)
	{
		___onPreCull_2 = value;
		Il2CppCodeGenWriteBarrier((&___onPreCull_2), value);
	}

	inline static int32_t get_offset_of_onPreRender_3() { return static_cast<int32_t>(offsetof(Camera_t101201881_StaticFields, ___onPreRender_3)); }
	inline CameraCallback_t2065492188 * get_onPreRender_3() const { return ___onPreRender_3; }
	inline CameraCallback_t2065492188 ** get_address_of_onPreRender_3() { return &___onPreRender_3; }
	inline void set_onPreRender_3(CameraCallback_t2065492188 * value)
	{
		___onPreRender_3 = value;
		Il2CppCodeGenWriteBarrier((&___onPreRender_3), value);
	}

	inline static int32_t get_offset_of_onPostRender_4() { return static_cast<int32_t>(offsetof(Camera_t101201881_StaticFields, ___onPostRender_4)); }
	inline CameraCallback_t2065492188 * get_onPostRender_4() const { return ___onPostRender_4; }
	inline CameraCallback_t2065492188 ** get_address_of_onPostRender_4() { return &___onPostRender_4; }
	inline void set_onPostRender_4(CameraCallback_t2065492188 * value)
	{
		___onPostRender_4 = value;
		Il2CppCodeGenWriteBarrier((&___onPostRender_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERA_T101201881_H
#ifndef MONOBEHAVIOUR_T2904604993_H
#define MONOBEHAVIOUR_T2904604993_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t2904604993  : public Behaviour_t4110946901
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T2904604993_H
#ifndef GUIELEMENT_T1292848738_H
#define GUIELEMENT_T1292848738_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GUIElement
struct  GUIElement_t1292848738  : public Behaviour_t4110946901
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUIELEMENT_T1292848738_H
#ifndef GUILAYER_T1929752731_H
#define GUILAYER_T1929752731_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GUILayer
struct  GUILayer_t1929752731  : public Behaviour_t4110946901
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUILAYER_T1929752731_H
// UnityEngine.Keyframe[]
struct KeyframeU5BU5D_t1322505427  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Keyframe_t3396755990  m_Items[1];

public:
	inline Keyframe_t3396755990  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Keyframe_t3396755990 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Keyframe_t3396755990  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Keyframe_t3396755990  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Keyframe_t3396755990 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Keyframe_t3396755990  value)
	{
		m_Items[index] = value;
	}
};
// System.Object[]
struct ObjectU5BU5D_t4290686858  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject * m_Items[1];

public:
	inline RuntimeObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline RuntimeObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Type[]
struct TypeU5BU5D_t1830665827  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Type_t * m_Items[1];

public:
	inline Type_t * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Type_t ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Type_t * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Type_t * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Type_t ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Type_t * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.RequireComponent[]
struct RequireComponentU5BU5D_t2045948322  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RequireComponent_t597737523 * m_Items[1];

public:
	inline RequireComponent_t597737523 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RequireComponent_t597737523 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RequireComponent_t597737523 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline RequireComponent_t597737523 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RequireComponent_t597737523 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RequireComponent_t597737523 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.DisallowMultipleComponent[]
struct DisallowMultipleComponentU5BU5D_t773128126  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) DisallowMultipleComponent_t2957315911 * m_Items[1];

public:
	inline DisallowMultipleComponent_t2957315911 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline DisallowMultipleComponent_t2957315911 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, DisallowMultipleComponent_t2957315911 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline DisallowMultipleComponent_t2957315911 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline DisallowMultipleComponent_t2957315911 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, DisallowMultipleComponent_t2957315911 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.ExecuteInEditMode[]
struct ExecuteInEditModeU5BU5D_t2797163057  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) ExecuteInEditMode_t1836144016 * m_Items[1];

public:
	inline ExecuteInEditMode_t1836144016 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline ExecuteInEditMode_t1836144016 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, ExecuteInEditMode_t1836144016 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline ExecuteInEditMode_t1836144016 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline ExecuteInEditMode_t1836144016 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, ExecuteInEditMode_t1836144016 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.Camera[]
struct CameraU5BU5D_t2722699428  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Camera_t101201881 * m_Items[1];

public:
	inline Camera_t101201881 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Camera_t101201881 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Camera_t101201881 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Camera_t101201881 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Camera_t101201881 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Camera_t101201881 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.IntPtr[]
struct IntPtrU5BU5D_t2893013242  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) intptr_t m_Items[1];

public:
	inline intptr_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline intptr_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, intptr_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline intptr_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline intptr_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, intptr_t value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Display[]
struct DisplayU5BU5D_t130122569  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Display_t4002355288 * m_Items[1];

public:
	inline Display_t4002355288 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Display_t4002355288 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Display_t4002355288 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Display_t4002355288 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Display_t4002355288 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Display_t4002355288 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Reflection.ParameterInfo[]
struct ParameterInfoU5BU5D_t854811313  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) ParameterInfo_t3893964560 * m_Items[1];

public:
	inline ParameterInfo_t3893964560 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline ParameterInfo_t3893964560 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, ParameterInfo_t3893964560 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline ParameterInfo_t3893964560 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline ParameterInfo_t3893964560 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, ParameterInfo_t3893964560 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Reflection.ParameterModifier[]
struct ParameterModifierU5BU5D_t3522550039  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) ParameterModifier_t969812002  m_Items[1];

public:
	inline ParameterModifier_t969812002  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline ParameterModifier_t969812002 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, ParameterModifier_t969812002  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline ParameterModifier_t969812002  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline ParameterModifier_t969812002 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, ParameterModifier_t969812002  value)
	{
		m_Items[index] = value;
	}
};
// System.Byte[]
struct ByteU5BU5D_t1014014593  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) uint8_t m_Items[1];

public:
	inline uint8_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline uint8_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, uint8_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline uint8_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline uint8_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, uint8_t value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Playables.PlayableBinding[]
struct PlayableBindingU5BU5D_t207642577  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) PlayableBinding_t136667248  m_Items[1];

public:
	inline PlayableBinding_t136667248  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline PlayableBinding_t136667248 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, PlayableBinding_t136667248  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline PlayableBinding_t136667248  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline PlayableBinding_t136667248 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, PlayableBinding_t136667248  value)
	{
		m_Items[index] = value;
	}
};

extern "C" void Object_t2461733472_marshal_pinvoke(const Object_t2461733472& unmarshaled, Object_t2461733472_marshaled_pinvoke& marshaled);
extern "C" void Object_t2461733472_marshal_pinvoke_back(const Object_t2461733472_marshaled_pinvoke& marshaled, Object_t2461733472& unmarshaled);
extern "C" void Object_t2461733472_marshal_pinvoke_cleanup(Object_t2461733472_marshaled_pinvoke& marshaled);
extern "C" void Object_t2461733472_marshal_com(const Object_t2461733472& unmarshaled, Object_t2461733472_marshaled_com& marshaled);
extern "C" void Object_t2461733472_marshal_com_back(const Object_t2461733472_marshaled_com& marshaled, Object_t2461733472& unmarshaled);
extern "C" void Object_t2461733472_marshal_com_cleanup(Object_t2461733472_marshaled_com& marshaled);

// System.Void System.Action`1<System.Object>::Invoke(!0)
extern "C"  void Action_1_Invoke_m2462094957_gshared (Action_1_t2305831757 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Stack`1<System.Object>::.ctor()
extern "C"  void Stack_1__ctor_m2269989442_gshared (Stack_1_t3886787695 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Stack`1<System.Object>::Push(!0)
extern "C"  void Stack_1_Push_m453132609_gshared (Stack_1_t3886787695 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// !0 System.Collections.Generic.Stack`1<System.Object>::Pop()
extern "C"  RuntimeObject * Stack_1_Pop_m2727410536_gshared (Stack_1_t3886787695 * __this, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.Stack`1<System.Object>::get_Count()
extern "C"  int32_t Stack_1_get_Count_m1690191970_gshared (Stack_1_t3886787695 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
extern "C"  void List_1__ctor_m4124788479_gshared (List_1_t1174585319 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Add(!0)
extern "C"  void List_1_Add_m2394526366_gshared (List_1_t1174585319 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// !0[] System.Collections.Generic.List`1<System.Object>::ToArray()
extern "C"  ObjectU5BU5D_t4290686858* List_1_ToArray_m1772277260_gshared (List_1_t1174585319 * __this, const RuntimeMethod* method);
// T UnityEngine.AttributeHelperEngine::GetCustomAttributeOfType<System.Object>(System.Type)
extern "C"  RuntimeObject * AttributeHelperEngine_GetCustomAttributeOfType_TisRuntimeObject_m3097276615_gshared (RuntimeObject * __this /* static, unused */, Type_t * ___klass0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.IntPtr,System.Object>::TryGetValue(!0,!1&)
extern "C"  bool Dictionary_2_TryGetValue_m2089679366_gshared (Dictionary_2_t287235217 * __this, intptr_t p0, RuntimeObject ** p1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.IntPtr,System.Object>::.ctor()
extern "C"  void Dictionary_2__ctor_m3305660079_gshared (Dictionary_2_t287235217 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Clear()
extern "C"  void List_1_Clear_m2295507134_gshared (List_1_t1174585319 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::AddRange(System.Collections.Generic.IEnumerable`1<!0>)
extern "C"  void List_1_AddRange_m2667872413_gshared (List_1_t1174585319 * __this, RuntimeObject* p0, const RuntimeMethod* method);
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Single>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
extern "C"  void CachedInvokableCall_1__ctor_m2316491615_gshared (CachedInvokableCall_1_t1592702797 * __this, Object_t2461733472 * p0, MethodInfo_t * p1, float p2, const RuntimeMethod* method);
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Int32>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
extern "C"  void CachedInvokableCall_1__ctor_m245935429_gshared (CachedInvokableCall_1_t3666972114 * __this, Object_t2461733472 * p0, MethodInfo_t * p1, int32_t p2, const RuntimeMethod* method);
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Object>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
extern "C"  void CachedInvokableCall_1__ctor_m3882627318_gshared (CachedInvokableCall_1_t96886622 * __this, Object_t2461733472 * p0, MethodInfo_t * p1, RuntimeObject * p2, const RuntimeMethod* method);
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Boolean>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
extern "C"  void CachedInvokableCall_1__ctor_m498507028_gshared (CachedInvokableCall_1_t3155509814 * __this, Object_t2461733472 * p0, MethodInfo_t * p1, bool p2, const RuntimeMethod* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<System.Object>::GetEnumerator()
extern "C"  Enumerator_t2934400376  List_1_GetEnumerator_m859558683_gshared (List_1_t1174585319 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<System.Object>::get_Current()
extern "C"  RuntimeObject * Enumerator_get_Current_m3814319504_gshared (Enumerator_t2934400376 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3640007406_gshared (Enumerator_t2934400376 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m1212514242_gshared (Enumerator_t2934400376 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Int32>::.ctor()
extern "C"  void List_1__ctor_m2518762847_gshared (List_1_t449703515 * __this, const RuntimeMethod* method);
// T UnityEngine.ScriptableObject::CreateInstance<System.Object>()
extern "C"  RuntimeObject * ScriptableObject_CreateInstance_TisRuntimeObject_m3633959524_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Int32>::Add(!0)
extern "C"  void List_1_Add_m3295068314_gshared (List_1_t449703515 * __this, int32_t p0, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityEvent`1<System.Int32>::Invoke(T0)
extern "C"  void UnityEvent_1_Invoke_m4202823606_gshared (UnityEvent_1_t3088907843 * __this, int32_t p0, const RuntimeMethod* method);
// System.Void System.Func`2<System.Object,System.Boolean>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_2__ctor_m1821613634_gshared (Func_2_t1469056018 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method);
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Where<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
extern "C"  RuntimeObject* Enumerable_Where_TisRuntimeObject_m1858347226_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* p0, Func_2_t1469056018 * p1, const RuntimeMethod* method);
// System.Boolean System.Linq.Enumerable::Any<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C"  bool Enumerable_Any_TisRuntimeObject_m8374975_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* p0, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityEvent`1<System.Object>::Invoke(T0)
extern "C"  void UnityEvent_1_Invoke_m3395488391_gshared (UnityEvent_1_t3813789647 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityEvent`1<System.Int32>::.ctor()
extern "C"  void UnityEvent_1__ctor_m1279621320_gshared (UnityEvent_1_t3088907843 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityEvent`1<System.Object>::.ctor()
extern "C"  void UnityEvent_1__ctor_m3630106552_gshared (UnityEvent_1_t3813789647 * __this, const RuntimeMethod* method);

// System.Void System.Attribute::.ctor()
extern "C"  void Attribute__ctor_m2897993467 (Attribute_t1659210826 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Object::.ctor()
extern "C"  void Object__ctor_m3446193457 (RuntimeObject * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationCurve::Init(UnityEngine.Keyframe[])
extern "C"  void AnimationCurve_Init_m1956642396 (AnimationCurve_t1726276845 * __this, KeyframeU5BU5D_t1322505427* ___keys0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationCurve::Cleanup()
extern "C"  void AnimationCurve_Cleanup_m243787505 (AnimationCurve_t1726276845 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Object::Finalize()
extern "C"  void Object_Finalize_m1722074356 (RuntimeObject * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Application/LowMemoryCallback::Invoke()
extern "C"  void LowMemoryCallback_Invoke_m3732620697 (LowMemoryCallback_t454721290 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Application/LogCallback::Invoke(System.String,System.String,UnityEngine.LogType)
extern "C"  void LogCallback_Invoke_m2853261436 (LogCallback_t3381938013 * __this, String_t* ___condition0, String_t* ___stackTrace1, int32_t ___type2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.UnityAction::Invoke()
extern "C"  void UnityAction_Invoke_m2515763748 (UnityAction_t35329723 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AsyncOperation::InternalDestroy()
extern "C"  void AsyncOperation_InternalDestroy_m1747224711 (AsyncOperation_t4233810744 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Action`1<UnityEngine.AsyncOperation>::Invoke(!0)
#define Action_1_Invoke_m258234264(__this, p0, method) ((  void (*) (Action_1_t3243625498 *, AsyncOperation_t4233810744 *, const RuntimeMethod*))Action_1_Invoke_m2462094957_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.Stack`1<System.Type>::.ctor()
#define Stack_1__ctor_m3382330311(__this, method) ((  void (*) (Stack_1_t2864815562 *, const RuntimeMethod*))Stack_1__ctor_m2269989442_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<System.Type>::Push(!0)
#define Stack_1_Push_m2062010219(__this, p0, method) ((  void (*) (Stack_1_t2864815562 *, Type_t *, const RuntimeMethod*))Stack_1_Push_m453132609_gshared)(__this, p0, method)
// System.Type System.Type::GetTypeFromHandle(System.RuntimeTypeHandle)
extern "C"  Type_t * Type_GetTypeFromHandle_m3171413529 (RuntimeObject * __this /* static, unused */, RuntimeTypeHandle_t637624852  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !0 System.Collections.Generic.Stack`1<System.Type>::Pop()
#define Stack_1_Pop_m2423581918(__this, method) ((  Type_t * (*) (Stack_1_t2864815562 *, const RuntimeMethod*))Stack_1_Pop_m2727410536_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Stack`1<System.Type>::get_Count()
#define Stack_1_get_Count_m865256008(__this, method) ((  int32_t (*) (Stack_1_t2864815562 *, const RuntimeMethod*))Stack_1_get_Count_m1690191970_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Type>::.ctor()
#define List_1__ctor_m1793766529(__this, method) ((  void (*) (List_1_t152613186 *, const RuntimeMethod*))List_1__ctor_m4124788479_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Type>::Add(!0)
#define List_1_Add_m2364263606(__this, p0, method) ((  void (*) (List_1_t152613186 *, Type_t *, const RuntimeMethod*))List_1_Add_m2394526366_gshared)(__this, p0, method)
// !0[] System.Collections.Generic.List`1<System.Type>::ToArray()
#define List_1_ToArray_m289903370(__this, method) ((  TypeU5BU5D_t1830665827* (*) (List_1_t152613186 *, const RuntimeMethod*))List_1_ToArray_m1772277260_gshared)(__this, method)
// T UnityEngine.AttributeHelperEngine::GetCustomAttributeOfType<UnityEngine.DefaultExecutionOrder>(System.Type)
#define AttributeHelperEngine_GetCustomAttributeOfType_TisDefaultExecutionOrder_t3862449190_m4154997828(__this /* static, unused */, ___klass0, method) ((  DefaultExecutionOrder_t3862449190 * (*) (RuntimeObject * /* static, unused */, Type_t *, const RuntimeMethod*))AttributeHelperEngine_GetCustomAttributeOfType_TisRuntimeObject_m3097276615_gshared)(__this /* static, unused */, ___klass0, method)
// System.Int32 UnityEngine.DefaultExecutionOrder::get_order()
extern "C"  int32_t DefaultExecutionOrder_get_order_m3989798846 (DefaultExecutionOrder_t3862449190 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Component::.ctor()
extern "C"  void Component__ctor_m2592563656 (Component_t2160255836 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Camera::INTERNAL_get_pixelRect(UnityEngine.Rect&)
extern "C"  void Camera_INTERNAL_get_pixelRect_m3102036321 (Camera_t101201881 * __this, Rect_t1111852907 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Camera::INTERNAL_CALL_ScreenPointToRay(UnityEngine.Camera,UnityEngine.Vector3&,UnityEngine.Ray&)
extern "C"  void Camera_INTERNAL_CALL_ScreenPointToRay_m244366202 (RuntimeObject * __this /* static, unused */, Camera_t101201881 * ___self0, Vector3_t3239555143 * ___position1, Ray_t1113537957 * ___value2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Camera/CameraCallback::Invoke(UnityEngine.Camera)
extern "C"  void CameraCallback_Invoke_m144364913 (CameraCallback_t2065492188 * __this, Camera_t101201881 * ___cam0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.Camera::INTERNAL_CALL_RaycastTry(UnityEngine.Camera,UnityEngine.Ray&,System.Single,System.Int32)
extern "C"  GameObject_t3732643618 * Camera_INTERNAL_CALL_RaycastTry_m2903709223 (RuntimeObject * __this /* static, unused */, Camera_t101201881 * ___self0, Ray_t1113537957 * ___ray1, float ___distance2, int32_t ___layerMask3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.Camera::INTERNAL_CALL_RaycastTry2D(UnityEngine.Camera,UnityEngine.Ray&,System.Single,System.Int32)
extern "C"  GameObject_t3732643618 * Camera_INTERNAL_CALL_RaycastTry2D_m700194433 (RuntimeObject * __this /* static, unused */, Camera_t101201881 * ___self0, Ray_t1113537957 * ___ray1, float ___distance2, int32_t ___layerMask3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UnityLogWriter::Init()
extern "C"  void UnityLogWriter_Init_m3153067179 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Color::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Color__ctor_m211164015 (Color_t4024113822 * __this, float ___r0, float ___g1, float ___b2, float ___a3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.UnityString::Format(System.String,System.Object[])
extern "C"  String_t* UnityString_Format_m2513837565 (RuntimeObject * __this /* static, unused */, String_t* ___fmt0, ObjectU5BU5D_t4290686858* ___args1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Color::ToString()
extern "C"  String_t* Color_ToString_m4292820139 (Color_t4024113822 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector4 UnityEngine.Color::op_Implicit(UnityEngine.Color)
extern "C"  Vector4_t4052901136  Color_op_Implicit_m4266979643 (RuntimeObject * __this /* static, unused */, Color_t4024113822  ___c0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Vector4::GetHashCode()
extern "C"  int32_t Vector4_GetHashCode_m1136089151 (Vector4_t4052901136 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Color::GetHashCode()
extern "C"  int32_t Color_GetHashCode_m3278237850 (Color_t4024113822 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Single::Equals(System.Single)
extern "C"  bool Single_Equals_m3179646383 (float* __this, float p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Color::Equals(System.Object)
extern "C"  bool Color_Equals_m3350585184 (Color_t4024113822 * __this, RuntimeObject * ___other0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector4::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Vector4__ctor_m3054764420 (Vector4_t4052901136 * __this, float ___x0, float ___y1, float ___z2, float ___w3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Object::.ctor()
extern "C"  void Object__ctor_m387379410 (Object_t2461733472 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Coroutine::ReleaseCoroutine()
extern "C"  void Coroutine_ReleaseCoroutine_m329501834 (Coroutine_t3619068392 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.CSSLayout.CSSSize UnityEngine.CSSLayout.CSSMeasureFunc::Invoke(System.IntPtr,System.Single,UnityEngine.CSSLayout.CSSMeasureMode,System.Single,UnityEngine.CSSLayout.CSSMeasureMode)
extern "C"  CSSSize_t2436738465  CSSMeasureFunc_Invoke_m2998463313 (CSSMeasureFunc_t2162796896 * __this, intptr_t ___node0, float ___width1, int32_t ___widthMode2, float ___height3, int32_t ___heightMode4, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.Dictionary`2<System.IntPtr,System.WeakReference>::TryGetValue(!0,!1&)
#define Dictionary_2_TryGetValue_m3843825608(__this, p0, p1, method) ((  bool (*) (Dictionary_2_t990142682 *, intptr_t, WeakReference_t3998924468 **, const RuntimeMethod*))Dictionary_2_TryGetValue_m2089679366_gshared)(__this, p0, p1, method)
// UnityEngine.CSSLayout.CSSMeasureFunc UnityEngine.CSSLayout.Native::CSSNodeGetMeasureFunc(System.IntPtr)
extern "C"  CSSMeasureFunc_t2162796896 * Native_CSSNodeGetMeasureFunc_m4025476427 (RuntimeObject * __this /* static, unused */, intptr_t ___node0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void* System.IntPtr::op_Explicit(System.IntPtr)
extern "C"  void* IntPtr_op_Explicit_m3354255807 (RuntimeObject * __this /* static, unused */, intptr_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Dictionary`2<System.IntPtr,System.WeakReference>::.ctor()
#define Dictionary_2__ctor_m4269798443(__this, method) ((  void (*) (Dictionary_2_t990142682 *, const RuntimeMethod*))Dictionary_2__ctor_m3305660079_gshared)(__this, method)
// System.Boolean System.IntPtr::op_Inequality(System.IntPtr,System.IntPtr)
extern "C"  bool IntPtr_op_Inequality_m1374147269 (RuntimeObject * __this /* static, unused */, intptr_t p0, intptr_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.CullingGroup::FinalizerFailure()
extern "C"  void CullingGroup_FinalizerFailure_m3512716862 (CullingGroup_t1255328198 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void* System.IntPtr::ToPointer()
extern "C"  void* IntPtr_ToPointer_m565950407 (intptr_t* __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.CullingGroup/StateChanged::Invoke(UnityEngine.CullingGroupEvent)
extern "C"  void StateChanged_Invoke_m1291922880 (StateChanged_t2514413929 * __this, CullingGroupEvent_t157813866  ___sphere0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ILogger UnityEngine.Debug::get_unityLogger()
extern "C"  RuntimeObject* Debug_get_unityLogger_m2700972930 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.DebugLogHandler::.ctor()
extern "C"  void DebugLogHandler__ctor_m2673380417 (DebugLogHandler_t1401667417 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Logger::.ctor(UnityEngine.ILogHandler)
extern "C"  void Logger__ctor_m4234190541 (Logger_t605719344 * __this, RuntimeObject* ___logHandler0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Format(System.String,System.Object[])
extern "C"  String_t* String_Format_m2496254338 (RuntimeObject * __this /* static, unused */, String_t* p0, ObjectU5BU5D_t4290686858* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.DebugLogHandler::Internal_Log(UnityEngine.LogType,System.String,UnityEngine.Object)
extern "C"  void DebugLogHandler_Internal_Log_m1682417589 (RuntimeObject * __this /* static, unused */, int32_t ___level0, String_t* ___msg1, Object_t2461733472 * ___obj2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.DebugLogHandler::Internal_LogException(System.Exception,UnityEngine.Object)
extern "C"  void DebugLogHandler_Internal_LogException_m3264502143 (RuntimeObject * __this /* static, unused */, Exception_t3361176243 * ___exception0, Object_t2461733472 * ___obj1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.IntPtr::.ctor(System.Int32)
extern "C"  void IntPtr__ctor_m1874125153 (intptr_t* __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Display::.ctor(System.IntPtr)
extern "C"  void Display__ctor_m906575725 (Display_t4002355288 * __this, intptr_t ___nativeDisplay0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Display/DisplaysUpdatedDelegate::Invoke()
extern "C"  void DisplaysUpdatedDelegate_Invoke_m1559204117 (DisplaysUpdatedDelegate_t2805775822 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Display::.ctor()
extern "C"  void Display__ctor_m1199150772 (Display_t4002355288 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.String::IsNullOrEmpty(System.String)
extern "C"  bool String_IsNullOrEmpty_m3560539680 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.String::IndexOf(System.String)
extern "C"  int32_t String_IndexOf_m374129865 (String_t* __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Math::Min(System.Int32,System.Int32)
extern "C"  int32_t Math_Min_m1276480861 (RuntimeObject * __this /* static, unused */, int32_t p0, int32_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Substring(System.Int32,System.Int32)
extern "C"  String_t* String_Substring_m2404821311 (String_t* __this, int32_t p0, int32_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.String::EndsWith(System.String)
extern "C"  bool String_EndsWith_m2915171759 (String_t* __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.String,System.String)
extern "C"  String_t* String_Concat_m47853911 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.ArgumentCache::TidyAssemblyTypeName()
extern "C"  void ArgumentCache_TidyAssemblyTypeName_m2531759392 (ArgumentCache_t3682645796 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.ArgumentNullException::.ctor(System.String)
extern "C"  void ArgumentNullException__ctor_m2607242403 (ArgumentNullException_t517973450 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Object System.Delegate::get_Target()
extern "C"  RuntimeObject * Delegate_get_Target_m1823718869 (Delegate_t96267039 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Object::ReferenceEquals(System.Object,System.Object)
extern "C"  bool Object_ReferenceEquals_m1923645570 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, RuntimeObject * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Inequality_m3819358377 (RuntimeObject * __this /* static, unused */, Object_t2461733472 * ___x0, Object_t2461733472 * ___y1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.BaseInvokableCall::.ctor(System.Object,System.Reflection.MethodInfo)
extern "C"  void BaseInvokableCall__ctor_m3419792134 (BaseInvokableCall_t2920707967 * __this, RuntimeObject * ___target0, MethodInfo_t * ___function1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Delegate UnityEngineInternal.NetFxCoreExtensions::CreateDelegate(System.Reflection.MethodInfo,System.Type,System.Object)
extern "C"  Delegate_t96267039 * NetFxCoreExtensions_CreateDelegate_m3544893108 (RuntimeObject * __this /* static, unused */, MethodInfo_t * ___self0, Type_t * ___delegateType1, RuntimeObject * ___target2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.InvokableCall::add_Delegate(UnityEngine.Events.UnityAction)
extern "C"  void InvokableCall_add_Delegate_m2772852427 (InvokableCall_t2876658856 * __this, UnityAction_t35329723 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Delegate System.Delegate::Combine(System.Delegate,System.Delegate)
extern "C"  Delegate_t96267039 * Delegate_Combine_m1239998138 (RuntimeObject * __this /* static, unused */, Delegate_t96267039 * p0, Delegate_t96267039 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Delegate System.Delegate::Remove(System.Delegate,System.Delegate)
extern "C"  Delegate_t96267039 * Delegate_Remove_m1542158 (RuntimeObject * __this /* static, unused */, Delegate_t96267039 * p0, Delegate_t96267039 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Events.BaseInvokableCall::AllowInvoke(System.Delegate)
extern "C"  bool BaseInvokableCall_AllowInvoke_m1751538175 (RuntimeObject * __this /* static, unused */, Delegate_t96267039 * ___delegate0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::.ctor()
#define List_1__ctor_m2027075001(__this, method) ((  void (*) (List_1_t799276283 *, const RuntimeMethod*))List_1__ctor_m4124788479_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::Add(!0)
#define List_1_Add_m3979596576(__this, p0, method) ((  void (*) (List_1_t799276283 *, BaseInvokableCall_t2920707967 *, const RuntimeMethod*))List_1_Add_m2394526366_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::Clear()
#define List_1_Clear_m550264580(__this, method) ((  void (*) (List_1_t799276283 *, const RuntimeMethod*))List_1_Clear_m2295507134_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::AddRange(System.Collections.Generic.IEnumerable`1<!0>)
#define List_1_AddRange_m3917463589(__this, p0, method) ((  void (*) (List_1_t799276283 *, RuntimeObject*, const RuntimeMethod*))List_1_AddRange_m2667872413_gshared)(__this, p0, method)
// System.Void UnityEngine.Events.ArgumentCache::.ctor()
extern "C"  void ArgumentCache__ctor_m2562784622 (ArgumentCache_t3682645796 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object UnityEngine.Events.PersistentCall::get_target()
extern "C"  Object_t2461733472 * PersistentCall_get_target_m4082064461 (PersistentCall_t2687057912 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Events.PersistentCall::get_methodName()
extern "C"  String_t* PersistentCall_get_methodName_m2687182531 (PersistentCall_t2687057912 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo UnityEngine.Events.UnityEventBase::FindMethod(UnityEngine.Events.PersistentCall)
extern "C"  MethodInfo_t * UnityEventBase_FindMethod_m595753448 (UnityEventBase_t3171537072 * __this, PersistentCall_t2687057912 * ___call0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.PersistentCall::GetObjectCall(UnityEngine.Object,System.Reflection.MethodInfo,UnityEngine.Events.ArgumentCache)
extern "C"  BaseInvokableCall_t2920707967 * PersistentCall_GetObjectCall_m2893916170 (RuntimeObject * __this /* static, unused */, Object_t2461733472 * ___target0, MethodInfo_t * ___method1, ArgumentCache_t3682645796 * ___arguments2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Events.ArgumentCache::get_floatArgument()
extern "C"  float ArgumentCache_get_floatArgument_m703828235 (ArgumentCache_t3682645796 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Single>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
#define CachedInvokableCall_1__ctor_m2316491615(__this, p0, p1, p2, method) ((  void (*) (CachedInvokableCall_1_t1592702797 *, Object_t2461733472 *, MethodInfo_t *, float, const RuntimeMethod*))CachedInvokableCall_1__ctor_m2316491615_gshared)(__this, p0, p1, p2, method)
// System.Int32 UnityEngine.Events.ArgumentCache::get_intArgument()
extern "C"  int32_t ArgumentCache_get_intArgument_m1266469321 (ArgumentCache_t3682645796 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Int32>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
#define CachedInvokableCall_1__ctor_m245935429(__this, p0, p1, p2, method) ((  void (*) (CachedInvokableCall_1_t3666972114 *, Object_t2461733472 *, MethodInfo_t *, int32_t, const RuntimeMethod*))CachedInvokableCall_1__ctor_m245935429_gshared)(__this, p0, p1, p2, method)
// System.String UnityEngine.Events.ArgumentCache::get_stringArgument()
extern "C"  String_t* ArgumentCache_get_stringArgument_m574520374 (ArgumentCache_t3682645796 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.String>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
#define CachedInvokableCall_1__ctor_m2544757654(__this, p0, p1, p2, method) ((  void (*) (CachedInvokableCall_1_t4079887551 *, Object_t2461733472 *, MethodInfo_t *, String_t*, const RuntimeMethod*))CachedInvokableCall_1__ctor_m3882627318_gshared)(__this, p0, p1, p2, method)
// System.Boolean UnityEngine.Events.ArgumentCache::get_boolArgument()
extern "C"  bool ArgumentCache_get_boolArgument_m600683273 (ArgumentCache_t3682645796 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Boolean>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
#define CachedInvokableCall_1__ctor_m498507028(__this, p0, p1, p2, method) ((  void (*) (CachedInvokableCall_1_t3155509814 *, Object_t2461733472 *, MethodInfo_t *, bool, const RuntimeMethod*))CachedInvokableCall_1__ctor_m498507028_gshared)(__this, p0, p1, p2, method)
// System.Void UnityEngine.Events.InvokableCall::.ctor(System.Object,System.Reflection.MethodInfo)
extern "C"  void InvokableCall__ctor_m2974797033 (InvokableCall_t2876658856 * __this, RuntimeObject * ___target0, MethodInfo_t * ___theFunction1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Events.ArgumentCache::get_unityObjectArgumentAssemblyTypeName()
extern "C"  String_t* ArgumentCache_get_unityObjectArgumentAssemblyTypeName_m3489746291 (ArgumentCache_t3682645796 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Type System.Type::GetType(System.String,System.Boolean)
extern "C"  Type_t * Type_GetType_m418477468 (RuntimeObject * __this /* static, unused */, String_t* p0, bool p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Reflection.ConstructorInfo System.Type::GetConstructor(System.Type[])
extern "C"  ConstructorInfo_t3360520918 * Type_GetConstructor_m272770726 (Type_t * __this, TypeU5BU5D_t1830665827* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object UnityEngine.Events.ArgumentCache::get_unityObjectArgument()
extern "C"  Object_t2461733472 * ArgumentCache_get_unityObjectArgument_m2559081415 (ArgumentCache_t3682645796 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Type System.Object::GetType()
extern "C"  Type_t * Object_GetType_m4087638284 (RuntimeObject * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Object System.Reflection.ConstructorInfo::Invoke(System.Object[])
extern "C"  RuntimeObject * ConstructorInfo_Invoke_m4178483106 (ConstructorInfo_t3360520918 * __this, ObjectU5BU5D_t4290686858* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>::.ctor()
#define List_1__ctor_m43236579(__this, method) ((  void (*) (List_1_t565626228 *, const RuntimeMethod*))List_1__ctor_m4124788479_gshared)(__this, method)
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>::GetEnumerator()
#define List_1_GetEnumerator_m1074128493(__this, method) ((  Enumerator_t2325441285  (*) (List_1_t565626228 *, const RuntimeMethod*))List_1_GetEnumerator_m859558683_gshared)(__this, method)
// !0 System.Collections.Generic.List`1/Enumerator<UnityEngine.Events.PersistentCall>::get_Current()
#define Enumerator_get_Current_m4156657100(__this, method) ((  PersistentCall_t2687057912 * (*) (Enumerator_t2325441285 *, const RuntimeMethod*))Enumerator_get_Current_m3814319504_gshared)(__this, method)
// System.Boolean UnityEngine.Events.PersistentCall::IsValid()
extern "C"  bool PersistentCall_IsValid_m2172430097 (PersistentCall_t2687057912 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.PersistentCall::GetRuntimeCall(UnityEngine.Events.UnityEventBase)
extern "C"  BaseInvokableCall_t2920707967 * PersistentCall_GetRuntimeCall_m1057898321 (PersistentCall_t2687057912 * __this, UnityEventBase_t3171537072 * ___theEvent0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.InvokableCallList::AddPersistentInvokableCall(UnityEngine.Events.BaseInvokableCall)
extern "C"  void InvokableCallList_AddPersistentInvokableCall_m3306354928 (InvokableCallList_t1673367030 * __this, BaseInvokableCall_t2920707967 * ___call0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Events.PersistentCall>::MoveNext()
#define Enumerator_MoveNext_m2090417815(__this, method) ((  bool (*) (Enumerator_t2325441285 *, const RuntimeMethod*))Enumerator_MoveNext_m3640007406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Events.PersistentCall>::Dispose()
#define Enumerator_Dispose_m2940250647(__this, method) ((  void (*) (Enumerator_t2325441285 *, const RuntimeMethod*))Enumerator_Dispose_m1212514242_gshared)(__this, method)
// System.Void UnityEngine.Events.UnityEventBase::.ctor()
extern "C"  void UnityEventBase__ctor_m1419821417 (UnityEventBase_t3171537072 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo UnityEngine.Events.UnityEventBase::GetValidMethodInfo(System.Object,System.String,System.Type[])
extern "C"  MethodInfo_t * UnityEventBase_GetValidMethodInfo_m1004474682 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___obj0, String_t* ___functionName1, TypeU5BU5D_t1830665827* ___argumentTypes2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.InvokableCallList::.ctor()
extern "C"  void InvokableCallList__ctor_m386930243 (InvokableCallList_t1673367030 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.PersistentCallGroup::.ctor()
extern "C"  void PersistentCallGroup__ctor_m2062212555 (PersistentCallGroup_t586105043 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.UnityEventBase::DirtyPersistentCalls()
extern "C"  void UnityEventBase_DirtyPersistentCalls_m1507352076 (UnityEventBase_t3171537072 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Events.ArgumentCache UnityEngine.Events.PersistentCall::get_arguments()
extern "C"  ArgumentCache_t3682645796 * PersistentCall_get_arguments_m1844117721 (PersistentCall_t2687057912 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Events.PersistentListenerMode UnityEngine.Events.PersistentCall::get_mode()
extern "C"  int32_t PersistentCall_get_mode_m2357383679 (PersistentCall_t2687057912 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo UnityEngine.Events.UnityEventBase::FindMethod(System.String,System.Object,UnityEngine.Events.PersistentListenerMode,System.Type)
extern "C"  MethodInfo_t * UnityEventBase_FindMethod_m3931843611 (UnityEventBase_t3171537072 * __this, String_t* ___name0, RuntimeObject * ___listener1, int32_t ___mode2, Type_t * ___argumentType3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.InvokableCallList::ClearPersistent()
extern "C"  void InvokableCallList_ClearPersistent_m1797712781 (InvokableCallList_t1673367030 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.PersistentCallGroup::Initialize(UnityEngine.Events.InvokableCallList,UnityEngine.Events.UnityEventBase)
extern "C"  void PersistentCallGroup_Initialize_m644878480 (PersistentCallGroup_t586105043 * __this, InvokableCallList_t1673367030 * ___invokableList0, UnityEventBase_t3171537072 * ___unityEventBase1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.UnityEventBase::RebuildPersistentCallsIfNeeded()
extern "C"  void UnityEventBase_RebuildPersistentCallsIfNeeded_m256317675 (UnityEventBase_t3171537072 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall> UnityEngine.Events.InvokableCallList::PrepareInvoke()
extern "C"  List_1_t799276283 * InvokableCallList_PrepareInvoke_m2613780459 (InvokableCallList_t1673367030 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.Object::ToString()
extern "C"  String_t* Object_ToString_m1080586622 (RuntimeObject * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.String,System.String,System.String)
extern "C"  String_t* String_Concat_m4209604784 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, String_t* p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo System.Type::GetMethod(System.String,System.Reflection.BindingFlags,System.Reflection.Binder,System.Type[],System.Reflection.ParameterModifier[])
extern "C"  MethodInfo_t * Type_GetMethod_m3484360104 (Type_t * __this, String_t* p0, int32_t p1, Binder_t2856902408 * p2, TypeU5BU5D_t1830665827* p3, ParameterModifierU5BU5D_t3522550039* p4, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Type::get_IsPrimitive()
extern "C"  bool Type_get_IsPrimitive_m3385012571 (Type_t * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Experimental.Rendering.RenderPipelineManager::set_currentPipeline(UnityEngine.Experimental.Rendering.IRenderPipeline)
extern "C"  void RenderPipelineManager_set_currentPipeline_m3602131277 (RuntimeObject * __this /* static, unused */, RuntimeObject* ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Experimental.Rendering.RenderPipelineManager::PrepareRenderPipeline(UnityEngine.Experimental.Rendering.IRenderPipelineAsset)
extern "C"  void RenderPipelineManager_PrepareRenderPipeline_m896043942 (RuntimeObject * __this /* static, unused */, RuntimeObject* ___pipe0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Experimental.Rendering.IRenderPipeline UnityEngine.Experimental.Rendering.RenderPipelineManager::get_currentPipeline()
extern "C"  RuntimeObject* RenderPipelineManager_get_currentPipeline_m3103495424 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Experimental.Rendering.ScriptableRenderContext::.ctor(System.IntPtr)
extern "C"  void ScriptableRenderContext__ctor_m242682668 (ScriptableRenderContext_t1486166028 * __this, intptr_t ___ptr0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Experimental.Rendering.RenderPipelineManager::CleanupRenderPipeline()
extern "C"  void RenderPipelineManager_CleanupRenderPipeline_m309528094 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GameObject::Internal_CreateGameObject(UnityEngine.GameObject,System.String)
extern "C"  void GameObject_Internal_CreateGameObject_m1823697062 (RuntimeObject * __this /* static, unused */, GameObject_t3732643618 * ___mono0, String_t* ___name1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gradient::Init()
extern "C"  void Gradient_Init_m1188969969 (Gradient_t130030130 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gradient::Cleanup()
extern "C"  void Gradient_Cleanup_m2195794132 (Gradient_t130030130 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Behaviour::.ctor()
extern "C"  void Behaviour__ctor_m1233868026 (Behaviour_t4110946901 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GUIElement UnityEngine.GUILayer::INTERNAL_CALL_HitTest(UnityEngine.GUILayer,UnityEngine.Vector3&)
extern "C"  GUIElement_t1292848738 * GUILayer_INTERNAL_CALL_HitTest_m422396558 (RuntimeObject * __this /* static, unused */, GUILayer_t1929752731 * ___self0, Vector3_t3239555143 * ___screenPosition1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Input::INTERNAL_get_mousePosition(UnityEngine.Vector3&)
extern "C"  void Input_INTERNAL_get_mousePosition_m3613716531 (RuntimeObject * __this /* static, unused */, Vector3_t3239555143 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine.Internal.DefaultValueAttribute::get_Value()
extern "C"  RuntimeObject * DefaultValueAttribute_get_Value_m1203122007 (DefaultValueAttribute_t2693326634 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Attribute::GetHashCode()
extern "C"  int32_t Attribute_GetHashCode_m2188337821 (Attribute_t1659210826 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.iOS.LocalNotification::Destroy()
extern "C"  void LocalNotification_Destroy_m1982161855 (LocalNotification_t2133447199 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.DateTime::.ctor(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.DateTimeKind)
extern "C"  void DateTime__ctor_m3814649173 (DateTime_t507798353 * __this, int32_t p0, int32_t p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, int32_t p6, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.DateTime::get_Ticks()
extern "C"  int64_t DateTime_get_Ticks_m3634951895 (DateTime_t507798353 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.iOS.RemoteNotification::Destroy()
extern "C"  void RemoteNotification_Destroy_m4186758914 (RemoteNotification_t414806740 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Logger::set_logHandler(UnityEngine.ILogHandler)
extern "C"  void Logger_set_logHandler_m6066634 (Logger_t605719344 * __this, RuntimeObject* ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Logger::set_logEnabled(System.Boolean)
extern "C"  void Logger_set_logEnabled_m1662937540 (Logger_t605719344 * __this, bool ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Logger::set_filterLogType(UnityEngine.LogType)
extern "C"  void Logger_set_filterLogType_m2745985065 (Logger_t605719344 * __this, int32_t ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Logger::get_logEnabled()
extern "C"  bool Logger_get_logEnabled_m2290808316 (Logger_t605719344 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.LogType UnityEngine.Logger::get_filterLogType()
extern "C"  int32_t Logger_get_filterLogType_m426955796 (Logger_t605719344 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Logger::IsLogTypeAllowed(UnityEngine.LogType)
extern "C"  bool Logger_IsLogTypeAllowed_m1817789334 (Logger_t605719344 * __this, int32_t ___logType0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ILogHandler UnityEngine.Logger::get_logHandler()
extern "C"  RuntimeObject* Logger_get_logHandler_m1719487699 (Logger_t605719344 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Logger::GetString(System.Object)
extern "C"  String_t* Logger_GetString_m2492438982 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___message0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.ArgumentNullException::.ctor(System.String,System.String)
extern "C"  void ArgumentNullException__ctor_m532419484 (ArgumentNullException_t517973450 * __this, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.ArgumentException::.ctor(System.String,System.String)
extern "C"  void ArgumentException__ctor_m2239500235 (ArgumentException_t1372035057 * __this, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.IntPtr::op_Equality(System.IntPtr,System.IntPtr)
extern "C"  bool IntPtr_op_Equality_m3978210203 (RuntimeObject * __this /* static, unused */, intptr_t p0, intptr_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ManagedStreamHelpers::ValidateLoadFromStream(System.IO.Stream)
extern "C"  void ManagedStreamHelpers_ValidateLoadFromStream_m2537091486 (RuntimeObject * __this /* static, unused */, Stream_t2349536632 * ___stream0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Mathf::Max(System.Single,System.Single)
extern "C"  float Mathf_Max_m3084430723 (RuntimeObject * __this /* static, unused */, float ___a0, float ___b1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Matrix4x4::.ctor(UnityEngine.Vector4,UnityEngine.Vector4,UnityEngine.Vector4,UnityEngine.Vector4)
extern "C"  void Matrix4x4__ctor_m614016869 (Matrix4x4_t3534180298 * __this, Vector4_t4052901136  ___column00, Vector4_t4052901136  ___column11, Vector4_t4052901136  ___column22, Vector4_t4052901136  ___column33, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector4 UnityEngine.Matrix4x4::GetColumn(System.Int32)
extern "C"  Vector4_t4052901136  Matrix4x4_GetColumn_m1315812967 (Matrix4x4_t3534180298 * __this, int32_t ___index0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Matrix4x4::GetHashCode()
extern "C"  int32_t Matrix4x4_GetHashCode_m2086764285 (Matrix4x4_t3534180298 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Vector4::Equals(System.Object)
extern "C"  bool Vector4_Equals_m480591454 (Vector4_t4052901136 * __this, RuntimeObject * ___other0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Matrix4x4::Equals(System.Object)
extern "C"  bool Matrix4x4_Equals_m1131202087 (Matrix4x4_t3534180298 * __this, RuntimeObject * ___other0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.IndexOutOfRangeException::.ctor(System.String)
extern "C"  void IndexOutOfRangeException__ctor_m2238521308 (IndexOutOfRangeException_t3041194587 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Matrix4x4::ToString()
extern "C"  String_t* Matrix4x4_ToString_m861927258 (Matrix4x4_t3534180298 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.NativeClassAttribute::set_QualifiedNativeName(System.String)
extern "C"  void NativeClassAttribute_set_QualifiedNativeName_m875392448 (NativeClassAttribute_t3085471410 * __this, String_t* ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents::.ctor()
extern "C"  void PlayerEditorConnectionEvents__ctor_m1455086094 (PlayerEditorConnectionEvents_t2151463461 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<System.Int32>::.ctor()
#define List_1__ctor_m2518762847(__this, method) ((  void (*) (List_1_t449703515 *, const RuntimeMethod*))List_1__ctor_m2518762847_gshared)(__this, method)
// System.Void UnityEngine.ScriptableObject::.ctor()
extern "C"  void ScriptableObject__ctor_m3589061004 (ScriptableObject_t476655885 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Equality_m2743582905 (RuntimeObject * __this /* static, unused */, Object_t2461733472 * ___x0, Object_t2461733472 * ___y1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Networking.PlayerConnection.PlayerConnection UnityEngine.Networking.PlayerConnection.PlayerConnection::CreateInstance()
extern "C"  PlayerConnection_t3559457672 * PlayerConnection_CreateInstance_m4274468596 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// T UnityEngine.ScriptableObject::CreateInstance<UnityEngine.Networking.PlayerConnection.PlayerConnection>()
#define ScriptableObject_CreateInstance_TisPlayerConnection_t3559457672_m1179650981(__this /* static, unused */, method) ((  PlayerConnection_t3559457672 * (*) (RuntimeObject * /* static, unused */, const RuntimeMethod*))ScriptableObject_CreateInstance_TisRuntimeObject_m3633959524_gshared)(__this /* static, unused */, method)
// System.Void UnityEngine.Object::set_hideFlags(UnityEngine.HideFlags)
extern "C"  void Object_set_hideFlags_m3010783544 (Object_t2461733472 * __this, int32_t ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.Marshal::Copy(System.IntPtr,System.Byte[],System.Int32,System.Int32)
extern "C"  void Marshal_Copy_m3898306904 (RuntimeObject * __this /* static, unused */, intptr_t p0, ByteU5BU5D_t1014014593* p1, int32_t p2, int32_t p3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Networking.PlayerConnection.PlayerConnection UnityEngine.Networking.PlayerConnection.PlayerConnection::get_instance()
extern "C"  PlayerConnection_t3559457672 * PlayerConnection_get_instance_m2190863265 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Guid::.ctor(System.String)
extern "C"  void Guid__ctor_m1415939458 (Guid_t * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents::InvokeMessageIdSubscribers(System.Guid,System.Byte[],System.Int32)
extern "C"  void PlayerEditorConnectionEvents_InvokeMessageIdSubscribers_m2032614817 (PlayerEditorConnectionEvents_t2151463461 * __this, Guid_t  ___messageId0, ByteU5BU5D_t1014014593* ___data1, int32_t ___playerId2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<System.Int32>::Add(!0)
#define List_1_Add_m3295068314(__this, p0, method) ((  void (*) (List_1_t449703515 *, int32_t, const RuntimeMethod*))List_1_Add_m3295068314_gshared)(__this, p0, method)
// System.Void UnityEngine.Events.UnityEvent`1<System.Int32>::Invoke(T0)
#define UnityEvent_1_Invoke_m4202823606(__this, p0, method) ((  void (*) (UnityEvent_1_t3088907843 *, int32_t, const RuntimeMethod*))UnityEvent_1_Invoke_m4202823606_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers>::.ctor()
#define List_1__ctor_m539526798(__this, method) ((  void (*) (List_1_t3831528118 *, const RuntimeMethod*))List_1__ctor_m4124788479_gshared)(__this, method)
// System.Void UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/ConnectionChangeEvent::.ctor()
extern "C"  void ConnectionChangeEvent__ctor_m1785426546 (ConnectionChangeEvent_t2308234365 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/<InvokeMessageIdSubscribers>c__AnonStorey0::.ctor()
extern "C"  void U3CInvokeMessageIdSubscribersU3Ec__AnonStorey0__ctor_m1102967907 (U3CInvokeMessageIdSubscribersU3Ec__AnonStorey0_t1928087868 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Func`2<UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers,System.Boolean>::.ctor(System.Object,System.IntPtr)
#define Func_2__ctor_m1624349331(__this, p0, p1, method) ((  void (*) (Func_2_t874238247 *, RuntimeObject *, intptr_t, const RuntimeMethod*))Func_2__ctor_m1821613634_gshared)(__this, p0, p1, method)
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Where<UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
#define Enumerable_Where_TisMessageTypeSubscribers_t1657992506_m190799551(__this /* static, unused */, p0, p1, method) ((  RuntimeObject* (*) (RuntimeObject * /* static, unused */, RuntimeObject*, Func_2_t874238247 *, const RuntimeMethod*))Enumerable_Where_TisRuntimeObject_m1858347226_gshared)(__this /* static, unused */, p0, p1, method)
// System.Boolean System.Linq.Enumerable::Any<UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers>(System.Collections.Generic.IEnumerable`1<!!0>)
#define Enumerable_Any_TisMessageTypeSubscribers_t1657992506_m1081467775(__this /* static, unused */, p0, method) ((  bool (*) (RuntimeObject * /* static, unused */, RuntimeObject*, const RuntimeMethod*))Enumerable_Any_TisRuntimeObject_m8374975_gshared)(__this /* static, unused */, p0, method)
// System.String System.String::Concat(System.Object,System.Object)
extern "C"  String_t* String_Concat_m2669786253 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, RuntimeObject * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Debug::LogError(System.Object)
extern "C"  void Debug_LogError_m1070074371 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___message0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.PlayerConnection.MessageEventArgs::.ctor()
extern "C"  void MessageEventArgs__ctor_m1689534235 (MessageEventArgs_t947408028 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.UnityEvent`1<UnityEngine.Networking.PlayerConnection.MessageEventArgs>::Invoke(T0)
#define UnityEvent_1_Invoke_m523984493(__this, p0, method) ((  void (*) (UnityEvent_1_t1465180672 *, MessageEventArgs_t947408028 *, const RuntimeMethod*))UnityEvent_1_Invoke_m3395488391_gshared)(__this, p0, method)
// System.Guid UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers::get_MessageTypeId()
extern "C"  Guid_t  MessageTypeSubscribers_get_MessageTypeId_m2156269021 (MessageTypeSubscribers_t1657992506 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Guid::op_Equality(System.Guid,System.Guid)
extern "C"  bool Guid_op_Equality_m3918449706 (RuntimeObject * __this /* static, unused */, Guid_t  p0, Guid_t  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.UnityEvent`1<System.Int32>::.ctor()
#define UnityEvent_1__ctor_m1279621320(__this, method) ((  void (*) (UnityEvent_1_t3088907843 *, const RuntimeMethod*))UnityEvent_1__ctor_m1279621320_gshared)(__this, method)
// System.Void UnityEngine.Events.UnityEvent`1<UnityEngine.Networking.PlayerConnection.MessageEventArgs>::.ctor()
#define UnityEvent_1__ctor_m183532629(__this, method) ((  void (*) (UnityEvent_1_t1465180672 *, const RuntimeMethod*))UnityEvent_1__ctor_m3630106552_gshared)(__this, method)
// System.Void UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageEvent::.ctor()
extern "C"  void MessageEvent__ctor_m3751876958 (MessageEvent_t2016149491 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Object::GetHashCode()
extern "C"  int32_t Object_GetHashCode_m1725373185 (RuntimeObject * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::CompareBaseObjects(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_CompareBaseObjects_m3289384931 (RuntimeObject * __this /* static, unused */, Object_t2461733472 * ___lhs0, Object_t2461733472 * ___rhs1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::IsNativeObjectAlive(UnityEngine.Object)
extern "C"  bool Object_IsNativeObjectAlive_m2916611270 (RuntimeObject * __this /* static, unused */, Object_t2461733472 * ___o0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.IntPtr UnityEngine.Object::GetCachedPtr()
extern "C"  intptr_t Object_GetCachedPtr_m2018497938 (Object_t2461733472 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Playables.Playable::.ctor(UnityEngine.Playables.PlayableHandle)
extern "C"  void Playable__ctor_m3942516336 (Playable_t1760619938 * __this, PlayableHandle_t3938927722  ___handle0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Playables.PlayableHandle UnityEngine.Playables.Playable::GetHandle()
extern "C"  PlayableHandle_t3938927722  Playable_GetHandle_m2973432975 (Playable_t1760619938 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Playables.PlayableHandle::op_Equality(UnityEngine.Playables.PlayableHandle,UnityEngine.Playables.PlayableHandle)
extern "C"  bool PlayableHandle_op_Equality_m2397680455 (RuntimeObject * __this /* static, unused */, PlayableHandle_t3938927722  ___x0, PlayableHandle_t3938927722  ___y1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Playables.Playable::Equals(UnityEngine.Playables.Playable)
extern "C"  bool Playable_Equals_m1196118830 (Playable_t1760619938 * __this, Playable_t1760619938  ___other0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Playables.PlayableHandle UnityEngine.Playables.PlayableHandle::get_Null()
extern "C"  PlayableHandle_t3938927722  PlayableHandle_get_Null_m2516265857 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Playables.Playable UnityEngine.Playables.Playable::get_Null()
extern "C"  Playable_t1760619938  Playable_get_Null_m1034340665 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Object System.Object::MemberwiseClone()
extern "C"  RuntimeObject * Object_MemberwiseClone_m1134924534 (RuntimeObject * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Playables.PlayableHandle::CompareVersion(UnityEngine.Playables.PlayableHandle,UnityEngine.Playables.PlayableHandle)
extern "C"  bool PlayableHandle_CompareVersion_m2720829020 (RuntimeObject * __this /* static, unused */, PlayableHandle_t3938927722  ___lhs0, PlayableHandle_t3938927722  ___rhs1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Playables.PlayableHandle::Equals(System.Object)
extern "C"  bool PlayableHandle_Equals_m771590314 (PlayableHandle_t3938927722 * __this, RuntimeObject * ___p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.IntPtr::GetHashCode()
extern "C"  int32_t IntPtr_GetHashCode_m2382536356 (intptr_t* __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Int32::GetHashCode()
extern "C"  int32_t Int32_GetHashCode_m339244912 (int32_t* __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Playables.PlayableHandle::GetHashCode()
extern "C"  int32_t PlayableHandle_GetHashCode_m1922874376 (PlayableHandle_t3938927722 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Playables.PlayableOutput::.ctor(UnityEngine.Playables.PlayableOutputHandle)
extern "C"  void PlayableOutput__ctor_m451176029 (PlayableOutput_t3840687608 * __this, PlayableOutputHandle_t2719521870  ___handle0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Playables.PlayableOutputHandle UnityEngine.Playables.PlayableOutput::GetHandle()
extern "C"  PlayableOutputHandle_t2719521870  PlayableOutput_GetHandle_m1350475571 (PlayableOutput_t3840687608 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Playables.PlayableOutputHandle::op_Equality(UnityEngine.Playables.PlayableOutputHandle,UnityEngine.Playables.PlayableOutputHandle)
extern "C"  bool PlayableOutputHandle_op_Equality_m1435240927 (RuntimeObject * __this /* static, unused */, PlayableOutputHandle_t2719521870  ___lhs0, PlayableOutputHandle_t2719521870  ___rhs1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Playables.PlayableOutput::Equals(UnityEngine.Playables.PlayableOutput)
extern "C"  bool PlayableOutput_Equals_m2076999710 (PlayableOutput_t3840687608 * __this, PlayableOutput_t3840687608  ___other0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Playables.PlayableOutputHandle UnityEngine.Playables.PlayableOutputHandle::get_Null()
extern "C"  PlayableOutputHandle_t2719521870  PlayableOutputHandle_get_Null_m3044568124 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Playables.PlayableOutputHandle::GetHashCode()
extern "C"  int32_t PlayableOutputHandle_GetHashCode_m4127769214 (PlayableOutputHandle_t2719521870 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Playables.PlayableOutputHandle::CompareVersion(UnityEngine.Playables.PlayableOutputHandle,UnityEngine.Playables.PlayableOutputHandle)
extern "C"  bool PlayableOutputHandle_CompareVersion_m3459929500 (RuntimeObject * __this /* static, unused */, PlayableOutputHandle_t2719521870  ___lhs0, PlayableOutputHandle_t2719521870  ___rhs1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Playables.PlayableOutputHandle::Equals(System.Object)
extern "C"  bool PlayableOutputHandle_Equals_m669007655 (PlayableOutputHandle_t2719521870 * __this, RuntimeObject * ___p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.PropertyName UnityEngine.PropertyNameUtils::PropertyNameFromString(System.String)
extern "C"  PropertyName_t666104458  PropertyNameUtils_PropertyNameFromString_m3539940257 (RuntimeObject * __this /* static, unused */, String_t* ___name0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.PropertyName::.ctor(UnityEngine.PropertyName)
extern "C"  void PropertyName__ctor_m1851822729 (PropertyName_t666104458 * __this, PropertyName_t666104458  ___other0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.PropertyName::.ctor(System.String)
extern "C"  void PropertyName__ctor_m2196050290 (PropertyName_t666104458 * __this, String_t* ___name0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.PropertyName::.ctor(System.Int32)
extern "C"  void PropertyName__ctor_m4077360820 (PropertyName_t666104458 * __this, int32_t ___id0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.PropertyName::GetHashCode()
extern "C"  int32_t PropertyName_GetHashCode_m1226794333 (PropertyName_t666104458 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.PropertyName::op_Equality(UnityEngine.PropertyName,UnityEngine.PropertyName)
extern "C"  bool PropertyName_op_Equality_m2654803850 (RuntimeObject * __this /* static, unused */, PropertyName_t666104458  ___lhs0, PropertyName_t666104458  ___rhs1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.PropertyName::Equals(System.Object)
extern "C"  bool PropertyName_Equals_m3895751744 (PropertyName_t666104458 * __this, RuntimeObject * ___other0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Format(System.String,System.Object)
extern "C"  String_t* String_Format_m1366448113 (RuntimeObject * __this /* static, unused */, String_t* p0, RuntimeObject * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.PropertyName::ToString()
extern "C"  String_t* PropertyName_ToString_m1969781279 (PropertyName_t666104458 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.PropertyNameUtils::PropertyNameFromString_Injected(System.String,UnityEngine.PropertyName&)
extern "C"  void PropertyNameUtils_PropertyNameFromString_Injected_m3547369790 (RuntimeObject * __this /* static, unused */, String_t* ___name0, PropertyName_t666104458 * ___ret1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Ray::get_direction()
extern "C"  Vector3_t3239555143  Ray_get_direction_m1714502355 (Ray_t1113537957 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Ray::ToString()
extern "C"  String_t* Ray_ToString_m3743478486 (Ray_t1113537957 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Rect::get_x()
extern "C"  float Rect_get_x_m4046749491 (Rect_t1111852907 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Rect::get_y()
extern "C"  float Rect_get_y_m1845352024 (Rect_t1111852907 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Rect::get_width()
extern "C"  float Rect_get_width_m710188174 (Rect_t1111852907 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Rect::get_height()
extern "C"  float Rect_get_height_m262661000 (Rect_t1111852907 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Rect::get_xMin()
extern "C"  float Rect_get_xMin_m490539547 (Rect_t1111852907 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Rect::get_yMin()
extern "C"  float Rect_get_yMin_m1596955344 (Rect_t1111852907 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Rect::get_xMax()
extern "C"  float Rect_get_xMax_m3140092843 (Rect_t1111852907 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Rect::get_yMax()
extern "C"  float Rect_get_yMax_m1499979192 (Rect_t1111852907 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Rect::Contains(UnityEngine.Vector3)
extern "C"  bool Rect_Contains_m594593135 (Rect_t1111852907 * __this, Vector3_t3239555143  ___point0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Single::GetHashCode()
extern "C"  int32_t Single_GetHashCode_m3263933369 (float* __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Rect::GetHashCode()
extern "C"  int32_t Rect_GetHashCode_m1190625508 (Rect_t1111852907 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Rect::Equals(System.Object)
extern "C"  bool Rect_Equals_m2317586560 (Rect_t1111852907 * __this, RuntimeObject * ___other0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Rect::ToString()
extern "C"  String_t* Rect_ToString_m2557162916 (Rect_t1111852907 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform/ReapplyDrivenProperties::Invoke(UnityEngine.RectTransform)
extern "C"  void ReapplyDrivenProperties_Invoke_m2731383309 (ReapplyDrivenProperties_t3880910420 * __this, RectTransform_t3950565364 * ___driven0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.AddComponentMenu::.ctor(System.String)
extern "C"  void AddComponentMenu__ctor_m3629709883 (AddComponentMenu_t3529181215 * __this, String_t* ___menuName0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m2897993467(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___menuName0;
		__this->set_m_AddComponentMenu_0(L_0);
		__this->set_m_Ordering_1(0);
		return;
	}
}
// Conversion methods for marshalling of: UnityEngine.AnimationCurve
extern "C" void AnimationCurve_t1726276845_marshal_pinvoke(const AnimationCurve_t1726276845& unmarshaled, AnimationCurve_t1726276845_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Ptr_0 = unmarshaled.get_m_Ptr_0();
}
extern "C" void AnimationCurve_t1726276845_marshal_pinvoke_back(const AnimationCurve_t1726276845_marshaled_pinvoke& marshaled, AnimationCurve_t1726276845& unmarshaled)
{
	intptr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	unmarshaled_m_Ptr_temp_0 = marshaled.___m_Ptr_0;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.AnimationCurve
extern "C" void AnimationCurve_t1726276845_marshal_pinvoke_cleanup(AnimationCurve_t1726276845_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.AnimationCurve
extern "C" void AnimationCurve_t1726276845_marshal_com(const AnimationCurve_t1726276845& unmarshaled, AnimationCurve_t1726276845_marshaled_com& marshaled)
{
	marshaled.___m_Ptr_0 = unmarshaled.get_m_Ptr_0();
}
extern "C" void AnimationCurve_t1726276845_marshal_com_back(const AnimationCurve_t1726276845_marshaled_com& marshaled, AnimationCurve_t1726276845& unmarshaled)
{
	intptr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	unmarshaled_m_Ptr_temp_0 = marshaled.___m_Ptr_0;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.AnimationCurve
extern "C" void AnimationCurve_t1726276845_marshal_com_cleanup(AnimationCurve_t1726276845_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.AnimationCurve::.ctor(UnityEngine.Keyframe[])
extern "C"  void AnimationCurve__ctor_m3076316537 (AnimationCurve_t1726276845 * __this, KeyframeU5BU5D_t1322505427* ___keys0, const RuntimeMethod* method)
{
	{
		Object__ctor_m3446193457(__this, /*hidden argument*/NULL);
		KeyframeU5BU5D_t1322505427* L_0 = ___keys0;
		AnimationCurve_Init_m1956642396(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.AnimationCurve::.ctor()
extern "C"  void AnimationCurve__ctor_m566668622 (AnimationCurve_t1726276845 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m3446193457(__this, /*hidden argument*/NULL);
		AnimationCurve_Init_m1956642396(__this, (KeyframeU5BU5D_t1322505427*)(KeyframeU5BU5D_t1322505427*)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.AnimationCurve::Cleanup()
extern "C"  void AnimationCurve_Cleanup_m243787505 (AnimationCurve_t1726276845 * __this, const RuntimeMethod* method)
{
	typedef void (*AnimationCurve_Cleanup_m243787505_ftn) (AnimationCurve_t1726276845 *);
	static AnimationCurve_Cleanup_m243787505_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationCurve_Cleanup_m243787505_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationCurve::Cleanup()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AnimationCurve::Finalize()
extern "C"  void AnimationCurve_Finalize_m4265665064 (AnimationCurve_t1726276845 * __this, const RuntimeMethod* method)
{
	Exception_t3361176243 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3361176243 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
	}

IL_0001:
	try
	{ // begin try (depth: 1)
		AnimationCurve_Cleanup_m243787505(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x13, FINALLY_000c);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3361176243 *)e.ex;
		goto FINALLY_000c;
	}

FINALLY_000c:
	{ // begin finally (depth: 1)
		Object_Finalize_m1722074356(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(12)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(12)
	{
		IL2CPP_JUMP_TBL(0x13, IL_0013)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3361176243 *)
	}

IL_0013:
	{
		return;
	}
}
// System.Void UnityEngine.AnimationCurve::Init(UnityEngine.Keyframe[])
extern "C"  void AnimationCurve_Init_m1956642396 (AnimationCurve_t1726276845 * __this, KeyframeU5BU5D_t1322505427* ___keys0, const RuntimeMethod* method)
{
	typedef void (*AnimationCurve_Init_m1956642396_ftn) (AnimationCurve_t1726276845 *, KeyframeU5BU5D_t1322505427*);
	static AnimationCurve_Init_m1956642396_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationCurve_Init_m1956642396_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationCurve::Init(UnityEngine.Keyframe[])");
	_il2cpp_icall_func(__this, ___keys0);
}
// System.Void UnityEngine.Application::CallLowMemory()
extern "C"  void Application_CallLowMemory_m1626726542 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Application_CallLowMemory_m1626726542_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	LowMemoryCallback_t454721290 * V_0 = NULL;
	{
		LowMemoryCallback_t454721290 * L_0 = ((Application_t2657524047_StaticFields*)il2cpp_codegen_static_fields_for(Application_t2657524047_il2cpp_TypeInfo_var))->get_lowMemory_0();
		V_0 = L_0;
		LowMemoryCallback_t454721290 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_0013;
		}
	}
	{
		LowMemoryCallback_t454721290 * L_2 = V_0;
		NullCheck(L_2);
		LowMemoryCallback_Invoke_m3732620697(L_2, /*hidden argument*/NULL);
	}

IL_0013:
	{
		return;
	}
}
// System.Void UnityEngine.Application::CallLogCallback(System.String,System.String,UnityEngine.LogType,System.Boolean)
extern "C"  void Application_CallLogCallback_m3808806632 (RuntimeObject * __this /* static, unused */, String_t* ___logString0, String_t* ___stackTrace1, int32_t ___type2, bool ___invokedOnMainThread3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Application_CallLogCallback_m3808806632_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	LogCallback_t3381938013 * V_0 = NULL;
	LogCallback_t3381938013 * V_1 = NULL;
	{
		bool L_0 = ___invokedOnMainThread3;
		if (!L_0)
		{
			goto IL_001e;
		}
	}
	{
		LogCallback_t3381938013 * L_1 = ((Application_t2657524047_StaticFields*)il2cpp_codegen_static_fields_for(Application_t2657524047_il2cpp_TypeInfo_var))->get_s_LogCallbackHandler_1();
		V_0 = L_1;
		LogCallback_t3381938013 * L_2 = V_0;
		if (!L_2)
		{
			goto IL_001d;
		}
	}
	{
		LogCallback_t3381938013 * L_3 = V_0;
		String_t* L_4 = ___logString0;
		String_t* L_5 = ___stackTrace1;
		int32_t L_6 = ___type2;
		NullCheck(L_3);
		LogCallback_Invoke_m2853261436(L_3, L_4, L_5, L_6, /*hidden argument*/NULL);
	}

IL_001d:
	{
	}

IL_001e:
	{
		LogCallback_t3381938013 * L_7 = ((Application_t2657524047_StaticFields*)il2cpp_codegen_static_fields_for(Application_t2657524047_il2cpp_TypeInfo_var))->get_s_LogCallbackHandlerThreaded_2();
		V_1 = L_7;
		LogCallback_t3381938013 * L_8 = V_1;
		if (!L_8)
		{
			goto IL_0033;
		}
	}
	{
		LogCallback_t3381938013 * L_9 = V_1;
		String_t* L_10 = ___logString0;
		String_t* L_11 = ___stackTrace1;
		int32_t L_12 = ___type2;
		NullCheck(L_9);
		LogCallback_Invoke_m2853261436(L_9, L_10, L_11, L_12, /*hidden argument*/NULL);
	}

IL_0033:
	{
		return;
	}
}
// System.Void UnityEngine.Application::InvokeOnBeforeRender()
extern "C"  void Application_InvokeOnBeforeRender_m818804610 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Application_InvokeOnBeforeRender_m818804610_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	UnityAction_t35329723 * V_0 = NULL;
	{
		UnityAction_t35329723 * L_0 = ((Application_t2657524047_StaticFields*)il2cpp_codegen_static_fields_for(Application_t2657524047_il2cpp_TypeInfo_var))->get_onBeforeRender_3();
		V_0 = L_0;
		UnityAction_t35329723 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_0013;
		}
	}
	{
		UnityAction_t35329723 * L_2 = V_0;
		NullCheck(L_2);
		UnityAction_Invoke_m2515763748(L_2, /*hidden argument*/NULL);
	}

IL_0013:
	{
		return;
	}
}
extern "C"  void DelegatePInvokeWrapper_LogCallback_t3381938013 (LogCallback_t3381938013 * __this, String_t* ___condition0, String_t* ___stackTrace1, int32_t ___type2, const RuntimeMethod* method)
{
	typedef void (STDCALL *PInvokeFunc)(char*, char*, int32_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((RuntimeDelegate*)__this)->method));

	// Marshaling of parameter '___condition0' to native representation
	char* ____condition0_marshaled = NULL;
	____condition0_marshaled = il2cpp_codegen_marshal_string(___condition0);

	// Marshaling of parameter '___stackTrace1' to native representation
	char* ____stackTrace1_marshaled = NULL;
	____stackTrace1_marshaled = il2cpp_codegen_marshal_string(___stackTrace1);

	// Native function invocation
	il2cppPInvokeFunc(____condition0_marshaled, ____stackTrace1_marshaled, ___type2);

	// Marshaling cleanup of parameter '___condition0' native representation
	il2cpp_codegen_marshal_free(____condition0_marshaled);
	____condition0_marshaled = NULL;

	// Marshaling cleanup of parameter '___stackTrace1' native representation
	il2cpp_codegen_marshal_free(____stackTrace1_marshaled);
	____stackTrace1_marshaled = NULL;

}
// System.Void UnityEngine.Application/LogCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void LogCallback__ctor_m2513058278 (LogCallback_t3381938013 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Application/LogCallback::Invoke(System.String,System.String,UnityEngine.LogType)
extern "C"  void LogCallback_Invoke_m2853261436 (LogCallback_t3381938013 * __this, String_t* ___condition0, String_t* ___stackTrace1, int32_t ___type2, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		LogCallback_Invoke_m2853261436((LogCallback_t3381938013 *)__this->get_prev_9(),___condition0, ___stackTrace1, ___type2, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (RuntimeObject *, void* __this, String_t* ___condition0, String_t* ___stackTrace1, int32_t ___type2, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___condition0, ___stackTrace1, ___type2,(RuntimeMethod*)(__this->get_method_3()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, String_t* ___condition0, String_t* ___stackTrace1, int32_t ___type2, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___condition0, ___stackTrace1, ___type2,(RuntimeMethod*)(__this->get_method_3()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, String_t* ___stackTrace1, int32_t ___type2, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___condition0, ___stackTrace1, ___type2,(RuntimeMethod*)(__this->get_method_3()));
	}
}
// System.IAsyncResult UnityEngine.Application/LogCallback::BeginInvoke(System.String,System.String,UnityEngine.LogType,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* LogCallback_BeginInvoke_m1314223040 (LogCallback_t3381938013 * __this, String_t* ___condition0, String_t* ___stackTrace1, int32_t ___type2, AsyncCallback_t1046330627 * ___callback3, RuntimeObject * ___object4, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LogCallback_BeginInvoke_m1314223040_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[4] = {0};
	__d_args[0] = ___condition0;
	__d_args[1] = ___stackTrace1;
	__d_args[2] = Box(LogType_t1259498875_il2cpp_TypeInfo_var, &___type2);
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback3, (RuntimeObject*)___object4);
}
// System.Void UnityEngine.Application/LogCallback::EndInvoke(System.IAsyncResult)
extern "C"  void LogCallback_EndInvoke_m3721814140 (LogCallback_t3381938013 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
extern "C"  void DelegatePInvokeWrapper_LowMemoryCallback_t454721290 (LowMemoryCallback_t454721290 * __this, const RuntimeMethod* method)
{
	typedef void (STDCALL *PInvokeFunc)();
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((RuntimeDelegate*)__this)->method));

	// Native function invocation
	il2cppPInvokeFunc();

}
// System.Void UnityEngine.Application/LowMemoryCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void LowMemoryCallback__ctor_m4170563135 (LowMemoryCallback_t454721290 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Application/LowMemoryCallback::Invoke()
extern "C"  void LowMemoryCallback_Invoke_m3732620697 (LowMemoryCallback_t454721290 * __this, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		LowMemoryCallback_Invoke_m3732620697((LowMemoryCallback_t454721290 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((RuntimeMethod*)(__this->get_method_3()))) && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (RuntimeObject *, void* __this, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),(RuntimeMethod*)(__this->get_method_3()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),(RuntimeMethod*)(__this->get_method_3()));
	}
}
// System.IAsyncResult UnityEngine.Application/LowMemoryCallback::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* LowMemoryCallback_BeginInvoke_m3629193632 (LowMemoryCallback_t454721290 * __this, AsyncCallback_t1046330627 * ___callback0, RuntimeObject * ___object1, const RuntimeMethod* method)
{
	void *__d_args[1] = {0};
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback0, (RuntimeObject*)___object1);
}
// System.Void UnityEngine.Application/LowMemoryCallback::EndInvoke(System.IAsyncResult)
extern "C"  void LowMemoryCallback_EndInvoke_m80814921 (LowMemoryCallback_t454721290 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// Conversion methods for marshalling of: UnityEngine.AssetBundleCreateRequest
extern "C" void AssetBundleCreateRequest_t3985900355_marshal_pinvoke(const AssetBundleCreateRequest_t3985900355& unmarshaled, AssetBundleCreateRequest_t3985900355_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Ptr_0 = unmarshaled.get_m_Ptr_0();
	marshaled.___m_completeCallback_1 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_m_completeCallback_1()));
}
extern "C" void AssetBundleCreateRequest_t3985900355_marshal_pinvoke_back(const AssetBundleCreateRequest_t3985900355_marshaled_pinvoke& marshaled, AssetBundleCreateRequest_t3985900355& unmarshaled)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AssetBundleCreateRequest_t3985900355_pinvoke_FromNativeMethodDefinition_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	intptr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	unmarshaled_m_Ptr_temp_0 = marshaled.___m_Ptr_0;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
	unmarshaled.set_m_completeCallback_1(il2cpp_codegen_marshal_function_ptr_to_delegate<Action_1_t3243625498>(marshaled.___m_completeCallback_1, Action_1_t3243625498_il2cpp_TypeInfo_var));
}
// Conversion method for clean up from marshalling of: UnityEngine.AssetBundleCreateRequest
extern "C" void AssetBundleCreateRequest_t3985900355_marshal_pinvoke_cleanup(AssetBundleCreateRequest_t3985900355_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.AssetBundleCreateRequest
extern "C" void AssetBundleCreateRequest_t3985900355_marshal_com(const AssetBundleCreateRequest_t3985900355& unmarshaled, AssetBundleCreateRequest_t3985900355_marshaled_com& marshaled)
{
	marshaled.___m_Ptr_0 = unmarshaled.get_m_Ptr_0();
	marshaled.___m_completeCallback_1 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_m_completeCallback_1()));
}
extern "C" void AssetBundleCreateRequest_t3985900355_marshal_com_back(const AssetBundleCreateRequest_t3985900355_marshaled_com& marshaled, AssetBundleCreateRequest_t3985900355& unmarshaled)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AssetBundleCreateRequest_t3985900355_com_FromNativeMethodDefinition_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	intptr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	unmarshaled_m_Ptr_temp_0 = marshaled.___m_Ptr_0;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
	unmarshaled.set_m_completeCallback_1(il2cpp_codegen_marshal_function_ptr_to_delegate<Action_1_t3243625498>(marshaled.___m_completeCallback_1, Action_1_t3243625498_il2cpp_TypeInfo_var));
}
// Conversion method for clean up from marshalling of: UnityEngine.AssetBundleCreateRequest
extern "C" void AssetBundleCreateRequest_t3985900355_marshal_com_cleanup(AssetBundleCreateRequest_t3985900355_marshaled_com& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.AssetBundleRequest
extern "C" void AssetBundleRequest_t2873072919_marshal_pinvoke(const AssetBundleRequest_t2873072919& unmarshaled, AssetBundleRequest_t2873072919_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Ptr_0 = unmarshaled.get_m_Ptr_0();
	marshaled.___m_completeCallback_1 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_m_completeCallback_1()));
}
extern "C" void AssetBundleRequest_t2873072919_marshal_pinvoke_back(const AssetBundleRequest_t2873072919_marshaled_pinvoke& marshaled, AssetBundleRequest_t2873072919& unmarshaled)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AssetBundleRequest_t2873072919_pinvoke_FromNativeMethodDefinition_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	intptr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	unmarshaled_m_Ptr_temp_0 = marshaled.___m_Ptr_0;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
	unmarshaled.set_m_completeCallback_1(il2cpp_codegen_marshal_function_ptr_to_delegate<Action_1_t3243625498>(marshaled.___m_completeCallback_1, Action_1_t3243625498_il2cpp_TypeInfo_var));
}
// Conversion method for clean up from marshalling of: UnityEngine.AssetBundleRequest
extern "C" void AssetBundleRequest_t2873072919_marshal_pinvoke_cleanup(AssetBundleRequest_t2873072919_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.AssetBundleRequest
extern "C" void AssetBundleRequest_t2873072919_marshal_com(const AssetBundleRequest_t2873072919& unmarshaled, AssetBundleRequest_t2873072919_marshaled_com& marshaled)
{
	marshaled.___m_Ptr_0 = unmarshaled.get_m_Ptr_0();
	marshaled.___m_completeCallback_1 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_m_completeCallback_1()));
}
extern "C" void AssetBundleRequest_t2873072919_marshal_com_back(const AssetBundleRequest_t2873072919_marshaled_com& marshaled, AssetBundleRequest_t2873072919& unmarshaled)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AssetBundleRequest_t2873072919_com_FromNativeMethodDefinition_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	intptr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	unmarshaled_m_Ptr_temp_0 = marshaled.___m_Ptr_0;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
	unmarshaled.set_m_completeCallback_1(il2cpp_codegen_marshal_function_ptr_to_delegate<Action_1_t3243625498>(marshaled.___m_completeCallback_1, Action_1_t3243625498_il2cpp_TypeInfo_var));
}
// Conversion method for clean up from marshalling of: UnityEngine.AssetBundleRequest
extern "C" void AssetBundleRequest_t2873072919_marshal_com_cleanup(AssetBundleRequest_t2873072919_marshaled_com& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.AsyncOperation
extern "C" void AsyncOperation_t4233810744_marshal_pinvoke(const AsyncOperation_t4233810744& unmarshaled, AsyncOperation_t4233810744_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Ptr_0 = unmarshaled.get_m_Ptr_0();
	marshaled.___m_completeCallback_1 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_m_completeCallback_1()));
}
extern "C" void AsyncOperation_t4233810744_marshal_pinvoke_back(const AsyncOperation_t4233810744_marshaled_pinvoke& marshaled, AsyncOperation_t4233810744& unmarshaled)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AsyncOperation_t4233810744_pinvoke_FromNativeMethodDefinition_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	intptr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	unmarshaled_m_Ptr_temp_0 = marshaled.___m_Ptr_0;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
	unmarshaled.set_m_completeCallback_1(il2cpp_codegen_marshal_function_ptr_to_delegate<Action_1_t3243625498>(marshaled.___m_completeCallback_1, Action_1_t3243625498_il2cpp_TypeInfo_var));
}
// Conversion method for clean up from marshalling of: UnityEngine.AsyncOperation
extern "C" void AsyncOperation_t4233810744_marshal_pinvoke_cleanup(AsyncOperation_t4233810744_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.AsyncOperation
extern "C" void AsyncOperation_t4233810744_marshal_com(const AsyncOperation_t4233810744& unmarshaled, AsyncOperation_t4233810744_marshaled_com& marshaled)
{
	marshaled.___m_Ptr_0 = unmarshaled.get_m_Ptr_0();
	marshaled.___m_completeCallback_1 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_m_completeCallback_1()));
}
extern "C" void AsyncOperation_t4233810744_marshal_com_back(const AsyncOperation_t4233810744_marshaled_com& marshaled, AsyncOperation_t4233810744& unmarshaled)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AsyncOperation_t4233810744_com_FromNativeMethodDefinition_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	intptr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	unmarshaled_m_Ptr_temp_0 = marshaled.___m_Ptr_0;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
	unmarshaled.set_m_completeCallback_1(il2cpp_codegen_marshal_function_ptr_to_delegate<Action_1_t3243625498>(marshaled.___m_completeCallback_1, Action_1_t3243625498_il2cpp_TypeInfo_var));
}
// Conversion method for clean up from marshalling of: UnityEngine.AsyncOperation
extern "C" void AsyncOperation_t4233810744_marshal_com_cleanup(AsyncOperation_t4233810744_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.AsyncOperation::InternalDestroy()
extern "C"  void AsyncOperation_InternalDestroy_m1747224711 (AsyncOperation_t4233810744 * __this, const RuntimeMethod* method)
{
	typedef void (*AsyncOperation_InternalDestroy_m1747224711_ftn) (AsyncOperation_t4233810744 *);
	static AsyncOperation_InternalDestroy_m1747224711_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AsyncOperation_InternalDestroy_m1747224711_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AsyncOperation::InternalDestroy()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AsyncOperation::Finalize()
extern "C"  void AsyncOperation_Finalize_m1200536139 (AsyncOperation_t4233810744 * __this, const RuntimeMethod* method)
{
	Exception_t3361176243 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3361176243 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
	}

IL_0001:
	try
	{ // begin try (depth: 1)
		AsyncOperation_InternalDestroy_m1747224711(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x13, FINALLY_000c);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3361176243 *)e.ex;
		goto FINALLY_000c;
	}

FINALLY_000c:
	{ // begin finally (depth: 1)
		Object_Finalize_m1722074356(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(12)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(12)
	{
		IL2CPP_JUMP_TBL(0x13, IL_0013)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3361176243 *)
	}

IL_0013:
	{
		return;
	}
}
// System.Void UnityEngine.AsyncOperation::InvokeCompletionEvent()
extern "C"  void AsyncOperation_InvokeCompletionEvent_m737953261 (AsyncOperation_t4233810744 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AsyncOperation_InvokeCompletionEvent_m737953261_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Action_1_t3243625498 * L_0 = __this->get_m_completeCallback_1();
		if (!L_0)
		{
			goto IL_0021;
		}
	}
	{
		Action_1_t3243625498 * L_1 = __this->get_m_completeCallback_1();
		NullCheck(L_1);
		Action_1_Invoke_m258234264(L_1, __this, /*hidden argument*/Action_1_Invoke_m258234264_RuntimeMethod_var);
		__this->set_m_completeCallback_1((Action_1_t3243625498 *)NULL);
	}

IL_0021:
	{
		return;
	}
}
// System.Type UnityEngine.AttributeHelperEngine::GetParentTypeDisallowingMultipleInclusion(System.Type)
extern "C"  Type_t * AttributeHelperEngine_GetParentTypeDisallowingMultipleInclusion_m1021838127 (RuntimeObject * __this /* static, unused */, Type_t * ___type0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AttributeHelperEngine_GetParentTypeDisallowingMultipleInclusion_m1021838127_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Stack_1_t2864815562 * V_0 = NULL;
	Type_t * V_1 = NULL;
	ObjectU5BU5D_t4290686858* V_2 = NULL;
	int32_t V_3 = 0;
	Type_t * V_4 = NULL;
	{
		Stack_1_t2864815562 * L_0 = (Stack_1_t2864815562 *)il2cpp_codegen_object_new(Stack_1_t2864815562_il2cpp_TypeInfo_var);
		Stack_1__ctor_m3382330311(L_0, /*hidden argument*/Stack_1__ctor_m3382330311_RuntimeMethod_var);
		V_0 = L_0;
		goto IL_001d;
	}

IL_000c:
	{
		Stack_1_t2864815562 * L_1 = V_0;
		Type_t * L_2 = ___type0;
		NullCheck(L_1);
		Stack_1_Push_m2062010219(L_1, L_2, /*hidden argument*/Stack_1_Push_m2062010219_RuntimeMethod_var);
		Type_t * L_3 = ___type0;
		NullCheck(L_3);
		Type_t * L_4 = VirtFuncInvoker0< Type_t * >::Invoke(17 /* System.Type System.Type::get_BaseType() */, L_3);
		___type0 = L_4;
	}

IL_001d:
	{
		Type_t * L_5 = ___type0;
		if (!L_5)
		{
			goto IL_0033;
		}
	}
	{
		Type_t * L_6 = ___type0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_7 = Type_GetTypeFromHandle_m3171413529(NULL /*static, unused*/, LoadTypeToken(MonoBehaviour_t2904604993_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((RuntimeObject*)(Type_t *)L_6) == ((RuntimeObject*)(Type_t *)L_7))))
		{
			goto IL_000c;
		}
	}

IL_0033:
	{
		V_1 = (Type_t *)NULL;
		goto IL_0067;
	}

IL_003a:
	{
		Stack_1_t2864815562 * L_8 = V_0;
		NullCheck(L_8);
		Type_t * L_9 = Stack_1_Pop_m2423581918(L_8, /*hidden argument*/Stack_1_Pop_m2423581918_RuntimeMethod_var);
		V_1 = L_9;
		Type_t * L_10 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_11 = Type_GetTypeFromHandle_m3171413529(NULL /*static, unused*/, LoadTypeToken(DisallowMultipleComponent_t2957315911_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_10);
		ObjectU5BU5D_t4290686858* L_12 = VirtFuncInvoker2< ObjectU5BU5D_t4290686858*, Type_t *, bool >::Invoke(13 /* System.Object[] System.Reflection.MemberInfo::GetCustomAttributes(System.Type,System.Boolean) */, L_10, L_11, (bool)0);
		V_2 = L_12;
		ObjectU5BU5D_t4290686858* L_13 = V_2;
		NullCheck(L_13);
		V_3 = (((int32_t)((int32_t)(((RuntimeArray *)L_13)->max_length))));
		int32_t L_14 = V_3;
		if (!L_14)
		{
			goto IL_0066;
		}
	}
	{
		Type_t * L_15 = V_1;
		V_4 = L_15;
		goto IL_007b;
	}

IL_0066:
	{
	}

IL_0067:
	{
		Stack_1_t2864815562 * L_16 = V_0;
		NullCheck(L_16);
		int32_t L_17 = Stack_1_get_Count_m865256008(L_16, /*hidden argument*/Stack_1_get_Count_m865256008_RuntimeMethod_var);
		if ((((int32_t)L_17) > ((int32_t)0)))
		{
			goto IL_003a;
		}
	}
	{
		V_4 = (Type_t *)NULL;
		goto IL_007b;
	}

IL_007b:
	{
		Type_t * L_18 = V_4;
		return L_18;
	}
}
// System.Type[] UnityEngine.AttributeHelperEngine::GetRequiredComponents(System.Type)
extern "C"  TypeU5BU5D_t1830665827* AttributeHelperEngine_GetRequiredComponents_m2091293497 (RuntimeObject * __this /* static, unused */, Type_t * ___klass0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AttributeHelperEngine_GetRequiredComponents_m2091293497_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t152613186 * V_0 = NULL;
	RequireComponentU5BU5D_t2045948322* V_1 = NULL;
	Type_t * V_2 = NULL;
	RequireComponent_t597737523 * V_3 = NULL;
	RequireComponentU5BU5D_t2045948322* V_4 = NULL;
	int32_t V_5 = 0;
	TypeU5BU5D_t1830665827* V_6 = NULL;
	TypeU5BU5D_t1830665827* V_7 = NULL;
	{
		V_0 = (List_1_t152613186 *)NULL;
		goto IL_00ef;
	}

IL_0008:
	{
		Type_t * L_0 = ___klass0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m3171413529(NULL /*static, unused*/, LoadTypeToken(RequireComponent_t597737523_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_0);
		ObjectU5BU5D_t4290686858* L_2 = VirtFuncInvoker2< ObjectU5BU5D_t4290686858*, Type_t *, bool >::Invoke(13 /* System.Object[] System.Reflection.MemberInfo::GetCustomAttributes(System.Type,System.Boolean) */, L_0, L_1, (bool)0);
		V_1 = ((RequireComponentU5BU5D_t2045948322*)Castclass((RuntimeObject*)L_2, RequireComponentU5BU5D_t2045948322_il2cpp_TypeInfo_var));
		Type_t * L_3 = ___klass0;
		NullCheck(L_3);
		Type_t * L_4 = VirtFuncInvoker0< Type_t * >::Invoke(17 /* System.Type System.Type::get_BaseType() */, L_3);
		V_2 = L_4;
		RequireComponentU5BU5D_t2045948322* L_5 = V_1;
		V_4 = L_5;
		V_5 = 0;
		goto IL_00e0;
	}

IL_0033:
	{
		RequireComponentU5BU5D_t2045948322* L_6 = V_4;
		int32_t L_7 = V_5;
		NullCheck(L_6);
		int32_t L_8 = L_7;
		RequireComponent_t597737523 * L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		V_3 = L_9;
		List_1_t152613186 * L_10 = V_0;
		if (L_10)
		{
			goto IL_0086;
		}
	}
	{
		RequireComponentU5BU5D_t2045948322* L_11 = V_1;
		NullCheck(L_11);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_11)->max_length))))) == ((uint32_t)1))))
		{
			goto IL_0086;
		}
	}
	{
		Type_t * L_12 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_13 = Type_GetTypeFromHandle_m3171413529(NULL /*static, unused*/, LoadTypeToken(MonoBehaviour_t2904604993_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((RuntimeObject*)(Type_t *)L_12) == ((RuntimeObject*)(Type_t *)L_13))))
		{
			goto IL_0086;
		}
	}
	{
		TypeU5BU5D_t1830665827* L_14 = ((TypeU5BU5D_t1830665827*)SZArrayNew(TypeU5BU5D_t1830665827_il2cpp_TypeInfo_var, (uint32_t)3));
		RequireComponent_t597737523 * L_15 = V_3;
		NullCheck(L_15);
		Type_t * L_16 = L_15->get_m_Type0_0();
		NullCheck(L_14);
		ArrayElementTypeCheck (L_14, L_16);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_16);
		TypeU5BU5D_t1830665827* L_17 = L_14;
		RequireComponent_t597737523 * L_18 = V_3;
		NullCheck(L_18);
		Type_t * L_19 = L_18->get_m_Type1_1();
		NullCheck(L_17);
		ArrayElementTypeCheck (L_17, L_19);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(1), (Type_t *)L_19);
		TypeU5BU5D_t1830665827* L_20 = L_17;
		RequireComponent_t597737523 * L_21 = V_3;
		NullCheck(L_21);
		Type_t * L_22 = L_21->get_m_Type2_2();
		NullCheck(L_20);
		ArrayElementTypeCheck (L_20, L_22);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(2), (Type_t *)L_22);
		V_6 = L_20;
		TypeU5BU5D_t1830665827* L_23 = V_6;
		V_7 = L_23;
		goto IL_0120;
	}

IL_0086:
	{
		List_1_t152613186 * L_24 = V_0;
		if (L_24)
		{
			goto IL_0093;
		}
	}
	{
		List_1_t152613186 * L_25 = (List_1_t152613186 *)il2cpp_codegen_object_new(List_1_t152613186_il2cpp_TypeInfo_var);
		List_1__ctor_m1793766529(L_25, /*hidden argument*/List_1__ctor_m1793766529_RuntimeMethod_var);
		V_0 = L_25;
	}

IL_0093:
	{
		RequireComponent_t597737523 * L_26 = V_3;
		NullCheck(L_26);
		Type_t * L_27 = L_26->get_m_Type0_0();
		if (!L_27)
		{
			goto IL_00aa;
		}
	}
	{
		List_1_t152613186 * L_28 = V_0;
		RequireComponent_t597737523 * L_29 = V_3;
		NullCheck(L_29);
		Type_t * L_30 = L_29->get_m_Type0_0();
		NullCheck(L_28);
		List_1_Add_m2364263606(L_28, L_30, /*hidden argument*/List_1_Add_m2364263606_RuntimeMethod_var);
	}

IL_00aa:
	{
		RequireComponent_t597737523 * L_31 = V_3;
		NullCheck(L_31);
		Type_t * L_32 = L_31->get_m_Type1_1();
		if (!L_32)
		{
			goto IL_00c1;
		}
	}
	{
		List_1_t152613186 * L_33 = V_0;
		RequireComponent_t597737523 * L_34 = V_3;
		NullCheck(L_34);
		Type_t * L_35 = L_34->get_m_Type1_1();
		NullCheck(L_33);
		List_1_Add_m2364263606(L_33, L_35, /*hidden argument*/List_1_Add_m2364263606_RuntimeMethod_var);
	}

IL_00c1:
	{
		RequireComponent_t597737523 * L_36 = V_3;
		NullCheck(L_36);
		Type_t * L_37 = L_36->get_m_Type2_2();
		if (!L_37)
		{
			goto IL_00d8;
		}
	}
	{
		List_1_t152613186 * L_38 = V_0;
		RequireComponent_t597737523 * L_39 = V_3;
		NullCheck(L_39);
		Type_t * L_40 = L_39->get_m_Type2_2();
		NullCheck(L_38);
		List_1_Add_m2364263606(L_38, L_40, /*hidden argument*/List_1_Add_m2364263606_RuntimeMethod_var);
	}

IL_00d8:
	{
		int32_t L_41 = V_5;
		V_5 = ((int32_t)((int32_t)L_41+(int32_t)1));
	}

IL_00e0:
	{
		int32_t L_42 = V_5;
		RequireComponentU5BU5D_t2045948322* L_43 = V_4;
		NullCheck(L_43);
		if ((((int32_t)L_42) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_43)->max_length)))))))
		{
			goto IL_0033;
		}
	}
	{
		Type_t * L_44 = V_2;
		___klass0 = L_44;
	}

IL_00ef:
	{
		Type_t * L_45 = ___klass0;
		if (!L_45)
		{
			goto IL_0105;
		}
	}
	{
		Type_t * L_46 = ___klass0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_47 = Type_GetTypeFromHandle_m3171413529(NULL /*static, unused*/, LoadTypeToken(MonoBehaviour_t2904604993_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((RuntimeObject*)(Type_t *)L_46) == ((RuntimeObject*)(Type_t *)L_47))))
		{
			goto IL_0008;
		}
	}

IL_0105:
	{
		List_1_t152613186 * L_48 = V_0;
		if (L_48)
		{
			goto IL_0113;
		}
	}
	{
		V_7 = (TypeU5BU5D_t1830665827*)NULL;
		goto IL_0120;
	}

IL_0113:
	{
		List_1_t152613186 * L_49 = V_0;
		NullCheck(L_49);
		TypeU5BU5D_t1830665827* L_50 = List_1_ToArray_m289903370(L_49, /*hidden argument*/List_1_ToArray_m289903370_RuntimeMethod_var);
		V_7 = L_50;
		goto IL_0120;
	}

IL_0120:
	{
		TypeU5BU5D_t1830665827* L_51 = V_7;
		return L_51;
	}
}
// System.Boolean UnityEngine.AttributeHelperEngine::CheckIsEditorScript(System.Type)
extern "C"  bool AttributeHelperEngine_CheckIsEditorScript_m2212222995 (RuntimeObject * __this /* static, unused */, Type_t * ___klass0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AttributeHelperEngine_CheckIsEditorScript_m2212222995_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ObjectU5BU5D_t4290686858* V_0 = NULL;
	int32_t V_1 = 0;
	bool V_2 = false;
	{
		goto IL_0033;
	}

IL_0006:
	{
		Type_t * L_0 = ___klass0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m3171413529(NULL /*static, unused*/, LoadTypeToken(ExecuteInEditMode_t1836144016_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_0);
		ObjectU5BU5D_t4290686858* L_2 = VirtFuncInvoker2< ObjectU5BU5D_t4290686858*, Type_t *, bool >::Invoke(13 /* System.Object[] System.Reflection.MemberInfo::GetCustomAttributes(System.Type,System.Boolean) */, L_0, L_1, (bool)0);
		V_0 = L_2;
		ObjectU5BU5D_t4290686858* L_3 = V_0;
		NullCheck(L_3);
		V_1 = (((int32_t)((int32_t)(((RuntimeArray *)L_3)->max_length))));
		int32_t L_4 = V_1;
		if (!L_4)
		{
			goto IL_002a;
		}
	}
	{
		V_2 = (bool)1;
		goto IL_0050;
	}

IL_002a:
	{
		Type_t * L_5 = ___klass0;
		NullCheck(L_5);
		Type_t * L_6 = VirtFuncInvoker0< Type_t * >::Invoke(17 /* System.Type System.Type::get_BaseType() */, L_5);
		___klass0 = L_6;
	}

IL_0033:
	{
		Type_t * L_7 = ___klass0;
		if (!L_7)
		{
			goto IL_0049;
		}
	}
	{
		Type_t * L_8 = ___klass0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_9 = Type_GetTypeFromHandle_m3171413529(NULL /*static, unused*/, LoadTypeToken(MonoBehaviour_t2904604993_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((RuntimeObject*)(Type_t *)L_8) == ((RuntimeObject*)(Type_t *)L_9))))
		{
			goto IL_0006;
		}
	}

IL_0049:
	{
		V_2 = (bool)0;
		goto IL_0050;
	}

IL_0050:
	{
		bool L_10 = V_2;
		return L_10;
	}
}
// System.Int32 UnityEngine.AttributeHelperEngine::GetDefaultExecutionOrderFor(System.Type)
extern "C"  int32_t AttributeHelperEngine_GetDefaultExecutionOrderFor_m1362990272 (RuntimeObject * __this /* static, unused */, Type_t * ___klass0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AttributeHelperEngine_GetDefaultExecutionOrderFor_m1362990272_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	DefaultExecutionOrder_t3862449190 * V_0 = NULL;
	int32_t V_1 = 0;
	{
		Type_t * L_0 = ___klass0;
		IL2CPP_RUNTIME_CLASS_INIT(AttributeHelperEngine_t3367628457_il2cpp_TypeInfo_var);
		DefaultExecutionOrder_t3862449190 * L_1 = AttributeHelperEngine_GetCustomAttributeOfType_TisDefaultExecutionOrder_t3862449190_m4154997828(NULL /*static, unused*/, L_0, /*hidden argument*/AttributeHelperEngine_GetCustomAttributeOfType_TisDefaultExecutionOrder_t3862449190_m4154997828_RuntimeMethod_var);
		V_0 = L_1;
		DefaultExecutionOrder_t3862449190 * L_2 = V_0;
		if (L_2)
		{
			goto IL_0015;
		}
	}
	{
		V_1 = 0;
		goto IL_0021;
	}

IL_0015:
	{
		DefaultExecutionOrder_t3862449190 * L_3 = V_0;
		NullCheck(L_3);
		int32_t L_4 = DefaultExecutionOrder_get_order_m3989798846(L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		goto IL_0021;
	}

IL_0021:
	{
		int32_t L_5 = V_1;
		return L_5;
	}
}
// System.Void UnityEngine.AttributeHelperEngine::.cctor()
extern "C"  void AttributeHelperEngine__cctor_m3752452713 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AttributeHelperEngine__cctor_m3752452713_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((AttributeHelperEngine_t3367628457_StaticFields*)il2cpp_codegen_static_fields_for(AttributeHelperEngine_t3367628457_il2cpp_TypeInfo_var))->set__disallowMultipleComponentArray_0(((DisallowMultipleComponentU5BU5D_t773128126*)SZArrayNew(DisallowMultipleComponentU5BU5D_t773128126_il2cpp_TypeInfo_var, (uint32_t)1)));
		((AttributeHelperEngine_t3367628457_StaticFields*)il2cpp_codegen_static_fields_for(AttributeHelperEngine_t3367628457_il2cpp_TypeInfo_var))->set__executeInEditModeArray_1(((ExecuteInEditModeU5BU5D_t2797163057*)SZArrayNew(ExecuteInEditModeU5BU5D_t2797163057_il2cpp_TypeInfo_var, (uint32_t)1)));
		((AttributeHelperEngine_t3367628457_StaticFields*)il2cpp_codegen_static_fields_for(AttributeHelperEngine_t3367628457_il2cpp_TypeInfo_var))->set__requireComponentArray_2(((RequireComponentU5BU5D_t2045948322*)SZArrayNew(RequireComponentU5BU5D_t2045948322_il2cpp_TypeInfo_var, (uint32_t)1)));
		return;
	}
}
// System.Void UnityEngine.Behaviour::.ctor()
extern "C"  void Behaviour__ctor_m1233868026 (Behaviour_t4110946901 * __this, const RuntimeMethod* method)
{
	{
		Component__ctor_m2592563656(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.Behaviour::get_enabled()
extern "C"  bool Behaviour_get_enabled_m1304335371 (Behaviour_t4110946901 * __this, const RuntimeMethod* method)
{
	typedef bool (*Behaviour_get_enabled_m1304335371_ftn) (Behaviour_t4110946901 *);
	static Behaviour_get_enabled_m1304335371_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Behaviour_get_enabled_m1304335371_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Behaviour::get_enabled()");
	bool retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.Bindings.UnmarshalledAttribute::.ctor()
extern "C"  void UnmarshalledAttribute__ctor_m3412418881 (UnmarshalledAttribute_t2695814082 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m2897993467(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Single UnityEngine.Camera::get_nearClipPlane()
extern "C"  float Camera_get_nearClipPlane_m167155657 (Camera_t101201881 * __this, const RuntimeMethod* method)
{
	typedef float (*Camera_get_nearClipPlane_m167155657_ftn) (Camera_t101201881 *);
	static Camera_get_nearClipPlane_m167155657_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_nearClipPlane_m167155657_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_nearClipPlane()");
	float retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Single UnityEngine.Camera::get_farClipPlane()
extern "C"  float Camera_get_farClipPlane_m1366251422 (Camera_t101201881 * __this, const RuntimeMethod* method)
{
	typedef float (*Camera_get_farClipPlane_m1366251422_ftn) (Camera_t101201881 *);
	static Camera_get_farClipPlane_m1366251422_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_farClipPlane_m1366251422_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_farClipPlane()");
	float retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Int32 UnityEngine.Camera::get_cullingMask()
extern "C"  int32_t Camera_get_cullingMask_m2771596515 (Camera_t101201881 * __this, const RuntimeMethod* method)
{
	typedef int32_t (*Camera_get_cullingMask_m2771596515_ftn) (Camera_t101201881 *);
	static Camera_get_cullingMask_m2771596515_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_cullingMask_m2771596515_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_cullingMask()");
	int32_t retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Int32 UnityEngine.Camera::get_eventMask()
extern "C"  int32_t Camera_get_eventMask_m3958975071 (Camera_t101201881 * __this, const RuntimeMethod* method)
{
	typedef int32_t (*Camera_get_eventMask_m3958975071_ftn) (Camera_t101201881 *);
	static Camera_get_eventMask_m3958975071_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_eventMask_m3958975071_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_eventMask()");
	int32_t retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// UnityEngine.Rect UnityEngine.Camera::get_pixelRect()
extern "C"  Rect_t1111852907  Camera_get_pixelRect_m3857366896 (Camera_t101201881 * __this, const RuntimeMethod* method)
{
	Rect_t1111852907  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Rect_t1111852907  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Camera_INTERNAL_get_pixelRect_m3102036321(__this, (&V_0), /*hidden argument*/NULL);
		Rect_t1111852907  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		Rect_t1111852907  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Camera::INTERNAL_get_pixelRect(UnityEngine.Rect&)
extern "C"  void Camera_INTERNAL_get_pixelRect_m3102036321 (Camera_t101201881 * __this, Rect_t1111852907 * ___value0, const RuntimeMethod* method)
{
	typedef void (*Camera_INTERNAL_get_pixelRect_m3102036321_ftn) (Camera_t101201881 *, Rect_t1111852907 *);
	static Camera_INTERNAL_get_pixelRect_m3102036321_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_get_pixelRect_m3102036321_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_get_pixelRect(UnityEngine.Rect&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.RenderTexture UnityEngine.Camera::get_targetTexture()
extern "C"  RenderTexture_t2667189599 * Camera_get_targetTexture_m903246907 (Camera_t101201881 * __this, const RuntimeMethod* method)
{
	typedef RenderTexture_t2667189599 * (*Camera_get_targetTexture_m903246907_ftn) (Camera_t101201881 *);
	static Camera_get_targetTexture_m903246907_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_targetTexture_m903246907_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_targetTexture()");
	RenderTexture_t2667189599 * retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// UnityEngine.CameraClearFlags UnityEngine.Camera::get_clearFlags()
extern "C"  int32_t Camera_get_clearFlags_m3577200564 (Camera_t101201881 * __this, const RuntimeMethod* method)
{
	typedef int32_t (*Camera_get_clearFlags_m3577200564_ftn) (Camera_t101201881 *);
	static Camera_get_clearFlags_m3577200564_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_clearFlags_m3577200564_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_clearFlags()");
	int32_t retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// UnityEngine.Ray UnityEngine.Camera::ScreenPointToRay(UnityEngine.Vector3)
extern "C"  Ray_t1113537957  Camera_ScreenPointToRay_m2744754050 (Camera_t101201881 * __this, Vector3_t3239555143  ___position0, const RuntimeMethod* method)
{
	Ray_t1113537957  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Ray_t1113537957  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Camera_INTERNAL_CALL_ScreenPointToRay_m244366202(NULL /*static, unused*/, __this, (&___position0), (&V_0), /*hidden argument*/NULL);
		Ray_t1113537957  L_0 = V_0;
		V_1 = L_0;
		goto IL_0012;
	}

IL_0012:
	{
		Ray_t1113537957  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Camera::INTERNAL_CALL_ScreenPointToRay(UnityEngine.Camera,UnityEngine.Vector3&,UnityEngine.Ray&)
extern "C"  void Camera_INTERNAL_CALL_ScreenPointToRay_m244366202 (RuntimeObject * __this /* static, unused */, Camera_t101201881 * ___self0, Vector3_t3239555143 * ___position1, Ray_t1113537957 * ___value2, const RuntimeMethod* method)
{
	typedef void (*Camera_INTERNAL_CALL_ScreenPointToRay_m244366202_ftn) (Camera_t101201881 *, Vector3_t3239555143 *, Ray_t1113537957 *);
	static Camera_INTERNAL_CALL_ScreenPointToRay_m244366202_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_CALL_ScreenPointToRay_m244366202_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_CALL_ScreenPointToRay(UnityEngine.Camera,UnityEngine.Vector3&,UnityEngine.Ray&)");
	_il2cpp_icall_func(___self0, ___position1, ___value2);
}
// System.Int32 UnityEngine.Camera::get_allCamerasCount()
extern "C"  int32_t Camera_get_allCamerasCount_m3527489092 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef int32_t (*Camera_get_allCamerasCount_m3527489092_ftn) ();
	static Camera_get_allCamerasCount_m3527489092_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_allCamerasCount_m3527489092_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_allCamerasCount()");
	int32_t retVal = _il2cpp_icall_func();
	return retVal;
}
// System.Int32 UnityEngine.Camera::GetAllCameras(UnityEngine.Camera[])
extern "C"  int32_t Camera_GetAllCameras_m557431570 (RuntimeObject * __this /* static, unused */, CameraU5BU5D_t2722699428* ___cameras0, const RuntimeMethod* method)
{
	typedef int32_t (*Camera_GetAllCameras_m557431570_ftn) (CameraU5BU5D_t2722699428*);
	static Camera_GetAllCameras_m557431570_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_GetAllCameras_m557431570_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::GetAllCameras(UnityEngine.Camera[])");
	int32_t retVal = _il2cpp_icall_func(___cameras0);
	return retVal;
}
// System.Void UnityEngine.Camera::FireOnPreCull(UnityEngine.Camera)
extern "C"  void Camera_FireOnPreCull_m2186829015 (RuntimeObject * __this /* static, unused */, Camera_t101201881 * ___cam0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Camera_FireOnPreCull_m2186829015_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		CameraCallback_t2065492188 * L_0 = ((Camera_t101201881_StaticFields*)il2cpp_codegen_static_fields_for(Camera_t101201881_il2cpp_TypeInfo_var))->get_onPreCull_2();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		CameraCallback_t2065492188 * L_1 = ((Camera_t101201881_StaticFields*)il2cpp_codegen_static_fields_for(Camera_t101201881_il2cpp_TypeInfo_var))->get_onPreCull_2();
		Camera_t101201881 * L_2 = ___cam0;
		NullCheck(L_1);
		CameraCallback_Invoke_m144364913(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0016:
	{
		return;
	}
}
// System.Void UnityEngine.Camera::FireOnPreRender(UnityEngine.Camera)
extern "C"  void Camera_FireOnPreRender_m1563217163 (RuntimeObject * __this /* static, unused */, Camera_t101201881 * ___cam0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Camera_FireOnPreRender_m1563217163_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		CameraCallback_t2065492188 * L_0 = ((Camera_t101201881_StaticFields*)il2cpp_codegen_static_fields_for(Camera_t101201881_il2cpp_TypeInfo_var))->get_onPreRender_3();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		CameraCallback_t2065492188 * L_1 = ((Camera_t101201881_StaticFields*)il2cpp_codegen_static_fields_for(Camera_t101201881_il2cpp_TypeInfo_var))->get_onPreRender_3();
		Camera_t101201881 * L_2 = ___cam0;
		NullCheck(L_1);
		CameraCallback_Invoke_m144364913(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0016:
	{
		return;
	}
}
// System.Void UnityEngine.Camera::FireOnPostRender(UnityEngine.Camera)
extern "C"  void Camera_FireOnPostRender_m4290449375 (RuntimeObject * __this /* static, unused */, Camera_t101201881 * ___cam0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Camera_FireOnPostRender_m4290449375_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		CameraCallback_t2065492188 * L_0 = ((Camera_t101201881_StaticFields*)il2cpp_codegen_static_fields_for(Camera_t101201881_il2cpp_TypeInfo_var))->get_onPostRender_4();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		CameraCallback_t2065492188 * L_1 = ((Camera_t101201881_StaticFields*)il2cpp_codegen_static_fields_for(Camera_t101201881_il2cpp_TypeInfo_var))->get_onPostRender_4();
		Camera_t101201881 * L_2 = ___cam0;
		NullCheck(L_1);
		CameraCallback_Invoke_m144364913(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0016:
	{
		return;
	}
}
// UnityEngine.GameObject UnityEngine.Camera::RaycastTry(UnityEngine.Ray,System.Single,System.Int32)
extern "C"  GameObject_t3732643618 * Camera_RaycastTry_m208704706 (Camera_t101201881 * __this, Ray_t1113537957  ___ray0, float ___distance1, int32_t ___layerMask2, const RuntimeMethod* method)
{
	GameObject_t3732643618 * V_0 = NULL;
	{
		float L_0 = ___distance1;
		int32_t L_1 = ___layerMask2;
		GameObject_t3732643618 * L_2 = Camera_INTERNAL_CALL_RaycastTry_m2903709223(NULL /*static, unused*/, __this, (&___ray0), L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0011;
	}

IL_0011:
	{
		GameObject_t3732643618 * L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.GameObject UnityEngine.Camera::INTERNAL_CALL_RaycastTry(UnityEngine.Camera,UnityEngine.Ray&,System.Single,System.Int32)
extern "C"  GameObject_t3732643618 * Camera_INTERNAL_CALL_RaycastTry_m2903709223 (RuntimeObject * __this /* static, unused */, Camera_t101201881 * ___self0, Ray_t1113537957 * ___ray1, float ___distance2, int32_t ___layerMask3, const RuntimeMethod* method)
{
	typedef GameObject_t3732643618 * (*Camera_INTERNAL_CALL_RaycastTry_m2903709223_ftn) (Camera_t101201881 *, Ray_t1113537957 *, float, int32_t);
	static Camera_INTERNAL_CALL_RaycastTry_m2903709223_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_CALL_RaycastTry_m2903709223_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_CALL_RaycastTry(UnityEngine.Camera,UnityEngine.Ray&,System.Single,System.Int32)");
	GameObject_t3732643618 * retVal = _il2cpp_icall_func(___self0, ___ray1, ___distance2, ___layerMask3);
	return retVal;
}
// UnityEngine.GameObject UnityEngine.Camera::RaycastTry2D(UnityEngine.Ray,System.Single,System.Int32)
extern "C"  GameObject_t3732643618 * Camera_RaycastTry2D_m3780933465 (Camera_t101201881 * __this, Ray_t1113537957  ___ray0, float ___distance1, int32_t ___layerMask2, const RuntimeMethod* method)
{
	GameObject_t3732643618 * V_0 = NULL;
	{
		float L_0 = ___distance1;
		int32_t L_1 = ___layerMask2;
		GameObject_t3732643618 * L_2 = Camera_INTERNAL_CALL_RaycastTry2D_m700194433(NULL /*static, unused*/, __this, (&___ray0), L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0011;
	}

IL_0011:
	{
		GameObject_t3732643618 * L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.GameObject UnityEngine.Camera::INTERNAL_CALL_RaycastTry2D(UnityEngine.Camera,UnityEngine.Ray&,System.Single,System.Int32)
extern "C"  GameObject_t3732643618 * Camera_INTERNAL_CALL_RaycastTry2D_m700194433 (RuntimeObject * __this /* static, unused */, Camera_t101201881 * ___self0, Ray_t1113537957 * ___ray1, float ___distance2, int32_t ___layerMask3, const RuntimeMethod* method)
{
	typedef GameObject_t3732643618 * (*Camera_INTERNAL_CALL_RaycastTry2D_m700194433_ftn) (Camera_t101201881 *, Ray_t1113537957 *, float, int32_t);
	static Camera_INTERNAL_CALL_RaycastTry2D_m700194433_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_CALL_RaycastTry2D_m700194433_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_CALL_RaycastTry2D(UnityEngine.Camera,UnityEngine.Ray&,System.Single,System.Int32)");
	GameObject_t3732643618 * retVal = _il2cpp_icall_func(___self0, ___ray1, ___distance2, ___layerMask3);
	return retVal;
}
// System.Void UnityEngine.Camera/CameraCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void CameraCallback__ctor_m1713115355 (CameraCallback_t2065492188 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Camera/CameraCallback::Invoke(UnityEngine.Camera)
extern "C"  void CameraCallback_Invoke_m144364913 (CameraCallback_t2065492188 * __this, Camera_t101201881 * ___cam0, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		CameraCallback_Invoke_m144364913((CameraCallback_t2065492188 *)__this->get_prev_9(),___cam0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (RuntimeObject *, void* __this, Camera_t101201881 * ___cam0, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___cam0,(RuntimeMethod*)(__this->get_method_3()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Camera_t101201881 * ___cam0, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___cam0,(RuntimeMethod*)(__this->get_method_3()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___cam0,(RuntimeMethod*)(__this->get_method_3()));
	}
}
// System.IAsyncResult UnityEngine.Camera/CameraCallback::BeginInvoke(UnityEngine.Camera,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* CameraCallback_BeginInvoke_m4085641401 (CameraCallback_t2065492188 * __this, Camera_t101201881 * ___cam0, AsyncCallback_t1046330627 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___cam0;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Void UnityEngine.Camera/CameraCallback::EndInvoke(System.IAsyncResult)
extern "C"  void CameraCallback_EndInvoke_m2093829129 (CameraCallback_t2065492188 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.ClassLibraryInitializer::Init()
extern "C"  void ClassLibraryInitializer_Init_m4274939016 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		UnityLogWriter_Init_m3153067179(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Color::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Color__ctor_m211164015 (Color_t4024113822 * __this, float ___r0, float ___g1, float ___b2, float ___a3, const RuntimeMethod* method)
{
	{
		float L_0 = ___r0;
		__this->set_r_0(L_0);
		float L_1 = ___g1;
		__this->set_g_1(L_1);
		float L_2 = ___b2;
		__this->set_b_2(L_2);
		float L_3 = ___a3;
		__this->set_a_3(L_3);
		return;
	}
}
extern "C"  void Color__ctor_m211164015_AdjustorThunk (RuntimeObject * __this, float ___r0, float ___g1, float ___b2, float ___a3, const RuntimeMethod* method)
{
	Color_t4024113822 * _thisAdjusted = reinterpret_cast<Color_t4024113822 *>(__this + 1);
	Color__ctor_m211164015(_thisAdjusted, ___r0, ___g1, ___b2, ___a3, method);
}
// System.String UnityEngine.Color::ToString()
extern "C"  String_t* Color_ToString_m4292820139 (Color_t4024113822 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Color_ToString_m4292820139_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		ObjectU5BU5D_t4290686858* L_0 = ((ObjectU5BU5D_t4290686858*)SZArrayNew(ObjectU5BU5D_t4290686858_il2cpp_TypeInfo_var, (uint32_t)4));
		float L_1 = __this->get_r_0();
		float L_2 = L_1;
		RuntimeObject * L_3 = Box(Single_t496865882_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_3);
		ObjectU5BU5D_t4290686858* L_4 = L_0;
		float L_5 = __this->get_g_1();
		float L_6 = L_5;
		RuntimeObject * L_7 = Box(Single_t496865882_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_7);
		ObjectU5BU5D_t4290686858* L_8 = L_4;
		float L_9 = __this->get_b_2();
		float L_10 = L_9;
		RuntimeObject * L_11 = Box(Single_t496865882_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_11);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_11);
		ObjectU5BU5D_t4290686858* L_12 = L_8;
		float L_13 = __this->get_a_3();
		float L_14 = L_13;
		RuntimeObject * L_15 = Box(Single_t496865882_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, L_15);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_15);
		String_t* L_16 = UnityString_Format_m2513837565(NULL /*static, unused*/, _stringLiteral2330298027, L_12, /*hidden argument*/NULL);
		V_0 = L_16;
		goto IL_004f;
	}

IL_004f:
	{
		String_t* L_17 = V_0;
		return L_17;
	}
}
extern "C"  String_t* Color_ToString_m4292820139_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Color_t4024113822 * _thisAdjusted = reinterpret_cast<Color_t4024113822 *>(__this + 1);
	return Color_ToString_m4292820139(_thisAdjusted, method);
}
// System.Int32 UnityEngine.Color::GetHashCode()
extern "C"  int32_t Color_GetHashCode_m3278237850 (Color_t4024113822 * __this, const RuntimeMethod* method)
{
	Vector4_t4052901136  V_0;
	memset(&V_0, 0, sizeof(V_0));
	int32_t V_1 = 0;
	{
		Vector4_t4052901136  L_0 = Color_op_Implicit_m4266979643(NULL /*static, unused*/, (*(Color_t4024113822 *)__this), /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Vector4_GetHashCode_m1136089151((&V_0), /*hidden argument*/NULL);
		V_1 = L_1;
		goto IL_0020;
	}

IL_0020:
	{
		int32_t L_2 = V_1;
		return L_2;
	}
}
extern "C"  int32_t Color_GetHashCode_m3278237850_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Color_t4024113822 * _thisAdjusted = reinterpret_cast<Color_t4024113822 *>(__this + 1);
	return Color_GetHashCode_m3278237850(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Color::Equals(System.Object)
extern "C"  bool Color_Equals_m3350585184 (Color_t4024113822 * __this, RuntimeObject * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Color_Equals_m3350585184_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Color_t4024113822  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t G_B7_0 = 0;
	{
		RuntimeObject * L_0 = ___other0;
		if (((RuntimeObject *)IsInstSealed((RuntimeObject*)L_0, Color_t4024113822_il2cpp_TypeInfo_var)))
		{
			goto IL_0013;
		}
	}
	{
		V_0 = (bool)0;
		goto IL_007a;
	}

IL_0013:
	{
		RuntimeObject * L_1 = ___other0;
		V_1 = ((*(Color_t4024113822 *)((Color_t4024113822 *)UnBox(L_1, Color_t4024113822_il2cpp_TypeInfo_var))));
		float* L_2 = __this->get_address_of_r_0();
		float L_3 = (&V_1)->get_r_0();
		bool L_4 = Single_Equals_m3179646383(L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0073;
		}
	}
	{
		float* L_5 = __this->get_address_of_g_1();
		float L_6 = (&V_1)->get_g_1();
		bool L_7 = Single_Equals_m3179646383(L_5, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0073;
		}
	}
	{
		float* L_8 = __this->get_address_of_b_2();
		float L_9 = (&V_1)->get_b_2();
		bool L_10 = Single_Equals_m3179646383(L_8, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0073;
		}
	}
	{
		float* L_11 = __this->get_address_of_a_3();
		float L_12 = (&V_1)->get_a_3();
		bool L_13 = Single_Equals_m3179646383(L_11, L_12, /*hidden argument*/NULL);
		G_B7_0 = ((int32_t)(L_13));
		goto IL_0074;
	}

IL_0073:
	{
		G_B7_0 = 0;
	}

IL_0074:
	{
		V_0 = (bool)G_B7_0;
		goto IL_007a;
	}

IL_007a:
	{
		bool L_14 = V_0;
		return L_14;
	}
}
extern "C"  bool Color_Equals_m3350585184_AdjustorThunk (RuntimeObject * __this, RuntimeObject * ___other0, const RuntimeMethod* method)
{
	Color_t4024113822 * _thisAdjusted = reinterpret_cast<Color_t4024113822 *>(__this + 1);
	return Color_Equals_m3350585184(_thisAdjusted, ___other0, method);
}
// UnityEngine.Color UnityEngine.Color::get_white()
extern "C"  Color_t4024113822  Color_get_white_m3869699080 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	Color_t4024113822  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Color_t4024113822  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Color__ctor_m211164015((&L_0), (1.0f), (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_0020;
	}

IL_0020:
	{
		Color_t4024113822  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Vector4 UnityEngine.Color::op_Implicit(UnityEngine.Color)
extern "C"  Vector4_t4052901136  Color_op_Implicit_m4266979643 (RuntimeObject * __this /* static, unused */, Color_t4024113822  ___c0, const RuntimeMethod* method)
{
	Vector4_t4052901136  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = (&___c0)->get_r_0();
		float L_1 = (&___c0)->get_g_1();
		float L_2 = (&___c0)->get_b_2();
		float L_3 = (&___c0)->get_a_3();
		Vector4_t4052901136  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Vector4__ctor_m3054764420((&L_4), L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_0028;
	}

IL_0028:
	{
		Vector4_t4052901136  L_5 = V_0;
		return L_5;
	}
}
// System.Void UnityEngine.Component::.ctor()
extern "C"  void Component__ctor_m2592563656 (Component_t2160255836 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Component__ctor_m2592563656_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_t2461733472_il2cpp_TypeInfo_var);
		Object__ctor_m387379410(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
extern "C"  GameObject_t3732643618 * Component_get_gameObject_m2710707649 (Component_t2160255836 * __this, const RuntimeMethod* method)
{
	typedef GameObject_t3732643618 * (*Component_get_gameObject_m2710707649_ftn) (Component_t2160255836 *);
	static Component_get_gameObject_m2710707649_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Component_get_gameObject_m2710707649_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Component::get_gameObject()");
	GameObject_t3732643618 * retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.Component::GetComponentFastPath(System.Type,System.IntPtr)
extern "C"  void Component_GetComponentFastPath_m3479205869 (Component_t2160255836 * __this, Type_t * ___type0, intptr_t ___oneFurtherThanResultValue1, const RuntimeMethod* method)
{
	typedef void (*Component_GetComponentFastPath_m3479205869_ftn) (Component_t2160255836 *, Type_t *, intptr_t);
	static Component_GetComponentFastPath_m3479205869_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Component_GetComponentFastPath_m3479205869_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Component::GetComponentFastPath(System.Type,System.IntPtr)");
	_il2cpp_icall_func(__this, ___type0, ___oneFurtherThanResultValue1);
}
// Conversion methods for marshalling of: UnityEngine.Coroutine
extern "C" void Coroutine_t3619068392_marshal_pinvoke(const Coroutine_t3619068392& unmarshaled, Coroutine_t3619068392_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Ptr_0 = unmarshaled.get_m_Ptr_0();
}
extern "C" void Coroutine_t3619068392_marshal_pinvoke_back(const Coroutine_t3619068392_marshaled_pinvoke& marshaled, Coroutine_t3619068392& unmarshaled)
{
	intptr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	unmarshaled_m_Ptr_temp_0 = marshaled.___m_Ptr_0;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.Coroutine
extern "C" void Coroutine_t3619068392_marshal_pinvoke_cleanup(Coroutine_t3619068392_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.Coroutine
extern "C" void Coroutine_t3619068392_marshal_com(const Coroutine_t3619068392& unmarshaled, Coroutine_t3619068392_marshaled_com& marshaled)
{
	marshaled.___m_Ptr_0 = unmarshaled.get_m_Ptr_0();
}
extern "C" void Coroutine_t3619068392_marshal_com_back(const Coroutine_t3619068392_marshaled_com& marshaled, Coroutine_t3619068392& unmarshaled)
{
	intptr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	unmarshaled_m_Ptr_temp_0 = marshaled.___m_Ptr_0;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.Coroutine
extern "C" void Coroutine_t3619068392_marshal_com_cleanup(Coroutine_t3619068392_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.Coroutine::ReleaseCoroutine()
extern "C"  void Coroutine_ReleaseCoroutine_m329501834 (Coroutine_t3619068392 * __this, const RuntimeMethod* method)
{
	typedef void (*Coroutine_ReleaseCoroutine_m329501834_ftn) (Coroutine_t3619068392 *);
	static Coroutine_ReleaseCoroutine_m329501834_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Coroutine_ReleaseCoroutine_m329501834_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Coroutine::ReleaseCoroutine()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Coroutine::Finalize()
extern "C"  void Coroutine_Finalize_m1627900536 (Coroutine_t3619068392 * __this, const RuntimeMethod* method)
{
	Exception_t3361176243 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3361176243 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
	}

IL_0001:
	try
	{ // begin try (depth: 1)
		Coroutine_ReleaseCoroutine_m329501834(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x13, FINALLY_000c);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3361176243 *)e.ex;
		goto FINALLY_000c;
	}

FINALLY_000c:
	{ // begin finally (depth: 1)
		Object_Finalize_m1722074356(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(12)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(12)
	{
		IL2CPP_JUMP_TBL(0x13, IL_0013)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3361176243 *)
	}

IL_0013:
	{
		return;
	}
}
extern "C"  CSSSize_t2436738465  DelegatePInvokeWrapper_CSSMeasureFunc_t2162796896 (CSSMeasureFunc_t2162796896 * __this, intptr_t ___node0, float ___width1, int32_t ___widthMode2, float ___height3, int32_t ___heightMode4, const RuntimeMethod* method)
{
	typedef CSSSize_t2436738465  (STDCALL *PInvokeFunc)(intptr_t, float, int32_t, float, int32_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((RuntimeDelegate*)__this)->method));

	// Native function invocation
	CSSSize_t2436738465  returnValue = il2cppPInvokeFunc(___node0, ___width1, ___widthMode2, ___height3, ___heightMode4);

	return returnValue;
}
// System.Void UnityEngine.CSSLayout.CSSMeasureFunc::.ctor(System.Object,System.IntPtr)
extern "C"  void CSSMeasureFunc__ctor_m324136384 (CSSMeasureFunc_t2162796896 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// UnityEngine.CSSLayout.CSSSize UnityEngine.CSSLayout.CSSMeasureFunc::Invoke(System.IntPtr,System.Single,UnityEngine.CSSLayout.CSSMeasureMode,System.Single,UnityEngine.CSSLayout.CSSMeasureMode)
extern "C"  CSSSize_t2436738465  CSSMeasureFunc_Invoke_m2998463313 (CSSMeasureFunc_t2162796896 * __this, intptr_t ___node0, float ___width1, int32_t ___widthMode2, float ___height3, int32_t ___heightMode4, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		CSSMeasureFunc_Invoke_m2998463313((CSSMeasureFunc_t2162796896 *)__this->get_prev_9(),___node0, ___width1, ___widthMode2, ___height3, ___heightMode4, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef CSSSize_t2436738465  (*FunctionPointerType) (RuntimeObject *, void* __this, intptr_t ___node0, float ___width1, int32_t ___widthMode2, float ___height3, int32_t ___heightMode4, const RuntimeMethod* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___node0, ___width1, ___widthMode2, ___height3, ___heightMode4,(RuntimeMethod*)(__this->get_method_3()));
	}
	else
	{
		typedef CSSSize_t2436738465  (*FunctionPointerType) (void* __this, intptr_t ___node0, float ___width1, int32_t ___widthMode2, float ___height3, int32_t ___heightMode4, const RuntimeMethod* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___node0, ___width1, ___widthMode2, ___height3, ___heightMode4,(RuntimeMethod*)(__this->get_method_3()));
	}
}
// System.IAsyncResult UnityEngine.CSSLayout.CSSMeasureFunc::BeginInvoke(System.IntPtr,System.Single,UnityEngine.CSSLayout.CSSMeasureMode,System.Single,UnityEngine.CSSLayout.CSSMeasureMode,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* CSSMeasureFunc_BeginInvoke_m3083536890 (CSSMeasureFunc_t2162796896 * __this, intptr_t ___node0, float ___width1, int32_t ___widthMode2, float ___height3, int32_t ___heightMode4, AsyncCallback_t1046330627 * ___callback5, RuntimeObject * ___object6, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CSSMeasureFunc_BeginInvoke_m3083536890_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[6] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___node0);
	__d_args[1] = Box(Single_t496865882_il2cpp_TypeInfo_var, &___width1);
	__d_args[2] = Box(CSSMeasureMode_t3615638984_il2cpp_TypeInfo_var, &___widthMode2);
	__d_args[3] = Box(Single_t496865882_il2cpp_TypeInfo_var, &___height3);
	__d_args[4] = Box(CSSMeasureMode_t3615638984_il2cpp_TypeInfo_var, &___heightMode4);
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback5, (RuntimeObject*)___object6);
}
// UnityEngine.CSSLayout.CSSSize UnityEngine.CSSLayout.CSSMeasureFunc::EndInvoke(System.IAsyncResult)
extern "C"  CSSSize_t2436738465  CSSMeasureFunc_EndInvoke_m231879325 (CSSMeasureFunc_t2162796896 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	RuntimeObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(CSSSize_t2436738465 *)UnBox ((RuntimeObject*)__result);
}
// UnityEngine.CSSLayout.CSSMeasureFunc UnityEngine.CSSLayout.Native::CSSNodeGetMeasureFunc(System.IntPtr)
extern "C"  CSSMeasureFunc_t2162796896 * Native_CSSNodeGetMeasureFunc_m4025476427 (RuntimeObject * __this /* static, unused */, intptr_t ___node0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Native_CSSNodeGetMeasureFunc_m4025476427_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	WeakReference_t3998924468 * V_0 = NULL;
	CSSMeasureFunc_t2162796896 * V_1 = NULL;
	{
		V_0 = (WeakReference_t3998924468 *)NULL;
		IL2CPP_RUNTIME_CLASS_INIT(Native_t4028515775_il2cpp_TypeInfo_var);
		Dictionary_2_t990142682 * L_0 = ((Native_t4028515775_StaticFields*)il2cpp_codegen_static_fields_for(Native_t4028515775_il2cpp_TypeInfo_var))->get_s_MeasureFunctions_0();
		intptr_t L_1 = ___node0;
		NullCheck(L_0);
		bool L_2 = Dictionary_2_TryGetValue_m3843825608(L_0, L_1, (&V_0), /*hidden argument*/Dictionary_2_TryGetValue_m3843825608_RuntimeMethod_var);
		if (!L_2)
		{
			goto IL_0032;
		}
	}
	{
		WeakReference_t3998924468 * L_3 = V_0;
		NullCheck(L_3);
		bool L_4 = VirtFuncInvoker0< bool >::Invoke(4 /* System.Boolean System.WeakReference::get_IsAlive() */, L_3);
		if (!L_4)
		{
			goto IL_0032;
		}
	}
	{
		WeakReference_t3998924468 * L_5 = V_0;
		NullCheck(L_5);
		RuntimeObject * L_6 = VirtFuncInvoker0< RuntimeObject * >::Invoke(5 /* System.Object System.WeakReference::get_Target() */, L_5);
		V_1 = ((CSSMeasureFunc_t2162796896 *)IsInstSealed((RuntimeObject*)L_6, CSSMeasureFunc_t2162796896_il2cpp_TypeInfo_var));
		goto IL_003a;
	}

IL_0032:
	{
		V_1 = (CSSMeasureFunc_t2162796896 *)NULL;
		goto IL_003a;
	}

IL_003a:
	{
		CSSMeasureFunc_t2162796896 * L_7 = V_1;
		return L_7;
	}
}
// System.Void UnityEngine.CSSLayout.Native::CSSNodeMeasureInvoke(System.IntPtr,System.Single,UnityEngine.CSSLayout.CSSMeasureMode,System.Single,UnityEngine.CSSLayout.CSSMeasureMode,System.IntPtr)
extern "C"  void Native_CSSNodeMeasureInvoke_m1142609737 (RuntimeObject * __this /* static, unused */, intptr_t ___node0, float ___width1, int32_t ___widthMode2, float ___height3, int32_t ___heightMode4, intptr_t ___returnValueAddress5, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Native_CSSNodeMeasureInvoke_m1142609737_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	CSSMeasureFunc_t2162796896 * V_0 = NULL;
	{
		intptr_t L_0 = ___node0;
		IL2CPP_RUNTIME_CLASS_INIT(Native_t4028515775_il2cpp_TypeInfo_var);
		CSSMeasureFunc_t2162796896 * L_1 = Native_CSSNodeGetMeasureFunc_m4025476427(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		CSSMeasureFunc_t2162796896 * L_2 = V_0;
		if (!L_2)
		{
			goto IL_0028;
		}
	}
	{
		intptr_t L_3 = ___returnValueAddress5;
		void* L_4 = IntPtr_op_Explicit_m3354255807(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		CSSMeasureFunc_t2162796896 * L_5 = V_0;
		intptr_t L_6 = ___node0;
		float L_7 = ___width1;
		int32_t L_8 = ___widthMode2;
		float L_9 = ___height3;
		int32_t L_10 = ___heightMode4;
		NullCheck(L_5);
		CSSSize_t2436738465  L_11 = CSSMeasureFunc_Invoke_m2998463313(L_5, L_6, L_7, L_8, L_9, L_10, /*hidden argument*/NULL);
		*(CSSSize_t2436738465 *)L_4 = L_11;
	}

IL_0028:
	{
		return;
	}
}
// System.Void UnityEngine.CSSLayout.Native::.cctor()
extern "C"  void Native__cctor_m2631422355 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Native__cctor_m2631422355_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t990142682 * L_0 = (Dictionary_2_t990142682 *)il2cpp_codegen_object_new(Dictionary_2_t990142682_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m4269798443(L_0, /*hidden argument*/Dictionary_2__ctor_m4269798443_RuntimeMethod_var);
		((Native_t4028515775_StaticFields*)il2cpp_codegen_static_fields_for(Native_t4028515775_il2cpp_TypeInfo_var))->set_s_MeasureFunctions_0(L_0);
		return;
	}
}
// Conversion methods for marshalling of: UnityEngine.CullingGroup
extern "C" void CullingGroup_t1255328198_marshal_pinvoke(const CullingGroup_t1255328198& unmarshaled, CullingGroup_t1255328198_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Ptr_0 = unmarshaled.get_m_Ptr_0();
	marshaled.___m_OnStateChanged_1 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_m_OnStateChanged_1()));
}
extern "C" void CullingGroup_t1255328198_marshal_pinvoke_back(const CullingGroup_t1255328198_marshaled_pinvoke& marshaled, CullingGroup_t1255328198& unmarshaled)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CullingGroup_t1255328198_pinvoke_FromNativeMethodDefinition_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	intptr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	unmarshaled_m_Ptr_temp_0 = marshaled.___m_Ptr_0;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
	unmarshaled.set_m_OnStateChanged_1(il2cpp_codegen_marshal_function_ptr_to_delegate<StateChanged_t2514413929>(marshaled.___m_OnStateChanged_1, StateChanged_t2514413929_il2cpp_TypeInfo_var));
}
// Conversion method for clean up from marshalling of: UnityEngine.CullingGroup
extern "C" void CullingGroup_t1255328198_marshal_pinvoke_cleanup(CullingGroup_t1255328198_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.CullingGroup
extern "C" void CullingGroup_t1255328198_marshal_com(const CullingGroup_t1255328198& unmarshaled, CullingGroup_t1255328198_marshaled_com& marshaled)
{
	marshaled.___m_Ptr_0 = unmarshaled.get_m_Ptr_0();
	marshaled.___m_OnStateChanged_1 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_m_OnStateChanged_1()));
}
extern "C" void CullingGroup_t1255328198_marshal_com_back(const CullingGroup_t1255328198_marshaled_com& marshaled, CullingGroup_t1255328198& unmarshaled)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CullingGroup_t1255328198_com_FromNativeMethodDefinition_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	intptr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	unmarshaled_m_Ptr_temp_0 = marshaled.___m_Ptr_0;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
	unmarshaled.set_m_OnStateChanged_1(il2cpp_codegen_marshal_function_ptr_to_delegate<StateChanged_t2514413929>(marshaled.___m_OnStateChanged_1, StateChanged_t2514413929_il2cpp_TypeInfo_var));
}
// Conversion method for clean up from marshalling of: UnityEngine.CullingGroup
extern "C" void CullingGroup_t1255328198_marshal_com_cleanup(CullingGroup_t1255328198_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.CullingGroup::Finalize()
extern "C"  void CullingGroup_Finalize_m3186244642 (CullingGroup_t1255328198 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CullingGroup_Finalize_m3186244642_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Exception_t3361176243 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3361176243 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
	}

IL_0001:
	try
	{ // begin try (depth: 1)
		{
			intptr_t L_0 = __this->get_m_Ptr_0();
			bool L_1 = IntPtr_op_Inequality_m1374147269(NULL /*static, unused*/, L_0, (intptr_t)(0), /*hidden argument*/NULL);
			if (!L_1)
			{
				goto IL_001e;
			}
		}

IL_0016:
		{
			CullingGroup_FinalizerFailure_m3512716862(__this, /*hidden argument*/NULL);
		}

IL_001e:
		{
			IL2CPP_LEAVE(0x2A, FINALLY_0023);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3361176243 *)e.ex;
		goto FINALLY_0023;
	}

FINALLY_0023:
	{ // begin finally (depth: 1)
		Object_Finalize_m1722074356(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(35)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(35)
	{
		IL2CPP_JUMP_TBL(0x2A, IL_002a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3361176243 *)
	}

IL_002a:
	{
		return;
	}
}
// System.Void UnityEngine.CullingGroup::Dispose()
extern "C"  void CullingGroup_Dispose_m1436928443 (CullingGroup_t1255328198 * __this, const RuntimeMethod* method)
{
	typedef void (*CullingGroup_Dispose_m1436928443_ftn) (CullingGroup_t1255328198 *);
	static CullingGroup_Dispose_m1436928443_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CullingGroup_Dispose_m1436928443_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CullingGroup::Dispose()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.CullingGroup::SendEvents(UnityEngine.CullingGroup,System.IntPtr,System.Int32)
extern "C"  void CullingGroup_SendEvents_m3781512400 (RuntimeObject * __this /* static, unused */, CullingGroup_t1255328198 * ___cullingGroup0, intptr_t ___eventsPtr1, int32_t ___count2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CullingGroup_SendEvents_m3781512400_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	CullingGroupEvent_t157813866 * V_0 = NULL;
	int32_t V_1 = 0;
	{
		void* L_0 = IntPtr_ToPointer_m565950407((&___eventsPtr1), /*hidden argument*/NULL);
		V_0 = (CullingGroupEvent_t157813866 *)L_0;
		CullingGroup_t1255328198 * L_1 = ___cullingGroup0;
		NullCheck(L_1);
		StateChanged_t2514413929 * L_2 = L_1->get_m_OnStateChanged_1();
		if (L_2)
		{
			goto IL_0019;
		}
	}
	{
		goto IL_0046;
	}

IL_0019:
	{
		V_1 = 0;
		goto IL_003f;
	}

IL_0020:
	{
		CullingGroup_t1255328198 * L_3 = ___cullingGroup0;
		NullCheck(L_3);
		StateChanged_t2514413929 * L_4 = L_3->get_m_OnStateChanged_1();
		CullingGroupEvent_t157813866 * L_5 = V_0;
		int32_t L_6 = V_1;
		uint32_t L_7 = il2cpp_codegen_sizeof(CullingGroupEvent_t157813866_il2cpp_TypeInfo_var);
		NullCheck(L_4);
		StateChanged_Invoke_m1291922880(L_4, (*(CullingGroupEvent_t157813866 *)((CullingGroupEvent_t157813866 *)((intptr_t)L_5+(intptr_t)((intptr_t)((intptr_t)(((intptr_t)L_6))*(int32_t)L_7))))), /*hidden argument*/NULL);
		int32_t L_8 = V_1;
		V_1 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_003f:
	{
		int32_t L_9 = V_1;
		int32_t L_10 = ___count2;
		if ((((int32_t)L_9) < ((int32_t)L_10)))
		{
			goto IL_0020;
		}
	}

IL_0046:
	{
		return;
	}
}
// System.Void UnityEngine.CullingGroup::FinalizerFailure()
extern "C"  void CullingGroup_FinalizerFailure_m3512716862 (CullingGroup_t1255328198 * __this, const RuntimeMethod* method)
{
	typedef void (*CullingGroup_FinalizerFailure_m3512716862_ftn) (CullingGroup_t1255328198 *);
	static CullingGroup_FinalizerFailure_m3512716862_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CullingGroup_FinalizerFailure_m3512716862_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CullingGroup::FinalizerFailure()");
	_il2cpp_icall_func(__this);
}
extern "C"  void DelegatePInvokeWrapper_StateChanged_t2514413929 (StateChanged_t2514413929 * __this, CullingGroupEvent_t157813866  ___sphere0, const RuntimeMethod* method)
{
	typedef void (STDCALL *PInvokeFunc)(CullingGroupEvent_t157813866 );
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((RuntimeDelegate*)__this)->method));

	// Native function invocation
	il2cppPInvokeFunc(___sphere0);

}
// System.Void UnityEngine.CullingGroup/StateChanged::.ctor(System.Object,System.IntPtr)
extern "C"  void StateChanged__ctor_m58376528 (StateChanged_t2514413929 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.CullingGroup/StateChanged::Invoke(UnityEngine.CullingGroupEvent)
extern "C"  void StateChanged_Invoke_m1291922880 (StateChanged_t2514413929 * __this, CullingGroupEvent_t157813866  ___sphere0, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		StateChanged_Invoke_m1291922880((StateChanged_t2514413929 *)__this->get_prev_9(),___sphere0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (RuntimeObject *, void* __this, CullingGroupEvent_t157813866  ___sphere0, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___sphere0,(RuntimeMethod*)(__this->get_method_3()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, CullingGroupEvent_t157813866  ___sphere0, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___sphere0,(RuntimeMethod*)(__this->get_method_3()));
	}
}
// System.IAsyncResult UnityEngine.CullingGroup/StateChanged::BeginInvoke(UnityEngine.CullingGroupEvent,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* StateChanged_BeginInvoke_m1155489289 (StateChanged_t2514413929 * __this, CullingGroupEvent_t157813866  ___sphere0, AsyncCallback_t1046330627 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StateChanged_BeginInvoke_m1155489289_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(CullingGroupEvent_t157813866_il2cpp_TypeInfo_var, &___sphere0);
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Void UnityEngine.CullingGroup/StateChanged::EndInvoke(System.IAsyncResult)
extern "C"  void StateChanged_EndInvoke_m2092849007 (StateChanged_t2514413929 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// UnityEngine.ILogger UnityEngine.Debug::get_unityLogger()
extern "C"  RuntimeObject* Debug_get_unityLogger_m2700972930 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Debug_get_unityLogger_m2700972930_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject* V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t2165388762_il2cpp_TypeInfo_var);
		RuntimeObject* L_0 = ((Debug_t2165388762_StaticFields*)il2cpp_codegen_static_fields_for(Debug_t2165388762_il2cpp_TypeInfo_var))->get_s_Logger_0();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		RuntimeObject* L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Debug::Log(System.Object)
extern "C"  void Debug_Log_m2305178311 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___message0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Debug_Log_m2305178311_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t2165388762_il2cpp_TypeInfo_var);
		RuntimeObject* L_0 = Debug_get_unityLogger_m2700972930(NULL /*static, unused*/, /*hidden argument*/NULL);
		RuntimeObject * L_1 = ___message0;
		NullCheck(L_0);
		InterfaceActionInvoker2< int32_t, RuntimeObject * >::Invoke(0 /* System.Void UnityEngine.ILogger::Log(UnityEngine.LogType,System.Object) */, ILogger_t2992480098_il2cpp_TypeInfo_var, L_0, 3, L_1);
		return;
	}
}
// System.Void UnityEngine.Debug::LogError(System.Object)
extern "C"  void Debug_LogError_m1070074371 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___message0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Debug_LogError_m1070074371_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t2165388762_il2cpp_TypeInfo_var);
		RuntimeObject* L_0 = Debug_get_unityLogger_m2700972930(NULL /*static, unused*/, /*hidden argument*/NULL);
		RuntimeObject * L_1 = ___message0;
		NullCheck(L_0);
		InterfaceActionInvoker2< int32_t, RuntimeObject * >::Invoke(0 /* System.Void UnityEngine.ILogger::Log(UnityEngine.LogType,System.Object) */, ILogger_t2992480098_il2cpp_TypeInfo_var, L_0, 0, L_1);
		return;
	}
}
// System.Void UnityEngine.Debug::LogException(System.Exception)
extern "C"  void Debug_LogException_m57812199 (RuntimeObject * __this /* static, unused */, Exception_t3361176243 * ___exception0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Debug_LogException_m57812199_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t2165388762_il2cpp_TypeInfo_var);
		RuntimeObject* L_0 = Debug_get_unityLogger_m2700972930(NULL /*static, unused*/, /*hidden argument*/NULL);
		Exception_t3361176243 * L_1 = ___exception0;
		NullCheck(L_0);
		InterfaceActionInvoker2< Exception_t3361176243 *, Object_t2461733472 * >::Invoke(1 /* System.Void UnityEngine.ILogHandler::LogException(System.Exception,UnityEngine.Object) */, ILogHandler_t2217299658_il2cpp_TypeInfo_var, L_0, L_1, (Object_t2461733472 *)NULL);
		return;
	}
}
// System.Void UnityEngine.Debug::.cctor()
extern "C"  void Debug__cctor_m1871084662 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Debug__cctor_m1871084662_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		DebugLogHandler_t1401667417 * L_0 = (DebugLogHandler_t1401667417 *)il2cpp_codegen_object_new(DebugLogHandler_t1401667417_il2cpp_TypeInfo_var);
		DebugLogHandler__ctor_m2673380417(L_0, /*hidden argument*/NULL);
		Logger_t605719344 * L_1 = (Logger_t605719344 *)il2cpp_codegen_object_new(Logger_t605719344_il2cpp_TypeInfo_var);
		Logger__ctor_m4234190541(L_1, L_0, /*hidden argument*/NULL);
		((Debug_t2165388762_StaticFields*)il2cpp_codegen_static_fields_for(Debug_t2165388762_il2cpp_TypeInfo_var))->set_s_Logger_0(L_1);
		return;
	}
}
// System.Void UnityEngine.DebugLogHandler::.ctor()
extern "C"  void DebugLogHandler__ctor_m2673380417 (DebugLogHandler_t1401667417 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m3446193457(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.DebugLogHandler::Internal_Log(UnityEngine.LogType,System.String,UnityEngine.Object)
extern "C"  void DebugLogHandler_Internal_Log_m1682417589 (RuntimeObject * __this /* static, unused */, int32_t ___level0, String_t* ___msg1, Object_t2461733472 * ___obj2, const RuntimeMethod* method)
{
	typedef void (*DebugLogHandler_Internal_Log_m1682417589_ftn) (int32_t, String_t*, Object_t2461733472 *);
	static DebugLogHandler_Internal_Log_m1682417589_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (DebugLogHandler_Internal_Log_m1682417589_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.DebugLogHandler::Internal_Log(UnityEngine.LogType,System.String,UnityEngine.Object)");
	_il2cpp_icall_func(___level0, ___msg1, ___obj2);
}
// System.Void UnityEngine.DebugLogHandler::Internal_LogException(System.Exception,UnityEngine.Object)
extern "C"  void DebugLogHandler_Internal_LogException_m3264502143 (RuntimeObject * __this /* static, unused */, Exception_t3361176243 * ___exception0, Object_t2461733472 * ___obj1, const RuntimeMethod* method)
{
	typedef void (*DebugLogHandler_Internal_LogException_m3264502143_ftn) (Exception_t3361176243 *, Object_t2461733472 *);
	static DebugLogHandler_Internal_LogException_m3264502143_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (DebugLogHandler_Internal_LogException_m3264502143_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.DebugLogHandler::Internal_LogException(System.Exception,UnityEngine.Object)");
	_il2cpp_icall_func(___exception0, ___obj1);
}
// System.Void UnityEngine.DebugLogHandler::LogFormat(UnityEngine.LogType,UnityEngine.Object,System.String,System.Object[])
extern "C"  void DebugLogHandler_LogFormat_m3907003566 (DebugLogHandler_t1401667417 * __this, int32_t ___logType0, Object_t2461733472 * ___context1, String_t* ___format2, ObjectU5BU5D_t4290686858* ___args3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DebugLogHandler_LogFormat_m3907003566_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___logType0;
		String_t* L_1 = ___format2;
		ObjectU5BU5D_t4290686858* L_2 = ___args3;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Format_m2496254338(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		Object_t2461733472 * L_4 = ___context1;
		DebugLogHandler_Internal_Log_m1682417589(NULL /*static, unused*/, L_0, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.DebugLogHandler::LogException(System.Exception,UnityEngine.Object)
extern "C"  void DebugLogHandler_LogException_m611967214 (DebugLogHandler_t1401667417 * __this, Exception_t3361176243 * ___exception0, Object_t2461733472 * ___context1, const RuntimeMethod* method)
{
	{
		Exception_t3361176243 * L_0 = ___exception0;
		Object_t2461733472 * L_1 = ___context1;
		DebugLogHandler_Internal_LogException_m3264502143(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UnityEngine.DefaultExecutionOrder::get_order()
extern "C"  int32_t DefaultExecutionOrder_get_order_m3989798846 (DefaultExecutionOrder_t3862449190 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U3CorderU3Ek__BackingField_0();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Display::.ctor()
extern "C"  void Display__ctor_m1199150772 (Display_t4002355288 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m3446193457(__this, /*hidden argument*/NULL);
		intptr_t L_0;
		memset(&L_0, 0, sizeof(L_0));
		IntPtr__ctor_m1874125153((&L_0), 0, /*hidden argument*/NULL);
		__this->set_nativeDisplay_0(L_0);
		return;
	}
}
// System.Void UnityEngine.Display::.ctor(System.IntPtr)
extern "C"  void Display__ctor_m906575725 (Display_t4002355288 * __this, intptr_t ___nativeDisplay0, const RuntimeMethod* method)
{
	{
		Object__ctor_m3446193457(__this, /*hidden argument*/NULL);
		intptr_t L_0 = ___nativeDisplay0;
		__this->set_nativeDisplay_0(L_0);
		return;
	}
}
// System.Void UnityEngine.Display::RecreateDisplayList(System.IntPtr[])
extern "C"  void Display_RecreateDisplayList_m3512407007 (RuntimeObject * __this /* static, unused */, IntPtrU5BU5D_t2893013242* ___nativeDisplay0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Display_RecreateDisplayList_m3512407007_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		IntPtrU5BU5D_t2893013242* L_0 = ___nativeDisplay0;
		NullCheck(L_0);
		IL2CPP_RUNTIME_CLASS_INIT(Display_t4002355288_il2cpp_TypeInfo_var);
		((Display_t4002355288_StaticFields*)il2cpp_codegen_static_fields_for(Display_t4002355288_il2cpp_TypeInfo_var))->set_displays_1(((DisplayU5BU5D_t130122569*)SZArrayNew(DisplayU5BU5D_t130122569_il2cpp_TypeInfo_var, (uint32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_0)->max_length)))))));
		V_0 = 0;
		goto IL_0028;
	}

IL_0015:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Display_t4002355288_il2cpp_TypeInfo_var);
		DisplayU5BU5D_t130122569* L_1 = ((Display_t4002355288_StaticFields*)il2cpp_codegen_static_fields_for(Display_t4002355288_il2cpp_TypeInfo_var))->get_displays_1();
		int32_t L_2 = V_0;
		IntPtrU5BU5D_t2893013242* L_3 = ___nativeDisplay0;
		int32_t L_4 = V_0;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		intptr_t L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		Display_t4002355288 * L_7 = (Display_t4002355288 *)il2cpp_codegen_object_new(Display_t4002355288_il2cpp_TypeInfo_var);
		Display__ctor_m906575725(L_7, L_6, /*hidden argument*/NULL);
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_7);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(L_2), (Display_t4002355288 *)L_7);
		int32_t L_8 = V_0;
		V_0 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_0028:
	{
		int32_t L_9 = V_0;
		IntPtrU5BU5D_t2893013242* L_10 = ___nativeDisplay0;
		NullCheck(L_10);
		if ((((int32_t)L_9) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_10)->max_length)))))))
		{
			goto IL_0015;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Display_t4002355288_il2cpp_TypeInfo_var);
		DisplayU5BU5D_t130122569* L_11 = ((Display_t4002355288_StaticFields*)il2cpp_codegen_static_fields_for(Display_t4002355288_il2cpp_TypeInfo_var))->get_displays_1();
		NullCheck(L_11);
		int32_t L_12 = 0;
		Display_t4002355288 * L_13 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		((Display_t4002355288_StaticFields*)il2cpp_codegen_static_fields_for(Display_t4002355288_il2cpp_TypeInfo_var))->set__mainDisplay_2(L_13);
		return;
	}
}
// System.Void UnityEngine.Display::FireDisplaysUpdated()
extern "C"  void Display_FireDisplaysUpdated_m3194723814 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Display_FireDisplaysUpdated_m3194723814_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Display_t4002355288_il2cpp_TypeInfo_var);
		DisplaysUpdatedDelegate_t2805775822 * L_0 = ((Display_t4002355288_StaticFields*)il2cpp_codegen_static_fields_for(Display_t4002355288_il2cpp_TypeInfo_var))->get_onDisplaysUpdated_3();
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Display_t4002355288_il2cpp_TypeInfo_var);
		DisplaysUpdatedDelegate_t2805775822 * L_1 = ((Display_t4002355288_StaticFields*)il2cpp_codegen_static_fields_for(Display_t4002355288_il2cpp_TypeInfo_var))->get_onDisplaysUpdated_3();
		NullCheck(L_1);
		DisplaysUpdatedDelegate_Invoke_m1559204117(L_1, /*hidden argument*/NULL);
	}

IL_0015:
	{
		return;
	}
}
// System.Void UnityEngine.Display::.cctor()
extern "C"  void Display__cctor_m3585651147 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Display__cctor_m3585651147_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		DisplayU5BU5D_t130122569* L_0 = ((DisplayU5BU5D_t130122569*)SZArrayNew(DisplayU5BU5D_t130122569_il2cpp_TypeInfo_var, (uint32_t)1));
		Display_t4002355288 * L_1 = (Display_t4002355288 *)il2cpp_codegen_object_new(Display_t4002355288_il2cpp_TypeInfo_var);
		Display__ctor_m1199150772(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Display_t4002355288 *)L_1);
		((Display_t4002355288_StaticFields*)il2cpp_codegen_static_fields_for(Display_t4002355288_il2cpp_TypeInfo_var))->set_displays_1(L_0);
		DisplayU5BU5D_t130122569* L_2 = ((Display_t4002355288_StaticFields*)il2cpp_codegen_static_fields_for(Display_t4002355288_il2cpp_TypeInfo_var))->get_displays_1();
		NullCheck(L_2);
		int32_t L_3 = 0;
		Display_t4002355288 * L_4 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		((Display_t4002355288_StaticFields*)il2cpp_codegen_static_fields_for(Display_t4002355288_il2cpp_TypeInfo_var))->set__mainDisplay_2(L_4);
		((Display_t4002355288_StaticFields*)il2cpp_codegen_static_fields_for(Display_t4002355288_il2cpp_TypeInfo_var))->set_onDisplaysUpdated_3((DisplaysUpdatedDelegate_t2805775822 *)NULL);
		return;
	}
}
extern "C"  void DelegatePInvokeWrapper_DisplaysUpdatedDelegate_t2805775822 (DisplaysUpdatedDelegate_t2805775822 * __this, const RuntimeMethod* method)
{
	typedef void (STDCALL *PInvokeFunc)();
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((RuntimeDelegate*)__this)->method));

	// Native function invocation
	il2cppPInvokeFunc();

}
// System.Void UnityEngine.Display/DisplaysUpdatedDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void DisplaysUpdatedDelegate__ctor_m1345164380 (DisplaysUpdatedDelegate_t2805775822 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Display/DisplaysUpdatedDelegate::Invoke()
extern "C"  void DisplaysUpdatedDelegate_Invoke_m1559204117 (DisplaysUpdatedDelegate_t2805775822 * __this, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DisplaysUpdatedDelegate_Invoke_m1559204117((DisplaysUpdatedDelegate_t2805775822 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((RuntimeMethod*)(__this->get_method_3()))) && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (RuntimeObject *, void* __this, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),(RuntimeMethod*)(__this->get_method_3()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),(RuntimeMethod*)(__this->get_method_3()));
	}
}
// System.IAsyncResult UnityEngine.Display/DisplaysUpdatedDelegate::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* DisplaysUpdatedDelegate_BeginInvoke_m3281943358 (DisplaysUpdatedDelegate_t2805775822 * __this, AsyncCallback_t1046330627 * ___callback0, RuntimeObject * ___object1, const RuntimeMethod* method)
{
	void *__d_args[1] = {0};
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback0, (RuntimeObject*)___object1);
}
// System.Void UnityEngine.Display/DisplaysUpdatedDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void DisplaysUpdatedDelegate_EndInvoke_m3619400991 (DisplaysUpdatedDelegate_t2805775822 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Events.ArgumentCache::.ctor()
extern "C"  void ArgumentCache__ctor_m2562784622 (ArgumentCache_t3682645796 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m3446193457(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Object UnityEngine.Events.ArgumentCache::get_unityObjectArgument()
extern "C"  Object_t2461733472 * ArgumentCache_get_unityObjectArgument_m2559081415 (ArgumentCache_t3682645796 * __this, const RuntimeMethod* method)
{
	Object_t2461733472 * V_0 = NULL;
	{
		Object_t2461733472 * L_0 = __this->get_m_ObjectArgument_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		Object_t2461733472 * L_1 = V_0;
		return L_1;
	}
}
// System.String UnityEngine.Events.ArgumentCache::get_unityObjectArgumentAssemblyTypeName()
extern "C"  String_t* ArgumentCache_get_unityObjectArgumentAssemblyTypeName_m3489746291 (ArgumentCache_t3682645796 * __this, const RuntimeMethod* method)
{
	String_t* V_0 = NULL;
	{
		String_t* L_0 = __this->get_m_ObjectArgumentAssemblyTypeName_1();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		String_t* L_1 = V_0;
		return L_1;
	}
}
// System.Int32 UnityEngine.Events.ArgumentCache::get_intArgument()
extern "C"  int32_t ArgumentCache_get_intArgument_m1266469321 (ArgumentCache_t3682645796 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_m_IntArgument_2();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Single UnityEngine.Events.ArgumentCache::get_floatArgument()
extern "C"  float ArgumentCache_get_floatArgument_m703828235 (ArgumentCache_t3682645796 * __this, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_FloatArgument_3();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		float L_1 = V_0;
		return L_1;
	}
}
// System.String UnityEngine.Events.ArgumentCache::get_stringArgument()
extern "C"  String_t* ArgumentCache_get_stringArgument_m574520374 (ArgumentCache_t3682645796 * __this, const RuntimeMethod* method)
{
	String_t* V_0 = NULL;
	{
		String_t* L_0 = __this->get_m_StringArgument_4();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		String_t* L_1 = V_0;
		return L_1;
	}
}
// System.Boolean UnityEngine.Events.ArgumentCache::get_boolArgument()
extern "C"  bool ArgumentCache_get_boolArgument_m600683273 (ArgumentCache_t3682645796 * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		bool L_0 = __this->get_m_BoolArgument_5();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Events.ArgumentCache::TidyAssemblyTypeName()
extern "C"  void ArgumentCache_TidyAssemblyTypeName_m2531759392 (ArgumentCache_t3682645796 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ArgumentCache_TidyAssemblyTypeName_m2531759392_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		String_t* L_0 = __this->get_m_ObjectArgumentAssemblyTypeName_1();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m3560539680(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		goto IL_00e4;
	}

IL_0016:
	{
		V_0 = ((int32_t)2147483647LL);
		String_t* L_2 = __this->get_m_ObjectArgumentAssemblyTypeName_1();
		NullCheck(L_2);
		int32_t L_3 = String_IndexOf_m374129865(L_2, _stringLiteral2088885139, /*hidden argument*/NULL);
		V_1 = L_3;
		int32_t L_4 = V_1;
		if ((((int32_t)L_4) == ((int32_t)(-1))))
		{
			goto IL_003c;
		}
	}
	{
		int32_t L_5 = V_1;
		int32_t L_6 = V_0;
		int32_t L_7 = Math_Min_m1276480861(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		V_0 = L_7;
	}

IL_003c:
	{
		String_t* L_8 = __this->get_m_ObjectArgumentAssemblyTypeName_1();
		NullCheck(L_8);
		int32_t L_9 = String_IndexOf_m374129865(L_8, _stringLiteral3374150706, /*hidden argument*/NULL);
		V_1 = L_9;
		int32_t L_10 = V_1;
		if ((((int32_t)L_10) == ((int32_t)(-1))))
		{
			goto IL_005c;
		}
	}
	{
		int32_t L_11 = V_1;
		int32_t L_12 = V_0;
		int32_t L_13 = Math_Min_m1276480861(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/NULL);
		V_0 = L_13;
	}

IL_005c:
	{
		String_t* L_14 = __this->get_m_ObjectArgumentAssemblyTypeName_1();
		NullCheck(L_14);
		int32_t L_15 = String_IndexOf_m374129865(L_14, _stringLiteral4013295064, /*hidden argument*/NULL);
		V_1 = L_15;
		int32_t L_16 = V_1;
		if ((((int32_t)L_16) == ((int32_t)(-1))))
		{
			goto IL_007c;
		}
	}
	{
		int32_t L_17 = V_1;
		int32_t L_18 = V_0;
		int32_t L_19 = Math_Min_m1276480861(NULL /*static, unused*/, L_17, L_18, /*hidden argument*/NULL);
		V_0 = L_19;
	}

IL_007c:
	{
		int32_t L_20 = V_0;
		if ((((int32_t)L_20) == ((int32_t)((int32_t)2147483647LL))))
		{
			goto IL_009a;
		}
	}
	{
		String_t* L_21 = __this->get_m_ObjectArgumentAssemblyTypeName_1();
		int32_t L_22 = V_0;
		NullCheck(L_21);
		String_t* L_23 = String_Substring_m2404821311(L_21, 0, L_22, /*hidden argument*/NULL);
		__this->set_m_ObjectArgumentAssemblyTypeName_1(L_23);
	}

IL_009a:
	{
		String_t* L_24 = __this->get_m_ObjectArgumentAssemblyTypeName_1();
		NullCheck(L_24);
		int32_t L_25 = String_IndexOf_m374129865(L_24, _stringLiteral1114441295, /*hidden argument*/NULL);
		V_1 = L_25;
		int32_t L_26 = V_1;
		if ((((int32_t)L_26) == ((int32_t)(-1))))
		{
			goto IL_00e4;
		}
	}
	{
		String_t* L_27 = __this->get_m_ObjectArgumentAssemblyTypeName_1();
		NullCheck(L_27);
		bool L_28 = String_EndsWith_m2915171759(L_27, _stringLiteral992642132, /*hidden argument*/NULL);
		if (!L_28)
		{
			goto IL_00e4;
		}
	}
	{
		String_t* L_29 = __this->get_m_ObjectArgumentAssemblyTypeName_1();
		int32_t L_30 = V_1;
		NullCheck(L_29);
		String_t* L_31 = String_Substring_m2404821311(L_29, 0, L_30, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_32 = String_Concat_m47853911(NULL /*static, unused*/, L_31, _stringLiteral3749740686, /*hidden argument*/NULL);
		__this->set_m_ObjectArgumentAssemblyTypeName_1(L_32);
	}

IL_00e4:
	{
		return;
	}
}
// System.Void UnityEngine.Events.ArgumentCache::OnBeforeSerialize()
extern "C"  void ArgumentCache_OnBeforeSerialize_m2612988137 (ArgumentCache_t3682645796 * __this, const RuntimeMethod* method)
{
	{
		ArgumentCache_TidyAssemblyTypeName_m2531759392(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.ArgumentCache::OnAfterDeserialize()
extern "C"  void ArgumentCache_OnAfterDeserialize_m4282301992 (ArgumentCache_t3682645796 * __this, const RuntimeMethod* method)
{
	{
		ArgumentCache_TidyAssemblyTypeName_m2531759392(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.BaseInvokableCall::.ctor(System.Object,System.Reflection.MethodInfo)
extern "C"  void BaseInvokableCall__ctor_m3419792134 (BaseInvokableCall_t2920707967 * __this, RuntimeObject * ___target0, MethodInfo_t * ___function1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BaseInvokableCall__ctor_m3419792134_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object__ctor_m3446193457(__this, /*hidden argument*/NULL);
		RuntimeObject * L_0 = ___target0;
		if (L_0)
		{
			goto IL_0018;
		}
	}
	{
		ArgumentNullException_t517973450 * L_1 = (ArgumentNullException_t517973450 *)il2cpp_codegen_object_new(ArgumentNullException_t517973450_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m2607242403(L_1, _stringLiteral2440078237, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		MethodInfo_t * L_2 = ___function1;
		if (L_2)
		{
			goto IL_0029;
		}
	}
	{
		ArgumentNullException_t517973450 * L_3 = (ArgumentNullException_t517973450 *)il2cpp_codegen_object_new(ArgumentNullException_t517973450_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m2607242403(L_3, _stringLiteral3966407617, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0029:
	{
		return;
	}
}
// System.Boolean UnityEngine.Events.BaseInvokableCall::AllowInvoke(System.Delegate)
extern "C"  bool BaseInvokableCall_AllowInvoke_m1751538175 (RuntimeObject * __this /* static, unused */, Delegate_t96267039 * ___delegate0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BaseInvokableCall_AllowInvoke_m1751538175_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject * V_0 = NULL;
	bool V_1 = false;
	Object_t2461733472 * V_2 = NULL;
	{
		Delegate_t96267039 * L_0 = ___delegate0;
		NullCheck(L_0);
		RuntimeObject * L_1 = Delegate_get_Target_m1823718869(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		RuntimeObject * L_2 = V_0;
		if (L_2)
		{
			goto IL_0015;
		}
	}
	{
		V_1 = (bool)1;
		goto IL_003c;
	}

IL_0015:
	{
		RuntimeObject * L_3 = V_0;
		V_2 = ((Object_t2461733472 *)IsInstClass((RuntimeObject*)L_3, Object_t2461733472_il2cpp_TypeInfo_var));
		Object_t2461733472 * L_4 = V_2;
		bool L_5 = Object_ReferenceEquals_m1923645570(NULL /*static, unused*/, L_4, NULL, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_0035;
		}
	}
	{
		Object_t2461733472 * L_6 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t2461733472_il2cpp_TypeInfo_var);
		bool L_7 = Object_op_Inequality_m3819358377(NULL /*static, unused*/, L_6, (Object_t2461733472 *)NULL, /*hidden argument*/NULL);
		V_1 = L_7;
		goto IL_003c;
	}

IL_0035:
	{
		V_1 = (bool)1;
		goto IL_003c;
	}

IL_003c:
	{
		bool L_8 = V_1;
		return L_8;
	}
}
// System.Void UnityEngine.Events.InvokableCall::.ctor(System.Object,System.Reflection.MethodInfo)
extern "C"  void InvokableCall__ctor_m2974797033 (InvokableCall_t2876658856 * __this, RuntimeObject * ___target0, MethodInfo_t * ___theFunction1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall__ctor_m2974797033_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		BaseInvokableCall__ctor_m3419792134(__this, L_0, L_1, /*hidden argument*/NULL);
		MethodInfo_t * L_2 = ___theFunction1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m3171413529(NULL /*static, unused*/, LoadTypeToken(UnityAction_t35329723_0_0_0_var), /*hidden argument*/NULL);
		RuntimeObject * L_4 = ___target0;
		Delegate_t96267039 * L_5 = NetFxCoreExtensions_CreateDelegate_m3544893108(NULL /*static, unused*/, L_2, L_3, L_4, /*hidden argument*/NULL);
		InvokableCall_add_Delegate_m2772852427(__this, ((UnityAction_t35329723 *)CastclassSealed((RuntimeObject*)L_5, UnityAction_t35329723_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall::add_Delegate(UnityEngine.Events.UnityAction)
extern "C"  void InvokableCall_add_Delegate_m2772852427 (InvokableCall_t2876658856 * __this, UnityAction_t35329723 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_add_Delegate_m2772852427_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	UnityAction_t35329723 * V_0 = NULL;
	UnityAction_t35329723 * V_1 = NULL;
	{
		UnityAction_t35329723 * L_0 = __this->get_Delegate_0();
		V_0 = L_0;
	}

IL_0007:
	{
		UnityAction_t35329723 * L_1 = V_0;
		V_1 = L_1;
		UnityAction_t35329723 ** L_2 = __this->get_address_of_Delegate_0();
		UnityAction_t35329723 * L_3 = V_1;
		UnityAction_t35329723 * L_4 = ___value0;
		Delegate_t96267039 * L_5 = Delegate_Combine_m1239998138(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		UnityAction_t35329723 * L_6 = V_0;
		UnityAction_t35329723 * L_7 = InterlockedCompareExchangeImpl<UnityAction_t35329723 *>(L_2, ((UnityAction_t35329723 *)CastclassSealed((RuntimeObject*)L_5, UnityAction_t35329723_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		UnityAction_t35329723 * L_8 = V_0;
		UnityAction_t35329723 * L_9 = V_1;
		if ((!(((RuntimeObject*)(UnityAction_t35329723 *)L_8) == ((RuntimeObject*)(UnityAction_t35329723 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall::remove_Delegate(UnityEngine.Events.UnityAction)
extern "C"  void InvokableCall_remove_Delegate_m599836031 (InvokableCall_t2876658856 * __this, UnityAction_t35329723 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_remove_Delegate_m599836031_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	UnityAction_t35329723 * V_0 = NULL;
	UnityAction_t35329723 * V_1 = NULL;
	{
		UnityAction_t35329723 * L_0 = __this->get_Delegate_0();
		V_0 = L_0;
	}

IL_0007:
	{
		UnityAction_t35329723 * L_1 = V_0;
		V_1 = L_1;
		UnityAction_t35329723 ** L_2 = __this->get_address_of_Delegate_0();
		UnityAction_t35329723 * L_3 = V_1;
		UnityAction_t35329723 * L_4 = ___value0;
		Delegate_t96267039 * L_5 = Delegate_Remove_m1542158(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		UnityAction_t35329723 * L_6 = V_0;
		UnityAction_t35329723 * L_7 = InterlockedCompareExchangeImpl<UnityAction_t35329723 *>(L_2, ((UnityAction_t35329723 *)CastclassSealed((RuntimeObject*)L_5, UnityAction_t35329723_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		UnityAction_t35329723 * L_8 = V_0;
		UnityAction_t35329723 * L_9 = V_1;
		if ((!(((RuntimeObject*)(UnityAction_t35329723 *)L_8) == ((RuntimeObject*)(UnityAction_t35329723 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall::Invoke(System.Object[])
extern "C"  void InvokableCall_Invoke_m1952365720 (InvokableCall_t2876658856 * __this, ObjectU5BU5D_t4290686858* ___args0, const RuntimeMethod* method)
{
	{
		UnityAction_t35329723 * L_0 = __this->get_Delegate_0();
		bool L_1 = BaseInvokableCall_AllowInvoke_m1751538175(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		UnityAction_t35329723 * L_2 = __this->get_Delegate_0();
		NullCheck(L_2);
		UnityAction_Invoke_m2515763748(L_2, /*hidden argument*/NULL);
	}

IL_001c:
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCallList::.ctor()
extern "C"  void InvokableCallList__ctor_m386930243 (InvokableCallList_t1673367030 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCallList__ctor_m386930243_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t799276283 * L_0 = (List_1_t799276283 *)il2cpp_codegen_object_new(List_1_t799276283_il2cpp_TypeInfo_var);
		List_1__ctor_m2027075001(L_0, /*hidden argument*/List_1__ctor_m2027075001_RuntimeMethod_var);
		__this->set_m_PersistentCalls_0(L_0);
		List_1_t799276283 * L_1 = (List_1_t799276283 *)il2cpp_codegen_object_new(List_1_t799276283_il2cpp_TypeInfo_var);
		List_1__ctor_m2027075001(L_1, /*hidden argument*/List_1__ctor_m2027075001_RuntimeMethod_var);
		__this->set_m_RuntimeCalls_1(L_1);
		List_1_t799276283 * L_2 = (List_1_t799276283 *)il2cpp_codegen_object_new(List_1_t799276283_il2cpp_TypeInfo_var);
		List_1__ctor_m2027075001(L_2, /*hidden argument*/List_1__ctor_m2027075001_RuntimeMethod_var);
		__this->set_m_ExecutingCalls_2(L_2);
		__this->set_m_NeedsUpdate_3((bool)1);
		Object__ctor_m3446193457(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCallList::AddPersistentInvokableCall(UnityEngine.Events.BaseInvokableCall)
extern "C"  void InvokableCallList_AddPersistentInvokableCall_m3306354928 (InvokableCallList_t1673367030 * __this, BaseInvokableCall_t2920707967 * ___call0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCallList_AddPersistentInvokableCall_m3306354928_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t799276283 * L_0 = __this->get_m_PersistentCalls_0();
		BaseInvokableCall_t2920707967 * L_1 = ___call0;
		NullCheck(L_0);
		List_1_Add_m3979596576(L_0, L_1, /*hidden argument*/List_1_Add_m3979596576_RuntimeMethod_var);
		__this->set_m_NeedsUpdate_3((bool)1);
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCallList::ClearPersistent()
extern "C"  void InvokableCallList_ClearPersistent_m1797712781 (InvokableCallList_t1673367030 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCallList_ClearPersistent_m1797712781_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t799276283 * L_0 = __this->get_m_PersistentCalls_0();
		NullCheck(L_0);
		List_1_Clear_m550264580(L_0, /*hidden argument*/List_1_Clear_m550264580_RuntimeMethod_var);
		__this->set_m_NeedsUpdate_3((bool)1);
		return;
	}
}
// System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall> UnityEngine.Events.InvokableCallList::PrepareInvoke()
extern "C"  List_1_t799276283 * InvokableCallList_PrepareInvoke_m2613780459 (InvokableCallList_t1673367030 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCallList_PrepareInvoke_m2613780459_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t799276283 * V_0 = NULL;
	{
		bool L_0 = __this->get_m_NeedsUpdate_3();
		if (!L_0)
		{
			goto IL_0042;
		}
	}
	{
		List_1_t799276283 * L_1 = __this->get_m_ExecutingCalls_2();
		NullCheck(L_1);
		List_1_Clear_m550264580(L_1, /*hidden argument*/List_1_Clear_m550264580_RuntimeMethod_var);
		List_1_t799276283 * L_2 = __this->get_m_ExecutingCalls_2();
		List_1_t799276283 * L_3 = __this->get_m_PersistentCalls_0();
		NullCheck(L_2);
		List_1_AddRange_m3917463589(L_2, L_3, /*hidden argument*/List_1_AddRange_m3917463589_RuntimeMethod_var);
		List_1_t799276283 * L_4 = __this->get_m_ExecutingCalls_2();
		List_1_t799276283 * L_5 = __this->get_m_RuntimeCalls_1();
		NullCheck(L_4);
		List_1_AddRange_m3917463589(L_4, L_5, /*hidden argument*/List_1_AddRange_m3917463589_RuntimeMethod_var);
		__this->set_m_NeedsUpdate_3((bool)0);
	}

IL_0042:
	{
		List_1_t799276283 * L_6 = __this->get_m_ExecutingCalls_2();
		V_0 = L_6;
		goto IL_004e;
	}

IL_004e:
	{
		List_1_t799276283 * L_7 = V_0;
		return L_7;
	}
}
// System.Void UnityEngine.Events.PersistentCall::.ctor()
extern "C"  void PersistentCall__ctor_m2649643936 (PersistentCall_t2687057912 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PersistentCall__ctor_m2649643936_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_m_Mode_2(0);
		ArgumentCache_t3682645796 * L_0 = (ArgumentCache_t3682645796 *)il2cpp_codegen_object_new(ArgumentCache_t3682645796_il2cpp_TypeInfo_var);
		ArgumentCache__ctor_m2562784622(L_0, /*hidden argument*/NULL);
		__this->set_m_Arguments_3(L_0);
		__this->set_m_CallState_4(2);
		Object__ctor_m3446193457(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Object UnityEngine.Events.PersistentCall::get_target()
extern "C"  Object_t2461733472 * PersistentCall_get_target_m4082064461 (PersistentCall_t2687057912 * __this, const RuntimeMethod* method)
{
	Object_t2461733472 * V_0 = NULL;
	{
		Object_t2461733472 * L_0 = __this->get_m_Target_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		Object_t2461733472 * L_1 = V_0;
		return L_1;
	}
}
// System.String UnityEngine.Events.PersistentCall::get_methodName()
extern "C"  String_t* PersistentCall_get_methodName_m2687182531 (PersistentCall_t2687057912 * __this, const RuntimeMethod* method)
{
	String_t* V_0 = NULL;
	{
		String_t* L_0 = __this->get_m_MethodName_1();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		String_t* L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Events.PersistentListenerMode UnityEngine.Events.PersistentCall::get_mode()
extern "C"  int32_t PersistentCall_get_mode_m2357383679 (PersistentCall_t2687057912 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_m_Mode_2();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Events.ArgumentCache UnityEngine.Events.PersistentCall::get_arguments()
extern "C"  ArgumentCache_t3682645796 * PersistentCall_get_arguments_m1844117721 (PersistentCall_t2687057912 * __this, const RuntimeMethod* method)
{
	ArgumentCache_t3682645796 * V_0 = NULL;
	{
		ArgumentCache_t3682645796 * L_0 = __this->get_m_Arguments_3();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		ArgumentCache_t3682645796 * L_1 = V_0;
		return L_1;
	}
}
// System.Boolean UnityEngine.Events.PersistentCall::IsValid()
extern "C"  bool PersistentCall_IsValid_m2172430097 (PersistentCall_t2687057912 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PersistentCall_IsValid_m2172430097_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		Object_t2461733472 * L_0 = PersistentCall_get_target_m4082064461(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t2461733472_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m3819358377(NULL /*static, unused*/, L_0, (Object_t2461733472 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		String_t* L_2 = PersistentCall_get_methodName_m2687182531(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_IsNullOrEmpty_m3560539680(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		G_B3_0 = ((((int32_t)L_3) == ((int32_t)0))? 1 : 0);
		goto IL_0023;
	}

IL_0022:
	{
		G_B3_0 = 0;
	}

IL_0023:
	{
		V_0 = (bool)G_B3_0;
		goto IL_0029;
	}

IL_0029:
	{
		bool L_4 = V_0;
		return L_4;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.PersistentCall::GetRuntimeCall(UnityEngine.Events.UnityEventBase)
extern "C"  BaseInvokableCall_t2920707967 * PersistentCall_GetRuntimeCall_m1057898321 (PersistentCall_t2687057912 * __this, UnityEventBase_t3171537072 * ___theEvent0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PersistentCall_GetRuntimeCall_m1057898321_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	BaseInvokableCall_t2920707967 * V_0 = NULL;
	MethodInfo_t * V_1 = NULL;
	int32_t V_2 = 0;
	{
		int32_t L_0 = __this->get_m_CallState_4();
		if (!L_0)
		{
			goto IL_0012;
		}
	}
	{
		UnityEventBase_t3171537072 * L_1 = ___theEvent0;
		if (L_1)
		{
			goto IL_0019;
		}
	}

IL_0012:
	{
		V_0 = (BaseInvokableCall_t2920707967 *)NULL;
		goto IL_0114;
	}

IL_0019:
	{
		UnityEventBase_t3171537072 * L_2 = ___theEvent0;
		NullCheck(L_2);
		MethodInfo_t * L_3 = UnityEventBase_FindMethod_m595753448(L_2, __this, /*hidden argument*/NULL);
		V_1 = L_3;
		MethodInfo_t * L_4 = V_1;
		if (L_4)
		{
			goto IL_002e;
		}
	}
	{
		V_0 = (BaseInvokableCall_t2920707967 *)NULL;
		goto IL_0114;
	}

IL_002e:
	{
		int32_t L_5 = __this->get_m_Mode_2();
		V_2 = L_5;
		int32_t L_6 = V_2;
		switch (L_6)
		{
			case 0:
			{
				goto IL_005c;
			}
			case 1:
			{
				goto IL_00fb;
			}
			case 2:
			{
				goto IL_006f;
			}
			case 3:
			{
				goto IL_00a4;
			}
			case 4:
			{
				goto IL_0087;
			}
			case 5:
			{
				goto IL_00c1;
			}
			case 6:
			{
				goto IL_00de;
			}
		}
	}
	{
		goto IL_010d;
	}

IL_005c:
	{
		UnityEventBase_t3171537072 * L_7 = ___theEvent0;
		Object_t2461733472 * L_8 = PersistentCall_get_target_m4082064461(__this, /*hidden argument*/NULL);
		MethodInfo_t * L_9 = V_1;
		NullCheck(L_7);
		BaseInvokableCall_t2920707967 * L_10 = VirtFuncInvoker2< BaseInvokableCall_t2920707967 *, RuntimeObject *, MethodInfo_t * >::Invoke(7 /* UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEventBase::GetDelegate(System.Object,System.Reflection.MethodInfo) */, L_7, L_8, L_9);
		V_0 = L_10;
		goto IL_0114;
	}

IL_006f:
	{
		Object_t2461733472 * L_11 = PersistentCall_get_target_m4082064461(__this, /*hidden argument*/NULL);
		MethodInfo_t * L_12 = V_1;
		ArgumentCache_t3682645796 * L_13 = __this->get_m_Arguments_3();
		BaseInvokableCall_t2920707967 * L_14 = PersistentCall_GetObjectCall_m2893916170(NULL /*static, unused*/, L_11, L_12, L_13, /*hidden argument*/NULL);
		V_0 = L_14;
		goto IL_0114;
	}

IL_0087:
	{
		Object_t2461733472 * L_15 = PersistentCall_get_target_m4082064461(__this, /*hidden argument*/NULL);
		MethodInfo_t * L_16 = V_1;
		ArgumentCache_t3682645796 * L_17 = __this->get_m_Arguments_3();
		NullCheck(L_17);
		float L_18 = ArgumentCache_get_floatArgument_m703828235(L_17, /*hidden argument*/NULL);
		CachedInvokableCall_1_t1592702797 * L_19 = (CachedInvokableCall_1_t1592702797 *)il2cpp_codegen_object_new(CachedInvokableCall_1_t1592702797_il2cpp_TypeInfo_var);
		CachedInvokableCall_1__ctor_m2316491615(L_19, L_15, L_16, L_18, /*hidden argument*/CachedInvokableCall_1__ctor_m2316491615_RuntimeMethod_var);
		V_0 = L_19;
		goto IL_0114;
	}

IL_00a4:
	{
		Object_t2461733472 * L_20 = PersistentCall_get_target_m4082064461(__this, /*hidden argument*/NULL);
		MethodInfo_t * L_21 = V_1;
		ArgumentCache_t3682645796 * L_22 = __this->get_m_Arguments_3();
		NullCheck(L_22);
		int32_t L_23 = ArgumentCache_get_intArgument_m1266469321(L_22, /*hidden argument*/NULL);
		CachedInvokableCall_1_t3666972114 * L_24 = (CachedInvokableCall_1_t3666972114 *)il2cpp_codegen_object_new(CachedInvokableCall_1_t3666972114_il2cpp_TypeInfo_var);
		CachedInvokableCall_1__ctor_m245935429(L_24, L_20, L_21, L_23, /*hidden argument*/CachedInvokableCall_1__ctor_m245935429_RuntimeMethod_var);
		V_0 = L_24;
		goto IL_0114;
	}

IL_00c1:
	{
		Object_t2461733472 * L_25 = PersistentCall_get_target_m4082064461(__this, /*hidden argument*/NULL);
		MethodInfo_t * L_26 = V_1;
		ArgumentCache_t3682645796 * L_27 = __this->get_m_Arguments_3();
		NullCheck(L_27);
		String_t* L_28 = ArgumentCache_get_stringArgument_m574520374(L_27, /*hidden argument*/NULL);
		CachedInvokableCall_1_t4079887551 * L_29 = (CachedInvokableCall_1_t4079887551 *)il2cpp_codegen_object_new(CachedInvokableCall_1_t4079887551_il2cpp_TypeInfo_var);
		CachedInvokableCall_1__ctor_m2544757654(L_29, L_25, L_26, L_28, /*hidden argument*/CachedInvokableCall_1__ctor_m2544757654_RuntimeMethod_var);
		V_0 = L_29;
		goto IL_0114;
	}

IL_00de:
	{
		Object_t2461733472 * L_30 = PersistentCall_get_target_m4082064461(__this, /*hidden argument*/NULL);
		MethodInfo_t * L_31 = V_1;
		ArgumentCache_t3682645796 * L_32 = __this->get_m_Arguments_3();
		NullCheck(L_32);
		bool L_33 = ArgumentCache_get_boolArgument_m600683273(L_32, /*hidden argument*/NULL);
		CachedInvokableCall_1_t3155509814 * L_34 = (CachedInvokableCall_1_t3155509814 *)il2cpp_codegen_object_new(CachedInvokableCall_1_t3155509814_il2cpp_TypeInfo_var);
		CachedInvokableCall_1__ctor_m498507028(L_34, L_30, L_31, L_33, /*hidden argument*/CachedInvokableCall_1__ctor_m498507028_RuntimeMethod_var);
		V_0 = L_34;
		goto IL_0114;
	}

IL_00fb:
	{
		Object_t2461733472 * L_35 = PersistentCall_get_target_m4082064461(__this, /*hidden argument*/NULL);
		MethodInfo_t * L_36 = V_1;
		InvokableCall_t2876658856 * L_37 = (InvokableCall_t2876658856 *)il2cpp_codegen_object_new(InvokableCall_t2876658856_il2cpp_TypeInfo_var);
		InvokableCall__ctor_m2974797033(L_37, L_35, L_36, /*hidden argument*/NULL);
		V_0 = L_37;
		goto IL_0114;
	}

IL_010d:
	{
		V_0 = (BaseInvokableCall_t2920707967 *)NULL;
		goto IL_0114;
	}

IL_0114:
	{
		BaseInvokableCall_t2920707967 * L_38 = V_0;
		return L_38;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.PersistentCall::GetObjectCall(UnityEngine.Object,System.Reflection.MethodInfo,UnityEngine.Events.ArgumentCache)
extern "C"  BaseInvokableCall_t2920707967 * PersistentCall_GetObjectCall_m2893916170 (RuntimeObject * __this /* static, unused */, Object_t2461733472 * ___target0, MethodInfo_t * ___method1, ArgumentCache_t3682645796 * ___arguments2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PersistentCall_GetObjectCall_m2893916170_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	Type_t * V_1 = NULL;
	Type_t * V_2 = NULL;
	ConstructorInfo_t3360520918 * V_3 = NULL;
	Object_t2461733472 * V_4 = NULL;
	BaseInvokableCall_t2920707967 * V_5 = NULL;
	Type_t * G_B3_0 = NULL;
	Type_t * G_B2_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3171413529(NULL /*static, unused*/, LoadTypeToken(Object_t2461733472_0_0_0_var), /*hidden argument*/NULL);
		V_0 = L_0;
		ArgumentCache_t3682645796 * L_1 = ___arguments2;
		NullCheck(L_1);
		String_t* L_2 = ArgumentCache_get_unityObjectArgumentAssemblyTypeName_m3489746291(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_IsNullOrEmpty_m3560539680(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_003a;
		}
	}
	{
		ArgumentCache_t3682645796 * L_4 = ___arguments2;
		NullCheck(L_4);
		String_t* L_5 = ArgumentCache_get_unityObjectArgumentAssemblyTypeName_m3489746291(L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_6 = il2cpp_codegen_get_type((Il2CppMethodPointer)&Type_GetType_m418477468, L_5, (bool)0, "UnityEngine.CoreModule, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null");
		Type_t * L_7 = L_6;
		G_B2_0 = L_7;
		if (L_7)
		{
			G_B3_0 = L_7;
			goto IL_0039;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_8 = Type_GetTypeFromHandle_m3171413529(NULL /*static, unused*/, LoadTypeToken(Object_t2461733472_0_0_0_var), /*hidden argument*/NULL);
		G_B3_0 = L_8;
	}

IL_0039:
	{
		V_0 = G_B3_0;
	}

IL_003a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_9 = Type_GetTypeFromHandle_m3171413529(NULL /*static, unused*/, LoadTypeToken(CachedInvokableCall_1_t4137603102_0_0_0_var), /*hidden argument*/NULL);
		V_1 = L_9;
		Type_t * L_10 = V_1;
		TypeU5BU5D_t1830665827* L_11 = ((TypeU5BU5D_t1830665827*)SZArrayNew(TypeU5BU5D_t1830665827_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_12 = V_0;
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_12);
		NullCheck(L_10);
		Type_t * L_13 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1830665827* >::Invoke(79 /* System.Type System.Type::MakeGenericType(System.Type[]) */, L_10, L_11);
		V_2 = L_13;
		Type_t * L_14 = V_2;
		TypeU5BU5D_t1830665827* L_15 = ((TypeU5BU5D_t1830665827*)SZArrayNew(TypeU5BU5D_t1830665827_il2cpp_TypeInfo_var, (uint32_t)3));
		Type_t * L_16 = Type_GetTypeFromHandle_m3171413529(NULL /*static, unused*/, LoadTypeToken(Object_t2461733472_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_15);
		ArrayElementTypeCheck (L_15, L_16);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_16);
		TypeU5BU5D_t1830665827* L_17 = L_15;
		Type_t * L_18 = Type_GetTypeFromHandle_m3171413529(NULL /*static, unused*/, LoadTypeToken(MethodInfo_t_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_17);
		ArrayElementTypeCheck (L_17, L_18);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(1), (Type_t *)L_18);
		TypeU5BU5D_t1830665827* L_19 = L_17;
		Type_t * L_20 = V_0;
		NullCheck(L_19);
		ArrayElementTypeCheck (L_19, L_20);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(2), (Type_t *)L_20);
		NullCheck(L_14);
		ConstructorInfo_t3360520918 * L_21 = Type_GetConstructor_m272770726(L_14, L_19, /*hidden argument*/NULL);
		V_3 = L_21;
		ArgumentCache_t3682645796 * L_22 = ___arguments2;
		NullCheck(L_22);
		Object_t2461733472 * L_23 = ArgumentCache_get_unityObjectArgument_m2559081415(L_22, /*hidden argument*/NULL);
		V_4 = L_23;
		Object_t2461733472 * L_24 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t2461733472_il2cpp_TypeInfo_var);
		bool L_25 = Object_op_Inequality_m3819358377(NULL /*static, unused*/, L_24, (Object_t2461733472 *)NULL, /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_00ab;
		}
	}
	{
		Type_t * L_26 = V_0;
		Object_t2461733472 * L_27 = V_4;
		NullCheck(L_27);
		Type_t * L_28 = Object_GetType_m4087638284(L_27, /*hidden argument*/NULL);
		NullCheck(L_26);
		bool L_29 = VirtFuncInvoker1< bool, Type_t * >::Invoke(40 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, L_26, L_28);
		if (L_29)
		{
			goto IL_00ab;
		}
	}
	{
		V_4 = (Object_t2461733472 *)NULL;
	}

IL_00ab:
	{
		ConstructorInfo_t3360520918 * L_30 = V_3;
		ObjectU5BU5D_t4290686858* L_31 = ((ObjectU5BU5D_t4290686858*)SZArrayNew(ObjectU5BU5D_t4290686858_il2cpp_TypeInfo_var, (uint32_t)3));
		Object_t2461733472 * L_32 = ___target0;
		NullCheck(L_31);
		ArrayElementTypeCheck (L_31, L_32);
		(L_31)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_32);
		ObjectU5BU5D_t4290686858* L_33 = L_31;
		MethodInfo_t * L_34 = ___method1;
		NullCheck(L_33);
		ArrayElementTypeCheck (L_33, L_34);
		(L_33)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_34);
		ObjectU5BU5D_t4290686858* L_35 = L_33;
		Object_t2461733472 * L_36 = V_4;
		NullCheck(L_35);
		ArrayElementTypeCheck (L_35, L_36);
		(L_35)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_36);
		NullCheck(L_30);
		RuntimeObject * L_37 = ConstructorInfo_Invoke_m4178483106(L_30, L_35, /*hidden argument*/NULL);
		V_5 = ((BaseInvokableCall_t2920707967 *)IsInstClass((RuntimeObject*)L_37, BaseInvokableCall_t2920707967_il2cpp_TypeInfo_var));
		goto IL_00d0;
	}

IL_00d0:
	{
		BaseInvokableCall_t2920707967 * L_38 = V_5;
		return L_38;
	}
}
// System.Void UnityEngine.Events.PersistentCallGroup::.ctor()
extern "C"  void PersistentCallGroup__ctor_m2062212555 (PersistentCallGroup_t586105043 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PersistentCallGroup__ctor_m2062212555_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object__ctor_m3446193457(__this, /*hidden argument*/NULL);
		List_1_t565626228 * L_0 = (List_1_t565626228 *)il2cpp_codegen_object_new(List_1_t565626228_il2cpp_TypeInfo_var);
		List_1__ctor_m43236579(L_0, /*hidden argument*/List_1__ctor_m43236579_RuntimeMethod_var);
		__this->set_m_Calls_0(L_0);
		return;
	}
}
// System.Void UnityEngine.Events.PersistentCallGroup::Initialize(UnityEngine.Events.InvokableCallList,UnityEngine.Events.UnityEventBase)
extern "C"  void PersistentCallGroup_Initialize_m644878480 (PersistentCallGroup_t586105043 * __this, InvokableCallList_t1673367030 * ___invokableList0, UnityEventBase_t3171537072 * ___unityEventBase1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PersistentCallGroup_Initialize_m644878480_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	PersistentCall_t2687057912 * V_0 = NULL;
	Enumerator_t2325441285  V_1;
	memset(&V_1, 0, sizeof(V_1));
	BaseInvokableCall_t2920707967 * V_2 = NULL;
	Exception_t3361176243 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3361176243 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		List_1_t565626228 * L_0 = __this->get_m_Calls_0();
		NullCheck(L_0);
		Enumerator_t2325441285  L_1 = List_1_GetEnumerator_m1074128493(L_0, /*hidden argument*/List_1_GetEnumerator_m1074128493_RuntimeMethod_var);
		V_1 = L_1;
	}

IL_000e:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0042;
		}

IL_0013:
		{
			PersistentCall_t2687057912 * L_2 = Enumerator_get_Current_m4156657100((&V_1), /*hidden argument*/Enumerator_get_Current_m4156657100_RuntimeMethod_var);
			V_0 = L_2;
			PersistentCall_t2687057912 * L_3 = V_0;
			NullCheck(L_3);
			bool L_4 = PersistentCall_IsValid_m2172430097(L_3, /*hidden argument*/NULL);
			if (L_4)
			{
				goto IL_002c;
			}
		}

IL_0027:
		{
			goto IL_0042;
		}

IL_002c:
		{
			PersistentCall_t2687057912 * L_5 = V_0;
			UnityEventBase_t3171537072 * L_6 = ___unityEventBase1;
			NullCheck(L_5);
			BaseInvokableCall_t2920707967 * L_7 = PersistentCall_GetRuntimeCall_m1057898321(L_5, L_6, /*hidden argument*/NULL);
			V_2 = L_7;
			BaseInvokableCall_t2920707967 * L_8 = V_2;
			if (!L_8)
			{
				goto IL_0041;
			}
		}

IL_003a:
		{
			InvokableCallList_t1673367030 * L_9 = ___invokableList0;
			BaseInvokableCall_t2920707967 * L_10 = V_2;
			NullCheck(L_9);
			InvokableCallList_AddPersistentInvokableCall_m3306354928(L_9, L_10, /*hidden argument*/NULL);
		}

IL_0041:
		{
		}

IL_0042:
		{
			bool L_11 = Enumerator_MoveNext_m2090417815((&V_1), /*hidden argument*/Enumerator_MoveNext_m2090417815_RuntimeMethod_var);
			if (L_11)
			{
				goto IL_0013;
			}
		}

IL_004e:
		{
			IL2CPP_LEAVE(0x61, FINALLY_0053);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3361176243 *)e.ex;
		goto FINALLY_0053;
	}

FINALLY_0053:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m2940250647((&V_1), /*hidden argument*/Enumerator_Dispose_m2940250647_RuntimeMethod_var);
		IL2CPP_END_FINALLY(83)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(83)
	{
		IL2CPP_JUMP_TBL(0x61, IL_0061)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3361176243 *)
	}

IL_0061:
	{
		return;
	}
}
extern "C"  void DelegatePInvokeWrapper_UnityAction_t35329723 (UnityAction_t35329723 * __this, const RuntimeMethod* method)
{
	typedef void (STDCALL *PInvokeFunc)();
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((RuntimeDelegate*)__this)->method));

	// Native function invocation
	il2cppPInvokeFunc();

}
// System.Void UnityEngine.Events.UnityAction::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction__ctor_m1074277545 (UnityAction_t35329723 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Events.UnityAction::Invoke()
extern "C"  void UnityAction_Invoke_m2515763748 (UnityAction_t35329723 * __this, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		UnityAction_Invoke_m2515763748((UnityAction_t35329723 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((RuntimeMethod*)(__this->get_method_3()))) && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (RuntimeObject *, void* __this, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),(RuntimeMethod*)(__this->get_method_3()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),(RuntimeMethod*)(__this->get_method_3()));
	}
}
// System.IAsyncResult UnityEngine.Events.UnityAction::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* UnityAction_BeginInvoke_m3164501995 (UnityAction_t35329723 * __this, AsyncCallback_t1046330627 * ___callback0, RuntimeObject * ___object1, const RuntimeMethod* method)
{
	void *__d_args[1] = {0};
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback0, (RuntimeObject*)___object1);
}
// System.Void UnityEngine.Events.UnityAction::EndInvoke(System.IAsyncResult)
extern "C"  void UnityAction_EndInvoke_m650613571 (UnityAction_t35329723 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Events.UnityEvent::.ctor()
extern "C"  void UnityEvent__ctor_m811779962 (UnityEvent_t1464431504 * __this, const RuntimeMethod* method)
{
	{
		__this->set_m_InvokeArray_4((ObjectU5BU5D_t4290686858*)NULL);
		UnityEventBase__ctor_m1419821417(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent::FindMethod_Impl(System.String,System.Object)
extern "C"  MethodInfo_t * UnityEvent_FindMethod_Impl_m3636630720 (UnityEvent_t1464431504 * __this, String_t* ___name0, RuntimeObject * ___targetObj1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_FindMethod_Impl_m3636630720_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MethodInfo_t * V_0 = NULL;
	{
		RuntimeObject * L_0 = ___targetObj1;
		String_t* L_1 = ___name0;
		MethodInfo_t * L_2 = UnityEventBase_GetValidMethodInfo_m1004474682(NULL /*static, unused*/, L_0, L_1, ((TypeU5BU5D_t1830665827*)SZArrayNew(TypeU5BU5D_t1830665827_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0014;
	}

IL_0014:
	{
		MethodInfo_t * L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern "C"  BaseInvokableCall_t2920707967 * UnityEvent_GetDelegate_m452312904 (UnityEvent_t1464431504 * __this, RuntimeObject * ___target0, MethodInfo_t * ___theFunction1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_GetDelegate_m452312904_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	BaseInvokableCall_t2920707967 * V_0 = NULL;
	{
		RuntimeObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		InvokableCall_t2876658856 * L_2 = (InvokableCall_t2876658856 *)il2cpp_codegen_object_new(InvokableCall_t2876658856_il2cpp_TypeInfo_var);
		InvokableCall__ctor_m2974797033(L_2, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_000e;
	}

IL_000e:
	{
		BaseInvokableCall_t2920707967 * L_3 = V_0;
		return L_3;
	}
}
// System.Void UnityEngine.Events.UnityEventBase::.ctor()
extern "C"  void UnityEventBase__ctor_m1419821417 (UnityEventBase_t3171537072 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEventBase__ctor_m1419821417_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_m_CallsDirty_3((bool)1);
		Object__ctor_m3446193457(__this, /*hidden argument*/NULL);
		InvokableCallList_t1673367030 * L_0 = (InvokableCallList_t1673367030 *)il2cpp_codegen_object_new(InvokableCallList_t1673367030_il2cpp_TypeInfo_var);
		InvokableCallList__ctor_m386930243(L_0, /*hidden argument*/NULL);
		__this->set_m_Calls_0(L_0);
		PersistentCallGroup_t586105043 * L_1 = (PersistentCallGroup_t586105043 *)il2cpp_codegen_object_new(PersistentCallGroup_t586105043_il2cpp_TypeInfo_var);
		PersistentCallGroup__ctor_m2062212555(L_1, /*hidden argument*/NULL);
		__this->set_m_PersistentCalls_1(L_1);
		Type_t * L_2 = Object_GetType_m4087638284(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(15 /* System.String System.Type::get_AssemblyQualifiedName() */, L_2);
		__this->set_m_TypeName_2(L_3);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEventBase::UnityEngine.ISerializationCallbackReceiver.OnBeforeSerialize()
extern "C"  void UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m303227655 (UnityEventBase_t3171537072 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.Events.UnityEventBase::UnityEngine.ISerializationCallbackReceiver.OnAfterDeserialize()
extern "C"  void UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m1452364050 (UnityEventBase_t3171537072 * __this, const RuntimeMethod* method)
{
	{
		UnityEventBase_DirtyPersistentCalls_m1507352076(__this, /*hidden argument*/NULL);
		Type_t * L_0 = Object_GetType_m4087638284(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(15 /* System.String System.Type::get_AssemblyQualifiedName() */, L_0);
		__this->set_m_TypeName_2(L_1);
		return;
	}
}
// System.Reflection.MethodInfo UnityEngine.Events.UnityEventBase::FindMethod(UnityEngine.Events.PersistentCall)
extern "C"  MethodInfo_t * UnityEventBase_FindMethod_m595753448 (UnityEventBase_t3171537072 * __this, PersistentCall_t2687057912 * ___call0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEventBase_FindMethod_m595753448_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	MethodInfo_t * V_1 = NULL;
	Type_t * G_B3_0 = NULL;
	Type_t * G_B2_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3171413529(NULL /*static, unused*/, LoadTypeToken(Object_t2461733472_0_0_0_var), /*hidden argument*/NULL);
		V_0 = L_0;
		PersistentCall_t2687057912 * L_1 = ___call0;
		NullCheck(L_1);
		ArgumentCache_t3682645796 * L_2 = PersistentCall_get_arguments_m1844117721(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		String_t* L_3 = ArgumentCache_get_unityObjectArgumentAssemblyTypeName_m3489746291(L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_4 = String_IsNullOrEmpty_m3560539680(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0044;
		}
	}
	{
		PersistentCall_t2687057912 * L_5 = ___call0;
		NullCheck(L_5);
		ArgumentCache_t3682645796 * L_6 = PersistentCall_get_arguments_m1844117721(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		String_t* L_7 = ArgumentCache_get_unityObjectArgumentAssemblyTypeName_m3489746291(L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_8 = il2cpp_codegen_get_type((Il2CppMethodPointer)&Type_GetType_m418477468, L_7, (bool)0, "UnityEngine.CoreModule, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null");
		Type_t * L_9 = L_8;
		G_B2_0 = L_9;
		if (L_9)
		{
			G_B3_0 = L_9;
			goto IL_0043;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_10 = Type_GetTypeFromHandle_m3171413529(NULL /*static, unused*/, LoadTypeToken(Object_t2461733472_0_0_0_var), /*hidden argument*/NULL);
		G_B3_0 = L_10;
	}

IL_0043:
	{
		V_0 = G_B3_0;
	}

IL_0044:
	{
		PersistentCall_t2687057912 * L_11 = ___call0;
		NullCheck(L_11);
		String_t* L_12 = PersistentCall_get_methodName_m2687182531(L_11, /*hidden argument*/NULL);
		PersistentCall_t2687057912 * L_13 = ___call0;
		NullCheck(L_13);
		Object_t2461733472 * L_14 = PersistentCall_get_target_m4082064461(L_13, /*hidden argument*/NULL);
		PersistentCall_t2687057912 * L_15 = ___call0;
		NullCheck(L_15);
		int32_t L_16 = PersistentCall_get_mode_m2357383679(L_15, /*hidden argument*/NULL);
		Type_t * L_17 = V_0;
		MethodInfo_t * L_18 = UnityEventBase_FindMethod_m3931843611(__this, L_12, L_14, L_16, L_17, /*hidden argument*/NULL);
		V_1 = L_18;
		goto IL_0063;
	}

IL_0063:
	{
		MethodInfo_t * L_19 = V_1;
		return L_19;
	}
}
// System.Reflection.MethodInfo UnityEngine.Events.UnityEventBase::FindMethod(System.String,System.Object,UnityEngine.Events.PersistentListenerMode,System.Type)
extern "C"  MethodInfo_t * UnityEventBase_FindMethod_m3931843611 (UnityEventBase_t3171537072 * __this, String_t* ___name0, RuntimeObject * ___listener1, int32_t ___mode2, Type_t * ___argumentType3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEventBase_FindMethod_m3931843611_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MethodInfo_t * V_0 = NULL;
	Type_t * G_B10_0 = NULL;
	int32_t G_B10_1 = 0;
	TypeU5BU5D_t1830665827* G_B10_2 = NULL;
	TypeU5BU5D_t1830665827* G_B10_3 = NULL;
	String_t* G_B10_4 = NULL;
	RuntimeObject * G_B10_5 = NULL;
	Type_t * G_B9_0 = NULL;
	int32_t G_B9_1 = 0;
	TypeU5BU5D_t1830665827* G_B9_2 = NULL;
	TypeU5BU5D_t1830665827* G_B9_3 = NULL;
	String_t* G_B9_4 = NULL;
	RuntimeObject * G_B9_5 = NULL;
	{
		int32_t L_0 = ___mode2;
		switch (L_0)
		{
			case 0:
			{
				goto IL_0028;
			}
			case 1:
			{
				goto IL_0036;
			}
			case 2:
			{
				goto IL_00c9;
			}
			case 3:
			{
				goto IL_0069;
			}
			case 4:
			{
				goto IL_0049;
			}
			case 5:
			{
				goto IL_00a9;
			}
			case 6:
			{
				goto IL_0089;
			}
		}
	}
	{
		goto IL_00f2;
	}

IL_0028:
	{
		String_t* L_1 = ___name0;
		RuntimeObject * L_2 = ___listener1;
		MethodInfo_t * L_3 = VirtFuncInvoker2< MethodInfo_t *, String_t*, RuntimeObject * >::Invoke(6 /* System.Reflection.MethodInfo UnityEngine.Events.UnityEventBase::FindMethod_Impl(System.String,System.Object) */, __this, L_1, L_2);
		V_0 = L_3;
		goto IL_00f9;
	}

IL_0036:
	{
		RuntimeObject * L_4 = ___listener1;
		String_t* L_5 = ___name0;
		MethodInfo_t * L_6 = UnityEventBase_GetValidMethodInfo_m1004474682(NULL /*static, unused*/, L_4, L_5, ((TypeU5BU5D_t1830665827*)SZArrayNew(TypeU5BU5D_t1830665827_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		V_0 = L_6;
		goto IL_00f9;
	}

IL_0049:
	{
		RuntimeObject * L_7 = ___listener1;
		String_t* L_8 = ___name0;
		TypeU5BU5D_t1830665827* L_9 = ((TypeU5BU5D_t1830665827*)SZArrayNew(TypeU5BU5D_t1830665827_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_10 = Type_GetTypeFromHandle_m3171413529(NULL /*static, unused*/, LoadTypeToken(Single_t496865882_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, L_10);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_10);
		MethodInfo_t * L_11 = UnityEventBase_GetValidMethodInfo_m1004474682(NULL /*static, unused*/, L_7, L_8, L_9, /*hidden argument*/NULL);
		V_0 = L_11;
		goto IL_00f9;
	}

IL_0069:
	{
		RuntimeObject * L_12 = ___listener1;
		String_t* L_13 = ___name0;
		TypeU5BU5D_t1830665827* L_14 = ((TypeU5BU5D_t1830665827*)SZArrayNew(TypeU5BU5D_t1830665827_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_15 = Type_GetTypeFromHandle_m3171413529(NULL /*static, unused*/, LoadTypeToken(Int32_t2571135199_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_14);
		ArrayElementTypeCheck (L_14, L_15);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_15);
		MethodInfo_t * L_16 = UnityEventBase_GetValidMethodInfo_m1004474682(NULL /*static, unused*/, L_12, L_13, L_14, /*hidden argument*/NULL);
		V_0 = L_16;
		goto IL_00f9;
	}

IL_0089:
	{
		RuntimeObject * L_17 = ___listener1;
		String_t* L_18 = ___name0;
		TypeU5BU5D_t1830665827* L_19 = ((TypeU5BU5D_t1830665827*)SZArrayNew(TypeU5BU5D_t1830665827_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_20 = Type_GetTypeFromHandle_m3171413529(NULL /*static, unused*/, LoadTypeToken(Boolean_t2059672899_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_19);
		ArrayElementTypeCheck (L_19, L_20);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_20);
		MethodInfo_t * L_21 = UnityEventBase_GetValidMethodInfo_m1004474682(NULL /*static, unused*/, L_17, L_18, L_19, /*hidden argument*/NULL);
		V_0 = L_21;
		goto IL_00f9;
	}

IL_00a9:
	{
		RuntimeObject * L_22 = ___listener1;
		String_t* L_23 = ___name0;
		TypeU5BU5D_t1830665827* L_24 = ((TypeU5BU5D_t1830665827*)SZArrayNew(TypeU5BU5D_t1830665827_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_25 = Type_GetTypeFromHandle_m3171413529(NULL /*static, unused*/, LoadTypeToken(String_t_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_24);
		ArrayElementTypeCheck (L_24, L_25);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_25);
		MethodInfo_t * L_26 = UnityEventBase_GetValidMethodInfo_m1004474682(NULL /*static, unused*/, L_22, L_23, L_24, /*hidden argument*/NULL);
		V_0 = L_26;
		goto IL_00f9;
	}

IL_00c9:
	{
		RuntimeObject * L_27 = ___listener1;
		String_t* L_28 = ___name0;
		TypeU5BU5D_t1830665827* L_29 = ((TypeU5BU5D_t1830665827*)SZArrayNew(TypeU5BU5D_t1830665827_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_30 = ___argumentType3;
		Type_t * L_31 = L_30;
		G_B9_0 = L_31;
		G_B9_1 = 0;
		G_B9_2 = L_29;
		G_B9_3 = L_29;
		G_B9_4 = L_28;
		G_B9_5 = L_27;
		if (L_31)
		{
			G_B10_0 = L_31;
			G_B10_1 = 0;
			G_B10_2 = L_29;
			G_B10_3 = L_29;
			G_B10_4 = L_28;
			G_B10_5 = L_27;
			goto IL_00e6;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_32 = Type_GetTypeFromHandle_m3171413529(NULL /*static, unused*/, LoadTypeToken(Object_t2461733472_0_0_0_var), /*hidden argument*/NULL);
		G_B10_0 = L_32;
		G_B10_1 = G_B9_1;
		G_B10_2 = G_B9_2;
		G_B10_3 = G_B9_3;
		G_B10_4 = G_B9_4;
		G_B10_5 = G_B9_5;
	}

IL_00e6:
	{
		NullCheck(G_B10_2);
		ArrayElementTypeCheck (G_B10_2, G_B10_0);
		(G_B10_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B10_1), (Type_t *)G_B10_0);
		MethodInfo_t * L_33 = UnityEventBase_GetValidMethodInfo_m1004474682(NULL /*static, unused*/, G_B10_5, G_B10_4, G_B10_3, /*hidden argument*/NULL);
		V_0 = L_33;
		goto IL_00f9;
	}

IL_00f2:
	{
		V_0 = (MethodInfo_t *)NULL;
		goto IL_00f9;
	}

IL_00f9:
	{
		MethodInfo_t * L_34 = V_0;
		return L_34;
	}
}
// System.Void UnityEngine.Events.UnityEventBase::DirtyPersistentCalls()
extern "C"  void UnityEventBase_DirtyPersistentCalls_m1507352076 (UnityEventBase_t3171537072 * __this, const RuntimeMethod* method)
{
	{
		InvokableCallList_t1673367030 * L_0 = __this->get_m_Calls_0();
		NullCheck(L_0);
		InvokableCallList_ClearPersistent_m1797712781(L_0, /*hidden argument*/NULL);
		__this->set_m_CallsDirty_3((bool)1);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEventBase::RebuildPersistentCallsIfNeeded()
extern "C"  void UnityEventBase_RebuildPersistentCallsIfNeeded_m256317675 (UnityEventBase_t3171537072 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_m_CallsDirty_3();
		if (!L_0)
		{
			goto IL_0027;
		}
	}
	{
		PersistentCallGroup_t586105043 * L_1 = __this->get_m_PersistentCalls_1();
		InvokableCallList_t1673367030 * L_2 = __this->get_m_Calls_0();
		NullCheck(L_1);
		PersistentCallGroup_Initialize_m644878480(L_1, L_2, __this, /*hidden argument*/NULL);
		__this->set_m_CallsDirty_3((bool)0);
	}

IL_0027:
	{
		return;
	}
}
// System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall> UnityEngine.Events.UnityEventBase::PrepareInvoke()
extern "C"  List_1_t799276283 * UnityEventBase_PrepareInvoke_m3974578209 (UnityEventBase_t3171537072 * __this, const RuntimeMethod* method)
{
	List_1_t799276283 * V_0 = NULL;
	{
		UnityEventBase_RebuildPersistentCallsIfNeeded_m256317675(__this, /*hidden argument*/NULL);
		InvokableCallList_t1673367030 * L_0 = __this->get_m_Calls_0();
		NullCheck(L_0);
		List_1_t799276283 * L_1 = InvokableCallList_PrepareInvoke_m2613780459(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0018;
	}

IL_0018:
	{
		List_1_t799276283 * L_2 = V_0;
		return L_2;
	}
}
// System.String UnityEngine.Events.UnityEventBase::ToString()
extern "C"  String_t* UnityEventBase_ToString_m2520642732 (UnityEventBase_t3171537072 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEventBase_ToString_m2520642732_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		String_t* L_0 = Object_ToString_m1080586622(__this, /*hidden argument*/NULL);
		Type_t * L_1 = Object_GetType_m4087638284(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		String_t* L_2 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, L_1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m4209604784(NULL /*static, unused*/, L_0, _stringLiteral578152108, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_0022;
	}

IL_0022:
	{
		String_t* L_4 = V_0;
		return L_4;
	}
}
// System.Reflection.MethodInfo UnityEngine.Events.UnityEventBase::GetValidMethodInfo(System.Object,System.String,System.Type[])
extern "C"  MethodInfo_t * UnityEventBase_GetValidMethodInfo_m1004474682 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___obj0, String_t* ___functionName1, TypeU5BU5D_t1830665827* ___argumentTypes2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEventBase_GetValidMethodInfo_m1004474682_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	MethodInfo_t * V_1 = NULL;
	ParameterInfoU5BU5D_t854811313* V_2 = NULL;
	bool V_3 = false;
	int32_t V_4 = 0;
	ParameterInfo_t3893964560 * V_5 = NULL;
	ParameterInfoU5BU5D_t854811313* V_6 = NULL;
	int32_t V_7 = 0;
	Type_t * V_8 = NULL;
	Type_t * V_9 = NULL;
	MethodInfo_t * V_10 = NULL;
	{
		RuntimeObject * L_0 = ___obj0;
		NullCheck(L_0);
		Type_t * L_1 = Object_GetType_m4087638284(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_009c;
	}

IL_000d:
	{
		Type_t * L_2 = V_0;
		String_t* L_3 = ___functionName1;
		TypeU5BU5D_t1830665827* L_4 = ___argumentTypes2;
		NullCheck(L_2);
		MethodInfo_t * L_5 = Type_GetMethod_m3484360104(L_2, L_3, ((int32_t)52), (Binder_t2856902408 *)NULL, L_4, (ParameterModifierU5BU5D_t3522550039*)(ParameterModifierU5BU5D_t3522550039*)NULL, /*hidden argument*/NULL);
		V_1 = L_5;
		MethodInfo_t * L_6 = V_1;
		if (!L_6)
		{
			goto IL_0094;
		}
	}
	{
		MethodInfo_t * L_7 = V_1;
		NullCheck(L_7);
		ParameterInfoU5BU5D_t854811313* L_8 = VirtFuncInvoker0< ParameterInfoU5BU5D_t854811313* >::Invoke(14 /* System.Reflection.ParameterInfo[] System.Reflection.MethodBase::GetParameters() */, L_7);
		V_2 = L_8;
		V_3 = (bool)1;
		V_4 = 0;
		ParameterInfoU5BU5D_t854811313* L_9 = V_2;
		V_6 = L_9;
		V_7 = 0;
		goto IL_007a;
	}

IL_003a:
	{
		ParameterInfoU5BU5D_t854811313* L_10 = V_6;
		int32_t L_11 = V_7;
		NullCheck(L_10);
		int32_t L_12 = L_11;
		ParameterInfo_t3893964560 * L_13 = (L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		V_5 = L_13;
		TypeU5BU5D_t1830665827* L_14 = ___argumentTypes2;
		int32_t L_15 = V_4;
		NullCheck(L_14);
		int32_t L_16 = L_15;
		Type_t * L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		V_8 = L_17;
		ParameterInfo_t3893964560 * L_18 = V_5;
		NullCheck(L_18);
		Type_t * L_19 = VirtFuncInvoker0< Type_t * >::Invoke(6 /* System.Type System.Reflection.ParameterInfo::get_ParameterType() */, L_18);
		V_9 = L_19;
		Type_t * L_20 = V_8;
		NullCheck(L_20);
		bool L_21 = Type_get_IsPrimitive_m3385012571(L_20, /*hidden argument*/NULL);
		Type_t * L_22 = V_9;
		NullCheck(L_22);
		bool L_23 = Type_get_IsPrimitive_m3385012571(L_22, /*hidden argument*/NULL);
		V_3 = (bool)((((int32_t)L_21) == ((int32_t)L_23))? 1 : 0);
		bool L_24 = V_3;
		if (L_24)
		{
			goto IL_006d;
		}
	}
	{
		goto IL_0085;
	}

IL_006d:
	{
		int32_t L_25 = V_4;
		V_4 = ((int32_t)((int32_t)L_25+(int32_t)1));
		int32_t L_26 = V_7;
		V_7 = ((int32_t)((int32_t)L_26+(int32_t)1));
	}

IL_007a:
	{
		int32_t L_27 = V_7;
		ParameterInfoU5BU5D_t854811313* L_28 = V_6;
		NullCheck(L_28);
		if ((((int32_t)L_27) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_28)->max_length)))))))
		{
			goto IL_003a;
		}
	}

IL_0085:
	{
		bool L_29 = V_3;
		if (!L_29)
		{
			goto IL_0093;
		}
	}
	{
		MethodInfo_t * L_30 = V_1;
		V_10 = L_30;
		goto IL_00ba;
	}

IL_0093:
	{
	}

IL_0094:
	{
		Type_t * L_31 = V_0;
		NullCheck(L_31);
		Type_t * L_32 = VirtFuncInvoker0< Type_t * >::Invoke(17 /* System.Type System.Type::get_BaseType() */, L_31);
		V_0 = L_32;
	}

IL_009c:
	{
		Type_t * L_33 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_34 = Type_GetTypeFromHandle_m3171413529(NULL /*static, unused*/, LoadTypeToken(RuntimeObject_0_0_0_var), /*hidden argument*/NULL);
		if ((((RuntimeObject*)(Type_t *)L_33) == ((RuntimeObject*)(Type_t *)L_34)))
		{
			goto IL_00b2;
		}
	}
	{
		Type_t * L_35 = V_0;
		if (L_35)
		{
			goto IL_000d;
		}
	}

IL_00b2:
	{
		V_10 = (MethodInfo_t *)NULL;
		goto IL_00ba;
	}

IL_00ba:
	{
		MethodInfo_t * L_36 = V_10;
		return L_36;
	}
}
// UnityEngine.Experimental.Rendering.IRenderPipeline UnityEngine.Experimental.Rendering.RenderPipelineManager::get_currentPipeline()
extern "C"  RuntimeObject* RenderPipelineManager_get_currentPipeline_m3103495424 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RenderPipelineManager_get_currentPipeline_m3103495424_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject* V_0 = NULL;
	{
		RuntimeObject* L_0 = ((RenderPipelineManager_t3681178706_StaticFields*)il2cpp_codegen_static_fields_for(RenderPipelineManager_t3681178706_il2cpp_TypeInfo_var))->get_U3CcurrentPipelineU3Ek__BackingField_1();
		V_0 = L_0;
		goto IL_000b;
	}

IL_000b:
	{
		RuntimeObject* L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Experimental.Rendering.RenderPipelineManager::set_currentPipeline(UnityEngine.Experimental.Rendering.IRenderPipeline)
extern "C"  void RenderPipelineManager_set_currentPipeline_m3602131277 (RuntimeObject * __this /* static, unused */, RuntimeObject* ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RenderPipelineManager_set_currentPipeline_m3602131277_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = ___value0;
		((RenderPipelineManager_t3681178706_StaticFields*)il2cpp_codegen_static_fields_for(RenderPipelineManager_t3681178706_il2cpp_TypeInfo_var))->set_U3CcurrentPipelineU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Void UnityEngine.Experimental.Rendering.RenderPipelineManager::CleanupRenderPipeline()
extern "C"  void RenderPipelineManager_CleanupRenderPipeline_m309528094 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RenderPipelineManager_CleanupRenderPipeline_m309528094_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = ((RenderPipelineManager_t3681178706_StaticFields*)il2cpp_codegen_static_fields_for(RenderPipelineManager_t3681178706_il2cpp_TypeInfo_var))->get_s_CurrentPipelineAsset_0();
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		RuntimeObject* L_1 = ((RenderPipelineManager_t3681178706_StaticFields*)il2cpp_codegen_static_fields_for(RenderPipelineManager_t3681178706_il2cpp_TypeInfo_var))->get_s_CurrentPipelineAsset_0();
		NullCheck(L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UnityEngine.Experimental.Rendering.IRenderPipelineAsset::DestroyCreatedInstances() */, IRenderPipelineAsset_t171638552_il2cpp_TypeInfo_var, L_1);
	}

IL_0015:
	{
		((RenderPipelineManager_t3681178706_StaticFields*)il2cpp_codegen_static_fields_for(RenderPipelineManager_t3681178706_il2cpp_TypeInfo_var))->set_s_CurrentPipelineAsset_0((RuntimeObject*)NULL);
		RenderPipelineManager_set_currentPipeline_m3602131277(NULL /*static, unused*/, (RuntimeObject*)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Experimental.Rendering.RenderPipelineManager::DoRenderLoop_Internal(UnityEngine.Experimental.Rendering.IRenderPipelineAsset,UnityEngine.Camera[],System.IntPtr)
extern "C"  void RenderPipelineManager_DoRenderLoop_Internal_m124400281 (RuntimeObject * __this /* static, unused */, RuntimeObject* ___pipe0, CameraU5BU5D_t2722699428* ___cameras1, intptr_t ___loopPtr2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RenderPipelineManager_DoRenderLoop_Internal_m124400281_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ScriptableRenderContext_t1486166028  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		RuntimeObject* L_0 = ___pipe0;
		RenderPipelineManager_PrepareRenderPipeline_m896043942(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		RuntimeObject* L_1 = RenderPipelineManager_get_currentPipeline_m3103495424(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0016;
		}
	}
	{
		goto IL_002a;
	}

IL_0016:
	{
		intptr_t L_2 = ___loopPtr2;
		ScriptableRenderContext__ctor_m242682668((&V_0), L_2, /*hidden argument*/NULL);
		RuntimeObject* L_3 = RenderPipelineManager_get_currentPipeline_m3103495424(NULL /*static, unused*/, /*hidden argument*/NULL);
		ScriptableRenderContext_t1486166028  L_4 = V_0;
		CameraU5BU5D_t2722699428* L_5 = ___cameras1;
		NullCheck(L_3);
		InterfaceActionInvoker2< ScriptableRenderContext_t1486166028 , CameraU5BU5D_t2722699428* >::Invoke(1 /* System.Void UnityEngine.Experimental.Rendering.IRenderPipeline::Render(UnityEngine.Experimental.Rendering.ScriptableRenderContext,UnityEngine.Camera[]) */, IRenderPipeline_t2460632778_il2cpp_TypeInfo_var, L_3, L_4, L_5);
	}

IL_002a:
	{
		return;
	}
}
// System.Void UnityEngine.Experimental.Rendering.RenderPipelineManager::PrepareRenderPipeline(UnityEngine.Experimental.Rendering.IRenderPipelineAsset)
extern "C"  void RenderPipelineManager_PrepareRenderPipeline_m896043942 (RuntimeObject * __this /* static, unused */, RuntimeObject* ___pipe0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RenderPipelineManager_PrepareRenderPipeline_m896043942_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = ((RenderPipelineManager_t3681178706_StaticFields*)il2cpp_codegen_static_fields_for(RenderPipelineManager_t3681178706_il2cpp_TypeInfo_var))->get_s_CurrentPipelineAsset_0();
		RuntimeObject* L_1 = ___pipe0;
		if ((((RuntimeObject*)(RuntimeObject*)L_0) == ((RuntimeObject*)(RuntimeObject*)L_1)))
		{
			goto IL_0025;
		}
	}
	{
		RuntimeObject* L_2 = ((RenderPipelineManager_t3681178706_StaticFields*)il2cpp_codegen_static_fields_for(RenderPipelineManager_t3681178706_il2cpp_TypeInfo_var))->get_s_CurrentPipelineAsset_0();
		if (!L_2)
		{
			goto IL_001e;
		}
	}
	{
		RenderPipelineManager_CleanupRenderPipeline_m309528094(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_001e:
	{
		RuntimeObject* L_3 = ___pipe0;
		((RenderPipelineManager_t3681178706_StaticFields*)il2cpp_codegen_static_fields_for(RenderPipelineManager_t3681178706_il2cpp_TypeInfo_var))->set_s_CurrentPipelineAsset_0(L_3);
	}

IL_0025:
	{
		RuntimeObject* L_4 = ((RenderPipelineManager_t3681178706_StaticFields*)il2cpp_codegen_static_fields_for(RenderPipelineManager_t3681178706_il2cpp_TypeInfo_var))->get_s_CurrentPipelineAsset_0();
		if (!L_4)
		{
			goto IL_0057;
		}
	}
	{
		RuntimeObject* L_5 = RenderPipelineManager_get_currentPipeline_m3103495424(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0048;
		}
	}
	{
		RuntimeObject* L_6 = RenderPipelineManager_get_currentPipeline_m3103495424(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_6);
		bool L_7 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean UnityEngine.Experimental.Rendering.IRenderPipeline::get_disposed() */, IRenderPipeline_t2460632778_il2cpp_TypeInfo_var, L_6);
		if (!L_7)
		{
			goto IL_0057;
		}
	}

IL_0048:
	{
		RuntimeObject* L_8 = ((RenderPipelineManager_t3681178706_StaticFields*)il2cpp_codegen_static_fields_for(RenderPipelineManager_t3681178706_il2cpp_TypeInfo_var))->get_s_CurrentPipelineAsset_0();
		NullCheck(L_8);
		RuntimeObject* L_9 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(1 /* UnityEngine.Experimental.Rendering.IRenderPipeline UnityEngine.Experimental.Rendering.IRenderPipelineAsset::CreatePipeline() */, IRenderPipelineAsset_t171638552_il2cpp_TypeInfo_var, L_8);
		RenderPipelineManager_set_currentPipeline_m3602131277(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
	}

IL_0057:
	{
		return;
	}
}
// System.Void UnityEngine.Experimental.Rendering.ScriptableRenderContext::.ctor(System.IntPtr)
extern "C"  void ScriptableRenderContext__ctor_m242682668 (ScriptableRenderContext_t1486166028 * __this, intptr_t ___ptr0, const RuntimeMethod* method)
{
	{
		intptr_t L_0 = ___ptr0;
		__this->set_m_Ptr_0(L_0);
		return;
	}
}
extern "C"  void ScriptableRenderContext__ctor_m242682668_AdjustorThunk (RuntimeObject * __this, intptr_t ___ptr0, const RuntimeMethod* method)
{
	ScriptableRenderContext_t1486166028 * _thisAdjusted = reinterpret_cast<ScriptableRenderContext_t1486166028 *>(__this + 1);
	ScriptableRenderContext__ctor_m242682668(_thisAdjusted, ___ptr0, method);
}
// Conversion methods for marshalling of: UnityEngine.FailedToLoadScriptObject
extern "C" void FailedToLoadScriptObject_t2431108707_marshal_pinvoke(const FailedToLoadScriptObject_t2431108707& unmarshaled, FailedToLoadScriptObject_t2431108707_marshaled_pinvoke& marshaled)
{
	marshaled.___m_CachedPtr_0 = unmarshaled.get_m_CachedPtr_0();
}
extern "C" void FailedToLoadScriptObject_t2431108707_marshal_pinvoke_back(const FailedToLoadScriptObject_t2431108707_marshaled_pinvoke& marshaled, FailedToLoadScriptObject_t2431108707& unmarshaled)
{
	intptr_t unmarshaled_m_CachedPtr_temp_0;
	memset(&unmarshaled_m_CachedPtr_temp_0, 0, sizeof(unmarshaled_m_CachedPtr_temp_0));
	unmarshaled_m_CachedPtr_temp_0 = marshaled.___m_CachedPtr_0;
	unmarshaled.set_m_CachedPtr_0(unmarshaled_m_CachedPtr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.FailedToLoadScriptObject
extern "C" void FailedToLoadScriptObject_t2431108707_marshal_pinvoke_cleanup(FailedToLoadScriptObject_t2431108707_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.FailedToLoadScriptObject
extern "C" void FailedToLoadScriptObject_t2431108707_marshal_com(const FailedToLoadScriptObject_t2431108707& unmarshaled, FailedToLoadScriptObject_t2431108707_marshaled_com& marshaled)
{
	marshaled.___m_CachedPtr_0 = unmarshaled.get_m_CachedPtr_0();
}
extern "C" void FailedToLoadScriptObject_t2431108707_marshal_com_back(const FailedToLoadScriptObject_t2431108707_marshaled_com& marshaled, FailedToLoadScriptObject_t2431108707& unmarshaled)
{
	intptr_t unmarshaled_m_CachedPtr_temp_0;
	memset(&unmarshaled_m_CachedPtr_temp_0, 0, sizeof(unmarshaled_m_CachedPtr_temp_0));
	unmarshaled_m_CachedPtr_temp_0 = marshaled.___m_CachedPtr_0;
	unmarshaled.set_m_CachedPtr_0(unmarshaled_m_CachedPtr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.FailedToLoadScriptObject
extern "C" void FailedToLoadScriptObject_t2431108707_marshal_com_cleanup(FailedToLoadScriptObject_t2431108707_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.GameObject::.ctor()
extern "C"  void GameObject__ctor_m717643193 (GameObject_t3732643618 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameObject__ctor_m717643193_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_t2461733472_il2cpp_TypeInfo_var);
		Object__ctor_m387379410(__this, /*hidden argument*/NULL);
		GameObject_Internal_CreateGameObject_m1823697062(NULL /*static, unused*/, __this, (String_t*)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GameObject::SendMessage(System.String,System.Object,UnityEngine.SendMessageOptions)
extern "C"  void GameObject_SendMessage_m239132133 (GameObject_t3732643618 * __this, String_t* ___methodName0, RuntimeObject * ___value1, int32_t ___options2, const RuntimeMethod* method)
{
	typedef void (*GameObject_SendMessage_m239132133_ftn) (GameObject_t3732643618 *, String_t*, RuntimeObject *, int32_t);
	static GameObject_SendMessage_m239132133_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_SendMessage_m239132133_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::SendMessage(System.String,System.Object,UnityEngine.SendMessageOptions)");
	_il2cpp_icall_func(__this, ___methodName0, ___value1, ___options2);
}
// System.Void UnityEngine.GameObject::Internal_CreateGameObject(UnityEngine.GameObject,System.String)
extern "C"  void GameObject_Internal_CreateGameObject_m1823697062 (RuntimeObject * __this /* static, unused */, GameObject_t3732643618 * ___mono0, String_t* ___name1, const RuntimeMethod* method)
{
	typedef void (*GameObject_Internal_CreateGameObject_m1823697062_ftn) (GameObject_t3732643618 *, String_t*);
	static GameObject_Internal_CreateGameObject_m1823697062_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_Internal_CreateGameObject_m1823697062_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::Internal_CreateGameObject(UnityEngine.GameObject,System.String)");
	_il2cpp_icall_func(___mono0, ___name1);
}
// Conversion methods for marshalling of: UnityEngine.Gradient
extern "C" void Gradient_t130030130_marshal_pinvoke(const Gradient_t130030130& unmarshaled, Gradient_t130030130_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Ptr_0 = unmarshaled.get_m_Ptr_0();
}
extern "C" void Gradient_t130030130_marshal_pinvoke_back(const Gradient_t130030130_marshaled_pinvoke& marshaled, Gradient_t130030130& unmarshaled)
{
	intptr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	unmarshaled_m_Ptr_temp_0 = marshaled.___m_Ptr_0;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.Gradient
extern "C" void Gradient_t130030130_marshal_pinvoke_cleanup(Gradient_t130030130_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.Gradient
extern "C" void Gradient_t130030130_marshal_com(const Gradient_t130030130& unmarshaled, Gradient_t130030130_marshaled_com& marshaled)
{
	marshaled.___m_Ptr_0 = unmarshaled.get_m_Ptr_0();
}
extern "C" void Gradient_t130030130_marshal_com_back(const Gradient_t130030130_marshaled_com& marshaled, Gradient_t130030130& unmarshaled)
{
	intptr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	unmarshaled_m_Ptr_temp_0 = marshaled.___m_Ptr_0;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.Gradient
extern "C" void Gradient_t130030130_marshal_com_cleanup(Gradient_t130030130_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.Gradient::.ctor()
extern "C"  void Gradient__ctor_m2009183950 (Gradient_t130030130 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m3446193457(__this, /*hidden argument*/NULL);
		Gradient_Init_m1188969969(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Gradient::Init()
extern "C"  void Gradient_Init_m1188969969 (Gradient_t130030130 * __this, const RuntimeMethod* method)
{
	typedef void (*Gradient_Init_m1188969969_ftn) (Gradient_t130030130 *);
	static Gradient_Init_m1188969969_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Gradient_Init_m1188969969_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Gradient::Init()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Gradient::Cleanup()
extern "C"  void Gradient_Cleanup_m2195794132 (Gradient_t130030130 * __this, const RuntimeMethod* method)
{
	typedef void (*Gradient_Cleanup_m2195794132_ftn) (Gradient_t130030130 *);
	static Gradient_Cleanup_m2195794132_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Gradient_Cleanup_m2195794132_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Gradient::Cleanup()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Gradient::Finalize()
extern "C"  void Gradient_Finalize_m3210182836 (Gradient_t130030130 * __this, const RuntimeMethod* method)
{
	Exception_t3361176243 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3361176243 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
	}

IL_0001:
	try
	{ // begin try (depth: 1)
		Gradient_Cleanup_m2195794132(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x13, FINALLY_000c);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3361176243 *)e.ex;
		goto FINALLY_000c;
	}

FINALLY_000c:
	{ // begin finally (depth: 1)
		Object_Finalize_m1722074356(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(12)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(12)
	{
		IL2CPP_JUMP_TBL(0x13, IL_0013)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3361176243 *)
	}

IL_0013:
	{
		return;
	}
}
// System.Void UnityEngine.GUIElement::.ctor()
extern "C"  void GUIElement__ctor_m3581839174 (GUIElement_t1292848738 * __this, const RuntimeMethod* method)
{
	{
		Behaviour__ctor_m1233868026(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIElement UnityEngine.GUILayer::HitTest(UnityEngine.Vector3)
extern "C"  GUIElement_t1292848738 * GUILayer_HitTest_m1935830346 (GUILayer_t1929752731 * __this, Vector3_t3239555143  ___screenPosition0, const RuntimeMethod* method)
{
	GUIElement_t1292848738 * V_0 = NULL;
	{
		GUIElement_t1292848738 * L_0 = GUILayer_INTERNAL_CALL_HitTest_m422396558(NULL /*static, unused*/, __this, (&___screenPosition0), /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000f;
	}

IL_000f:
	{
		GUIElement_t1292848738 * L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.GUIElement UnityEngine.GUILayer::INTERNAL_CALL_HitTest(UnityEngine.GUILayer,UnityEngine.Vector3&)
extern "C"  GUIElement_t1292848738 * GUILayer_INTERNAL_CALL_HitTest_m422396558 (RuntimeObject * __this /* static, unused */, GUILayer_t1929752731 * ___self0, Vector3_t3239555143 * ___screenPosition1, const RuntimeMethod* method)
{
	typedef GUIElement_t1292848738 * (*GUILayer_INTERNAL_CALL_HitTest_m422396558_ftn) (GUILayer_t1929752731 *, Vector3_t3239555143 *);
	static GUILayer_INTERNAL_CALL_HitTest_m422396558_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUILayer_INTERNAL_CALL_HitTest_m422396558_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUILayer::INTERNAL_CALL_HitTest(UnityEngine.GUILayer,UnityEngine.Vector3&)");
	GUIElement_t1292848738 * retVal = _il2cpp_icall_func(___self0, ___screenPosition1);
	return retVal;
}
// System.Boolean UnityEngine.Input::GetMouseButton(System.Int32)
extern "C"  bool Input_GetMouseButton_m294091231 (RuntimeObject * __this /* static, unused */, int32_t ___button0, const RuntimeMethod* method)
{
	typedef bool (*Input_GetMouseButton_m294091231_ftn) (int32_t);
	static Input_GetMouseButton_m294091231_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_GetMouseButton_m294091231_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::GetMouseButton(System.Int32)");
	bool retVal = _il2cpp_icall_func(___button0);
	return retVal;
}
// System.Boolean UnityEngine.Input::GetMouseButtonDown(System.Int32)
extern "C"  bool Input_GetMouseButtonDown_m3867833682 (RuntimeObject * __this /* static, unused */, int32_t ___button0, const RuntimeMethod* method)
{
	typedef bool (*Input_GetMouseButtonDown_m3867833682_ftn) (int32_t);
	static Input_GetMouseButtonDown_m3867833682_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_GetMouseButtonDown_m3867833682_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::GetMouseButtonDown(System.Int32)");
	bool retVal = _il2cpp_icall_func(___button0);
	return retVal;
}
// UnityEngine.Vector3 UnityEngine.Input::get_mousePosition()
extern "C"  Vector3_t3239555143  Input_get_mousePosition_m3311395491 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Input_get_mousePosition_m3311395491_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t3239555143  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t3239555143  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t2650329352_il2cpp_TypeInfo_var);
		Input_INTERNAL_get_mousePosition_m3613716531(NULL /*static, unused*/, (&V_0), /*hidden argument*/NULL);
		Vector3_t3239555143  L_0 = V_0;
		V_1 = L_0;
		goto IL_000f;
	}

IL_000f:
	{
		Vector3_t3239555143  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Input::INTERNAL_get_mousePosition(UnityEngine.Vector3&)
extern "C"  void Input_INTERNAL_get_mousePosition_m3613716531 (RuntimeObject * __this /* static, unused */, Vector3_t3239555143 * ___value0, const RuntimeMethod* method)
{
	typedef void (*Input_INTERNAL_get_mousePosition_m3613716531_ftn) (Vector3_t3239555143 *);
	static Input_INTERNAL_get_mousePosition_m3613716531_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_INTERNAL_get_mousePosition_m3613716531_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::INTERNAL_get_mousePosition(UnityEngine.Vector3&)");
	_il2cpp_icall_func(___value0);
}
// System.Void UnityEngine.Input::.cctor()
extern "C"  void Input__cctor_m865649194 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Input__cctor_m865649194_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((Input_t2650329352_StaticFields*)il2cpp_codegen_static_fields_for(Input_t2650329352_il2cpp_TypeInfo_var))->set_m_MainGyro_0((Gyroscope_t1916239707 *)NULL);
		return;
	}
}
// System.Void UnityEngine.Internal.DefaultValueAttribute::.ctor(System.String)
extern "C"  void DefaultValueAttribute__ctor_m2061771615 (DefaultValueAttribute_t2693326634 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m2897993467(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___value0;
		__this->set_DefaultValue_0(L_0);
		return;
	}
}
// System.Object UnityEngine.Internal.DefaultValueAttribute::get_Value()
extern "C"  RuntimeObject * DefaultValueAttribute_get_Value_m1203122007 (DefaultValueAttribute_t2693326634 * __this, const RuntimeMethod* method)
{
	RuntimeObject * V_0 = NULL;
	{
		RuntimeObject * L_0 = __this->get_DefaultValue_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		RuntimeObject * L_1 = V_0;
		return L_1;
	}
}
// System.Boolean UnityEngine.Internal.DefaultValueAttribute::Equals(System.Object)
extern "C"  bool DefaultValueAttribute_Equals_m3424877481 (DefaultValueAttribute_t2693326634 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DefaultValueAttribute_Equals_m3424877481_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	DefaultValueAttribute_t2693326634 * V_0 = NULL;
	bool V_1 = false;
	{
		RuntimeObject * L_0 = ___obj0;
		V_0 = ((DefaultValueAttribute_t2693326634 *)IsInstClass((RuntimeObject*)L_0, DefaultValueAttribute_t2693326634_il2cpp_TypeInfo_var));
		DefaultValueAttribute_t2693326634 * L_1 = V_0;
		if (L_1)
		{
			goto IL_0015;
		}
	}
	{
		V_1 = (bool)0;
		goto IL_0046;
	}

IL_0015:
	{
		RuntimeObject * L_2 = __this->get_DefaultValue_0();
		if (L_2)
		{
			goto IL_002f;
		}
	}
	{
		DefaultValueAttribute_t2693326634 * L_3 = V_0;
		NullCheck(L_3);
		RuntimeObject * L_4 = DefaultValueAttribute_get_Value_m1203122007(L_3, /*hidden argument*/NULL);
		V_1 = (bool)((((RuntimeObject*)(RuntimeObject *)L_4) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0);
		goto IL_0046;
	}

IL_002f:
	{
		RuntimeObject * L_5 = __this->get_DefaultValue_0();
		DefaultValueAttribute_t2693326634 * L_6 = V_0;
		NullCheck(L_6);
		RuntimeObject * L_7 = DefaultValueAttribute_get_Value_m1203122007(L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		bool L_8 = VirtFuncInvoker1< bool, RuntimeObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, L_5, L_7);
		V_1 = L_8;
		goto IL_0046;
	}

IL_0046:
	{
		bool L_9 = V_1;
		return L_9;
	}
}
// System.Int32 UnityEngine.Internal.DefaultValueAttribute::GetHashCode()
extern "C"  int32_t DefaultValueAttribute_GetHashCode_m2657444706 (DefaultValueAttribute_t2693326634 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		RuntimeObject * L_0 = __this->get_DefaultValue_0();
		if (L_0)
		{
			goto IL_0018;
		}
	}
	{
		int32_t L_1 = Attribute_GetHashCode_m2188337821(__this, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0029;
	}

IL_0018:
	{
		RuntimeObject * L_2 = __this->get_DefaultValue_0();
		NullCheck(L_2);
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, L_2);
		V_0 = L_3;
		goto IL_0029;
	}

IL_0029:
	{
		int32_t L_4 = V_0;
		return L_4;
	}
}
// System.Void UnityEngine.iOS.LocalNotification::Destroy()
extern "C"  void LocalNotification_Destroy_m1982161855 (LocalNotification_t2133447199 * __this, const RuntimeMethod* method)
{
	typedef void (*LocalNotification_Destroy_m1982161855_ftn) (LocalNotification_t2133447199 *);
	static LocalNotification_Destroy_m1982161855_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (LocalNotification_Destroy_m1982161855_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.iOS.LocalNotification::Destroy()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.iOS.LocalNotification::Finalize()
extern "C"  void LocalNotification_Finalize_m874233874 (LocalNotification_t2133447199 * __this, const RuntimeMethod* method)
{
	Exception_t3361176243 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3361176243 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
	}

IL_0001:
	try
	{ // begin try (depth: 1)
		LocalNotification_Destroy_m1982161855(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x13, FINALLY_000c);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3361176243 *)e.ex;
		goto FINALLY_000c;
	}

FINALLY_000c:
	{ // begin finally (depth: 1)
		Object_Finalize_m1722074356(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(12)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(12)
	{
		IL2CPP_JUMP_TBL(0x13, IL_0013)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3361176243 *)
	}

IL_0013:
	{
		return;
	}
}
// System.Void UnityEngine.iOS.LocalNotification::.cctor()
extern "C"  void LocalNotification__cctor_m140754005 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LocalNotification__cctor_m140754005_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	DateTime_t507798353  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		DateTime__ctor_m3814649173((&V_0), ((int32_t)2001), 1, 1, 0, 0, 0, 1, /*hidden argument*/NULL);
		int64_t L_0 = DateTime_get_Ticks_m3634951895((&V_0), /*hidden argument*/NULL);
		((LocalNotification_t2133447199_StaticFields*)il2cpp_codegen_static_fields_for(LocalNotification_t2133447199_il2cpp_TypeInfo_var))->set_m_NSReferenceDateTicks_1(L_0);
		return;
	}
}
// System.Void UnityEngine.iOS.RemoteNotification::Destroy()
extern "C"  void RemoteNotification_Destroy_m4186758914 (RemoteNotification_t414806740 * __this, const RuntimeMethod* method)
{
	typedef void (*RemoteNotification_Destroy_m4186758914_ftn) (RemoteNotification_t414806740 *);
	static RemoteNotification_Destroy_m4186758914_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RemoteNotification_Destroy_m4186758914_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.iOS.RemoteNotification::Destroy()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.iOS.RemoteNotification::Finalize()
extern "C"  void RemoteNotification_Finalize_m2240751185 (RemoteNotification_t414806740 * __this, const RuntimeMethod* method)
{
	Exception_t3361176243 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3361176243 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
	}

IL_0001:
	try
	{ // begin try (depth: 1)
		RemoteNotification_Destroy_m4186758914(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x13, FINALLY_000c);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3361176243 *)e.ex;
		goto FINALLY_000c;
	}

FINALLY_000c:
	{ // begin finally (depth: 1)
		Object_Finalize_m1722074356(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(12)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(12)
	{
		IL2CPP_JUMP_TBL(0x13, IL_0013)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3361176243 *)
	}

IL_0013:
	{
		return;
	}
}
// System.Void UnityEngine.Logger::.ctor(UnityEngine.ILogHandler)
extern "C"  void Logger__ctor_m4234190541 (Logger_t605719344 * __this, RuntimeObject* ___logHandler0, const RuntimeMethod* method)
{
	{
		Object__ctor_m3446193457(__this, /*hidden argument*/NULL);
		RuntimeObject* L_0 = ___logHandler0;
		Logger_set_logHandler_m6066634(__this, L_0, /*hidden argument*/NULL);
		Logger_set_logEnabled_m1662937540(__this, (bool)1, /*hidden argument*/NULL);
		Logger_set_filterLogType_m2745985065(__this, 3, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.ILogHandler UnityEngine.Logger::get_logHandler()
extern "C"  RuntimeObject* Logger_get_logHandler_m1719487699 (Logger_t605719344 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		RuntimeObject* L_0 = __this->get_U3ClogHandlerU3Ek__BackingField_0();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		RuntimeObject* L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Logger::set_logHandler(UnityEngine.ILogHandler)
extern "C"  void Logger_set_logHandler_m6066634 (Logger_t605719344 * __this, RuntimeObject* ___value0, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = ___value0;
		__this->set_U3ClogHandlerU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Boolean UnityEngine.Logger::get_logEnabled()
extern "C"  bool Logger_get_logEnabled_m2290808316 (Logger_t605719344 * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		bool L_0 = __this->get_U3ClogEnabledU3Ek__BackingField_1();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Logger::set_logEnabled(System.Boolean)
extern "C"  void Logger_set_logEnabled_m1662937540 (Logger_t605719344 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3ClogEnabledU3Ek__BackingField_1(L_0);
		return;
	}
}
// UnityEngine.LogType UnityEngine.Logger::get_filterLogType()
extern "C"  int32_t Logger_get_filterLogType_m426955796 (Logger_t605719344 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U3CfilterLogTypeU3Ek__BackingField_2();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Logger::set_filterLogType(UnityEngine.LogType)
extern "C"  void Logger_set_filterLogType_m2745985065 (Logger_t605719344 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CfilterLogTypeU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Boolean UnityEngine.Logger::IsLogTypeAllowed(UnityEngine.LogType)
extern "C"  bool Logger_IsLogTypeAllowed_m1817789334 (Logger_t605719344 * __this, int32_t ___logType0, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		bool L_0 = Logger_get_logEnabled_m2290808316(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_003a;
		}
	}
	{
		int32_t L_1 = ___logType0;
		if ((!(((uint32_t)L_1) == ((uint32_t)4))))
		{
			goto IL_001b;
		}
	}
	{
		V_0 = (bool)1;
		goto IL_0041;
	}

IL_001b:
	{
		int32_t L_2 = Logger_get_filterLogType_m426955796(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_2) == ((int32_t)4)))
		{
			goto IL_0039;
		}
	}
	{
		int32_t L_3 = ___logType0;
		int32_t L_4 = Logger_get_filterLogType_m426955796(__this, /*hidden argument*/NULL);
		V_0 = (bool)((((int32_t)((((int32_t)L_3) > ((int32_t)L_4))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0041;
	}

IL_0039:
	{
	}

IL_003a:
	{
		V_0 = (bool)0;
		goto IL_0041;
	}

IL_0041:
	{
		bool L_5 = V_0;
		return L_5;
	}
}
// System.String UnityEngine.Logger::GetString(System.Object)
extern "C"  String_t* Logger_GetString_m2492438982 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___message0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Logger_GetString_m2492438982_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	String_t* G_B3_0 = NULL;
	{
		RuntimeObject * L_0 = ___message0;
		if (!L_0)
		{
			goto IL_0012;
		}
	}
	{
		RuntimeObject * L_1 = ___message0;
		NullCheck(L_1);
		String_t* L_2 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_1);
		G_B3_0 = L_2;
		goto IL_0017;
	}

IL_0012:
	{
		G_B3_0 = _stringLiteral614899718;
	}

IL_0017:
	{
		V_0 = G_B3_0;
		goto IL_001d;
	}

IL_001d:
	{
		String_t* L_3 = V_0;
		return L_3;
	}
}
// System.Void UnityEngine.Logger::Log(UnityEngine.LogType,System.Object)
extern "C"  void Logger_Log_m2914072330 (Logger_t605719344 * __this, int32_t ___logType0, RuntimeObject * ___message1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Logger_Log_m2914072330_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___logType0;
		bool L_1 = Logger_IsLogTypeAllowed_m1817789334(__this, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002e;
		}
	}
	{
		RuntimeObject* L_2 = Logger_get_logHandler_m1719487699(__this, /*hidden argument*/NULL);
		int32_t L_3 = ___logType0;
		ObjectU5BU5D_t4290686858* L_4 = ((ObjectU5BU5D_t4290686858*)SZArrayNew(ObjectU5BU5D_t4290686858_il2cpp_TypeInfo_var, (uint32_t)1));
		RuntimeObject * L_5 = ___message1;
		String_t* L_6 = Logger_GetString_m2492438982(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_6);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_6);
		NullCheck(L_2);
		InterfaceActionInvoker4< int32_t, Object_t2461733472 *, String_t*, ObjectU5BU5D_t4290686858* >::Invoke(0 /* System.Void UnityEngine.ILogHandler::LogFormat(UnityEngine.LogType,UnityEngine.Object,System.String,System.Object[]) */, ILogHandler_t2217299658_il2cpp_TypeInfo_var, L_2, L_3, (Object_t2461733472 *)NULL, _stringLiteral1047698264, L_4);
	}

IL_002e:
	{
		return;
	}
}
// System.Void UnityEngine.Logger::LogFormat(UnityEngine.LogType,UnityEngine.Object,System.String,System.Object[])
extern "C"  void Logger_LogFormat_m1581286573 (Logger_t605719344 * __this, int32_t ___logType0, Object_t2461733472 * ___context1, String_t* ___format2, ObjectU5BU5D_t4290686858* ___args3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Logger_LogFormat_m1581286573_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___logType0;
		bool L_1 = Logger_IsLogTypeAllowed_m1817789334(__this, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		RuntimeObject* L_2 = Logger_get_logHandler_m1719487699(__this, /*hidden argument*/NULL);
		int32_t L_3 = ___logType0;
		Object_t2461733472 * L_4 = ___context1;
		String_t* L_5 = ___format2;
		ObjectU5BU5D_t4290686858* L_6 = ___args3;
		NullCheck(L_2);
		InterfaceActionInvoker4< int32_t, Object_t2461733472 *, String_t*, ObjectU5BU5D_t4290686858* >::Invoke(0 /* System.Void UnityEngine.ILogHandler::LogFormat(UnityEngine.LogType,UnityEngine.Object,System.String,System.Object[]) */, ILogHandler_t2217299658_il2cpp_TypeInfo_var, L_2, L_3, L_4, L_5, L_6);
	}

IL_001d:
	{
		return;
	}
}
// System.Void UnityEngine.Logger::LogException(System.Exception,UnityEngine.Object)
extern "C"  void Logger_LogException_m1366382550 (Logger_t605719344 * __this, Exception_t3361176243 * ___exception0, Object_t2461733472 * ___context1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Logger_LogException_m1366382550_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = Logger_get_logEnabled_m2290808316(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0019;
		}
	}
	{
		RuntimeObject* L_1 = Logger_get_logHandler_m1719487699(__this, /*hidden argument*/NULL);
		Exception_t3361176243 * L_2 = ___exception0;
		Object_t2461733472 * L_3 = ___context1;
		NullCheck(L_1);
		InterfaceActionInvoker2< Exception_t3361176243 *, Object_t2461733472 * >::Invoke(1 /* System.Void UnityEngine.ILogHandler::LogException(System.Exception,UnityEngine.Object) */, ILogHandler_t2217299658_il2cpp_TypeInfo_var, L_1, L_2, L_3);
	}

IL_0019:
	{
		return;
	}
}
// System.Void UnityEngine.ManagedStreamHelpers::ValidateLoadFromStream(System.IO.Stream)
extern "C"  void ManagedStreamHelpers_ValidateLoadFromStream_m2537091486 (RuntimeObject * __this /* static, unused */, Stream_t2349536632 * ___stream0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ManagedStreamHelpers_ValidateLoadFromStream_m2537091486_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Stream_t2349536632 * L_0 = ___stream0;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t517973450 * L_1 = (ArgumentNullException_t517973450 *)il2cpp_codegen_object_new(ArgumentNullException_t517973450_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m532419484(L_1, _stringLiteral1321367729, _stringLiteral2562582605, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0017:
	{
		Stream_t2349536632 * L_2 = ___stream0;
		NullCheck(L_2);
		bool L_3 = VirtFuncInvoker0< bool >::Invoke(5 /* System.Boolean System.IO.Stream::get_CanRead() */, L_2);
		if (L_3)
		{
			goto IL_0032;
		}
	}
	{
		ArgumentException_t1372035057 * L_4 = (ArgumentException_t1372035057 *)il2cpp_codegen_object_new(ArgumentException_t1372035057_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2239500235(L_4, _stringLiteral1188424634, _stringLiteral2562582605, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_0032:
	{
		Stream_t2349536632 * L_5 = ___stream0;
		NullCheck(L_5);
		bool L_6 = VirtFuncInvoker0< bool >::Invoke(6 /* System.Boolean System.IO.Stream::get_CanSeek() */, L_5);
		if (L_6)
		{
			goto IL_004d;
		}
	}
	{
		ArgumentException_t1372035057 * L_7 = (ArgumentException_t1372035057 *)il2cpp_codegen_object_new(ArgumentException_t1372035057_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2239500235(L_7, _stringLiteral2966094564, _stringLiteral2562582605, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
	}

IL_004d:
	{
		return;
	}
}
// System.Void UnityEngine.ManagedStreamHelpers::ManagedStreamRead(System.Byte[],System.Int32,System.Int32,System.IO.Stream,System.IntPtr)
extern "C"  void ManagedStreamHelpers_ManagedStreamRead_m4024070239 (RuntimeObject * __this /* static, unused */, ByteU5BU5D_t1014014593* ___buffer0, int32_t ___offset1, int32_t ___count2, Stream_t2349536632 * ___stream3, intptr_t ___returnValueAddress4, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ManagedStreamHelpers_ManagedStreamRead_m4024070239_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		intptr_t L_0 = ___returnValueAddress4;
		bool L_1 = IntPtr_op_Equality_m3978210203(NULL /*static, unused*/, L_0, (intptr_t)(0), /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		ArgumentException_t1372035057 * L_2 = (ArgumentException_t1372035057 *)il2cpp_codegen_object_new(ArgumentException_t1372035057_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2239500235(L_2, _stringLiteral904118955, _stringLiteral310943400, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0022:
	{
		Stream_t2349536632 * L_3 = ___stream3;
		ManagedStreamHelpers_ValidateLoadFromStream_m2537091486(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		intptr_t L_4 = ___returnValueAddress4;
		void* L_5 = IntPtr_op_Explicit_m3354255807(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		Stream_t2349536632 * L_6 = ___stream3;
		ByteU5BU5D_t1014014593* L_7 = ___buffer0;
		int32_t L_8 = ___offset1;
		int32_t L_9 = ___count2;
		NullCheck(L_6);
		int32_t L_10 = VirtFuncInvoker3< int32_t, ByteU5BU5D_t1014014593*, int32_t, int32_t >::Invoke(14 /* System.Int32 System.IO.Stream::Read(System.Byte[],System.Int32,System.Int32) */, L_6, L_7, L_8, L_9);
		*((int32_t*)(L_5)) = (int32_t)L_10;
		return;
	}
}
// System.Void UnityEngine.ManagedStreamHelpers::ManagedStreamSeek(System.Int64,System.UInt32,System.IO.Stream,System.IntPtr)
extern "C"  void ManagedStreamHelpers_ManagedStreamSeek_m504681536 (RuntimeObject * __this /* static, unused */, int64_t ___offset0, uint32_t ___origin1, Stream_t2349536632 * ___stream2, intptr_t ___returnValueAddress3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ManagedStreamHelpers_ManagedStreamSeek_m504681536_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		intptr_t L_0 = ___returnValueAddress3;
		bool L_1 = IntPtr_op_Equality_m3978210203(NULL /*static, unused*/, L_0, (intptr_t)(0), /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		ArgumentException_t1372035057 * L_2 = (ArgumentException_t1372035057 *)il2cpp_codegen_object_new(ArgumentException_t1372035057_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2239500235(L_2, _stringLiteral904118955, _stringLiteral310943400, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0021:
	{
		Stream_t2349536632 * L_3 = ___stream2;
		ManagedStreamHelpers_ValidateLoadFromStream_m2537091486(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		intptr_t L_4 = ___returnValueAddress3;
		void* L_5 = IntPtr_op_Explicit_m3354255807(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		Stream_t2349536632 * L_6 = ___stream2;
		int64_t L_7 = ___offset0;
		uint32_t L_8 = ___origin1;
		NullCheck(L_6);
		int64_t L_9 = VirtFuncInvoker2< int64_t, int64_t, int32_t >::Invoke(16 /* System.Int64 System.IO.Stream::Seek(System.Int64,System.IO.SeekOrigin) */, L_6, L_7, L_8);
		*((int64_t*)(L_5)) = (int64_t)L_9;
		return;
	}
}
// System.Void UnityEngine.ManagedStreamHelpers::ManagedStreamLength(System.IO.Stream,System.IntPtr)
extern "C"  void ManagedStreamHelpers_ManagedStreamLength_m1632106524 (RuntimeObject * __this /* static, unused */, Stream_t2349536632 * ___stream0, intptr_t ___returnValueAddress1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ManagedStreamHelpers_ManagedStreamLength_m1632106524_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		intptr_t L_0 = ___returnValueAddress1;
		bool L_1 = IntPtr_op_Equality_m3978210203(NULL /*static, unused*/, L_0, (intptr_t)(0), /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		ArgumentException_t1372035057 * L_2 = (ArgumentException_t1372035057 *)il2cpp_codegen_object_new(ArgumentException_t1372035057_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2239500235(L_2, _stringLiteral904118955, _stringLiteral310943400, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0021:
	{
		Stream_t2349536632 * L_3 = ___stream0;
		ManagedStreamHelpers_ValidateLoadFromStream_m2537091486(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		intptr_t L_4 = ___returnValueAddress1;
		void* L_5 = IntPtr_op_Explicit_m3354255807(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		Stream_t2349536632 * L_6 = ___stream0;
		NullCheck(L_6);
		int64_t L_7 = VirtFuncInvoker0< int64_t >::Invoke(8 /* System.Int64 System.IO.Stream::get_Length() */, L_6);
		*((int64_t*)(L_5)) = (int64_t)L_7;
		return;
	}
}
// System.Single UnityEngine.Mathf::Abs(System.Single)
extern "C"  float Mathf_Abs_m2765530332 (RuntimeObject * __this /* static, unused */, float ___f0, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = ___f0;
		float L_1 = fabsf(L_0);
		V_0 = (((float)((float)L_1)));
		goto IL_000e;
	}

IL_000e:
	{
		float L_2 = V_0;
		return L_2;
	}
}
// System.Single UnityEngine.Mathf::Max(System.Single,System.Single)
extern "C"  float Mathf_Max_m3084430723 (RuntimeObject * __this /* static, unused */, float ___a0, float ___b1, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	float G_B3_0 = 0.0f;
	{
		float L_0 = ___a0;
		float L_1 = ___b1;
		if ((!(((float)L_0) > ((float)L_1))))
		{
			goto IL_000e;
		}
	}
	{
		float L_2 = ___a0;
		G_B3_0 = L_2;
		goto IL_000f;
	}

IL_000e:
	{
		float L_3 = ___b1;
		G_B3_0 = L_3;
	}

IL_000f:
	{
		V_0 = G_B3_0;
		goto IL_0015;
	}

IL_0015:
	{
		float L_4 = V_0;
		return L_4;
	}
}
// System.Boolean UnityEngine.Mathf::Approximately(System.Single,System.Single)
extern "C"  bool Mathf_Approximately_m1455183363 (RuntimeObject * __this /* static, unused */, float ___a0, float ___b1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Mathf_Approximately_m1455183363_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		float L_0 = ___b1;
		float L_1 = ___a0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1822574071_il2cpp_TypeInfo_var);
		float L_2 = fabsf(((float)((float)L_0-(float)L_1)));
		float L_3 = ___a0;
		float L_4 = fabsf(L_3);
		float L_5 = ___b1;
		float L_6 = fabsf(L_5);
		float L_7 = Mathf_Max_m3084430723(NULL /*static, unused*/, L_4, L_6, /*hidden argument*/NULL);
		float L_8 = ((Mathf_t1822574071_StaticFields*)il2cpp_codegen_static_fields_for(Mathf_t1822574071_il2cpp_TypeInfo_var))->get_Epsilon_0();
		float L_9 = Mathf_Max_m3084430723(NULL /*static, unused*/, ((float)((float)(1.0E-06f)*(float)L_7)), ((float)((float)L_8*(float)(8.0f))), /*hidden argument*/NULL);
		V_0 = (bool)((((float)L_2) < ((float)L_9))? 1 : 0);
		goto IL_0038;
	}

IL_0038:
	{
		bool L_10 = V_0;
		return L_10;
	}
}
// System.Void UnityEngine.Mathf::.cctor()
extern "C"  void Mathf__cctor_m3961786392 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Mathf__cctor_m3961786392_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float G_B3_0;
	memset(&G_B3_0, 0, sizeof(G_B3_0));
	{
		IL2CPP_RUNTIME_CLASS_INIT(MathfInternal_t1431629077_il2cpp_TypeInfo_var);
		bool L_0 = ((MathfInternal_t1431629077_StaticFields*)il2cpp_codegen_static_fields_for(MathfInternal_t1431629077_il2cpp_TypeInfo_var))->get_IsFlushToZeroEnabled_2();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MathfInternal_t1431629077_il2cpp_TypeInfo_var);
		float L_1 = ((MathfInternal_t1431629077_StaticFields*)il2cpp_codegen_static_fields_for(MathfInternal_t1431629077_il2cpp_TypeInfo_var))->get_FloatMinNormal_0();
		il2cpp_codegen_memory_barrier();
		G_B3_0 = L_1;
		goto IL_001d;
	}

IL_0016:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MathfInternal_t1431629077_il2cpp_TypeInfo_var);
		float L_2 = ((MathfInternal_t1431629077_StaticFields*)il2cpp_codegen_static_fields_for(MathfInternal_t1431629077_il2cpp_TypeInfo_var))->get_FloatMinDenormal_1();
		il2cpp_codegen_memory_barrier();
		G_B3_0 = L_2;
	}

IL_001d:
	{
		((Mathf_t1822574071_StaticFields*)il2cpp_codegen_static_fields_for(Mathf_t1822574071_il2cpp_TypeInfo_var))->set_Epsilon_0(G_B3_0);
		return;
	}
}
// System.Void UnityEngine.Matrix4x4::.ctor(UnityEngine.Vector4,UnityEngine.Vector4,UnityEngine.Vector4,UnityEngine.Vector4)
extern "C"  void Matrix4x4__ctor_m614016869 (Matrix4x4_t3534180298 * __this, Vector4_t4052901136  ___column00, Vector4_t4052901136  ___column11, Vector4_t4052901136  ___column22, Vector4_t4052901136  ___column33, const RuntimeMethod* method)
{
	{
		float L_0 = (&___column00)->get_x_0();
		__this->set_m00_0(L_0);
		float L_1 = (&___column11)->get_x_0();
		__this->set_m01_4(L_1);
		float L_2 = (&___column22)->get_x_0();
		__this->set_m02_8(L_2);
		float L_3 = (&___column33)->get_x_0();
		__this->set_m03_12(L_3);
		float L_4 = (&___column00)->get_y_1();
		__this->set_m10_1(L_4);
		float L_5 = (&___column11)->get_y_1();
		__this->set_m11_5(L_5);
		float L_6 = (&___column22)->get_y_1();
		__this->set_m12_9(L_6);
		float L_7 = (&___column33)->get_y_1();
		__this->set_m13_13(L_7);
		float L_8 = (&___column00)->get_z_2();
		__this->set_m20_2(L_8);
		float L_9 = (&___column11)->get_z_2();
		__this->set_m21_6(L_9);
		float L_10 = (&___column22)->get_z_2();
		__this->set_m22_10(L_10);
		float L_11 = (&___column33)->get_z_2();
		__this->set_m23_14(L_11);
		float L_12 = (&___column00)->get_w_3();
		__this->set_m30_3(L_12);
		float L_13 = (&___column11)->get_w_3();
		__this->set_m31_7(L_13);
		float L_14 = (&___column22)->get_w_3();
		__this->set_m32_11(L_14);
		float L_15 = (&___column33)->get_w_3();
		__this->set_m33_15(L_15);
		return;
	}
}
extern "C"  void Matrix4x4__ctor_m614016869_AdjustorThunk (RuntimeObject * __this, Vector4_t4052901136  ___column00, Vector4_t4052901136  ___column11, Vector4_t4052901136  ___column22, Vector4_t4052901136  ___column33, const RuntimeMethod* method)
{
	Matrix4x4_t3534180298 * _thisAdjusted = reinterpret_cast<Matrix4x4_t3534180298 *>(__this + 1);
	Matrix4x4__ctor_m614016869(_thisAdjusted, ___column00, ___column11, ___column22, ___column33, method);
}
// System.Int32 UnityEngine.Matrix4x4::GetHashCode()
extern "C"  int32_t Matrix4x4_GetHashCode_m2086764285 (Matrix4x4_t3534180298 * __this, const RuntimeMethod* method)
{
	Vector4_t4052901136  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector4_t4052901136  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector4_t4052901136  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector4_t4052901136  V_3;
	memset(&V_3, 0, sizeof(V_3));
	int32_t V_4 = 0;
	{
		Vector4_t4052901136  L_0 = Matrix4x4_GetColumn_m1315812967(__this, 0, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Vector4_GetHashCode_m1136089151((&V_0), /*hidden argument*/NULL);
		Vector4_t4052901136  L_2 = Matrix4x4_GetColumn_m1315812967(__this, 1, /*hidden argument*/NULL);
		V_1 = L_2;
		int32_t L_3 = Vector4_GetHashCode_m1136089151((&V_1), /*hidden argument*/NULL);
		Vector4_t4052901136  L_4 = Matrix4x4_GetColumn_m1315812967(__this, 2, /*hidden argument*/NULL);
		V_2 = L_4;
		int32_t L_5 = Vector4_GetHashCode_m1136089151((&V_2), /*hidden argument*/NULL);
		Vector4_t4052901136  L_6 = Matrix4x4_GetColumn_m1315812967(__this, 3, /*hidden argument*/NULL);
		V_3 = L_6;
		int32_t L_7 = Vector4_GetHashCode_m1136089151((&V_3), /*hidden argument*/NULL);
		V_4 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_1^(int32_t)((int32_t)((int32_t)L_3<<(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_5>>(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_7>>(int32_t)1))));
		goto IL_0065;
	}

IL_0065:
	{
		int32_t L_8 = V_4;
		return L_8;
	}
}
extern "C"  int32_t Matrix4x4_GetHashCode_m2086764285_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Matrix4x4_t3534180298 * _thisAdjusted = reinterpret_cast<Matrix4x4_t3534180298 *>(__this + 1);
	return Matrix4x4_GetHashCode_m2086764285(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Matrix4x4::Equals(System.Object)
extern "C"  bool Matrix4x4_Equals_m1131202087 (Matrix4x4_t3534180298 * __this, RuntimeObject * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Matrix4x4_Equals_m1131202087_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Matrix4x4_t3534180298  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector4_t4052901136  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector4_t4052901136  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector4_t4052901136  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector4_t4052901136  V_5;
	memset(&V_5, 0, sizeof(V_5));
	int32_t G_B7_0 = 0;
	{
		RuntimeObject * L_0 = ___other0;
		if (((RuntimeObject *)IsInstSealed((RuntimeObject*)L_0, Matrix4x4_t3534180298_il2cpp_TypeInfo_var)))
		{
			goto IL_0013;
		}
	}
	{
		V_0 = (bool)0;
		goto IL_00bc;
	}

IL_0013:
	{
		RuntimeObject * L_1 = ___other0;
		V_1 = ((*(Matrix4x4_t3534180298 *)((Matrix4x4_t3534180298 *)UnBox(L_1, Matrix4x4_t3534180298_il2cpp_TypeInfo_var))));
		Vector4_t4052901136  L_2 = Matrix4x4_GetColumn_m1315812967(__this, 0, /*hidden argument*/NULL);
		V_2 = L_2;
		Vector4_t4052901136  L_3 = Matrix4x4_GetColumn_m1315812967((&V_1), 0, /*hidden argument*/NULL);
		Vector4_t4052901136  L_4 = L_3;
		RuntimeObject * L_5 = Box(Vector4_t4052901136_il2cpp_TypeInfo_var, &L_4);
		bool L_6 = Vector4_Equals_m480591454((&V_2), L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_00b5;
		}
	}
	{
		Vector4_t4052901136  L_7 = Matrix4x4_GetColumn_m1315812967(__this, 1, /*hidden argument*/NULL);
		V_3 = L_7;
		Vector4_t4052901136  L_8 = Matrix4x4_GetColumn_m1315812967((&V_1), 1, /*hidden argument*/NULL);
		Vector4_t4052901136  L_9 = L_8;
		RuntimeObject * L_10 = Box(Vector4_t4052901136_il2cpp_TypeInfo_var, &L_9);
		bool L_11 = Vector4_Equals_m480591454((&V_3), L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_00b5;
		}
	}
	{
		Vector4_t4052901136  L_12 = Matrix4x4_GetColumn_m1315812967(__this, 2, /*hidden argument*/NULL);
		V_4 = L_12;
		Vector4_t4052901136  L_13 = Matrix4x4_GetColumn_m1315812967((&V_1), 2, /*hidden argument*/NULL);
		Vector4_t4052901136  L_14 = L_13;
		RuntimeObject * L_15 = Box(Vector4_t4052901136_il2cpp_TypeInfo_var, &L_14);
		bool L_16 = Vector4_Equals_m480591454((&V_4), L_15, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_00b5;
		}
	}
	{
		Vector4_t4052901136  L_17 = Matrix4x4_GetColumn_m1315812967(__this, 3, /*hidden argument*/NULL);
		V_5 = L_17;
		Vector4_t4052901136  L_18 = Matrix4x4_GetColumn_m1315812967((&V_1), 3, /*hidden argument*/NULL);
		Vector4_t4052901136  L_19 = L_18;
		RuntimeObject * L_20 = Box(Vector4_t4052901136_il2cpp_TypeInfo_var, &L_19);
		bool L_21 = Vector4_Equals_m480591454((&V_5), L_20, /*hidden argument*/NULL);
		G_B7_0 = ((int32_t)(L_21));
		goto IL_00b6;
	}

IL_00b5:
	{
		G_B7_0 = 0;
	}

IL_00b6:
	{
		V_0 = (bool)G_B7_0;
		goto IL_00bc;
	}

IL_00bc:
	{
		bool L_22 = V_0;
		return L_22;
	}
}
extern "C"  bool Matrix4x4_Equals_m1131202087_AdjustorThunk (RuntimeObject * __this, RuntimeObject * ___other0, const RuntimeMethod* method)
{
	Matrix4x4_t3534180298 * _thisAdjusted = reinterpret_cast<Matrix4x4_t3534180298 *>(__this + 1);
	return Matrix4x4_Equals_m1131202087(_thisAdjusted, ___other0, method);
}
// UnityEngine.Vector4 UnityEngine.Matrix4x4::GetColumn(System.Int32)
extern "C"  Vector4_t4052901136  Matrix4x4_GetColumn_m1315812967 (Matrix4x4_t3534180298 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Matrix4x4_GetColumn_m1315812967_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector4_t4052901136  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		switch (L_0)
		{
			case 0:
			{
				goto IL_001c;
			}
			case 1:
			{
				goto IL_003f;
			}
			case 2:
			{
				goto IL_0062;
			}
			case 3:
			{
				goto IL_0085;
			}
		}
	}
	{
		goto IL_00a8;
	}

IL_001c:
	{
		float L_1 = __this->get_m00_0();
		float L_2 = __this->get_m10_1();
		float L_3 = __this->get_m20_2();
		float L_4 = __this->get_m30_3();
		Vector4_t4052901136  L_5;
		memset(&L_5, 0, sizeof(L_5));
		Vector4__ctor_m3054764420((&L_5), L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		goto IL_00b3;
	}

IL_003f:
	{
		float L_6 = __this->get_m01_4();
		float L_7 = __this->get_m11_5();
		float L_8 = __this->get_m21_6();
		float L_9 = __this->get_m31_7();
		Vector4_t4052901136  L_10;
		memset(&L_10, 0, sizeof(L_10));
		Vector4__ctor_m3054764420((&L_10), L_6, L_7, L_8, L_9, /*hidden argument*/NULL);
		V_0 = L_10;
		goto IL_00b3;
	}

IL_0062:
	{
		float L_11 = __this->get_m02_8();
		float L_12 = __this->get_m12_9();
		float L_13 = __this->get_m22_10();
		float L_14 = __this->get_m32_11();
		Vector4_t4052901136  L_15;
		memset(&L_15, 0, sizeof(L_15));
		Vector4__ctor_m3054764420((&L_15), L_11, L_12, L_13, L_14, /*hidden argument*/NULL);
		V_0 = L_15;
		goto IL_00b3;
	}

IL_0085:
	{
		float L_16 = __this->get_m03_12();
		float L_17 = __this->get_m13_13();
		float L_18 = __this->get_m23_14();
		float L_19 = __this->get_m33_15();
		Vector4_t4052901136  L_20;
		memset(&L_20, 0, sizeof(L_20));
		Vector4__ctor_m3054764420((&L_20), L_16, L_17, L_18, L_19, /*hidden argument*/NULL);
		V_0 = L_20;
		goto IL_00b3;
	}

IL_00a8:
	{
		IndexOutOfRangeException_t3041194587 * L_21 = (IndexOutOfRangeException_t3041194587 *)il2cpp_codegen_object_new(IndexOutOfRangeException_t3041194587_il2cpp_TypeInfo_var);
		IndexOutOfRangeException__ctor_m2238521308(L_21, _stringLiteral2500430000, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_21);
	}

IL_00b3:
	{
		Vector4_t4052901136  L_22 = V_0;
		return L_22;
	}
}
extern "C"  Vector4_t4052901136  Matrix4x4_GetColumn_m1315812967_AdjustorThunk (RuntimeObject * __this, int32_t ___index0, const RuntimeMethod* method)
{
	Matrix4x4_t3534180298 * _thisAdjusted = reinterpret_cast<Matrix4x4_t3534180298 *>(__this + 1);
	return Matrix4x4_GetColumn_m1315812967(_thisAdjusted, ___index0, method);
}
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::get_identity()
extern "C"  Matrix4x4_t3534180298  Matrix4x4_get_identity_m3045349954 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Matrix4x4_get_identity_m3045349954_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Matrix4x4_t3534180298  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Matrix4x4_t3534180298_il2cpp_TypeInfo_var);
		Matrix4x4_t3534180298  L_0 = ((Matrix4x4_t3534180298_StaticFields*)il2cpp_codegen_static_fields_for(Matrix4x4_t3534180298_il2cpp_TypeInfo_var))->get_identityMatrix_17();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		Matrix4x4_t3534180298  L_1 = V_0;
		return L_1;
	}
}
// System.String UnityEngine.Matrix4x4::ToString()
extern "C"  String_t* Matrix4x4_ToString_m861927258 (Matrix4x4_t3534180298 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Matrix4x4_ToString_m861927258_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		ObjectU5BU5D_t4290686858* L_0 = ((ObjectU5BU5D_t4290686858*)SZArrayNew(ObjectU5BU5D_t4290686858_il2cpp_TypeInfo_var, (uint32_t)((int32_t)16)));
		float L_1 = __this->get_m00_0();
		float L_2 = L_1;
		RuntimeObject * L_3 = Box(Single_t496865882_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_3);
		ObjectU5BU5D_t4290686858* L_4 = L_0;
		float L_5 = __this->get_m01_4();
		float L_6 = L_5;
		RuntimeObject * L_7 = Box(Single_t496865882_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_7);
		ObjectU5BU5D_t4290686858* L_8 = L_4;
		float L_9 = __this->get_m02_8();
		float L_10 = L_9;
		RuntimeObject * L_11 = Box(Single_t496865882_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_11);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_11);
		ObjectU5BU5D_t4290686858* L_12 = L_8;
		float L_13 = __this->get_m03_12();
		float L_14 = L_13;
		RuntimeObject * L_15 = Box(Single_t496865882_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, L_15);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_15);
		ObjectU5BU5D_t4290686858* L_16 = L_12;
		float L_17 = __this->get_m10_1();
		float L_18 = L_17;
		RuntimeObject * L_19 = Box(Single_t496865882_il2cpp_TypeInfo_var, &L_18);
		NullCheck(L_16);
		ArrayElementTypeCheck (L_16, L_19);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(4), (RuntimeObject *)L_19);
		ObjectU5BU5D_t4290686858* L_20 = L_16;
		float L_21 = __this->get_m11_5();
		float L_22 = L_21;
		RuntimeObject * L_23 = Box(Single_t496865882_il2cpp_TypeInfo_var, &L_22);
		NullCheck(L_20);
		ArrayElementTypeCheck (L_20, L_23);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(5), (RuntimeObject *)L_23);
		ObjectU5BU5D_t4290686858* L_24 = L_20;
		float L_25 = __this->get_m12_9();
		float L_26 = L_25;
		RuntimeObject * L_27 = Box(Single_t496865882_il2cpp_TypeInfo_var, &L_26);
		NullCheck(L_24);
		ArrayElementTypeCheck (L_24, L_27);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(6), (RuntimeObject *)L_27);
		ObjectU5BU5D_t4290686858* L_28 = L_24;
		float L_29 = __this->get_m13_13();
		float L_30 = L_29;
		RuntimeObject * L_31 = Box(Single_t496865882_il2cpp_TypeInfo_var, &L_30);
		NullCheck(L_28);
		ArrayElementTypeCheck (L_28, L_31);
		(L_28)->SetAt(static_cast<il2cpp_array_size_t>(7), (RuntimeObject *)L_31);
		ObjectU5BU5D_t4290686858* L_32 = L_28;
		float L_33 = __this->get_m20_2();
		float L_34 = L_33;
		RuntimeObject * L_35 = Box(Single_t496865882_il2cpp_TypeInfo_var, &L_34);
		NullCheck(L_32);
		ArrayElementTypeCheck (L_32, L_35);
		(L_32)->SetAt(static_cast<il2cpp_array_size_t>(8), (RuntimeObject *)L_35);
		ObjectU5BU5D_t4290686858* L_36 = L_32;
		float L_37 = __this->get_m21_6();
		float L_38 = L_37;
		RuntimeObject * L_39 = Box(Single_t496865882_il2cpp_TypeInfo_var, &L_38);
		NullCheck(L_36);
		ArrayElementTypeCheck (L_36, L_39);
		(L_36)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)9)), (RuntimeObject *)L_39);
		ObjectU5BU5D_t4290686858* L_40 = L_36;
		float L_41 = __this->get_m22_10();
		float L_42 = L_41;
		RuntimeObject * L_43 = Box(Single_t496865882_il2cpp_TypeInfo_var, &L_42);
		NullCheck(L_40);
		ArrayElementTypeCheck (L_40, L_43);
		(L_40)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)10)), (RuntimeObject *)L_43);
		ObjectU5BU5D_t4290686858* L_44 = L_40;
		float L_45 = __this->get_m23_14();
		float L_46 = L_45;
		RuntimeObject * L_47 = Box(Single_t496865882_il2cpp_TypeInfo_var, &L_46);
		NullCheck(L_44);
		ArrayElementTypeCheck (L_44, L_47);
		(L_44)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)11)), (RuntimeObject *)L_47);
		ObjectU5BU5D_t4290686858* L_48 = L_44;
		float L_49 = __this->get_m30_3();
		float L_50 = L_49;
		RuntimeObject * L_51 = Box(Single_t496865882_il2cpp_TypeInfo_var, &L_50);
		NullCheck(L_48);
		ArrayElementTypeCheck (L_48, L_51);
		(L_48)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)12)), (RuntimeObject *)L_51);
		ObjectU5BU5D_t4290686858* L_52 = L_48;
		float L_53 = __this->get_m31_7();
		float L_54 = L_53;
		RuntimeObject * L_55 = Box(Single_t496865882_il2cpp_TypeInfo_var, &L_54);
		NullCheck(L_52);
		ArrayElementTypeCheck (L_52, L_55);
		(L_52)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)13)), (RuntimeObject *)L_55);
		ObjectU5BU5D_t4290686858* L_56 = L_52;
		float L_57 = __this->get_m32_11();
		float L_58 = L_57;
		RuntimeObject * L_59 = Box(Single_t496865882_il2cpp_TypeInfo_var, &L_58);
		NullCheck(L_56);
		ArrayElementTypeCheck (L_56, L_59);
		(L_56)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)14)), (RuntimeObject *)L_59);
		ObjectU5BU5D_t4290686858* L_60 = L_56;
		float L_61 = __this->get_m33_15();
		float L_62 = L_61;
		RuntimeObject * L_63 = Box(Single_t496865882_il2cpp_TypeInfo_var, &L_62);
		NullCheck(L_60);
		ArrayElementTypeCheck (L_60, L_63);
		(L_60)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)15)), (RuntimeObject *)L_63);
		String_t* L_64 = UnityString_Format_m2513837565(NULL /*static, unused*/, _stringLiteral2341087010, L_60, /*hidden argument*/NULL);
		V_0 = L_64;
		goto IL_00ff;
	}

IL_00ff:
	{
		String_t* L_65 = V_0;
		return L_65;
	}
}
extern "C"  String_t* Matrix4x4_ToString_m861927258_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Matrix4x4_t3534180298 * _thisAdjusted = reinterpret_cast<Matrix4x4_t3534180298 *>(__this + 1);
	return Matrix4x4_ToString_m861927258(_thisAdjusted, method);
}
// System.Void UnityEngine.Matrix4x4::.cctor()
extern "C"  void Matrix4x4__cctor_m770025791 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Matrix4x4__cctor_m770025791_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Vector4_t4052901136  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Vector4__ctor_m3054764420((&L_0), (0.0f), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		Vector4_t4052901136  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Vector4__ctor_m3054764420((&L_1), (0.0f), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		Vector4_t4052901136  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector4__ctor_m3054764420((&L_2), (0.0f), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		Vector4_t4052901136  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Vector4__ctor_m3054764420((&L_3), (0.0f), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		Matrix4x4_t3534180298  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Matrix4x4__ctor_m614016869((&L_4), L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		((Matrix4x4_t3534180298_StaticFields*)il2cpp_codegen_static_fields_for(Matrix4x4_t3534180298_il2cpp_TypeInfo_var))->set_zeroMatrix_16(L_4);
		Vector4_t4052901136  L_5;
		memset(&L_5, 0, sizeof(L_5));
		Vector4__ctor_m3054764420((&L_5), (1.0f), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		Vector4_t4052901136  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Vector4__ctor_m3054764420((&L_6), (0.0f), (1.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		Vector4_t4052901136  L_7;
		memset(&L_7, 0, sizeof(L_7));
		Vector4__ctor_m3054764420((&L_7), (0.0f), (0.0f), (1.0f), (0.0f), /*hidden argument*/NULL);
		Vector4_t4052901136  L_8;
		memset(&L_8, 0, sizeof(L_8));
		Vector4__ctor_m3054764420((&L_8), (0.0f), (0.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		Matrix4x4_t3534180298  L_9;
		memset(&L_9, 0, sizeof(L_9));
		Matrix4x4__ctor_m614016869((&L_9), L_5, L_6, L_7, L_8, /*hidden argument*/NULL);
		((Matrix4x4_t3534180298_StaticFields*)il2cpp_codegen_static_fields_for(Matrix4x4_t3534180298_il2cpp_TypeInfo_var))->set_identityMatrix_17(L_9);
		return;
	}
}
// System.Void UnityEngine.MonoBehaviour::.ctor()
extern "C"  void MonoBehaviour__ctor_m3125940511 (MonoBehaviour_t2904604993 * __this, const RuntimeMethod* method)
{
	{
		Behaviour__ctor_m1233868026(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.NativeClassAttribute::.ctor(System.String)
extern "C"  void NativeClassAttribute__ctor_m714604756 (NativeClassAttribute_t3085471410 * __this, String_t* ___qualifiedCppName0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m2897993467(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___qualifiedCppName0;
		NativeClassAttribute_set_QualifiedNativeName_m875392448(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.NativeClassAttribute::set_QualifiedNativeName(System.String)
extern "C"  void NativeClassAttribute_set_QualifiedNativeName_m875392448 (NativeClassAttribute_t3085471410 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CQualifiedNativeNameU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Void UnityEngine.Networking.PlayerConnection.MessageEventArgs::.ctor()
extern "C"  void MessageEventArgs__ctor_m1689534235 (MessageEventArgs_t947408028 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m3446193457(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Networking.PlayerConnection.PlayerConnection::.ctor()
extern "C"  void PlayerConnection__ctor_m527712293 (PlayerConnection_t3559457672 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerConnection__ctor_m527712293_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		PlayerEditorConnectionEvents_t2151463461 * L_0 = (PlayerEditorConnectionEvents_t2151463461 *)il2cpp_codegen_object_new(PlayerEditorConnectionEvents_t2151463461_il2cpp_TypeInfo_var);
		PlayerEditorConnectionEvents__ctor_m1455086094(L_0, /*hidden argument*/NULL);
		__this->set_m_PlayerEditorConnectionEvents_2(L_0);
		List_1_t449703515 * L_1 = (List_1_t449703515 *)il2cpp_codegen_object_new(List_1_t449703515_il2cpp_TypeInfo_var);
		List_1__ctor_m2518762847(L_1, /*hidden argument*/List_1__ctor_m2518762847_RuntimeMethod_var);
		__this->set_m_connectedPlayers_3(L_1);
		ScriptableObject__ctor_m3589061004(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Networking.PlayerConnection.PlayerConnection UnityEngine.Networking.PlayerConnection.PlayerConnection::get_instance()
extern "C"  PlayerConnection_t3559457672 * PlayerConnection_get_instance_m2190863265 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerConnection_get_instance_m2190863265_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	PlayerConnection_t3559457672 * V_0 = NULL;
	{
		PlayerConnection_t3559457672 * L_0 = ((PlayerConnection_t3559457672_StaticFields*)il2cpp_codegen_static_fields_for(PlayerConnection_t3559457672_il2cpp_TypeInfo_var))->get_s_Instance_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t2461733472_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m2743582905(NULL /*static, unused*/, L_0, (Object_t2461733472 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		PlayerConnection_t3559457672 * L_2 = PlayerConnection_CreateInstance_m4274468596(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0028;
	}

IL_001d:
	{
		PlayerConnection_t3559457672 * L_3 = ((PlayerConnection_t3559457672_StaticFields*)il2cpp_codegen_static_fields_for(PlayerConnection_t3559457672_il2cpp_TypeInfo_var))->get_s_Instance_4();
		V_0 = L_3;
		goto IL_0028;
	}

IL_0028:
	{
		PlayerConnection_t3559457672 * L_4 = V_0;
		return L_4;
	}
}
// UnityEngine.Networking.PlayerConnection.PlayerConnection UnityEngine.Networking.PlayerConnection.PlayerConnection::CreateInstance()
extern "C"  PlayerConnection_t3559457672 * PlayerConnection_CreateInstance_m4274468596 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerConnection_CreateInstance_m4274468596_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	PlayerConnection_t3559457672 * V_0 = NULL;
	{
		PlayerConnection_t3559457672 * L_0 = ScriptableObject_CreateInstance_TisPlayerConnection_t3559457672_m1179650981(NULL /*static, unused*/, /*hidden argument*/ScriptableObject_CreateInstance_TisPlayerConnection_t3559457672_m1179650981_RuntimeMethod_var);
		((PlayerConnection_t3559457672_StaticFields*)il2cpp_codegen_static_fields_for(PlayerConnection_t3559457672_il2cpp_TypeInfo_var))->set_s_Instance_4(L_0);
		PlayerConnection_t3559457672 * L_1 = ((PlayerConnection_t3559457672_StaticFields*)il2cpp_codegen_static_fields_for(PlayerConnection_t3559457672_il2cpp_TypeInfo_var))->get_s_Instance_4();
		NullCheck(L_1);
		Object_set_hideFlags_m3010783544(L_1, ((int32_t)61), /*hidden argument*/NULL);
		PlayerConnection_t3559457672 * L_2 = ((PlayerConnection_t3559457672_StaticFields*)il2cpp_codegen_static_fields_for(PlayerConnection_t3559457672_il2cpp_TypeInfo_var))->get_s_Instance_4();
		V_0 = L_2;
		goto IL_0022;
	}

IL_0022:
	{
		PlayerConnection_t3559457672 * L_3 = V_0;
		return L_3;
	}
}
// System.Void UnityEngine.Networking.PlayerConnection.PlayerConnection::MessageCallbackInternal(System.IntPtr,System.UInt64,System.UInt64,System.String)
extern "C"  void PlayerConnection_MessageCallbackInternal_m469132778 (RuntimeObject * __this /* static, unused */, intptr_t ___data0, uint64_t ___size1, uint64_t ___guid2, String_t* ___messageId3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerConnection_MessageCallbackInternal_m469132778_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ByteU5BU5D_t1014014593* V_0 = NULL;
	{
		V_0 = (ByteU5BU5D_t1014014593*)NULL;
		uint64_t L_0 = ___size1;
		if ((!(((uint64_t)L_0) > ((uint64_t)(((int64_t)((int64_t)0)))))))
		{
			goto IL_001f;
		}
	}
	{
		uint64_t L_1 = ___size1;
		if ((uint64_t)(L_1) > INTPTR_MAX) IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_overflow_exception());
		V_0 = ((ByteU5BU5D_t1014014593*)SZArrayNew(ByteU5BU5D_t1014014593_il2cpp_TypeInfo_var, (uint32_t)(((intptr_t)L_1))));
		intptr_t L_2 = ___data0;
		ByteU5BU5D_t1014014593* L_3 = V_0;
		uint64_t L_4 = ___size1;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t4207306895_il2cpp_TypeInfo_var);
		Marshal_Copy_m3898306904(NULL /*static, unused*/, L_2, L_3, 0, (((int32_t)((int32_t)L_4))), /*hidden argument*/NULL);
	}

IL_001f:
	{
		PlayerConnection_t3559457672 * L_5 = PlayerConnection_get_instance_m2190863265(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_5);
		PlayerEditorConnectionEvents_t2151463461 * L_6 = L_5->get_m_PlayerEditorConnectionEvents_2();
		String_t* L_7 = ___messageId3;
		Guid_t  L_8;
		memset(&L_8, 0, sizeof(L_8));
		Guid__ctor_m1415939458((&L_8), L_7, /*hidden argument*/NULL);
		ByteU5BU5D_t1014014593* L_9 = V_0;
		uint64_t L_10 = ___guid2;
		NullCheck(L_6);
		PlayerEditorConnectionEvents_InvokeMessageIdSubscribers_m2032614817(L_6, L_8, L_9, (((int32_t)((int32_t)L_10))), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Networking.PlayerConnection.PlayerConnection::ConnectedCallbackInternal(System.Int32)
extern "C"  void PlayerConnection_ConnectedCallbackInternal_m1135070508 (RuntimeObject * __this /* static, unused */, int32_t ___playerId0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerConnection_ConnectedCallbackInternal_m1135070508_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		PlayerConnection_t3559457672 * L_0 = PlayerConnection_get_instance_m2190863265(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		List_1_t449703515 * L_1 = L_0->get_m_connectedPlayers_3();
		int32_t L_2 = ___playerId0;
		NullCheck(L_1);
		List_1_Add_m3295068314(L_1, L_2, /*hidden argument*/List_1_Add_m3295068314_RuntimeMethod_var);
		PlayerConnection_t3559457672 * L_3 = PlayerConnection_get_instance_m2190863265(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		PlayerEditorConnectionEvents_t2151463461 * L_4 = L_3->get_m_PlayerEditorConnectionEvents_2();
		NullCheck(L_4);
		ConnectionChangeEvent_t2308234365 * L_5 = L_4->get_connectionEvent_1();
		int32_t L_6 = ___playerId0;
		NullCheck(L_5);
		UnityEvent_1_Invoke_m4202823606(L_5, L_6, /*hidden argument*/UnityEvent_1_Invoke_m4202823606_RuntimeMethod_var);
		return;
	}
}
// System.Void UnityEngine.Networking.PlayerConnection.PlayerConnection::DisconnectedCallback(System.Int32)
extern "C"  void PlayerConnection_DisconnectedCallback_m2631702132 (RuntimeObject * __this /* static, unused */, int32_t ___playerId0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerConnection_DisconnectedCallback_m2631702132_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		PlayerConnection_t3559457672 * L_0 = PlayerConnection_get_instance_m2190863265(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		PlayerEditorConnectionEvents_t2151463461 * L_1 = L_0->get_m_PlayerEditorConnectionEvents_2();
		NullCheck(L_1);
		ConnectionChangeEvent_t2308234365 * L_2 = L_1->get_disconnectionEvent_2();
		int32_t L_3 = ___playerId0;
		NullCheck(L_2);
		UnityEvent_1_Invoke_m4202823606(L_2, L_3, /*hidden argument*/UnityEvent_1_Invoke_m4202823606_RuntimeMethod_var);
		return;
	}
}
// System.Void UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents::.ctor()
extern "C"  void PlayerEditorConnectionEvents__ctor_m1455086094 (PlayerEditorConnectionEvents_t2151463461 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerEditorConnectionEvents__ctor_m1455086094_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t3831528118 * L_0 = (List_1_t3831528118 *)il2cpp_codegen_object_new(List_1_t3831528118_il2cpp_TypeInfo_var);
		List_1__ctor_m539526798(L_0, /*hidden argument*/List_1__ctor_m539526798_RuntimeMethod_var);
		__this->set_messageTypeSubscribers_0(L_0);
		ConnectionChangeEvent_t2308234365 * L_1 = (ConnectionChangeEvent_t2308234365 *)il2cpp_codegen_object_new(ConnectionChangeEvent_t2308234365_il2cpp_TypeInfo_var);
		ConnectionChangeEvent__ctor_m1785426546(L_1, /*hidden argument*/NULL);
		__this->set_connectionEvent_1(L_1);
		ConnectionChangeEvent_t2308234365 * L_2 = (ConnectionChangeEvent_t2308234365 *)il2cpp_codegen_object_new(ConnectionChangeEvent_t2308234365_il2cpp_TypeInfo_var);
		ConnectionChangeEvent__ctor_m1785426546(L_2, /*hidden argument*/NULL);
		__this->set_disconnectionEvent_2(L_2);
		Object__ctor_m3446193457(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents::InvokeMessageIdSubscribers(System.Guid,System.Byte[],System.Int32)
extern "C"  void PlayerEditorConnectionEvents_InvokeMessageIdSubscribers_m2032614817 (PlayerEditorConnectionEvents_t2151463461 * __this, Guid_t  ___messageId0, ByteU5BU5D_t1014014593* ___data1, int32_t ___playerId2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerEditorConnectionEvents_InvokeMessageIdSubscribers_m2032614817_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CInvokeMessageIdSubscribersU3Ec__AnonStorey0_t1928087868 * V_0 = NULL;
	RuntimeObject* V_1 = NULL;
	MessageEventArgs_t947408028 * V_2 = NULL;
	MessageEventArgs_t947408028 * V_3 = NULL;
	MessageTypeSubscribers_t1657992506 * V_4 = NULL;
	RuntimeObject* V_5 = NULL;
	Exception_t3361176243 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3361176243 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		U3CInvokeMessageIdSubscribersU3Ec__AnonStorey0_t1928087868 * L_0 = (U3CInvokeMessageIdSubscribersU3Ec__AnonStorey0_t1928087868 *)il2cpp_codegen_object_new(U3CInvokeMessageIdSubscribersU3Ec__AnonStorey0_t1928087868_il2cpp_TypeInfo_var);
		U3CInvokeMessageIdSubscribersU3Ec__AnonStorey0__ctor_m1102967907(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CInvokeMessageIdSubscribersU3Ec__AnonStorey0_t1928087868 * L_1 = V_0;
		Guid_t  L_2 = ___messageId0;
		NullCheck(L_1);
		L_1->set_messageId_0(L_2);
		List_1_t3831528118 * L_3 = __this->get_messageTypeSubscribers_0();
		U3CInvokeMessageIdSubscribersU3Ec__AnonStorey0_t1928087868 * L_4 = V_0;
		intptr_t L_5 = (intptr_t)U3CInvokeMessageIdSubscribersU3Ec__AnonStorey0_U3CU3Em__0_m974971376_RuntimeMethod_var;
		Func_2_t874238247 * L_6 = (Func_2_t874238247 *)il2cpp_codegen_object_new(Func_2_t874238247_il2cpp_TypeInfo_var);
		Func_2__ctor_m1624349331(L_6, L_4, L_5, /*hidden argument*/Func_2__ctor_m1624349331_RuntimeMethod_var);
		RuntimeObject* L_7 = Enumerable_Where_TisMessageTypeSubscribers_t1657992506_m190799551(NULL /*static, unused*/, L_3, L_6, /*hidden argument*/Enumerable_Where_TisMessageTypeSubscribers_t1657992506_m190799551_RuntimeMethod_var);
		V_1 = L_7;
		RuntimeObject* L_8 = V_1;
		bool L_9 = Enumerable_Any_TisMessageTypeSubscribers_t1657992506_m1081467775(NULL /*static, unused*/, L_8, /*hidden argument*/Enumerable_Any_TisMessageTypeSubscribers_t1657992506_m1081467775_RuntimeMethod_var);
		if (L_9)
		{
			goto IL_0051;
		}
	}
	{
		U3CInvokeMessageIdSubscribersU3Ec__AnonStorey0_t1928087868 * L_10 = V_0;
		NullCheck(L_10);
		Guid_t  L_11 = L_10->get_messageId_0();
		Guid_t  L_12 = L_11;
		RuntimeObject * L_13 = Box(Guid_t_il2cpp_TypeInfo_var, &L_12);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m2669786253(NULL /*static, unused*/, _stringLiteral3857039302, L_13, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t2165388762_il2cpp_TypeInfo_var);
		Debug_LogError_m1070074371(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		goto IL_00ad;
	}

IL_0051:
	{
		MessageEventArgs_t947408028 * L_15 = (MessageEventArgs_t947408028 *)il2cpp_codegen_object_new(MessageEventArgs_t947408028_il2cpp_TypeInfo_var);
		MessageEventArgs__ctor_m1689534235(L_15, /*hidden argument*/NULL);
		V_3 = L_15;
		MessageEventArgs_t947408028 * L_16 = V_3;
		int32_t L_17 = ___playerId2;
		NullCheck(L_16);
		L_16->set_playerId_0(L_17);
		MessageEventArgs_t947408028 * L_18 = V_3;
		ByteU5BU5D_t1014014593* L_19 = ___data1;
		NullCheck(L_18);
		L_18->set_data_1(L_19);
		MessageEventArgs_t947408028 * L_20 = V_3;
		V_2 = L_20;
		RuntimeObject* L_21 = V_1;
		NullCheck(L_21);
		RuntimeObject* L_22 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers>::GetEnumerator() */, IEnumerable_1_t4069794933_il2cpp_TypeInfo_var, L_21);
		V_5 = L_22;
	}

IL_0070:
	try
	{ // begin try (depth: 1)
		{
			goto IL_008d;
		}

IL_0075:
		{
			RuntimeObject* L_23 = V_5;
			NullCheck(L_23);
			MessageTypeSubscribers_t1657992506 * L_24 = InterfaceFuncInvoker0< MessageTypeSubscribers_t1657992506 * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers>::get_Current() */, IEnumerator_1_t603347181_il2cpp_TypeInfo_var, L_23);
			V_4 = L_24;
			MessageTypeSubscribers_t1657992506 * L_25 = V_4;
			NullCheck(L_25);
			MessageEvent_t2016149491 * L_26 = L_25->get_messageCallback_2();
			MessageEventArgs_t947408028 * L_27 = V_2;
			NullCheck(L_26);
			UnityEvent_1_Invoke_m523984493(L_26, L_27, /*hidden argument*/UnityEvent_1_Invoke_m523984493_RuntimeMethod_var);
		}

IL_008d:
		{
			RuntimeObject* L_28 = V_5;
			NullCheck(L_28);
			bool L_29 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t4262330110_il2cpp_TypeInfo_var, L_28);
			if (L_29)
			{
				goto IL_0075;
			}
		}

IL_0099:
		{
			IL2CPP_LEAVE(0xAD, FINALLY_009e);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3361176243 *)e.ex;
		goto FINALLY_009e;
	}

FINALLY_009e:
	{ // begin finally (depth: 1)
		{
			RuntimeObject* L_30 = V_5;
			if (!L_30)
			{
				goto IL_00ac;
			}
		}

IL_00a5:
		{
			RuntimeObject* L_31 = V_5;
			NullCheck(L_31);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t318870991_il2cpp_TypeInfo_var, L_31);
		}

IL_00ac:
		{
			IL2CPP_END_FINALLY(158)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(158)
	{
		IL2CPP_JUMP_TBL(0xAD, IL_00ad)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3361176243 *)
	}

IL_00ad:
	{
		return;
	}
}
// System.Void UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/<InvokeMessageIdSubscribers>c__AnonStorey0::.ctor()
extern "C"  void U3CInvokeMessageIdSubscribersU3Ec__AnonStorey0__ctor_m1102967907 (U3CInvokeMessageIdSubscribersU3Ec__AnonStorey0_t1928087868 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m3446193457(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/<InvokeMessageIdSubscribers>c__AnonStorey0::<>m__0(UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers)
extern "C"  bool U3CInvokeMessageIdSubscribersU3Ec__AnonStorey0_U3CU3Em__0_m974971376 (U3CInvokeMessageIdSubscribersU3Ec__AnonStorey0_t1928087868 * __this, MessageTypeSubscribers_t1657992506 * ___x0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CInvokeMessageIdSubscribersU3Ec__AnonStorey0_U3CU3Em__0_m974971376_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		MessageTypeSubscribers_t1657992506 * L_0 = ___x0;
		NullCheck(L_0);
		Guid_t  L_1 = MessageTypeSubscribers_get_MessageTypeId_m2156269021(L_0, /*hidden argument*/NULL);
		Guid_t  L_2 = __this->get_messageId_0();
		IL2CPP_RUNTIME_CLASS_INIT(Guid_t_il2cpp_TypeInfo_var);
		bool L_3 = Guid_op_Equality_m3918449706(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_0017;
	}

IL_0017:
	{
		bool L_4 = V_0;
		return L_4;
	}
}
// System.Void UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/ConnectionChangeEvent::.ctor()
extern "C"  void ConnectionChangeEvent__ctor_m1785426546 (ConnectionChangeEvent_t2308234365 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ConnectionChangeEvent__ctor_m1785426546_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		UnityEvent_1__ctor_m1279621320(__this, /*hidden argument*/UnityEvent_1__ctor_m1279621320_RuntimeMethod_var);
		return;
	}
}
// System.Void UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageEvent::.ctor()
extern "C"  void MessageEvent__ctor_m3751876958 (MessageEvent_t2016149491 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MessageEvent__ctor_m3751876958_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		UnityEvent_1__ctor_m183532629(__this, /*hidden argument*/UnityEvent_1__ctor_m183532629_RuntimeMethod_var);
		return;
	}
}
// System.Void UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers::.ctor()
extern "C"  void MessageTypeSubscribers__ctor_m2303372877 (MessageTypeSubscribers_t1657992506 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MessageTypeSubscribers__ctor_m2303372877_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_subscriberCount_1(0);
		MessageEvent_t2016149491 * L_0 = (MessageEvent_t2016149491 *)il2cpp_codegen_object_new(MessageEvent_t2016149491_il2cpp_TypeInfo_var);
		MessageEvent__ctor_m3751876958(L_0, /*hidden argument*/NULL);
		__this->set_messageCallback_2(L_0);
		Object__ctor_m3446193457(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Guid UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers::get_MessageTypeId()
extern "C"  Guid_t  MessageTypeSubscribers_get_MessageTypeId_m2156269021 (MessageTypeSubscribers_t1657992506 * __this, const RuntimeMethod* method)
{
	Guid_t  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		String_t* L_0 = __this->get_m_messageTypeId_0();
		Guid_t  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Guid__ctor_m1415939458((&L_1), L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		Guid_t  L_2 = V_0;
		return L_2;
	}
}
// Conversion methods for marshalling of: UnityEngine.Object
extern "C" void Object_t2461733472_marshal_pinvoke(const Object_t2461733472& unmarshaled, Object_t2461733472_marshaled_pinvoke& marshaled)
{
	marshaled.___m_CachedPtr_0 = unmarshaled.get_m_CachedPtr_0();
}
extern "C" void Object_t2461733472_marshal_pinvoke_back(const Object_t2461733472_marshaled_pinvoke& marshaled, Object_t2461733472& unmarshaled)
{
	intptr_t unmarshaled_m_CachedPtr_temp_0;
	memset(&unmarshaled_m_CachedPtr_temp_0, 0, sizeof(unmarshaled_m_CachedPtr_temp_0));
	unmarshaled_m_CachedPtr_temp_0 = marshaled.___m_CachedPtr_0;
	unmarshaled.set_m_CachedPtr_0(unmarshaled_m_CachedPtr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.Object
extern "C" void Object_t2461733472_marshal_pinvoke_cleanup(Object_t2461733472_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.Object
extern "C" void Object_t2461733472_marshal_com(const Object_t2461733472& unmarshaled, Object_t2461733472_marshaled_com& marshaled)
{
	marshaled.___m_CachedPtr_0 = unmarshaled.get_m_CachedPtr_0();
}
extern "C" void Object_t2461733472_marshal_com_back(const Object_t2461733472_marshaled_com& marshaled, Object_t2461733472& unmarshaled)
{
	intptr_t unmarshaled_m_CachedPtr_temp_0;
	memset(&unmarshaled_m_CachedPtr_temp_0, 0, sizeof(unmarshaled_m_CachedPtr_temp_0));
	unmarshaled_m_CachedPtr_temp_0 = marshaled.___m_CachedPtr_0;
	unmarshaled.set_m_CachedPtr_0(unmarshaled_m_CachedPtr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.Object
extern "C" void Object_t2461733472_marshal_com_cleanup(Object_t2461733472_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.Object::.ctor()
extern "C"  void Object__ctor_m387379410 (Object_t2461733472 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m3446193457(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Object::set_hideFlags(UnityEngine.HideFlags)
extern "C"  void Object_set_hideFlags_m3010783544 (Object_t2461733472 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	typedef void (*Object_set_hideFlags_m3010783544_ftn) (Object_t2461733472 *, int32_t);
	static Object_set_hideFlags_m3010783544_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_set_hideFlags_m3010783544_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::set_hideFlags(UnityEngine.HideFlags)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.String UnityEngine.Object::ToString()
extern "C"  String_t* Object_ToString_m842583452 (Object_t2461733472 * __this, const RuntimeMethod* method)
{
	typedef String_t* (*Object_ToString_m842583452_ftn) (Object_t2461733472 *);
	static Object_ToString_m842583452_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_ToString_m842583452_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::ToString()");
	String_t* retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Int32 UnityEngine.Object::GetHashCode()
extern "C"  int32_t Object_GetHashCode_m3084030858 (Object_t2461733472 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = Object_GetHashCode_m1725373185(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Boolean UnityEngine.Object::Equals(System.Object)
extern "C"  bool Object_Equals_m4058157391 (Object_t2461733472 * __this, RuntimeObject * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_Equals_m4058157391_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Object_t2461733472 * V_0 = NULL;
	bool V_1 = false;
	{
		RuntimeObject * L_0 = ___other0;
		V_0 = ((Object_t2461733472 *)IsInstClass((RuntimeObject*)L_0, Object_t2461733472_il2cpp_TypeInfo_var));
		Object_t2461733472 * L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t2461733472_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Equality_m2743582905(NULL /*static, unused*/, L_1, (Object_t2461733472 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002c;
		}
	}
	{
		RuntimeObject * L_3 = ___other0;
		if (!L_3)
		{
			goto IL_002c;
		}
	}
	{
		RuntimeObject * L_4 = ___other0;
		if (((Object_t2461733472 *)IsInstClass((RuntimeObject*)L_4, Object_t2461733472_il2cpp_TypeInfo_var)))
		{
			goto IL_002c;
		}
	}
	{
		V_1 = (bool)0;
		goto IL_0039;
	}

IL_002c:
	{
		Object_t2461733472 * L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t2461733472_il2cpp_TypeInfo_var);
		bool L_6 = Object_CompareBaseObjects_m3289384931(NULL /*static, unused*/, __this, L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		goto IL_0039;
	}

IL_0039:
	{
		bool L_7 = V_1;
		return L_7;
	}
}
// System.Boolean UnityEngine.Object::op_Implicit(UnityEngine.Object)
extern "C"  bool Object_op_Implicit_m3975891434 (RuntimeObject * __this /* static, unused */, Object_t2461733472 * ___exists0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_op_Implicit_m3975891434_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		Object_t2461733472 * L_0 = ___exists0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t2461733472_il2cpp_TypeInfo_var);
		bool L_1 = Object_CompareBaseObjects_m3289384931(NULL /*static, unused*/, L_0, (Object_t2461733472 *)NULL, /*hidden argument*/NULL);
		V_0 = (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
		goto IL_0011;
	}

IL_0011:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Boolean UnityEngine.Object::CompareBaseObjects(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_CompareBaseObjects_m3289384931 (RuntimeObject * __this /* static, unused */, Object_t2461733472 * ___lhs0, Object_t2461733472 * ___rhs1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_CompareBaseObjects_m3289384931_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	bool V_1 = false;
	bool V_2 = false;
	{
		Object_t2461733472 * L_0 = ___lhs0;
		V_0 = (bool)((((RuntimeObject*)(Object_t2461733472 *)L_0) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0);
		Object_t2461733472 * L_1 = ___rhs1;
		V_1 = (bool)((((RuntimeObject*)(Object_t2461733472 *)L_1) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0);
		bool L_2 = V_1;
		if (!L_2)
		{
			goto IL_001e;
		}
	}
	{
		bool L_3 = V_0;
		if (!L_3)
		{
			goto IL_001e;
		}
	}
	{
		V_2 = (bool)1;
		goto IL_0055;
	}

IL_001e:
	{
		bool L_4 = V_1;
		if (!L_4)
		{
			goto IL_0033;
		}
	}
	{
		Object_t2461733472 * L_5 = ___lhs0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t2461733472_il2cpp_TypeInfo_var);
		bool L_6 = Object_IsNativeObjectAlive_m2916611270(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		V_2 = (bool)((((int32_t)L_6) == ((int32_t)0))? 1 : 0);
		goto IL_0055;
	}

IL_0033:
	{
		bool L_7 = V_0;
		if (!L_7)
		{
			goto IL_0048;
		}
	}
	{
		Object_t2461733472 * L_8 = ___rhs1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t2461733472_il2cpp_TypeInfo_var);
		bool L_9 = Object_IsNativeObjectAlive_m2916611270(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		V_2 = (bool)((((int32_t)L_9) == ((int32_t)0))? 1 : 0);
		goto IL_0055;
	}

IL_0048:
	{
		Object_t2461733472 * L_10 = ___lhs0;
		Object_t2461733472 * L_11 = ___rhs1;
		bool L_12 = Object_ReferenceEquals_m1923645570(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		V_2 = L_12;
		goto IL_0055;
	}

IL_0055:
	{
		bool L_13 = V_2;
		return L_13;
	}
}
// System.Boolean UnityEngine.Object::IsNativeObjectAlive(UnityEngine.Object)
extern "C"  bool Object_IsNativeObjectAlive_m2916611270 (RuntimeObject * __this /* static, unused */, Object_t2461733472 * ___o0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_IsNativeObjectAlive_m2916611270_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		Object_t2461733472 * L_0 = ___o0;
		NullCheck(L_0);
		intptr_t L_1 = Object_GetCachedPtr_m2018497938(L_0, /*hidden argument*/NULL);
		bool L_2 = IntPtr_op_Inequality_m1374147269(NULL /*static, unused*/, L_1, (intptr_t)(0), /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0017;
	}

IL_0017:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
// System.IntPtr UnityEngine.Object::GetCachedPtr()
extern "C"  intptr_t Object_GetCachedPtr_m2018497938 (Object_t2461733472 * __this, const RuntimeMethod* method)
{
	intptr_t V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		intptr_t L_0 = __this->get_m_CachedPtr_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		intptr_t L_1 = V_0;
		return L_1;
	}
}
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Equality_m2743582905 (RuntimeObject * __this /* static, unused */, Object_t2461733472 * ___x0, Object_t2461733472 * ___y1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_op_Equality_m2743582905_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		Object_t2461733472 * L_0 = ___x0;
		Object_t2461733472 * L_1 = ___y1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t2461733472_il2cpp_TypeInfo_var);
		bool L_2 = Object_CompareBaseObjects_m3289384931(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_000e;
	}

IL_000e:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Inequality_m3819358377 (RuntimeObject * __this /* static, unused */, Object_t2461733472 * ___x0, Object_t2461733472 * ___y1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_op_Inequality_m3819358377_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		Object_t2461733472 * L_0 = ___x0;
		Object_t2461733472 * L_1 = ___y1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t2461733472_il2cpp_TypeInfo_var);
		bool L_2 = Object_CompareBaseObjects_m3289384931(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = (bool)((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
		goto IL_0011;
	}

IL_0011:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
// System.Void UnityEngine.Object::.cctor()
extern "C"  void Object__cctor_m3598847078 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object__cctor_m3598847078_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((Object_t2461733472_StaticFields*)il2cpp_codegen_static_fields_for(Object_t2461733472_il2cpp_TypeInfo_var))->set_OffsetOfInstanceIDInCPlusPlusObject_1((-1));
		return;
	}
}
// System.Void UnityEngine.Playables.Playable::.ctor(UnityEngine.Playables.PlayableHandle)
extern "C"  void Playable__ctor_m3942516336 (Playable_t1760619938 * __this, PlayableHandle_t3938927722  ___handle0, const RuntimeMethod* method)
{
	{
		PlayableHandle_t3938927722  L_0 = ___handle0;
		__this->set_m_Handle_0(L_0);
		return;
	}
}
extern "C"  void Playable__ctor_m3942516336_AdjustorThunk (RuntimeObject * __this, PlayableHandle_t3938927722  ___handle0, const RuntimeMethod* method)
{
	Playable_t1760619938 * _thisAdjusted = reinterpret_cast<Playable_t1760619938 *>(__this + 1);
	Playable__ctor_m3942516336(_thisAdjusted, ___handle0, method);
}
// UnityEngine.Playables.Playable UnityEngine.Playables.Playable::get_Null()
extern "C"  Playable_t1760619938  Playable_get_Null_m1034340665 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Playable_get_Null_m1034340665_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Playable_t1760619938  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Playable_t1760619938_il2cpp_TypeInfo_var);
		Playable_t1760619938  L_0 = ((Playable_t1760619938_StaticFields*)il2cpp_codegen_static_fields_for(Playable_t1760619938_il2cpp_TypeInfo_var))->get_m_NullPlayable_1();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		Playable_t1760619938  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Playables.PlayableHandle UnityEngine.Playables.Playable::GetHandle()
extern "C"  PlayableHandle_t3938927722  Playable_GetHandle_m2973432975 (Playable_t1760619938 * __this, const RuntimeMethod* method)
{
	PlayableHandle_t3938927722  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		PlayableHandle_t3938927722  L_0 = __this->get_m_Handle_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		PlayableHandle_t3938927722  L_1 = V_0;
		return L_1;
	}
}
extern "C"  PlayableHandle_t3938927722  Playable_GetHandle_m2973432975_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Playable_t1760619938 * _thisAdjusted = reinterpret_cast<Playable_t1760619938 *>(__this + 1);
	return Playable_GetHandle_m2973432975(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Playables.Playable::Equals(UnityEngine.Playables.Playable)
extern "C"  bool Playable_Equals_m1196118830 (Playable_t1760619938 * __this, Playable_t1760619938  ___other0, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		PlayableHandle_t3938927722  L_0 = Playable_GetHandle_m2973432975(__this, /*hidden argument*/NULL);
		PlayableHandle_t3938927722  L_1 = Playable_GetHandle_m2973432975((&___other0), /*hidden argument*/NULL);
		bool L_2 = PlayableHandle_op_Equality_m2397680455(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0019;
	}

IL_0019:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
extern "C"  bool Playable_Equals_m1196118830_AdjustorThunk (RuntimeObject * __this, Playable_t1760619938  ___other0, const RuntimeMethod* method)
{
	Playable_t1760619938 * _thisAdjusted = reinterpret_cast<Playable_t1760619938 *>(__this + 1);
	return Playable_Equals_m1196118830(_thisAdjusted, ___other0, method);
}
// System.Void UnityEngine.Playables.Playable::.cctor()
extern "C"  void Playable__cctor_m2479433417 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Playable__cctor_m2479433417_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		PlayableHandle_t3938927722  L_0 = PlayableHandle_get_Null_m2516265857(NULL /*static, unused*/, /*hidden argument*/NULL);
		Playable_t1760619938  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Playable__ctor_m3942516336((&L_1), L_0, /*hidden argument*/NULL);
		((Playable_t1760619938_StaticFields*)il2cpp_codegen_static_fields_for(Playable_t1760619938_il2cpp_TypeInfo_var))->set_m_NullPlayable_1(L_1);
		return;
	}
}
// System.Void UnityEngine.Playables.PlayableAsset::.ctor()
extern "C"  void PlayableAsset__ctor_m1918338898 (PlayableAsset_t2436284446 * __this, const RuntimeMethod* method)
{
	{
		ScriptableObject__ctor_m3589061004(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Double UnityEngine.Playables.PlayableAsset::get_duration()
extern "C"  double PlayableAsset_get_duration_m2883676437 (PlayableAsset_t2436284446 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayableAsset_get_duration_m2883676437_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	double V_0 = 0.0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayableBinding_t136667248_il2cpp_TypeInfo_var);
		double L_0 = ((PlayableBinding_t136667248_StaticFields*)il2cpp_codegen_static_fields_for(PlayableBinding_t136667248_il2cpp_TypeInfo_var))->get_DefaultDuration_1();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		double L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Playables.PlayableAsset::Internal_CreatePlayable(UnityEngine.Playables.PlayableAsset,UnityEngine.Playables.PlayableGraph,UnityEngine.GameObject,System.IntPtr)
extern "C"  void PlayableAsset_Internal_CreatePlayable_m4149673157 (RuntimeObject * __this /* static, unused */, PlayableAsset_t2436284446 * ___asset0, PlayableGraph_t1897216728  ___graph1, GameObject_t3732643618 * ___go2, intptr_t ___ptr3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayableAsset_Internal_CreatePlayable_m4149673157_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Playable_t1760619938  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Playable_t1760619938 * V_1 = NULL;
	{
		PlayableAsset_t2436284446 * L_0 = ___asset0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t2461733472_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m2743582905(NULL /*static, unused*/, L_0, (Object_t2461733472 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Playable_t1760619938_il2cpp_TypeInfo_var);
		Playable_t1760619938  L_2 = Playable_get_Null_m1034340665(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0021;
	}

IL_0018:
	{
		PlayableAsset_t2436284446 * L_3 = ___asset0;
		PlayableGraph_t1897216728  L_4 = ___graph1;
		GameObject_t3732643618 * L_5 = ___go2;
		NullCheck(L_3);
		Playable_t1760619938  L_6 = VirtFuncInvoker2< Playable_t1760619938 , PlayableGraph_t1897216728 , GameObject_t3732643618 * >::Invoke(4 /* UnityEngine.Playables.Playable UnityEngine.Playables.PlayableAsset::CreatePlayable(UnityEngine.Playables.PlayableGraph,UnityEngine.GameObject) */, L_3, L_4, L_5);
		V_0 = L_6;
	}

IL_0021:
	{
		void* L_7 = IntPtr_ToPointer_m565950407((&___ptr3), /*hidden argument*/NULL);
		V_1 = (Playable_t1760619938 *)L_7;
		Playable_t1760619938 * L_8 = V_1;
		Playable_t1760619938  L_9 = V_0;
		*(Playable_t1760619938 *)L_8 = L_9;
		return;
	}
}
// System.Void UnityEngine.Playables.PlayableAsset::Internal_GetPlayableAssetDuration(UnityEngine.Playables.PlayableAsset,System.IntPtr)
extern "C"  void PlayableAsset_Internal_GetPlayableAssetDuration_m3126328874 (RuntimeObject * __this /* static, unused */, PlayableAsset_t2436284446 * ___asset0, intptr_t ___ptrToDouble1, const RuntimeMethod* method)
{
	double V_0 = 0.0;
	double* V_1 = NULL;
	{
		PlayableAsset_t2436284446 * L_0 = ___asset0;
		NullCheck(L_0);
		double L_1 = VirtFuncInvoker0< double >::Invoke(5 /* System.Double UnityEngine.Playables.PlayableAsset::get_duration() */, L_0);
		V_0 = L_1;
		void* L_2 = IntPtr_ToPointer_m565950407((&___ptrToDouble1), /*hidden argument*/NULL);
		V_1 = (double*)L_2;
		double* L_3 = V_1;
		double L_4 = V_0;
		*((double*)(L_3)) = (double)L_4;
		return;
	}
}
// System.Void UnityEngine.Playables.PlayableBehaviour::.ctor()
extern "C"  void PlayableBehaviour__ctor_m1062833712 (PlayableBehaviour_t4011616635 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m3446193457(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object UnityEngine.Playables.PlayableBehaviour::Clone()
extern "C"  RuntimeObject * PlayableBehaviour_Clone_m1846106316 (PlayableBehaviour_t4011616635 * __this, const RuntimeMethod* method)
{
	RuntimeObject * V_0 = NULL;
	{
		RuntimeObject * L_0 = Object_MemberwiseClone_m1134924534(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		RuntimeObject * L_1 = V_0;
		return L_1;
	}
}


// Conversion methods for marshalling of: UnityEngine.Playables.PlayableBinding
extern "C" void PlayableBinding_t136667248_marshal_pinvoke(const PlayableBinding_t136667248& unmarshaled, PlayableBinding_t136667248_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___U3CsourceBindingTypeU3Ek__BackingField_5Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field '<sourceBindingType>k__BackingField' of type 'PlayableBinding': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___U3CsourceBindingTypeU3Ek__BackingField_5Exception);
}
extern "C" void PlayableBinding_t136667248_marshal_pinvoke_back(const PlayableBinding_t136667248_marshaled_pinvoke& marshaled, PlayableBinding_t136667248& unmarshaled)
{
	Il2CppCodeGenException* ___U3CsourceBindingTypeU3Ek__BackingField_5Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field '<sourceBindingType>k__BackingField' of type 'PlayableBinding': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___U3CsourceBindingTypeU3Ek__BackingField_5Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.Playables.PlayableBinding
extern "C" void PlayableBinding_t136667248_marshal_pinvoke_cleanup(PlayableBinding_t136667248_marshaled_pinvoke& marshaled)
{
}


// Conversion methods for marshalling of: UnityEngine.Playables.PlayableBinding
extern "C" void PlayableBinding_t136667248_marshal_com(const PlayableBinding_t136667248& unmarshaled, PlayableBinding_t136667248_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___U3CsourceBindingTypeU3Ek__BackingField_5Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field '<sourceBindingType>k__BackingField' of type 'PlayableBinding': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___U3CsourceBindingTypeU3Ek__BackingField_5Exception);
}
extern "C" void PlayableBinding_t136667248_marshal_com_back(const PlayableBinding_t136667248_marshaled_com& marshaled, PlayableBinding_t136667248& unmarshaled)
{
	Il2CppCodeGenException* ___U3CsourceBindingTypeU3Ek__BackingField_5Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field '<sourceBindingType>k__BackingField' of type 'PlayableBinding': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___U3CsourceBindingTypeU3Ek__BackingField_5Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.Playables.PlayableBinding
extern "C" void PlayableBinding_t136667248_marshal_com_cleanup(PlayableBinding_t136667248_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.Playables.PlayableBinding::.cctor()
extern "C"  void PlayableBinding__cctor_m1831276805 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayableBinding__cctor_m1831276805_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((PlayableBinding_t136667248_StaticFields*)il2cpp_codegen_static_fields_for(PlayableBinding_t136667248_il2cpp_TypeInfo_var))->set_None_0(((PlayableBindingU5BU5D_t207642577*)SZArrayNew(PlayableBindingU5BU5D_t207642577_il2cpp_TypeInfo_var, (uint32_t)0)));
		((PlayableBinding_t136667248_StaticFields*)il2cpp_codegen_static_fields_for(PlayableBinding_t136667248_il2cpp_TypeInfo_var))->set_DefaultDuration_1((std::numeric_limits<double>::infinity()));
		return;
	}
}
// UnityEngine.Playables.PlayableHandle UnityEngine.Playables.PlayableHandle::get_Null()
extern "C"  PlayableHandle_t3938927722  PlayableHandle_get_Null_m2516265857 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayableHandle_get_Null_m2516265857_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	PlayableHandle_t3938927722  V_0;
	memset(&V_0, 0, sizeof(V_0));
	PlayableHandle_t3938927722  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Initobj (PlayableHandle_t3938927722_il2cpp_TypeInfo_var, (&V_0));
		(&V_0)->set_m_Version_1(((int32_t)10));
		PlayableHandle_t3938927722  L_0 = V_0;
		V_1 = L_0;
		goto IL_0019;
	}

IL_0019:
	{
		PlayableHandle_t3938927722  L_1 = V_1;
		return L_1;
	}
}
// System.Boolean UnityEngine.Playables.PlayableHandle::op_Equality(UnityEngine.Playables.PlayableHandle,UnityEngine.Playables.PlayableHandle)
extern "C"  bool PlayableHandle_op_Equality_m2397680455 (RuntimeObject * __this /* static, unused */, PlayableHandle_t3938927722  ___x0, PlayableHandle_t3938927722  ___y1, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		PlayableHandle_t3938927722  L_0 = ___x0;
		PlayableHandle_t3938927722  L_1 = ___y1;
		bool L_2 = PlayableHandle_CompareVersion_m2720829020(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_000e;
	}

IL_000e:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
// System.Boolean UnityEngine.Playables.PlayableHandle::Equals(System.Object)
extern "C"  bool PlayableHandle_Equals_m771590314 (PlayableHandle_t3938927722 * __this, RuntimeObject * ___p0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayableHandle_Equals_m771590314_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		RuntimeObject * L_0 = ___p0;
		if (((RuntimeObject *)IsInstSealed((RuntimeObject*)L_0, PlayableHandle_t3938927722_il2cpp_TypeInfo_var)))
		{
			goto IL_0013;
		}
	}
	{
		V_0 = (bool)0;
		goto IL_002a;
	}

IL_0013:
	{
		RuntimeObject * L_1 = ___p0;
		bool L_2 = PlayableHandle_CompareVersion_m2720829020(NULL /*static, unused*/, (*(PlayableHandle_t3938927722 *)__this), ((*(PlayableHandle_t3938927722 *)((PlayableHandle_t3938927722 *)UnBox(L_1, PlayableHandle_t3938927722_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_002a;
	}

IL_002a:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
extern "C"  bool PlayableHandle_Equals_m771590314_AdjustorThunk (RuntimeObject * __this, RuntimeObject * ___p0, const RuntimeMethod* method)
{
	PlayableHandle_t3938927722 * _thisAdjusted = reinterpret_cast<PlayableHandle_t3938927722 *>(__this + 1);
	return PlayableHandle_Equals_m771590314(_thisAdjusted, ___p0, method);
}
// System.Int32 UnityEngine.Playables.PlayableHandle::GetHashCode()
extern "C"  int32_t PlayableHandle_GetHashCode_m1922874376 (PlayableHandle_t3938927722 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		intptr_t* L_0 = __this->get_address_of_m_Handle_0();
		int32_t L_1 = IntPtr_GetHashCode_m2382536356(L_0, /*hidden argument*/NULL);
		int32_t* L_2 = __this->get_address_of_m_Version_1();
		int32_t L_3 = Int32_GetHashCode_m339244912(L_2, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)L_1^(int32_t)L_3));
		goto IL_002a;
	}

IL_002a:
	{
		int32_t L_4 = V_0;
		return L_4;
	}
}
extern "C"  int32_t PlayableHandle_GetHashCode_m1922874376_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	PlayableHandle_t3938927722 * _thisAdjusted = reinterpret_cast<PlayableHandle_t3938927722 *>(__this + 1);
	return PlayableHandle_GetHashCode_m1922874376(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Playables.PlayableHandle::CompareVersion(UnityEngine.Playables.PlayableHandle,UnityEngine.Playables.PlayableHandle)
extern "C"  bool PlayableHandle_CompareVersion_m2720829020 (RuntimeObject * __this /* static, unused */, PlayableHandle_t3938927722  ___lhs0, PlayableHandle_t3938927722  ___rhs1, const RuntimeMethod* method)
{
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		intptr_t L_0 = (&___lhs0)->get_m_Handle_0();
		intptr_t L_1 = (&___rhs1)->get_m_Handle_0();
		bool L_2 = IntPtr_op_Equality_m3978210203(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002b;
		}
	}
	{
		int32_t L_3 = (&___lhs0)->get_m_Version_1();
		int32_t L_4 = (&___rhs1)->get_m_Version_1();
		G_B3_0 = ((((int32_t)L_3) == ((int32_t)L_4))? 1 : 0);
		goto IL_002c;
	}

IL_002b:
	{
		G_B3_0 = 0;
	}

IL_002c:
	{
		V_0 = (bool)G_B3_0;
		goto IL_0032;
	}

IL_0032:
	{
		bool L_5 = V_0;
		return L_5;
	}
}
// System.Void UnityEngine.Playables.PlayableOutput::.ctor(UnityEngine.Playables.PlayableOutputHandle)
extern "C"  void PlayableOutput__ctor_m451176029 (PlayableOutput_t3840687608 * __this, PlayableOutputHandle_t2719521870  ___handle0, const RuntimeMethod* method)
{
	{
		PlayableOutputHandle_t2719521870  L_0 = ___handle0;
		__this->set_m_Handle_0(L_0);
		return;
	}
}
extern "C"  void PlayableOutput__ctor_m451176029_AdjustorThunk (RuntimeObject * __this, PlayableOutputHandle_t2719521870  ___handle0, const RuntimeMethod* method)
{
	PlayableOutput_t3840687608 * _thisAdjusted = reinterpret_cast<PlayableOutput_t3840687608 *>(__this + 1);
	PlayableOutput__ctor_m451176029(_thisAdjusted, ___handle0, method);
}
// UnityEngine.Playables.PlayableOutputHandle UnityEngine.Playables.PlayableOutput::GetHandle()
extern "C"  PlayableOutputHandle_t2719521870  PlayableOutput_GetHandle_m1350475571 (PlayableOutput_t3840687608 * __this, const RuntimeMethod* method)
{
	PlayableOutputHandle_t2719521870  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		PlayableOutputHandle_t2719521870  L_0 = __this->get_m_Handle_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		PlayableOutputHandle_t2719521870  L_1 = V_0;
		return L_1;
	}
}
extern "C"  PlayableOutputHandle_t2719521870  PlayableOutput_GetHandle_m1350475571_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	PlayableOutput_t3840687608 * _thisAdjusted = reinterpret_cast<PlayableOutput_t3840687608 *>(__this + 1);
	return PlayableOutput_GetHandle_m1350475571(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Playables.PlayableOutput::Equals(UnityEngine.Playables.PlayableOutput)
extern "C"  bool PlayableOutput_Equals_m2076999710 (PlayableOutput_t3840687608 * __this, PlayableOutput_t3840687608  ___other0, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		PlayableOutputHandle_t2719521870  L_0 = PlayableOutput_GetHandle_m1350475571(__this, /*hidden argument*/NULL);
		PlayableOutputHandle_t2719521870  L_1 = PlayableOutput_GetHandle_m1350475571((&___other0), /*hidden argument*/NULL);
		bool L_2 = PlayableOutputHandle_op_Equality_m1435240927(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0019;
	}

IL_0019:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
extern "C"  bool PlayableOutput_Equals_m2076999710_AdjustorThunk (RuntimeObject * __this, PlayableOutput_t3840687608  ___other0, const RuntimeMethod* method)
{
	PlayableOutput_t3840687608 * _thisAdjusted = reinterpret_cast<PlayableOutput_t3840687608 *>(__this + 1);
	return PlayableOutput_Equals_m2076999710(_thisAdjusted, ___other0, method);
}
// System.Void UnityEngine.Playables.PlayableOutput::.cctor()
extern "C"  void PlayableOutput__cctor_m43115862 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayableOutput__cctor_m43115862_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		PlayableOutputHandle_t2719521870  L_0 = PlayableOutputHandle_get_Null_m3044568124(NULL /*static, unused*/, /*hidden argument*/NULL);
		PlayableOutput_t3840687608  L_1;
		memset(&L_1, 0, sizeof(L_1));
		PlayableOutput__ctor_m451176029((&L_1), L_0, /*hidden argument*/NULL);
		((PlayableOutput_t3840687608_StaticFields*)il2cpp_codegen_static_fields_for(PlayableOutput_t3840687608_il2cpp_TypeInfo_var))->set_m_NullPlayableOutput_1(L_1);
		return;
	}
}
// UnityEngine.Playables.PlayableOutputHandle UnityEngine.Playables.PlayableOutputHandle::get_Null()
extern "C"  PlayableOutputHandle_t2719521870  PlayableOutputHandle_get_Null_m3044568124 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayableOutputHandle_get_Null_m3044568124_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	PlayableOutputHandle_t2719521870  V_0;
	memset(&V_0, 0, sizeof(V_0));
	PlayableOutputHandle_t2719521870  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Initobj (PlayableOutputHandle_t2719521870_il2cpp_TypeInfo_var, (&V_0));
		(&V_0)->set_m_Version_1(((int32_t)2147483647LL));
		PlayableOutputHandle_t2719521870  L_0 = V_0;
		V_1 = L_0;
		goto IL_001c;
	}

IL_001c:
	{
		PlayableOutputHandle_t2719521870  L_1 = V_1;
		return L_1;
	}
}
// System.Int32 UnityEngine.Playables.PlayableOutputHandle::GetHashCode()
extern "C"  int32_t PlayableOutputHandle_GetHashCode_m4127769214 (PlayableOutputHandle_t2719521870 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		intptr_t* L_0 = __this->get_address_of_m_Handle_0();
		int32_t L_1 = IntPtr_GetHashCode_m2382536356(L_0, /*hidden argument*/NULL);
		int32_t* L_2 = __this->get_address_of_m_Version_1();
		int32_t L_3 = Int32_GetHashCode_m339244912(L_2, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)L_1^(int32_t)L_3));
		goto IL_002a;
	}

IL_002a:
	{
		int32_t L_4 = V_0;
		return L_4;
	}
}
extern "C"  int32_t PlayableOutputHandle_GetHashCode_m4127769214_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	PlayableOutputHandle_t2719521870 * _thisAdjusted = reinterpret_cast<PlayableOutputHandle_t2719521870 *>(__this + 1);
	return PlayableOutputHandle_GetHashCode_m4127769214(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Playables.PlayableOutputHandle::op_Equality(UnityEngine.Playables.PlayableOutputHandle,UnityEngine.Playables.PlayableOutputHandle)
extern "C"  bool PlayableOutputHandle_op_Equality_m1435240927 (RuntimeObject * __this /* static, unused */, PlayableOutputHandle_t2719521870  ___lhs0, PlayableOutputHandle_t2719521870  ___rhs1, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		PlayableOutputHandle_t2719521870  L_0 = ___lhs0;
		PlayableOutputHandle_t2719521870  L_1 = ___rhs1;
		bool L_2 = PlayableOutputHandle_CompareVersion_m3459929500(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_000e;
	}

IL_000e:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
// System.Boolean UnityEngine.Playables.PlayableOutputHandle::Equals(System.Object)
extern "C"  bool PlayableOutputHandle_Equals_m669007655 (PlayableOutputHandle_t2719521870 * __this, RuntimeObject * ___p0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayableOutputHandle_Equals_m669007655_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		RuntimeObject * L_0 = ___p0;
		if (!((RuntimeObject *)IsInstSealed((RuntimeObject*)L_0, PlayableOutputHandle_t2719521870_il2cpp_TypeInfo_var)))
		{
			goto IL_001f;
		}
	}
	{
		RuntimeObject * L_1 = ___p0;
		bool L_2 = PlayableOutputHandle_CompareVersion_m3459929500(NULL /*static, unused*/, (*(PlayableOutputHandle_t2719521870 *)__this), ((*(PlayableOutputHandle_t2719521870 *)((PlayableOutputHandle_t2719521870 *)UnBox(L_1, PlayableOutputHandle_t2719521870_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_2));
		goto IL_0020;
	}

IL_001f:
	{
		G_B3_0 = 0;
	}

IL_0020:
	{
		V_0 = (bool)G_B3_0;
		goto IL_0026;
	}

IL_0026:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
extern "C"  bool PlayableOutputHandle_Equals_m669007655_AdjustorThunk (RuntimeObject * __this, RuntimeObject * ___p0, const RuntimeMethod* method)
{
	PlayableOutputHandle_t2719521870 * _thisAdjusted = reinterpret_cast<PlayableOutputHandle_t2719521870 *>(__this + 1);
	return PlayableOutputHandle_Equals_m669007655(_thisAdjusted, ___p0, method);
}
// System.Boolean UnityEngine.Playables.PlayableOutputHandle::CompareVersion(UnityEngine.Playables.PlayableOutputHandle,UnityEngine.Playables.PlayableOutputHandle)
extern "C"  bool PlayableOutputHandle_CompareVersion_m3459929500 (RuntimeObject * __this /* static, unused */, PlayableOutputHandle_t2719521870  ___lhs0, PlayableOutputHandle_t2719521870  ___rhs1, const RuntimeMethod* method)
{
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		intptr_t L_0 = (&___lhs0)->get_m_Handle_0();
		intptr_t L_1 = (&___rhs1)->get_m_Handle_0();
		bool L_2 = IntPtr_op_Equality_m3978210203(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002b;
		}
	}
	{
		int32_t L_3 = (&___lhs0)->get_m_Version_1();
		int32_t L_4 = (&___rhs1)->get_m_Version_1();
		G_B3_0 = ((((int32_t)L_3) == ((int32_t)L_4))? 1 : 0);
		goto IL_002c;
	}

IL_002b:
	{
		G_B3_0 = 0;
	}

IL_002c:
	{
		V_0 = (bool)G_B3_0;
		goto IL_0032;
	}

IL_0032:
	{
		bool L_5 = V_0;
		return L_5;
	}
}
// System.Void UnityEngine.PropertyName::.ctor(System.String)
extern "C"  void PropertyName__ctor_m2196050290 (PropertyName_t666104458 * __this, String_t* ___name0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___name0;
		PropertyName_t666104458  L_1 = PropertyNameUtils_PropertyNameFromString_m3539940257(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		PropertyName__ctor_m1851822729(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void PropertyName__ctor_m2196050290_AdjustorThunk (RuntimeObject * __this, String_t* ___name0, const RuntimeMethod* method)
{
	PropertyName_t666104458 * _thisAdjusted = reinterpret_cast<PropertyName_t666104458 *>(__this + 1);
	PropertyName__ctor_m2196050290(_thisAdjusted, ___name0, method);
}
// System.Void UnityEngine.PropertyName::.ctor(UnityEngine.PropertyName)
extern "C"  void PropertyName__ctor_m1851822729 (PropertyName_t666104458 * __this, PropertyName_t666104458  ___other0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (&___other0)->get_id_0();
		__this->set_id_0(L_0);
		return;
	}
}
extern "C"  void PropertyName__ctor_m1851822729_AdjustorThunk (RuntimeObject * __this, PropertyName_t666104458  ___other0, const RuntimeMethod* method)
{
	PropertyName_t666104458 * _thisAdjusted = reinterpret_cast<PropertyName_t666104458 *>(__this + 1);
	PropertyName__ctor_m1851822729(_thisAdjusted, ___other0, method);
}
// System.Void UnityEngine.PropertyName::.ctor(System.Int32)
extern "C"  void PropertyName__ctor_m4077360820 (PropertyName_t666104458 * __this, int32_t ___id0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___id0;
		__this->set_id_0(L_0);
		return;
	}
}
extern "C"  void PropertyName__ctor_m4077360820_AdjustorThunk (RuntimeObject * __this, int32_t ___id0, const RuntimeMethod* method)
{
	PropertyName_t666104458 * _thisAdjusted = reinterpret_cast<PropertyName_t666104458 *>(__this + 1);
	PropertyName__ctor_m4077360820(_thisAdjusted, ___id0, method);
}
// System.Boolean UnityEngine.PropertyName::op_Equality(UnityEngine.PropertyName,UnityEngine.PropertyName)
extern "C"  bool PropertyName_op_Equality_m2654803850 (RuntimeObject * __this /* static, unused */, PropertyName_t666104458  ___lhs0, PropertyName_t666104458  ___rhs1, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		int32_t L_0 = (&___lhs0)->get_id_0();
		int32_t L_1 = (&___rhs1)->get_id_0();
		V_0 = (bool)((((int32_t)L_0) == ((int32_t)L_1))? 1 : 0);
		goto IL_0017;
	}

IL_0017:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Boolean UnityEngine.PropertyName::op_Inequality(UnityEngine.PropertyName,UnityEngine.PropertyName)
extern "C"  bool PropertyName_op_Inequality_m3678544633 (RuntimeObject * __this /* static, unused */, PropertyName_t666104458  ___lhs0, PropertyName_t666104458  ___rhs1, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		int32_t L_0 = (&___lhs0)->get_id_0();
		int32_t L_1 = (&___rhs1)->get_id_0();
		V_0 = (bool)((((int32_t)((((int32_t)L_0) == ((int32_t)L_1))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_001a;
	}

IL_001a:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Int32 UnityEngine.PropertyName::GetHashCode()
extern "C"  int32_t PropertyName_GetHashCode_m1226794333 (PropertyName_t666104458 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_id_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
extern "C"  int32_t PropertyName_GetHashCode_m1226794333_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	PropertyName_t666104458 * _thisAdjusted = reinterpret_cast<PropertyName_t666104458 *>(__this + 1);
	return PropertyName_GetHashCode_m1226794333(_thisAdjusted, method);
}
// System.Boolean UnityEngine.PropertyName::Equals(System.Object)
extern "C"  bool PropertyName_Equals_m3895751744 (PropertyName_t666104458 * __this, RuntimeObject * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PropertyName_Equals_m3895751744_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		RuntimeObject * L_0 = ___other0;
		if (!((RuntimeObject *)IsInstSealed((RuntimeObject*)L_0, PropertyName_t666104458_il2cpp_TypeInfo_var)))
		{
			goto IL_001f;
		}
	}
	{
		RuntimeObject * L_1 = ___other0;
		bool L_2 = PropertyName_op_Equality_m2654803850(NULL /*static, unused*/, (*(PropertyName_t666104458 *)__this), ((*(PropertyName_t666104458 *)((PropertyName_t666104458 *)UnBox(L_1, PropertyName_t666104458_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_2));
		goto IL_0020;
	}

IL_001f:
	{
		G_B3_0 = 0;
	}

IL_0020:
	{
		V_0 = (bool)G_B3_0;
		goto IL_0026;
	}

IL_0026:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
extern "C"  bool PropertyName_Equals_m3895751744_AdjustorThunk (RuntimeObject * __this, RuntimeObject * ___other0, const RuntimeMethod* method)
{
	PropertyName_t666104458 * _thisAdjusted = reinterpret_cast<PropertyName_t666104458 *>(__this + 1);
	return PropertyName_Equals_m3895751744(_thisAdjusted, ___other0, method);
}
// UnityEngine.PropertyName UnityEngine.PropertyName::op_Implicit(System.String)
extern "C"  PropertyName_t666104458  PropertyName_op_Implicit_m2033544225 (RuntimeObject * __this /* static, unused */, String_t* ___name0, const RuntimeMethod* method)
{
	PropertyName_t666104458  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		String_t* L_0 = ___name0;
		PropertyName_t666104458  L_1;
		memset(&L_1, 0, sizeof(L_1));
		PropertyName__ctor_m2196050290((&L_1), L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000d;
	}

IL_000d:
	{
		PropertyName_t666104458  L_2 = V_0;
		return L_2;
	}
}
// UnityEngine.PropertyName UnityEngine.PropertyName::op_Implicit(System.Int32)
extern "C"  PropertyName_t666104458  PropertyName_op_Implicit_m3783576796 (RuntimeObject * __this /* static, unused */, int32_t ___id0, const RuntimeMethod* method)
{
	PropertyName_t666104458  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___id0;
		PropertyName_t666104458  L_1;
		memset(&L_1, 0, sizeof(L_1));
		PropertyName__ctor_m4077360820((&L_1), L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000d;
	}

IL_000d:
	{
		PropertyName_t666104458  L_2 = V_0;
		return L_2;
	}
}
// System.String UnityEngine.PropertyName::ToString()
extern "C"  String_t* PropertyName_ToString_m1969781279 (PropertyName_t666104458 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PropertyName_ToString_m1969781279_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		int32_t L_0 = __this->get_id_0();
		int32_t L_1 = L_0;
		RuntimeObject * L_2 = Box(Int32_t2571135199_il2cpp_TypeInfo_var, &L_1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Format_m1366448113(NULL /*static, unused*/, _stringLiteral909797061, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_001c;
	}

IL_001c:
	{
		String_t* L_4 = V_0;
		return L_4;
	}
}
extern "C"  String_t* PropertyName_ToString_m1969781279_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	PropertyName_t666104458 * _thisAdjusted = reinterpret_cast<PropertyName_t666104458 *>(__this + 1);
	return PropertyName_ToString_m1969781279(_thisAdjusted, method);
}
// UnityEngine.PropertyName UnityEngine.PropertyNameUtils::PropertyNameFromString(System.String)
extern "C"  PropertyName_t666104458  PropertyNameUtils_PropertyNameFromString_m3539940257 (RuntimeObject * __this /* static, unused */, String_t* ___name0, const RuntimeMethod* method)
{
	PropertyName_t666104458  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		String_t* L_0 = ___name0;
		PropertyNameUtils_PropertyNameFromString_Injected_m3547369790(NULL /*static, unused*/, L_0, (&V_0), /*hidden argument*/NULL);
		PropertyName_t666104458  L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.PropertyNameUtils::PropertyNameFromString_Injected(System.String,UnityEngine.PropertyName&)
extern "C"  void PropertyNameUtils_PropertyNameFromString_Injected_m3547369790 (RuntimeObject * __this /* static, unused */, String_t* ___name0, PropertyName_t666104458 * ___ret1, const RuntimeMethod* method)
{
	typedef void (*PropertyNameUtils_PropertyNameFromString_Injected_m3547369790_ftn) (String_t*, PropertyName_t666104458 *);
	static PropertyNameUtils_PropertyNameFromString_Injected_m3547369790_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PropertyNameUtils_PropertyNameFromString_Injected_m3547369790_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.PropertyNameUtils::PropertyNameFromString_Injected(System.String,UnityEngine.PropertyName&)");
	_il2cpp_icall_func(___name0, ___ret1);
}
// UnityEngine.Vector3 UnityEngine.Ray::get_direction()
extern "C"  Vector3_t3239555143  Ray_get_direction_m1714502355 (Ray_t1113537957 * __this, const RuntimeMethod* method)
{
	Vector3_t3239555143  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector3_t3239555143  L_0 = __this->get_m_Direction_1();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		Vector3_t3239555143  L_1 = V_0;
		return L_1;
	}
}
extern "C"  Vector3_t3239555143  Ray_get_direction_m1714502355_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Ray_t1113537957 * _thisAdjusted = reinterpret_cast<Ray_t1113537957 *>(__this + 1);
	return Ray_get_direction_m1714502355(_thisAdjusted, method);
}
// System.String UnityEngine.Ray::ToString()
extern "C"  String_t* Ray_ToString_m3743478486 (Ray_t1113537957 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Ray_ToString_m3743478486_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		ObjectU5BU5D_t4290686858* L_0 = ((ObjectU5BU5D_t4290686858*)SZArrayNew(ObjectU5BU5D_t4290686858_il2cpp_TypeInfo_var, (uint32_t)2));
		Vector3_t3239555143  L_1 = __this->get_m_Origin_0();
		Vector3_t3239555143  L_2 = L_1;
		RuntimeObject * L_3 = Box(Vector3_t3239555143_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_3);
		ObjectU5BU5D_t4290686858* L_4 = L_0;
		Vector3_t3239555143  L_5 = __this->get_m_Direction_1();
		Vector3_t3239555143  L_6 = L_5;
		RuntimeObject * L_7 = Box(Vector3_t3239555143_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_7);
		String_t* L_8 = UnityString_Format_m2513837565(NULL /*static, unused*/, _stringLiteral1596573497, L_4, /*hidden argument*/NULL);
		V_0 = L_8;
		goto IL_0033;
	}

IL_0033:
	{
		String_t* L_9 = V_0;
		return L_9;
	}
}
extern "C"  String_t* Ray_ToString_m3743478486_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Ray_t1113537957 * _thisAdjusted = reinterpret_cast<Ray_t1113537957 *>(__this + 1);
	return Ray_ToString_m3743478486(_thisAdjusted, method);
}
// System.Single UnityEngine.Rect::get_x()
extern "C"  float Rect_get_x_m4046749491 (Rect_t1111852907 * __this, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_XMin_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		float L_1 = V_0;
		return L_1;
	}
}
extern "C"  float Rect_get_x_m4046749491_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Rect_t1111852907 * _thisAdjusted = reinterpret_cast<Rect_t1111852907 *>(__this + 1);
	return Rect_get_x_m4046749491(_thisAdjusted, method);
}
// System.Single UnityEngine.Rect::get_y()
extern "C"  float Rect_get_y_m1845352024 (Rect_t1111852907 * __this, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_YMin_1();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		float L_1 = V_0;
		return L_1;
	}
}
extern "C"  float Rect_get_y_m1845352024_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Rect_t1111852907 * _thisAdjusted = reinterpret_cast<Rect_t1111852907 *>(__this + 1);
	return Rect_get_y_m1845352024(_thisAdjusted, method);
}
// System.Single UnityEngine.Rect::get_width()
extern "C"  float Rect_get_width_m710188174 (Rect_t1111852907 * __this, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_Width_2();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		float L_1 = V_0;
		return L_1;
	}
}
extern "C"  float Rect_get_width_m710188174_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Rect_t1111852907 * _thisAdjusted = reinterpret_cast<Rect_t1111852907 *>(__this + 1);
	return Rect_get_width_m710188174(_thisAdjusted, method);
}
// System.Single UnityEngine.Rect::get_height()
extern "C"  float Rect_get_height_m262661000 (Rect_t1111852907 * __this, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_Height_3();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		float L_1 = V_0;
		return L_1;
	}
}
extern "C"  float Rect_get_height_m262661000_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Rect_t1111852907 * _thisAdjusted = reinterpret_cast<Rect_t1111852907 *>(__this + 1);
	return Rect_get_height_m262661000(_thisAdjusted, method);
}
// System.Single UnityEngine.Rect::get_xMin()
extern "C"  float Rect_get_xMin_m490539547 (Rect_t1111852907 * __this, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_XMin_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		float L_1 = V_0;
		return L_1;
	}
}
extern "C"  float Rect_get_xMin_m490539547_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Rect_t1111852907 * _thisAdjusted = reinterpret_cast<Rect_t1111852907 *>(__this + 1);
	return Rect_get_xMin_m490539547(_thisAdjusted, method);
}
// System.Single UnityEngine.Rect::get_yMin()
extern "C"  float Rect_get_yMin_m1596955344 (Rect_t1111852907 * __this, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_YMin_1();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		float L_1 = V_0;
		return L_1;
	}
}
extern "C"  float Rect_get_yMin_m1596955344_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Rect_t1111852907 * _thisAdjusted = reinterpret_cast<Rect_t1111852907 *>(__this + 1);
	return Rect_get_yMin_m1596955344(_thisAdjusted, method);
}
// System.Single UnityEngine.Rect::get_xMax()
extern "C"  float Rect_get_xMax_m3140092843 (Rect_t1111852907 * __this, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_Width_2();
		float L_1 = __this->get_m_XMin_0();
		V_0 = ((float)((float)L_0+(float)L_1));
		goto IL_0014;
	}

IL_0014:
	{
		float L_2 = V_0;
		return L_2;
	}
}
extern "C"  float Rect_get_xMax_m3140092843_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Rect_t1111852907 * _thisAdjusted = reinterpret_cast<Rect_t1111852907 *>(__this + 1);
	return Rect_get_xMax_m3140092843(_thisAdjusted, method);
}
// System.Single UnityEngine.Rect::get_yMax()
extern "C"  float Rect_get_yMax_m1499979192 (Rect_t1111852907 * __this, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_Height_3();
		float L_1 = __this->get_m_YMin_1();
		V_0 = ((float)((float)L_0+(float)L_1));
		goto IL_0014;
	}

IL_0014:
	{
		float L_2 = V_0;
		return L_2;
	}
}
extern "C"  float Rect_get_yMax_m1499979192_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Rect_t1111852907 * _thisAdjusted = reinterpret_cast<Rect_t1111852907 *>(__this + 1);
	return Rect_get_yMax_m1499979192(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Rect::Contains(UnityEngine.Vector3)
extern "C"  bool Rect_Contains_m594593135 (Rect_t1111852907 * __this, Vector3_t3239555143  ___point0, const RuntimeMethod* method)
{
	bool V_0 = false;
	int32_t G_B5_0 = 0;
	{
		float L_0 = (&___point0)->get_x_0();
		float L_1 = Rect_get_xMin_m490539547(__this, /*hidden argument*/NULL);
		if ((!(((float)L_0) >= ((float)L_1))))
		{
			goto IL_0048;
		}
	}
	{
		float L_2 = (&___point0)->get_x_0();
		float L_3 = Rect_get_xMax_m3140092843(__this, /*hidden argument*/NULL);
		if ((!(((float)L_2) < ((float)L_3))))
		{
			goto IL_0048;
		}
	}
	{
		float L_4 = (&___point0)->get_y_1();
		float L_5 = Rect_get_yMin_m1596955344(__this, /*hidden argument*/NULL);
		if ((!(((float)L_4) >= ((float)L_5))))
		{
			goto IL_0048;
		}
	}
	{
		float L_6 = (&___point0)->get_y_1();
		float L_7 = Rect_get_yMax_m1499979192(__this, /*hidden argument*/NULL);
		G_B5_0 = ((((float)L_6) < ((float)L_7))? 1 : 0);
		goto IL_0049;
	}

IL_0048:
	{
		G_B5_0 = 0;
	}

IL_0049:
	{
		V_0 = (bool)G_B5_0;
		goto IL_004f;
	}

IL_004f:
	{
		bool L_8 = V_0;
		return L_8;
	}
}
extern "C"  bool Rect_Contains_m594593135_AdjustorThunk (RuntimeObject * __this, Vector3_t3239555143  ___point0, const RuntimeMethod* method)
{
	Rect_t1111852907 * _thisAdjusted = reinterpret_cast<Rect_t1111852907 *>(__this + 1);
	return Rect_Contains_m594593135(_thisAdjusted, ___point0, method);
}
// System.Int32 UnityEngine.Rect::GetHashCode()
extern "C"  int32_t Rect_GetHashCode_m1190625508 (Rect_t1111852907 * __this, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	int32_t V_4 = 0;
	{
		float L_0 = Rect_get_x_m4046749491(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Single_GetHashCode_m3263933369((&V_0), /*hidden argument*/NULL);
		float L_2 = Rect_get_width_m710188174(__this, /*hidden argument*/NULL);
		V_1 = L_2;
		int32_t L_3 = Single_GetHashCode_m3263933369((&V_1), /*hidden argument*/NULL);
		float L_4 = Rect_get_y_m1845352024(__this, /*hidden argument*/NULL);
		V_2 = L_4;
		int32_t L_5 = Single_GetHashCode_m3263933369((&V_2), /*hidden argument*/NULL);
		float L_6 = Rect_get_height_m262661000(__this, /*hidden argument*/NULL);
		V_3 = L_6;
		int32_t L_7 = Single_GetHashCode_m3263933369((&V_3), /*hidden argument*/NULL);
		V_4 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_1^(int32_t)((int32_t)((int32_t)L_3<<(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_5>>(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_7>>(int32_t)1))));
		goto IL_0061;
	}

IL_0061:
	{
		int32_t L_8 = V_4;
		return L_8;
	}
}
extern "C"  int32_t Rect_GetHashCode_m1190625508_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Rect_t1111852907 * _thisAdjusted = reinterpret_cast<Rect_t1111852907 *>(__this + 1);
	return Rect_GetHashCode_m1190625508(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Rect::Equals(System.Object)
extern "C"  bool Rect_Equals_m2317586560 (Rect_t1111852907 * __this, RuntimeObject * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Rect_Equals_m2317586560_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Rect_t1111852907  V_1;
	memset(&V_1, 0, sizeof(V_1));
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	int32_t G_B7_0 = 0;
	{
		RuntimeObject * L_0 = ___other0;
		if (((RuntimeObject *)IsInstSealed((RuntimeObject*)L_0, Rect_t1111852907_il2cpp_TypeInfo_var)))
		{
			goto IL_0013;
		}
	}
	{
		V_0 = (bool)0;
		goto IL_0088;
	}

IL_0013:
	{
		RuntimeObject * L_1 = ___other0;
		V_1 = ((*(Rect_t1111852907 *)((Rect_t1111852907 *)UnBox(L_1, Rect_t1111852907_il2cpp_TypeInfo_var))));
		float L_2 = Rect_get_x_m4046749491(__this, /*hidden argument*/NULL);
		V_2 = L_2;
		float L_3 = Rect_get_x_m4046749491((&V_1), /*hidden argument*/NULL);
		bool L_4 = Single_Equals_m3179646383((&V_2), L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0081;
		}
	}
	{
		float L_5 = Rect_get_y_m1845352024(__this, /*hidden argument*/NULL);
		V_3 = L_5;
		float L_6 = Rect_get_y_m1845352024((&V_1), /*hidden argument*/NULL);
		bool L_7 = Single_Equals_m3179646383((&V_3), L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0081;
		}
	}
	{
		float L_8 = Rect_get_width_m710188174(__this, /*hidden argument*/NULL);
		V_4 = L_8;
		float L_9 = Rect_get_width_m710188174((&V_1), /*hidden argument*/NULL);
		bool L_10 = Single_Equals_m3179646383((&V_4), L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0081;
		}
	}
	{
		float L_11 = Rect_get_height_m262661000(__this, /*hidden argument*/NULL);
		V_5 = L_11;
		float L_12 = Rect_get_height_m262661000((&V_1), /*hidden argument*/NULL);
		bool L_13 = Single_Equals_m3179646383((&V_5), L_12, /*hidden argument*/NULL);
		G_B7_0 = ((int32_t)(L_13));
		goto IL_0082;
	}

IL_0081:
	{
		G_B7_0 = 0;
	}

IL_0082:
	{
		V_0 = (bool)G_B7_0;
		goto IL_0088;
	}

IL_0088:
	{
		bool L_14 = V_0;
		return L_14;
	}
}
extern "C"  bool Rect_Equals_m2317586560_AdjustorThunk (RuntimeObject * __this, RuntimeObject * ___other0, const RuntimeMethod* method)
{
	Rect_t1111852907 * _thisAdjusted = reinterpret_cast<Rect_t1111852907 *>(__this + 1);
	return Rect_Equals_m2317586560(_thisAdjusted, ___other0, method);
}
// System.String UnityEngine.Rect::ToString()
extern "C"  String_t* Rect_ToString_m2557162916 (Rect_t1111852907 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Rect_ToString_m2557162916_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		ObjectU5BU5D_t4290686858* L_0 = ((ObjectU5BU5D_t4290686858*)SZArrayNew(ObjectU5BU5D_t4290686858_il2cpp_TypeInfo_var, (uint32_t)4));
		float L_1 = Rect_get_x_m4046749491(__this, /*hidden argument*/NULL);
		float L_2 = L_1;
		RuntimeObject * L_3 = Box(Single_t496865882_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_3);
		ObjectU5BU5D_t4290686858* L_4 = L_0;
		float L_5 = Rect_get_y_m1845352024(__this, /*hidden argument*/NULL);
		float L_6 = L_5;
		RuntimeObject * L_7 = Box(Single_t496865882_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_7);
		ObjectU5BU5D_t4290686858* L_8 = L_4;
		float L_9 = Rect_get_width_m710188174(__this, /*hidden argument*/NULL);
		float L_10 = L_9;
		RuntimeObject * L_11 = Box(Single_t496865882_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_11);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_11);
		ObjectU5BU5D_t4290686858* L_12 = L_8;
		float L_13 = Rect_get_height_m262661000(__this, /*hidden argument*/NULL);
		float L_14 = L_13;
		RuntimeObject * L_15 = Box(Single_t496865882_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, L_15);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_15);
		String_t* L_16 = UnityString_Format_m2513837565(NULL /*static, unused*/, _stringLiteral4037084165, L_12, /*hidden argument*/NULL);
		V_0 = L_16;
		goto IL_004f;
	}

IL_004f:
	{
		String_t* L_17 = V_0;
		return L_17;
	}
}
extern "C"  String_t* Rect_ToString_m2557162916_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Rect_t1111852907 * _thisAdjusted = reinterpret_cast<Rect_t1111852907 *>(__this + 1);
	return Rect_ToString_m2557162916(_thisAdjusted, method);
}
// System.Void UnityEngine.RectTransform::SendReapplyDrivenProperties(UnityEngine.RectTransform)
extern "C"  void RectTransform_SendReapplyDrivenProperties_m2257690066 (RuntimeObject * __this /* static, unused */, RectTransform_t3950565364 * ___driven0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RectTransform_SendReapplyDrivenProperties_m2257690066_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ReapplyDrivenProperties_t3880910420 * L_0 = ((RectTransform_t3950565364_StaticFields*)il2cpp_codegen_static_fields_for(RectTransform_t3950565364_il2cpp_TypeInfo_var))->get_reapplyDrivenProperties_2();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		ReapplyDrivenProperties_t3880910420 * L_1 = ((RectTransform_t3950565364_StaticFields*)il2cpp_codegen_static_fields_for(RectTransform_t3950565364_il2cpp_TypeInfo_var))->get_reapplyDrivenProperties_2();
		RectTransform_t3950565364 * L_2 = ___driven0;
		NullCheck(L_1);
		ReapplyDrivenProperties_Invoke_m2731383309(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0016:
	{
		return;
	}
}
// System.Void UnityEngine.RectTransform/ReapplyDrivenProperties::.ctor(System.Object,System.IntPtr)
extern "C"  void ReapplyDrivenProperties__ctor_m4026315179 (ReapplyDrivenProperties_t3880910420 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.RectTransform/ReapplyDrivenProperties::Invoke(UnityEngine.RectTransform)
extern "C"  void ReapplyDrivenProperties_Invoke_m2731383309 (ReapplyDrivenProperties_t3880910420 * __this, RectTransform_t3950565364 * ___driven0, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		ReapplyDrivenProperties_Invoke_m2731383309((ReapplyDrivenProperties_t3880910420 *)__this->get_prev_9(),___driven0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (RuntimeObject *, void* __this, RectTransform_t3950565364 * ___driven0, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___driven0,(RuntimeMethod*)(__this->get_method_3()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, RectTransform_t3950565364 * ___driven0, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___driven0,(RuntimeMethod*)(__this->get_method_3()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___driven0,(RuntimeMethod*)(__this->get_method_3()));
	}
}
// System.IAsyncResult UnityEngine.RectTransform/ReapplyDrivenProperties::BeginInvoke(UnityEngine.RectTransform,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* ReapplyDrivenProperties_BeginInvoke_m287277383 (ReapplyDrivenProperties_t3880910420 * __this, RectTransform_t3950565364 * ___driven0, AsyncCallback_t1046330627 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___driven0;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Void UnityEngine.RectTransform/ReapplyDrivenProperties::EndInvoke(System.IAsyncResult)
extern "C"  void ReapplyDrivenProperties_EndInvoke_m3904559314 (ReapplyDrivenProperties_t3880910420 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
