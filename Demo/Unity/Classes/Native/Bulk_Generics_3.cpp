﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t522578821;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t449703515;
// System.InvalidOperationException
struct InvalidOperationException_t1956282378;
// System.Type
struct Type_t;
// System.ObjectDisposedException
struct ObjectDisposedException_t363085662;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t1174585319;
// System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>
struct List_1_t2599265951;
// System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>
struct List_1_t546271037;
// System.ArgumentOutOfRangeException
struct ArgumentOutOfRangeException_t1745463579;
// System.Collections.Generic.IEnumerator`1<System.Int32>
struct IEnumerator_1_t1516489874;
// System.Collections.IEnumerator
struct IEnumerator_t4262330110;
// System.ArgumentException
struct ArgumentException_t1372035057;
// System.Collections.Generic.ICollection`1<System.Int32>
struct ICollection_1_t3310782218;
// System.Collections.Generic.IEnumerable`1<System.Int32>
struct IEnumerable_1_t687970330;
// System.Int32[]
struct Int32U5BU5D_t2856113350;
// System.ArgumentNullException
struct ArgumentNullException_t517973450;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t2241371678;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t4035664022;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t1412852134;
// System.Object[]
struct ObjectU5BU5D_t4290686858;
// System.Collections.Generic.IEnumerator`1<System.Reflection.CustomAttributeNamedArgument>
struct IEnumerator_1_t3666052310;
// System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeNamedArgument>
struct ICollection_1_t1165377358;
// System.Collections.Generic.IEnumerable`1<System.Reflection.CustomAttributeNamedArgument>
struct IEnumerable_1_t2837532766;
// System.Reflection.CustomAttributeNamedArgument[]
struct CustomAttributeNamedArgumentU5BU5D_t3127017202;
// System.Collections.Generic.IEnumerator`1<System.Reflection.CustomAttributeTypedArgument>
struct IEnumerator_1_t1613057396;
// System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeTypedArgument>
struct ICollection_1_t3407349740;
// System.Collections.Generic.IEnumerable`1<System.Reflection.CustomAttributeTypedArgument>
struct IEnumerable_1_t784537852;
// System.Reflection.CustomAttributeTypedArgument[]
struct CustomAttributeTypedArgumentU5BU5D_t2668173852;
// System.Collections.Generic.Queue`1<System.Object>
struct Queue_1_t2668810036;
// System.Collections.Generic.Queue`1<UnityEngine.UnitySynchronizationContext/WorkRequest>
struct Queue_1_t1920222844;
// System.Collections.Generic.IEnumerator`1<UnityEngine.UnitySynchronizationContext/WorkRequest>
struct IEnumerator_1_t1492784486;
// System.Collections.Generic.Stack`1<System.Object>
struct Stack_1_t3886787695;
// System.Collections.ObjectModel.Collection`1<System.Object>
struct Collection_1_t915301837;
// System.Collections.Generic.IList`1<System.Object>
struct IList_1_t1015479969;
// System.NotSupportedException
struct NotSupportedException_t404487136;
// System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>
struct Collection_1_t2339982469;
// System.Collections.Generic.IList`1<System.Reflection.CustomAttributeNamedArgument>
struct IList_1_t2440160601;
// System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>
struct Collection_1_t286987555;
// System.Collections.Generic.IList`1<System.Reflection.CustomAttributeTypedArgument>
struct IList_1_t387165687;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>
struct ReadOnlyCollection_1_t1566706403;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>
struct ReadOnlyCollection_1_t2991387035;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>
struct ReadOnlyCollection_1_t938392121;
// System.Comparison`1<System.Object>
struct Comparison_1_t1028579740;
// System.IAsyncResult
struct IAsyncResult_t2277996523;
// System.AsyncCallback
struct AsyncCallback_t1046330627;
// System.Converter`2<System.Object,System.Object>
struct Converter_2_t4184931014;
// System.Func`2<System.Object,System.Boolean>
struct Func_2_t1469056018;
// System.Func`2<System.Object,System.Object>
struct Func_2_t2705400122;
// UnityEngine.UnitySynchronizationContext/WorkRequest[]
struct WorkRequestU5BU5D_t3723184674;
// System.IntPtr[]
struct IntPtrU5BU5D_t2893013242;
// System.Collections.IDictionary
struct IDictionary_t1911576662;
// System.Char[]
struct CharU5BU5D_t2864810077;
// System.Threading.SendOrPostCallback
struct SendOrPostCallback_t2649904830;
// System.Threading.ManualResetEvent
struct ManualResetEvent_t3770519771;
// System.Void
struct Void_t1078098495;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t2624639528;
// System.Reflection.MemberInfo
struct MemberInfo_t;
// System.Type[]
struct TypeU5BU5D_t1830665827;
// System.Reflection.MemberFilter
struct MemberFilter_t1197335030;

extern RuntimeClass* StringU5BU5D_t522578821_il2cpp_TypeInfo_var;
extern RuntimeClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1608275668;
extern Il2CppCodeGenString* _stringLiteral2131789870;
extern Il2CppCodeGenString* _stringLiteral527848695;
extern const uint32_t KeyValuePair_2_ToString_m272197277_MetadataUsageId;
extern const uint32_t KeyValuePair_2_ToString_m2957839857_MetadataUsageId;
extern const uint32_t KeyValuePair_2_ToString_m3938306873_MetadataUsageId;
extern const uint32_t KeyValuePair_2_ToString_m1421527622_MetadataUsageId;
extern RuntimeClass* InvalidOperationException_t1956282378_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m1371828734_MetadataUsageId;
extern RuntimeClass* ObjectDisposedException_t363085662_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral573806582;
extern const uint32_t Enumerator_VerifyState_m2300229768_MetadataUsageId;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m1417958798_MetadataUsageId;
extern const uint32_t Enumerator_VerifyState_m2889154_MetadataUsageId;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m4080730988_MetadataUsageId;
extern const uint32_t Enumerator_VerifyState_m1870543230_MetadataUsageId;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m2003157540_MetadataUsageId;
extern const uint32_t Enumerator_VerifyState_m827611239_MetadataUsageId;
extern RuntimeClass* ArgumentOutOfRangeException_t1745463579_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2929636671;
extern const uint32_t List_1__ctor_m3542732767_MetadataUsageId;
extern RuntimeClass* NullReferenceException_t2549690990_il2cpp_TypeInfo_var;
extern RuntimeClass* InvalidCastException_t1009887698_il2cpp_TypeInfo_var;
extern RuntimeClass* ArgumentException_t1372035057_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3684952963;
extern const uint32_t List_1_System_Collections_IList_Add_m2139410595_MetadataUsageId;
extern const uint32_t List_1_System_Collections_IList_Contains_m2533151603_MetadataUsageId;
extern const uint32_t List_1_System_Collections_IList_IndexOf_m3575252870_MetadataUsageId;
extern const uint32_t List_1_System_Collections_IList_Insert_m386691397_MetadataUsageId;
extern const uint32_t List_1_System_Collections_IList_Remove_m2848662898_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3197723981;
extern const uint32_t List_1_System_Collections_IList_set_Item_m3284437010_MetadataUsageId;
extern RuntimeClass* IEnumerator_t4262330110_il2cpp_TypeInfo_var;
extern RuntimeClass* IDisposable_t318870991_il2cpp_TypeInfo_var;
extern const uint32_t List_1_AddEnumerable_m1412616697_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1171314202;
extern const uint32_t List_1_CheckIndex_m2334255516_MetadataUsageId;
extern RuntimeClass* ArgumentNullException_t517973450_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2657729115;
extern const uint32_t List_1_CheckCollection_m2677906726_MetadataUsageId;
extern const uint32_t List_1_RemoveAt_m2834769719_MetadataUsageId;
extern const uint32_t List_1_set_Capacity_m885225211_MetadataUsageId;
extern const uint32_t List_1_get_Item_m4017031976_MetadataUsageId;
extern const uint32_t List_1_set_Item_m3378504976_MetadataUsageId;
extern const uint32_t List_1__ctor_m628538615_MetadataUsageId;
extern const uint32_t List_1_System_Collections_IList_Add_m1495383345_MetadataUsageId;
extern const uint32_t List_1_System_Collections_IList_Contains_m1442344937_MetadataUsageId;
extern const uint32_t List_1_System_Collections_IList_IndexOf_m2504690593_MetadataUsageId;
extern const uint32_t List_1_System_Collections_IList_Insert_m4294775317_MetadataUsageId;
extern const uint32_t List_1_System_Collections_IList_Remove_m2638065942_MetadataUsageId;
extern const uint32_t List_1_System_Collections_IList_set_Item_m98177035_MetadataUsageId;
extern const uint32_t List_1_AddEnumerable_m761171694_MetadataUsageId;
extern const uint32_t List_1_CheckIndex_m1105134347_MetadataUsageId;
extern const uint32_t List_1_CheckCollection_m54451682_MetadataUsageId;
extern const uint32_t List_1_RemoveAt_m3581840026_MetadataUsageId;
extern const uint32_t List_1_set_Capacity_m2480705992_MetadataUsageId;
extern const uint32_t List_1_get_Item_m30538788_MetadataUsageId;
extern const uint32_t List_1_set_Item_m4287706498_MetadataUsageId;
extern const uint32_t List_1__ctor_m3193603608_MetadataUsageId;
extern const uint32_t List_1_System_Collections_IList_Add_m4289758969_MetadataUsageId;
extern const uint32_t List_1_System_Collections_IList_Contains_m2200782575_MetadataUsageId;
extern const uint32_t List_1_System_Collections_IList_IndexOf_m3909285767_MetadataUsageId;
extern const uint32_t List_1_System_Collections_IList_Insert_m1035245345_MetadataUsageId;
extern const uint32_t List_1_System_Collections_IList_Remove_m1601974356_MetadataUsageId;
extern const uint32_t List_1_System_Collections_IList_set_Item_m2806321811_MetadataUsageId;
extern const uint32_t List_1_AddEnumerable_m2006982645_MetadataUsageId;
extern const uint32_t List_1_CheckIndex_m3158883304_MetadataUsageId;
extern const uint32_t List_1_CheckCollection_m1098393654_MetadataUsageId;
extern const uint32_t List_1_RemoveAt_m173482288_MetadataUsageId;
extern const uint32_t List_1_set_Capacity_m1168893075_MetadataUsageId;
extern const uint32_t List_1_get_Item_m3385836824_MetadataUsageId;
extern const uint32_t List_1_set_Item_m3966278890_MetadataUsageId;
extern const uint32_t List_1__ctor_m3102341422_MetadataUsageId;
extern const uint32_t List_1_System_Collections_IList_Add_m149808363_MetadataUsageId;
extern const uint32_t List_1_System_Collections_IList_Contains_m3356661005_MetadataUsageId;
extern const uint32_t List_1_System_Collections_IList_IndexOf_m1161609980_MetadataUsageId;
extern const uint32_t List_1_System_Collections_IList_Insert_m311886778_MetadataUsageId;
extern const uint32_t List_1_System_Collections_IList_Remove_m3278455504_MetadataUsageId;
extern const uint32_t List_1_System_Collections_IList_set_Item_m64158457_MetadataUsageId;
extern const uint32_t List_1_AddEnumerable_m2205638403_MetadataUsageId;
extern const uint32_t List_1_CheckIndex_m1537533006_MetadataUsageId;
extern const uint32_t List_1_CheckCollection_m1542265315_MetadataUsageId;
extern const uint32_t List_1_RemoveAt_m2674740562_MetadataUsageId;
extern const uint32_t List_1_set_Capacity_m553904685_MetadataUsageId;
extern const uint32_t List_1_get_Item_m3151231619_MetadataUsageId;
extern const uint32_t List_1_set_Item_m1870828102_MetadataUsageId;
extern const uint32_t Enumerator_System_Collections_IEnumerator_Reset_m2386365462_MetadataUsageId;
extern const uint32_t Enumerator_MoveNext_m726149558_MetadataUsageId;
extern const uint32_t Enumerator_get_Current_m373730718_MetadataUsageId;
extern const uint32_t Enumerator_System_Collections_IEnumerator_Reset_m2717756568_MetadataUsageId;
extern const uint32_t Enumerator_MoveNext_m2652681985_MetadataUsageId;
extern const uint32_t Enumerator_get_Current_m1195414335_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3036108506;
extern const uint32_t Queue_1__ctor_m460898850_MetadataUsageId;
extern RuntimeClass* ArrayTypeMismatchException_t1927459263_il2cpp_TypeInfo_var;
extern const uint32_t Queue_1_System_Collections_ICollection_CopyTo_m3128935235_MetadataUsageId;
extern RuntimeClass* RuntimeObject_il2cpp_TypeInfo_var;
extern const uint32_t Queue_1_Dequeue_m426484238_MetadataUsageId;
extern const uint32_t Queue_1_Peek_m632453695_MetadataUsageId;
extern const uint32_t Queue_1__ctor_m1955792661_MetadataUsageId;
extern const uint32_t Queue_1_System_Collections_ICollection_CopyTo_m3735502342_MetadataUsageId;
extern RuntimeClass* WorkRequest_t2547429811_il2cpp_TypeInfo_var;
extern const uint32_t Queue_1_Dequeue_m926929859_MetadataUsageId;
extern const uint32_t Queue_1_Peek_m1793053076_MetadataUsageId;
extern const uint32_t Enumerator_System_Collections_IEnumerator_Reset_m4168111423_MetadataUsageId;
extern const uint32_t Enumerator_MoveNext_m2828987763_MetadataUsageId;
extern const uint32_t Enumerator_get_Current_m887231645_MetadataUsageId;
extern const uint32_t Stack_1_System_Collections_ICollection_CopyTo_m1246417032_MetadataUsageId;
extern const uint32_t Stack_1_Pop_m2727410536_MetadataUsageId;
extern RuntimeClass* ICollection_t1881172908_il2cpp_TypeInfo_var;
extern const uint32_t Collection_1__ctor_m2329675133_MetadataUsageId;
extern const uint32_t Collection_1_System_Collections_ICollection_CopyTo_m1677282244_MetadataUsageId;
extern RuntimeClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t Collection_1_IsValidItem_m2716631577_MetadataUsageId;
extern const uint32_t Collection_1_ConvertItem_m274959791_MetadataUsageId;
extern RuntimeClass* NotSupportedException_t404487136_il2cpp_TypeInfo_var;
extern const uint32_t Collection_1_CheckWritable_m269185732_MetadataUsageId;
extern const uint32_t Collection_1__ctor_m905238948_MetadataUsageId;
extern const uint32_t Collection_1_System_Collections_ICollection_CopyTo_m262821365_MetadataUsageId;
extern const uint32_t Collection_1_IsValidItem_m1823302628_MetadataUsageId;
extern const uint32_t Collection_1_ConvertItem_m1597384526_MetadataUsageId;
extern const uint32_t Collection_1_CheckWritable_m1149777024_MetadataUsageId;
extern const uint32_t Collection_1__ctor_m249788010_MetadataUsageId;
extern const uint32_t Collection_1_System_Collections_ICollection_CopyTo_m3489689141_MetadataUsageId;
extern const uint32_t Collection_1_IsValidItem_m2435816978_MetadataUsageId;
extern const uint32_t Collection_1_ConvertItem_m1866434341_MetadataUsageId;
extern const uint32_t Collection_1_CheckWritable_m3803255060_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1392156319;
extern const uint32_t ReadOnlyCollection_1__ctor_m2942681720_MetadataUsageId;
extern const uint32_t ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m3636396274_MetadataUsageId;
extern const uint32_t ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m2459590670_MetadataUsageId;
extern const uint32_t ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m56048574_MetadataUsageId;
extern const uint32_t ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m2475928297_MetadataUsageId;
extern const uint32_t ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m3133058195_MetadataUsageId;
extern const uint32_t ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m2396010076_MetadataUsageId;
extern const uint32_t ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1659178407_MetadataUsageId;
extern RuntimeClass* IEnumerable_t1872631827_il2cpp_TypeInfo_var;
extern const uint32_t ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m2575411489_MetadataUsageId;
extern const uint32_t ReadOnlyCollection_1_System_Collections_IList_Add_m512791597_MetadataUsageId;
extern const uint32_t ReadOnlyCollection_1_System_Collections_IList_Clear_m1723151712_MetadataUsageId;
extern const uint32_t ReadOnlyCollection_1_System_Collections_IList_Insert_m3706834204_MetadataUsageId;
extern const uint32_t ReadOnlyCollection_1_System_Collections_IList_Remove_m1188019359_MetadataUsageId;
extern const uint32_t ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m846301158_MetadataUsageId;
extern const uint32_t ReadOnlyCollection_1_System_Collections_IList_set_Item_m2878433948_MetadataUsageId;
extern const uint32_t ReadOnlyCollection_1__ctor_m2132837842_MetadataUsageId;
extern const uint32_t ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m722286098_MetadataUsageId;
extern const uint32_t ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m940293707_MetadataUsageId;
extern const uint32_t ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m2195383377_MetadataUsageId;
extern const uint32_t ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m2723190029_MetadataUsageId;
extern const uint32_t ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1126840894_MetadataUsageId;
extern const uint32_t ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m4285705850_MetadataUsageId;
extern const uint32_t ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m3787892541_MetadataUsageId;
extern const uint32_t ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m1931371127_MetadataUsageId;
extern const uint32_t ReadOnlyCollection_1_System_Collections_IList_Add_m3318813241_MetadataUsageId;
extern const uint32_t ReadOnlyCollection_1_System_Collections_IList_Clear_m1348697002_MetadataUsageId;
extern const uint32_t ReadOnlyCollection_1_System_Collections_IList_Insert_m209543415_MetadataUsageId;
extern const uint32_t ReadOnlyCollection_1_System_Collections_IList_Remove_m207792272_MetadataUsageId;
extern const uint32_t ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m2584752914_MetadataUsageId;
extern const uint32_t ReadOnlyCollection_1_System_Collections_IList_set_Item_m736915307_MetadataUsageId;
extern const uint32_t ReadOnlyCollection_1__ctor_m3588137511_MetadataUsageId;
extern const uint32_t ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2164904258_MetadataUsageId;
extern const uint32_t ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m3903190083_MetadataUsageId;
extern const uint32_t ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m3712054434_MetadataUsageId;
extern const uint32_t ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m269180624_MetadataUsageId;
extern const uint32_t ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1391504284_MetadataUsageId;
extern const uint32_t ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m3922031874_MetadataUsageId;
extern const uint32_t ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m4098886227_MetadataUsageId;
extern const uint32_t ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m183135228_MetadataUsageId;
extern const uint32_t ReadOnlyCollection_1_System_Collections_IList_Add_m3099590404_MetadataUsageId;
extern const uint32_t ReadOnlyCollection_1_System_Collections_IList_Clear_m2584525582_MetadataUsageId;
extern const uint32_t ReadOnlyCollection_1_System_Collections_IList_Insert_m3639190649_MetadataUsageId;
extern const uint32_t ReadOnlyCollection_1_System_Collections_IList_Remove_m2921050119_MetadataUsageId;
extern const uint32_t ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1775574839_MetadataUsageId;
extern const uint32_t ReadOnlyCollection_1_System_Collections_IList_set_Item_m1368414667_MetadataUsageId;

struct StringU5BU5D_t522578821;
struct Int32U5BU5D_t2856113350;
struct ObjectU5BU5D_t4290686858;
struct CustomAttributeNamedArgumentU5BU5D_t3127017202;
struct CustomAttributeTypedArgumentU5BU5D_t2668173852;
struct WorkRequestU5BU5D_t3723184674;


#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef QUEUE_1_T1920222844_H
#define QUEUE_1_T1920222844_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Queue`1<UnityEngine.UnitySynchronizationContext/WorkRequest>
struct  Queue_1_t1920222844  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.Queue`1::_array
	WorkRequestU5BU5D_t3723184674* ____array_0;
	// System.Int32 System.Collections.Generic.Queue`1::_head
	int32_t ____head_1;
	// System.Int32 System.Collections.Generic.Queue`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.Queue`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__array_0() { return static_cast<int32_t>(offsetof(Queue_1_t1920222844, ____array_0)); }
	inline WorkRequestU5BU5D_t3723184674* get__array_0() const { return ____array_0; }
	inline WorkRequestU5BU5D_t3723184674** get_address_of__array_0() { return &____array_0; }
	inline void set__array_0(WorkRequestU5BU5D_t3723184674* value)
	{
		____array_0 = value;
		Il2CppCodeGenWriteBarrier((&____array_0), value);
	}

	inline static int32_t get_offset_of__head_1() { return static_cast<int32_t>(offsetof(Queue_1_t1920222844, ____head_1)); }
	inline int32_t get__head_1() const { return ____head_1; }
	inline int32_t* get_address_of__head_1() { return &____head_1; }
	inline void set__head_1(int32_t value)
	{
		____head_1 = value;
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(Queue_1_t1920222844, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(Queue_1_t1920222844, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUEUE_1_T1920222844_H
#ifndef QUEUE_1_T2668810036_H
#define QUEUE_1_T2668810036_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Queue`1<System.Object>
struct  Queue_1_t2668810036  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.Queue`1::_array
	ObjectU5BU5D_t4290686858* ____array_0;
	// System.Int32 System.Collections.Generic.Queue`1::_head
	int32_t ____head_1;
	// System.Int32 System.Collections.Generic.Queue`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.Queue`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__array_0() { return static_cast<int32_t>(offsetof(Queue_1_t2668810036, ____array_0)); }
	inline ObjectU5BU5D_t4290686858* get__array_0() const { return ____array_0; }
	inline ObjectU5BU5D_t4290686858** get_address_of__array_0() { return &____array_0; }
	inline void set__array_0(ObjectU5BU5D_t4290686858* value)
	{
		____array_0 = value;
		Il2CppCodeGenWriteBarrier((&____array_0), value);
	}

	inline static int32_t get_offset_of__head_1() { return static_cast<int32_t>(offsetof(Queue_1_t2668810036, ____head_1)); }
	inline int32_t get__head_1() const { return ____head_1; }
	inline int32_t* get_address_of__head_1() { return &____head_1; }
	inline void set__head_1(int32_t value)
	{
		____head_1 = value;
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(Queue_1_t2668810036, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(Queue_1_t2668810036, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUEUE_1_T2668810036_H
#ifndef COLLECTION_1_T2339982469_H
#define COLLECTION_1_T2339982469_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>
struct  Collection_1_t2339982469  : public RuntimeObject
{
public:
	// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.Collection`1::list
	RuntimeObject* ___list_0;
	// System.Object System.Collections.ObjectModel.Collection`1::syncRoot
	RuntimeObject * ___syncRoot_1;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Collection_1_t2339982469, ___list_0)); }
	inline RuntimeObject* get_list_0() const { return ___list_0; }
	inline RuntimeObject** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(RuntimeObject* value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((&___list_0), value);
	}

	inline static int32_t get_offset_of_syncRoot_1() { return static_cast<int32_t>(offsetof(Collection_1_t2339982469, ___syncRoot_1)); }
	inline RuntimeObject * get_syncRoot_1() const { return ___syncRoot_1; }
	inline RuntimeObject ** get_address_of_syncRoot_1() { return &___syncRoot_1; }
	inline void set_syncRoot_1(RuntimeObject * value)
	{
		___syncRoot_1 = value;
		Il2CppCodeGenWriteBarrier((&___syncRoot_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLECTION_1_T2339982469_H
#ifndef COLLECTION_1_T286987555_H
#define COLLECTION_1_T286987555_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>
struct  Collection_1_t286987555  : public RuntimeObject
{
public:
	// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.Collection`1::list
	RuntimeObject* ___list_0;
	// System.Object System.Collections.ObjectModel.Collection`1::syncRoot
	RuntimeObject * ___syncRoot_1;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Collection_1_t286987555, ___list_0)); }
	inline RuntimeObject* get_list_0() const { return ___list_0; }
	inline RuntimeObject** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(RuntimeObject* value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((&___list_0), value);
	}

	inline static int32_t get_offset_of_syncRoot_1() { return static_cast<int32_t>(offsetof(Collection_1_t286987555, ___syncRoot_1)); }
	inline RuntimeObject * get_syncRoot_1() const { return ___syncRoot_1; }
	inline RuntimeObject ** get_address_of_syncRoot_1() { return &___syncRoot_1; }
	inline void set_syncRoot_1(RuntimeObject * value)
	{
		___syncRoot_1 = value;
		Il2CppCodeGenWriteBarrier((&___syncRoot_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLECTION_1_T286987555_H
#ifndef READONLYCOLLECTION_1_T1566706403_H
#define READONLYCOLLECTION_1_T1566706403_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>
struct  ReadOnlyCollection_1_t1566706403  : public RuntimeObject
{
public:
	// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1::list
	RuntimeObject* ___list_0;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(ReadOnlyCollection_1_t1566706403, ___list_0)); }
	inline RuntimeObject* get_list_0() const { return ___list_0; }
	inline RuntimeObject** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(RuntimeObject* value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((&___list_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // READONLYCOLLECTION_1_T1566706403_H
#ifndef LIST_1_T546271037_H
#define LIST_1_T546271037_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>
struct  List_1_t546271037  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	CustomAttributeTypedArgumentU5BU5D_t2668173852* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t546271037, ____items_1)); }
	inline CustomAttributeTypedArgumentU5BU5D_t2668173852* get__items_1() const { return ____items_1; }
	inline CustomAttributeTypedArgumentU5BU5D_t2668173852** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(CustomAttributeTypedArgumentU5BU5D_t2668173852* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t546271037, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t546271037, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t546271037_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	CustomAttributeTypedArgumentU5BU5D_t2668173852* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t546271037_StaticFields, ___EmptyArray_4)); }
	inline CustomAttributeTypedArgumentU5BU5D_t2668173852* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline CustomAttributeTypedArgumentU5BU5D_t2668173852** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(CustomAttributeTypedArgumentU5BU5D_t2668173852* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T546271037_H
#ifndef READONLYCOLLECTION_1_T2991387035_H
#define READONLYCOLLECTION_1_T2991387035_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>
struct  ReadOnlyCollection_1_t2991387035  : public RuntimeObject
{
public:
	// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1::list
	RuntimeObject* ___list_0;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(ReadOnlyCollection_1_t2991387035, ___list_0)); }
	inline RuntimeObject* get_list_0() const { return ___list_0; }
	inline RuntimeObject** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(RuntimeObject* value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((&___list_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // READONLYCOLLECTION_1_T2991387035_H
#ifndef READONLYCOLLECTION_1_T938392121_H
#define READONLYCOLLECTION_1_T938392121_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>
struct  ReadOnlyCollection_1_t938392121  : public RuntimeObject
{
public:
	// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1::list
	RuntimeObject* ___list_0;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(ReadOnlyCollection_1_t938392121, ___list_0)); }
	inline RuntimeObject* get_list_0() const { return ___list_0; }
	inline RuntimeObject** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(RuntimeObject* value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((&___list_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // READONLYCOLLECTION_1_T938392121_H
#ifndef LIST_1_T2599265951_H
#define LIST_1_T2599265951_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>
struct  List_1_t2599265951  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	CustomAttributeNamedArgumentU5BU5D_t3127017202* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t2599265951, ____items_1)); }
	inline CustomAttributeNamedArgumentU5BU5D_t3127017202* get__items_1() const { return ____items_1; }
	inline CustomAttributeNamedArgumentU5BU5D_t3127017202** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(CustomAttributeNamedArgumentU5BU5D_t3127017202* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t2599265951, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t2599265951, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t2599265951_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	CustomAttributeNamedArgumentU5BU5D_t3127017202* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t2599265951_StaticFields, ___EmptyArray_4)); }
	inline CustomAttributeNamedArgumentU5BU5D_t3127017202* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline CustomAttributeNamedArgumentU5BU5D_t3127017202** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(CustomAttributeNamedArgumentU5BU5D_t3127017202* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T2599265951_H
#ifndef STACK_1_T3886787695_H
#define STACK_1_T3886787695_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Stack`1<System.Object>
struct  Stack_1_t3886787695  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.Stack`1::_array
	ObjectU5BU5D_t4290686858* ____array_0;
	// System.Int32 System.Collections.Generic.Stack`1::_size
	int32_t ____size_1;
	// System.Int32 System.Collections.Generic.Stack`1::_version
	int32_t ____version_2;

public:
	inline static int32_t get_offset_of__array_0() { return static_cast<int32_t>(offsetof(Stack_1_t3886787695, ____array_0)); }
	inline ObjectU5BU5D_t4290686858* get__array_0() const { return ____array_0; }
	inline ObjectU5BU5D_t4290686858** get_address_of__array_0() { return &____array_0; }
	inline void set__array_0(ObjectU5BU5D_t4290686858* value)
	{
		____array_0 = value;
		Il2CppCodeGenWriteBarrier((&____array_0), value);
	}

	inline static int32_t get_offset_of__size_1() { return static_cast<int32_t>(offsetof(Stack_1_t3886787695, ____size_1)); }
	inline int32_t get__size_1() const { return ____size_1; }
	inline int32_t* get_address_of__size_1() { return &____size_1; }
	inline void set__size_1(int32_t value)
	{
		____size_1 = value;
	}

	inline static int32_t get_offset_of__version_2() { return static_cast<int32_t>(offsetof(Stack_1_t3886787695, ____version_2)); }
	inline int32_t get__version_2() const { return ____version_2; }
	inline int32_t* get_address_of__version_2() { return &____version_2; }
	inline void set__version_2(int32_t value)
	{
		____version_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STACK_1_T3886787695_H
#ifndef LIST_1_T1174585319_H
#define LIST_1_T1174585319_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<System.Object>
struct  List_1_t1174585319  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ObjectU5BU5D_t4290686858* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t1174585319, ____items_1)); }
	inline ObjectU5BU5D_t4290686858* get__items_1() const { return ____items_1; }
	inline ObjectU5BU5D_t4290686858** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ObjectU5BU5D_t4290686858* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t1174585319, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t1174585319, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t1174585319_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	ObjectU5BU5D_t4290686858* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t1174585319_StaticFields, ___EmptyArray_4)); }
	inline ObjectU5BU5D_t4290686858* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline ObjectU5BU5D_t4290686858** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(ObjectU5BU5D_t4290686858* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T1174585319_H
#ifndef MEMBERINFO_T_H
#define MEMBERINFO_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MemberInfo
struct  MemberInfo_t  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERINFO_T_H
#ifndef VALUETYPE_T1920584095_H
#define VALUETYPE_T1920584095_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t1920584095  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t1920584095_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t1920584095_marshaled_com
{
};
#endif // VALUETYPE_T1920584095_H
#ifndef COLLECTION_1_T915301837_H
#define COLLECTION_1_T915301837_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.ObjectModel.Collection`1<System.Object>
struct  Collection_1_t915301837  : public RuntimeObject
{
public:
	// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.Collection`1::list
	RuntimeObject* ___list_0;
	// System.Object System.Collections.ObjectModel.Collection`1::syncRoot
	RuntimeObject * ___syncRoot_1;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Collection_1_t915301837, ___list_0)); }
	inline RuntimeObject* get_list_0() const { return ___list_0; }
	inline RuntimeObject** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(RuntimeObject* value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((&___list_0), value);
	}

	inline static int32_t get_offset_of_syncRoot_1() { return static_cast<int32_t>(offsetof(Collection_1_t915301837, ___syncRoot_1)); }
	inline RuntimeObject * get_syncRoot_1() const { return ___syncRoot_1; }
	inline RuntimeObject ** get_address_of_syncRoot_1() { return &___syncRoot_1; }
	inline void set_syncRoot_1(RuntimeObject * value)
	{
		___syncRoot_1 = value;
		Il2CppCodeGenWriteBarrier((&___syncRoot_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLECTION_1_T915301837_H
#ifndef LIST_1_T449703515_H
#define LIST_1_T449703515_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<System.Int32>
struct  List_1_t449703515  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	Int32U5BU5D_t2856113350* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t449703515, ____items_1)); }
	inline Int32U5BU5D_t2856113350* get__items_1() const { return ____items_1; }
	inline Int32U5BU5D_t2856113350** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(Int32U5BU5D_t2856113350* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t449703515, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t449703515, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t449703515_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	Int32U5BU5D_t2856113350* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t449703515_StaticFields, ___EmptyArray_4)); }
	inline Int32U5BU5D_t2856113350* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline Int32U5BU5D_t2856113350** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(Int32U5BU5D_t2856113350* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T449703515_H
#ifndef EXCEPTION_T3361176243_H
#define EXCEPTION_T3361176243_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t3361176243  : public RuntimeObject
{
public:
	// System.IntPtr[] System.Exception::trace_ips
	IntPtrU5BU5D_t2893013242* ___trace_ips_0;
	// System.Exception System.Exception::inner_exception
	Exception_t3361176243 * ___inner_exception_1;
	// System.String System.Exception::message
	String_t* ___message_2;
	// System.String System.Exception::help_link
	String_t* ___help_link_3;
	// System.String System.Exception::class_name
	String_t* ___class_name_4;
	// System.String System.Exception::stack_trace
	String_t* ___stack_trace_5;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_6;
	// System.Int32 System.Exception::remote_stack_index
	int32_t ___remote_stack_index_7;
	// System.Int32 System.Exception::hresult
	int32_t ___hresult_8;
	// System.String System.Exception::source
	String_t* ___source_9;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_10;

public:
	inline static int32_t get_offset_of_trace_ips_0() { return static_cast<int32_t>(offsetof(Exception_t3361176243, ___trace_ips_0)); }
	inline IntPtrU5BU5D_t2893013242* get_trace_ips_0() const { return ___trace_ips_0; }
	inline IntPtrU5BU5D_t2893013242** get_address_of_trace_ips_0() { return &___trace_ips_0; }
	inline void set_trace_ips_0(IntPtrU5BU5D_t2893013242* value)
	{
		___trace_ips_0 = value;
		Il2CppCodeGenWriteBarrier((&___trace_ips_0), value);
	}

	inline static int32_t get_offset_of_inner_exception_1() { return static_cast<int32_t>(offsetof(Exception_t3361176243, ___inner_exception_1)); }
	inline Exception_t3361176243 * get_inner_exception_1() const { return ___inner_exception_1; }
	inline Exception_t3361176243 ** get_address_of_inner_exception_1() { return &___inner_exception_1; }
	inline void set_inner_exception_1(Exception_t3361176243 * value)
	{
		___inner_exception_1 = value;
		Il2CppCodeGenWriteBarrier((&___inner_exception_1), value);
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(Exception_t3361176243, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier((&___message_2), value);
	}

	inline static int32_t get_offset_of_help_link_3() { return static_cast<int32_t>(offsetof(Exception_t3361176243, ___help_link_3)); }
	inline String_t* get_help_link_3() const { return ___help_link_3; }
	inline String_t** get_address_of_help_link_3() { return &___help_link_3; }
	inline void set_help_link_3(String_t* value)
	{
		___help_link_3 = value;
		Il2CppCodeGenWriteBarrier((&___help_link_3), value);
	}

	inline static int32_t get_offset_of_class_name_4() { return static_cast<int32_t>(offsetof(Exception_t3361176243, ___class_name_4)); }
	inline String_t* get_class_name_4() const { return ___class_name_4; }
	inline String_t** get_address_of_class_name_4() { return &___class_name_4; }
	inline void set_class_name_4(String_t* value)
	{
		___class_name_4 = value;
		Il2CppCodeGenWriteBarrier((&___class_name_4), value);
	}

	inline static int32_t get_offset_of_stack_trace_5() { return static_cast<int32_t>(offsetof(Exception_t3361176243, ___stack_trace_5)); }
	inline String_t* get_stack_trace_5() const { return ___stack_trace_5; }
	inline String_t** get_address_of_stack_trace_5() { return &___stack_trace_5; }
	inline void set_stack_trace_5(String_t* value)
	{
		___stack_trace_5 = value;
		Il2CppCodeGenWriteBarrier((&___stack_trace_5), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_6() { return static_cast<int32_t>(offsetof(Exception_t3361176243, ____remoteStackTraceString_6)); }
	inline String_t* get__remoteStackTraceString_6() const { return ____remoteStackTraceString_6; }
	inline String_t** get_address_of__remoteStackTraceString_6() { return &____remoteStackTraceString_6; }
	inline void set__remoteStackTraceString_6(String_t* value)
	{
		____remoteStackTraceString_6 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_6), value);
	}

	inline static int32_t get_offset_of_remote_stack_index_7() { return static_cast<int32_t>(offsetof(Exception_t3361176243, ___remote_stack_index_7)); }
	inline int32_t get_remote_stack_index_7() const { return ___remote_stack_index_7; }
	inline int32_t* get_address_of_remote_stack_index_7() { return &___remote_stack_index_7; }
	inline void set_remote_stack_index_7(int32_t value)
	{
		___remote_stack_index_7 = value;
	}

	inline static int32_t get_offset_of_hresult_8() { return static_cast<int32_t>(offsetof(Exception_t3361176243, ___hresult_8)); }
	inline int32_t get_hresult_8() const { return ___hresult_8; }
	inline int32_t* get_address_of_hresult_8() { return &___hresult_8; }
	inline void set_hresult_8(int32_t value)
	{
		___hresult_8 = value;
	}

	inline static int32_t get_offset_of_source_9() { return static_cast<int32_t>(offsetof(Exception_t3361176243, ___source_9)); }
	inline String_t* get_source_9() const { return ___source_9; }
	inline String_t** get_address_of_source_9() { return &___source_9; }
	inline void set_source_9(String_t* value)
	{
		___source_9 = value;
		Il2CppCodeGenWriteBarrier((&___source_9), value);
	}

	inline static int32_t get_offset_of__data_10() { return static_cast<int32_t>(offsetof(Exception_t3361176243, ____data_10)); }
	inline RuntimeObject* get__data_10() const { return ____data_10; }
	inline RuntimeObject** get_address_of__data_10() { return &____data_10; }
	inline void set__data_10(RuntimeObject* value)
	{
		____data_10 = value;
		Il2CppCodeGenWriteBarrier((&____data_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTION_T3361176243_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::length
	int32_t ___length_0;
	// System.Char System.String::start_char
	Il2CppChar ___start_char_1;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(String_t, ___length_0)); }
	inline int32_t get_length_0() const { return ___length_0; }
	inline int32_t* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(int32_t value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_start_char_1() { return static_cast<int32_t>(offsetof(String_t, ___start_char_1)); }
	inline Il2CppChar get_start_char_1() const { return ___start_char_1; }
	inline Il2CppChar* get_address_of_start_char_1() { return &___start_char_1; }
	inline void set_start_char_1(Il2CppChar value)
	{
		___start_char_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_2;
	// System.Char[] System.String::WhiteChars
	CharU5BU5D_t2864810077* ___WhiteChars_3;

public:
	inline static int32_t get_offset_of_Empty_2() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_2)); }
	inline String_t* get_Empty_2() const { return ___Empty_2; }
	inline String_t** get_address_of_Empty_2() { return &___Empty_2; }
	inline void set_Empty_2(String_t* value)
	{
		___Empty_2 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_2), value);
	}

	inline static int32_t get_offset_of_WhiteChars_3() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___WhiteChars_3)); }
	inline CharU5BU5D_t2864810077* get_WhiteChars_3() const { return ___WhiteChars_3; }
	inline CharU5BU5D_t2864810077** get_address_of_WhiteChars_3() { return &___WhiteChars_3; }
	inline void set_WhiteChars_3(CharU5BU5D_t2864810077* value)
	{
		___WhiteChars_3 = value;
		Il2CppCodeGenWriteBarrier((&___WhiteChars_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef ENUMERATOR_T1041909981_H
#define ENUMERATOR_T1041909981_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Stack`1/Enumerator<System.Object>
struct  Enumerator_t1041909981 
{
public:
	// System.Collections.Generic.Stack`1<T> System.Collections.Generic.Stack`1/Enumerator::parent
	Stack_1_t3886787695 * ___parent_0;
	// System.Int32 System.Collections.Generic.Stack`1/Enumerator::idx
	int32_t ___idx_1;
	// System.Int32 System.Collections.Generic.Stack`1/Enumerator::_version
	int32_t ____version_2;

public:
	inline static int32_t get_offset_of_parent_0() { return static_cast<int32_t>(offsetof(Enumerator_t1041909981, ___parent_0)); }
	inline Stack_1_t3886787695 * get_parent_0() const { return ___parent_0; }
	inline Stack_1_t3886787695 ** get_address_of_parent_0() { return &___parent_0; }
	inline void set_parent_0(Stack_1_t3886787695 * value)
	{
		___parent_0 = value;
		Il2CppCodeGenWriteBarrier((&___parent_0), value);
	}

	inline static int32_t get_offset_of_idx_1() { return static_cast<int32_t>(offsetof(Enumerator_t1041909981, ___idx_1)); }
	inline int32_t get_idx_1() const { return ___idx_1; }
	inline int32_t* get_address_of_idx_1() { return &___idx_1; }
	inline void set_idx_1(int32_t value)
	{
		___idx_1 = value;
	}

	inline static int32_t get_offset_of__version_2() { return static_cast<int32_t>(offsetof(Enumerator_t1041909981, ____version_2)); }
	inline int32_t get__version_2() const { return ____version_2; }
	inline int32_t* get_address_of__version_2() { return &____version_2; }
	inline void set__version_2(int32_t value)
	{
		____version_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T1041909981_H
#ifndef WORKREQUEST_T2547429811_H
#define WORKREQUEST_T2547429811_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UnitySynchronizationContext/WorkRequest
struct  WorkRequest_t2547429811 
{
public:
	// System.Threading.SendOrPostCallback UnityEngine.UnitySynchronizationContext/WorkRequest::m_DelagateCallback
	SendOrPostCallback_t2649904830 * ___m_DelagateCallback_0;
	// System.Object UnityEngine.UnitySynchronizationContext/WorkRequest::m_DelagateState
	RuntimeObject * ___m_DelagateState_1;
	// System.Threading.ManualResetEvent UnityEngine.UnitySynchronizationContext/WorkRequest::m_WaitHandle
	ManualResetEvent_t3770519771 * ___m_WaitHandle_2;

public:
	inline static int32_t get_offset_of_m_DelagateCallback_0() { return static_cast<int32_t>(offsetof(WorkRequest_t2547429811, ___m_DelagateCallback_0)); }
	inline SendOrPostCallback_t2649904830 * get_m_DelagateCallback_0() const { return ___m_DelagateCallback_0; }
	inline SendOrPostCallback_t2649904830 ** get_address_of_m_DelagateCallback_0() { return &___m_DelagateCallback_0; }
	inline void set_m_DelagateCallback_0(SendOrPostCallback_t2649904830 * value)
	{
		___m_DelagateCallback_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_DelagateCallback_0), value);
	}

	inline static int32_t get_offset_of_m_DelagateState_1() { return static_cast<int32_t>(offsetof(WorkRequest_t2547429811, ___m_DelagateState_1)); }
	inline RuntimeObject * get_m_DelagateState_1() const { return ___m_DelagateState_1; }
	inline RuntimeObject ** get_address_of_m_DelagateState_1() { return &___m_DelagateState_1; }
	inline void set_m_DelagateState_1(RuntimeObject * value)
	{
		___m_DelagateState_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_DelagateState_1), value);
	}

	inline static int32_t get_offset_of_m_WaitHandle_2() { return static_cast<int32_t>(offsetof(WorkRequest_t2547429811, ___m_WaitHandle_2)); }
	inline ManualResetEvent_t3770519771 * get_m_WaitHandle_2() const { return ___m_WaitHandle_2; }
	inline ManualResetEvent_t3770519771 ** get_address_of_m_WaitHandle_2() { return &___m_WaitHandle_2; }
	inline void set_m_WaitHandle_2(ManualResetEvent_t3770519771 * value)
	{
		___m_WaitHandle_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_WaitHandle_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.UnitySynchronizationContext/WorkRequest
struct WorkRequest_t2547429811_marshaled_pinvoke
{
	Il2CppMethodPointer ___m_DelagateCallback_0;
	Il2CppIUnknown* ___m_DelagateState_1;
	ManualResetEvent_t3770519771 * ___m_WaitHandle_2;
};
// Native definition for COM marshalling of UnityEngine.UnitySynchronizationContext/WorkRequest
struct WorkRequest_t2547429811_marshaled_com
{
	Il2CppMethodPointer ___m_DelagateCallback_0;
	Il2CppIUnknown* ___m_DelagateState_1;
	ManualResetEvent_t3770519771 * ___m_WaitHandle_2;
};
#endif // WORKREQUEST_T2547429811_H
#ifndef ENUMERATOR_T3479511895_H
#define ENUMERATOR_T3479511895_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Queue`1/Enumerator<UnityEngine.UnitySynchronizationContext/WorkRequest>
struct  Enumerator_t3479511895 
{
public:
	// System.Collections.Generic.Queue`1<T> System.Collections.Generic.Queue`1/Enumerator::q
	Queue_1_t1920222844 * ___q_0;
	// System.Int32 System.Collections.Generic.Queue`1/Enumerator::idx
	int32_t ___idx_1;
	// System.Int32 System.Collections.Generic.Queue`1/Enumerator::ver
	int32_t ___ver_2;

public:
	inline static int32_t get_offset_of_q_0() { return static_cast<int32_t>(offsetof(Enumerator_t3479511895, ___q_0)); }
	inline Queue_1_t1920222844 * get_q_0() const { return ___q_0; }
	inline Queue_1_t1920222844 ** get_address_of_q_0() { return &___q_0; }
	inline void set_q_0(Queue_1_t1920222844 * value)
	{
		___q_0 = value;
		Il2CppCodeGenWriteBarrier((&___q_0), value);
	}

	inline static int32_t get_offset_of_idx_1() { return static_cast<int32_t>(offsetof(Enumerator_t3479511895, ___idx_1)); }
	inline int32_t get_idx_1() const { return ___idx_1; }
	inline int32_t* get_address_of_idx_1() { return &___idx_1; }
	inline void set_idx_1(int32_t value)
	{
		___idx_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t3479511895, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T3479511895_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef ENUMERATOR_T4228099087_H
#define ENUMERATOR_T4228099087_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Queue`1/Enumerator<System.Object>
struct  Enumerator_t4228099087 
{
public:
	// System.Collections.Generic.Queue`1<T> System.Collections.Generic.Queue`1/Enumerator::q
	Queue_1_t2668810036 * ___q_0;
	// System.Int32 System.Collections.Generic.Queue`1/Enumerator::idx
	int32_t ___idx_1;
	// System.Int32 System.Collections.Generic.Queue`1/Enumerator::ver
	int32_t ___ver_2;

public:
	inline static int32_t get_offset_of_q_0() { return static_cast<int32_t>(offsetof(Enumerator_t4228099087, ___q_0)); }
	inline Queue_1_t2668810036 * get_q_0() const { return ___q_0; }
	inline Queue_1_t2668810036 ** get_address_of_q_0() { return &___q_0; }
	inline void set_q_0(Queue_1_t2668810036 * value)
	{
		___q_0 = value;
		Il2CppCodeGenWriteBarrier((&___q_0), value);
	}

	inline static int32_t get_offset_of_idx_1() { return static_cast<int32_t>(offsetof(Enumerator_t4228099087, ___idx_1)); }
	inline int32_t get_idx_1() const { return ___idx_1; }
	inline int32_t* get_address_of_idx_1() { return &___idx_1; }
	inline void set_idx_1(int32_t value)
	{
		___idx_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t4228099087, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T4228099087_H
#ifndef ENUM_T750633987_H
#define ENUM_T750633987_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t750633987  : public ValueType_t1920584095
{
public:

public:
};

struct Enum_t750633987_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t2864810077* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t750633987_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t2864810077* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t2864810077** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t2864810077* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t750633987_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t750633987_marshaled_com
{
};
#endif // ENUM_T750633987_H
#ifndef VOID_T1078098495_H
#define VOID_T1078098495_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1078098495 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1078098495_H
#ifndef CUSTOMATTRIBUTETYPEDARGUMENT_T2667702721_H
#define CUSTOMATTRIBUTETYPEDARGUMENT_T2667702721_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.CustomAttributeTypedArgument
struct  CustomAttributeTypedArgument_t2667702721 
{
public:
	// System.Type System.Reflection.CustomAttributeTypedArgument::argumentType
	Type_t * ___argumentType_0;
	// System.Object System.Reflection.CustomAttributeTypedArgument::value
	RuntimeObject * ___value_1;

public:
	inline static int32_t get_offset_of_argumentType_0() { return static_cast<int32_t>(offsetof(CustomAttributeTypedArgument_t2667702721, ___argumentType_0)); }
	inline Type_t * get_argumentType_0() const { return ___argumentType_0; }
	inline Type_t ** get_address_of_argumentType_0() { return &___argumentType_0; }
	inline void set_argumentType_0(Type_t * value)
	{
		___argumentType_0 = value;
		Il2CppCodeGenWriteBarrier((&___argumentType_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(CustomAttributeTypedArgument_t2667702721, ___value_1)); }
	inline RuntimeObject * get_value_1() const { return ___value_1; }
	inline RuntimeObject ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(RuntimeObject * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Reflection.CustomAttributeTypedArgument
struct CustomAttributeTypedArgument_t2667702721_marshaled_pinvoke
{
	Type_t * ___argumentType_0;
	Il2CppIUnknown* ___value_1;
};
// Native definition for COM marshalling of System.Reflection.CustomAttributeTypedArgument
struct CustomAttributeTypedArgument_t2667702721_marshaled_com
{
	Type_t * ___argumentType_0;
	Il2CppIUnknown* ___value_1;
};
#endif // CUSTOMATTRIBUTETYPEDARGUMENT_T2667702721_H
#ifndef BOOLEAN_T2059672899_H
#define BOOLEAN_T2059672899_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t2059672899 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t2059672899, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t2059672899_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t2059672899_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t2059672899_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T2059672899_H
#ifndef KEYVALUEPAIR_2_T2877556189_H
#define KEYVALUEPAIR_2_T2877556189_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>
struct  KeyValuePair_2_t2877556189 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	RuntimeObject * ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	int32_t ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2877556189, ___key_0)); }
	inline RuntimeObject * get_key_0() const { return ___key_0; }
	inline RuntimeObject ** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(RuntimeObject * value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2877556189, ___value_1)); }
	inline int32_t get_value_1() const { return ___value_1; }
	inline int32_t* get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(int32_t value)
	{
		___value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T2877556189_H
#ifndef INT32_T2571135199_H
#define INT32_T2571135199_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2571135199 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t2571135199, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2571135199_H
#ifndef KEYVALUEPAIR_2_T3602437993_H
#define KEYVALUEPAIR_2_T3602437993_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
struct  KeyValuePair_2_t3602437993 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	RuntimeObject * ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	RuntimeObject * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t3602437993, ___key_0)); }
	inline RuntimeObject * get_key_0() const { return ___key_0; }
	inline RuntimeObject ** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(RuntimeObject * value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t3602437993, ___value_1)); }
	inline RuntimeObject * get_value_1() const { return ___value_1; }
	inline RuntimeObject ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(RuntimeObject * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T3602437993_H
#ifndef ENUMERATOR_T2209518572_H
#define ENUMERATOR_T2209518572_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<System.Int32>
struct  Enumerator_t2209518572 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t449703515 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	int32_t ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_t2209518572, ___l_0)); }
	inline List_1_t449703515 * get_l_0() const { return ___l_0; }
	inline List_1_t449703515 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t449703515 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t2209518572, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t2209518572, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t2209518572, ___current_3)); }
	inline int32_t get_current_3() const { return ___current_3; }
	inline int32_t* get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(int32_t value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T2209518572_H
#ifndef ENUMERATOR_T2934400376_H
#define ENUMERATOR_T2934400376_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<System.Object>
struct  Enumerator_t2934400376 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t1174585319 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	RuntimeObject * ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_t2934400376, ___l_0)); }
	inline List_1_t1174585319 * get_l_0() const { return ___l_0; }
	inline List_1_t1174585319 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t1174585319 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t2934400376, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t2934400376, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t2934400376, ___current_3)); }
	inline RuntimeObject * get_current_3() const { return ___current_3; }
	inline RuntimeObject ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(RuntimeObject * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T2934400376_H
#ifndef KEYVALUEPAIR_2_T2366093889_H
#define KEYVALUEPAIR_2_T2366093889_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>
struct  KeyValuePair_2_t2366093889 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	RuntimeObject * ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	bool ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2366093889, ___key_0)); }
	inline RuntimeObject * get_key_0() const { return ___key_0; }
	inline RuntimeObject ** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(RuntimeObject * value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2366093889, ___value_1)); }
	inline bool get_value_1() const { return ___value_1; }
	inline bool* get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(bool value)
	{
		___value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T2366093889_H
#ifndef SYSTEMEXCEPTION_T3635077273_H
#define SYSTEMEXCEPTION_T3635077273_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t3635077273  : public Exception_t3361176243
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T3635077273_H
#ifndef BINDINGFLAGS_T3268090012_H
#define BINDINGFLAGS_T3268090012_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.BindingFlags
struct  BindingFlags_t3268090012 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BindingFlags_t3268090012, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDINGFLAGS_T3268090012_H
#ifndef NOTSUPPORTEDEXCEPTION_T404487136_H
#define NOTSUPPORTEDEXCEPTION_T404487136_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.NotSupportedException
struct  NotSupportedException_t404487136  : public SystemException_t3635077273
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOTSUPPORTEDEXCEPTION_T404487136_H
#ifndef RUNTIMETYPEHANDLE_T637624852_H
#define RUNTIMETYPEHANDLE_T637624852_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.RuntimeTypeHandle
struct  RuntimeTypeHandle_t637624852 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_t637624852, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMETYPEHANDLE_T637624852_H
#ifndef DELEGATE_T96267039_H
#define DELEGATE_T96267039_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t96267039  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t2624639528 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t96267039, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t96267039, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t96267039, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t96267039, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t96267039, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t96267039, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t96267039, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t96267039, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t96267039, ___data_8)); }
	inline DelegateData_t2624639528 * get_data_8() const { return ___data_8; }
	inline DelegateData_t2624639528 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t2624639528 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T96267039_H
#ifndef ARGUMENTEXCEPTION_T1372035057_H
#define ARGUMENTEXCEPTION_T1372035057_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ArgumentException
struct  ArgumentException_t1372035057  : public SystemException_t3635077273
{
public:
	// System.String System.ArgumentException::param_name
	String_t* ___param_name_12;

public:
	inline static int32_t get_offset_of_param_name_12() { return static_cast<int32_t>(offsetof(ArgumentException_t1372035057, ___param_name_12)); }
	inline String_t* get_param_name_12() const { return ___param_name_12; }
	inline String_t** get_address_of_param_name_12() { return &___param_name_12; }
	inline void set_param_name_12(String_t* value)
	{
		___param_name_12 = value;
		Il2CppCodeGenWriteBarrier((&___param_name_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARGUMENTEXCEPTION_T1372035057_H
#ifndef INVALIDCASTEXCEPTION_T1009887698_H
#define INVALIDCASTEXCEPTION_T1009887698_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.InvalidCastException
struct  InvalidCastException_t1009887698  : public SystemException_t3635077273
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVALIDCASTEXCEPTION_T1009887698_H
#ifndef NULLREFERENCEEXCEPTION_T2549690990_H
#define NULLREFERENCEEXCEPTION_T2549690990_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.NullReferenceException
struct  NullReferenceException_t2549690990  : public SystemException_t3635077273
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLREFERENCEEXCEPTION_T2549690990_H
#ifndef KEYVALUEPAIR_2_T2204764377_H
#define KEYVALUEPAIR_2_T2204764377_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Object>
struct  KeyValuePair_2_t2204764377 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	intptr_t ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	RuntimeObject * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2204764377, ___key_0)); }
	inline intptr_t get_key_0() const { return ___key_0; }
	inline intptr_t* get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(intptr_t value)
	{
		___key_0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2204764377, ___value_1)); }
	inline RuntimeObject * get_value_1() const { return ___value_1; }
	inline RuntimeObject ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(RuntimeObject * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T2204764377_H
#ifndef ENUMERATOR_T2306086094_H
#define ENUMERATOR_T2306086094_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>
struct  Enumerator_t2306086094 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t546271037 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	CustomAttributeTypedArgument_t2667702721  ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_t2306086094, ___l_0)); }
	inline List_1_t546271037 * get_l_0() const { return ___l_0; }
	inline List_1_t546271037 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t546271037 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t2306086094, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t2306086094, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t2306086094, ___current_3)); }
	inline CustomAttributeTypedArgument_t2667702721  get_current_3() const { return ___current_3; }
	inline CustomAttributeTypedArgument_t2667702721 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(CustomAttributeTypedArgument_t2667702721  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T2306086094_H
#ifndef CUSTOMATTRIBUTENAMEDARGUMENT_T425730339_H
#define CUSTOMATTRIBUTENAMEDARGUMENT_T425730339_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.CustomAttributeNamedArgument
struct  CustomAttributeNamedArgument_t425730339 
{
public:
	// System.Reflection.CustomAttributeTypedArgument System.Reflection.CustomAttributeNamedArgument::typedArgument
	CustomAttributeTypedArgument_t2667702721  ___typedArgument_0;
	// System.Reflection.MemberInfo System.Reflection.CustomAttributeNamedArgument::memberInfo
	MemberInfo_t * ___memberInfo_1;

public:
	inline static int32_t get_offset_of_typedArgument_0() { return static_cast<int32_t>(offsetof(CustomAttributeNamedArgument_t425730339, ___typedArgument_0)); }
	inline CustomAttributeTypedArgument_t2667702721  get_typedArgument_0() const { return ___typedArgument_0; }
	inline CustomAttributeTypedArgument_t2667702721 * get_address_of_typedArgument_0() { return &___typedArgument_0; }
	inline void set_typedArgument_0(CustomAttributeTypedArgument_t2667702721  value)
	{
		___typedArgument_0 = value;
	}

	inline static int32_t get_offset_of_memberInfo_1() { return static_cast<int32_t>(offsetof(CustomAttributeNamedArgument_t425730339, ___memberInfo_1)); }
	inline MemberInfo_t * get_memberInfo_1() const { return ___memberInfo_1; }
	inline MemberInfo_t ** get_address_of_memberInfo_1() { return &___memberInfo_1; }
	inline void set_memberInfo_1(MemberInfo_t * value)
	{
		___memberInfo_1 = value;
		Il2CppCodeGenWriteBarrier((&___memberInfo_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Reflection.CustomAttributeNamedArgument
struct CustomAttributeNamedArgument_t425730339_marshaled_pinvoke
{
	CustomAttributeTypedArgument_t2667702721_marshaled_pinvoke ___typedArgument_0;
	MemberInfo_t * ___memberInfo_1;
};
// Native definition for COM marshalling of System.Reflection.CustomAttributeNamedArgument
struct CustomAttributeNamedArgument_t425730339_marshaled_com
{
	CustomAttributeTypedArgument_t2667702721_marshaled_com ___typedArgument_0;
	MemberInfo_t * ___memberInfo_1;
};
#endif // CUSTOMATTRIBUTENAMEDARGUMENT_T425730339_H
#ifndef INVALIDOPERATIONEXCEPTION_T1956282378_H
#define INVALIDOPERATIONEXCEPTION_T1956282378_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.InvalidOperationException
struct  InvalidOperationException_t1956282378  : public SystemException_t3635077273
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVALIDOPERATIONEXCEPTION_T1956282378_H
#ifndef ARRAYTYPEMISMATCHEXCEPTION_T1927459263_H
#define ARRAYTYPEMISMATCHEXCEPTION_T1927459263_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ArrayTypeMismatchException
struct  ArrayTypeMismatchException_t1927459263  : public SystemException_t3635077273
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARRAYTYPEMISMATCHEXCEPTION_T1927459263_H
#ifndef MULTICASTDELEGATE_T69367374_H
#define MULTICASTDELEGATE_T69367374_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t69367374  : public Delegate_t96267039
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t69367374 * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t69367374 * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t69367374, ___prev_9)); }
	inline MulticastDelegate_t69367374 * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t69367374 ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t69367374 * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t69367374, ___kpm_next_10)); }
	inline MulticastDelegate_t69367374 * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t69367374 ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t69367374 * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T69367374_H
#ifndef TYPE_T_H
#define TYPE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Type
struct  Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_t637624852  ____impl_1;

public:
	inline static int32_t get_offset_of__impl_1() { return static_cast<int32_t>(offsetof(Type_t, ____impl_1)); }
	inline RuntimeTypeHandle_t637624852  get__impl_1() const { return ____impl_1; }
	inline RuntimeTypeHandle_t637624852 * get_address_of__impl_1() { return &____impl_1; }
	inline void set__impl_1(RuntimeTypeHandle_t637624852  value)
	{
		____impl_1 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_2;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t1830665827* ___EmptyTypes_3;
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t1197335030 * ___FilterAttribute_4;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t1197335030 * ___FilterName_5;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t1197335030 * ___FilterNameIgnoreCase_6;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_7;

public:
	inline static int32_t get_offset_of_Delimiter_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_2)); }
	inline Il2CppChar get_Delimiter_2() const { return ___Delimiter_2; }
	inline Il2CppChar* get_address_of_Delimiter_2() { return &___Delimiter_2; }
	inline void set_Delimiter_2(Il2CppChar value)
	{
		___Delimiter_2 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_3)); }
	inline TypeU5BU5D_t1830665827* get_EmptyTypes_3() const { return ___EmptyTypes_3; }
	inline TypeU5BU5D_t1830665827** get_address_of_EmptyTypes_3() { return &___EmptyTypes_3; }
	inline void set_EmptyTypes_3(TypeU5BU5D_t1830665827* value)
	{
		___EmptyTypes_3 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyTypes_3), value);
	}

	inline static int32_t get_offset_of_FilterAttribute_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_4)); }
	inline MemberFilter_t1197335030 * get_FilterAttribute_4() const { return ___FilterAttribute_4; }
	inline MemberFilter_t1197335030 ** get_address_of_FilterAttribute_4() { return &___FilterAttribute_4; }
	inline void set_FilterAttribute_4(MemberFilter_t1197335030 * value)
	{
		___FilterAttribute_4 = value;
		Il2CppCodeGenWriteBarrier((&___FilterAttribute_4), value);
	}

	inline static int32_t get_offset_of_FilterName_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_5)); }
	inline MemberFilter_t1197335030 * get_FilterName_5() const { return ___FilterName_5; }
	inline MemberFilter_t1197335030 ** get_address_of_FilterName_5() { return &___FilterName_5; }
	inline void set_FilterName_5(MemberFilter_t1197335030 * value)
	{
		___FilterName_5 = value;
		Il2CppCodeGenWriteBarrier((&___FilterName_5), value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_6)); }
	inline MemberFilter_t1197335030 * get_FilterNameIgnoreCase_6() const { return ___FilterNameIgnoreCase_6; }
	inline MemberFilter_t1197335030 ** get_address_of_FilterNameIgnoreCase_6() { return &___FilterNameIgnoreCase_6; }
	inline void set_FilterNameIgnoreCase_6(MemberFilter_t1197335030 * value)
	{
		___FilterNameIgnoreCase_6 = value;
		Il2CppCodeGenWriteBarrier((&___FilterNameIgnoreCase_6), value);
	}

	inline static int32_t get_offset_of_Missing_7() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_7)); }
	inline RuntimeObject * get_Missing_7() const { return ___Missing_7; }
	inline RuntimeObject ** get_address_of_Missing_7() { return &___Missing_7; }
	inline void set_Missing_7(RuntimeObject * value)
	{
		___Missing_7 = value;
		Il2CppCodeGenWriteBarrier((&___Missing_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T_H
#ifndef OBJECTDISPOSEDEXCEPTION_T363085662_H
#define OBJECTDISPOSEDEXCEPTION_T363085662_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ObjectDisposedException
struct  ObjectDisposedException_t363085662  : public InvalidOperationException_t1956282378
{
public:
	// System.String System.ObjectDisposedException::obj_name
	String_t* ___obj_name_12;
	// System.String System.ObjectDisposedException::msg
	String_t* ___msg_13;

public:
	inline static int32_t get_offset_of_obj_name_12() { return static_cast<int32_t>(offsetof(ObjectDisposedException_t363085662, ___obj_name_12)); }
	inline String_t* get_obj_name_12() const { return ___obj_name_12; }
	inline String_t** get_address_of_obj_name_12() { return &___obj_name_12; }
	inline void set_obj_name_12(String_t* value)
	{
		___obj_name_12 = value;
		Il2CppCodeGenWriteBarrier((&___obj_name_12), value);
	}

	inline static int32_t get_offset_of_msg_13() { return static_cast<int32_t>(offsetof(ObjectDisposedException_t363085662, ___msg_13)); }
	inline String_t* get_msg_13() const { return ___msg_13; }
	inline String_t** get_address_of_msg_13() { return &___msg_13; }
	inline void set_msg_13(String_t* value)
	{
		___msg_13 = value;
		Il2CppCodeGenWriteBarrier((&___msg_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTDISPOSEDEXCEPTION_T363085662_H
#ifndef ARGUMENTNULLEXCEPTION_T517973450_H
#define ARGUMENTNULLEXCEPTION_T517973450_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ArgumentNullException
struct  ArgumentNullException_t517973450  : public ArgumentException_t1372035057
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARGUMENTNULLEXCEPTION_T517973450_H
#ifndef ARGUMENTOUTOFRANGEEXCEPTION_T1745463579_H
#define ARGUMENTOUTOFRANGEEXCEPTION_T1745463579_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ArgumentOutOfRangeException
struct  ArgumentOutOfRangeException_t1745463579  : public ArgumentException_t1372035057
{
public:
	// System.Object System.ArgumentOutOfRangeException::actual_value
	RuntimeObject * ___actual_value_13;

public:
	inline static int32_t get_offset_of_actual_value_13() { return static_cast<int32_t>(offsetof(ArgumentOutOfRangeException_t1745463579, ___actual_value_13)); }
	inline RuntimeObject * get_actual_value_13() const { return ___actual_value_13; }
	inline RuntimeObject ** get_address_of_actual_value_13() { return &___actual_value_13; }
	inline void set_actual_value_13(RuntimeObject * value)
	{
		___actual_value_13 = value;
		Il2CppCodeGenWriteBarrier((&___actual_value_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARGUMENTOUTOFRANGEEXCEPTION_T1745463579_H
#ifndef ENUMERATOR_T64113712_H
#define ENUMERATOR_T64113712_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>
struct  Enumerator_t64113712 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t2599265951 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	CustomAttributeNamedArgument_t425730339  ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_t64113712, ___l_0)); }
	inline List_1_t2599265951 * get_l_0() const { return ___l_0; }
	inline List_1_t2599265951 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t2599265951 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t64113712, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t64113712, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t64113712, ___current_3)); }
	inline CustomAttributeNamedArgument_t425730339  get_current_3() const { return ___current_3; }
	inline CustomAttributeNamedArgument_t425730339 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(CustomAttributeNamedArgument_t425730339  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T64113712_H
#ifndef FUNC_2_T2705400122_H
#define FUNC_2_T2705400122_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Func`2<System.Object,System.Object>
struct  Func_2_t2705400122  : public MulticastDelegate_t69367374
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FUNC_2_T2705400122_H
#ifndef CONVERTER_2_T4184931014_H
#define CONVERTER_2_T4184931014_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Converter`2<System.Object,System.Object>
struct  Converter_2_t4184931014  : public MulticastDelegate_t69367374
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONVERTER_2_T4184931014_H
#ifndef ASYNCCALLBACK_T1046330627_H
#define ASYNCCALLBACK_T1046330627_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.AsyncCallback
struct  AsyncCallback_t1046330627  : public MulticastDelegate_t69367374
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCCALLBACK_T1046330627_H
#ifndef COMPARISON_1_T1028579740_H
#define COMPARISON_1_T1028579740_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Comparison`1<System.Object>
struct  Comparison_1_t1028579740  : public MulticastDelegate_t69367374
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPARISON_1_T1028579740_H
#ifndef FUNC_2_T1469056018_H
#define FUNC_2_T1469056018_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Func`2<System.Object,System.Boolean>
struct  Func_2_t1469056018  : public MulticastDelegate_t69367374
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FUNC_2_T1469056018_H
// System.String[]
struct StringU5BU5D_t522578821  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) String_t* m_Items[1];

public:
	inline String_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline String_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, String_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline String_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline String_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, String_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Int32[]
struct Int32U5BU5D_t2856113350  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int32_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int32_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int32_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// System.Object[]
struct ObjectU5BU5D_t4290686858  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject * m_Items[1];

public:
	inline RuntimeObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline RuntimeObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Reflection.CustomAttributeNamedArgument[]
struct CustomAttributeNamedArgumentU5BU5D_t3127017202  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) CustomAttributeNamedArgument_t425730339  m_Items[1];

public:
	inline CustomAttributeNamedArgument_t425730339  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline CustomAttributeNamedArgument_t425730339 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, CustomAttributeNamedArgument_t425730339  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline CustomAttributeNamedArgument_t425730339  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline CustomAttributeNamedArgument_t425730339 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, CustomAttributeNamedArgument_t425730339  value)
	{
		m_Items[index] = value;
	}
};
// System.Reflection.CustomAttributeTypedArgument[]
struct CustomAttributeTypedArgumentU5BU5D_t2668173852  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) CustomAttributeTypedArgument_t2667702721  m_Items[1];

public:
	inline CustomAttributeTypedArgument_t2667702721  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline CustomAttributeTypedArgument_t2667702721 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, CustomAttributeTypedArgument_t2667702721  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline CustomAttributeTypedArgument_t2667702721  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline CustomAttributeTypedArgument_t2667702721 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, CustomAttributeTypedArgument_t2667702721  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.UnitySynchronizationContext/WorkRequest[]
struct WorkRequestU5BU5D_t3723184674  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) WorkRequest_t2547429811  m_Items[1];

public:
	inline WorkRequest_t2547429811  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline WorkRequest_t2547429811 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, WorkRequest_t2547429811  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline WorkRequest_t2547429811  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline WorkRequest_t2547429811 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, WorkRequest_t2547429811  value)
	{
		m_Items[index] = value;
	}
};


// System.Void System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Object>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m212314078_gshared (KeyValuePair_2_t2204764377 * __this, intptr_t p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Object>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m608210558_gshared (KeyValuePair_2_t2204764377 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m1805377863_gshared (KeyValuePair_2_t2204764377 * __this, intptr_t ___key0, RuntimeObject * ___value1, const RuntimeMethod* method);
// TKey System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Object>::get_Key()
extern "C"  intptr_t KeyValuePair_2_get_Key_m646124322_gshared (KeyValuePair_2_t2204764377 * __this, const RuntimeMethod* method);
// TValue System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Object>::get_Value()
extern "C"  RuntimeObject * KeyValuePair_2_get_Value_m2473668865_gshared (KeyValuePair_2_t2204764377 * __this, const RuntimeMethod* method);
// System.String System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Object>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m272197277_gshared (KeyValuePair_2_t2204764377 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m4110759363_gshared (KeyValuePair_2_t2366093889 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m3810493383_gshared (KeyValuePair_2_t2366093889 * __this, bool p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m346367360_gshared (KeyValuePair_2_t2366093889 * __this, RuntimeObject * ___key0, bool ___value1, const RuntimeMethod* method);
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::get_Key()
extern "C"  RuntimeObject * KeyValuePair_2_get_Key_m1494729318_gshared (KeyValuePair_2_t2366093889 * __this, const RuntimeMethod* method);
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::get_Value()
extern "C"  bool KeyValuePair_2_get_Value_m682268374_gshared (KeyValuePair_2_t2366093889 * __this, const RuntimeMethod* method);
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m2957839857_gshared (KeyValuePair_2_t2366093889 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m1817844414_gshared (KeyValuePair_2_t2877556189 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m605429017_gshared (KeyValuePair_2_t2877556189 * __this, int32_t p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m3988816228_gshared (KeyValuePair_2_t2877556189 * __this, RuntimeObject * ___key0, int32_t ___value1, const RuntimeMethod* method);
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::get_Key()
extern "C"  RuntimeObject * KeyValuePair_2_get_Key_m1080276696_gshared (KeyValuePair_2_t2877556189 * __this, const RuntimeMethod* method);
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::get_Value()
extern "C"  int32_t KeyValuePair_2_get_Value_m3213725153_gshared (KeyValuePair_2_t2877556189 * __this, const RuntimeMethod* method);
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m3938306873_gshared (KeyValuePair_2_t2877556189 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m2678256174_gshared (KeyValuePair_2_t3602437993 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m1656636255_gshared (KeyValuePair_2_t3602437993 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m2416117444_gshared (KeyValuePair_2_t3602437993 * __this, RuntimeObject * ___key0, RuntimeObject * ___value1, const RuntimeMethod* method);
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::get_Key()
extern "C"  RuntimeObject * KeyValuePair_2_get_Key_m2242947300_gshared (KeyValuePair_2_t3602437993 * __this, const RuntimeMethod* method);
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::get_Value()
extern "C"  RuntimeObject * KeyValuePair_2_get_Value_m1442051799_gshared (KeyValuePair_2_t3602437993 * __this, const RuntimeMethod* method);
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m1421527622_gshared (KeyValuePair_2_t3602437993 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m223128127_gshared (Enumerator_t2209518572 * __this, List_1_t449703515 * ___l0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::VerifyState()
extern "C"  void Enumerator_VerifyState_m2300229768_gshared (Enumerator_t2209518572 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2247808877_gshared (Enumerator_t2209518572 * __this, const RuntimeMethod* method);
// System.Object System.Collections.Generic.List`1/Enumerator<System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * Enumerator_System_Collections_IEnumerator_get_Current_m1371828734_gshared (Enumerator_t2209518572 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::Dispose()
extern "C"  void Enumerator_Dispose_m3280506413_gshared (Enumerator_t2209518572 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Int32>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1421551000_gshared (Enumerator_t2209518572 * __this, const RuntimeMethod* method);
// T System.Collections.Generic.List`1/Enumerator<System.Int32>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m2531165950_gshared (Enumerator_t2209518572 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m3625216372_gshared (Enumerator_t2934400376 * __this, List_1_t1174585319 * ___l0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::VerifyState()
extern "C"  void Enumerator_VerifyState_m2889154_gshared (Enumerator_t2934400376 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m4103439166_gshared (Enumerator_t2934400376 * __this, const RuntimeMethod* method);
// System.Object System.Collections.Generic.List`1/Enumerator<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * Enumerator_System_Collections_IEnumerator_get_Current_m1417958798_gshared (Enumerator_t2934400376 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m1212514242_gshared (Enumerator_t2934400376 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3640007406_gshared (Enumerator_t2934400376 * __this, const RuntimeMethod* method);
// T System.Collections.Generic.List`1/Enumerator<System.Object>::get_Current()
extern "C"  RuntimeObject * Enumerator_get_Current_m3814319504_gshared (Enumerator_t2934400376 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m2449765543_gshared (Enumerator_t64113712 * __this, List_1_t2599265951 * ___l0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::VerifyState()
extern "C"  void Enumerator_VerifyState_m1870543230_gshared (Enumerator_t64113712 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m854669720_gshared (Enumerator_t64113712 * __this, const RuntimeMethod* method);
// System.Object System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * Enumerator_System_Collections_IEnumerator_get_Current_m4080730988_gshared (Enumerator_t64113712 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::Dispose()
extern "C"  void Enumerator_Dispose_m858809926_gshared (Enumerator_t64113712 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3058718994_gshared (Enumerator_t64113712 * __this, const RuntimeMethod* method);
// T System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::get_Current()
extern "C"  CustomAttributeNamedArgument_t425730339  Enumerator_get_Current_m3125146575_gshared (Enumerator_t64113712 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m2446290647_gshared (Enumerator_t2306086094 * __this, List_1_t546271037 * ___l0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::VerifyState()
extern "C"  void Enumerator_VerifyState_m827611239_gshared (Enumerator_t2306086094 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m4223785200_gshared (Enumerator_t2306086094 * __this, const RuntimeMethod* method);
// System.Object System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * Enumerator_System_Collections_IEnumerator_get_Current_m2003157540_gshared (Enumerator_t2306086094 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::Dispose()
extern "C"  void Enumerator_Dispose_m1763116863_gshared (Enumerator_t2306086094 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2664888314_gshared (Enumerator_t2306086094 * __this, const RuntimeMethod* method);
// T System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::get_Current()
extern "C"  CustomAttributeTypedArgument_t2667702721  Enumerator_get_Current_m1772309331_gshared (Enumerator_t2306086094 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Queue`1/Enumerator<System.Object>::.ctor(System.Collections.Generic.Queue`1<T>)
extern "C"  void Enumerator__ctor_m1020038761_gshared (Enumerator_t4228099087 * __this, Queue_1_t2668810036 * ___q0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Queue`1/Enumerator<System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2386365462_gshared (Enumerator_t4228099087 * __this, const RuntimeMethod* method);
// T System.Collections.Generic.Queue`1/Enumerator<System.Object>::get_Current()
extern "C"  RuntimeObject * Enumerator_get_Current_m373730718_gshared (Enumerator_t4228099087 * __this, const RuntimeMethod* method);
// System.Object System.Collections.Generic.Queue`1/Enumerator<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * Enumerator_System_Collections_IEnumerator_get_Current_m4200670532_gshared (Enumerator_t4228099087 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Queue`1/Enumerator<System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m937788283_gshared (Enumerator_t4228099087 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Queue`1/Enumerator<System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m726149558_gshared (Enumerator_t4228099087 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Queue`1/Enumerator<UnityEngine.UnitySynchronizationContext/WorkRequest>::.ctor(System.Collections.Generic.Queue`1<T>)
extern "C"  void Enumerator__ctor_m2190717051_gshared (Enumerator_t3479511895 * __this, Queue_1_t1920222844 * ___q0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Queue`1/Enumerator<UnityEngine.UnitySynchronizationContext/WorkRequest>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2717756568_gshared (Enumerator_t3479511895 * __this, const RuntimeMethod* method);
// T System.Collections.Generic.Queue`1/Enumerator<UnityEngine.UnitySynchronizationContext/WorkRequest>::get_Current()
extern "C"  WorkRequest_t2547429811  Enumerator_get_Current_m1195414335_gshared (Enumerator_t3479511895 * __this, const RuntimeMethod* method);
// System.Object System.Collections.Generic.Queue`1/Enumerator<UnityEngine.UnitySynchronizationContext/WorkRequest>::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * Enumerator_System_Collections_IEnumerator_get_Current_m3652643606_gshared (Enumerator_t3479511895 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Queue`1/Enumerator<UnityEngine.UnitySynchronizationContext/WorkRequest>::Dispose()
extern "C"  void Enumerator_Dispose_m3437219742_gshared (Enumerator_t3479511895 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Queue`1/Enumerator<UnityEngine.UnitySynchronizationContext/WorkRequest>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2652681985_gshared (Enumerator_t3479511895 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Stack`1/Enumerator<System.Object>::.ctor(System.Collections.Generic.Stack`1<T>)
extern "C"  void Enumerator__ctor_m1549060886_gshared (Enumerator_t1041909981 * __this, Stack_1_t3886787695 * ___t0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Stack`1/Enumerator<System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m4168111423_gshared (Enumerator_t1041909981 * __this, const RuntimeMethod* method);
// T System.Collections.Generic.Stack`1/Enumerator<System.Object>::get_Current()
extern "C"  RuntimeObject * Enumerator_get_Current_m887231645_gshared (Enumerator_t1041909981 * __this, const RuntimeMethod* method);
// System.Object System.Collections.Generic.Stack`1/Enumerator<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * Enumerator_System_Collections_IEnumerator_get_Current_m3744667164_gshared (Enumerator_t1041909981 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Stack`1/Enumerator<System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m2724312910_gshared (Enumerator_t1041909981 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Stack`1/Enumerator<System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2828987763_gshared (Enumerator_t1041909981 * __this, const RuntimeMethod* method);
// System.Int32 System.Comparison`1<System.Object>::Invoke(T,T)
extern "C"  int32_t Comparison_1_Invoke_m727370941_gshared (Comparison_1_t1028579740 * __this, RuntimeObject * ___x0, RuntimeObject * ___y1, const RuntimeMethod* method);
// TOutput System.Converter`2<System.Object,System.Object>::Invoke(TInput)
extern "C"  RuntimeObject * Converter_2_Invoke_m303716893_gshared (Converter_2_t4184931014 * __this, RuntimeObject * ___input0, const RuntimeMethod* method);
// TResult System.Func`2<System.Object,System.Boolean>::Invoke(T)
extern "C"  bool Func_2_Invoke_m2527071059_gshared (Func_2_t1469056018 * __this, RuntimeObject * ___arg10, const RuntimeMethod* method);
// TResult System.Func`2<System.Object,System.Object>::Invoke(T)
extern "C"  RuntimeObject * Func_2_Invoke_m4186273240_gshared (Func_2_t2705400122 * __this, RuntimeObject * ___arg10, const RuntimeMethod* method);

// System.Void System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Object>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m212314078(__this, p0, method) ((  void (*) (KeyValuePair_2_t2204764377 *, intptr_t, const RuntimeMethod*))KeyValuePair_2_set_Key_m212314078_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Object>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m608210558(__this, p0, method) ((  void (*) (KeyValuePair_2_t2204764377 *, RuntimeObject *, const RuntimeMethod*))KeyValuePair_2_set_Value_m608210558_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Object>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m1805377863(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t2204764377 *, intptr_t, RuntimeObject *, const RuntimeMethod*))KeyValuePair_2__ctor_m1805377863_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Object>::get_Key()
#define KeyValuePair_2_get_Key_m646124322(__this, method) ((  intptr_t (*) (KeyValuePair_2_t2204764377 *, const RuntimeMethod*))KeyValuePair_2_get_Key_m646124322_gshared)(__this, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Object>::get_Value()
#define KeyValuePair_2_get_Value_m2473668865(__this, method) ((  RuntimeObject * (*) (KeyValuePair_2_t2204764377 *, const RuntimeMethod*))KeyValuePair_2_get_Value_m2473668865_gshared)(__this, method)
// System.String System.IntPtr::ToString()
extern "C"  String_t* IntPtr_ToString_m2819219677 (intptr_t* __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.String[])
extern "C"  String_t* String_Concat_m2040094846 (RuntimeObject * __this /* static, unused */, StringU5BU5D_t522578821* ___values0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Object>::ToString()
#define KeyValuePair_2_ToString_m272197277(__this, method) ((  String_t* (*) (KeyValuePair_2_t2204764377 *, const RuntimeMethod*))KeyValuePair_2_ToString_m272197277_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m4110759363(__this, p0, method) ((  void (*) (KeyValuePair_2_t2366093889 *, RuntimeObject *, const RuntimeMethod*))KeyValuePair_2_set_Key_m4110759363_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m3810493383(__this, p0, method) ((  void (*) (KeyValuePair_2_t2366093889 *, bool, const RuntimeMethod*))KeyValuePair_2_set_Value_m3810493383_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m346367360(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t2366093889 *, RuntimeObject *, bool, const RuntimeMethod*))KeyValuePair_2__ctor_m346367360_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::get_Key()
#define KeyValuePair_2_get_Key_m1494729318(__this, method) ((  RuntimeObject * (*) (KeyValuePair_2_t2366093889 *, const RuntimeMethod*))KeyValuePair_2_get_Key_m1494729318_gshared)(__this, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::get_Value()
#define KeyValuePair_2_get_Value_m682268374(__this, method) ((  bool (*) (KeyValuePair_2_t2366093889 *, const RuntimeMethod*))KeyValuePair_2_get_Value_m682268374_gshared)(__this, method)
// System.String System.Boolean::ToString()
extern "C"  String_t* Boolean_ToString_m3488188482 (bool* __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::ToString()
#define KeyValuePair_2_ToString_m2957839857(__this, method) ((  String_t* (*) (KeyValuePair_2_t2366093889 *, const RuntimeMethod*))KeyValuePair_2_ToString_m2957839857_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m1817844414(__this, p0, method) ((  void (*) (KeyValuePair_2_t2877556189 *, RuntimeObject *, const RuntimeMethod*))KeyValuePair_2_set_Key_m1817844414_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m605429017(__this, p0, method) ((  void (*) (KeyValuePair_2_t2877556189 *, int32_t, const RuntimeMethod*))KeyValuePair_2_set_Value_m605429017_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m3988816228(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t2877556189 *, RuntimeObject *, int32_t, const RuntimeMethod*))KeyValuePair_2__ctor_m3988816228_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::get_Key()
#define KeyValuePair_2_get_Key_m1080276696(__this, method) ((  RuntimeObject * (*) (KeyValuePair_2_t2877556189 *, const RuntimeMethod*))KeyValuePair_2_get_Key_m1080276696_gshared)(__this, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::get_Value()
#define KeyValuePair_2_get_Value_m3213725153(__this, method) ((  int32_t (*) (KeyValuePair_2_t2877556189 *, const RuntimeMethod*))KeyValuePair_2_get_Value_m3213725153_gshared)(__this, method)
// System.String System.Int32::ToString()
extern "C"  String_t* Int32_ToString_m942907109 (int32_t* __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::ToString()
#define KeyValuePair_2_ToString_m3938306873(__this, method) ((  String_t* (*) (KeyValuePair_2_t2877556189 *, const RuntimeMethod*))KeyValuePair_2_ToString_m3938306873_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m2678256174(__this, p0, method) ((  void (*) (KeyValuePair_2_t3602437993 *, RuntimeObject *, const RuntimeMethod*))KeyValuePair_2_set_Key_m2678256174_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m1656636255(__this, p0, method) ((  void (*) (KeyValuePair_2_t3602437993 *, RuntimeObject *, const RuntimeMethod*))KeyValuePair_2_set_Value_m1656636255_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m2416117444(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t3602437993 *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*))KeyValuePair_2__ctor_m2416117444_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::get_Key()
#define KeyValuePair_2_get_Key_m2242947300(__this, method) ((  RuntimeObject * (*) (KeyValuePair_2_t3602437993 *, const RuntimeMethod*))KeyValuePair_2_get_Key_m2242947300_gshared)(__this, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::get_Value()
#define KeyValuePair_2_get_Value_m1442051799(__this, method) ((  RuntimeObject * (*) (KeyValuePair_2_t3602437993 *, const RuntimeMethod*))KeyValuePair_2_get_Value_m1442051799_gshared)(__this, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::ToString()
#define KeyValuePair_2_ToString_m1421527622(__this, method) ((  String_t* (*) (KeyValuePair_2_t3602437993 *, const RuntimeMethod*))KeyValuePair_2_ToString_m1421527622_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m223128127(__this, ___l0, method) ((  void (*) (Enumerator_t2209518572 *, List_1_t449703515 *, const RuntimeMethod*))Enumerator__ctor_m223128127_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::VerifyState()
#define Enumerator_VerifyState_m2300229768(__this, method) ((  void (*) (Enumerator_t2209518572 *, const RuntimeMethod*))Enumerator_VerifyState_m2300229768_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2247808877(__this, method) ((  void (*) (Enumerator_t2209518572 *, const RuntimeMethod*))Enumerator_System_Collections_IEnumerator_Reset_m2247808877_gshared)(__this, method)
// System.Void System.InvalidOperationException::.ctor()
extern "C"  void InvalidOperationException__ctor_m3074049270 (InvalidOperationException_t1956282378 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Generic.List`1/Enumerator<System.Int32>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1371828734(__this, method) ((  RuntimeObject * (*) (Enumerator_t2209518572 *, const RuntimeMethod*))Enumerator_System_Collections_IEnumerator_get_Current_m1371828734_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::Dispose()
#define Enumerator_Dispose_m3280506413(__this, method) ((  void (*) (Enumerator_t2209518572 *, const RuntimeMethod*))Enumerator_Dispose_m3280506413_gshared)(__this, method)
// System.Type System.Object::GetType()
extern "C"  Type_t * Object_GetType_m4087638284 (RuntimeObject * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.ObjectDisposedException::.ctor(System.String)
extern "C"  void ObjectDisposedException__ctor_m781126761 (ObjectDisposedException_t363085662 * __this, String_t* ___objectName0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.InvalidOperationException::.ctor(System.String)
extern "C"  void InvalidOperationException__ctor_m1754939824 (InvalidOperationException_t1956282378 * __this, String_t* ___message0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Int32>::MoveNext()
#define Enumerator_MoveNext_m1421551000(__this, method) ((  bool (*) (Enumerator_t2209518572 *, const RuntimeMethod*))Enumerator_MoveNext_m1421551000_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<System.Int32>::get_Current()
#define Enumerator_get_Current_m2531165950(__this, method) ((  int32_t (*) (Enumerator_t2209518572 *, const RuntimeMethod*))Enumerator_get_Current_m2531165950_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m3625216372(__this, ___l0, method) ((  void (*) (Enumerator_t2934400376 *, List_1_t1174585319 *, const RuntimeMethod*))Enumerator__ctor_m3625216372_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::VerifyState()
#define Enumerator_VerifyState_m2889154(__this, method) ((  void (*) (Enumerator_t2934400376 *, const RuntimeMethod*))Enumerator_VerifyState_m2889154_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m4103439166(__this, method) ((  void (*) (Enumerator_t2934400376 *, const RuntimeMethod*))Enumerator_System_Collections_IEnumerator_Reset_m4103439166_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<System.Object>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1417958798(__this, method) ((  RuntimeObject * (*) (Enumerator_t2934400376 *, const RuntimeMethod*))Enumerator_System_Collections_IEnumerator_get_Current_m1417958798_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::Dispose()
#define Enumerator_Dispose_m1212514242(__this, method) ((  void (*) (Enumerator_t2934400376 *, const RuntimeMethod*))Enumerator_Dispose_m1212514242_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Object>::MoveNext()
#define Enumerator_MoveNext_m3640007406(__this, method) ((  bool (*) (Enumerator_t2934400376 *, const RuntimeMethod*))Enumerator_MoveNext_m3640007406_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<System.Object>::get_Current()
#define Enumerator_get_Current_m3814319504(__this, method) ((  RuntimeObject * (*) (Enumerator_t2934400376 *, const RuntimeMethod*))Enumerator_get_Current_m3814319504_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m2449765543(__this, ___l0, method) ((  void (*) (Enumerator_t64113712 *, List_1_t2599265951 *, const RuntimeMethod*))Enumerator__ctor_m2449765543_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::VerifyState()
#define Enumerator_VerifyState_m1870543230(__this, method) ((  void (*) (Enumerator_t64113712 *, const RuntimeMethod*))Enumerator_VerifyState_m1870543230_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m854669720(__this, method) ((  void (*) (Enumerator_t64113712 *, const RuntimeMethod*))Enumerator_System_Collections_IEnumerator_Reset_m854669720_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m4080730988(__this, method) ((  RuntimeObject * (*) (Enumerator_t64113712 *, const RuntimeMethod*))Enumerator_System_Collections_IEnumerator_get_Current_m4080730988_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::Dispose()
#define Enumerator_Dispose_m858809926(__this, method) ((  void (*) (Enumerator_t64113712 *, const RuntimeMethod*))Enumerator_Dispose_m858809926_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::MoveNext()
#define Enumerator_MoveNext_m3058718994(__this, method) ((  bool (*) (Enumerator_t64113712 *, const RuntimeMethod*))Enumerator_MoveNext_m3058718994_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::get_Current()
#define Enumerator_get_Current_m3125146575(__this, method) ((  CustomAttributeNamedArgument_t425730339  (*) (Enumerator_t64113712 *, const RuntimeMethod*))Enumerator_get_Current_m3125146575_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m2446290647(__this, ___l0, method) ((  void (*) (Enumerator_t2306086094 *, List_1_t546271037 *, const RuntimeMethod*))Enumerator__ctor_m2446290647_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::VerifyState()
#define Enumerator_VerifyState_m827611239(__this, method) ((  void (*) (Enumerator_t2306086094 *, const RuntimeMethod*))Enumerator_VerifyState_m827611239_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m4223785200(__this, method) ((  void (*) (Enumerator_t2306086094 *, const RuntimeMethod*))Enumerator_System_Collections_IEnumerator_Reset_m4223785200_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2003157540(__this, method) ((  RuntimeObject * (*) (Enumerator_t2306086094 *, const RuntimeMethod*))Enumerator_System_Collections_IEnumerator_get_Current_m2003157540_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::Dispose()
#define Enumerator_Dispose_m1763116863(__this, method) ((  void (*) (Enumerator_t2306086094 *, const RuntimeMethod*))Enumerator_Dispose_m1763116863_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::MoveNext()
#define Enumerator_MoveNext_m2664888314(__this, method) ((  bool (*) (Enumerator_t2306086094 *, const RuntimeMethod*))Enumerator_MoveNext_m2664888314_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::get_Current()
#define Enumerator_get_Current_m1772309331(__this, method) ((  CustomAttributeTypedArgument_t2667702721  (*) (Enumerator_t2306086094 *, const RuntimeMethod*))Enumerator_get_Current_m1772309331_gshared)(__this, method)
// System.Void System.Object::.ctor()
extern "C"  void Object__ctor_m3446193457 (RuntimeObject * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.ArgumentOutOfRangeException::.ctor(System.String)
extern "C"  void ArgumentOutOfRangeException__ctor_m2829693980 (ArgumentOutOfRangeException_t1745463579 * __this, String_t* ___paramName0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array::Copy(System.Array,System.Int32,System.Array,System.Int32,System.Int32)
extern "C"  void Array_Copy_m1287631566 (RuntimeObject * __this /* static, unused */, RuntimeArray * ___sourceArray0, int32_t ___sourceIndex1, RuntimeArray * ___destinationArray2, int32_t ___destinationIndex3, int32_t ___length4, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.ArgumentException::.ctor(System.String)
extern "C"  void ArgumentException__ctor_m775170528 (ArgumentException_t1372035057 * __this, String_t* ___message0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Math::Max(System.Int32,System.Int32)
extern "C"  int32_t Math_Max_m3051295998 (RuntimeObject * __this /* static, unused */, int32_t ___val10, int32_t ___val21, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array::Clear(System.Array,System.Int32,System.Int32)
extern "C"  void Array_Clear_m305290227 (RuntimeObject * __this /* static, unused */, RuntimeArray * ___array0, int32_t ___index1, int32_t ___length2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.ArgumentNullException::.ctor(System.String)
extern "C"  void ArgumentNullException__ctor_m2607242403 (ArgumentNullException_t517973450 * __this, String_t* ___paramName0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array::Copy(System.Array,System.Array,System.Int32)
extern "C"  void Array_Copy_m392395275 (RuntimeObject * __this /* static, unused */, RuntimeArray * ___sourceArray0, RuntimeArray * ___destinationArray1, int32_t ___length2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.ArgumentOutOfRangeException::.ctor()
extern "C"  void ArgumentOutOfRangeException__ctor_m3684908528 (ArgumentOutOfRangeException_t1745463579 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Queue`1/Enumerator<System.Object>::.ctor(System.Collections.Generic.Queue`1<T>)
#define Enumerator__ctor_m1020038761(__this, ___q0, method) ((  void (*) (Enumerator_t4228099087 *, Queue_1_t2668810036 *, const RuntimeMethod*))Enumerator__ctor_m1020038761_gshared)(__this, ___q0, method)
// System.Void System.Collections.Generic.Queue`1/Enumerator<System.Object>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2386365462(__this, method) ((  void (*) (Enumerator_t4228099087 *, const RuntimeMethod*))Enumerator_System_Collections_IEnumerator_Reset_m2386365462_gshared)(__this, method)
// T System.Collections.Generic.Queue`1/Enumerator<System.Object>::get_Current()
#define Enumerator_get_Current_m373730718(__this, method) ((  RuntimeObject * (*) (Enumerator_t4228099087 *, const RuntimeMethod*))Enumerator_get_Current_m373730718_gshared)(__this, method)
// System.Object System.Collections.Generic.Queue`1/Enumerator<System.Object>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m4200670532(__this, method) ((  RuntimeObject * (*) (Enumerator_t4228099087 *, const RuntimeMethod*))Enumerator_System_Collections_IEnumerator_get_Current_m4200670532_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1/Enumerator<System.Object>::Dispose()
#define Enumerator_Dispose_m937788283(__this, method) ((  void (*) (Enumerator_t4228099087 *, const RuntimeMethod*))Enumerator_Dispose_m937788283_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Queue`1/Enumerator<System.Object>::MoveNext()
#define Enumerator_MoveNext_m726149558(__this, method) ((  bool (*) (Enumerator_t4228099087 *, const RuntimeMethod*))Enumerator_MoveNext_m726149558_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1/Enumerator<UnityEngine.UnitySynchronizationContext/WorkRequest>::.ctor(System.Collections.Generic.Queue`1<T>)
#define Enumerator__ctor_m2190717051(__this, ___q0, method) ((  void (*) (Enumerator_t3479511895 *, Queue_1_t1920222844 *, const RuntimeMethod*))Enumerator__ctor_m2190717051_gshared)(__this, ___q0, method)
// System.Void System.Collections.Generic.Queue`1/Enumerator<UnityEngine.UnitySynchronizationContext/WorkRequest>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2717756568(__this, method) ((  void (*) (Enumerator_t3479511895 *, const RuntimeMethod*))Enumerator_System_Collections_IEnumerator_Reset_m2717756568_gshared)(__this, method)
// T System.Collections.Generic.Queue`1/Enumerator<UnityEngine.UnitySynchronizationContext/WorkRequest>::get_Current()
#define Enumerator_get_Current_m1195414335(__this, method) ((  WorkRequest_t2547429811  (*) (Enumerator_t3479511895 *, const RuntimeMethod*))Enumerator_get_Current_m1195414335_gshared)(__this, method)
// System.Object System.Collections.Generic.Queue`1/Enumerator<UnityEngine.UnitySynchronizationContext/WorkRequest>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3652643606(__this, method) ((  RuntimeObject * (*) (Enumerator_t3479511895 *, const RuntimeMethod*))Enumerator_System_Collections_IEnumerator_get_Current_m3652643606_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1/Enumerator<UnityEngine.UnitySynchronizationContext/WorkRequest>::Dispose()
#define Enumerator_Dispose_m3437219742(__this, method) ((  void (*) (Enumerator_t3479511895 *, const RuntimeMethod*))Enumerator_Dispose_m3437219742_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Queue`1/Enumerator<UnityEngine.UnitySynchronizationContext/WorkRequest>::MoveNext()
#define Enumerator_MoveNext_m2652681985(__this, method) ((  bool (*) (Enumerator_t3479511895 *, const RuntimeMethod*))Enumerator_MoveNext_m2652681985_gshared)(__this, method)
// System.Void System.ArgumentNullException::.ctor()
extern "C"  void ArgumentNullException__ctor_m348650132 (ArgumentNullException_t517973450 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Array::get_Length()
extern "C"  int32_t Array_get_Length_m1201869234 (RuntimeArray * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Math::Min(System.Int32,System.Int32)
extern "C"  int32_t Math_Min_m1276480861 (RuntimeObject * __this /* static, unused */, int32_t p0, int32_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.ArgumentException::.ctor()
extern "C"  void ArgumentException__ctor_m996376509 (ArgumentException_t1372035057 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Stack`1/Enumerator<System.Object>::.ctor(System.Collections.Generic.Stack`1<T>)
#define Enumerator__ctor_m1549060886(__this, ___t0, method) ((  void (*) (Enumerator_t1041909981 *, Stack_1_t3886787695 *, const RuntimeMethod*))Enumerator__ctor_m1549060886_gshared)(__this, ___t0, method)
// System.Void System.Collections.Generic.Stack`1/Enumerator<System.Object>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m4168111423(__this, method) ((  void (*) (Enumerator_t1041909981 *, const RuntimeMethod*))Enumerator_System_Collections_IEnumerator_Reset_m4168111423_gshared)(__this, method)
// T System.Collections.Generic.Stack`1/Enumerator<System.Object>::get_Current()
#define Enumerator_get_Current_m887231645(__this, method) ((  RuntimeObject * (*) (Enumerator_t1041909981 *, const RuntimeMethod*))Enumerator_get_Current_m887231645_gshared)(__this, method)
// System.Object System.Collections.Generic.Stack`1/Enumerator<System.Object>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3744667164(__this, method) ((  RuntimeObject * (*) (Enumerator_t1041909981 *, const RuntimeMethod*))Enumerator_System_Collections_IEnumerator_get_Current_m3744667164_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1/Enumerator<System.Object>::Dispose()
#define Enumerator_Dispose_m2724312910(__this, method) ((  void (*) (Enumerator_t1041909981 *, const RuntimeMethod*))Enumerator_Dispose_m2724312910_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Stack`1/Enumerator<System.Object>::MoveNext()
#define Enumerator_MoveNext_m2828987763(__this, method) ((  bool (*) (Enumerator_t1041909981 *, const RuntimeMethod*))Enumerator_MoveNext_m2828987763_gshared)(__this, method)
// System.Void System.Array::CopyTo(System.Array,System.Int32)
extern "C"  void Array_CopyTo_m332099161 (RuntimeArray * __this, RuntimeArray * p0, int32_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array::Reverse(System.Array,System.Int32,System.Int32)
extern "C"  void Array_Reverse_m3946279211 (RuntimeObject * __this /* static, unused */, RuntimeArray * p0, int32_t p1, int32_t p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Type System.Type::GetTypeFromHandle(System.RuntimeTypeHandle)
extern "C"  Type_t * Type_GetTypeFromHandle_m3171413529 (RuntimeObject * __this /* static, unused */, RuntimeTypeHandle_t637624852  ___handle0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Type::get_IsValueType()
extern "C"  bool Type_get_IsValueType_m2191848361 (Type_t * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.NotSupportedException::.ctor()
extern "C"  void NotSupportedException__ctor_m2647670222 (NotSupportedException_t404487136 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Comparison`1<System.Object>::Invoke(T,T)
#define Comparison_1_Invoke_m727370941(__this, ___x0, ___y1, method) ((  int32_t (*) (Comparison_1_t1028579740 *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*))Comparison_1_Invoke_m727370941_gshared)(__this, ___x0, ___y1, method)
// TOutput System.Converter`2<System.Object,System.Object>::Invoke(TInput)
#define Converter_2_Invoke_m303716893(__this, ___input0, method) ((  RuntimeObject * (*) (Converter_2_t4184931014 *, RuntimeObject *, const RuntimeMethod*))Converter_2_Invoke_m303716893_gshared)(__this, ___input0, method)
// TResult System.Func`2<System.Object,System.Boolean>::Invoke(T)
#define Func_2_Invoke_m2527071059(__this, ___arg10, method) ((  bool (*) (Func_2_t1469056018 *, RuntimeObject *, const RuntimeMethod*))Func_2_Invoke_m2527071059_gshared)(__this, ___arg10, method)
// TResult System.Func`2<System.Object,System.Object>::Invoke(T)
#define Func_2_Invoke_m4186273240(__this, ___arg10, method) ((  RuntimeObject * (*) (Func_2_t2705400122 *, RuntimeObject *, const RuntimeMethod*))Func_2_Invoke_m4186273240_gshared)(__this, ___arg10, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m1805377863_gshared (KeyValuePair_2_t2204764377 * __this, intptr_t ___key0, RuntimeObject * ___value1, const RuntimeMethod* method)
{
	{
		intptr_t L_0 = ___key0;
		KeyValuePair_2_set_Key_m212314078((KeyValuePair_2_t2204764377 *)__this, (intptr_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		RuntimeObject * L_1 = ___value1;
		KeyValuePair_2_set_Value_m608210558((KeyValuePair_2_t2204764377 *)__this, (RuntimeObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m1805377863_AdjustorThunk (RuntimeObject * __this, intptr_t ___key0, RuntimeObject * ___value1, const RuntimeMethod* method)
{
	KeyValuePair_2_t2204764377 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2204764377 *>(__this + 1);
	KeyValuePair_2__ctor_m1805377863(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Object>::get_Key()
extern "C"  intptr_t KeyValuePair_2_get_Key_m646124322_gshared (KeyValuePair_2_t2204764377 * __this, const RuntimeMethod* method)
{
	{
		intptr_t L_0 = (intptr_t)__this->get_key_0();
		return L_0;
	}
}
extern "C"  intptr_t KeyValuePair_2_get_Key_m646124322_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t2204764377 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2204764377 *>(__this + 1);
	return KeyValuePair_2_get_Key_m646124322(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Object>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m212314078_gshared (KeyValuePair_2_t2204764377 * __this, intptr_t ___value0, const RuntimeMethod* method)
{
	{
		intptr_t L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m212314078_AdjustorThunk (RuntimeObject * __this, intptr_t ___value0, const RuntimeMethod* method)
{
	KeyValuePair_2_t2204764377 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2204764377 *>(__this + 1);
	KeyValuePair_2_set_Key_m212314078(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Object>::get_Value()
extern "C"  RuntimeObject * KeyValuePair_2_get_Value_m2473668865_gshared (KeyValuePair_2_t2204764377 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_value_1();
		return L_0;
	}
}
extern "C"  RuntimeObject * KeyValuePair_2_get_Value_m2473668865_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t2204764377 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2204764377 *>(__this + 1);
	return KeyValuePair_2_get_Value_m2473668865(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Object>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m608210558_gshared (KeyValuePair_2_t2204764377 * __this, RuntimeObject * ___value0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m608210558_AdjustorThunk (RuntimeObject * __this, RuntimeObject * ___value0, const RuntimeMethod* method)
{
	KeyValuePair_2_t2204764377 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2204764377 *>(__this + 1);
	KeyValuePair_2_set_Value_m608210558(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Object>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m272197277_gshared (KeyValuePair_2_t2204764377 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m272197277_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	intptr_t V_0;
	memset(&V_0, 0, sizeof(V_0));
	RuntimeObject * V_1 = NULL;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t522578821* G_B2_1 = NULL;
	StringU5BU5D_t522578821* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t522578821* G_B1_1 = NULL;
	StringU5BU5D_t522578821* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t522578821* G_B3_2 = NULL;
	StringU5BU5D_t522578821* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t522578821* G_B5_1 = NULL;
	StringU5BU5D_t522578821* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t522578821* G_B4_1 = NULL;
	StringU5BU5D_t522578821* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t522578821* G_B6_2 = NULL;
	StringU5BU5D_t522578821* G_B6_3 = NULL;
	{
		StringU5BU5D_t522578821* L_0 = (StringU5BU5D_t522578821*)((StringU5BU5D_t522578821*)SZArrayNew(StringU5BU5D_t522578821_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, _stringLiteral1608275668);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral1608275668);
		StringU5BU5D_t522578821* L_1 = (StringU5BU5D_t522578821*)L_0;
		intptr_t L_2 = KeyValuePair_2_get_Key_m646124322((KeyValuePair_2_t2204764377 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
	}
	{
		intptr_t L_3 = KeyValuePair_2_get_Key_m646124322((KeyValuePair_2_t2204764377 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (intptr_t)L_3;
		String_t* L_4 = IntPtr_ToString_m2819219677((intptr_t*)(&V_0), /*hidden argument*/NULL);
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t522578821* L_6 = (StringU5BU5D_t522578821*)G_B3_3;
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, _stringLiteral2131789870);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral2131789870);
		StringU5BU5D_t522578821* L_7 = (StringU5BU5D_t522578821*)L_6;
		RuntimeObject * L_8 = KeyValuePair_2_get_Value_m2473668865((KeyValuePair_2_t2204764377 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
		if (!L_8)
		{
			G_B5_0 = 3;
			G_B5_1 = L_7;
			G_B5_2 = L_7;
			goto IL_0072;
		}
	}
	{
		RuntimeObject * L_9 = KeyValuePair_2_get_Value_m2473668865((KeyValuePair_2_t2204764377 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (RuntimeObject *)L_9;
		NullCheck((RuntimeObject *)(*(&V_1)));
		String_t* L_10 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (RuntimeObject *)(*(&V_1)));
		G_B6_0 = L_10;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		G_B6_0 = L_11;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t522578821* L_12 = (StringU5BU5D_t522578821*)G_B6_3;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral527848695);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral527848695);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m2040094846(NULL /*static, unused*/, (StringU5BU5D_t522578821*)L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m272197277_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t2204764377 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2204764377 *>(__this + 1);
	return KeyValuePair_2_ToString_m272197277(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m346367360_gshared (KeyValuePair_2_t2366093889 * __this, RuntimeObject * ___key0, bool ___value1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___key0;
		KeyValuePair_2_set_Key_m4110759363((KeyValuePair_2_t2366093889 *)__this, (RuntimeObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		bool L_1 = ___value1;
		KeyValuePair_2_set_Value_m3810493383((KeyValuePair_2_t2366093889 *)__this, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m346367360_AdjustorThunk (RuntimeObject * __this, RuntimeObject * ___key0, bool ___value1, const RuntimeMethod* method)
{
	KeyValuePair_2_t2366093889 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2366093889 *>(__this + 1);
	KeyValuePair_2__ctor_m346367360(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::get_Key()
extern "C"  RuntimeObject * KeyValuePair_2_get_Key_m1494729318_gshared (KeyValuePair_2_t2366093889 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_key_0();
		return L_0;
	}
}
extern "C"  RuntimeObject * KeyValuePair_2_get_Key_m1494729318_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t2366093889 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2366093889 *>(__this + 1);
	return KeyValuePair_2_get_Key_m1494729318(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m4110759363_gshared (KeyValuePair_2_t2366093889 * __this, RuntimeObject * ___value0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m4110759363_AdjustorThunk (RuntimeObject * __this, RuntimeObject * ___value0, const RuntimeMethod* method)
{
	KeyValuePair_2_t2366093889 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2366093889 *>(__this + 1);
	KeyValuePair_2_set_Key_m4110759363(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::get_Value()
extern "C"  bool KeyValuePair_2_get_Value_m682268374_gshared (KeyValuePair_2_t2366093889 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = (bool)__this->get_value_1();
		return L_0;
	}
}
extern "C"  bool KeyValuePair_2_get_Value_m682268374_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t2366093889 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2366093889 *>(__this + 1);
	return KeyValuePair_2_get_Value_m682268374(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m3810493383_gshared (KeyValuePair_2_t2366093889 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m3810493383_AdjustorThunk (RuntimeObject * __this, bool ___value0, const RuntimeMethod* method)
{
	KeyValuePair_2_t2366093889 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2366093889 *>(__this + 1);
	KeyValuePair_2_set_Value_m3810493383(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m2957839857_gshared (KeyValuePair_2_t2366093889 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m2957839857_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject * V_0 = NULL;
	bool V_1 = false;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t522578821* G_B2_1 = NULL;
	StringU5BU5D_t522578821* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t522578821* G_B1_1 = NULL;
	StringU5BU5D_t522578821* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t522578821* G_B3_2 = NULL;
	StringU5BU5D_t522578821* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t522578821* G_B5_1 = NULL;
	StringU5BU5D_t522578821* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t522578821* G_B4_1 = NULL;
	StringU5BU5D_t522578821* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t522578821* G_B6_2 = NULL;
	StringU5BU5D_t522578821* G_B6_3 = NULL;
	{
		StringU5BU5D_t522578821* L_0 = (StringU5BU5D_t522578821*)((StringU5BU5D_t522578821*)SZArrayNew(StringU5BU5D_t522578821_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, _stringLiteral1608275668);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral1608275668);
		StringU5BU5D_t522578821* L_1 = (StringU5BU5D_t522578821*)L_0;
		RuntimeObject * L_2 = KeyValuePair_2_get_Key_m1494729318((KeyValuePair_2_t2366093889 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
		if (!L_2)
		{
			G_B2_0 = 1;
			G_B2_1 = L_1;
			G_B2_2 = L_1;
			goto IL_0039;
		}
	}
	{
		RuntimeObject * L_3 = KeyValuePair_2_get_Key_m1494729318((KeyValuePair_2_t2366093889 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (RuntimeObject *)L_3;
		NullCheck((RuntimeObject *)(*(&V_0)));
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (RuntimeObject *)(*(&V_0)));
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t522578821* L_6 = (StringU5BU5D_t522578821*)G_B3_3;
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, _stringLiteral2131789870);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral2131789870);
		StringU5BU5D_t522578821* L_7 = (StringU5BU5D_t522578821*)L_6;
		bool L_8 = KeyValuePair_2_get_Value_m682268374((KeyValuePair_2_t2366093889 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
	}
	{
		bool L_9 = KeyValuePair_2_get_Value_m682268374((KeyValuePair_2_t2366093889 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (bool)L_9;
		String_t* L_10 = Boolean_ToString_m3488188482((bool*)(&V_1), /*hidden argument*/NULL);
		G_B6_0 = L_10;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		G_B6_0 = L_11;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t522578821* L_12 = (StringU5BU5D_t522578821*)G_B6_3;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral527848695);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral527848695);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m2040094846(NULL /*static, unused*/, (StringU5BU5D_t522578821*)L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m2957839857_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t2366093889 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2366093889 *>(__this + 1);
	return KeyValuePair_2_ToString_m2957839857(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m3988816228_gshared (KeyValuePair_2_t2877556189 * __this, RuntimeObject * ___key0, int32_t ___value1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___key0;
		KeyValuePair_2_set_Key_m1817844414((KeyValuePair_2_t2877556189 *)__this, (RuntimeObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = ___value1;
		KeyValuePair_2_set_Value_m605429017((KeyValuePair_2_t2877556189 *)__this, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m3988816228_AdjustorThunk (RuntimeObject * __this, RuntimeObject * ___key0, int32_t ___value1, const RuntimeMethod* method)
{
	KeyValuePair_2_t2877556189 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2877556189 *>(__this + 1);
	KeyValuePair_2__ctor_m3988816228(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::get_Key()
extern "C"  RuntimeObject * KeyValuePair_2_get_Key_m1080276696_gshared (KeyValuePair_2_t2877556189 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_key_0();
		return L_0;
	}
}
extern "C"  RuntimeObject * KeyValuePair_2_get_Key_m1080276696_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t2877556189 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2877556189 *>(__this + 1);
	return KeyValuePair_2_get_Key_m1080276696(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m1817844414_gshared (KeyValuePair_2_t2877556189 * __this, RuntimeObject * ___value0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m1817844414_AdjustorThunk (RuntimeObject * __this, RuntimeObject * ___value0, const RuntimeMethod* method)
{
	KeyValuePair_2_t2877556189 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2877556189 *>(__this + 1);
	KeyValuePair_2_set_Key_m1817844414(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::get_Value()
extern "C"  int32_t KeyValuePair_2_get_Value_m3213725153_gshared (KeyValuePair_2_t2877556189 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_value_1();
		return L_0;
	}
}
extern "C"  int32_t KeyValuePair_2_get_Value_m3213725153_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t2877556189 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2877556189 *>(__this + 1);
	return KeyValuePair_2_get_Value_m3213725153(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m605429017_gshared (KeyValuePair_2_t2877556189 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m605429017_AdjustorThunk (RuntimeObject * __this, int32_t ___value0, const RuntimeMethod* method)
{
	KeyValuePair_2_t2877556189 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2877556189 *>(__this + 1);
	KeyValuePair_2_set_Value_m605429017(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m3938306873_gshared (KeyValuePair_2_t2877556189 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m3938306873_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t522578821* G_B2_1 = NULL;
	StringU5BU5D_t522578821* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t522578821* G_B1_1 = NULL;
	StringU5BU5D_t522578821* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t522578821* G_B3_2 = NULL;
	StringU5BU5D_t522578821* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t522578821* G_B5_1 = NULL;
	StringU5BU5D_t522578821* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t522578821* G_B4_1 = NULL;
	StringU5BU5D_t522578821* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t522578821* G_B6_2 = NULL;
	StringU5BU5D_t522578821* G_B6_3 = NULL;
	{
		StringU5BU5D_t522578821* L_0 = (StringU5BU5D_t522578821*)((StringU5BU5D_t522578821*)SZArrayNew(StringU5BU5D_t522578821_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, _stringLiteral1608275668);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral1608275668);
		StringU5BU5D_t522578821* L_1 = (StringU5BU5D_t522578821*)L_0;
		RuntimeObject * L_2 = KeyValuePair_2_get_Key_m1080276696((KeyValuePair_2_t2877556189 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
		if (!L_2)
		{
			G_B2_0 = 1;
			G_B2_1 = L_1;
			G_B2_2 = L_1;
			goto IL_0039;
		}
	}
	{
		RuntimeObject * L_3 = KeyValuePair_2_get_Key_m1080276696((KeyValuePair_2_t2877556189 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (RuntimeObject *)L_3;
		NullCheck((RuntimeObject *)(*(&V_0)));
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (RuntimeObject *)(*(&V_0)));
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t522578821* L_6 = (StringU5BU5D_t522578821*)G_B3_3;
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, _stringLiteral2131789870);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral2131789870);
		StringU5BU5D_t522578821* L_7 = (StringU5BU5D_t522578821*)L_6;
		int32_t L_8 = KeyValuePair_2_get_Value_m3213725153((KeyValuePair_2_t2877556189 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
	}
	{
		int32_t L_9 = KeyValuePair_2_get_Value_m3213725153((KeyValuePair_2_t2877556189 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (int32_t)L_9;
		String_t* L_10 = Int32_ToString_m942907109((int32_t*)(&V_1), /*hidden argument*/NULL);
		G_B6_0 = L_10;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		G_B6_0 = L_11;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t522578821* L_12 = (StringU5BU5D_t522578821*)G_B6_3;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral527848695);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral527848695);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m2040094846(NULL /*static, unused*/, (StringU5BU5D_t522578821*)L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m3938306873_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t2877556189 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2877556189 *>(__this + 1);
	return KeyValuePair_2_ToString_m3938306873(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m2416117444_gshared (KeyValuePair_2_t3602437993 * __this, RuntimeObject * ___key0, RuntimeObject * ___value1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___key0;
		KeyValuePair_2_set_Key_m2678256174((KeyValuePair_2_t3602437993 *)__this, (RuntimeObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		RuntimeObject * L_1 = ___value1;
		KeyValuePair_2_set_Value_m1656636255((KeyValuePair_2_t3602437993 *)__this, (RuntimeObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m2416117444_AdjustorThunk (RuntimeObject * __this, RuntimeObject * ___key0, RuntimeObject * ___value1, const RuntimeMethod* method)
{
	KeyValuePair_2_t3602437993 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3602437993 *>(__this + 1);
	KeyValuePair_2__ctor_m2416117444(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::get_Key()
extern "C"  RuntimeObject * KeyValuePair_2_get_Key_m2242947300_gshared (KeyValuePair_2_t3602437993 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_key_0();
		return L_0;
	}
}
extern "C"  RuntimeObject * KeyValuePair_2_get_Key_m2242947300_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t3602437993 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3602437993 *>(__this + 1);
	return KeyValuePair_2_get_Key_m2242947300(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m2678256174_gshared (KeyValuePair_2_t3602437993 * __this, RuntimeObject * ___value0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m2678256174_AdjustorThunk (RuntimeObject * __this, RuntimeObject * ___value0, const RuntimeMethod* method)
{
	KeyValuePair_2_t3602437993 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3602437993 *>(__this + 1);
	KeyValuePair_2_set_Key_m2678256174(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::get_Value()
extern "C"  RuntimeObject * KeyValuePair_2_get_Value_m1442051799_gshared (KeyValuePair_2_t3602437993 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_value_1();
		return L_0;
	}
}
extern "C"  RuntimeObject * KeyValuePair_2_get_Value_m1442051799_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t3602437993 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3602437993 *>(__this + 1);
	return KeyValuePair_2_get_Value_m1442051799(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m1656636255_gshared (KeyValuePair_2_t3602437993 * __this, RuntimeObject * ___value0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m1656636255_AdjustorThunk (RuntimeObject * __this, RuntimeObject * ___value0, const RuntimeMethod* method)
{
	KeyValuePair_2_t3602437993 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3602437993 *>(__this + 1);
	KeyValuePair_2_set_Value_m1656636255(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m1421527622_gshared (KeyValuePair_2_t3602437993 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m1421527622_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject * V_0 = NULL;
	RuntimeObject * V_1 = NULL;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t522578821* G_B2_1 = NULL;
	StringU5BU5D_t522578821* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t522578821* G_B1_1 = NULL;
	StringU5BU5D_t522578821* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t522578821* G_B3_2 = NULL;
	StringU5BU5D_t522578821* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t522578821* G_B5_1 = NULL;
	StringU5BU5D_t522578821* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t522578821* G_B4_1 = NULL;
	StringU5BU5D_t522578821* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t522578821* G_B6_2 = NULL;
	StringU5BU5D_t522578821* G_B6_3 = NULL;
	{
		StringU5BU5D_t522578821* L_0 = (StringU5BU5D_t522578821*)((StringU5BU5D_t522578821*)SZArrayNew(StringU5BU5D_t522578821_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, _stringLiteral1608275668);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral1608275668);
		StringU5BU5D_t522578821* L_1 = (StringU5BU5D_t522578821*)L_0;
		RuntimeObject * L_2 = KeyValuePair_2_get_Key_m2242947300((KeyValuePair_2_t3602437993 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
		if (!L_2)
		{
			G_B2_0 = 1;
			G_B2_1 = L_1;
			G_B2_2 = L_1;
			goto IL_0039;
		}
	}
	{
		RuntimeObject * L_3 = KeyValuePair_2_get_Key_m2242947300((KeyValuePair_2_t3602437993 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (RuntimeObject *)L_3;
		NullCheck((RuntimeObject *)(*(&V_0)));
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (RuntimeObject *)(*(&V_0)));
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t522578821* L_6 = (StringU5BU5D_t522578821*)G_B3_3;
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, _stringLiteral2131789870);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral2131789870);
		StringU5BU5D_t522578821* L_7 = (StringU5BU5D_t522578821*)L_6;
		RuntimeObject * L_8 = KeyValuePair_2_get_Value_m1442051799((KeyValuePair_2_t3602437993 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
		if (!L_8)
		{
			G_B5_0 = 3;
			G_B5_1 = L_7;
			G_B5_2 = L_7;
			goto IL_0072;
		}
	}
	{
		RuntimeObject * L_9 = KeyValuePair_2_get_Value_m1442051799((KeyValuePair_2_t3602437993 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (RuntimeObject *)L_9;
		NullCheck((RuntimeObject *)(*(&V_1)));
		String_t* L_10 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (RuntimeObject *)(*(&V_1)));
		G_B6_0 = L_10;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		G_B6_0 = L_11;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t522578821* L_12 = (StringU5BU5D_t522578821*)G_B6_3;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral527848695);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral527848695);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m2040094846(NULL /*static, unused*/, (StringU5BU5D_t522578821*)L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m1421527622_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t3602437993 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3602437993 *>(__this + 1);
	return KeyValuePair_2_ToString_m1421527622(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m223128127_gshared (Enumerator_t2209518572 * __this, List_1_t449703515 * ___l0, const RuntimeMethod* method)
{
	{
		List_1_t449703515 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t449703515 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m223128127_AdjustorThunk (RuntimeObject * __this, List_1_t449703515 * ___l0, const RuntimeMethod* method)
{
	Enumerator_t2209518572 * _thisAdjusted = reinterpret_cast<Enumerator_t2209518572 *>(__this + 1);
	Enumerator__ctor_m223128127(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2247808877_gshared (Enumerator_t2209518572 * __this, const RuntimeMethod* method)
{
	{
		Enumerator_VerifyState_m2300229768((Enumerator_t2209518572 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2247808877_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t2209518572 * _thisAdjusted = reinterpret_cast<Enumerator_t2209518572 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m2247808877(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * Enumerator_System_Collections_IEnumerator_get_Current_m1371828734_gshared (Enumerator_t2209518572 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m1371828734_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_VerifyState_m2300229768((Enumerator_t2209518572 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1956282378 * L_1 = (InvalidOperationException_t1956282378 *)il2cpp_codegen_object_new(InvalidOperationException_t1956282378_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m3074049270(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_current_3();
		int32_t L_3 = L_2;
		RuntimeObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  RuntimeObject * Enumerator_System_Collections_IEnumerator_get_Current_m1371828734_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t2209518572 * _thisAdjusted = reinterpret_cast<Enumerator_t2209518572 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m1371828734(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::Dispose()
extern "C"  void Enumerator_Dispose_m3280506413_gshared (Enumerator_t2209518572 * __this, const RuntimeMethod* method)
{
	{
		__this->set_l_0((List_1_t449703515 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m3280506413_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t2209518572 * _thisAdjusted = reinterpret_cast<Enumerator_t2209518572 *>(__this + 1);
	Enumerator_Dispose_m3280506413(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::VerifyState()
extern "C"  void Enumerator_VerifyState_m2300229768_gshared (Enumerator_t2209518572 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m2300229768_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t449703515 * L_0 = (List_1_t449703515 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t2209518572  L_1 = (*(Enumerator_t2209518572 *)__this);
		RuntimeObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((RuntimeObject *)L_2);
		Type_t * L_3 = Object_GetType_m4087638284((RuntimeObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t363085662 * L_5 = (ObjectDisposedException_t363085662 *)il2cpp_codegen_object_new(ObjectDisposedException_t363085662_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m781126761(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t449703515 * L_7 = (List_1_t449703515 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t1956282378 * L_9 = (InvalidOperationException_t1956282378 *)il2cpp_codegen_object_new(InvalidOperationException_t1956282378_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1754939824(L_9, (String_t*)_stringLiteral573806582, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m2300229768_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t2209518572 * _thisAdjusted = reinterpret_cast<Enumerator_t2209518572 *>(__this + 1);
	Enumerator_VerifyState_m2300229768(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Int32>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1421551000_gshared (Enumerator_t2209518572 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		Enumerator_VerifyState_m2300229768((Enumerator_t2209518572 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t449703515 * L_2 = (List_1_t449703515 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t449703515 * L_4 = (List_1_t449703515 *)__this->get_l_0();
		NullCheck(L_4);
		Int32U5BU5D_t2856113350* L_5 = (Int32U5BU5D_t2856113350*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		int32_t L_9 = L_8;
		int32_t L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m1421551000_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t2209518572 * _thisAdjusted = reinterpret_cast<Enumerator_t2209518572 *>(__this + 1);
	return Enumerator_MoveNext_m1421551000(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<System.Int32>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m2531165950_gshared (Enumerator_t2209518572 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_current_3();
		return L_0;
	}
}
extern "C"  int32_t Enumerator_get_Current_m2531165950_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t2209518572 * _thisAdjusted = reinterpret_cast<Enumerator_t2209518572 *>(__this + 1);
	return Enumerator_get_Current_m2531165950(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m3625216372_gshared (Enumerator_t2934400376 * __this, List_1_t1174585319 * ___l0, const RuntimeMethod* method)
{
	{
		List_1_t1174585319 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t1174585319 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m3625216372_AdjustorThunk (RuntimeObject * __this, List_1_t1174585319 * ___l0, const RuntimeMethod* method)
{
	Enumerator_t2934400376 * _thisAdjusted = reinterpret_cast<Enumerator_t2934400376 *>(__this + 1);
	Enumerator__ctor_m3625216372(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m4103439166_gshared (Enumerator_t2934400376 * __this, const RuntimeMethod* method)
{
	{
		Enumerator_VerifyState_m2889154((Enumerator_t2934400376 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m4103439166_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t2934400376 * _thisAdjusted = reinterpret_cast<Enumerator_t2934400376 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m4103439166(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * Enumerator_System_Collections_IEnumerator_get_Current_m1417958798_gshared (Enumerator_t2934400376 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m1417958798_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_VerifyState_m2889154((Enumerator_t2934400376 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1956282378 * L_1 = (InvalidOperationException_t1956282378 *)il2cpp_codegen_object_new(InvalidOperationException_t1956282378_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m3074049270(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		RuntimeObject * L_2 = (RuntimeObject *)__this->get_current_3();
		return L_2;
	}
}
extern "C"  RuntimeObject * Enumerator_System_Collections_IEnumerator_get_Current_m1417958798_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t2934400376 * _thisAdjusted = reinterpret_cast<Enumerator_t2934400376 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m1417958798(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m1212514242_gshared (Enumerator_t2934400376 * __this, const RuntimeMethod* method)
{
	{
		__this->set_l_0((List_1_t1174585319 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m1212514242_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t2934400376 * _thisAdjusted = reinterpret_cast<Enumerator_t2934400376 *>(__this + 1);
	Enumerator_Dispose_m1212514242(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::VerifyState()
extern "C"  void Enumerator_VerifyState_m2889154_gshared (Enumerator_t2934400376 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m2889154_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t1174585319 * L_0 = (List_1_t1174585319 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t2934400376  L_1 = (*(Enumerator_t2934400376 *)__this);
		RuntimeObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((RuntimeObject *)L_2);
		Type_t * L_3 = Object_GetType_m4087638284((RuntimeObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t363085662 * L_5 = (ObjectDisposedException_t363085662 *)il2cpp_codegen_object_new(ObjectDisposedException_t363085662_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m781126761(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t1174585319 * L_7 = (List_1_t1174585319 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t1956282378 * L_9 = (InvalidOperationException_t1956282378 *)il2cpp_codegen_object_new(InvalidOperationException_t1956282378_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1754939824(L_9, (String_t*)_stringLiteral573806582, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m2889154_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t2934400376 * _thisAdjusted = reinterpret_cast<Enumerator_t2934400376 *>(__this + 1);
	Enumerator_VerifyState_m2889154(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3640007406_gshared (Enumerator_t2934400376 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		Enumerator_VerifyState_m2889154((Enumerator_t2934400376 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t1174585319 * L_2 = (List_1_t1174585319 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t1174585319 * L_4 = (List_1_t1174585319 *)__this->get_l_0();
		NullCheck(L_4);
		ObjectU5BU5D_t4290686858* L_5 = (ObjectU5BU5D_t4290686858*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		int32_t L_9 = L_8;
		RuntimeObject * L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m3640007406_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t2934400376 * _thisAdjusted = reinterpret_cast<Enumerator_t2934400376 *>(__this + 1);
	return Enumerator_MoveNext_m3640007406(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<System.Object>::get_Current()
extern "C"  RuntimeObject * Enumerator_get_Current_m3814319504_gshared (Enumerator_t2934400376 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_current_3();
		return L_0;
	}
}
extern "C"  RuntimeObject * Enumerator_get_Current_m3814319504_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t2934400376 * _thisAdjusted = reinterpret_cast<Enumerator_t2934400376 *>(__this + 1);
	return Enumerator_get_Current_m3814319504(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m2449765543_gshared (Enumerator_t64113712 * __this, List_1_t2599265951 * ___l0, const RuntimeMethod* method)
{
	{
		List_1_t2599265951 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t2599265951 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m2449765543_AdjustorThunk (RuntimeObject * __this, List_1_t2599265951 * ___l0, const RuntimeMethod* method)
{
	Enumerator_t64113712 * _thisAdjusted = reinterpret_cast<Enumerator_t64113712 *>(__this + 1);
	Enumerator__ctor_m2449765543(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m854669720_gshared (Enumerator_t64113712 * __this, const RuntimeMethod* method)
{
	{
		Enumerator_VerifyState_m1870543230((Enumerator_t64113712 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m854669720_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t64113712 * _thisAdjusted = reinterpret_cast<Enumerator_t64113712 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m854669720(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * Enumerator_System_Collections_IEnumerator_get_Current_m4080730988_gshared (Enumerator_t64113712 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m4080730988_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_VerifyState_m1870543230((Enumerator_t64113712 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1956282378 * L_1 = (InvalidOperationException_t1956282378 *)il2cpp_codegen_object_new(InvalidOperationException_t1956282378_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m3074049270(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		CustomAttributeNamedArgument_t425730339  L_2 = (CustomAttributeNamedArgument_t425730339 )__this->get_current_3();
		CustomAttributeNamedArgument_t425730339  L_3 = L_2;
		RuntimeObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  RuntimeObject * Enumerator_System_Collections_IEnumerator_get_Current_m4080730988_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t64113712 * _thisAdjusted = reinterpret_cast<Enumerator_t64113712 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m4080730988(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::Dispose()
extern "C"  void Enumerator_Dispose_m858809926_gshared (Enumerator_t64113712 * __this, const RuntimeMethod* method)
{
	{
		__this->set_l_0((List_1_t2599265951 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m858809926_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t64113712 * _thisAdjusted = reinterpret_cast<Enumerator_t64113712 *>(__this + 1);
	Enumerator_Dispose_m858809926(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::VerifyState()
extern "C"  void Enumerator_VerifyState_m1870543230_gshared (Enumerator_t64113712 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m1870543230_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t2599265951 * L_0 = (List_1_t2599265951 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t64113712  L_1 = (*(Enumerator_t64113712 *)__this);
		RuntimeObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((RuntimeObject *)L_2);
		Type_t * L_3 = Object_GetType_m4087638284((RuntimeObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t363085662 * L_5 = (ObjectDisposedException_t363085662 *)il2cpp_codegen_object_new(ObjectDisposedException_t363085662_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m781126761(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t2599265951 * L_7 = (List_1_t2599265951 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t1956282378 * L_9 = (InvalidOperationException_t1956282378 *)il2cpp_codegen_object_new(InvalidOperationException_t1956282378_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1754939824(L_9, (String_t*)_stringLiteral573806582, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m1870543230_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t64113712 * _thisAdjusted = reinterpret_cast<Enumerator_t64113712 *>(__this + 1);
	Enumerator_VerifyState_m1870543230(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3058718994_gshared (Enumerator_t64113712 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		Enumerator_VerifyState_m1870543230((Enumerator_t64113712 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t2599265951 * L_2 = (List_1_t2599265951 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t2599265951 * L_4 = (List_1_t2599265951 *)__this->get_l_0();
		NullCheck(L_4);
		CustomAttributeNamedArgumentU5BU5D_t3127017202* L_5 = (CustomAttributeNamedArgumentU5BU5D_t3127017202*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		int32_t L_9 = L_8;
		CustomAttributeNamedArgument_t425730339  L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m3058718994_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t64113712 * _thisAdjusted = reinterpret_cast<Enumerator_t64113712 *>(__this + 1);
	return Enumerator_MoveNext_m3058718994(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::get_Current()
extern "C"  CustomAttributeNamedArgument_t425730339  Enumerator_get_Current_m3125146575_gshared (Enumerator_t64113712 * __this, const RuntimeMethod* method)
{
	{
		CustomAttributeNamedArgument_t425730339  L_0 = (CustomAttributeNamedArgument_t425730339 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  CustomAttributeNamedArgument_t425730339  Enumerator_get_Current_m3125146575_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t64113712 * _thisAdjusted = reinterpret_cast<Enumerator_t64113712 *>(__this + 1);
	return Enumerator_get_Current_m3125146575(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m2446290647_gshared (Enumerator_t2306086094 * __this, List_1_t546271037 * ___l0, const RuntimeMethod* method)
{
	{
		List_1_t546271037 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t546271037 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m2446290647_AdjustorThunk (RuntimeObject * __this, List_1_t546271037 * ___l0, const RuntimeMethod* method)
{
	Enumerator_t2306086094 * _thisAdjusted = reinterpret_cast<Enumerator_t2306086094 *>(__this + 1);
	Enumerator__ctor_m2446290647(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m4223785200_gshared (Enumerator_t2306086094 * __this, const RuntimeMethod* method)
{
	{
		Enumerator_VerifyState_m827611239((Enumerator_t2306086094 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m4223785200_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t2306086094 * _thisAdjusted = reinterpret_cast<Enumerator_t2306086094 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m4223785200(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * Enumerator_System_Collections_IEnumerator_get_Current_m2003157540_gshared (Enumerator_t2306086094 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m2003157540_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_VerifyState_m827611239((Enumerator_t2306086094 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1956282378 * L_1 = (InvalidOperationException_t1956282378 *)il2cpp_codegen_object_new(InvalidOperationException_t1956282378_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m3074049270(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		CustomAttributeTypedArgument_t2667702721  L_2 = (CustomAttributeTypedArgument_t2667702721 )__this->get_current_3();
		CustomAttributeTypedArgument_t2667702721  L_3 = L_2;
		RuntimeObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  RuntimeObject * Enumerator_System_Collections_IEnumerator_get_Current_m2003157540_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t2306086094 * _thisAdjusted = reinterpret_cast<Enumerator_t2306086094 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m2003157540(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::Dispose()
extern "C"  void Enumerator_Dispose_m1763116863_gshared (Enumerator_t2306086094 * __this, const RuntimeMethod* method)
{
	{
		__this->set_l_0((List_1_t546271037 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m1763116863_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t2306086094 * _thisAdjusted = reinterpret_cast<Enumerator_t2306086094 *>(__this + 1);
	Enumerator_Dispose_m1763116863(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::VerifyState()
extern "C"  void Enumerator_VerifyState_m827611239_gshared (Enumerator_t2306086094 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m827611239_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t546271037 * L_0 = (List_1_t546271037 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t2306086094  L_1 = (*(Enumerator_t2306086094 *)__this);
		RuntimeObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((RuntimeObject *)L_2);
		Type_t * L_3 = Object_GetType_m4087638284((RuntimeObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t363085662 * L_5 = (ObjectDisposedException_t363085662 *)il2cpp_codegen_object_new(ObjectDisposedException_t363085662_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m781126761(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t546271037 * L_7 = (List_1_t546271037 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t1956282378 * L_9 = (InvalidOperationException_t1956282378 *)il2cpp_codegen_object_new(InvalidOperationException_t1956282378_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1754939824(L_9, (String_t*)_stringLiteral573806582, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m827611239_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t2306086094 * _thisAdjusted = reinterpret_cast<Enumerator_t2306086094 *>(__this + 1);
	Enumerator_VerifyState_m827611239(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2664888314_gshared (Enumerator_t2306086094 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		Enumerator_VerifyState_m827611239((Enumerator_t2306086094 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t546271037 * L_2 = (List_1_t546271037 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t546271037 * L_4 = (List_1_t546271037 *)__this->get_l_0();
		NullCheck(L_4);
		CustomAttributeTypedArgumentU5BU5D_t2668173852* L_5 = (CustomAttributeTypedArgumentU5BU5D_t2668173852*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		int32_t L_9 = L_8;
		CustomAttributeTypedArgument_t2667702721  L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m2664888314_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t2306086094 * _thisAdjusted = reinterpret_cast<Enumerator_t2306086094 *>(__this + 1);
	return Enumerator_MoveNext_m2664888314(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::get_Current()
extern "C"  CustomAttributeTypedArgument_t2667702721  Enumerator_get_Current_m1772309331_gshared (Enumerator_t2306086094 * __this, const RuntimeMethod* method)
{
	{
		CustomAttributeTypedArgument_t2667702721  L_0 = (CustomAttributeTypedArgument_t2667702721 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  CustomAttributeTypedArgument_t2667702721  Enumerator_get_Current_m1772309331_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t2306086094 * _thisAdjusted = reinterpret_cast<Enumerator_t2306086094 *>(__this + 1);
	return Enumerator_get_Current_m1772309331(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1<System.Int32>::.ctor()
extern "C"  void List_1__ctor_m2518762847_gshared (List_1_t449703515 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m3446193457((RuntimeObject *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		Int32U5BU5D_t2856113350* L_0 = ((List_1_t449703515_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0)))->get_EmptyArray_4();
		__this->set__items_1(L_0);
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Int32>::.ctor(System.Int32)
extern "C"  void List_1__ctor_m3542732767_gshared (List_1_t449703515 * __this, int32_t ___capacity0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1__ctor_m3542732767_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m3446193457((RuntimeObject *)__this, /*hidden argument*/NULL);
		int32_t L_0 = ___capacity0;
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		ArgumentOutOfRangeException_t1745463579 * L_1 = (ArgumentOutOfRangeException_t1745463579 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t1745463579_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2829693980(L_1, (String_t*)_stringLiteral2929636671, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = ___capacity0;
		__this->set__items_1(((Int32U5BU5D_t2856113350*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1), (uint32_t)L_2)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Int32>::.cctor()
extern "C"  void List_1__cctor_m4211836207_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		((List_1_t449703515_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)))->set_EmptyArray_4(((Int32U5BU5D_t2856113350*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (uint32_t)0)));
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<System.Int32>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  RuntimeObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m4192552342_gshared (List_1_t449703515 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((List_1_t449703515 *)__this);
		Enumerator_t2209518572  L_0 = ((  Enumerator_t2209518572  (*) (List_1_t449703515 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((List_1_t449703515 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		Enumerator_t2209518572  L_1 = L_0;
		RuntimeObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), &L_1);
		return (RuntimeObject*)L_2;
	}
}
// System.Void System.Collections.Generic.List`1<System.Int32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m1023640867_gshared (List_1_t449703515 * __this, RuntimeArray * ___array0, int32_t ___arrayIndex1, const RuntimeMethod* method)
{
	{
		Int32U5BU5D_t2856113350* L_0 = (Int32U5BU5D_t2856113350*)__this->get__items_1();
		RuntimeArray * L_1 = ___array0;
		int32_t L_2 = ___arrayIndex1;
		int32_t L_3 = (int32_t)__this->get__size_2();
		Array_Copy_m1287631566(NULL /*static, unused*/, (RuntimeArray *)(RuntimeArray *)L_0, (int32_t)0, (RuntimeArray *)L_1, (int32_t)L_2, (int32_t)L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.List`1<System.Int32>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  RuntimeObject* List_1_System_Collections_IEnumerable_GetEnumerator_m3712794436_gshared (List_1_t449703515 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((List_1_t449703515 *)__this);
		Enumerator_t2209518572  L_0 = ((  Enumerator_t2209518572  (*) (List_1_t449703515 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((List_1_t449703515 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		Enumerator_t2209518572  L_1 = L_0;
		RuntimeObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), &L_1);
		return (RuntimeObject*)L_2;
	}
}
// System.Int32 System.Collections.Generic.List`1<System.Int32>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_Add_m2139410595_gshared (List_1_t449703515 * __this, RuntimeObject * ___item0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_Add_m2139410595_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Exception_t3361176243 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3361176243 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			RuntimeObject * L_0 = ___item0;
			NullCheck((List_1_t449703515 *)__this);
			((  void (*) (List_1_t449703515 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((List_1_t449703515 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
			int32_t L_1 = (int32_t)__this->get__size_2();
			V_0 = (int32_t)((int32_t)((int32_t)L_1-(int32_t)1));
			goto IL_0036;
		}

IL_001a:
		{
			; // IL_001a: leave IL_002b
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3361176243 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t2549690990_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_001f;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t1009887698_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_0025;
		throw e;
	}

CATCH_001f:
	{ // begin catch(System.NullReferenceException)
		goto IL_002b;
	} // end catch (depth: 1)

CATCH_0025:
	{ // begin catch(System.InvalidCastException)
		goto IL_002b;
	} // end catch (depth: 1)

IL_002b:
	{
		ArgumentException_t1372035057 * L_2 = (ArgumentException_t1372035057 *)il2cpp_codegen_object_new(ArgumentException_t1372035057_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m775170528(L_2, (String_t*)_stringLiteral3684952963, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0036:
	{
		int32_t L_3 = V_0;
		return L_3;
	}
}
// System.Boolean System.Collections.Generic.List`1<System.Int32>::System.Collections.IList.Contains(System.Object)
extern "C"  bool List_1_System_Collections_IList_Contains_m2533151603_gshared (List_1_t449703515 * __this, RuntimeObject * ___item0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_Contains_m2533151603_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Exception_t3361176243 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3361176243 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			RuntimeObject * L_0 = ___item0;
			NullCheck((List_1_t449703515 *)__this);
			bool L_1 = ((  bool (*) (List_1_t449703515 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t449703515 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
			V_0 = (bool)L_1;
			goto IL_0025;
		}

IL_0012:
		{
			; // IL_0012: leave IL_0023
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3361176243 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t2549690990_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_0017;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t1009887698_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_001d;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.NullReferenceException)
		goto IL_0023;
	} // end catch (depth: 1)

CATCH_001d:
	{ // begin catch(System.InvalidCastException)
		goto IL_0023;
	} // end catch (depth: 1)

IL_0023:
	{
		return (bool)0;
	}

IL_0025:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Int32 System.Collections.Generic.List`1<System.Int32>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m3575252870_gshared (List_1_t449703515 * __this, RuntimeObject * ___item0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_IndexOf_m3575252870_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Exception_t3361176243 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3361176243 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			RuntimeObject * L_0 = ___item0;
			NullCheck((List_1_t449703515 *)__this);
			int32_t L_1 = ((  int32_t (*) (List_1_t449703515 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((List_1_t449703515 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
			V_0 = (int32_t)L_1;
			goto IL_0025;
		}

IL_0012:
		{
			; // IL_0012: leave IL_0023
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3361176243 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t2549690990_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_0017;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t1009887698_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_001d;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.NullReferenceException)
		goto IL_0023;
	} // end catch (depth: 1)

CATCH_001d:
	{ // begin catch(System.InvalidCastException)
		goto IL_0023;
	} // end catch (depth: 1)

IL_0023:
	{
		return (-1);
	}

IL_0025:
	{
		int32_t L_2 = V_0;
		return L_2;
	}
}
// System.Void System.Collections.Generic.List`1<System.Int32>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_Insert_m386691397_gshared (List_1_t449703515 * __this, int32_t ___index0, RuntimeObject * ___item1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_Insert_m386691397_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Exception_t3361176243 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3361176243 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t449703515 *)__this);
		((  void (*) (List_1_t449703515 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((List_1_t449703515 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			int32_t L_1 = ___index0;
			RuntimeObject * L_2 = ___item1;
			NullCheck((List_1_t449703515 *)__this);
			((  void (*) (List_1_t449703515 *, int32_t, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((List_1_t449703515 *)__this, (int32_t)L_1, (int32_t)((*(int32_t*)((int32_t*)UnBox(L_2, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
			goto IL_0035;
		}

IL_0019:
		{
			; // IL_0019: leave IL_002a
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3361176243 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t2549690990_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_001e;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t1009887698_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_0024;
		throw e;
	}

CATCH_001e:
	{ // begin catch(System.NullReferenceException)
		goto IL_002a;
	} // end catch (depth: 1)

CATCH_0024:
	{ // begin catch(System.InvalidCastException)
		goto IL_002a;
	} // end catch (depth: 1)

IL_002a:
	{
		ArgumentException_t1372035057 * L_3 = (ArgumentException_t1372035057 *)il2cpp_codegen_object_new(ArgumentException_t1372035057_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m775170528(L_3, (String_t*)_stringLiteral3684952963, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0035:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Int32>::System.Collections.IList.Remove(System.Object)
extern "C"  void List_1_System_Collections_IList_Remove_m2848662898_gshared (List_1_t449703515 * __this, RuntimeObject * ___item0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_Remove_m2848662898_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Exception_t3361176243 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3361176243 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			RuntimeObject * L_0 = ___item0;
			NullCheck((List_1_t449703515 *)__this);
			((  bool (*) (List_1_t449703515 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((List_1_t449703515 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
			goto IL_0023;
		}

IL_0012:
		{
			; // IL_0012: leave IL_0023
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3361176243 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t2549690990_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_0017;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t1009887698_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_001d;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.NullReferenceException)
		goto IL_0023;
	} // end catch (depth: 1)

CATCH_001d:
	{ // begin catch(System.InvalidCastException)
		goto IL_0023;
	} // end catch (depth: 1)

IL_0023:
	{
		return;
	}
}
// System.Boolean System.Collections.Generic.List`1<System.Int32>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2016662284_gshared (List_1_t449703515 * __this, const RuntimeMethod* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Generic.List`1<System.Int32>::System.Collections.ICollection.get_SyncRoot()
extern "C"  RuntimeObject * List_1_System_Collections_ICollection_get_SyncRoot_m2464317490_gshared (List_1_t449703515 * __this, const RuntimeMethod* method)
{
	{
		return __this;
	}
}
// System.Object System.Collections.Generic.List`1<System.Int32>::System.Collections.IList.get_Item(System.Int32)
extern "C"  RuntimeObject * List_1_System_Collections_IList_get_Item_m2015667882_gshared (List_1_t449703515 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t449703515 *)__this);
		int32_t L_1 = ((  int32_t (*) (List_1_t449703515 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((List_1_t449703515 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		int32_t L_2 = L_1;
		RuntimeObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), &L_2);
		return L_3;
	}
}
// System.Void System.Collections.Generic.List`1<System.Int32>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_set_Item_m3284437010_gshared (List_1_t449703515 * __this, int32_t ___index0, RuntimeObject * ___value1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_set_Item_m3284437010_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Exception_t3361176243 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3361176243 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			int32_t L_0 = ___index0;
			RuntimeObject * L_1 = ___value1;
			NullCheck((List_1_t449703515 *)__this);
			((  void (*) (List_1_t449703515 *, int32_t, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((List_1_t449703515 *)__this, (int32_t)L_0, (int32_t)((*(int32_t*)((int32_t*)UnBox(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
			goto IL_002e;
		}

IL_0012:
		{
			; // IL_0012: leave IL_0023
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3361176243 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t2549690990_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_0017;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t1009887698_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_001d;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.NullReferenceException)
		goto IL_0023;
	} // end catch (depth: 1)

CATCH_001d:
	{ // begin catch(System.InvalidCastException)
		goto IL_0023;
	} // end catch (depth: 1)

IL_0023:
	{
		ArgumentException_t1372035057 * L_2 = (ArgumentException_t1372035057 *)il2cpp_codegen_object_new(ArgumentException_t1372035057_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m775170528(L_2, (String_t*)_stringLiteral3197723981, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_002e:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Int32>::Add(T)
extern "C"  void List_1_Add_m3295068314_gshared (List_1_t449703515 * __this, int32_t ___item0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		Int32U5BU5D_t2856113350* L_1 = (Int32U5BU5D_t2856113350*)__this->get__items_1();
		NullCheck(L_1);
		if ((!(((uint32_t)L_0) == ((uint32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_1)->max_length))))))))
		{
			goto IL_001a;
		}
	}
	{
		NullCheck((List_1_t449703515 *)__this);
		((  void (*) (List_1_t449703515 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13)->methodPointer)((List_1_t449703515 *)__this, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13));
	}

IL_001a:
	{
		Int32U5BU5D_t2856113350* L_2 = (Int32U5BU5D_t2856113350*)__this->get__items_1();
		int32_t L_3 = (int32_t)__this->get__size_2();
		int32_t L_4 = (int32_t)L_3;
		V_0 = (int32_t)L_4;
		__this->set__size_2(((int32_t)((int32_t)L_4+(int32_t)1)));
		int32_t L_5 = V_0;
		int32_t L_6 = ___item0;
		NullCheck(L_2);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (int32_t)L_6);
		int32_t L_7 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_7+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Int32>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m565285224_gshared (List_1_t449703515 * __this, int32_t ___newCount0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		int32_t L_1 = ___newCount0;
		V_0 = (int32_t)((int32_t)((int32_t)L_0+(int32_t)L_1));
		int32_t L_2 = V_0;
		Int32U5BU5D_t2856113350* L_3 = (Int32U5BU5D_t2856113350*)__this->get__items_1();
		NullCheck(L_3);
		if ((((int32_t)L_2) <= ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_3)->max_length)))))))
		{
			goto IL_0031;
		}
	}
	{
		NullCheck((List_1_t449703515 *)__this);
		int32_t L_4 = ((  int32_t (*) (List_1_t449703515 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14)->methodPointer)((List_1_t449703515 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14));
		int32_t L_5 = Math_Max_m3051295998(NULL /*static, unused*/, (int32_t)((int32_t)((int32_t)L_4*(int32_t)2)), (int32_t)4, /*hidden argument*/NULL);
		int32_t L_6 = V_0;
		int32_t L_7 = Math_Max_m3051295998(NULL /*static, unused*/, (int32_t)L_5, (int32_t)L_6, /*hidden argument*/NULL);
		NullCheck((List_1_t449703515 *)__this);
		((  void (*) (List_1_t449703515 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15)->methodPointer)((List_1_t449703515 *)__this, (int32_t)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15));
	}

IL_0031:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Int32>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m3152010686_gshared (List_1_t449703515 * __this, RuntimeObject* ___collection0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		RuntimeObject* L_0 = ___collection0;
		NullCheck((RuntimeObject*)L_0);
		int32_t L_1 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Int32>::get_Count() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 16), (RuntimeObject*)L_0);
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		if (L_2)
		{
			goto IL_000e;
		}
	}
	{
		return;
	}

IL_000e:
	{
		int32_t L_3 = V_0;
		NullCheck((List_1_t449703515 *)__this);
		((  void (*) (List_1_t449703515 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13)->methodPointer)((List_1_t449703515 *)__this, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13));
		RuntimeObject* L_4 = ___collection0;
		Int32U5BU5D_t2856113350* L_5 = (Int32U5BU5D_t2856113350*)__this->get__items_1();
		int32_t L_6 = (int32_t)__this->get__size_2();
		NullCheck((RuntimeObject*)L_4);
		InterfaceActionInvoker2< Int32U5BU5D_t2856113350*, int32_t >::Invoke(5 /* System.Void System.Collections.Generic.ICollection`1<System.Int32>::CopyTo(T[],System.Int32) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 16), (RuntimeObject*)L_4, (Int32U5BU5D_t2856113350*)L_5, (int32_t)L_6);
		int32_t L_7 = (int32_t)__this->get__size_2();
		int32_t L_8 = V_0;
		__this->set__size_2(((int32_t)((int32_t)L_7+(int32_t)L_8)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Int32>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddEnumerable_m1412616697_gshared (List_1_t449703515 * __this, RuntimeObject* ___enumerable0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_AddEnumerable_m1412616697_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	RuntimeObject* V_1 = NULL;
	Exception_t3361176243 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3361176243 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		RuntimeObject* L_0 = ___enumerable0;
		NullCheck((RuntimeObject*)L_0);
		RuntimeObject* L_1 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Int32>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 17), (RuntimeObject*)L_0);
		V_1 = (RuntimeObject*)L_1;
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			goto IL_001a;
		}

IL_000c:
		{
			RuntimeObject* L_2 = V_1;
			NullCheck((RuntimeObject*)L_2);
			int32_t L_3 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* T System.Collections.Generic.IEnumerator`1<System.Int32>::get_Current() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 18), (RuntimeObject*)L_2);
			V_0 = (int32_t)L_3;
			int32_t L_4 = V_0;
			NullCheck((List_1_t449703515 *)__this);
			((  void (*) (List_1_t449703515 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((List_1_t449703515 *)__this, (int32_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		}

IL_001a:
		{
			RuntimeObject* L_5 = V_1;
			NullCheck((RuntimeObject*)L_5);
			bool L_6 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t4262330110_il2cpp_TypeInfo_var, (RuntimeObject*)L_5);
			if (L_6)
			{
				goto IL_000c;
			}
		}

IL_0025:
		{
			IL2CPP_LEAVE(0x35, FINALLY_002a);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3361176243 *)e.ex;
		goto FINALLY_002a;
	}

FINALLY_002a:
	{ // begin finally (depth: 1)
		{
			RuntimeObject* L_7 = V_1;
			if (L_7)
			{
				goto IL_002e;
			}
		}

IL_002d:
		{
			IL2CPP_END_FINALLY(42)
		}

IL_002e:
		{
			RuntimeObject* L_8 = V_1;
			NullCheck((RuntimeObject*)L_8);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t318870991_il2cpp_TypeInfo_var, (RuntimeObject*)L_8);
			IL2CPP_END_FINALLY(42)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(42)
	{
		IL2CPP_JUMP_TBL(0x35, IL_0035)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3361176243 *)
	}

IL_0035:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Int32>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m837954575_gshared (List_1_t449703515 * __this, RuntimeObject* ___collection0, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		RuntimeObject* L_0 = ___collection0;
		NullCheck((List_1_t449703515 *)__this);
		((  void (*) (List_1_t449703515 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19)->methodPointer)((List_1_t449703515 *)__this, (RuntimeObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19));
		RuntimeObject* L_1 = ___collection0;
		V_0 = (RuntimeObject*)((RuntimeObject*)IsInst((RuntimeObject*)L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 16)));
		RuntimeObject* L_2 = V_0;
		if (!L_2)
		{
			goto IL_0020;
		}
	}
	{
		RuntimeObject* L_3 = V_0;
		NullCheck((List_1_t449703515 *)__this);
		((  void (*) (List_1_t449703515 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 20)->methodPointer)((List_1_t449703515 *)__this, (RuntimeObject*)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 20));
		goto IL_0027;
	}

IL_0020:
	{
		RuntimeObject* L_4 = ___collection0;
		NullCheck((List_1_t449703515 *)__this);
		((  void (*) (List_1_t449703515 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 21)->methodPointer)((List_1_t449703515 *)__this, (RuntimeObject*)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 21));
	}

IL_0027:
	{
		int32_t L_5 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_5+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Int32>::Clear()
extern "C"  void List_1_Clear_m2743269208_gshared (List_1_t449703515 * __this, const RuntimeMethod* method)
{
	{
		Int32U5BU5D_t2856113350* L_0 = (Int32U5BU5D_t2856113350*)__this->get__items_1();
		Int32U5BU5D_t2856113350* L_1 = (Int32U5BU5D_t2856113350*)__this->get__items_1();
		NullCheck(L_1);
		Array_Clear_m305290227(NULL /*static, unused*/, (RuntimeArray *)(RuntimeArray *)L_0, (int32_t)0, (int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_1)->max_length)))), /*hidden argument*/NULL);
		__this->set__size_2(0);
		int32_t L_2 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_2+(int32_t)1)));
		return;
	}
}
// System.Boolean System.Collections.Generic.List`1<System.Int32>::Contains(T)
extern "C"  bool List_1_Contains_m1882654501_gshared (List_1_t449703515 * __this, int32_t ___item0, const RuntimeMethod* method)
{
	{
		Int32U5BU5D_t2856113350* L_0 = (Int32U5BU5D_t2856113350*)__this->get__items_1();
		int32_t L_1 = ___item0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		int32_t L_3 = ((  int32_t (*) (RuntimeObject * /* static, unused */, Int32U5BU5D_t2856113350*, int32_t, int32_t, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 22)->methodPointer)(NULL /*static, unused*/, (Int32U5BU5D_t2856113350*)L_0, (int32_t)L_1, (int32_t)0, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 22));
		return (bool)((((int32_t)((((int32_t)L_3) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void System.Collections.Generic.List`1<System.Int32>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m611965896_gshared (List_1_t449703515 * __this, Int32U5BU5D_t2856113350* ___array0, int32_t ___arrayIndex1, const RuntimeMethod* method)
{
	{
		Int32U5BU5D_t2856113350* L_0 = (Int32U5BU5D_t2856113350*)__this->get__items_1();
		Int32U5BU5D_t2856113350* L_1 = ___array0;
		int32_t L_2 = ___arrayIndex1;
		int32_t L_3 = (int32_t)__this->get__size_2();
		Array_Copy_m1287631566(NULL /*static, unused*/, (RuntimeArray *)(RuntimeArray *)L_0, (int32_t)0, (RuntimeArray *)(RuntimeArray *)L_1, (int32_t)L_2, (int32_t)L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<System.Int32>::GetEnumerator()
extern "C"  Enumerator_t2209518572  List_1_GetEnumerator_m3386766211_gshared (List_1_t449703515 * __this, const RuntimeMethod* method)
{
	{
		Enumerator_t2209518572  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Enumerator__ctor_m223128127((&L_0), (List_1_t449703515 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 23));
		return L_0;
	}
}
// System.Int32 System.Collections.Generic.List`1<System.Int32>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m3378712235_gshared (List_1_t449703515 * __this, int32_t ___item0, const RuntimeMethod* method)
{
	{
		Int32U5BU5D_t2856113350* L_0 = (Int32U5BU5D_t2856113350*)__this->get__items_1();
		int32_t L_1 = ___item0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		int32_t L_3 = ((  int32_t (*) (RuntimeObject * /* static, unused */, Int32U5BU5D_t2856113350*, int32_t, int32_t, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 22)->methodPointer)(NULL /*static, unused*/, (Int32U5BU5D_t2856113350*)L_0, (int32_t)L_1, (int32_t)0, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 22));
		return L_3;
	}
}
// System.Void System.Collections.Generic.List`1<System.Int32>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m1179232910_gshared (List_1_t449703515 * __this, int32_t ___start0, int32_t ___delta1, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___delta1;
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_000c;
		}
	}
	{
		int32_t L_1 = ___start0;
		int32_t L_2 = ___delta1;
		___start0 = (int32_t)((int32_t)((int32_t)L_1-(int32_t)L_2));
	}

IL_000c:
	{
		int32_t L_3 = ___start0;
		int32_t L_4 = (int32_t)__this->get__size_2();
		if ((((int32_t)L_3) >= ((int32_t)L_4)))
		{
			goto IL_0035;
		}
	}
	{
		Int32U5BU5D_t2856113350* L_5 = (Int32U5BU5D_t2856113350*)__this->get__items_1();
		int32_t L_6 = ___start0;
		Int32U5BU5D_t2856113350* L_7 = (Int32U5BU5D_t2856113350*)__this->get__items_1();
		int32_t L_8 = ___start0;
		int32_t L_9 = ___delta1;
		int32_t L_10 = (int32_t)__this->get__size_2();
		int32_t L_11 = ___start0;
		Array_Copy_m1287631566(NULL /*static, unused*/, (RuntimeArray *)(RuntimeArray *)L_5, (int32_t)L_6, (RuntimeArray *)(RuntimeArray *)L_7, (int32_t)((int32_t)((int32_t)L_8+(int32_t)L_9)), (int32_t)((int32_t)((int32_t)L_10-(int32_t)L_11)), /*hidden argument*/NULL);
	}

IL_0035:
	{
		int32_t L_12 = (int32_t)__this->get__size_2();
		int32_t L_13 = ___delta1;
		__this->set__size_2(((int32_t)((int32_t)L_12+(int32_t)L_13)));
		int32_t L_14 = ___delta1;
		if ((((int32_t)L_14) >= ((int32_t)0)))
		{
			goto IL_005d;
		}
	}
	{
		Int32U5BU5D_t2856113350* L_15 = (Int32U5BU5D_t2856113350*)__this->get__items_1();
		int32_t L_16 = (int32_t)__this->get__size_2();
		int32_t L_17 = ___delta1;
		Array_Clear_m305290227(NULL /*static, unused*/, (RuntimeArray *)(RuntimeArray *)L_15, (int32_t)L_16, (int32_t)((-L_17)), /*hidden argument*/NULL);
	}

IL_005d:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Int32>::CheckIndex(System.Int32)
extern "C"  void List_1_CheckIndex_m2334255516_gshared (List_1_t449703515 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_CheckIndex_m2334255516_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___index0;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_0013;
		}
	}
	{
		int32_t L_1 = ___index0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_1) > ((uint32_t)L_2))))
		{
			goto IL_001e;
		}
	}

IL_0013:
	{
		ArgumentOutOfRangeException_t1745463579 * L_3 = (ArgumentOutOfRangeException_t1745463579 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t1745463579_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2829693980(L_3, (String_t*)_stringLiteral1171314202, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001e:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Int32>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m148687994_gshared (List_1_t449703515 * __this, int32_t ___index0, int32_t ___item1, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t449703515 *)__this);
		((  void (*) (List_1_t449703515 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((List_1_t449703515 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		int32_t L_1 = (int32_t)__this->get__size_2();
		Int32U5BU5D_t2856113350* L_2 = (Int32U5BU5D_t2856113350*)__this->get__items_1();
		NullCheck(L_2);
		if ((!(((uint32_t)L_1) == ((uint32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_2)->max_length))))))))
		{
			goto IL_0021;
		}
	}
	{
		NullCheck((List_1_t449703515 *)__this);
		((  void (*) (List_1_t449703515 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13)->methodPointer)((List_1_t449703515 *)__this, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13));
	}

IL_0021:
	{
		int32_t L_3 = ___index0;
		NullCheck((List_1_t449703515 *)__this);
		((  void (*) (List_1_t449703515 *, int32_t, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 24)->methodPointer)((List_1_t449703515 *)__this, (int32_t)L_3, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 24));
		Int32U5BU5D_t2856113350* L_4 = (Int32U5BU5D_t2856113350*)__this->get__items_1();
		int32_t L_5 = ___index0;
		int32_t L_6 = ___item1;
		NullCheck(L_4);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (int32_t)L_6);
		int32_t L_7 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_7+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Int32>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_CheckCollection_m2677906726_gshared (List_1_t449703515 * __this, RuntimeObject* ___collection0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_CheckCollection_m2677906726_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = ___collection0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t517973450 * L_1 = (ArgumentNullException_t517973450 *)il2cpp_codegen_object_new(ArgumentNullException_t517973450_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m2607242403(L_1, (String_t*)_stringLiteral2657729115, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		return;
	}
}
// System.Boolean System.Collections.Generic.List`1<System.Int32>::Remove(T)
extern "C"  bool List_1_Remove_m2143690351_gshared (List_1_t449703515 * __this, int32_t ___item0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___item0;
		NullCheck((List_1_t449703515 *)__this);
		int32_t L_1 = ((  int32_t (*) (List_1_t449703515 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((List_1_t449703515 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)(-1))))
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_3 = V_0;
		NullCheck((List_1_t449703515 *)__this);
		((  void (*) (List_1_t449703515 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 25)->methodPointer)((List_1_t449703515 *)__this, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 25));
	}

IL_0016:
	{
		int32_t L_4 = V_0;
		return (bool)((((int32_t)((((int32_t)L_4) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void System.Collections.Generic.List`1<System.Int32>::RemoveAt(System.Int32)
extern "C"  void List_1_RemoveAt_m2834769719_gshared (List_1_t449703515 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_RemoveAt_m2834769719_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___index0;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_0013;
		}
	}
	{
		int32_t L_1 = ___index0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_1) >= ((uint32_t)L_2))))
		{
			goto IL_001e;
		}
	}

IL_0013:
	{
		ArgumentOutOfRangeException_t1745463579 * L_3 = (ArgumentOutOfRangeException_t1745463579 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t1745463579_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2829693980(L_3, (String_t*)_stringLiteral1171314202, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001e:
	{
		int32_t L_4 = ___index0;
		NullCheck((List_1_t449703515 *)__this);
		((  void (*) (List_1_t449703515 *, int32_t, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 24)->methodPointer)((List_1_t449703515 *)__this, (int32_t)L_4, (int32_t)(-1), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 24));
		Int32U5BU5D_t2856113350* L_5 = (Int32U5BU5D_t2856113350*)__this->get__items_1();
		int32_t L_6 = (int32_t)__this->get__size_2();
		Array_Clear_m305290227(NULL /*static, unused*/, (RuntimeArray *)(RuntimeArray *)L_5, (int32_t)L_6, (int32_t)1, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_7+(int32_t)1)));
		return;
	}
}
// T[] System.Collections.Generic.List`1<System.Int32>::ToArray()
extern "C"  Int32U5BU5D_t2856113350* List_1_ToArray_m3554048317_gshared (List_1_t449703515 * __this, const RuntimeMethod* method)
{
	Int32U5BU5D_t2856113350* V_0 = NULL;
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		V_0 = (Int32U5BU5D_t2856113350*)((Int32U5BU5D_t2856113350*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1), (uint32_t)L_0));
		Int32U5BU5D_t2856113350* L_1 = (Int32U5BU5D_t2856113350*)__this->get__items_1();
		Int32U5BU5D_t2856113350* L_2 = V_0;
		int32_t L_3 = (int32_t)__this->get__size_2();
		Array_Copy_m392395275(NULL /*static, unused*/, (RuntimeArray *)(RuntimeArray *)L_1, (RuntimeArray *)(RuntimeArray *)L_2, (int32_t)L_3, /*hidden argument*/NULL);
		Int32U5BU5D_t2856113350* L_4 = V_0;
		return L_4;
	}
}
// System.Int32 System.Collections.Generic.List`1<System.Int32>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m3451588432_gshared (List_1_t449703515 * __this, const RuntimeMethod* method)
{
	{
		Int32U5BU5D_t2856113350* L_0 = (Int32U5BU5D_t2856113350*)__this->get__items_1();
		NullCheck(L_0);
		return (((int32_t)((int32_t)(((RuntimeArray *)L_0)->max_length))));
	}
}
// System.Void System.Collections.Generic.List`1<System.Int32>::set_Capacity(System.Int32)
extern "C"  void List_1_set_Capacity_m885225211_gshared (List_1_t449703515 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_set_Capacity_m885225211_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___value0;
		int32_t L_1 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_0) < ((uint32_t)L_1))))
		{
			goto IL_0012;
		}
	}
	{
		ArgumentOutOfRangeException_t1745463579 * L_2 = (ArgumentOutOfRangeException_t1745463579 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t1745463579_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3684908528(L_2, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0012:
	{
		Int32U5BU5D_t2856113350** L_3 = (Int32U5BU5D_t2856113350**)__this->get_address_of__items_1();
		int32_t L_4 = ___value0;
		((  void (*) (RuntimeObject * /* static, unused */, Int32U5BU5D_t2856113350**, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 26)->methodPointer)(NULL /*static, unused*/, (Int32U5BU5D_t2856113350**)L_3, (int32_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 26));
		return;
	}
}
// System.Int32 System.Collections.Generic.List`1<System.Int32>::get_Count()
extern "C"  int32_t List_1_get_Count_m1788667731_gshared (List_1_t449703515 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		return L_0;
	}
}
// T System.Collections.Generic.List`1<System.Int32>::get_Item(System.Int32)
extern "C"  int32_t List_1_get_Item_m4017031976_gshared (List_1_t449703515 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_get_Item_m4017031976_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___index0;
		int32_t L_1 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t1745463579 * L_2 = (ArgumentOutOfRangeException_t1745463579 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t1745463579_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2829693980(L_2, (String_t*)_stringLiteral1171314202, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		Int32U5BU5D_t2856113350* L_3 = (Int32U5BU5D_t2856113350*)__this->get__items_1();
		int32_t L_4 = ___index0;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		int32_t L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		return L_6;
	}
}
// System.Void System.Collections.Generic.List`1<System.Int32>::set_Item(System.Int32,T)
extern "C"  void List_1_set_Item_m3378504976_gshared (List_1_t449703515 * __this, int32_t ___index0, int32_t ___value1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_set_Item_m3378504976_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t449703515 *)__this);
		((  void (*) (List_1_t449703515 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((List_1_t449703515 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		int32_t L_1 = ___index0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_1) == ((uint32_t)L_2))))
		{
			goto IL_001e;
		}
	}
	{
		ArgumentOutOfRangeException_t1745463579 * L_3 = (ArgumentOutOfRangeException_t1745463579 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t1745463579_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2829693980(L_3, (String_t*)_stringLiteral1171314202, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001e:
	{
		Int32U5BU5D_t2856113350* L_4 = (Int32U5BU5D_t2856113350*)__this->get__items_1();
		int32_t L_5 = ___index0;
		int32_t L_6 = ___value1;
		NullCheck(L_4);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (int32_t)L_6);
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
extern "C"  void List_1__ctor_m4124788479_gshared (List_1_t1174585319 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m3446193457((RuntimeObject *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		ObjectU5BU5D_t4290686858* L_0 = ((List_1_t1174585319_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0)))->get_EmptyArray_4();
		__this->set__items_1(L_0);
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor(System.Int32)
extern "C"  void List_1__ctor_m628538615_gshared (List_1_t1174585319 * __this, int32_t ___capacity0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1__ctor_m628538615_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m3446193457((RuntimeObject *)__this, /*hidden argument*/NULL);
		int32_t L_0 = ___capacity0;
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		ArgumentOutOfRangeException_t1745463579 * L_1 = (ArgumentOutOfRangeException_t1745463579 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t1745463579_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2829693980(L_1, (String_t*)_stringLiteral2929636671, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = ___capacity0;
		__this->set__items_1(((ObjectU5BU5D_t4290686858*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1), (uint32_t)L_2)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Object>::.cctor()
extern "C"  void List_1__cctor_m2338257204_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		((List_1_t1174585319_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)))->set_EmptyArray_4(((ObjectU5BU5D_t4290686858*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (uint32_t)0)));
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<System.Object>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  RuntimeObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1039389195_gshared (List_1_t1174585319 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((List_1_t1174585319 *)__this);
		Enumerator_t2934400376  L_0 = ((  Enumerator_t2934400376  (*) (List_1_t1174585319 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((List_1_t1174585319 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		Enumerator_t2934400376  L_1 = L_0;
		RuntimeObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), &L_1);
		return (RuntimeObject*)L_2;
	}
}
// System.Void System.Collections.Generic.List`1<System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m2831225823_gshared (List_1_t1174585319 * __this, RuntimeArray * ___array0, int32_t ___arrayIndex1, const RuntimeMethod* method)
{
	{
		ObjectU5BU5D_t4290686858* L_0 = (ObjectU5BU5D_t4290686858*)__this->get__items_1();
		RuntimeArray * L_1 = ___array0;
		int32_t L_2 = ___arrayIndex1;
		int32_t L_3 = (int32_t)__this->get__size_2();
		Array_Copy_m1287631566(NULL /*static, unused*/, (RuntimeArray *)(RuntimeArray *)L_0, (int32_t)0, (RuntimeArray *)L_1, (int32_t)L_2, (int32_t)L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.List`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  RuntimeObject* List_1_System_Collections_IEnumerable_GetEnumerator_m2170893257_gshared (List_1_t1174585319 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((List_1_t1174585319 *)__this);
		Enumerator_t2934400376  L_0 = ((  Enumerator_t2934400376  (*) (List_1_t1174585319 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((List_1_t1174585319 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		Enumerator_t2934400376  L_1 = L_0;
		RuntimeObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), &L_1);
		return (RuntimeObject*)L_2;
	}
}
// System.Int32 System.Collections.Generic.List`1<System.Object>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_Add_m1495383345_gshared (List_1_t1174585319 * __this, RuntimeObject * ___item0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_Add_m1495383345_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Exception_t3361176243 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3361176243 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			RuntimeObject * L_0 = ___item0;
			NullCheck((List_1_t1174585319 *)__this);
			((  void (*) (List_1_t1174585319 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((List_1_t1174585319 *)__this, (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
			int32_t L_1 = (int32_t)__this->get__size_2();
			V_0 = (int32_t)((int32_t)((int32_t)L_1-(int32_t)1));
			goto IL_0036;
		}

IL_001a:
		{
			; // IL_001a: leave IL_002b
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3361176243 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t2549690990_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_001f;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t1009887698_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_0025;
		throw e;
	}

CATCH_001f:
	{ // begin catch(System.NullReferenceException)
		goto IL_002b;
	} // end catch (depth: 1)

CATCH_0025:
	{ // begin catch(System.InvalidCastException)
		goto IL_002b;
	} // end catch (depth: 1)

IL_002b:
	{
		ArgumentException_t1372035057 * L_2 = (ArgumentException_t1372035057 *)il2cpp_codegen_object_new(ArgumentException_t1372035057_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m775170528(L_2, (String_t*)_stringLiteral3684952963, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0036:
	{
		int32_t L_3 = V_0;
		return L_3;
	}
}
// System.Boolean System.Collections.Generic.List`1<System.Object>::System.Collections.IList.Contains(System.Object)
extern "C"  bool List_1_System_Collections_IList_Contains_m1442344937_gshared (List_1_t1174585319 * __this, RuntimeObject * ___item0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_Contains_m1442344937_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Exception_t3361176243 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3361176243 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			RuntimeObject * L_0 = ___item0;
			NullCheck((List_1_t1174585319 *)__this);
			bool L_1 = ((  bool (*) (List_1_t1174585319 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t1174585319 *)__this, (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
			V_0 = (bool)L_1;
			goto IL_0025;
		}

IL_0012:
		{
			; // IL_0012: leave IL_0023
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3361176243 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t2549690990_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_0017;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t1009887698_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_001d;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.NullReferenceException)
		goto IL_0023;
	} // end catch (depth: 1)

CATCH_001d:
	{ // begin catch(System.InvalidCastException)
		goto IL_0023;
	} // end catch (depth: 1)

IL_0023:
	{
		return (bool)0;
	}

IL_0025:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Int32 System.Collections.Generic.List`1<System.Object>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m2504690593_gshared (List_1_t1174585319 * __this, RuntimeObject * ___item0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_IndexOf_m2504690593_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Exception_t3361176243 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3361176243 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			RuntimeObject * L_0 = ___item0;
			NullCheck((List_1_t1174585319 *)__this);
			int32_t L_1 = ((  int32_t (*) (List_1_t1174585319 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((List_1_t1174585319 *)__this, (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
			V_0 = (int32_t)L_1;
			goto IL_0025;
		}

IL_0012:
		{
			; // IL_0012: leave IL_0023
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3361176243 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t2549690990_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_0017;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t1009887698_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_001d;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.NullReferenceException)
		goto IL_0023;
	} // end catch (depth: 1)

CATCH_001d:
	{ // begin catch(System.InvalidCastException)
		goto IL_0023;
	} // end catch (depth: 1)

IL_0023:
	{
		return (-1);
	}

IL_0025:
	{
		int32_t L_2 = V_0;
		return L_2;
	}
}
// System.Void System.Collections.Generic.List`1<System.Object>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_Insert_m4294775317_gshared (List_1_t1174585319 * __this, int32_t ___index0, RuntimeObject * ___item1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_Insert_m4294775317_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Exception_t3361176243 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3361176243 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t1174585319 *)__this);
		((  void (*) (List_1_t1174585319 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((List_1_t1174585319 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			int32_t L_1 = ___index0;
			RuntimeObject * L_2 = ___item1;
			NullCheck((List_1_t1174585319 *)__this);
			((  void (*) (List_1_t1174585319 *, int32_t, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((List_1_t1174585319 *)__this, (int32_t)L_1, (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_2, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
			goto IL_0035;
		}

IL_0019:
		{
			; // IL_0019: leave IL_002a
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3361176243 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t2549690990_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_001e;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t1009887698_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_0024;
		throw e;
	}

CATCH_001e:
	{ // begin catch(System.NullReferenceException)
		goto IL_002a;
	} // end catch (depth: 1)

CATCH_0024:
	{ // begin catch(System.InvalidCastException)
		goto IL_002a;
	} // end catch (depth: 1)

IL_002a:
	{
		ArgumentException_t1372035057 * L_3 = (ArgumentException_t1372035057 *)il2cpp_codegen_object_new(ArgumentException_t1372035057_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m775170528(L_3, (String_t*)_stringLiteral3684952963, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0035:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Object>::System.Collections.IList.Remove(System.Object)
extern "C"  void List_1_System_Collections_IList_Remove_m2638065942_gshared (List_1_t1174585319 * __this, RuntimeObject * ___item0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_Remove_m2638065942_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Exception_t3361176243 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3361176243 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			RuntimeObject * L_0 = ___item0;
			NullCheck((List_1_t1174585319 *)__this);
			((  bool (*) (List_1_t1174585319 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((List_1_t1174585319 *)__this, (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
			goto IL_0023;
		}

IL_0012:
		{
			; // IL_0012: leave IL_0023
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3361176243 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t2549690990_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_0017;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t1009887698_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_001d;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.NullReferenceException)
		goto IL_0023;
	} // end catch (depth: 1)

CATCH_001d:
	{ // begin catch(System.InvalidCastException)
		goto IL_0023;
	} // end catch (depth: 1)

IL_0023:
	{
		return;
	}
}
// System.Boolean System.Collections.Generic.List`1<System.Object>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3411257674_gshared (List_1_t1174585319 * __this, const RuntimeMethod* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Generic.List`1<System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  RuntimeObject * List_1_System_Collections_ICollection_get_SyncRoot_m1586963370_gshared (List_1_t1174585319 * __this, const RuntimeMethod* method)
{
	{
		return __this;
	}
}
// System.Object System.Collections.Generic.List`1<System.Object>::System.Collections.IList.get_Item(System.Int32)
extern "C"  RuntimeObject * List_1_System_Collections_IList_get_Item_m1684017049_gshared (List_1_t1174585319 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t1174585319 *)__this);
		RuntimeObject * L_1 = ((  RuntimeObject * (*) (List_1_t1174585319 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((List_1_t1174585319 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		return L_1;
	}
}
// System.Void System.Collections.Generic.List`1<System.Object>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_set_Item_m98177035_gshared (List_1_t1174585319 * __this, int32_t ___index0, RuntimeObject * ___value1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_set_Item_m98177035_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Exception_t3361176243 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3361176243 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			int32_t L_0 = ___index0;
			RuntimeObject * L_1 = ___value1;
			NullCheck((List_1_t1174585319 *)__this);
			((  void (*) (List_1_t1174585319 *, int32_t, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((List_1_t1174585319 *)__this, (int32_t)L_0, (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
			goto IL_002e;
		}

IL_0012:
		{
			; // IL_0012: leave IL_0023
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3361176243 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t2549690990_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_0017;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t1009887698_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_001d;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.NullReferenceException)
		goto IL_0023;
	} // end catch (depth: 1)

CATCH_001d:
	{ // begin catch(System.InvalidCastException)
		goto IL_0023;
	} // end catch (depth: 1)

IL_0023:
	{
		ArgumentException_t1372035057 * L_2 = (ArgumentException_t1372035057 *)il2cpp_codegen_object_new(ArgumentException_t1372035057_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m775170528(L_2, (String_t*)_stringLiteral3197723981, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_002e:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Object>::Add(T)
extern "C"  void List_1_Add_m2394526366_gshared (List_1_t1174585319 * __this, RuntimeObject * ___item0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		ObjectU5BU5D_t4290686858* L_1 = (ObjectU5BU5D_t4290686858*)__this->get__items_1();
		NullCheck(L_1);
		if ((!(((uint32_t)L_0) == ((uint32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_1)->max_length))))))))
		{
			goto IL_001a;
		}
	}
	{
		NullCheck((List_1_t1174585319 *)__this);
		((  void (*) (List_1_t1174585319 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13)->methodPointer)((List_1_t1174585319 *)__this, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13));
	}

IL_001a:
	{
		ObjectU5BU5D_t4290686858* L_2 = (ObjectU5BU5D_t4290686858*)__this->get__items_1();
		int32_t L_3 = (int32_t)__this->get__size_2();
		int32_t L_4 = (int32_t)L_3;
		V_0 = (int32_t)L_4;
		__this->set__size_2(((int32_t)((int32_t)L_4+(int32_t)1)));
		int32_t L_5 = V_0;
		RuntimeObject * L_6 = ___item0;
		NullCheck(L_2);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (RuntimeObject *)L_6);
		int32_t L_7 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_7+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Object>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m1717374771_gshared (List_1_t1174585319 * __this, int32_t ___newCount0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		int32_t L_1 = ___newCount0;
		V_0 = (int32_t)((int32_t)((int32_t)L_0+(int32_t)L_1));
		int32_t L_2 = V_0;
		ObjectU5BU5D_t4290686858* L_3 = (ObjectU5BU5D_t4290686858*)__this->get__items_1();
		NullCheck(L_3);
		if ((((int32_t)L_2) <= ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_3)->max_length)))))))
		{
			goto IL_0031;
		}
	}
	{
		NullCheck((List_1_t1174585319 *)__this);
		int32_t L_4 = ((  int32_t (*) (List_1_t1174585319 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14)->methodPointer)((List_1_t1174585319 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14));
		int32_t L_5 = Math_Max_m3051295998(NULL /*static, unused*/, (int32_t)((int32_t)((int32_t)L_4*(int32_t)2)), (int32_t)4, /*hidden argument*/NULL);
		int32_t L_6 = V_0;
		int32_t L_7 = Math_Max_m3051295998(NULL /*static, unused*/, (int32_t)L_5, (int32_t)L_6, /*hidden argument*/NULL);
		NullCheck((List_1_t1174585319 *)__this);
		((  void (*) (List_1_t1174585319 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15)->methodPointer)((List_1_t1174585319 *)__this, (int32_t)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15));
	}

IL_0031:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Object>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m530721861_gshared (List_1_t1174585319 * __this, RuntimeObject* ___collection0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		RuntimeObject* L_0 = ___collection0;
		NullCheck((RuntimeObject*)L_0);
		int32_t L_1 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Object>::get_Count() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 16), (RuntimeObject*)L_0);
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		if (L_2)
		{
			goto IL_000e;
		}
	}
	{
		return;
	}

IL_000e:
	{
		int32_t L_3 = V_0;
		NullCheck((List_1_t1174585319 *)__this);
		((  void (*) (List_1_t1174585319 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13)->methodPointer)((List_1_t1174585319 *)__this, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13));
		RuntimeObject* L_4 = ___collection0;
		ObjectU5BU5D_t4290686858* L_5 = (ObjectU5BU5D_t4290686858*)__this->get__items_1();
		int32_t L_6 = (int32_t)__this->get__size_2();
		NullCheck((RuntimeObject*)L_4);
		InterfaceActionInvoker2< ObjectU5BU5D_t4290686858*, int32_t >::Invoke(5 /* System.Void System.Collections.Generic.ICollection`1<System.Object>::CopyTo(T[],System.Int32) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 16), (RuntimeObject*)L_4, (ObjectU5BU5D_t4290686858*)L_5, (int32_t)L_6);
		int32_t L_7 = (int32_t)__this->get__size_2();
		int32_t L_8 = V_0;
		__this->set__size_2(((int32_t)((int32_t)L_7+(int32_t)L_8)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Object>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddEnumerable_m761171694_gshared (List_1_t1174585319 * __this, RuntimeObject* ___enumerable0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_AddEnumerable_m761171694_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject * V_0 = NULL;
	RuntimeObject* V_1 = NULL;
	Exception_t3361176243 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3361176243 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		RuntimeObject* L_0 = ___enumerable0;
		NullCheck((RuntimeObject*)L_0);
		RuntimeObject* L_1 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 17), (RuntimeObject*)L_0);
		V_1 = (RuntimeObject*)L_1;
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			goto IL_001a;
		}

IL_000c:
		{
			RuntimeObject* L_2 = V_1;
			NullCheck((RuntimeObject*)L_2);
			RuntimeObject * L_3 = InterfaceFuncInvoker0< RuntimeObject * >::Invoke(0 /* T System.Collections.Generic.IEnumerator`1<System.Object>::get_Current() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 18), (RuntimeObject*)L_2);
			V_0 = (RuntimeObject *)L_3;
			RuntimeObject * L_4 = V_0;
			NullCheck((List_1_t1174585319 *)__this);
			((  void (*) (List_1_t1174585319 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((List_1_t1174585319 *)__this, (RuntimeObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		}

IL_001a:
		{
			RuntimeObject* L_5 = V_1;
			NullCheck((RuntimeObject*)L_5);
			bool L_6 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t4262330110_il2cpp_TypeInfo_var, (RuntimeObject*)L_5);
			if (L_6)
			{
				goto IL_000c;
			}
		}

IL_0025:
		{
			IL2CPP_LEAVE(0x35, FINALLY_002a);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3361176243 *)e.ex;
		goto FINALLY_002a;
	}

FINALLY_002a:
	{ // begin finally (depth: 1)
		{
			RuntimeObject* L_7 = V_1;
			if (L_7)
			{
				goto IL_002e;
			}
		}

IL_002d:
		{
			IL2CPP_END_FINALLY(42)
		}

IL_002e:
		{
			RuntimeObject* L_8 = V_1;
			NullCheck((RuntimeObject*)L_8);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t318870991_il2cpp_TypeInfo_var, (RuntimeObject*)L_8);
			IL2CPP_END_FINALLY(42)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(42)
	{
		IL2CPP_JUMP_TBL(0x35, IL_0035)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3361176243 *)
	}

IL_0035:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Object>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m2667872413_gshared (List_1_t1174585319 * __this, RuntimeObject* ___collection0, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		RuntimeObject* L_0 = ___collection0;
		NullCheck((List_1_t1174585319 *)__this);
		((  void (*) (List_1_t1174585319 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19)->methodPointer)((List_1_t1174585319 *)__this, (RuntimeObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19));
		RuntimeObject* L_1 = ___collection0;
		V_0 = (RuntimeObject*)((RuntimeObject*)IsInst((RuntimeObject*)L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 16)));
		RuntimeObject* L_2 = V_0;
		if (!L_2)
		{
			goto IL_0020;
		}
	}
	{
		RuntimeObject* L_3 = V_0;
		NullCheck((List_1_t1174585319 *)__this);
		((  void (*) (List_1_t1174585319 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 20)->methodPointer)((List_1_t1174585319 *)__this, (RuntimeObject*)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 20));
		goto IL_0027;
	}

IL_0020:
	{
		RuntimeObject* L_4 = ___collection0;
		NullCheck((List_1_t1174585319 *)__this);
		((  void (*) (List_1_t1174585319 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 21)->methodPointer)((List_1_t1174585319 *)__this, (RuntimeObject*)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 21));
	}

IL_0027:
	{
		int32_t L_5 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_5+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Object>::Clear()
extern "C"  void List_1_Clear_m2295507134_gshared (List_1_t1174585319 * __this, const RuntimeMethod* method)
{
	{
		ObjectU5BU5D_t4290686858* L_0 = (ObjectU5BU5D_t4290686858*)__this->get__items_1();
		ObjectU5BU5D_t4290686858* L_1 = (ObjectU5BU5D_t4290686858*)__this->get__items_1();
		NullCheck(L_1);
		Array_Clear_m305290227(NULL /*static, unused*/, (RuntimeArray *)(RuntimeArray *)L_0, (int32_t)0, (int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_1)->max_length)))), /*hidden argument*/NULL);
		__this->set__size_2(0);
		int32_t L_2 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_2+(int32_t)1)));
		return;
	}
}
// System.Boolean System.Collections.Generic.List`1<System.Object>::Contains(T)
extern "C"  bool List_1_Contains_m3093124920_gshared (List_1_t1174585319 * __this, RuntimeObject * ___item0, const RuntimeMethod* method)
{
	{
		ObjectU5BU5D_t4290686858* L_0 = (ObjectU5BU5D_t4290686858*)__this->get__items_1();
		RuntimeObject * L_1 = ___item0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		int32_t L_3 = ((  int32_t (*) (RuntimeObject * /* static, unused */, ObjectU5BU5D_t4290686858*, RuntimeObject *, int32_t, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 22)->methodPointer)(NULL /*static, unused*/, (ObjectU5BU5D_t4290686858*)L_0, (RuntimeObject *)L_1, (int32_t)0, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 22));
		return (bool)((((int32_t)((((int32_t)L_3) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void System.Collections.Generic.List`1<System.Object>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m1447901471_gshared (List_1_t1174585319 * __this, ObjectU5BU5D_t4290686858* ___array0, int32_t ___arrayIndex1, const RuntimeMethod* method)
{
	{
		ObjectU5BU5D_t4290686858* L_0 = (ObjectU5BU5D_t4290686858*)__this->get__items_1();
		ObjectU5BU5D_t4290686858* L_1 = ___array0;
		int32_t L_2 = ___arrayIndex1;
		int32_t L_3 = (int32_t)__this->get__size_2();
		Array_Copy_m1287631566(NULL /*static, unused*/, (RuntimeArray *)(RuntimeArray *)L_0, (int32_t)0, (RuntimeArray *)(RuntimeArray *)L_1, (int32_t)L_2, (int32_t)L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<System.Object>::GetEnumerator()
extern "C"  Enumerator_t2934400376  List_1_GetEnumerator_m859558683_gshared (List_1_t1174585319 * __this, const RuntimeMethod* method)
{
	{
		Enumerator_t2934400376  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Enumerator__ctor_m3625216372((&L_0), (List_1_t1174585319 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 23));
		return L_0;
	}
}
// System.Int32 System.Collections.Generic.List`1<System.Object>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m249675008_gshared (List_1_t1174585319 * __this, RuntimeObject * ___item0, const RuntimeMethod* method)
{
	{
		ObjectU5BU5D_t4290686858* L_0 = (ObjectU5BU5D_t4290686858*)__this->get__items_1();
		RuntimeObject * L_1 = ___item0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		int32_t L_3 = ((  int32_t (*) (RuntimeObject * /* static, unused */, ObjectU5BU5D_t4290686858*, RuntimeObject *, int32_t, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 22)->methodPointer)(NULL /*static, unused*/, (ObjectU5BU5D_t4290686858*)L_0, (RuntimeObject *)L_1, (int32_t)0, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 22));
		return L_3;
	}
}
// System.Void System.Collections.Generic.List`1<System.Object>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m640961218_gshared (List_1_t1174585319 * __this, int32_t ___start0, int32_t ___delta1, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___delta1;
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_000c;
		}
	}
	{
		int32_t L_1 = ___start0;
		int32_t L_2 = ___delta1;
		___start0 = (int32_t)((int32_t)((int32_t)L_1-(int32_t)L_2));
	}

IL_000c:
	{
		int32_t L_3 = ___start0;
		int32_t L_4 = (int32_t)__this->get__size_2();
		if ((((int32_t)L_3) >= ((int32_t)L_4)))
		{
			goto IL_0035;
		}
	}
	{
		ObjectU5BU5D_t4290686858* L_5 = (ObjectU5BU5D_t4290686858*)__this->get__items_1();
		int32_t L_6 = ___start0;
		ObjectU5BU5D_t4290686858* L_7 = (ObjectU5BU5D_t4290686858*)__this->get__items_1();
		int32_t L_8 = ___start0;
		int32_t L_9 = ___delta1;
		int32_t L_10 = (int32_t)__this->get__size_2();
		int32_t L_11 = ___start0;
		Array_Copy_m1287631566(NULL /*static, unused*/, (RuntimeArray *)(RuntimeArray *)L_5, (int32_t)L_6, (RuntimeArray *)(RuntimeArray *)L_7, (int32_t)((int32_t)((int32_t)L_8+(int32_t)L_9)), (int32_t)((int32_t)((int32_t)L_10-(int32_t)L_11)), /*hidden argument*/NULL);
	}

IL_0035:
	{
		int32_t L_12 = (int32_t)__this->get__size_2();
		int32_t L_13 = ___delta1;
		__this->set__size_2(((int32_t)((int32_t)L_12+(int32_t)L_13)));
		int32_t L_14 = ___delta1;
		if ((((int32_t)L_14) >= ((int32_t)0)))
		{
			goto IL_005d;
		}
	}
	{
		ObjectU5BU5D_t4290686858* L_15 = (ObjectU5BU5D_t4290686858*)__this->get__items_1();
		int32_t L_16 = (int32_t)__this->get__size_2();
		int32_t L_17 = ___delta1;
		Array_Clear_m305290227(NULL /*static, unused*/, (RuntimeArray *)(RuntimeArray *)L_15, (int32_t)L_16, (int32_t)((-L_17)), /*hidden argument*/NULL);
	}

IL_005d:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Object>::CheckIndex(System.Int32)
extern "C"  void List_1_CheckIndex_m1105134347_gshared (List_1_t1174585319 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_CheckIndex_m1105134347_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___index0;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_0013;
		}
	}
	{
		int32_t L_1 = ___index0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_1) > ((uint32_t)L_2))))
		{
			goto IL_001e;
		}
	}

IL_0013:
	{
		ArgumentOutOfRangeException_t1745463579 * L_3 = (ArgumentOutOfRangeException_t1745463579 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t1745463579_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2829693980(L_3, (String_t*)_stringLiteral1171314202, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001e:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Object>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m178135930_gshared (List_1_t1174585319 * __this, int32_t ___index0, RuntimeObject * ___item1, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t1174585319 *)__this);
		((  void (*) (List_1_t1174585319 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((List_1_t1174585319 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		int32_t L_1 = (int32_t)__this->get__size_2();
		ObjectU5BU5D_t4290686858* L_2 = (ObjectU5BU5D_t4290686858*)__this->get__items_1();
		NullCheck(L_2);
		if ((!(((uint32_t)L_1) == ((uint32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_2)->max_length))))))))
		{
			goto IL_0021;
		}
	}
	{
		NullCheck((List_1_t1174585319 *)__this);
		((  void (*) (List_1_t1174585319 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13)->methodPointer)((List_1_t1174585319 *)__this, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13));
	}

IL_0021:
	{
		int32_t L_3 = ___index0;
		NullCheck((List_1_t1174585319 *)__this);
		((  void (*) (List_1_t1174585319 *, int32_t, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 24)->methodPointer)((List_1_t1174585319 *)__this, (int32_t)L_3, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 24));
		ObjectU5BU5D_t4290686858* L_4 = (ObjectU5BU5D_t4290686858*)__this->get__items_1();
		int32_t L_5 = ___index0;
		RuntimeObject * L_6 = ___item1;
		NullCheck(L_4);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (RuntimeObject *)L_6);
		int32_t L_7 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_7+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Object>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_CheckCollection_m54451682_gshared (List_1_t1174585319 * __this, RuntimeObject* ___collection0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_CheckCollection_m54451682_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = ___collection0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t517973450 * L_1 = (ArgumentNullException_t517973450 *)il2cpp_codegen_object_new(ArgumentNullException_t517973450_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m2607242403(L_1, (String_t*)_stringLiteral2657729115, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		return;
	}
}
// System.Boolean System.Collections.Generic.List`1<System.Object>::Remove(T)
extern "C"  bool List_1_Remove_m2429580421_gshared (List_1_t1174585319 * __this, RuntimeObject * ___item0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		RuntimeObject * L_0 = ___item0;
		NullCheck((List_1_t1174585319 *)__this);
		int32_t L_1 = ((  int32_t (*) (List_1_t1174585319 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((List_1_t1174585319 *)__this, (RuntimeObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)(-1))))
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_3 = V_0;
		NullCheck((List_1_t1174585319 *)__this);
		((  void (*) (List_1_t1174585319 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 25)->methodPointer)((List_1_t1174585319 *)__this, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 25));
	}

IL_0016:
	{
		int32_t L_4 = V_0;
		return (bool)((((int32_t)((((int32_t)L_4) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void System.Collections.Generic.List`1<System.Object>::RemoveAt(System.Int32)
extern "C"  void List_1_RemoveAt_m3581840026_gshared (List_1_t1174585319 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_RemoveAt_m3581840026_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___index0;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_0013;
		}
	}
	{
		int32_t L_1 = ___index0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_1) >= ((uint32_t)L_2))))
		{
			goto IL_001e;
		}
	}

IL_0013:
	{
		ArgumentOutOfRangeException_t1745463579 * L_3 = (ArgumentOutOfRangeException_t1745463579 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t1745463579_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2829693980(L_3, (String_t*)_stringLiteral1171314202, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001e:
	{
		int32_t L_4 = ___index0;
		NullCheck((List_1_t1174585319 *)__this);
		((  void (*) (List_1_t1174585319 *, int32_t, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 24)->methodPointer)((List_1_t1174585319 *)__this, (int32_t)L_4, (int32_t)(-1), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 24));
		ObjectU5BU5D_t4290686858* L_5 = (ObjectU5BU5D_t4290686858*)__this->get__items_1();
		int32_t L_6 = (int32_t)__this->get__size_2();
		Array_Clear_m305290227(NULL /*static, unused*/, (RuntimeArray *)(RuntimeArray *)L_5, (int32_t)L_6, (int32_t)1, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_7+(int32_t)1)));
		return;
	}
}
// T[] System.Collections.Generic.List`1<System.Object>::ToArray()
extern "C"  ObjectU5BU5D_t4290686858* List_1_ToArray_m1772277260_gshared (List_1_t1174585319 * __this, const RuntimeMethod* method)
{
	ObjectU5BU5D_t4290686858* V_0 = NULL;
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		V_0 = (ObjectU5BU5D_t4290686858*)((ObjectU5BU5D_t4290686858*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1), (uint32_t)L_0));
		ObjectU5BU5D_t4290686858* L_1 = (ObjectU5BU5D_t4290686858*)__this->get__items_1();
		ObjectU5BU5D_t4290686858* L_2 = V_0;
		int32_t L_3 = (int32_t)__this->get__size_2();
		Array_Copy_m392395275(NULL /*static, unused*/, (RuntimeArray *)(RuntimeArray *)L_1, (RuntimeArray *)(RuntimeArray *)L_2, (int32_t)L_3, /*hidden argument*/NULL);
		ObjectU5BU5D_t4290686858* L_4 = V_0;
		return L_4;
	}
}
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m2022576286_gshared (List_1_t1174585319 * __this, const RuntimeMethod* method)
{
	{
		ObjectU5BU5D_t4290686858* L_0 = (ObjectU5BU5D_t4290686858*)__this->get__items_1();
		NullCheck(L_0);
		return (((int32_t)((int32_t)(((RuntimeArray *)L_0)->max_length))));
	}
}
// System.Void System.Collections.Generic.List`1<System.Object>::set_Capacity(System.Int32)
extern "C"  void List_1_set_Capacity_m2480705992_gshared (List_1_t1174585319 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_set_Capacity_m2480705992_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___value0;
		int32_t L_1 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_0) < ((uint32_t)L_1))))
		{
			goto IL_0012;
		}
	}
	{
		ArgumentOutOfRangeException_t1745463579 * L_2 = (ArgumentOutOfRangeException_t1745463579 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t1745463579_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3684908528(L_2, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0012:
	{
		ObjectU5BU5D_t4290686858** L_3 = (ObjectU5BU5D_t4290686858**)__this->get_address_of__items_1();
		int32_t L_4 = ___value0;
		((  void (*) (RuntimeObject * /* static, unused */, ObjectU5BU5D_t4290686858**, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 26)->methodPointer)(NULL /*static, unused*/, (ObjectU5BU5D_t4290686858**)L_3, (int32_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 26));
		return;
	}
}
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
extern "C"  int32_t List_1_get_Count_m2507529371_gshared (List_1_t1174585319 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		return L_0;
	}
}
// T System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32)
extern "C"  RuntimeObject * List_1_get_Item_m30538788_gshared (List_1_t1174585319 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_get_Item_m30538788_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___index0;
		int32_t L_1 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t1745463579 * L_2 = (ArgumentOutOfRangeException_t1745463579 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t1745463579_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2829693980(L_2, (String_t*)_stringLiteral1171314202, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		ObjectU5BU5D_t4290686858* L_3 = (ObjectU5BU5D_t4290686858*)__this->get__items_1();
		int32_t L_4 = ___index0;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		RuntimeObject * L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		return L_6;
	}
}
// System.Void System.Collections.Generic.List`1<System.Object>::set_Item(System.Int32,T)
extern "C"  void List_1_set_Item_m4287706498_gshared (List_1_t1174585319 * __this, int32_t ___index0, RuntimeObject * ___value1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_set_Item_m4287706498_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t1174585319 *)__this);
		((  void (*) (List_1_t1174585319 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((List_1_t1174585319 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		int32_t L_1 = ___index0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_1) == ((uint32_t)L_2))))
		{
			goto IL_001e;
		}
	}
	{
		ArgumentOutOfRangeException_t1745463579 * L_3 = (ArgumentOutOfRangeException_t1745463579 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t1745463579_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2829693980(L_3, (String_t*)_stringLiteral1171314202, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001e:
	{
		ObjectU5BU5D_t4290686858* L_4 = (ObjectU5BU5D_t4290686858*)__this->get__items_1();
		int32_t L_5 = ___index0;
		RuntimeObject * L_6 = ___value1;
		NullCheck(L_4);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (RuntimeObject *)L_6);
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::.ctor()
extern "C"  void List_1__ctor_m2325872442_gshared (List_1_t2599265951 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m3446193457((RuntimeObject *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		CustomAttributeNamedArgumentU5BU5D_t3127017202* L_0 = ((List_1_t2599265951_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0)))->get_EmptyArray_4();
		__this->set__items_1(L_0);
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::.ctor(System.Int32)
extern "C"  void List_1__ctor_m3193603608_gshared (List_1_t2599265951 * __this, int32_t ___capacity0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1__ctor_m3193603608_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m3446193457((RuntimeObject *)__this, /*hidden argument*/NULL);
		int32_t L_0 = ___capacity0;
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		ArgumentOutOfRangeException_t1745463579 * L_1 = (ArgumentOutOfRangeException_t1745463579 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t1745463579_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2829693980(L_1, (String_t*)_stringLiteral2929636671, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = ___capacity0;
		__this->set__items_1(((CustomAttributeNamedArgumentU5BU5D_t3127017202*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1), (uint32_t)L_2)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::.cctor()
extern "C"  void List_1__cctor_m479685755_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		((List_1_t2599265951_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)))->set_EmptyArray_4(((CustomAttributeNamedArgumentU5BU5D_t3127017202*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (uint32_t)0)));
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  RuntimeObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m571915265_gshared (List_1_t2599265951 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((List_1_t2599265951 *)__this);
		Enumerator_t64113712  L_0 = ((  Enumerator_t64113712  (*) (List_1_t2599265951 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((List_1_t2599265951 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		Enumerator_t64113712  L_1 = L_0;
		RuntimeObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), &L_1);
		return (RuntimeObject*)L_2;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m1210970992_gshared (List_1_t2599265951 * __this, RuntimeArray * ___array0, int32_t ___arrayIndex1, const RuntimeMethod* method)
{
	{
		CustomAttributeNamedArgumentU5BU5D_t3127017202* L_0 = (CustomAttributeNamedArgumentU5BU5D_t3127017202*)__this->get__items_1();
		RuntimeArray * L_1 = ___array0;
		int32_t L_2 = ___arrayIndex1;
		int32_t L_3 = (int32_t)__this->get__size_2();
		Array_Copy_m1287631566(NULL /*static, unused*/, (RuntimeArray *)(RuntimeArray *)L_0, (int32_t)0, (RuntimeArray *)L_1, (int32_t)L_2, (int32_t)L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  RuntimeObject* List_1_System_Collections_IEnumerable_GetEnumerator_m847427345_gshared (List_1_t2599265951 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((List_1_t2599265951 *)__this);
		Enumerator_t64113712  L_0 = ((  Enumerator_t64113712  (*) (List_1_t2599265951 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((List_1_t2599265951 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		Enumerator_t64113712  L_1 = L_0;
		RuntimeObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), &L_1);
		return (RuntimeObject*)L_2;
	}
}
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_Add_m4289758969_gshared (List_1_t2599265951 * __this, RuntimeObject * ___item0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_Add_m4289758969_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Exception_t3361176243 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3361176243 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			RuntimeObject * L_0 = ___item0;
			NullCheck((List_1_t2599265951 *)__this);
			((  void (*) (List_1_t2599265951 *, CustomAttributeNamedArgument_t425730339 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((List_1_t2599265951 *)__this, (CustomAttributeNamedArgument_t425730339 )((*(CustomAttributeNamedArgument_t425730339 *)((CustomAttributeNamedArgument_t425730339 *)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
			int32_t L_1 = (int32_t)__this->get__size_2();
			V_0 = (int32_t)((int32_t)((int32_t)L_1-(int32_t)1));
			goto IL_0036;
		}

IL_001a:
		{
			; // IL_001a: leave IL_002b
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3361176243 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t2549690990_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_001f;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t1009887698_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_0025;
		throw e;
	}

CATCH_001f:
	{ // begin catch(System.NullReferenceException)
		goto IL_002b;
	} // end catch (depth: 1)

CATCH_0025:
	{ // begin catch(System.InvalidCastException)
		goto IL_002b;
	} // end catch (depth: 1)

IL_002b:
	{
		ArgumentException_t1372035057 * L_2 = (ArgumentException_t1372035057 *)il2cpp_codegen_object_new(ArgumentException_t1372035057_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m775170528(L_2, (String_t*)_stringLiteral3684952963, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0036:
	{
		int32_t L_3 = V_0;
		return L_3;
	}
}
// System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.Contains(System.Object)
extern "C"  bool List_1_System_Collections_IList_Contains_m2200782575_gshared (List_1_t2599265951 * __this, RuntimeObject * ___item0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_Contains_m2200782575_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Exception_t3361176243 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3361176243 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			RuntimeObject * L_0 = ___item0;
			NullCheck((List_1_t2599265951 *)__this);
			bool L_1 = ((  bool (*) (List_1_t2599265951 *, CustomAttributeNamedArgument_t425730339 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t2599265951 *)__this, (CustomAttributeNamedArgument_t425730339 )((*(CustomAttributeNamedArgument_t425730339 *)((CustomAttributeNamedArgument_t425730339 *)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
			V_0 = (bool)L_1;
			goto IL_0025;
		}

IL_0012:
		{
			; // IL_0012: leave IL_0023
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3361176243 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t2549690990_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_0017;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t1009887698_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_001d;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.NullReferenceException)
		goto IL_0023;
	} // end catch (depth: 1)

CATCH_001d:
	{ // begin catch(System.InvalidCastException)
		goto IL_0023;
	} // end catch (depth: 1)

IL_0023:
	{
		return (bool)0;
	}

IL_0025:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m3909285767_gshared (List_1_t2599265951 * __this, RuntimeObject * ___item0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_IndexOf_m3909285767_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Exception_t3361176243 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3361176243 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			RuntimeObject * L_0 = ___item0;
			NullCheck((List_1_t2599265951 *)__this);
			int32_t L_1 = ((  int32_t (*) (List_1_t2599265951 *, CustomAttributeNamedArgument_t425730339 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((List_1_t2599265951 *)__this, (CustomAttributeNamedArgument_t425730339 )((*(CustomAttributeNamedArgument_t425730339 *)((CustomAttributeNamedArgument_t425730339 *)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
			V_0 = (int32_t)L_1;
			goto IL_0025;
		}

IL_0012:
		{
			; // IL_0012: leave IL_0023
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3361176243 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t2549690990_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_0017;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t1009887698_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_001d;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.NullReferenceException)
		goto IL_0023;
	} // end catch (depth: 1)

CATCH_001d:
	{ // begin catch(System.InvalidCastException)
		goto IL_0023;
	} // end catch (depth: 1)

IL_0023:
	{
		return (-1);
	}

IL_0025:
	{
		int32_t L_2 = V_0;
		return L_2;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_Insert_m1035245345_gshared (List_1_t2599265951 * __this, int32_t ___index0, RuntimeObject * ___item1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_Insert_m1035245345_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Exception_t3361176243 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3361176243 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t2599265951 *)__this);
		((  void (*) (List_1_t2599265951 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((List_1_t2599265951 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			int32_t L_1 = ___index0;
			RuntimeObject * L_2 = ___item1;
			NullCheck((List_1_t2599265951 *)__this);
			((  void (*) (List_1_t2599265951 *, int32_t, CustomAttributeNamedArgument_t425730339 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((List_1_t2599265951 *)__this, (int32_t)L_1, (CustomAttributeNamedArgument_t425730339 )((*(CustomAttributeNamedArgument_t425730339 *)((CustomAttributeNamedArgument_t425730339 *)UnBox(L_2, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
			goto IL_0035;
		}

IL_0019:
		{
			; // IL_0019: leave IL_002a
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3361176243 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t2549690990_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_001e;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t1009887698_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_0024;
		throw e;
	}

CATCH_001e:
	{ // begin catch(System.NullReferenceException)
		goto IL_002a;
	} // end catch (depth: 1)

CATCH_0024:
	{ // begin catch(System.InvalidCastException)
		goto IL_002a;
	} // end catch (depth: 1)

IL_002a:
	{
		ArgumentException_t1372035057 * L_3 = (ArgumentException_t1372035057 *)il2cpp_codegen_object_new(ArgumentException_t1372035057_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m775170528(L_3, (String_t*)_stringLiteral3684952963, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0035:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.Remove(System.Object)
extern "C"  void List_1_System_Collections_IList_Remove_m1601974356_gshared (List_1_t2599265951 * __this, RuntimeObject * ___item0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_Remove_m1601974356_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Exception_t3361176243 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3361176243 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			RuntimeObject * L_0 = ___item0;
			NullCheck((List_1_t2599265951 *)__this);
			((  bool (*) (List_1_t2599265951 *, CustomAttributeNamedArgument_t425730339 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((List_1_t2599265951 *)__this, (CustomAttributeNamedArgument_t425730339 )((*(CustomAttributeNamedArgument_t425730339 *)((CustomAttributeNamedArgument_t425730339 *)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
			goto IL_0023;
		}

IL_0012:
		{
			; // IL_0012: leave IL_0023
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3361176243 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t2549690990_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_0017;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t1009887698_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_001d;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.NullReferenceException)
		goto IL_0023;
	} // end catch (depth: 1)

CATCH_001d:
	{ // begin catch(System.InvalidCastException)
		goto IL_0023;
	} // end catch (depth: 1)

IL_0023:
	{
		return;
	}
}
// System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m48362325_gshared (List_1_t2599265951 * __this, const RuntimeMethod* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.ICollection.get_SyncRoot()
extern "C"  RuntimeObject * List_1_System_Collections_ICollection_get_SyncRoot_m12470516_gshared (List_1_t2599265951 * __this, const RuntimeMethod* method)
{
	{
		return __this;
	}
}
// System.Object System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.get_Item(System.Int32)
extern "C"  RuntimeObject * List_1_System_Collections_IList_get_Item_m4255795257_gshared (List_1_t2599265951 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t2599265951 *)__this);
		CustomAttributeNamedArgument_t425730339  L_1 = ((  CustomAttributeNamedArgument_t425730339  (*) (List_1_t2599265951 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((List_1_t2599265951 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		CustomAttributeNamedArgument_t425730339  L_2 = L_1;
		RuntimeObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), &L_2);
		return L_3;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_set_Item_m2806321811_gshared (List_1_t2599265951 * __this, int32_t ___index0, RuntimeObject * ___value1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_set_Item_m2806321811_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Exception_t3361176243 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3361176243 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			int32_t L_0 = ___index0;
			RuntimeObject * L_1 = ___value1;
			NullCheck((List_1_t2599265951 *)__this);
			((  void (*) (List_1_t2599265951 *, int32_t, CustomAttributeNamedArgument_t425730339 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((List_1_t2599265951 *)__this, (int32_t)L_0, (CustomAttributeNamedArgument_t425730339 )((*(CustomAttributeNamedArgument_t425730339 *)((CustomAttributeNamedArgument_t425730339 *)UnBox(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
			goto IL_002e;
		}

IL_0012:
		{
			; // IL_0012: leave IL_0023
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3361176243 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t2549690990_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_0017;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t1009887698_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_001d;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.NullReferenceException)
		goto IL_0023;
	} // end catch (depth: 1)

CATCH_001d:
	{ // begin catch(System.InvalidCastException)
		goto IL_0023;
	} // end catch (depth: 1)

IL_0023:
	{
		ArgumentException_t1372035057 * L_2 = (ArgumentException_t1372035057 *)il2cpp_codegen_object_new(ArgumentException_t1372035057_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m775170528(L_2, (String_t*)_stringLiteral3197723981, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_002e:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::Add(T)
extern "C"  void List_1_Add_m4007094055_gshared (List_1_t2599265951 * __this, CustomAttributeNamedArgument_t425730339  ___item0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		CustomAttributeNamedArgumentU5BU5D_t3127017202* L_1 = (CustomAttributeNamedArgumentU5BU5D_t3127017202*)__this->get__items_1();
		NullCheck(L_1);
		if ((!(((uint32_t)L_0) == ((uint32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_1)->max_length))))))))
		{
			goto IL_001a;
		}
	}
	{
		NullCheck((List_1_t2599265951 *)__this);
		((  void (*) (List_1_t2599265951 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13)->methodPointer)((List_1_t2599265951 *)__this, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13));
	}

IL_001a:
	{
		CustomAttributeNamedArgumentU5BU5D_t3127017202* L_2 = (CustomAttributeNamedArgumentU5BU5D_t3127017202*)__this->get__items_1();
		int32_t L_3 = (int32_t)__this->get__size_2();
		int32_t L_4 = (int32_t)L_3;
		V_0 = (int32_t)L_4;
		__this->set__size_2(((int32_t)((int32_t)L_4+(int32_t)1)));
		int32_t L_5 = V_0;
		CustomAttributeNamedArgument_t425730339  L_6 = ___item0;
		NullCheck(L_2);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (CustomAttributeNamedArgument_t425730339 )L_6);
		int32_t L_7 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_7+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m954177724_gshared (List_1_t2599265951 * __this, int32_t ___newCount0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		int32_t L_1 = ___newCount0;
		V_0 = (int32_t)((int32_t)((int32_t)L_0+(int32_t)L_1));
		int32_t L_2 = V_0;
		CustomAttributeNamedArgumentU5BU5D_t3127017202* L_3 = (CustomAttributeNamedArgumentU5BU5D_t3127017202*)__this->get__items_1();
		NullCheck(L_3);
		if ((((int32_t)L_2) <= ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_3)->max_length)))))))
		{
			goto IL_0031;
		}
	}
	{
		NullCheck((List_1_t2599265951 *)__this);
		int32_t L_4 = ((  int32_t (*) (List_1_t2599265951 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14)->methodPointer)((List_1_t2599265951 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14));
		int32_t L_5 = Math_Max_m3051295998(NULL /*static, unused*/, (int32_t)((int32_t)((int32_t)L_4*(int32_t)2)), (int32_t)4, /*hidden argument*/NULL);
		int32_t L_6 = V_0;
		int32_t L_7 = Math_Max_m3051295998(NULL /*static, unused*/, (int32_t)L_5, (int32_t)L_6, /*hidden argument*/NULL);
		NullCheck((List_1_t2599265951 *)__this);
		((  void (*) (List_1_t2599265951 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15)->methodPointer)((List_1_t2599265951 *)__this, (int32_t)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15));
	}

IL_0031:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m205501888_gshared (List_1_t2599265951 * __this, RuntimeObject* ___collection0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		RuntimeObject* L_0 = ___collection0;
		NullCheck((RuntimeObject*)L_0);
		int32_t L_1 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeNamedArgument>::get_Count() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 16), (RuntimeObject*)L_0);
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		if (L_2)
		{
			goto IL_000e;
		}
	}
	{
		return;
	}

IL_000e:
	{
		int32_t L_3 = V_0;
		NullCheck((List_1_t2599265951 *)__this);
		((  void (*) (List_1_t2599265951 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13)->methodPointer)((List_1_t2599265951 *)__this, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13));
		RuntimeObject* L_4 = ___collection0;
		CustomAttributeNamedArgumentU5BU5D_t3127017202* L_5 = (CustomAttributeNamedArgumentU5BU5D_t3127017202*)__this->get__items_1();
		int32_t L_6 = (int32_t)__this->get__size_2();
		NullCheck((RuntimeObject*)L_4);
		InterfaceActionInvoker2< CustomAttributeNamedArgumentU5BU5D_t3127017202*, int32_t >::Invoke(5 /* System.Void System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeNamedArgument>::CopyTo(T[],System.Int32) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 16), (RuntimeObject*)L_4, (CustomAttributeNamedArgumentU5BU5D_t3127017202*)L_5, (int32_t)L_6);
		int32_t L_7 = (int32_t)__this->get__size_2();
		int32_t L_8 = V_0;
		__this->set__size_2(((int32_t)((int32_t)L_7+(int32_t)L_8)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddEnumerable_m2006982645_gshared (List_1_t2599265951 * __this, RuntimeObject* ___enumerable0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_AddEnumerable_m2006982645_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	CustomAttributeNamedArgument_t425730339  V_0;
	memset(&V_0, 0, sizeof(V_0));
	RuntimeObject* V_1 = NULL;
	Exception_t3361176243 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3361176243 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		RuntimeObject* L_0 = ___enumerable0;
		NullCheck((RuntimeObject*)L_0);
		RuntimeObject* L_1 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Reflection.CustomAttributeNamedArgument>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 17), (RuntimeObject*)L_0);
		V_1 = (RuntimeObject*)L_1;
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			goto IL_001a;
		}

IL_000c:
		{
			RuntimeObject* L_2 = V_1;
			NullCheck((RuntimeObject*)L_2);
			CustomAttributeNamedArgument_t425730339  L_3 = InterfaceFuncInvoker0< CustomAttributeNamedArgument_t425730339  >::Invoke(0 /* T System.Collections.Generic.IEnumerator`1<System.Reflection.CustomAttributeNamedArgument>::get_Current() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 18), (RuntimeObject*)L_2);
			V_0 = (CustomAttributeNamedArgument_t425730339 )L_3;
			CustomAttributeNamedArgument_t425730339  L_4 = V_0;
			NullCheck((List_1_t2599265951 *)__this);
			((  void (*) (List_1_t2599265951 *, CustomAttributeNamedArgument_t425730339 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((List_1_t2599265951 *)__this, (CustomAttributeNamedArgument_t425730339 )L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		}

IL_001a:
		{
			RuntimeObject* L_5 = V_1;
			NullCheck((RuntimeObject*)L_5);
			bool L_6 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t4262330110_il2cpp_TypeInfo_var, (RuntimeObject*)L_5);
			if (L_6)
			{
				goto IL_000c;
			}
		}

IL_0025:
		{
			IL2CPP_LEAVE(0x35, FINALLY_002a);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3361176243 *)e.ex;
		goto FINALLY_002a;
	}

FINALLY_002a:
	{ // begin finally (depth: 1)
		{
			RuntimeObject* L_7 = V_1;
			if (L_7)
			{
				goto IL_002e;
			}
		}

IL_002d:
		{
			IL2CPP_END_FINALLY(42)
		}

IL_002e:
		{
			RuntimeObject* L_8 = V_1;
			NullCheck((RuntimeObject*)L_8);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t318870991_il2cpp_TypeInfo_var, (RuntimeObject*)L_8);
			IL2CPP_END_FINALLY(42)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(42)
	{
		IL2CPP_JUMP_TBL(0x35, IL_0035)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3361176243 *)
	}

IL_0035:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m1785959115_gshared (List_1_t2599265951 * __this, RuntimeObject* ___collection0, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		RuntimeObject* L_0 = ___collection0;
		NullCheck((List_1_t2599265951 *)__this);
		((  void (*) (List_1_t2599265951 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19)->methodPointer)((List_1_t2599265951 *)__this, (RuntimeObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19));
		RuntimeObject* L_1 = ___collection0;
		V_0 = (RuntimeObject*)((RuntimeObject*)IsInst((RuntimeObject*)L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 16)));
		RuntimeObject* L_2 = V_0;
		if (!L_2)
		{
			goto IL_0020;
		}
	}
	{
		RuntimeObject* L_3 = V_0;
		NullCheck((List_1_t2599265951 *)__this);
		((  void (*) (List_1_t2599265951 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 20)->methodPointer)((List_1_t2599265951 *)__this, (RuntimeObject*)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 20));
		goto IL_0027;
	}

IL_0020:
	{
		RuntimeObject* L_4 = ___collection0;
		NullCheck((List_1_t2599265951 *)__this);
		((  void (*) (List_1_t2599265951 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 21)->methodPointer)((List_1_t2599265951 *)__this, (RuntimeObject*)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 21));
	}

IL_0027:
	{
		int32_t L_5 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_5+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::Clear()
extern "C"  void List_1_Clear_m2484255258_gshared (List_1_t2599265951 * __this, const RuntimeMethod* method)
{
	{
		CustomAttributeNamedArgumentU5BU5D_t3127017202* L_0 = (CustomAttributeNamedArgumentU5BU5D_t3127017202*)__this->get__items_1();
		CustomAttributeNamedArgumentU5BU5D_t3127017202* L_1 = (CustomAttributeNamedArgumentU5BU5D_t3127017202*)__this->get__items_1();
		NullCheck(L_1);
		Array_Clear_m305290227(NULL /*static, unused*/, (RuntimeArray *)(RuntimeArray *)L_0, (int32_t)0, (int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_1)->max_length)))), /*hidden argument*/NULL);
		__this->set__size_2(0);
		int32_t L_2 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_2+(int32_t)1)));
		return;
	}
}
// System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::Contains(T)
extern "C"  bool List_1_Contains_m1094704041_gshared (List_1_t2599265951 * __this, CustomAttributeNamedArgument_t425730339  ___item0, const RuntimeMethod* method)
{
	{
		CustomAttributeNamedArgumentU5BU5D_t3127017202* L_0 = (CustomAttributeNamedArgumentU5BU5D_t3127017202*)__this->get__items_1();
		CustomAttributeNamedArgument_t425730339  L_1 = ___item0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		int32_t L_3 = ((  int32_t (*) (RuntimeObject * /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t3127017202*, CustomAttributeNamedArgument_t425730339 , int32_t, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 22)->methodPointer)(NULL /*static, unused*/, (CustomAttributeNamedArgumentU5BU5D_t3127017202*)L_0, (CustomAttributeNamedArgument_t425730339 )L_1, (int32_t)0, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 22));
		return (bool)((((int32_t)((((int32_t)L_3) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m908450568_gshared (List_1_t2599265951 * __this, CustomAttributeNamedArgumentU5BU5D_t3127017202* ___array0, int32_t ___arrayIndex1, const RuntimeMethod* method)
{
	{
		CustomAttributeNamedArgumentU5BU5D_t3127017202* L_0 = (CustomAttributeNamedArgumentU5BU5D_t3127017202*)__this->get__items_1();
		CustomAttributeNamedArgumentU5BU5D_t3127017202* L_1 = ___array0;
		int32_t L_2 = ___arrayIndex1;
		int32_t L_3 = (int32_t)__this->get__size_2();
		Array_Copy_m1287631566(NULL /*static, unused*/, (RuntimeArray *)(RuntimeArray *)L_0, (int32_t)0, (RuntimeArray *)(RuntimeArray *)L_1, (int32_t)L_2, (int32_t)L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::GetEnumerator()
extern "C"  Enumerator_t64113712  List_1_GetEnumerator_m2526568000_gshared (List_1_t2599265951 * __this, const RuntimeMethod* method)
{
	{
		Enumerator_t64113712  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Enumerator__ctor_m2449765543((&L_0), (List_1_t2599265951 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 23));
		return L_0;
	}
}
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m1611896199_gshared (List_1_t2599265951 * __this, CustomAttributeNamedArgument_t425730339  ___item0, const RuntimeMethod* method)
{
	{
		CustomAttributeNamedArgumentU5BU5D_t3127017202* L_0 = (CustomAttributeNamedArgumentU5BU5D_t3127017202*)__this->get__items_1();
		CustomAttributeNamedArgument_t425730339  L_1 = ___item0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		int32_t L_3 = ((  int32_t (*) (RuntimeObject * /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t3127017202*, CustomAttributeNamedArgument_t425730339 , int32_t, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 22)->methodPointer)(NULL /*static, unused*/, (CustomAttributeNamedArgumentU5BU5D_t3127017202*)L_0, (CustomAttributeNamedArgument_t425730339 )L_1, (int32_t)0, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 22));
		return L_3;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m3598182289_gshared (List_1_t2599265951 * __this, int32_t ___start0, int32_t ___delta1, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___delta1;
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_000c;
		}
	}
	{
		int32_t L_1 = ___start0;
		int32_t L_2 = ___delta1;
		___start0 = (int32_t)((int32_t)((int32_t)L_1-(int32_t)L_2));
	}

IL_000c:
	{
		int32_t L_3 = ___start0;
		int32_t L_4 = (int32_t)__this->get__size_2();
		if ((((int32_t)L_3) >= ((int32_t)L_4)))
		{
			goto IL_0035;
		}
	}
	{
		CustomAttributeNamedArgumentU5BU5D_t3127017202* L_5 = (CustomAttributeNamedArgumentU5BU5D_t3127017202*)__this->get__items_1();
		int32_t L_6 = ___start0;
		CustomAttributeNamedArgumentU5BU5D_t3127017202* L_7 = (CustomAttributeNamedArgumentU5BU5D_t3127017202*)__this->get__items_1();
		int32_t L_8 = ___start0;
		int32_t L_9 = ___delta1;
		int32_t L_10 = (int32_t)__this->get__size_2();
		int32_t L_11 = ___start0;
		Array_Copy_m1287631566(NULL /*static, unused*/, (RuntimeArray *)(RuntimeArray *)L_5, (int32_t)L_6, (RuntimeArray *)(RuntimeArray *)L_7, (int32_t)((int32_t)((int32_t)L_8+(int32_t)L_9)), (int32_t)((int32_t)((int32_t)L_10-(int32_t)L_11)), /*hidden argument*/NULL);
	}

IL_0035:
	{
		int32_t L_12 = (int32_t)__this->get__size_2();
		int32_t L_13 = ___delta1;
		__this->set__size_2(((int32_t)((int32_t)L_12+(int32_t)L_13)));
		int32_t L_14 = ___delta1;
		if ((((int32_t)L_14) >= ((int32_t)0)))
		{
			goto IL_005d;
		}
	}
	{
		CustomAttributeNamedArgumentU5BU5D_t3127017202* L_15 = (CustomAttributeNamedArgumentU5BU5D_t3127017202*)__this->get__items_1();
		int32_t L_16 = (int32_t)__this->get__size_2();
		int32_t L_17 = ___delta1;
		Array_Clear_m305290227(NULL /*static, unused*/, (RuntimeArray *)(RuntimeArray *)L_15, (int32_t)L_16, (int32_t)((-L_17)), /*hidden argument*/NULL);
	}

IL_005d:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::CheckIndex(System.Int32)
extern "C"  void List_1_CheckIndex_m3158883304_gshared (List_1_t2599265951 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_CheckIndex_m3158883304_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___index0;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_0013;
		}
	}
	{
		int32_t L_1 = ___index0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_1) > ((uint32_t)L_2))))
		{
			goto IL_001e;
		}
	}

IL_0013:
	{
		ArgumentOutOfRangeException_t1745463579 * L_3 = (ArgumentOutOfRangeException_t1745463579 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t1745463579_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2829693980(L_3, (String_t*)_stringLiteral1171314202, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001e:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m2856357716_gshared (List_1_t2599265951 * __this, int32_t ___index0, CustomAttributeNamedArgument_t425730339  ___item1, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t2599265951 *)__this);
		((  void (*) (List_1_t2599265951 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((List_1_t2599265951 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		int32_t L_1 = (int32_t)__this->get__size_2();
		CustomAttributeNamedArgumentU5BU5D_t3127017202* L_2 = (CustomAttributeNamedArgumentU5BU5D_t3127017202*)__this->get__items_1();
		NullCheck(L_2);
		if ((!(((uint32_t)L_1) == ((uint32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_2)->max_length))))))))
		{
			goto IL_0021;
		}
	}
	{
		NullCheck((List_1_t2599265951 *)__this);
		((  void (*) (List_1_t2599265951 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13)->methodPointer)((List_1_t2599265951 *)__this, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13));
	}

IL_0021:
	{
		int32_t L_3 = ___index0;
		NullCheck((List_1_t2599265951 *)__this);
		((  void (*) (List_1_t2599265951 *, int32_t, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 24)->methodPointer)((List_1_t2599265951 *)__this, (int32_t)L_3, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 24));
		CustomAttributeNamedArgumentU5BU5D_t3127017202* L_4 = (CustomAttributeNamedArgumentU5BU5D_t3127017202*)__this->get__items_1();
		int32_t L_5 = ___index0;
		CustomAttributeNamedArgument_t425730339  L_6 = ___item1;
		NullCheck(L_4);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (CustomAttributeNamedArgument_t425730339 )L_6);
		int32_t L_7 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_7+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_CheckCollection_m1098393654_gshared (List_1_t2599265951 * __this, RuntimeObject* ___collection0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_CheckCollection_m1098393654_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = ___collection0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t517973450 * L_1 = (ArgumentNullException_t517973450 *)il2cpp_codegen_object_new(ArgumentNullException_t517973450_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m2607242403(L_1, (String_t*)_stringLiteral2657729115, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		return;
	}
}
// System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::Remove(T)
extern "C"  bool List_1_Remove_m604126856_gshared (List_1_t2599265951 * __this, CustomAttributeNamedArgument_t425730339  ___item0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		CustomAttributeNamedArgument_t425730339  L_0 = ___item0;
		NullCheck((List_1_t2599265951 *)__this);
		int32_t L_1 = ((  int32_t (*) (List_1_t2599265951 *, CustomAttributeNamedArgument_t425730339 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((List_1_t2599265951 *)__this, (CustomAttributeNamedArgument_t425730339 )L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)(-1))))
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_3 = V_0;
		NullCheck((List_1_t2599265951 *)__this);
		((  void (*) (List_1_t2599265951 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 25)->methodPointer)((List_1_t2599265951 *)__this, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 25));
	}

IL_0016:
	{
		int32_t L_4 = V_0;
		return (bool)((((int32_t)((((int32_t)L_4) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::RemoveAt(System.Int32)
extern "C"  void List_1_RemoveAt_m173482288_gshared (List_1_t2599265951 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_RemoveAt_m173482288_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___index0;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_0013;
		}
	}
	{
		int32_t L_1 = ___index0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_1) >= ((uint32_t)L_2))))
		{
			goto IL_001e;
		}
	}

IL_0013:
	{
		ArgumentOutOfRangeException_t1745463579 * L_3 = (ArgumentOutOfRangeException_t1745463579 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t1745463579_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2829693980(L_3, (String_t*)_stringLiteral1171314202, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001e:
	{
		int32_t L_4 = ___index0;
		NullCheck((List_1_t2599265951 *)__this);
		((  void (*) (List_1_t2599265951 *, int32_t, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 24)->methodPointer)((List_1_t2599265951 *)__this, (int32_t)L_4, (int32_t)(-1), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 24));
		CustomAttributeNamedArgumentU5BU5D_t3127017202* L_5 = (CustomAttributeNamedArgumentU5BU5D_t3127017202*)__this->get__items_1();
		int32_t L_6 = (int32_t)__this->get__size_2();
		Array_Clear_m305290227(NULL /*static, unused*/, (RuntimeArray *)(RuntimeArray *)L_5, (int32_t)L_6, (int32_t)1, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_7+(int32_t)1)));
		return;
	}
}
// T[] System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::ToArray()
extern "C"  CustomAttributeNamedArgumentU5BU5D_t3127017202* List_1_ToArray_m1898930587_gshared (List_1_t2599265951 * __this, const RuntimeMethod* method)
{
	CustomAttributeNamedArgumentU5BU5D_t3127017202* V_0 = NULL;
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		V_0 = (CustomAttributeNamedArgumentU5BU5D_t3127017202*)((CustomAttributeNamedArgumentU5BU5D_t3127017202*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1), (uint32_t)L_0));
		CustomAttributeNamedArgumentU5BU5D_t3127017202* L_1 = (CustomAttributeNamedArgumentU5BU5D_t3127017202*)__this->get__items_1();
		CustomAttributeNamedArgumentU5BU5D_t3127017202* L_2 = V_0;
		int32_t L_3 = (int32_t)__this->get__size_2();
		Array_Copy_m392395275(NULL /*static, unused*/, (RuntimeArray *)(RuntimeArray *)L_1, (RuntimeArray *)(RuntimeArray *)L_2, (int32_t)L_3, /*hidden argument*/NULL);
		CustomAttributeNamedArgumentU5BU5D_t3127017202* L_4 = V_0;
		return L_4;
	}
}
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m2138185256_gshared (List_1_t2599265951 * __this, const RuntimeMethod* method)
{
	{
		CustomAttributeNamedArgumentU5BU5D_t3127017202* L_0 = (CustomAttributeNamedArgumentU5BU5D_t3127017202*)__this->get__items_1();
		NullCheck(L_0);
		return (((int32_t)((int32_t)(((RuntimeArray *)L_0)->max_length))));
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::set_Capacity(System.Int32)
extern "C"  void List_1_set_Capacity_m1168893075_gshared (List_1_t2599265951 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_set_Capacity_m1168893075_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___value0;
		int32_t L_1 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_0) < ((uint32_t)L_1))))
		{
			goto IL_0012;
		}
	}
	{
		ArgumentOutOfRangeException_t1745463579 * L_2 = (ArgumentOutOfRangeException_t1745463579 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t1745463579_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3684908528(L_2, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0012:
	{
		CustomAttributeNamedArgumentU5BU5D_t3127017202** L_3 = (CustomAttributeNamedArgumentU5BU5D_t3127017202**)__this->get_address_of__items_1();
		int32_t L_4 = ___value0;
		((  void (*) (RuntimeObject * /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t3127017202**, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 26)->methodPointer)(NULL /*static, unused*/, (CustomAttributeNamedArgumentU5BU5D_t3127017202**)L_3, (int32_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 26));
		return;
	}
}
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::get_Count()
extern "C"  int32_t List_1_get_Count_m2516697055_gshared (List_1_t2599265951 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		return L_0;
	}
}
// T System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::get_Item(System.Int32)
extern "C"  CustomAttributeNamedArgument_t425730339  List_1_get_Item_m3385836824_gshared (List_1_t2599265951 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_get_Item_m3385836824_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___index0;
		int32_t L_1 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t1745463579 * L_2 = (ArgumentOutOfRangeException_t1745463579 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t1745463579_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2829693980(L_2, (String_t*)_stringLiteral1171314202, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		CustomAttributeNamedArgumentU5BU5D_t3127017202* L_3 = (CustomAttributeNamedArgumentU5BU5D_t3127017202*)__this->get__items_1();
		int32_t L_4 = ___index0;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		CustomAttributeNamedArgument_t425730339  L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		return L_6;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::set_Item(System.Int32,T)
extern "C"  void List_1_set_Item_m3966278890_gshared (List_1_t2599265951 * __this, int32_t ___index0, CustomAttributeNamedArgument_t425730339  ___value1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_set_Item_m3966278890_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t2599265951 *)__this);
		((  void (*) (List_1_t2599265951 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((List_1_t2599265951 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		int32_t L_1 = ___index0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_1) == ((uint32_t)L_2))))
		{
			goto IL_001e;
		}
	}
	{
		ArgumentOutOfRangeException_t1745463579 * L_3 = (ArgumentOutOfRangeException_t1745463579 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t1745463579_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2829693980(L_3, (String_t*)_stringLiteral1171314202, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001e:
	{
		CustomAttributeNamedArgumentU5BU5D_t3127017202* L_4 = (CustomAttributeNamedArgumentU5BU5D_t3127017202*)__this->get__items_1();
		int32_t L_5 = ___index0;
		CustomAttributeNamedArgument_t425730339  L_6 = ___value1;
		NullCheck(L_4);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (CustomAttributeNamedArgument_t425730339 )L_6);
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::.ctor()
extern "C"  void List_1__ctor_m2944886508_gshared (List_1_t546271037 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m3446193457((RuntimeObject *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		CustomAttributeTypedArgumentU5BU5D_t2668173852* L_0 = ((List_1_t546271037_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0)))->get_EmptyArray_4();
		__this->set__items_1(L_0);
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::.ctor(System.Int32)
extern "C"  void List_1__ctor_m3102341422_gshared (List_1_t546271037 * __this, int32_t ___capacity0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1__ctor_m3102341422_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m3446193457((RuntimeObject *)__this, /*hidden argument*/NULL);
		int32_t L_0 = ___capacity0;
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		ArgumentOutOfRangeException_t1745463579 * L_1 = (ArgumentOutOfRangeException_t1745463579 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t1745463579_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2829693980(L_1, (String_t*)_stringLiteral2929636671, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = ___capacity0;
		__this->set__items_1(((CustomAttributeTypedArgumentU5BU5D_t2668173852*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1), (uint32_t)L_2)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::.cctor()
extern "C"  void List_1__cctor_m3869846711_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		((List_1_t546271037_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)))->set_EmptyArray_4(((CustomAttributeTypedArgumentU5BU5D_t2668173852*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (uint32_t)0)));
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  RuntimeObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3712476806_gshared (List_1_t546271037 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((List_1_t546271037 *)__this);
		Enumerator_t2306086094  L_0 = ((  Enumerator_t2306086094  (*) (List_1_t546271037 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((List_1_t546271037 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		Enumerator_t2306086094  L_1 = L_0;
		RuntimeObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), &L_1);
		return (RuntimeObject*)L_2;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m2015964324_gshared (List_1_t546271037 * __this, RuntimeArray * ___array0, int32_t ___arrayIndex1, const RuntimeMethod* method)
{
	{
		CustomAttributeTypedArgumentU5BU5D_t2668173852* L_0 = (CustomAttributeTypedArgumentU5BU5D_t2668173852*)__this->get__items_1();
		RuntimeArray * L_1 = ___array0;
		int32_t L_2 = ___arrayIndex1;
		int32_t L_3 = (int32_t)__this->get__size_2();
		Array_Copy_m1287631566(NULL /*static, unused*/, (RuntimeArray *)(RuntimeArray *)L_0, (int32_t)0, (RuntimeArray *)L_1, (int32_t)L_2, (int32_t)L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  RuntimeObject* List_1_System_Collections_IEnumerable_GetEnumerator_m1451768791_gshared (List_1_t546271037 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((List_1_t546271037 *)__this);
		Enumerator_t2306086094  L_0 = ((  Enumerator_t2306086094  (*) (List_1_t546271037 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((List_1_t546271037 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		Enumerator_t2306086094  L_1 = L_0;
		RuntimeObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), &L_1);
		return (RuntimeObject*)L_2;
	}
}
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_Add_m149808363_gshared (List_1_t546271037 * __this, RuntimeObject * ___item0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_Add_m149808363_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Exception_t3361176243 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3361176243 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			RuntimeObject * L_0 = ___item0;
			NullCheck((List_1_t546271037 *)__this);
			((  void (*) (List_1_t546271037 *, CustomAttributeTypedArgument_t2667702721 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((List_1_t546271037 *)__this, (CustomAttributeTypedArgument_t2667702721 )((*(CustomAttributeTypedArgument_t2667702721 *)((CustomAttributeTypedArgument_t2667702721 *)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
			int32_t L_1 = (int32_t)__this->get__size_2();
			V_0 = (int32_t)((int32_t)((int32_t)L_1-(int32_t)1));
			goto IL_0036;
		}

IL_001a:
		{
			; // IL_001a: leave IL_002b
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3361176243 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t2549690990_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_001f;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t1009887698_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_0025;
		throw e;
	}

CATCH_001f:
	{ // begin catch(System.NullReferenceException)
		goto IL_002b;
	} // end catch (depth: 1)

CATCH_0025:
	{ // begin catch(System.InvalidCastException)
		goto IL_002b;
	} // end catch (depth: 1)

IL_002b:
	{
		ArgumentException_t1372035057 * L_2 = (ArgumentException_t1372035057 *)il2cpp_codegen_object_new(ArgumentException_t1372035057_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m775170528(L_2, (String_t*)_stringLiteral3684952963, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0036:
	{
		int32_t L_3 = V_0;
		return L_3;
	}
}
// System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.Contains(System.Object)
extern "C"  bool List_1_System_Collections_IList_Contains_m3356661005_gshared (List_1_t546271037 * __this, RuntimeObject * ___item0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_Contains_m3356661005_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Exception_t3361176243 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3361176243 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			RuntimeObject * L_0 = ___item0;
			NullCheck((List_1_t546271037 *)__this);
			bool L_1 = ((  bool (*) (List_1_t546271037 *, CustomAttributeTypedArgument_t2667702721 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t546271037 *)__this, (CustomAttributeTypedArgument_t2667702721 )((*(CustomAttributeTypedArgument_t2667702721 *)((CustomAttributeTypedArgument_t2667702721 *)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
			V_0 = (bool)L_1;
			goto IL_0025;
		}

IL_0012:
		{
			; // IL_0012: leave IL_0023
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3361176243 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t2549690990_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_0017;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t1009887698_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_001d;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.NullReferenceException)
		goto IL_0023;
	} // end catch (depth: 1)

CATCH_001d:
	{ // begin catch(System.InvalidCastException)
		goto IL_0023;
	} // end catch (depth: 1)

IL_0023:
	{
		return (bool)0;
	}

IL_0025:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m1161609980_gshared (List_1_t546271037 * __this, RuntimeObject * ___item0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_IndexOf_m1161609980_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Exception_t3361176243 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3361176243 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			RuntimeObject * L_0 = ___item0;
			NullCheck((List_1_t546271037 *)__this);
			int32_t L_1 = ((  int32_t (*) (List_1_t546271037 *, CustomAttributeTypedArgument_t2667702721 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((List_1_t546271037 *)__this, (CustomAttributeTypedArgument_t2667702721 )((*(CustomAttributeTypedArgument_t2667702721 *)((CustomAttributeTypedArgument_t2667702721 *)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
			V_0 = (int32_t)L_1;
			goto IL_0025;
		}

IL_0012:
		{
			; // IL_0012: leave IL_0023
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3361176243 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t2549690990_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_0017;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t1009887698_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_001d;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.NullReferenceException)
		goto IL_0023;
	} // end catch (depth: 1)

CATCH_001d:
	{ // begin catch(System.InvalidCastException)
		goto IL_0023;
	} // end catch (depth: 1)

IL_0023:
	{
		return (-1);
	}

IL_0025:
	{
		int32_t L_2 = V_0;
		return L_2;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_Insert_m311886778_gshared (List_1_t546271037 * __this, int32_t ___index0, RuntimeObject * ___item1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_Insert_m311886778_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Exception_t3361176243 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3361176243 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t546271037 *)__this);
		((  void (*) (List_1_t546271037 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((List_1_t546271037 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			int32_t L_1 = ___index0;
			RuntimeObject * L_2 = ___item1;
			NullCheck((List_1_t546271037 *)__this);
			((  void (*) (List_1_t546271037 *, int32_t, CustomAttributeTypedArgument_t2667702721 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((List_1_t546271037 *)__this, (int32_t)L_1, (CustomAttributeTypedArgument_t2667702721 )((*(CustomAttributeTypedArgument_t2667702721 *)((CustomAttributeTypedArgument_t2667702721 *)UnBox(L_2, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
			goto IL_0035;
		}

IL_0019:
		{
			; // IL_0019: leave IL_002a
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3361176243 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t2549690990_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_001e;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t1009887698_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_0024;
		throw e;
	}

CATCH_001e:
	{ // begin catch(System.NullReferenceException)
		goto IL_002a;
	} // end catch (depth: 1)

CATCH_0024:
	{ // begin catch(System.InvalidCastException)
		goto IL_002a;
	} // end catch (depth: 1)

IL_002a:
	{
		ArgumentException_t1372035057 * L_3 = (ArgumentException_t1372035057 *)il2cpp_codegen_object_new(ArgumentException_t1372035057_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m775170528(L_3, (String_t*)_stringLiteral3684952963, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0035:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.Remove(System.Object)
extern "C"  void List_1_System_Collections_IList_Remove_m3278455504_gshared (List_1_t546271037 * __this, RuntimeObject * ___item0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_Remove_m3278455504_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Exception_t3361176243 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3361176243 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			RuntimeObject * L_0 = ___item0;
			NullCheck((List_1_t546271037 *)__this);
			((  bool (*) (List_1_t546271037 *, CustomAttributeTypedArgument_t2667702721 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((List_1_t546271037 *)__this, (CustomAttributeTypedArgument_t2667702721 )((*(CustomAttributeTypedArgument_t2667702721 *)((CustomAttributeTypedArgument_t2667702721 *)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
			goto IL_0023;
		}

IL_0012:
		{
			; // IL_0012: leave IL_0023
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3361176243 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t2549690990_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_0017;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t1009887698_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_001d;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.NullReferenceException)
		goto IL_0023;
	} // end catch (depth: 1)

CATCH_001d:
	{ // begin catch(System.InvalidCastException)
		goto IL_0023;
	} // end catch (depth: 1)

IL_0023:
	{
		return;
	}
}
// System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3582577512_gshared (List_1_t546271037 * __this, const RuntimeMethod* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.ICollection.get_SyncRoot()
extern "C"  RuntimeObject * List_1_System_Collections_ICollection_get_SyncRoot_m2327125404_gshared (List_1_t546271037 * __this, const RuntimeMethod* method)
{
	{
		return __this;
	}
}
// System.Object System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.get_Item(System.Int32)
extern "C"  RuntimeObject * List_1_System_Collections_IList_get_Item_m695163593_gshared (List_1_t546271037 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t546271037 *)__this);
		CustomAttributeTypedArgument_t2667702721  L_1 = ((  CustomAttributeTypedArgument_t2667702721  (*) (List_1_t546271037 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((List_1_t546271037 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		CustomAttributeTypedArgument_t2667702721  L_2 = L_1;
		RuntimeObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), &L_2);
		return L_3;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_set_Item_m64158457_gshared (List_1_t546271037 * __this, int32_t ___index0, RuntimeObject * ___value1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_set_Item_m64158457_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Exception_t3361176243 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3361176243 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			int32_t L_0 = ___index0;
			RuntimeObject * L_1 = ___value1;
			NullCheck((List_1_t546271037 *)__this);
			((  void (*) (List_1_t546271037 *, int32_t, CustomAttributeTypedArgument_t2667702721 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((List_1_t546271037 *)__this, (int32_t)L_0, (CustomAttributeTypedArgument_t2667702721 )((*(CustomAttributeTypedArgument_t2667702721 *)((CustomAttributeTypedArgument_t2667702721 *)UnBox(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
			goto IL_002e;
		}

IL_0012:
		{
			; // IL_0012: leave IL_0023
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3361176243 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t2549690990_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_0017;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t1009887698_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_001d;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.NullReferenceException)
		goto IL_0023;
	} // end catch (depth: 1)

CATCH_001d:
	{ // begin catch(System.InvalidCastException)
		goto IL_0023;
	} // end catch (depth: 1)

IL_0023:
	{
		ArgumentException_t1372035057 * L_2 = (ArgumentException_t1372035057 *)il2cpp_codegen_object_new(ArgumentException_t1372035057_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m775170528(L_2, (String_t*)_stringLiteral3197723981, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_002e:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::Add(T)
extern "C"  void List_1_Add_m4132343367_gshared (List_1_t546271037 * __this, CustomAttributeTypedArgument_t2667702721  ___item0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		CustomAttributeTypedArgumentU5BU5D_t2668173852* L_1 = (CustomAttributeTypedArgumentU5BU5D_t2668173852*)__this->get__items_1();
		NullCheck(L_1);
		if ((!(((uint32_t)L_0) == ((uint32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_1)->max_length))))))))
		{
			goto IL_001a;
		}
	}
	{
		NullCheck((List_1_t546271037 *)__this);
		((  void (*) (List_1_t546271037 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13)->methodPointer)((List_1_t546271037 *)__this, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13));
	}

IL_001a:
	{
		CustomAttributeTypedArgumentU5BU5D_t2668173852* L_2 = (CustomAttributeTypedArgumentU5BU5D_t2668173852*)__this->get__items_1();
		int32_t L_3 = (int32_t)__this->get__size_2();
		int32_t L_4 = (int32_t)L_3;
		V_0 = (int32_t)L_4;
		__this->set__size_2(((int32_t)((int32_t)L_4+(int32_t)1)));
		int32_t L_5 = V_0;
		CustomAttributeTypedArgument_t2667702721  L_6 = ___item0;
		NullCheck(L_2);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (CustomAttributeTypedArgument_t2667702721 )L_6);
		int32_t L_7 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_7+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m256255205_gshared (List_1_t546271037 * __this, int32_t ___newCount0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		int32_t L_1 = ___newCount0;
		V_0 = (int32_t)((int32_t)((int32_t)L_0+(int32_t)L_1));
		int32_t L_2 = V_0;
		CustomAttributeTypedArgumentU5BU5D_t2668173852* L_3 = (CustomAttributeTypedArgumentU5BU5D_t2668173852*)__this->get__items_1();
		NullCheck(L_3);
		if ((((int32_t)L_2) <= ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_3)->max_length)))))))
		{
			goto IL_0031;
		}
	}
	{
		NullCheck((List_1_t546271037 *)__this);
		int32_t L_4 = ((  int32_t (*) (List_1_t546271037 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14)->methodPointer)((List_1_t546271037 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14));
		int32_t L_5 = Math_Max_m3051295998(NULL /*static, unused*/, (int32_t)((int32_t)((int32_t)L_4*(int32_t)2)), (int32_t)4, /*hidden argument*/NULL);
		int32_t L_6 = V_0;
		int32_t L_7 = Math_Max_m3051295998(NULL /*static, unused*/, (int32_t)L_5, (int32_t)L_6, /*hidden argument*/NULL);
		NullCheck((List_1_t546271037 *)__this);
		((  void (*) (List_1_t546271037 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15)->methodPointer)((List_1_t546271037 *)__this, (int32_t)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15));
	}

IL_0031:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m1200876232_gshared (List_1_t546271037 * __this, RuntimeObject* ___collection0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		RuntimeObject* L_0 = ___collection0;
		NullCheck((RuntimeObject*)L_0);
		int32_t L_1 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeTypedArgument>::get_Count() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 16), (RuntimeObject*)L_0);
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		if (L_2)
		{
			goto IL_000e;
		}
	}
	{
		return;
	}

IL_000e:
	{
		int32_t L_3 = V_0;
		NullCheck((List_1_t546271037 *)__this);
		((  void (*) (List_1_t546271037 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13)->methodPointer)((List_1_t546271037 *)__this, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13));
		RuntimeObject* L_4 = ___collection0;
		CustomAttributeTypedArgumentU5BU5D_t2668173852* L_5 = (CustomAttributeTypedArgumentU5BU5D_t2668173852*)__this->get__items_1();
		int32_t L_6 = (int32_t)__this->get__size_2();
		NullCheck((RuntimeObject*)L_4);
		InterfaceActionInvoker2< CustomAttributeTypedArgumentU5BU5D_t2668173852*, int32_t >::Invoke(5 /* System.Void System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeTypedArgument>::CopyTo(T[],System.Int32) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 16), (RuntimeObject*)L_4, (CustomAttributeTypedArgumentU5BU5D_t2668173852*)L_5, (int32_t)L_6);
		int32_t L_7 = (int32_t)__this->get__size_2();
		int32_t L_8 = V_0;
		__this->set__size_2(((int32_t)((int32_t)L_7+(int32_t)L_8)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddEnumerable_m2205638403_gshared (List_1_t546271037 * __this, RuntimeObject* ___enumerable0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_AddEnumerable_m2205638403_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	CustomAttributeTypedArgument_t2667702721  V_0;
	memset(&V_0, 0, sizeof(V_0));
	RuntimeObject* V_1 = NULL;
	Exception_t3361176243 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3361176243 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		RuntimeObject* L_0 = ___enumerable0;
		NullCheck((RuntimeObject*)L_0);
		RuntimeObject* L_1 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Reflection.CustomAttributeTypedArgument>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 17), (RuntimeObject*)L_0);
		V_1 = (RuntimeObject*)L_1;
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			goto IL_001a;
		}

IL_000c:
		{
			RuntimeObject* L_2 = V_1;
			NullCheck((RuntimeObject*)L_2);
			CustomAttributeTypedArgument_t2667702721  L_3 = InterfaceFuncInvoker0< CustomAttributeTypedArgument_t2667702721  >::Invoke(0 /* T System.Collections.Generic.IEnumerator`1<System.Reflection.CustomAttributeTypedArgument>::get_Current() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 18), (RuntimeObject*)L_2);
			V_0 = (CustomAttributeTypedArgument_t2667702721 )L_3;
			CustomAttributeTypedArgument_t2667702721  L_4 = V_0;
			NullCheck((List_1_t546271037 *)__this);
			((  void (*) (List_1_t546271037 *, CustomAttributeTypedArgument_t2667702721 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((List_1_t546271037 *)__this, (CustomAttributeTypedArgument_t2667702721 )L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		}

IL_001a:
		{
			RuntimeObject* L_5 = V_1;
			NullCheck((RuntimeObject*)L_5);
			bool L_6 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t4262330110_il2cpp_TypeInfo_var, (RuntimeObject*)L_5);
			if (L_6)
			{
				goto IL_000c;
			}
		}

IL_0025:
		{
			IL2CPP_LEAVE(0x35, FINALLY_002a);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3361176243 *)e.ex;
		goto FINALLY_002a;
	}

FINALLY_002a:
	{ // begin finally (depth: 1)
		{
			RuntimeObject* L_7 = V_1;
			if (L_7)
			{
				goto IL_002e;
			}
		}

IL_002d:
		{
			IL2CPP_END_FINALLY(42)
		}

IL_002e:
		{
			RuntimeObject* L_8 = V_1;
			NullCheck((RuntimeObject*)L_8);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t318870991_il2cpp_TypeInfo_var, (RuntimeObject*)L_8);
			IL2CPP_END_FINALLY(42)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(42)
	{
		IL2CPP_JUMP_TBL(0x35, IL_0035)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3361176243 *)
	}

IL_0035:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m2035820333_gshared (List_1_t546271037 * __this, RuntimeObject* ___collection0, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		RuntimeObject* L_0 = ___collection0;
		NullCheck((List_1_t546271037 *)__this);
		((  void (*) (List_1_t546271037 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19)->methodPointer)((List_1_t546271037 *)__this, (RuntimeObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19));
		RuntimeObject* L_1 = ___collection0;
		V_0 = (RuntimeObject*)((RuntimeObject*)IsInst((RuntimeObject*)L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 16)));
		RuntimeObject* L_2 = V_0;
		if (!L_2)
		{
			goto IL_0020;
		}
	}
	{
		RuntimeObject* L_3 = V_0;
		NullCheck((List_1_t546271037 *)__this);
		((  void (*) (List_1_t546271037 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 20)->methodPointer)((List_1_t546271037 *)__this, (RuntimeObject*)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 20));
		goto IL_0027;
	}

IL_0020:
	{
		RuntimeObject* L_4 = ___collection0;
		NullCheck((List_1_t546271037 *)__this);
		((  void (*) (List_1_t546271037 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 21)->methodPointer)((List_1_t546271037 *)__this, (RuntimeObject*)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 21));
	}

IL_0027:
	{
		int32_t L_5 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_5+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::Clear()
extern "C"  void List_1_Clear_m1341365677_gshared (List_1_t546271037 * __this, const RuntimeMethod* method)
{
	{
		CustomAttributeTypedArgumentU5BU5D_t2668173852* L_0 = (CustomAttributeTypedArgumentU5BU5D_t2668173852*)__this->get__items_1();
		CustomAttributeTypedArgumentU5BU5D_t2668173852* L_1 = (CustomAttributeTypedArgumentU5BU5D_t2668173852*)__this->get__items_1();
		NullCheck(L_1);
		Array_Clear_m305290227(NULL /*static, unused*/, (RuntimeArray *)(RuntimeArray *)L_0, (int32_t)0, (int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_1)->max_length)))), /*hidden argument*/NULL);
		__this->set__size_2(0);
		int32_t L_2 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_2+(int32_t)1)));
		return;
	}
}
// System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::Contains(T)
extern "C"  bool List_1_Contains_m3028167073_gshared (List_1_t546271037 * __this, CustomAttributeTypedArgument_t2667702721  ___item0, const RuntimeMethod* method)
{
	{
		CustomAttributeTypedArgumentU5BU5D_t2668173852* L_0 = (CustomAttributeTypedArgumentU5BU5D_t2668173852*)__this->get__items_1();
		CustomAttributeTypedArgument_t2667702721  L_1 = ___item0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		int32_t L_3 = ((  int32_t (*) (RuntimeObject * /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2668173852*, CustomAttributeTypedArgument_t2667702721 , int32_t, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 22)->methodPointer)(NULL /*static, unused*/, (CustomAttributeTypedArgumentU5BU5D_t2668173852*)L_0, (CustomAttributeTypedArgument_t2667702721 )L_1, (int32_t)0, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 22));
		return (bool)((((int32_t)((((int32_t)L_3) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m3352220069_gshared (List_1_t546271037 * __this, CustomAttributeTypedArgumentU5BU5D_t2668173852* ___array0, int32_t ___arrayIndex1, const RuntimeMethod* method)
{
	{
		CustomAttributeTypedArgumentU5BU5D_t2668173852* L_0 = (CustomAttributeTypedArgumentU5BU5D_t2668173852*)__this->get__items_1();
		CustomAttributeTypedArgumentU5BU5D_t2668173852* L_1 = ___array0;
		int32_t L_2 = ___arrayIndex1;
		int32_t L_3 = (int32_t)__this->get__size_2();
		Array_Copy_m1287631566(NULL /*static, unused*/, (RuntimeArray *)(RuntimeArray *)L_0, (int32_t)0, (RuntimeArray *)(RuntimeArray *)L_1, (int32_t)L_2, (int32_t)L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::GetEnumerator()
extern "C"  Enumerator_t2306086094  List_1_GetEnumerator_m4078226539_gshared (List_1_t546271037 * __this, const RuntimeMethod* method)
{
	{
		Enumerator_t2306086094  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Enumerator__ctor_m2446290647((&L_0), (List_1_t546271037 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 23));
		return L_0;
	}
}
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m3116972282_gshared (List_1_t546271037 * __this, CustomAttributeTypedArgument_t2667702721  ___item0, const RuntimeMethod* method)
{
	{
		CustomAttributeTypedArgumentU5BU5D_t2668173852* L_0 = (CustomAttributeTypedArgumentU5BU5D_t2668173852*)__this->get__items_1();
		CustomAttributeTypedArgument_t2667702721  L_1 = ___item0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		int32_t L_3 = ((  int32_t (*) (RuntimeObject * /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2668173852*, CustomAttributeTypedArgument_t2667702721 , int32_t, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 22)->methodPointer)(NULL /*static, unused*/, (CustomAttributeTypedArgumentU5BU5D_t2668173852*)L_0, (CustomAttributeTypedArgument_t2667702721 )L_1, (int32_t)0, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 22));
		return L_3;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m647474724_gshared (List_1_t546271037 * __this, int32_t ___start0, int32_t ___delta1, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___delta1;
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_000c;
		}
	}
	{
		int32_t L_1 = ___start0;
		int32_t L_2 = ___delta1;
		___start0 = (int32_t)((int32_t)((int32_t)L_1-(int32_t)L_2));
	}

IL_000c:
	{
		int32_t L_3 = ___start0;
		int32_t L_4 = (int32_t)__this->get__size_2();
		if ((((int32_t)L_3) >= ((int32_t)L_4)))
		{
			goto IL_0035;
		}
	}
	{
		CustomAttributeTypedArgumentU5BU5D_t2668173852* L_5 = (CustomAttributeTypedArgumentU5BU5D_t2668173852*)__this->get__items_1();
		int32_t L_6 = ___start0;
		CustomAttributeTypedArgumentU5BU5D_t2668173852* L_7 = (CustomAttributeTypedArgumentU5BU5D_t2668173852*)__this->get__items_1();
		int32_t L_8 = ___start0;
		int32_t L_9 = ___delta1;
		int32_t L_10 = (int32_t)__this->get__size_2();
		int32_t L_11 = ___start0;
		Array_Copy_m1287631566(NULL /*static, unused*/, (RuntimeArray *)(RuntimeArray *)L_5, (int32_t)L_6, (RuntimeArray *)(RuntimeArray *)L_7, (int32_t)((int32_t)((int32_t)L_8+(int32_t)L_9)), (int32_t)((int32_t)((int32_t)L_10-(int32_t)L_11)), /*hidden argument*/NULL);
	}

IL_0035:
	{
		int32_t L_12 = (int32_t)__this->get__size_2();
		int32_t L_13 = ___delta1;
		__this->set__size_2(((int32_t)((int32_t)L_12+(int32_t)L_13)));
		int32_t L_14 = ___delta1;
		if ((((int32_t)L_14) >= ((int32_t)0)))
		{
			goto IL_005d;
		}
	}
	{
		CustomAttributeTypedArgumentU5BU5D_t2668173852* L_15 = (CustomAttributeTypedArgumentU5BU5D_t2668173852*)__this->get__items_1();
		int32_t L_16 = (int32_t)__this->get__size_2();
		int32_t L_17 = ___delta1;
		Array_Clear_m305290227(NULL /*static, unused*/, (RuntimeArray *)(RuntimeArray *)L_15, (int32_t)L_16, (int32_t)((-L_17)), /*hidden argument*/NULL);
	}

IL_005d:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::CheckIndex(System.Int32)
extern "C"  void List_1_CheckIndex_m1537533006_gshared (List_1_t546271037 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_CheckIndex_m1537533006_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___index0;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_0013;
		}
	}
	{
		int32_t L_1 = ___index0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_1) > ((uint32_t)L_2))))
		{
			goto IL_001e;
		}
	}

IL_0013:
	{
		ArgumentOutOfRangeException_t1745463579 * L_3 = (ArgumentOutOfRangeException_t1745463579 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t1745463579_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2829693980(L_3, (String_t*)_stringLiteral1171314202, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001e:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m3976002586_gshared (List_1_t546271037 * __this, int32_t ___index0, CustomAttributeTypedArgument_t2667702721  ___item1, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t546271037 *)__this);
		((  void (*) (List_1_t546271037 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((List_1_t546271037 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		int32_t L_1 = (int32_t)__this->get__size_2();
		CustomAttributeTypedArgumentU5BU5D_t2668173852* L_2 = (CustomAttributeTypedArgumentU5BU5D_t2668173852*)__this->get__items_1();
		NullCheck(L_2);
		if ((!(((uint32_t)L_1) == ((uint32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_2)->max_length))))))))
		{
			goto IL_0021;
		}
	}
	{
		NullCheck((List_1_t546271037 *)__this);
		((  void (*) (List_1_t546271037 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13)->methodPointer)((List_1_t546271037 *)__this, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13));
	}

IL_0021:
	{
		int32_t L_3 = ___index0;
		NullCheck((List_1_t546271037 *)__this);
		((  void (*) (List_1_t546271037 *, int32_t, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 24)->methodPointer)((List_1_t546271037 *)__this, (int32_t)L_3, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 24));
		CustomAttributeTypedArgumentU5BU5D_t2668173852* L_4 = (CustomAttributeTypedArgumentU5BU5D_t2668173852*)__this->get__items_1();
		int32_t L_5 = ___index0;
		CustomAttributeTypedArgument_t2667702721  L_6 = ___item1;
		NullCheck(L_4);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (CustomAttributeTypedArgument_t2667702721 )L_6);
		int32_t L_7 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_7+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_CheckCollection_m1542265315_gshared (List_1_t546271037 * __this, RuntimeObject* ___collection0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_CheckCollection_m1542265315_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = ___collection0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t517973450 * L_1 = (ArgumentNullException_t517973450 *)il2cpp_codegen_object_new(ArgumentNullException_t517973450_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m2607242403(L_1, (String_t*)_stringLiteral2657729115, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		return;
	}
}
// System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::Remove(T)
extern "C"  bool List_1_Remove_m3658113883_gshared (List_1_t546271037 * __this, CustomAttributeTypedArgument_t2667702721  ___item0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		CustomAttributeTypedArgument_t2667702721  L_0 = ___item0;
		NullCheck((List_1_t546271037 *)__this);
		int32_t L_1 = ((  int32_t (*) (List_1_t546271037 *, CustomAttributeTypedArgument_t2667702721 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((List_1_t546271037 *)__this, (CustomAttributeTypedArgument_t2667702721 )L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)(-1))))
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_3 = V_0;
		NullCheck((List_1_t546271037 *)__this);
		((  void (*) (List_1_t546271037 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 25)->methodPointer)((List_1_t546271037 *)__this, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 25));
	}

IL_0016:
	{
		int32_t L_4 = V_0;
		return (bool)((((int32_t)((((int32_t)L_4) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::RemoveAt(System.Int32)
extern "C"  void List_1_RemoveAt_m2674740562_gshared (List_1_t546271037 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_RemoveAt_m2674740562_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___index0;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_0013;
		}
	}
	{
		int32_t L_1 = ___index0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_1) >= ((uint32_t)L_2))))
		{
			goto IL_001e;
		}
	}

IL_0013:
	{
		ArgumentOutOfRangeException_t1745463579 * L_3 = (ArgumentOutOfRangeException_t1745463579 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t1745463579_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2829693980(L_3, (String_t*)_stringLiteral1171314202, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001e:
	{
		int32_t L_4 = ___index0;
		NullCheck((List_1_t546271037 *)__this);
		((  void (*) (List_1_t546271037 *, int32_t, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 24)->methodPointer)((List_1_t546271037 *)__this, (int32_t)L_4, (int32_t)(-1), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 24));
		CustomAttributeTypedArgumentU5BU5D_t2668173852* L_5 = (CustomAttributeTypedArgumentU5BU5D_t2668173852*)__this->get__items_1();
		int32_t L_6 = (int32_t)__this->get__size_2();
		Array_Clear_m305290227(NULL /*static, unused*/, (RuntimeArray *)(RuntimeArray *)L_5, (int32_t)L_6, (int32_t)1, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_7+(int32_t)1)));
		return;
	}
}
// T[] System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::ToArray()
extern "C"  CustomAttributeTypedArgumentU5BU5D_t2668173852* List_1_ToArray_m3240212600_gshared (List_1_t546271037 * __this, const RuntimeMethod* method)
{
	CustomAttributeTypedArgumentU5BU5D_t2668173852* V_0 = NULL;
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		V_0 = (CustomAttributeTypedArgumentU5BU5D_t2668173852*)((CustomAttributeTypedArgumentU5BU5D_t2668173852*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1), (uint32_t)L_0));
		CustomAttributeTypedArgumentU5BU5D_t2668173852* L_1 = (CustomAttributeTypedArgumentU5BU5D_t2668173852*)__this->get__items_1();
		CustomAttributeTypedArgumentU5BU5D_t2668173852* L_2 = V_0;
		int32_t L_3 = (int32_t)__this->get__size_2();
		Array_Copy_m392395275(NULL /*static, unused*/, (RuntimeArray *)(RuntimeArray *)L_1, (RuntimeArray *)(RuntimeArray *)L_2, (int32_t)L_3, /*hidden argument*/NULL);
		CustomAttributeTypedArgumentU5BU5D_t2668173852* L_4 = V_0;
		return L_4;
	}
}
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m2015618496_gshared (List_1_t546271037 * __this, const RuntimeMethod* method)
{
	{
		CustomAttributeTypedArgumentU5BU5D_t2668173852* L_0 = (CustomAttributeTypedArgumentU5BU5D_t2668173852*)__this->get__items_1();
		NullCheck(L_0);
		return (((int32_t)((int32_t)(((RuntimeArray *)L_0)->max_length))));
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::set_Capacity(System.Int32)
extern "C"  void List_1_set_Capacity_m553904685_gshared (List_1_t546271037 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_set_Capacity_m553904685_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___value0;
		int32_t L_1 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_0) < ((uint32_t)L_1))))
		{
			goto IL_0012;
		}
	}
	{
		ArgumentOutOfRangeException_t1745463579 * L_2 = (ArgumentOutOfRangeException_t1745463579 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t1745463579_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3684908528(L_2, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0012:
	{
		CustomAttributeTypedArgumentU5BU5D_t2668173852** L_3 = (CustomAttributeTypedArgumentU5BU5D_t2668173852**)__this->get_address_of__items_1();
		int32_t L_4 = ___value0;
		((  void (*) (RuntimeObject * /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2668173852**, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 26)->methodPointer)(NULL /*static, unused*/, (CustomAttributeTypedArgumentU5BU5D_t2668173852**)L_3, (int32_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 26));
		return;
	}
}
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::get_Count()
extern "C"  int32_t List_1_get_Count_m2261823747_gshared (List_1_t546271037 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		return L_0;
	}
}
// T System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::get_Item(System.Int32)
extern "C"  CustomAttributeTypedArgument_t2667702721  List_1_get_Item_m3151231619_gshared (List_1_t546271037 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_get_Item_m3151231619_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___index0;
		int32_t L_1 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t1745463579 * L_2 = (ArgumentOutOfRangeException_t1745463579 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t1745463579_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2829693980(L_2, (String_t*)_stringLiteral1171314202, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		CustomAttributeTypedArgumentU5BU5D_t2668173852* L_3 = (CustomAttributeTypedArgumentU5BU5D_t2668173852*)__this->get__items_1();
		int32_t L_4 = ___index0;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		CustomAttributeTypedArgument_t2667702721  L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		return L_6;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::set_Item(System.Int32,T)
extern "C"  void List_1_set_Item_m1870828102_gshared (List_1_t546271037 * __this, int32_t ___index0, CustomAttributeTypedArgument_t2667702721  ___value1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_set_Item_m1870828102_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t546271037 *)__this);
		((  void (*) (List_1_t546271037 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((List_1_t546271037 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		int32_t L_1 = ___index0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_1) == ((uint32_t)L_2))))
		{
			goto IL_001e;
		}
	}
	{
		ArgumentOutOfRangeException_t1745463579 * L_3 = (ArgumentOutOfRangeException_t1745463579 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t1745463579_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2829693980(L_3, (String_t*)_stringLiteral1171314202, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001e:
	{
		CustomAttributeTypedArgumentU5BU5D_t2668173852* L_4 = (CustomAttributeTypedArgumentU5BU5D_t2668173852*)__this->get__items_1();
		int32_t L_5 = ___index0;
		CustomAttributeTypedArgument_t2667702721  L_6 = ___value1;
		NullCheck(L_4);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (CustomAttributeTypedArgument_t2667702721 )L_6);
		return;
	}
}
// System.Void System.Collections.Generic.Queue`1/Enumerator<System.Object>::.ctor(System.Collections.Generic.Queue`1<T>)
extern "C"  void Enumerator__ctor_m1020038761_gshared (Enumerator_t4228099087 * __this, Queue_1_t2668810036 * ___q0, const RuntimeMethod* method)
{
	{
		Queue_1_t2668810036 * L_0 = ___q0;
		__this->set_q_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		Queue_1_t2668810036 * L_1 = ___q0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m1020038761_AdjustorThunk (RuntimeObject * __this, Queue_1_t2668810036 * ___q0, const RuntimeMethod* method)
{
	Enumerator_t4228099087 * _thisAdjusted = reinterpret_cast<Enumerator_t4228099087 *>(__this + 1);
	Enumerator__ctor_m1020038761(_thisAdjusted, ___q0, method);
}
// System.Void System.Collections.Generic.Queue`1/Enumerator<System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2386365462_gshared (Enumerator_t4228099087 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_Reset_m2386365462_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_ver_2();
		Queue_1_t2668810036 * L_1 = (Queue_1_t2668810036 *)__this->get_q_0();
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		if ((((int32_t)L_0) == ((int32_t)L_2)))
		{
			goto IL_001c;
		}
	}
	{
		InvalidOperationException_t1956282378 * L_3 = (InvalidOperationException_t1956282378 *)il2cpp_codegen_object_new(InvalidOperationException_t1956282378_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m3074049270(L_3, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001c:
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2386365462_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t4228099087 * _thisAdjusted = reinterpret_cast<Enumerator_t4228099087 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m2386365462(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Queue`1/Enumerator<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * Enumerator_System_Collections_IEnumerator_get_Current_m4200670532_gshared (Enumerator_t4228099087 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = Enumerator_get_Current_m373730718((Enumerator_t4228099087 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_0;
	}
}
extern "C"  RuntimeObject * Enumerator_System_Collections_IEnumerator_get_Current_m4200670532_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t4228099087 * _thisAdjusted = reinterpret_cast<Enumerator_t4228099087 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m4200670532(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Queue`1/Enumerator<System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m937788283_gshared (Enumerator_t4228099087 * __this, const RuntimeMethod* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void Enumerator_Dispose_m937788283_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t4228099087 * _thisAdjusted = reinterpret_cast<Enumerator_t4228099087 *>(__this + 1);
	Enumerator_Dispose_m937788283(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Queue`1/Enumerator<System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m726149558_gshared (Enumerator_t4228099087 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_MoveNext_m726149558_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t G_B7_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_ver_2();
		Queue_1_t2668810036 * L_1 = (Queue_1_t2668810036 *)__this->get_q_0();
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		if ((((int32_t)L_0) == ((int32_t)L_2)))
		{
			goto IL_001c;
		}
	}
	{
		InvalidOperationException_t1956282378 * L_3 = (InvalidOperationException_t1956282378 *)il2cpp_codegen_object_new(InvalidOperationException_t1956282378_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m3074049270(L_3, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001c:
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_4) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_003a;
		}
	}
	{
		Queue_1_t2668810036 * L_5 = (Queue_1_t2668810036 *)__this->get_q_0();
		NullCheck(L_5);
		int32_t L_6 = (int32_t)L_5->get__size_2();
		__this->set_idx_1(L_6);
	}

IL_003a:
	{
		int32_t L_7 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_7) == ((int32_t)(-1))))
		{
			goto IL_005f;
		}
	}
	{
		int32_t L_8 = (int32_t)__this->get_idx_1();
		int32_t L_9 = (int32_t)((int32_t)((int32_t)L_8-(int32_t)1));
		V_0 = (int32_t)L_9;
		__this->set_idx_1(L_9);
		int32_t L_10 = V_0;
		G_B7_0 = ((((int32_t)((((int32_t)L_10) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0060;
	}

IL_005f:
	{
		G_B7_0 = 0;
	}

IL_0060:
	{
		return (bool)G_B7_0;
	}
}
extern "C"  bool Enumerator_MoveNext_m726149558_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t4228099087 * _thisAdjusted = reinterpret_cast<Enumerator_t4228099087 *>(__this + 1);
	return Enumerator_MoveNext_m726149558(_thisAdjusted, method);
}
// T System.Collections.Generic.Queue`1/Enumerator<System.Object>::get_Current()
extern "C"  RuntimeObject * Enumerator_get_Current_m373730718_gshared (Enumerator_t4228099087 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_get_Current_m373730718_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0012;
		}
	}
	{
		InvalidOperationException_t1956282378 * L_1 = (InvalidOperationException_t1956282378 *)il2cpp_codegen_object_new(InvalidOperationException_t1956282378_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m3074049270(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		Queue_1_t2668810036 * L_2 = (Queue_1_t2668810036 *)__this->get_q_0();
		NullCheck(L_2);
		ObjectU5BU5D_t4290686858* L_3 = (ObjectU5BU5D_t4290686858*)L_2->get__array_0();
		Queue_1_t2668810036 * L_4 = (Queue_1_t2668810036 *)__this->get_q_0();
		NullCheck(L_4);
		int32_t L_5 = (int32_t)L_4->get__size_2();
		int32_t L_6 = (int32_t)__this->get_idx_1();
		Queue_1_t2668810036 * L_7 = (Queue_1_t2668810036 *)__this->get_q_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__head_1();
		Queue_1_t2668810036 * L_9 = (Queue_1_t2668810036 *)__this->get_q_0();
		NullCheck(L_9);
		ObjectU5BU5D_t4290686858* L_10 = (ObjectU5BU5D_t4290686858*)L_9->get__array_0();
		NullCheck(L_10);
		NullCheck(L_3);
		int32_t L_11 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_5-(int32_t)1))-(int32_t)L_6))+(int32_t)L_8))%(int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_10)->max_length))))));
		RuntimeObject * L_12 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		return L_12;
	}
}
extern "C"  RuntimeObject * Enumerator_get_Current_m373730718_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t4228099087 * _thisAdjusted = reinterpret_cast<Enumerator_t4228099087 *>(__this + 1);
	return Enumerator_get_Current_m373730718(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Queue`1/Enumerator<UnityEngine.UnitySynchronizationContext/WorkRequest>::.ctor(System.Collections.Generic.Queue`1<T>)
extern "C"  void Enumerator__ctor_m2190717051_gshared (Enumerator_t3479511895 * __this, Queue_1_t1920222844 * ___q0, const RuntimeMethod* method)
{
	{
		Queue_1_t1920222844 * L_0 = ___q0;
		__this->set_q_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		Queue_1_t1920222844 * L_1 = ___q0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m2190717051_AdjustorThunk (RuntimeObject * __this, Queue_1_t1920222844 * ___q0, const RuntimeMethod* method)
{
	Enumerator_t3479511895 * _thisAdjusted = reinterpret_cast<Enumerator_t3479511895 *>(__this + 1);
	Enumerator__ctor_m2190717051(_thisAdjusted, ___q0, method);
}
// System.Void System.Collections.Generic.Queue`1/Enumerator<UnityEngine.UnitySynchronizationContext/WorkRequest>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2717756568_gshared (Enumerator_t3479511895 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_Reset_m2717756568_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_ver_2();
		Queue_1_t1920222844 * L_1 = (Queue_1_t1920222844 *)__this->get_q_0();
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		if ((((int32_t)L_0) == ((int32_t)L_2)))
		{
			goto IL_001c;
		}
	}
	{
		InvalidOperationException_t1956282378 * L_3 = (InvalidOperationException_t1956282378 *)il2cpp_codegen_object_new(InvalidOperationException_t1956282378_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m3074049270(L_3, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001c:
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2717756568_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t3479511895 * _thisAdjusted = reinterpret_cast<Enumerator_t3479511895 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m2717756568(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Queue`1/Enumerator<UnityEngine.UnitySynchronizationContext/WorkRequest>::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * Enumerator_System_Collections_IEnumerator_get_Current_m3652643606_gshared (Enumerator_t3479511895 * __this, const RuntimeMethod* method)
{
	{
		WorkRequest_t2547429811  L_0 = Enumerator_get_Current_m1195414335((Enumerator_t3479511895 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		WorkRequest_t2547429811  L_1 = L_0;
		RuntimeObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  RuntimeObject * Enumerator_System_Collections_IEnumerator_get_Current_m3652643606_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t3479511895 * _thisAdjusted = reinterpret_cast<Enumerator_t3479511895 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m3652643606(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Queue`1/Enumerator<UnityEngine.UnitySynchronizationContext/WorkRequest>::Dispose()
extern "C"  void Enumerator_Dispose_m3437219742_gshared (Enumerator_t3479511895 * __this, const RuntimeMethod* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void Enumerator_Dispose_m3437219742_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t3479511895 * _thisAdjusted = reinterpret_cast<Enumerator_t3479511895 *>(__this + 1);
	Enumerator_Dispose_m3437219742(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Queue`1/Enumerator<UnityEngine.UnitySynchronizationContext/WorkRequest>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2652681985_gshared (Enumerator_t3479511895 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_MoveNext_m2652681985_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t G_B7_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_ver_2();
		Queue_1_t1920222844 * L_1 = (Queue_1_t1920222844 *)__this->get_q_0();
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		if ((((int32_t)L_0) == ((int32_t)L_2)))
		{
			goto IL_001c;
		}
	}
	{
		InvalidOperationException_t1956282378 * L_3 = (InvalidOperationException_t1956282378 *)il2cpp_codegen_object_new(InvalidOperationException_t1956282378_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m3074049270(L_3, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001c:
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_4) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_003a;
		}
	}
	{
		Queue_1_t1920222844 * L_5 = (Queue_1_t1920222844 *)__this->get_q_0();
		NullCheck(L_5);
		int32_t L_6 = (int32_t)L_5->get__size_2();
		__this->set_idx_1(L_6);
	}

IL_003a:
	{
		int32_t L_7 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_7) == ((int32_t)(-1))))
		{
			goto IL_005f;
		}
	}
	{
		int32_t L_8 = (int32_t)__this->get_idx_1();
		int32_t L_9 = (int32_t)((int32_t)((int32_t)L_8-(int32_t)1));
		V_0 = (int32_t)L_9;
		__this->set_idx_1(L_9);
		int32_t L_10 = V_0;
		G_B7_0 = ((((int32_t)((((int32_t)L_10) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0060;
	}

IL_005f:
	{
		G_B7_0 = 0;
	}

IL_0060:
	{
		return (bool)G_B7_0;
	}
}
extern "C"  bool Enumerator_MoveNext_m2652681985_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t3479511895 * _thisAdjusted = reinterpret_cast<Enumerator_t3479511895 *>(__this + 1);
	return Enumerator_MoveNext_m2652681985(_thisAdjusted, method);
}
// T System.Collections.Generic.Queue`1/Enumerator<UnityEngine.UnitySynchronizationContext/WorkRequest>::get_Current()
extern "C"  WorkRequest_t2547429811  Enumerator_get_Current_m1195414335_gshared (Enumerator_t3479511895 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_get_Current_m1195414335_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0012;
		}
	}
	{
		InvalidOperationException_t1956282378 * L_1 = (InvalidOperationException_t1956282378 *)il2cpp_codegen_object_new(InvalidOperationException_t1956282378_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m3074049270(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		Queue_1_t1920222844 * L_2 = (Queue_1_t1920222844 *)__this->get_q_0();
		NullCheck(L_2);
		WorkRequestU5BU5D_t3723184674* L_3 = (WorkRequestU5BU5D_t3723184674*)L_2->get__array_0();
		Queue_1_t1920222844 * L_4 = (Queue_1_t1920222844 *)__this->get_q_0();
		NullCheck(L_4);
		int32_t L_5 = (int32_t)L_4->get__size_2();
		int32_t L_6 = (int32_t)__this->get_idx_1();
		Queue_1_t1920222844 * L_7 = (Queue_1_t1920222844 *)__this->get_q_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__head_1();
		Queue_1_t1920222844 * L_9 = (Queue_1_t1920222844 *)__this->get_q_0();
		NullCheck(L_9);
		WorkRequestU5BU5D_t3723184674* L_10 = (WorkRequestU5BU5D_t3723184674*)L_9->get__array_0();
		NullCheck(L_10);
		NullCheck(L_3);
		int32_t L_11 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_5-(int32_t)1))-(int32_t)L_6))+(int32_t)L_8))%(int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_10)->max_length))))));
		WorkRequest_t2547429811  L_12 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		return L_12;
	}
}
extern "C"  WorkRequest_t2547429811  Enumerator_get_Current_m1195414335_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t3479511895 * _thisAdjusted = reinterpret_cast<Enumerator_t3479511895 *>(__this + 1);
	return Enumerator_get_Current_m1195414335(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Queue`1<System.Object>::.ctor()
extern "C"  void Queue_1__ctor_m1429745961_gshared (Queue_1_t2668810036 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m3446193457((RuntimeObject *)__this, /*hidden argument*/NULL);
		__this->set__array_0(((ObjectU5BU5D_t4290686858*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (uint32_t)0)));
		return;
	}
}
// System.Void System.Collections.Generic.Queue`1<System.Object>::.ctor(System.Int32)
extern "C"  void Queue_1__ctor_m460898850_gshared (Queue_1_t2668810036 * __this, int32_t ___count0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Queue_1__ctor_m460898850_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m3446193457((RuntimeObject *)__this, /*hidden argument*/NULL);
		int32_t L_0 = ___count0;
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		ArgumentOutOfRangeException_t1745463579 * L_1 = (ArgumentOutOfRangeException_t1745463579 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t1745463579_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2829693980(L_1, (String_t*)_stringLiteral3036108506, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = ___count0;
		__this->set__array_0(((ObjectU5BU5D_t4290686858*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (uint32_t)L_2)));
		return;
	}
}
// System.Void System.Collections.Generic.Queue`1<System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Queue_1_System_Collections_ICollection_CopyTo_m3128935235_gshared (Queue_1_t2668810036 * __this, RuntimeArray * ___array0, int32_t ___idx1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Queue_1_System_Collections_ICollection_CopyTo_m3128935235_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	Exception_t3361176243 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3361176243 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		RuntimeArray * L_0 = ___array0;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		ArgumentNullException_t517973450 * L_1 = (ArgumentNullException_t517973450 *)il2cpp_codegen_object_new(ArgumentNullException_t517973450_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m348650132(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_000c:
	{
		int32_t L_2 = ___idx1;
		RuntimeArray * L_3 = ___array0;
		NullCheck((RuntimeArray *)L_3);
		int32_t L_4 = Array_get_Length_m1201869234((RuntimeArray *)L_3, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_2) > ((uint32_t)L_4))))
		{
			goto IL_001e;
		}
	}
	{
		ArgumentOutOfRangeException_t1745463579 * L_5 = (ArgumentOutOfRangeException_t1745463579 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t1745463579_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3684908528(L_5, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_001e:
	{
		RuntimeArray * L_6 = ___array0;
		NullCheck((RuntimeArray *)L_6);
		int32_t L_7 = Array_get_Length_m1201869234((RuntimeArray *)L_6, /*hidden argument*/NULL);
		int32_t L_8 = ___idx1;
		int32_t L_9 = (int32_t)__this->get__size_2();
		if ((((int32_t)((int32_t)((int32_t)L_7-(int32_t)L_8))) >= ((int32_t)L_9)))
		{
			goto IL_0037;
		}
	}
	{
		ArgumentOutOfRangeException_t1745463579 * L_10 = (ArgumentOutOfRangeException_t1745463579 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t1745463579_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3684908528(L_10, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_10);
	}

IL_0037:
	{
		int32_t L_11 = (int32_t)__this->get__size_2();
		if (L_11)
		{
			goto IL_0043;
		}
	}
	{
		return;
	}

IL_0043:
	try
	{ // begin try (depth: 1)
		{
			ObjectU5BU5D_t4290686858* L_12 = (ObjectU5BU5D_t4290686858*)__this->get__array_0();
			NullCheck(L_12);
			V_0 = (int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_12)->max_length))));
			int32_t L_13 = V_0;
			int32_t L_14 = (int32_t)__this->get__head_1();
			V_1 = (int32_t)((int32_t)((int32_t)L_13-(int32_t)L_14));
			ObjectU5BU5D_t4290686858* L_15 = (ObjectU5BU5D_t4290686858*)__this->get__array_0();
			int32_t L_16 = (int32_t)__this->get__head_1();
			RuntimeArray * L_17 = ___array0;
			int32_t L_18 = ___idx1;
			int32_t L_19 = (int32_t)__this->get__size_2();
			int32_t L_20 = V_1;
			int32_t L_21 = Math_Min_m1276480861(NULL /*static, unused*/, (int32_t)L_19, (int32_t)L_20, /*hidden argument*/NULL);
			Array_Copy_m1287631566(NULL /*static, unused*/, (RuntimeArray *)(RuntimeArray *)L_15, (int32_t)L_16, (RuntimeArray *)L_17, (int32_t)L_18, (int32_t)L_21, /*hidden argument*/NULL);
			int32_t L_22 = (int32_t)__this->get__size_2();
			int32_t L_23 = V_1;
			if ((((int32_t)L_22) <= ((int32_t)L_23)))
			{
				goto IL_0098;
			}
		}

IL_0080:
		{
			ObjectU5BU5D_t4290686858* L_24 = (ObjectU5BU5D_t4290686858*)__this->get__array_0();
			RuntimeArray * L_25 = ___array0;
			int32_t L_26 = ___idx1;
			int32_t L_27 = V_1;
			int32_t L_28 = (int32_t)__this->get__size_2();
			int32_t L_29 = V_1;
			Array_Copy_m1287631566(NULL /*static, unused*/, (RuntimeArray *)(RuntimeArray *)L_24, (int32_t)0, (RuntimeArray *)L_25, (int32_t)((int32_t)((int32_t)L_26+(int32_t)L_27)), (int32_t)((int32_t)((int32_t)L_28-(int32_t)L_29)), /*hidden argument*/NULL);
		}

IL_0098:
		{
			goto IL_00a9;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3361176243 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (ArrayTypeMismatchException_t1927459263_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_009d;
		throw e;
	}

CATCH_009d:
	{ // begin catch(System.ArrayTypeMismatchException)
		{
			ArgumentException_t1372035057 * L_30 = (ArgumentException_t1372035057 *)il2cpp_codegen_object_new(ArgumentException_t1372035057_il2cpp_TypeInfo_var);
			ArgumentException__ctor_m996376509(L_30, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_30);
		}

IL_00a4:
		{
			goto IL_00a9;
		}
	} // end catch (depth: 1)

IL_00a9:
	{
		return;
	}
}
// System.Object System.Collections.Generic.Queue`1<System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  RuntimeObject * Queue_1_System_Collections_ICollection_get_SyncRoot_m1772186678_gshared (Queue_1_t2668810036 * __this, const RuntimeMethod* method)
{
	{
		return __this;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.Queue`1<System.Object>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  RuntimeObject* Queue_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1225540566_gshared (Queue_1_t2668810036 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((Queue_1_t2668810036 *)__this);
		Enumerator_t4228099087  L_0 = ((  Enumerator_t4228099087  (*) (Queue_1_t2668810036 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((Queue_1_t2668810036 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Enumerator_t4228099087  L_1 = L_0;
		RuntimeObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		return (RuntimeObject*)L_2;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.Queue`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  RuntimeObject* Queue_1_System_Collections_IEnumerable_GetEnumerator_m2911827169_gshared (Queue_1_t2668810036 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((Queue_1_t2668810036 *)__this);
		Enumerator_t4228099087  L_0 = ((  Enumerator_t4228099087  (*) (Queue_1_t2668810036 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((Queue_1_t2668810036 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Enumerator_t4228099087  L_1 = L_0;
		RuntimeObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		return (RuntimeObject*)L_2;
	}
}
// T System.Collections.Generic.Queue`1<System.Object>::Dequeue()
extern "C"  RuntimeObject * Queue_1_Dequeue_m426484238_gshared (Queue_1_t2668810036 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Queue_1_Dequeue_m426484238_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject * V_0 = NULL;
	RuntimeObject * V_1 = NULL;
	int32_t V_2 = 0;
	{
		NullCheck((Queue_1_t2668810036 *)__this);
		RuntimeObject * L_0 = ((  RuntimeObject * (*) (Queue_1_t2668810036 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((Queue_1_t2668810036 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (RuntimeObject *)L_0;
		ObjectU5BU5D_t4290686858* L_1 = (ObjectU5BU5D_t4290686858*)__this->get__array_0();
		int32_t L_2 = (int32_t)__this->get__head_1();
		Initobj (RuntimeObject_il2cpp_TypeInfo_var, (&V_1));
		RuntimeObject * L_3 = V_1;
		NullCheck(L_1);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(L_2), (RuntimeObject *)L_3);
		int32_t L_4 = (int32_t)__this->get__head_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4+(int32_t)1));
		V_2 = (int32_t)L_5;
		__this->set__head_1(L_5);
		int32_t L_6 = V_2;
		ObjectU5BU5D_t4290686858* L_7 = (ObjectU5BU5D_t4290686858*)__this->get__array_0();
		NullCheck(L_7);
		if ((!(((uint32_t)L_6) == ((uint32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_7)->max_length))))))))
		{
			goto IL_0046;
		}
	}
	{
		__this->set__head_1(0);
	}

IL_0046:
	{
		int32_t L_8 = (int32_t)__this->get__size_2();
		__this->set__size_2(((int32_t)((int32_t)L_8-(int32_t)1)));
		int32_t L_9 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_9+(int32_t)1)));
		RuntimeObject * L_10 = V_0;
		return L_10;
	}
}
// T System.Collections.Generic.Queue`1<System.Object>::Peek()
extern "C"  RuntimeObject * Queue_1_Peek_m632453695_gshared (Queue_1_t2668810036 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Queue_1_Peek_m632453695_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		InvalidOperationException_t1956282378 * L_1 = (InvalidOperationException_t1956282378 *)il2cpp_codegen_object_new(InvalidOperationException_t1956282378_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m3074049270(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		ObjectU5BU5D_t4290686858* L_2 = (ObjectU5BU5D_t4290686858*)__this->get__array_0();
		int32_t L_3 = (int32_t)__this->get__head_1();
		NullCheck(L_2);
		int32_t L_4 = L_3;
		RuntimeObject * L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		return L_5;
	}
}
// System.Int32 System.Collections.Generic.Queue`1<System.Object>::get_Count()
extern "C"  int32_t Queue_1_get_Count_m2243988828_gshared (Queue_1_t2668810036 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		return L_0;
	}
}
// System.Collections.Generic.Queue`1/Enumerator<T> System.Collections.Generic.Queue`1<System.Object>::GetEnumerator()
extern "C"  Enumerator_t4228099087  Queue_1_GetEnumerator_m1999917688_gshared (Queue_1_t2668810036 * __this, const RuntimeMethod* method)
{
	{
		Enumerator_t4228099087  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Enumerator__ctor_m1020038761((&L_0), (Queue_1_t2668810036 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_0;
	}
}
// System.Void System.Collections.Generic.Queue`1<UnityEngine.UnitySynchronizationContext/WorkRequest>::.ctor()
extern "C"  void Queue_1__ctor_m1926024332_gshared (Queue_1_t1920222844 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m3446193457((RuntimeObject *)__this, /*hidden argument*/NULL);
		__this->set__array_0(((WorkRequestU5BU5D_t3723184674*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (uint32_t)0)));
		return;
	}
}
// System.Void System.Collections.Generic.Queue`1<UnityEngine.UnitySynchronizationContext/WorkRequest>::.ctor(System.Int32)
extern "C"  void Queue_1__ctor_m1955792661_gshared (Queue_1_t1920222844 * __this, int32_t ___count0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Queue_1__ctor_m1955792661_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m3446193457((RuntimeObject *)__this, /*hidden argument*/NULL);
		int32_t L_0 = ___count0;
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		ArgumentOutOfRangeException_t1745463579 * L_1 = (ArgumentOutOfRangeException_t1745463579 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t1745463579_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2829693980(L_1, (String_t*)_stringLiteral3036108506, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = ___count0;
		__this->set__array_0(((WorkRequestU5BU5D_t3723184674*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (uint32_t)L_2)));
		return;
	}
}
// System.Void System.Collections.Generic.Queue`1<UnityEngine.UnitySynchronizationContext/WorkRequest>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Queue_1_System_Collections_ICollection_CopyTo_m3735502342_gshared (Queue_1_t1920222844 * __this, RuntimeArray * ___array0, int32_t ___idx1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Queue_1_System_Collections_ICollection_CopyTo_m3735502342_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	Exception_t3361176243 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3361176243 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		RuntimeArray * L_0 = ___array0;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		ArgumentNullException_t517973450 * L_1 = (ArgumentNullException_t517973450 *)il2cpp_codegen_object_new(ArgumentNullException_t517973450_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m348650132(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_000c:
	{
		int32_t L_2 = ___idx1;
		RuntimeArray * L_3 = ___array0;
		NullCheck((RuntimeArray *)L_3);
		int32_t L_4 = Array_get_Length_m1201869234((RuntimeArray *)L_3, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_2) > ((uint32_t)L_4))))
		{
			goto IL_001e;
		}
	}
	{
		ArgumentOutOfRangeException_t1745463579 * L_5 = (ArgumentOutOfRangeException_t1745463579 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t1745463579_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3684908528(L_5, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_001e:
	{
		RuntimeArray * L_6 = ___array0;
		NullCheck((RuntimeArray *)L_6);
		int32_t L_7 = Array_get_Length_m1201869234((RuntimeArray *)L_6, /*hidden argument*/NULL);
		int32_t L_8 = ___idx1;
		int32_t L_9 = (int32_t)__this->get__size_2();
		if ((((int32_t)((int32_t)((int32_t)L_7-(int32_t)L_8))) >= ((int32_t)L_9)))
		{
			goto IL_0037;
		}
	}
	{
		ArgumentOutOfRangeException_t1745463579 * L_10 = (ArgumentOutOfRangeException_t1745463579 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t1745463579_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3684908528(L_10, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_10);
	}

IL_0037:
	{
		int32_t L_11 = (int32_t)__this->get__size_2();
		if (L_11)
		{
			goto IL_0043;
		}
	}
	{
		return;
	}

IL_0043:
	try
	{ // begin try (depth: 1)
		{
			WorkRequestU5BU5D_t3723184674* L_12 = (WorkRequestU5BU5D_t3723184674*)__this->get__array_0();
			NullCheck(L_12);
			V_0 = (int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_12)->max_length))));
			int32_t L_13 = V_0;
			int32_t L_14 = (int32_t)__this->get__head_1();
			V_1 = (int32_t)((int32_t)((int32_t)L_13-(int32_t)L_14));
			WorkRequestU5BU5D_t3723184674* L_15 = (WorkRequestU5BU5D_t3723184674*)__this->get__array_0();
			int32_t L_16 = (int32_t)__this->get__head_1();
			RuntimeArray * L_17 = ___array0;
			int32_t L_18 = ___idx1;
			int32_t L_19 = (int32_t)__this->get__size_2();
			int32_t L_20 = V_1;
			int32_t L_21 = Math_Min_m1276480861(NULL /*static, unused*/, (int32_t)L_19, (int32_t)L_20, /*hidden argument*/NULL);
			Array_Copy_m1287631566(NULL /*static, unused*/, (RuntimeArray *)(RuntimeArray *)L_15, (int32_t)L_16, (RuntimeArray *)L_17, (int32_t)L_18, (int32_t)L_21, /*hidden argument*/NULL);
			int32_t L_22 = (int32_t)__this->get__size_2();
			int32_t L_23 = V_1;
			if ((((int32_t)L_22) <= ((int32_t)L_23)))
			{
				goto IL_0098;
			}
		}

IL_0080:
		{
			WorkRequestU5BU5D_t3723184674* L_24 = (WorkRequestU5BU5D_t3723184674*)__this->get__array_0();
			RuntimeArray * L_25 = ___array0;
			int32_t L_26 = ___idx1;
			int32_t L_27 = V_1;
			int32_t L_28 = (int32_t)__this->get__size_2();
			int32_t L_29 = V_1;
			Array_Copy_m1287631566(NULL /*static, unused*/, (RuntimeArray *)(RuntimeArray *)L_24, (int32_t)0, (RuntimeArray *)L_25, (int32_t)((int32_t)((int32_t)L_26+(int32_t)L_27)), (int32_t)((int32_t)((int32_t)L_28-(int32_t)L_29)), /*hidden argument*/NULL);
		}

IL_0098:
		{
			goto IL_00a9;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3361176243 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (ArrayTypeMismatchException_t1927459263_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_009d;
		throw e;
	}

CATCH_009d:
	{ // begin catch(System.ArrayTypeMismatchException)
		{
			ArgumentException_t1372035057 * L_30 = (ArgumentException_t1372035057 *)il2cpp_codegen_object_new(ArgumentException_t1372035057_il2cpp_TypeInfo_var);
			ArgumentException__ctor_m996376509(L_30, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_30);
		}

IL_00a4:
		{
			goto IL_00a9;
		}
	} // end catch (depth: 1)

IL_00a9:
	{
		return;
	}
}
// System.Object System.Collections.Generic.Queue`1<UnityEngine.UnitySynchronizationContext/WorkRequest>::System.Collections.ICollection.get_SyncRoot()
extern "C"  RuntimeObject * Queue_1_System_Collections_ICollection_get_SyncRoot_m2928280003_gshared (Queue_1_t1920222844 * __this, const RuntimeMethod* method)
{
	{
		return __this;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.Queue`1<UnityEngine.UnitySynchronizationContext/WorkRequest>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  RuntimeObject* Queue_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m621500733_gshared (Queue_1_t1920222844 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((Queue_1_t1920222844 *)__this);
		Enumerator_t3479511895  L_0 = ((  Enumerator_t3479511895  (*) (Queue_1_t1920222844 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((Queue_1_t1920222844 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Enumerator_t3479511895  L_1 = L_0;
		RuntimeObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		return (RuntimeObject*)L_2;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.Queue`1<UnityEngine.UnitySynchronizationContext/WorkRequest>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  RuntimeObject* Queue_1_System_Collections_IEnumerable_GetEnumerator_m45051010_gshared (Queue_1_t1920222844 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((Queue_1_t1920222844 *)__this);
		Enumerator_t3479511895  L_0 = ((  Enumerator_t3479511895  (*) (Queue_1_t1920222844 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((Queue_1_t1920222844 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Enumerator_t3479511895  L_1 = L_0;
		RuntimeObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		return (RuntimeObject*)L_2;
	}
}
// T System.Collections.Generic.Queue`1<UnityEngine.UnitySynchronizationContext/WorkRequest>::Dequeue()
extern "C"  WorkRequest_t2547429811  Queue_1_Dequeue_m926929859_gshared (Queue_1_t1920222844 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Queue_1_Dequeue_m926929859_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	WorkRequest_t2547429811  V_0;
	memset(&V_0, 0, sizeof(V_0));
	WorkRequest_t2547429811  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t V_2 = 0;
	{
		NullCheck((Queue_1_t1920222844 *)__this);
		WorkRequest_t2547429811  L_0 = ((  WorkRequest_t2547429811  (*) (Queue_1_t1920222844 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((Queue_1_t1920222844 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (WorkRequest_t2547429811 )L_0;
		WorkRequestU5BU5D_t3723184674* L_1 = (WorkRequestU5BU5D_t3723184674*)__this->get__array_0();
		int32_t L_2 = (int32_t)__this->get__head_1();
		Initobj (WorkRequest_t2547429811_il2cpp_TypeInfo_var, (&V_1));
		WorkRequest_t2547429811  L_3 = V_1;
		NullCheck(L_1);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(L_2), (WorkRequest_t2547429811 )L_3);
		int32_t L_4 = (int32_t)__this->get__head_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4+(int32_t)1));
		V_2 = (int32_t)L_5;
		__this->set__head_1(L_5);
		int32_t L_6 = V_2;
		WorkRequestU5BU5D_t3723184674* L_7 = (WorkRequestU5BU5D_t3723184674*)__this->get__array_0();
		NullCheck(L_7);
		if ((!(((uint32_t)L_6) == ((uint32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_7)->max_length))))))))
		{
			goto IL_0046;
		}
	}
	{
		__this->set__head_1(0);
	}

IL_0046:
	{
		int32_t L_8 = (int32_t)__this->get__size_2();
		__this->set__size_2(((int32_t)((int32_t)L_8-(int32_t)1)));
		int32_t L_9 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_9+(int32_t)1)));
		WorkRequest_t2547429811  L_10 = V_0;
		return L_10;
	}
}
// T System.Collections.Generic.Queue`1<UnityEngine.UnitySynchronizationContext/WorkRequest>::Peek()
extern "C"  WorkRequest_t2547429811  Queue_1_Peek_m1793053076_gshared (Queue_1_t1920222844 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Queue_1_Peek_m1793053076_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		InvalidOperationException_t1956282378 * L_1 = (InvalidOperationException_t1956282378 *)il2cpp_codegen_object_new(InvalidOperationException_t1956282378_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m3074049270(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		WorkRequestU5BU5D_t3723184674* L_2 = (WorkRequestU5BU5D_t3723184674*)__this->get__array_0();
		int32_t L_3 = (int32_t)__this->get__head_1();
		NullCheck(L_2);
		int32_t L_4 = L_3;
		WorkRequest_t2547429811  L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		return L_5;
	}
}
// System.Int32 System.Collections.Generic.Queue`1<UnityEngine.UnitySynchronizationContext/WorkRequest>::get_Count()
extern "C"  int32_t Queue_1_get_Count_m3721210504_gshared (Queue_1_t1920222844 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		return L_0;
	}
}
// System.Collections.Generic.Queue`1/Enumerator<T> System.Collections.Generic.Queue`1<UnityEngine.UnitySynchronizationContext/WorkRequest>::GetEnumerator()
extern "C"  Enumerator_t3479511895  Queue_1_GetEnumerator_m1263559340_gshared (Queue_1_t1920222844 * __this, const RuntimeMethod* method)
{
	{
		Enumerator_t3479511895  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Enumerator__ctor_m2190717051((&L_0), (Queue_1_t1920222844 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_0;
	}
}
// System.Void System.Collections.Generic.Stack`1/Enumerator<System.Object>::.ctor(System.Collections.Generic.Stack`1<T>)
extern "C"  void Enumerator__ctor_m1549060886_gshared (Enumerator_t1041909981 * __this, Stack_1_t3886787695 * ___t0, const RuntimeMethod* method)
{
	{
		Stack_1_t3886787695 * L_0 = ___t0;
		__this->set_parent_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		Stack_1_t3886787695 * L_1 = ___t0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_2();
		__this->set__version_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m1549060886_AdjustorThunk (RuntimeObject * __this, Stack_1_t3886787695 * ___t0, const RuntimeMethod* method)
{
	Enumerator_t1041909981 * _thisAdjusted = reinterpret_cast<Enumerator_t1041909981 *>(__this + 1);
	Enumerator__ctor_m1549060886(_thisAdjusted, ___t0, method);
}
// System.Void System.Collections.Generic.Stack`1/Enumerator<System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m4168111423_gshared (Enumerator_t1041909981 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_Reset_m4168111423_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get__version_2();
		Stack_1_t3886787695 * L_1 = (Stack_1_t3886787695 *)__this->get_parent_0();
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_2();
		if ((((int32_t)L_0) == ((int32_t)L_2)))
		{
			goto IL_001c;
		}
	}
	{
		InvalidOperationException_t1956282378 * L_3 = (InvalidOperationException_t1956282378 *)il2cpp_codegen_object_new(InvalidOperationException_t1956282378_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m3074049270(L_3, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001c:
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m4168111423_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t1041909981 * _thisAdjusted = reinterpret_cast<Enumerator_t1041909981 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m4168111423(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Stack`1/Enumerator<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * Enumerator_System_Collections_IEnumerator_get_Current_m3744667164_gshared (Enumerator_t1041909981 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = Enumerator_get_Current_m887231645((Enumerator_t1041909981 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_0;
	}
}
extern "C"  RuntimeObject * Enumerator_System_Collections_IEnumerator_get_Current_m3744667164_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t1041909981 * _thisAdjusted = reinterpret_cast<Enumerator_t1041909981 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m3744667164(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Stack`1/Enumerator<System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m2724312910_gshared (Enumerator_t1041909981 * __this, const RuntimeMethod* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void Enumerator_Dispose_m2724312910_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t1041909981 * _thisAdjusted = reinterpret_cast<Enumerator_t1041909981 *>(__this + 1);
	Enumerator_Dispose_m2724312910(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Stack`1/Enumerator<System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2828987763_gshared (Enumerator_t1041909981 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_MoveNext_m2828987763_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t G_B7_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get__version_2();
		Stack_1_t3886787695 * L_1 = (Stack_1_t3886787695 *)__this->get_parent_0();
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_2();
		if ((((int32_t)L_0) == ((int32_t)L_2)))
		{
			goto IL_001c;
		}
	}
	{
		InvalidOperationException_t1956282378 * L_3 = (InvalidOperationException_t1956282378 *)il2cpp_codegen_object_new(InvalidOperationException_t1956282378_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m3074049270(L_3, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001c:
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_4) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_003a;
		}
	}
	{
		Stack_1_t3886787695 * L_5 = (Stack_1_t3886787695 *)__this->get_parent_0();
		NullCheck(L_5);
		int32_t L_6 = (int32_t)L_5->get__size_1();
		__this->set_idx_1(L_6);
	}

IL_003a:
	{
		int32_t L_7 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_7) == ((int32_t)(-1))))
		{
			goto IL_005f;
		}
	}
	{
		int32_t L_8 = (int32_t)__this->get_idx_1();
		int32_t L_9 = (int32_t)((int32_t)((int32_t)L_8-(int32_t)1));
		V_0 = (int32_t)L_9;
		__this->set_idx_1(L_9);
		int32_t L_10 = V_0;
		G_B7_0 = ((((int32_t)((((int32_t)L_10) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0060;
	}

IL_005f:
	{
		G_B7_0 = 0;
	}

IL_0060:
	{
		return (bool)G_B7_0;
	}
}
extern "C"  bool Enumerator_MoveNext_m2828987763_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t1041909981 * _thisAdjusted = reinterpret_cast<Enumerator_t1041909981 *>(__this + 1);
	return Enumerator_MoveNext_m2828987763(_thisAdjusted, method);
}
// T System.Collections.Generic.Stack`1/Enumerator<System.Object>::get_Current()
extern "C"  RuntimeObject * Enumerator_get_Current_m887231645_gshared (Enumerator_t1041909981 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_get_Current_m887231645_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0012;
		}
	}
	{
		InvalidOperationException_t1956282378 * L_1 = (InvalidOperationException_t1956282378 *)il2cpp_codegen_object_new(InvalidOperationException_t1956282378_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m3074049270(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		Stack_1_t3886787695 * L_2 = (Stack_1_t3886787695 *)__this->get_parent_0();
		NullCheck(L_2);
		ObjectU5BU5D_t4290686858* L_3 = (ObjectU5BU5D_t4290686858*)L_2->get__array_0();
		int32_t L_4 = (int32_t)__this->get_idx_1();
		NullCheck(L_3);
		int32_t L_5 = L_4;
		RuntimeObject * L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		return L_6;
	}
}
extern "C"  RuntimeObject * Enumerator_get_Current_m887231645_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t1041909981 * _thisAdjusted = reinterpret_cast<Enumerator_t1041909981 *>(__this + 1);
	return Enumerator_get_Current_m887231645(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Stack`1<System.Object>::.ctor()
extern "C"  void Stack_1__ctor_m2269989442_gshared (Stack_1_t3886787695 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m3446193457((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object System.Collections.Generic.Stack`1<System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  RuntimeObject * Stack_1_System_Collections_ICollection_get_SyncRoot_m2840273767_gshared (Stack_1_t3886787695 * __this, const RuntimeMethod* method)
{
	{
		return __this;
	}
}
// System.Void System.Collections.Generic.Stack`1<System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Stack_1_System_Collections_ICollection_CopyTo_m1246417032_gshared (Stack_1_t3886787695 * __this, RuntimeArray * ___dest0, int32_t ___idx1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Stack_1_System_Collections_ICollection_CopyTo_m1246417032_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Exception_t3361176243 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3361176243 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			ObjectU5BU5D_t4290686858* L_0 = (ObjectU5BU5D_t4290686858*)__this->get__array_0();
			if (!L_0)
			{
				goto IL_0025;
			}
		}

IL_000b:
		{
			ObjectU5BU5D_t4290686858* L_1 = (ObjectU5BU5D_t4290686858*)__this->get__array_0();
			RuntimeArray * L_2 = ___dest0;
			int32_t L_3 = ___idx1;
			NullCheck((RuntimeArray *)(RuntimeArray *)L_1);
			Array_CopyTo_m332099161((RuntimeArray *)(RuntimeArray *)L_1, (RuntimeArray *)L_2, (int32_t)L_3, /*hidden argument*/NULL);
			RuntimeArray * L_4 = ___dest0;
			int32_t L_5 = ___idx1;
			int32_t L_6 = (int32_t)__this->get__size_1();
			Array_Reverse_m3946279211(NULL /*static, unused*/, (RuntimeArray *)L_4, (int32_t)L_5, (int32_t)L_6, /*hidden argument*/NULL);
		}

IL_0025:
		{
			goto IL_0036;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3361176243 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (ArrayTypeMismatchException_t1927459263_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_002a;
		throw e;
	}

CATCH_002a:
	{ // begin catch(System.ArrayTypeMismatchException)
		{
			ArgumentException_t1372035057 * L_7 = (ArgumentException_t1372035057 *)il2cpp_codegen_object_new(ArgumentException_t1372035057_il2cpp_TypeInfo_var);
			ArgumentException__ctor_m996376509(L_7, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
		}

IL_0031:
		{
			goto IL_0036;
		}
	} // end catch (depth: 1)

IL_0036:
	{
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.Stack`1<System.Object>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  RuntimeObject* Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3647686190_gshared (Stack_1_t3886787695 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((Stack_1_t3886787695 *)__this);
		Enumerator_t1041909981  L_0 = ((  Enumerator_t1041909981  (*) (Stack_1_t3886787695 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Stack_1_t3886787695 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		Enumerator_t1041909981  L_1 = L_0;
		RuntimeObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1), &L_1);
		return (RuntimeObject*)L_2;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.Stack`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  RuntimeObject* Stack_1_System_Collections_IEnumerable_GetEnumerator_m1956083755_gshared (Stack_1_t3886787695 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((Stack_1_t3886787695 *)__this);
		Enumerator_t1041909981  L_0 = ((  Enumerator_t1041909981  (*) (Stack_1_t3886787695 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Stack_1_t3886787695 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		Enumerator_t1041909981  L_1 = L_0;
		RuntimeObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1), &L_1);
		return (RuntimeObject*)L_2;
	}
}
// T System.Collections.Generic.Stack`1<System.Object>::Pop()
extern "C"  RuntimeObject * Stack_1_Pop_m2727410536_gshared (Stack_1_t3886787695 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Stack_1_Pop_m2727410536_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject * V_0 = NULL;
	int32_t V_1 = 0;
	RuntimeObject * V_2 = NULL;
	{
		int32_t L_0 = (int32_t)__this->get__size_1();
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		InvalidOperationException_t1956282378 * L_1 = (InvalidOperationException_t1956282378 *)il2cpp_codegen_object_new(InvalidOperationException_t1956282378_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m3074049270(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		int32_t L_2 = (int32_t)__this->get__version_2();
		__this->set__version_2(((int32_t)((int32_t)L_2+(int32_t)1)));
		ObjectU5BU5D_t4290686858* L_3 = (ObjectU5BU5D_t4290686858*)__this->get__array_0();
		int32_t L_4 = (int32_t)__this->get__size_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_1 = (int32_t)L_5;
		__this->set__size_1(L_5);
		int32_t L_6 = V_1;
		NullCheck(L_3);
		int32_t L_7 = L_6;
		RuntimeObject * L_8 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		V_0 = (RuntimeObject *)L_8;
		ObjectU5BU5D_t4290686858* L_9 = (ObjectU5BU5D_t4290686858*)__this->get__array_0();
		int32_t L_10 = (int32_t)__this->get__size_1();
		Initobj (RuntimeObject_il2cpp_TypeInfo_var, (&V_2));
		RuntimeObject * L_11 = V_2;
		NullCheck(L_9);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(L_10), (RuntimeObject *)L_11);
		RuntimeObject * L_12 = V_0;
		return L_12;
	}
}
// System.Void System.Collections.Generic.Stack`1<System.Object>::Push(T)
extern "C"  void Stack_1_Push_m453132609_gshared (Stack_1_t3886787695 * __this, RuntimeObject * ___t0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	ObjectU5BU5D_t4290686858** G_B4_0 = NULL;
	ObjectU5BU5D_t4290686858** G_B3_0 = NULL;
	int32_t G_B5_0 = 0;
	ObjectU5BU5D_t4290686858** G_B5_1 = NULL;
	{
		ObjectU5BU5D_t4290686858* L_0 = (ObjectU5BU5D_t4290686858*)__this->get__array_0();
		if (!L_0)
		{
			goto IL_001e;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get__size_1();
		ObjectU5BU5D_t4290686858* L_2 = (ObjectU5BU5D_t4290686858*)__this->get__array_0();
		NullCheck(L_2);
		if ((!(((uint32_t)L_1) == ((uint32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_2)->max_length))))))))
		{
			goto IL_0043;
		}
	}

IL_001e:
	{
		ObjectU5BU5D_t4290686858** L_3 = (ObjectU5BU5D_t4290686858**)__this->get_address_of__array_0();
		int32_t L_4 = (int32_t)__this->get__size_1();
		G_B3_0 = L_3;
		if (L_4)
		{
			G_B4_0 = L_3;
			goto IL_0036;
		}
	}
	{
		G_B5_0 = ((int32_t)16);
		G_B5_1 = G_B3_0;
		goto IL_003e;
	}

IL_0036:
	{
		int32_t L_5 = (int32_t)__this->get__size_1();
		G_B5_0 = ((int32_t)((int32_t)2*(int32_t)L_5));
		G_B5_1 = G_B4_0;
	}

IL_003e:
	{
		((  void (*) (RuntimeObject * /* static, unused */, ObjectU5BU5D_t4290686858**, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(NULL /*static, unused*/, (ObjectU5BU5D_t4290686858**)G_B5_1, (int32_t)G_B5_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
	}

IL_0043:
	{
		int32_t L_6 = (int32_t)__this->get__version_2();
		__this->set__version_2(((int32_t)((int32_t)L_6+(int32_t)1)));
		ObjectU5BU5D_t4290686858* L_7 = (ObjectU5BU5D_t4290686858*)__this->get__array_0();
		int32_t L_8 = (int32_t)__this->get__size_1();
		int32_t L_9 = (int32_t)L_8;
		V_0 = (int32_t)L_9;
		__this->set__size_1(((int32_t)((int32_t)L_9+(int32_t)1)));
		int32_t L_10 = V_0;
		RuntimeObject * L_11 = ___t0;
		NullCheck(L_7);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(L_10), (RuntimeObject *)L_11);
		return;
	}
}
// System.Int32 System.Collections.Generic.Stack`1<System.Object>::get_Count()
extern "C"  int32_t Stack_1_get_Count_m1690191970_gshared (Stack_1_t3886787695 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__size_1();
		return L_0;
	}
}
// System.Collections.Generic.Stack`1/Enumerator<T> System.Collections.Generic.Stack`1<System.Object>::GetEnumerator()
extern "C"  Enumerator_t1041909981  Stack_1_GetEnumerator_m3405702571_gshared (Stack_1_t3886787695 * __this, const RuntimeMethod* method)
{
	{
		Enumerator_t1041909981  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Enumerator__ctor_m1549060886((&L_0), (Stack_1_t3886787695 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return L_0;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::.ctor()
extern "C"  void Collection_1__ctor_m2329675133_gshared (Collection_1_t915301837 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Collection_1__ctor_m2329675133_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t1174585319 * V_0 = NULL;
	RuntimeObject* V_1 = NULL;
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m3446193457((RuntimeObject *)__this, /*hidden argument*/NULL);
		List_1_t1174585319 * L_0 = (List_1_t1174585319 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		((  void (*) (List_1_t1174585319 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		V_0 = (List_1_t1174585319 *)L_0;
		List_1_t1174585319 * L_1 = V_0;
		V_1 = (RuntimeObject*)L_1;
		RuntimeObject* L_2 = V_1;
		NullCheck((RuntimeObject*)L_2);
		RuntimeObject * L_3 = InterfaceFuncInvoker0< RuntimeObject * >::Invoke(1 /* System.Object System.Collections.ICollection::get_SyncRoot() */, ICollection_t1881172908_il2cpp_TypeInfo_var, (RuntimeObject*)L_2);
		__this->set_syncRoot_1(L_3);
		List_1_t1174585319 * L_4 = V_0;
		__this->set_list_0(L_4);
		return;
	}
}
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m368499356_gshared (Collection_1_t915301837 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_list_0();
		NullCheck((RuntimeObject*)L_0);
		bool L_1 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.Generic.ICollection`1<System.Object>::get_IsReadOnly() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (RuntimeObject*)L_0);
		return L_1;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Collection_1_System_Collections_ICollection_CopyTo_m1677282244_gshared (Collection_1_t915301837 * __this, RuntimeArray * ___array0, int32_t ___index1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Collection_1_System_Collections_ICollection_CopyTo_m1677282244_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_list_0();
		RuntimeArray * L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((RuntimeObject*)((RuntimeObject*)Castclass((RuntimeObject*)L_0, ICollection_t1881172908_il2cpp_TypeInfo_var)));
		InterfaceActionInvoker2< RuntimeArray *, int32_t >::Invoke(2 /* System.Void System.Collections.ICollection::CopyTo(System.Array,System.Int32) */, ICollection_t1881172908_il2cpp_TypeInfo_var, (RuntimeObject*)((RuntimeObject*)Castclass((RuntimeObject*)L_0, ICollection_t1881172908_il2cpp_TypeInfo_var)), (RuntimeArray *)L_1, (int32_t)L_2);
		return;
	}
}
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  RuntimeObject* Collection_1_System_Collections_IEnumerable_GetEnumerator_m1875690981_gshared (Collection_1_t915301837 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_list_0();
		NullCheck((RuntimeObject*)L_0);
		RuntimeObject* L_1 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (RuntimeObject*)L_0);
		return L_1;
	}
}
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_Add_m335685528_gshared (Collection_1_t915301837 * __this, RuntimeObject * ___value0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_list_0();
		NullCheck((RuntimeObject*)L_0);
		int32_t L_1 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Object>::get_Count() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (RuntimeObject*)L_0);
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		RuntimeObject * L_3 = ___value0;
		RuntimeObject * L_4 = ((  RuntimeObject * (*) (RuntimeObject * /* static, unused */, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (RuntimeObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		NullCheck((Collection_1_t915301837 *)__this);
		VirtActionInvoker2< int32_t, RuntimeObject * >::Invoke(31 /* System.Void System.Collections.ObjectModel.Collection`1<System.Object>::InsertItem(System.Int32,T) */, (Collection_1_t915301837 *)__this, (int32_t)L_2, (RuntimeObject *)L_4);
		int32_t L_5 = V_0;
		return L_5;
	}
}
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.IList.Contains(System.Object)
extern "C"  bool Collection_1_System_Collections_IList_Contains_m337336929_gshared (Collection_1_t915301837 * __this, RuntimeObject * ___value0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___value0;
		bool L_1 = ((  bool (*) (RuntimeObject * /* static, unused */, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)(NULL /*static, unused*/, (RuntimeObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		RuntimeObject* L_2 = (RuntimeObject*)__this->get_list_0();
		RuntimeObject * L_3 = ___value0;
		NullCheck((RuntimeObject*)L_2);
		bool L_4 = InterfaceFuncInvoker1< bool, RuntimeObject * >::Invoke(4 /* System.Boolean System.Collections.Generic.ICollection`1<System.Object>::Contains(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (RuntimeObject*)L_2, (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))));
		return L_4;
	}

IL_001d:
	{
		return (bool)0;
	}
}
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_IndexOf_m968131552_gshared (Collection_1_t915301837 * __this, RuntimeObject * ___value0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___value0;
		bool L_1 = ((  bool (*) (RuntimeObject * /* static, unused */, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)(NULL /*static, unused*/, (RuntimeObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		RuntimeObject* L_2 = (RuntimeObject*)__this->get_list_0();
		RuntimeObject * L_3 = ___value0;
		NullCheck((RuntimeObject*)L_2);
		int32_t L_4 = InterfaceFuncInvoker1< int32_t, RuntimeObject * >::Invoke(0 /* System.Int32 System.Collections.Generic.IList`1<System.Object>::IndexOf(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 9), (RuntimeObject*)L_2, (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))));
		return L_4;
	}

IL_001d:
	{
		return (-1);
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_Insert_m3690947804_gshared (Collection_1_t915301837 * __this, int32_t ___index0, RuntimeObject * ___value1, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___index0;
		RuntimeObject * L_1 = ___value1;
		RuntimeObject * L_2 = ((  RuntimeObject * (*) (RuntimeObject * /* static, unused */, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (RuntimeObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		NullCheck((Collection_1_t915301837 *)__this);
		VirtActionInvoker2< int32_t, RuntimeObject * >::Invoke(31 /* System.Void System.Collections.ObjectModel.Collection`1<System.Object>::InsertItem(System.Int32,T) */, (Collection_1_t915301837 *)__this, (int32_t)L_0, (RuntimeObject *)L_2);
		return;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.IList.Remove(System.Object)
extern "C"  void Collection_1_System_Collections_IList_Remove_m995683070_gshared (Collection_1_t915301837 * __this, RuntimeObject * ___value0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_list_0();
		((  void (*) (RuntimeObject * /* static, unused */, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)(NULL /*static, unused*/, (RuntimeObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		RuntimeObject * L_1 = ___value0;
		RuntimeObject * L_2 = ((  RuntimeObject * (*) (RuntimeObject * /* static, unused */, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (RuntimeObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		NullCheck((Collection_1_t915301837 *)__this);
		int32_t L_3 = ((  int32_t (*) (Collection_1_t915301837 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((Collection_1_t915301837 *)__this, (RuntimeObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		V_0 = (int32_t)L_3;
		int32_t L_4 = V_0;
		NullCheck((Collection_1_t915301837 *)__this);
		VirtActionInvoker1< int32_t >::Invoke(32 /* System.Void System.Collections.ObjectModel.Collection`1<System.Object>::RemoveItem(System.Int32) */, (Collection_1_t915301837 *)__this, (int32_t)L_4);
		return;
	}
}
// System.Object System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  RuntimeObject * Collection_1_System_Collections_ICollection_get_SyncRoot_m216244742_gshared (Collection_1_t915301837 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_syncRoot_1();
		return L_0;
	}
}
// System.Object System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.IList.get_Item(System.Int32)
extern "C"  RuntimeObject * Collection_1_System_Collections_IList_get_Item_m3453525127_gshared (Collection_1_t915301837 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_list_0();
		int32_t L_1 = ___index0;
		NullCheck((RuntimeObject*)L_0);
		RuntimeObject * L_2 = InterfaceFuncInvoker1< RuntimeObject *, int32_t >::Invoke(3 /* T System.Collections.Generic.IList`1<System.Object>::get_Item(System.Int32) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 9), (RuntimeObject*)L_0, (int32_t)L_1);
		return L_2;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_set_Item_m2897544744_gshared (Collection_1_t915301837 * __this, int32_t ___index0, RuntimeObject * ___value1, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___index0;
		RuntimeObject * L_1 = ___value1;
		RuntimeObject * L_2 = ((  RuntimeObject * (*) (RuntimeObject * /* static, unused */, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (RuntimeObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		NullCheck((Collection_1_t915301837 *)__this);
		VirtActionInvoker2< int32_t, RuntimeObject * >::Invoke(33 /* System.Void System.Collections.ObjectModel.Collection`1<System.Object>::SetItem(System.Int32,T) */, (Collection_1_t915301837 *)__this, (int32_t)L_0, (RuntimeObject *)L_2);
		return;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::Add(T)
extern "C"  void Collection_1_Add_m1043259938_gshared (Collection_1_t915301837 * __this, RuntimeObject * ___item0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_list_0();
		NullCheck((RuntimeObject*)L_0);
		int32_t L_1 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Object>::get_Count() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (RuntimeObject*)L_0);
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		RuntimeObject * L_3 = ___item0;
		NullCheck((Collection_1_t915301837 *)__this);
		VirtActionInvoker2< int32_t, RuntimeObject * >::Invoke(31 /* System.Void System.Collections.ObjectModel.Collection`1<System.Object>::InsertItem(System.Int32,T) */, (Collection_1_t915301837 *)__this, (int32_t)L_2, (RuntimeObject *)L_3);
		return;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::Clear()
extern "C"  void Collection_1_Clear_m280056723_gshared (Collection_1_t915301837 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((Collection_1_t915301837 *)__this);
		VirtActionInvoker0::Invoke(30 /* System.Void System.Collections.ObjectModel.Collection`1<System.Object>::ClearItems() */, (Collection_1_t915301837 *)__this);
		return;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::ClearItems()
extern "C"  void Collection_1_ClearItems_m1836978172_gshared (Collection_1_t915301837 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_list_0();
		NullCheck((RuntimeObject*)L_0);
		InterfaceActionInvoker0::Invoke(3 /* System.Void System.Collections.Generic.ICollection`1<System.Object>::Clear() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (RuntimeObject*)L_0);
		return;
	}
}
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Object>::Contains(T)
extern "C"  bool Collection_1_Contains_m11037949_gshared (Collection_1_t915301837 * __this, RuntimeObject * ___item0, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_list_0();
		RuntimeObject * L_1 = ___item0;
		NullCheck((RuntimeObject*)L_0);
		bool L_2 = InterfaceFuncInvoker1< bool, RuntimeObject * >::Invoke(4 /* System.Boolean System.Collections.Generic.ICollection`1<System.Object>::Contains(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (RuntimeObject*)L_0, (RuntimeObject *)L_1);
		return L_2;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::CopyTo(T[],System.Int32)
extern "C"  void Collection_1_CopyTo_m1121109827_gshared (Collection_1_t915301837 * __this, ObjectU5BU5D_t4290686858* ___array0, int32_t ___index1, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_list_0();
		ObjectU5BU5D_t4290686858* L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((RuntimeObject*)L_0);
		InterfaceActionInvoker2< ObjectU5BU5D_t4290686858*, int32_t >::Invoke(5 /* System.Void System.Collections.Generic.ICollection`1<System.Object>::CopyTo(T[],System.Int32) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (RuntimeObject*)L_0, (ObjectU5BU5D_t4290686858*)L_1, (int32_t)L_2);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<System.Object>::GetEnumerator()
extern "C"  RuntimeObject* Collection_1_GetEnumerator_m2117452691_gshared (Collection_1_t915301837 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_list_0();
		NullCheck((RuntimeObject*)L_0);
		RuntimeObject* L_1 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (RuntimeObject*)L_0);
		return L_1;
	}
}
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Object>::IndexOf(T)
extern "C"  int32_t Collection_1_IndexOf_m357142369_gshared (Collection_1_t915301837 * __this, RuntimeObject * ___item0, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_list_0();
		RuntimeObject * L_1 = ___item0;
		NullCheck((RuntimeObject*)L_0);
		int32_t L_2 = InterfaceFuncInvoker1< int32_t, RuntimeObject * >::Invoke(0 /* System.Int32 System.Collections.Generic.IList`1<System.Object>::IndexOf(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 9), (RuntimeObject*)L_0, (RuntimeObject *)L_1);
		return L_2;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::Insert(System.Int32,T)
extern "C"  void Collection_1_Insert_m914854840_gshared (Collection_1_t915301837 * __this, int32_t ___index0, RuntimeObject * ___item1, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___index0;
		RuntimeObject * L_1 = ___item1;
		NullCheck((Collection_1_t915301837 *)__this);
		VirtActionInvoker2< int32_t, RuntimeObject * >::Invoke(31 /* System.Void System.Collections.ObjectModel.Collection`1<System.Object>::InsertItem(System.Int32,T) */, (Collection_1_t915301837 *)__this, (int32_t)L_0, (RuntimeObject *)L_1);
		return;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::InsertItem(System.Int32,T)
extern "C"  void Collection_1_InsertItem_m2001384755_gshared (Collection_1_t915301837 * __this, int32_t ___index0, RuntimeObject * ___item1, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_list_0();
		int32_t L_1 = ___index0;
		RuntimeObject * L_2 = ___item1;
		NullCheck((RuntimeObject*)L_0);
		InterfaceActionInvoker2< int32_t, RuntimeObject * >::Invoke(1 /* System.Void System.Collections.Generic.IList`1<System.Object>::Insert(System.Int32,T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 9), (RuntimeObject*)L_0, (int32_t)L_1, (RuntimeObject *)L_2);
		return;
	}
}
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Object>::Remove(T)
extern "C"  bool Collection_1_Remove_m2669109124_gshared (Collection_1_t915301837 * __this, RuntimeObject * ___item0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		RuntimeObject * L_0 = ___item0;
		NullCheck((Collection_1_t915301837 *)__this);
		int32_t L_1 = ((  int32_t (*) (Collection_1_t915301837 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((Collection_1_t915301837 *)__this, (RuntimeObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_0011;
		}
	}
	{
		return (bool)0;
	}

IL_0011:
	{
		int32_t L_3 = V_0;
		NullCheck((Collection_1_t915301837 *)__this);
		VirtActionInvoker1< int32_t >::Invoke(32 /* System.Void System.Collections.ObjectModel.Collection`1<System.Object>::RemoveItem(System.Int32) */, (Collection_1_t915301837 *)__this, (int32_t)L_3);
		return (bool)1;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::RemoveAt(System.Int32)
extern "C"  void Collection_1_RemoveAt_m1362554194_gshared (Collection_1_t915301837 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___index0;
		NullCheck((Collection_1_t915301837 *)__this);
		VirtActionInvoker1< int32_t >::Invoke(32 /* System.Void System.Collections.ObjectModel.Collection`1<System.Object>::RemoveItem(System.Int32) */, (Collection_1_t915301837 *)__this, (int32_t)L_0);
		return;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::RemoveItem(System.Int32)
extern "C"  void Collection_1_RemoveItem_m4208855760_gshared (Collection_1_t915301837 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_list_0();
		int32_t L_1 = ___index0;
		NullCheck((RuntimeObject*)L_0);
		InterfaceActionInvoker1< int32_t >::Invoke(2 /* System.Void System.Collections.Generic.IList`1<System.Object>::RemoveAt(System.Int32) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 9), (RuntimeObject*)L_0, (int32_t)L_1);
		return;
	}
}
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Object>::get_Count()
extern "C"  int32_t Collection_1_get_Count_m4086630925_gshared (Collection_1_t915301837 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_list_0();
		NullCheck((RuntimeObject*)L_0);
		int32_t L_1 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Object>::get_Count() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (RuntimeObject*)L_0);
		return L_1;
	}
}
// T System.Collections.ObjectModel.Collection`1<System.Object>::get_Item(System.Int32)
extern "C"  RuntimeObject * Collection_1_get_Item_m344562427_gshared (Collection_1_t915301837 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_list_0();
		int32_t L_1 = ___index0;
		NullCheck((RuntimeObject*)L_0);
		RuntimeObject * L_2 = InterfaceFuncInvoker1< RuntimeObject *, int32_t >::Invoke(3 /* T System.Collections.Generic.IList`1<System.Object>::get_Item(System.Int32) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 9), (RuntimeObject*)L_0, (int32_t)L_1);
		return L_2;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::set_Item(System.Int32,T)
extern "C"  void Collection_1_set_Item_m4073878468_gshared (Collection_1_t915301837 * __this, int32_t ___index0, RuntimeObject * ___value1, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___index0;
		RuntimeObject * L_1 = ___value1;
		NullCheck((Collection_1_t915301837 *)__this);
		VirtActionInvoker2< int32_t, RuntimeObject * >::Invoke(33 /* System.Void System.Collections.ObjectModel.Collection`1<System.Object>::SetItem(System.Int32,T) */, (Collection_1_t915301837 *)__this, (int32_t)L_0, (RuntimeObject *)L_1);
		return;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::SetItem(System.Int32,T)
extern "C"  void Collection_1_SetItem_m397905956_gshared (Collection_1_t915301837 * __this, int32_t ___index0, RuntimeObject * ___item1, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_list_0();
		int32_t L_1 = ___index0;
		RuntimeObject * L_2 = ___item1;
		NullCheck((RuntimeObject*)L_0);
		InterfaceActionInvoker2< int32_t, RuntimeObject * >::Invoke(4 /* System.Void System.Collections.Generic.IList`1<System.Object>::set_Item(System.Int32,T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 9), (RuntimeObject*)L_0, (int32_t)L_1, (RuntimeObject *)L_2);
		return;
	}
}
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Object>::IsValidItem(System.Object)
extern "C"  bool Collection_1_IsValidItem_m2716631577_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject * ___item0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Collection_1_IsValidItem_m2716631577_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B4_0 = 0;
	int32_t G_B6_0 = 0;
	{
		RuntimeObject * L_0 = ___item0;
		if (((RuntimeObject *)IsInst((RuntimeObject*)L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8))))
		{
			goto IL_0028;
		}
	}
	{
		RuntimeObject * L_1 = ___item0;
		if (L_1)
		{
			goto IL_0025;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m3171413529(NULL /*static, unused*/, (RuntimeTypeHandle_t637624852 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_2);
		bool L_3 = Type_get_IsValueType_m2191848361((Type_t *)L_2, /*hidden argument*/NULL);
		G_B4_0 = ((((int32_t)L_3) == ((int32_t)0))? 1 : 0);
		goto IL_0026;
	}

IL_0025:
	{
		G_B4_0 = 0;
	}

IL_0026:
	{
		G_B6_0 = G_B4_0;
		goto IL_0029;
	}

IL_0028:
	{
		G_B6_0 = 1;
	}

IL_0029:
	{
		return (bool)G_B6_0;
	}
}
// T System.Collections.ObjectModel.Collection`1<System.Object>::ConvertItem(System.Object)
extern "C"  RuntimeObject * Collection_1_ConvertItem_m274959791_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject * ___item0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Collection_1_ConvertItem_m274959791_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject * L_0 = ___item0;
		bool L_1 = ((  bool (*) (RuntimeObject * /* static, unused */, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->methodPointer)(NULL /*static, unused*/, (RuntimeObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		RuntimeObject * L_2 = ___item0;
		return ((RuntimeObject *)Castclass((RuntimeObject*)L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)));
	}

IL_0012:
	{
		ArgumentException_t1372035057 * L_3 = (ArgumentException_t1372035057 *)il2cpp_codegen_object_new(ArgumentException_t1372035057_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m775170528(L_3, (String_t*)_stringLiteral3684952963, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1_CheckWritable_m269185732_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* ___list0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Collection_1_CheckWritable_m269185732_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = ___list0;
		NullCheck((RuntimeObject*)L_0);
		bool L_1 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.Generic.ICollection`1<System.Object>::get_IsReadOnly() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (RuntimeObject*)L_0);
		if (!L_1)
		{
			goto IL_0011;
		}
	}
	{
		NotSupportedException_t404487136 * L_2 = (NotSupportedException_t404487136 *)il2cpp_codegen_object_new(NotSupportedException_t404487136_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2647670222(L_2, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0011:
	{
		return;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::.ctor()
extern "C"  void Collection_1__ctor_m905238948_gshared (Collection_1_t2339982469 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Collection_1__ctor_m905238948_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t2599265951 * V_0 = NULL;
	RuntimeObject* V_1 = NULL;
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m3446193457((RuntimeObject *)__this, /*hidden argument*/NULL);
		List_1_t2599265951 * L_0 = (List_1_t2599265951 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		((  void (*) (List_1_t2599265951 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		V_0 = (List_1_t2599265951 *)L_0;
		List_1_t2599265951 * L_1 = V_0;
		V_1 = (RuntimeObject*)L_1;
		RuntimeObject* L_2 = V_1;
		NullCheck((RuntimeObject*)L_2);
		RuntimeObject * L_3 = InterfaceFuncInvoker0< RuntimeObject * >::Invoke(1 /* System.Object System.Collections.ICollection::get_SyncRoot() */, ICollection_t1881172908_il2cpp_TypeInfo_var, (RuntimeObject*)L_2);
		__this->set_syncRoot_1(L_3);
		List_1_t2599265951 * L_4 = V_0;
		__this->set_list_0(L_4);
		return;
	}
}
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1604210436_gshared (Collection_1_t2339982469 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_list_0();
		NullCheck((RuntimeObject*)L_0);
		bool L_1 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeNamedArgument>::get_IsReadOnly() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (RuntimeObject*)L_0);
		return L_1;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Collection_1_System_Collections_ICollection_CopyTo_m262821365_gshared (Collection_1_t2339982469 * __this, RuntimeArray * ___array0, int32_t ___index1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Collection_1_System_Collections_ICollection_CopyTo_m262821365_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_list_0();
		RuntimeArray * L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((RuntimeObject*)((RuntimeObject*)Castclass((RuntimeObject*)L_0, ICollection_t1881172908_il2cpp_TypeInfo_var)));
		InterfaceActionInvoker2< RuntimeArray *, int32_t >::Invoke(2 /* System.Void System.Collections.ICollection::CopyTo(System.Array,System.Int32) */, ICollection_t1881172908_il2cpp_TypeInfo_var, (RuntimeObject*)((RuntimeObject*)Castclass((RuntimeObject*)L_0, ICollection_t1881172908_il2cpp_TypeInfo_var)), (RuntimeArray *)L_1, (int32_t)L_2);
		return;
	}
}
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  RuntimeObject* Collection_1_System_Collections_IEnumerable_GetEnumerator_m4286718423_gshared (Collection_1_t2339982469 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_list_0();
		NullCheck((RuntimeObject*)L_0);
		RuntimeObject* L_1 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Reflection.CustomAttributeNamedArgument>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (RuntimeObject*)L_0);
		return L_1;
	}
}
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_Add_m85363484_gshared (Collection_1_t2339982469 * __this, RuntimeObject * ___value0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_list_0();
		NullCheck((RuntimeObject*)L_0);
		int32_t L_1 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeNamedArgument>::get_Count() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (RuntimeObject*)L_0);
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		RuntimeObject * L_3 = ___value0;
		CustomAttributeNamedArgument_t425730339  L_4 = ((  CustomAttributeNamedArgument_t425730339  (*) (RuntimeObject * /* static, unused */, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (RuntimeObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		NullCheck((Collection_1_t2339982469 *)__this);
		VirtActionInvoker2< int32_t, CustomAttributeNamedArgument_t425730339  >::Invoke(31 /* System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::InsertItem(System.Int32,T) */, (Collection_1_t2339982469 *)__this, (int32_t)L_2, (CustomAttributeNamedArgument_t425730339 )L_4);
		int32_t L_5 = V_0;
		return L_5;
	}
}
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.Contains(System.Object)
extern "C"  bool Collection_1_System_Collections_IList_Contains_m977999784_gshared (Collection_1_t2339982469 * __this, RuntimeObject * ___value0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___value0;
		bool L_1 = ((  bool (*) (RuntimeObject * /* static, unused */, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)(NULL /*static, unused*/, (RuntimeObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		RuntimeObject* L_2 = (RuntimeObject*)__this->get_list_0();
		RuntimeObject * L_3 = ___value0;
		NullCheck((RuntimeObject*)L_2);
		bool L_4 = InterfaceFuncInvoker1< bool, CustomAttributeNamedArgument_t425730339  >::Invoke(4 /* System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeNamedArgument>::Contains(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (RuntimeObject*)L_2, (CustomAttributeNamedArgument_t425730339 )((*(CustomAttributeNamedArgument_t425730339 *)((CustomAttributeNamedArgument_t425730339 *)UnBox(L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))));
		return L_4;
	}

IL_001d:
	{
		return (bool)0;
	}
}
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_IndexOf_m1205829444_gshared (Collection_1_t2339982469 * __this, RuntimeObject * ___value0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___value0;
		bool L_1 = ((  bool (*) (RuntimeObject * /* static, unused */, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)(NULL /*static, unused*/, (RuntimeObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		RuntimeObject* L_2 = (RuntimeObject*)__this->get_list_0();
		RuntimeObject * L_3 = ___value0;
		NullCheck((RuntimeObject*)L_2);
		int32_t L_4 = InterfaceFuncInvoker1< int32_t, CustomAttributeNamedArgument_t425730339  >::Invoke(0 /* System.Int32 System.Collections.Generic.IList`1<System.Reflection.CustomAttributeNamedArgument>::IndexOf(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 9), (RuntimeObject*)L_2, (CustomAttributeNamedArgument_t425730339 )((*(CustomAttributeNamedArgument_t425730339 *)((CustomAttributeNamedArgument_t425730339 *)UnBox(L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))));
		return L_4;
	}

IL_001d:
	{
		return (-1);
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_Insert_m4193667472_gshared (Collection_1_t2339982469 * __this, int32_t ___index0, RuntimeObject * ___value1, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___index0;
		RuntimeObject * L_1 = ___value1;
		CustomAttributeNamedArgument_t425730339  L_2 = ((  CustomAttributeNamedArgument_t425730339  (*) (RuntimeObject * /* static, unused */, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (RuntimeObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		NullCheck((Collection_1_t2339982469 *)__this);
		VirtActionInvoker2< int32_t, CustomAttributeNamedArgument_t425730339  >::Invoke(31 /* System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::InsertItem(System.Int32,T) */, (Collection_1_t2339982469 *)__this, (int32_t)L_0, (CustomAttributeNamedArgument_t425730339 )L_2);
		return;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.Remove(System.Object)
extern "C"  void Collection_1_System_Collections_IList_Remove_m3588390596_gshared (Collection_1_t2339982469 * __this, RuntimeObject * ___value0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_list_0();
		((  void (*) (RuntimeObject * /* static, unused */, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)(NULL /*static, unused*/, (RuntimeObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		RuntimeObject * L_1 = ___value0;
		CustomAttributeNamedArgument_t425730339  L_2 = ((  CustomAttributeNamedArgument_t425730339  (*) (RuntimeObject * /* static, unused */, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (RuntimeObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		NullCheck((Collection_1_t2339982469 *)__this);
		int32_t L_3 = ((  int32_t (*) (Collection_1_t2339982469 *, CustomAttributeNamedArgument_t425730339 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((Collection_1_t2339982469 *)__this, (CustomAttributeNamedArgument_t425730339 )L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		V_0 = (int32_t)L_3;
		int32_t L_4 = V_0;
		NullCheck((Collection_1_t2339982469 *)__this);
		VirtActionInvoker1< int32_t >::Invoke(32 /* System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::RemoveItem(System.Int32) */, (Collection_1_t2339982469 *)__this, (int32_t)L_4);
		return;
	}
}
// System.Object System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.ICollection.get_SyncRoot()
extern "C"  RuntimeObject * Collection_1_System_Collections_ICollection_get_SyncRoot_m206388838_gshared (Collection_1_t2339982469 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_syncRoot_1();
		return L_0;
	}
}
// System.Object System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.get_Item(System.Int32)
extern "C"  RuntimeObject * Collection_1_System_Collections_IList_get_Item_m443959703_gshared (Collection_1_t2339982469 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_list_0();
		int32_t L_1 = ___index0;
		NullCheck((RuntimeObject*)L_0);
		CustomAttributeNamedArgument_t425730339  L_2 = InterfaceFuncInvoker1< CustomAttributeNamedArgument_t425730339 , int32_t >::Invoke(3 /* T System.Collections.Generic.IList`1<System.Reflection.CustomAttributeNamedArgument>::get_Item(System.Int32) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 9), (RuntimeObject*)L_0, (int32_t)L_1);
		CustomAttributeNamedArgument_t425730339  L_3 = L_2;
		RuntimeObject * L_4 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8), &L_3);
		return L_4;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_set_Item_m3363045019_gshared (Collection_1_t2339982469 * __this, int32_t ___index0, RuntimeObject * ___value1, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___index0;
		RuntimeObject * L_1 = ___value1;
		CustomAttributeNamedArgument_t425730339  L_2 = ((  CustomAttributeNamedArgument_t425730339  (*) (RuntimeObject * /* static, unused */, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (RuntimeObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		NullCheck((Collection_1_t2339982469 *)__this);
		VirtActionInvoker2< int32_t, CustomAttributeNamedArgument_t425730339  >::Invoke(33 /* System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::SetItem(System.Int32,T) */, (Collection_1_t2339982469 *)__this, (int32_t)L_0, (CustomAttributeNamedArgument_t425730339 )L_2);
		return;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::Add(T)
extern "C"  void Collection_1_Add_m837866340_gshared (Collection_1_t2339982469 * __this, CustomAttributeNamedArgument_t425730339  ___item0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_list_0();
		NullCheck((RuntimeObject*)L_0);
		int32_t L_1 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeNamedArgument>::get_Count() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (RuntimeObject*)L_0);
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		CustomAttributeNamedArgument_t425730339  L_3 = ___item0;
		NullCheck((Collection_1_t2339982469 *)__this);
		VirtActionInvoker2< int32_t, CustomAttributeNamedArgument_t425730339  >::Invoke(31 /* System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::InsertItem(System.Int32,T) */, (Collection_1_t2339982469 *)__this, (int32_t)L_2, (CustomAttributeNamedArgument_t425730339 )L_3);
		return;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::Clear()
extern "C"  void Collection_1_Clear_m4028058305_gshared (Collection_1_t2339982469 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((Collection_1_t2339982469 *)__this);
		VirtActionInvoker0::Invoke(30 /* System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::ClearItems() */, (Collection_1_t2339982469 *)__this);
		return;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::ClearItems()
extern "C"  void Collection_1_ClearItems_m2298354737_gshared (Collection_1_t2339982469 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_list_0();
		NullCheck((RuntimeObject*)L_0);
		InterfaceActionInvoker0::Invoke(3 /* System.Void System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeNamedArgument>::Clear() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (RuntimeObject*)L_0);
		return;
	}
}
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::Contains(T)
extern "C"  bool Collection_1_Contains_m759904946_gshared (Collection_1_t2339982469 * __this, CustomAttributeNamedArgument_t425730339  ___item0, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_list_0();
		CustomAttributeNamedArgument_t425730339  L_1 = ___item0;
		NullCheck((RuntimeObject*)L_0);
		bool L_2 = InterfaceFuncInvoker1< bool, CustomAttributeNamedArgument_t425730339  >::Invoke(4 /* System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeNamedArgument>::Contains(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (RuntimeObject*)L_0, (CustomAttributeNamedArgument_t425730339 )L_1);
		return L_2;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::CopyTo(T[],System.Int32)
extern "C"  void Collection_1_CopyTo_m44258151_gshared (Collection_1_t2339982469 * __this, CustomAttributeNamedArgumentU5BU5D_t3127017202* ___array0, int32_t ___index1, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_list_0();
		CustomAttributeNamedArgumentU5BU5D_t3127017202* L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((RuntimeObject*)L_0);
		InterfaceActionInvoker2< CustomAttributeNamedArgumentU5BU5D_t3127017202*, int32_t >::Invoke(5 /* System.Void System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeNamedArgument>::CopyTo(T[],System.Int32) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (RuntimeObject*)L_0, (CustomAttributeNamedArgumentU5BU5D_t3127017202*)L_1, (int32_t)L_2);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::GetEnumerator()
extern "C"  RuntimeObject* Collection_1_GetEnumerator_m2878335679_gshared (Collection_1_t2339982469 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_list_0();
		NullCheck((RuntimeObject*)L_0);
		RuntimeObject* L_1 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Reflection.CustomAttributeNamedArgument>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (RuntimeObject*)L_0);
		return L_1;
	}
}
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::IndexOf(T)
extern "C"  int32_t Collection_1_IndexOf_m2232125673_gshared (Collection_1_t2339982469 * __this, CustomAttributeNamedArgument_t425730339  ___item0, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_list_0();
		CustomAttributeNamedArgument_t425730339  L_1 = ___item0;
		NullCheck((RuntimeObject*)L_0);
		int32_t L_2 = InterfaceFuncInvoker1< int32_t, CustomAttributeNamedArgument_t425730339  >::Invoke(0 /* System.Int32 System.Collections.Generic.IList`1<System.Reflection.CustomAttributeNamedArgument>::IndexOf(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 9), (RuntimeObject*)L_0, (CustomAttributeNamedArgument_t425730339 )L_1);
		return L_2;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::Insert(System.Int32,T)
extern "C"  void Collection_1_Insert_m398820411_gshared (Collection_1_t2339982469 * __this, int32_t ___index0, CustomAttributeNamedArgument_t425730339  ___item1, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___index0;
		CustomAttributeNamedArgument_t425730339  L_1 = ___item1;
		NullCheck((Collection_1_t2339982469 *)__this);
		VirtActionInvoker2< int32_t, CustomAttributeNamedArgument_t425730339  >::Invoke(31 /* System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::InsertItem(System.Int32,T) */, (Collection_1_t2339982469 *)__this, (int32_t)L_0, (CustomAttributeNamedArgument_t425730339 )L_1);
		return;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::InsertItem(System.Int32,T)
extern "C"  void Collection_1_InsertItem_m249866385_gshared (Collection_1_t2339982469 * __this, int32_t ___index0, CustomAttributeNamedArgument_t425730339  ___item1, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_list_0();
		int32_t L_1 = ___index0;
		CustomAttributeNamedArgument_t425730339  L_2 = ___item1;
		NullCheck((RuntimeObject*)L_0);
		InterfaceActionInvoker2< int32_t, CustomAttributeNamedArgument_t425730339  >::Invoke(1 /* System.Void System.Collections.Generic.IList`1<System.Reflection.CustomAttributeNamedArgument>::Insert(System.Int32,T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 9), (RuntimeObject*)L_0, (int32_t)L_1, (CustomAttributeNamedArgument_t425730339 )L_2);
		return;
	}
}
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::Remove(T)
extern "C"  bool Collection_1_Remove_m163140352_gshared (Collection_1_t2339982469 * __this, CustomAttributeNamedArgument_t425730339  ___item0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		CustomAttributeNamedArgument_t425730339  L_0 = ___item0;
		NullCheck((Collection_1_t2339982469 *)__this);
		int32_t L_1 = ((  int32_t (*) (Collection_1_t2339982469 *, CustomAttributeNamedArgument_t425730339 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((Collection_1_t2339982469 *)__this, (CustomAttributeNamedArgument_t425730339 )L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_0011;
		}
	}
	{
		return (bool)0;
	}

IL_0011:
	{
		int32_t L_3 = V_0;
		NullCheck((Collection_1_t2339982469 *)__this);
		VirtActionInvoker1< int32_t >::Invoke(32 /* System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::RemoveItem(System.Int32) */, (Collection_1_t2339982469 *)__this, (int32_t)L_3);
		return (bool)1;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::RemoveAt(System.Int32)
extern "C"  void Collection_1_RemoveAt_m1159245362_gshared (Collection_1_t2339982469 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___index0;
		NullCheck((Collection_1_t2339982469 *)__this);
		VirtActionInvoker1< int32_t >::Invoke(32 /* System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::RemoveItem(System.Int32) */, (Collection_1_t2339982469 *)__this, (int32_t)L_0);
		return;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::RemoveItem(System.Int32)
extern "C"  void Collection_1_RemoveItem_m1966842543_gshared (Collection_1_t2339982469 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_list_0();
		int32_t L_1 = ___index0;
		NullCheck((RuntimeObject*)L_0);
		InterfaceActionInvoker1< int32_t >::Invoke(2 /* System.Void System.Collections.Generic.IList`1<System.Reflection.CustomAttributeNamedArgument>::RemoveAt(System.Int32) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 9), (RuntimeObject*)L_0, (int32_t)L_1);
		return;
	}
}
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::get_Count()
extern "C"  int32_t Collection_1_get_Count_m3543401576_gshared (Collection_1_t2339982469 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_list_0();
		NullCheck((RuntimeObject*)L_0);
		int32_t L_1 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeNamedArgument>::get_Count() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (RuntimeObject*)L_0);
		return L_1;
	}
}
// T System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::get_Item(System.Int32)
extern "C"  CustomAttributeNamedArgument_t425730339  Collection_1_get_Item_m812058820_gshared (Collection_1_t2339982469 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_list_0();
		int32_t L_1 = ___index0;
		NullCheck((RuntimeObject*)L_0);
		CustomAttributeNamedArgument_t425730339  L_2 = InterfaceFuncInvoker1< CustomAttributeNamedArgument_t425730339 , int32_t >::Invoke(3 /* T System.Collections.Generic.IList`1<System.Reflection.CustomAttributeNamedArgument>::get_Item(System.Int32) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 9), (RuntimeObject*)L_0, (int32_t)L_1);
		return L_2;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::set_Item(System.Int32,T)
extern "C"  void Collection_1_set_Item_m3622626775_gshared (Collection_1_t2339982469 * __this, int32_t ___index0, CustomAttributeNamedArgument_t425730339  ___value1, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___index0;
		CustomAttributeNamedArgument_t425730339  L_1 = ___value1;
		NullCheck((Collection_1_t2339982469 *)__this);
		VirtActionInvoker2< int32_t, CustomAttributeNamedArgument_t425730339  >::Invoke(33 /* System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::SetItem(System.Int32,T) */, (Collection_1_t2339982469 *)__this, (int32_t)L_0, (CustomAttributeNamedArgument_t425730339 )L_1);
		return;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::SetItem(System.Int32,T)
extern "C"  void Collection_1_SetItem_m2682542669_gshared (Collection_1_t2339982469 * __this, int32_t ___index0, CustomAttributeNamedArgument_t425730339  ___item1, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_list_0();
		int32_t L_1 = ___index0;
		CustomAttributeNamedArgument_t425730339  L_2 = ___item1;
		NullCheck((RuntimeObject*)L_0);
		InterfaceActionInvoker2< int32_t, CustomAttributeNamedArgument_t425730339  >::Invoke(4 /* System.Void System.Collections.Generic.IList`1<System.Reflection.CustomAttributeNamedArgument>::set_Item(System.Int32,T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 9), (RuntimeObject*)L_0, (int32_t)L_1, (CustomAttributeNamedArgument_t425730339 )L_2);
		return;
	}
}
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::IsValidItem(System.Object)
extern "C"  bool Collection_1_IsValidItem_m1823302628_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject * ___item0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Collection_1_IsValidItem_m1823302628_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B4_0 = 0;
	int32_t G_B6_0 = 0;
	{
		RuntimeObject * L_0 = ___item0;
		if (((RuntimeObject *)IsInst((RuntimeObject*)L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8))))
		{
			goto IL_0028;
		}
	}
	{
		RuntimeObject * L_1 = ___item0;
		if (L_1)
		{
			goto IL_0025;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m3171413529(NULL /*static, unused*/, (RuntimeTypeHandle_t637624852 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_2);
		bool L_3 = Type_get_IsValueType_m2191848361((Type_t *)L_2, /*hidden argument*/NULL);
		G_B4_0 = ((((int32_t)L_3) == ((int32_t)0))? 1 : 0);
		goto IL_0026;
	}

IL_0025:
	{
		G_B4_0 = 0;
	}

IL_0026:
	{
		G_B6_0 = G_B4_0;
		goto IL_0029;
	}

IL_0028:
	{
		G_B6_0 = 1;
	}

IL_0029:
	{
		return (bool)G_B6_0;
	}
}
// T System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::ConvertItem(System.Object)
extern "C"  CustomAttributeNamedArgument_t425730339  Collection_1_ConvertItem_m1597384526_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject * ___item0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Collection_1_ConvertItem_m1597384526_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject * L_0 = ___item0;
		bool L_1 = ((  bool (*) (RuntimeObject * /* static, unused */, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->methodPointer)(NULL /*static, unused*/, (RuntimeObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		RuntimeObject * L_2 = ___item0;
		return ((*(CustomAttributeNamedArgument_t425730339 *)((CustomAttributeNamedArgument_t425730339 *)UnBox(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)))));
	}

IL_0012:
	{
		ArgumentException_t1372035057 * L_3 = (ArgumentException_t1372035057 *)il2cpp_codegen_object_new(ArgumentException_t1372035057_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m775170528(L_3, (String_t*)_stringLiteral3684952963, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1_CheckWritable_m1149777024_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* ___list0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Collection_1_CheckWritable_m1149777024_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = ___list0;
		NullCheck((RuntimeObject*)L_0);
		bool L_1 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeNamedArgument>::get_IsReadOnly() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (RuntimeObject*)L_0);
		if (!L_1)
		{
			goto IL_0011;
		}
	}
	{
		NotSupportedException_t404487136 * L_2 = (NotSupportedException_t404487136 *)il2cpp_codegen_object_new(NotSupportedException_t404487136_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2647670222(L_2, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0011:
	{
		return;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::.ctor()
extern "C"  void Collection_1__ctor_m249788010_gshared (Collection_1_t286987555 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Collection_1__ctor_m249788010_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t546271037 * V_0 = NULL;
	RuntimeObject* V_1 = NULL;
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m3446193457((RuntimeObject *)__this, /*hidden argument*/NULL);
		List_1_t546271037 * L_0 = (List_1_t546271037 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		((  void (*) (List_1_t546271037 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		V_0 = (List_1_t546271037 *)L_0;
		List_1_t546271037 * L_1 = V_0;
		V_1 = (RuntimeObject*)L_1;
		RuntimeObject* L_2 = V_1;
		NullCheck((RuntimeObject*)L_2);
		RuntimeObject * L_3 = InterfaceFuncInvoker0< RuntimeObject * >::Invoke(1 /* System.Object System.Collections.ICollection::get_SyncRoot() */, ICollection_t1881172908_il2cpp_TypeInfo_var, (RuntimeObject*)L_2);
		__this->set_syncRoot_1(L_3);
		List_1_t546271037 * L_4 = V_0;
		__this->set_list_0(L_4);
		return;
	}
}
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m677017886_gshared (Collection_1_t286987555 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_list_0();
		NullCheck((RuntimeObject*)L_0);
		bool L_1 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeTypedArgument>::get_IsReadOnly() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (RuntimeObject*)L_0);
		return L_1;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Collection_1_System_Collections_ICollection_CopyTo_m3489689141_gshared (Collection_1_t286987555 * __this, RuntimeArray * ___array0, int32_t ___index1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Collection_1_System_Collections_ICollection_CopyTo_m3489689141_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_list_0();
		RuntimeArray * L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((RuntimeObject*)((RuntimeObject*)Castclass((RuntimeObject*)L_0, ICollection_t1881172908_il2cpp_TypeInfo_var)));
		InterfaceActionInvoker2< RuntimeArray *, int32_t >::Invoke(2 /* System.Void System.Collections.ICollection::CopyTo(System.Array,System.Int32) */, ICollection_t1881172908_il2cpp_TypeInfo_var, (RuntimeObject*)((RuntimeObject*)Castclass((RuntimeObject*)L_0, ICollection_t1881172908_il2cpp_TypeInfo_var)), (RuntimeArray *)L_1, (int32_t)L_2);
		return;
	}
}
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  RuntimeObject* Collection_1_System_Collections_IEnumerable_GetEnumerator_m4163124676_gshared (Collection_1_t286987555 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_list_0();
		NullCheck((RuntimeObject*)L_0);
		RuntimeObject* L_1 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Reflection.CustomAttributeTypedArgument>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (RuntimeObject*)L_0);
		return L_1;
	}
}
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_Add_m1414007204_gshared (Collection_1_t286987555 * __this, RuntimeObject * ___value0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_list_0();
		NullCheck((RuntimeObject*)L_0);
		int32_t L_1 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeTypedArgument>::get_Count() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (RuntimeObject*)L_0);
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		RuntimeObject * L_3 = ___value0;
		CustomAttributeTypedArgument_t2667702721  L_4 = ((  CustomAttributeTypedArgument_t2667702721  (*) (RuntimeObject * /* static, unused */, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (RuntimeObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		NullCheck((Collection_1_t286987555 *)__this);
		VirtActionInvoker2< int32_t, CustomAttributeTypedArgument_t2667702721  >::Invoke(31 /* System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::InsertItem(System.Int32,T) */, (Collection_1_t286987555 *)__this, (int32_t)L_2, (CustomAttributeTypedArgument_t2667702721 )L_4);
		int32_t L_5 = V_0;
		return L_5;
	}
}
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.Contains(System.Object)
extern "C"  bool Collection_1_System_Collections_IList_Contains_m3814732167_gshared (Collection_1_t286987555 * __this, RuntimeObject * ___value0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___value0;
		bool L_1 = ((  bool (*) (RuntimeObject * /* static, unused */, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)(NULL /*static, unused*/, (RuntimeObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		RuntimeObject* L_2 = (RuntimeObject*)__this->get_list_0();
		RuntimeObject * L_3 = ___value0;
		NullCheck((RuntimeObject*)L_2);
		bool L_4 = InterfaceFuncInvoker1< bool, CustomAttributeTypedArgument_t2667702721  >::Invoke(4 /* System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeTypedArgument>::Contains(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (RuntimeObject*)L_2, (CustomAttributeTypedArgument_t2667702721 )((*(CustomAttributeTypedArgument_t2667702721 *)((CustomAttributeTypedArgument_t2667702721 *)UnBox(L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))));
		return L_4;
	}

IL_001d:
	{
		return (bool)0;
	}
}
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_IndexOf_m3908813626_gshared (Collection_1_t286987555 * __this, RuntimeObject * ___value0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___value0;
		bool L_1 = ((  bool (*) (RuntimeObject * /* static, unused */, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)(NULL /*static, unused*/, (RuntimeObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		RuntimeObject* L_2 = (RuntimeObject*)__this->get_list_0();
		RuntimeObject * L_3 = ___value0;
		NullCheck((RuntimeObject*)L_2);
		int32_t L_4 = InterfaceFuncInvoker1< int32_t, CustomAttributeTypedArgument_t2667702721  >::Invoke(0 /* System.Int32 System.Collections.Generic.IList`1<System.Reflection.CustomAttributeTypedArgument>::IndexOf(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 9), (RuntimeObject*)L_2, (CustomAttributeTypedArgument_t2667702721 )((*(CustomAttributeTypedArgument_t2667702721 *)((CustomAttributeTypedArgument_t2667702721 *)UnBox(L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))));
		return L_4;
	}

IL_001d:
	{
		return (-1);
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_Insert_m706084411_gshared (Collection_1_t286987555 * __this, int32_t ___index0, RuntimeObject * ___value1, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___index0;
		RuntimeObject * L_1 = ___value1;
		CustomAttributeTypedArgument_t2667702721  L_2 = ((  CustomAttributeTypedArgument_t2667702721  (*) (RuntimeObject * /* static, unused */, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (RuntimeObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		NullCheck((Collection_1_t286987555 *)__this);
		VirtActionInvoker2< int32_t, CustomAttributeTypedArgument_t2667702721  >::Invoke(31 /* System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::InsertItem(System.Int32,T) */, (Collection_1_t286987555 *)__this, (int32_t)L_0, (CustomAttributeTypedArgument_t2667702721 )L_2);
		return;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.Remove(System.Object)
extern "C"  void Collection_1_System_Collections_IList_Remove_m3493313931_gshared (Collection_1_t286987555 * __this, RuntimeObject * ___value0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_list_0();
		((  void (*) (RuntimeObject * /* static, unused */, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)(NULL /*static, unused*/, (RuntimeObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		RuntimeObject * L_1 = ___value0;
		CustomAttributeTypedArgument_t2667702721  L_2 = ((  CustomAttributeTypedArgument_t2667702721  (*) (RuntimeObject * /* static, unused */, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (RuntimeObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		NullCheck((Collection_1_t286987555 *)__this);
		int32_t L_3 = ((  int32_t (*) (Collection_1_t286987555 *, CustomAttributeTypedArgument_t2667702721 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((Collection_1_t286987555 *)__this, (CustomAttributeTypedArgument_t2667702721 )L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		V_0 = (int32_t)L_3;
		int32_t L_4 = V_0;
		NullCheck((Collection_1_t286987555 *)__this);
		VirtActionInvoker1< int32_t >::Invoke(32 /* System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::RemoveItem(System.Int32) */, (Collection_1_t286987555 *)__this, (int32_t)L_4);
		return;
	}
}
// System.Object System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.ICollection.get_SyncRoot()
extern "C"  RuntimeObject * Collection_1_System_Collections_ICollection_get_SyncRoot_m2295883198_gshared (Collection_1_t286987555 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_syncRoot_1();
		return L_0;
	}
}
// System.Object System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.get_Item(System.Int32)
extern "C"  RuntimeObject * Collection_1_System_Collections_IList_get_Item_m1690475062_gshared (Collection_1_t286987555 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_list_0();
		int32_t L_1 = ___index0;
		NullCheck((RuntimeObject*)L_0);
		CustomAttributeTypedArgument_t2667702721  L_2 = InterfaceFuncInvoker1< CustomAttributeTypedArgument_t2667702721 , int32_t >::Invoke(3 /* T System.Collections.Generic.IList`1<System.Reflection.CustomAttributeTypedArgument>::get_Item(System.Int32) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 9), (RuntimeObject*)L_0, (int32_t)L_1);
		CustomAttributeTypedArgument_t2667702721  L_3 = L_2;
		RuntimeObject * L_4 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8), &L_3);
		return L_4;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_set_Item_m236195538_gshared (Collection_1_t286987555 * __this, int32_t ___index0, RuntimeObject * ___value1, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___index0;
		RuntimeObject * L_1 = ___value1;
		CustomAttributeTypedArgument_t2667702721  L_2 = ((  CustomAttributeTypedArgument_t2667702721  (*) (RuntimeObject * /* static, unused */, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (RuntimeObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		NullCheck((Collection_1_t286987555 *)__this);
		VirtActionInvoker2< int32_t, CustomAttributeTypedArgument_t2667702721  >::Invoke(33 /* System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::SetItem(System.Int32,T) */, (Collection_1_t286987555 *)__this, (int32_t)L_0, (CustomAttributeTypedArgument_t2667702721 )L_2);
		return;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::Add(T)
extern "C"  void Collection_1_Add_m214414947_gshared (Collection_1_t286987555 * __this, CustomAttributeTypedArgument_t2667702721  ___item0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_list_0();
		NullCheck((RuntimeObject*)L_0);
		int32_t L_1 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeTypedArgument>::get_Count() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (RuntimeObject*)L_0);
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		CustomAttributeTypedArgument_t2667702721  L_3 = ___item0;
		NullCheck((Collection_1_t286987555 *)__this);
		VirtActionInvoker2< int32_t, CustomAttributeTypedArgument_t2667702721  >::Invoke(31 /* System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::InsertItem(System.Int32,T) */, (Collection_1_t286987555 *)__this, (int32_t)L_2, (CustomAttributeTypedArgument_t2667702721 )L_3);
		return;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::Clear()
extern "C"  void Collection_1_Clear_m1245138857_gshared (Collection_1_t286987555 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((Collection_1_t286987555 *)__this);
		VirtActionInvoker0::Invoke(30 /* System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::ClearItems() */, (Collection_1_t286987555 *)__this);
		return;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::ClearItems()
extern "C"  void Collection_1_ClearItems_m3930770573_gshared (Collection_1_t286987555 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_list_0();
		NullCheck((RuntimeObject*)L_0);
		InterfaceActionInvoker0::Invoke(3 /* System.Void System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeTypedArgument>::Clear() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (RuntimeObject*)L_0);
		return;
	}
}
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::Contains(T)
extern "C"  bool Collection_1_Contains_m2424492765_gshared (Collection_1_t286987555 * __this, CustomAttributeTypedArgument_t2667702721  ___item0, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_list_0();
		CustomAttributeTypedArgument_t2667702721  L_1 = ___item0;
		NullCheck((RuntimeObject*)L_0);
		bool L_2 = InterfaceFuncInvoker1< bool, CustomAttributeTypedArgument_t2667702721  >::Invoke(4 /* System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeTypedArgument>::Contains(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (RuntimeObject*)L_0, (CustomAttributeTypedArgument_t2667702721 )L_1);
		return L_2;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::CopyTo(T[],System.Int32)
extern "C"  void Collection_1_CopyTo_m3191658765_gshared (Collection_1_t286987555 * __this, CustomAttributeTypedArgumentU5BU5D_t2668173852* ___array0, int32_t ___index1, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_list_0();
		CustomAttributeTypedArgumentU5BU5D_t2668173852* L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((RuntimeObject*)L_0);
		InterfaceActionInvoker2< CustomAttributeTypedArgumentU5BU5D_t2668173852*, int32_t >::Invoke(5 /* System.Void System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeTypedArgument>::CopyTo(T[],System.Int32) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (RuntimeObject*)L_0, (CustomAttributeTypedArgumentU5BU5D_t2668173852*)L_1, (int32_t)L_2);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::GetEnumerator()
extern "C"  RuntimeObject* Collection_1_GetEnumerator_m185110896_gshared (Collection_1_t286987555 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_list_0();
		NullCheck((RuntimeObject*)L_0);
		RuntimeObject* L_1 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Reflection.CustomAttributeTypedArgument>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (RuntimeObject*)L_0);
		return L_1;
	}
}
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::IndexOf(T)
extern "C"  int32_t Collection_1_IndexOf_m97241429_gshared (Collection_1_t286987555 * __this, CustomAttributeTypedArgument_t2667702721  ___item0, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_list_0();
		CustomAttributeTypedArgument_t2667702721  L_1 = ___item0;
		NullCheck((RuntimeObject*)L_0);
		int32_t L_2 = InterfaceFuncInvoker1< int32_t, CustomAttributeTypedArgument_t2667702721  >::Invoke(0 /* System.Int32 System.Collections.Generic.IList`1<System.Reflection.CustomAttributeTypedArgument>::IndexOf(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 9), (RuntimeObject*)L_0, (CustomAttributeTypedArgument_t2667702721 )L_1);
		return L_2;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::Insert(System.Int32,T)
extern "C"  void Collection_1_Insert_m3929619862_gshared (Collection_1_t286987555 * __this, int32_t ___index0, CustomAttributeTypedArgument_t2667702721  ___item1, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___index0;
		CustomAttributeTypedArgument_t2667702721  L_1 = ___item1;
		NullCheck((Collection_1_t286987555 *)__this);
		VirtActionInvoker2< int32_t, CustomAttributeTypedArgument_t2667702721  >::Invoke(31 /* System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::InsertItem(System.Int32,T) */, (Collection_1_t286987555 *)__this, (int32_t)L_0, (CustomAttributeTypedArgument_t2667702721 )L_1);
		return;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::InsertItem(System.Int32,T)
extern "C"  void Collection_1_InsertItem_m307180003_gshared (Collection_1_t286987555 * __this, int32_t ___index0, CustomAttributeTypedArgument_t2667702721  ___item1, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_list_0();
		int32_t L_1 = ___index0;
		CustomAttributeTypedArgument_t2667702721  L_2 = ___item1;
		NullCheck((RuntimeObject*)L_0);
		InterfaceActionInvoker2< int32_t, CustomAttributeTypedArgument_t2667702721  >::Invoke(1 /* System.Void System.Collections.Generic.IList`1<System.Reflection.CustomAttributeTypedArgument>::Insert(System.Int32,T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 9), (RuntimeObject*)L_0, (int32_t)L_1, (CustomAttributeTypedArgument_t2667702721 )L_2);
		return;
	}
}
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::Remove(T)
extern "C"  bool Collection_1_Remove_m835318213_gshared (Collection_1_t286987555 * __this, CustomAttributeTypedArgument_t2667702721  ___item0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		CustomAttributeTypedArgument_t2667702721  L_0 = ___item0;
		NullCheck((Collection_1_t286987555 *)__this);
		int32_t L_1 = ((  int32_t (*) (Collection_1_t286987555 *, CustomAttributeTypedArgument_t2667702721 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((Collection_1_t286987555 *)__this, (CustomAttributeTypedArgument_t2667702721 )L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_0011;
		}
	}
	{
		return (bool)0;
	}

IL_0011:
	{
		int32_t L_3 = V_0;
		NullCheck((Collection_1_t286987555 *)__this);
		VirtActionInvoker1< int32_t >::Invoke(32 /* System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::RemoveItem(System.Int32) */, (Collection_1_t286987555 *)__this, (int32_t)L_3);
		return (bool)1;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::RemoveAt(System.Int32)
extern "C"  void Collection_1_RemoveAt_m99483852_gshared (Collection_1_t286987555 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___index0;
		NullCheck((Collection_1_t286987555 *)__this);
		VirtActionInvoker1< int32_t >::Invoke(32 /* System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::RemoveItem(System.Int32) */, (Collection_1_t286987555 *)__this, (int32_t)L_0);
		return;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::RemoveItem(System.Int32)
extern "C"  void Collection_1_RemoveItem_m4291645041_gshared (Collection_1_t286987555 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_list_0();
		int32_t L_1 = ___index0;
		NullCheck((RuntimeObject*)L_0);
		InterfaceActionInvoker1< int32_t >::Invoke(2 /* System.Void System.Collections.Generic.IList`1<System.Reflection.CustomAttributeTypedArgument>::RemoveAt(System.Int32) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 9), (RuntimeObject*)L_0, (int32_t)L_1);
		return;
	}
}
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::get_Count()
extern "C"  int32_t Collection_1_get_Count_m3051484433_gshared (Collection_1_t286987555 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_list_0();
		NullCheck((RuntimeObject*)L_0);
		int32_t L_1 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeTypedArgument>::get_Count() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (RuntimeObject*)L_0);
		return L_1;
	}
}
// T System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::get_Item(System.Int32)
extern "C"  CustomAttributeTypedArgument_t2667702721  Collection_1_get_Item_m89393537_gshared (Collection_1_t286987555 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_list_0();
		int32_t L_1 = ___index0;
		NullCheck((RuntimeObject*)L_0);
		CustomAttributeTypedArgument_t2667702721  L_2 = InterfaceFuncInvoker1< CustomAttributeTypedArgument_t2667702721 , int32_t >::Invoke(3 /* T System.Collections.Generic.IList`1<System.Reflection.CustomAttributeTypedArgument>::get_Item(System.Int32) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 9), (RuntimeObject*)L_0, (int32_t)L_1);
		return L_2;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::set_Item(System.Int32,T)
extern "C"  void Collection_1_set_Item_m2088067960_gshared (Collection_1_t286987555 * __this, int32_t ___index0, CustomAttributeTypedArgument_t2667702721  ___value1, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___index0;
		CustomAttributeTypedArgument_t2667702721  L_1 = ___value1;
		NullCheck((Collection_1_t286987555 *)__this);
		VirtActionInvoker2< int32_t, CustomAttributeTypedArgument_t2667702721  >::Invoke(33 /* System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::SetItem(System.Int32,T) */, (Collection_1_t286987555 *)__this, (int32_t)L_0, (CustomAttributeTypedArgument_t2667702721 )L_1);
		return;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::SetItem(System.Int32,T)
extern "C"  void Collection_1_SetItem_m3561579506_gshared (Collection_1_t286987555 * __this, int32_t ___index0, CustomAttributeTypedArgument_t2667702721  ___item1, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_list_0();
		int32_t L_1 = ___index0;
		CustomAttributeTypedArgument_t2667702721  L_2 = ___item1;
		NullCheck((RuntimeObject*)L_0);
		InterfaceActionInvoker2< int32_t, CustomAttributeTypedArgument_t2667702721  >::Invoke(4 /* System.Void System.Collections.Generic.IList`1<System.Reflection.CustomAttributeTypedArgument>::set_Item(System.Int32,T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 9), (RuntimeObject*)L_0, (int32_t)L_1, (CustomAttributeTypedArgument_t2667702721 )L_2);
		return;
	}
}
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::IsValidItem(System.Object)
extern "C"  bool Collection_1_IsValidItem_m2435816978_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject * ___item0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Collection_1_IsValidItem_m2435816978_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B4_0 = 0;
	int32_t G_B6_0 = 0;
	{
		RuntimeObject * L_0 = ___item0;
		if (((RuntimeObject *)IsInst((RuntimeObject*)L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8))))
		{
			goto IL_0028;
		}
	}
	{
		RuntimeObject * L_1 = ___item0;
		if (L_1)
		{
			goto IL_0025;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m3171413529(NULL /*static, unused*/, (RuntimeTypeHandle_t637624852 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_2);
		bool L_3 = Type_get_IsValueType_m2191848361((Type_t *)L_2, /*hidden argument*/NULL);
		G_B4_0 = ((((int32_t)L_3) == ((int32_t)0))? 1 : 0);
		goto IL_0026;
	}

IL_0025:
	{
		G_B4_0 = 0;
	}

IL_0026:
	{
		G_B6_0 = G_B4_0;
		goto IL_0029;
	}

IL_0028:
	{
		G_B6_0 = 1;
	}

IL_0029:
	{
		return (bool)G_B6_0;
	}
}
// T System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::ConvertItem(System.Object)
extern "C"  CustomAttributeTypedArgument_t2667702721  Collection_1_ConvertItem_m1866434341_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject * ___item0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Collection_1_ConvertItem_m1866434341_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject * L_0 = ___item0;
		bool L_1 = ((  bool (*) (RuntimeObject * /* static, unused */, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->methodPointer)(NULL /*static, unused*/, (RuntimeObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		RuntimeObject * L_2 = ___item0;
		return ((*(CustomAttributeTypedArgument_t2667702721 *)((CustomAttributeTypedArgument_t2667702721 *)UnBox(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)))));
	}

IL_0012:
	{
		ArgumentException_t1372035057 * L_3 = (ArgumentException_t1372035057 *)il2cpp_codegen_object_new(ArgumentException_t1372035057_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m775170528(L_3, (String_t*)_stringLiteral3684952963, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1_CheckWritable_m3803255060_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* ___list0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Collection_1_CheckWritable_m3803255060_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = ___list0;
		NullCheck((RuntimeObject*)L_0);
		bool L_1 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeTypedArgument>::get_IsReadOnly() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (RuntimeObject*)L_0);
		if (!L_1)
		{
			goto IL_0011;
		}
	}
	{
		NotSupportedException_t404487136 * L_2 = (NotSupportedException_t404487136 *)il2cpp_codegen_object_new(NotSupportedException_t404487136_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2647670222(L_2, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0011:
	{
		return;
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void ReadOnlyCollection_1__ctor_m2942681720_gshared (ReadOnlyCollection_1_t1566706403 * __this, RuntimeObject* ___list0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1__ctor_m2942681720_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m3446193457((RuntimeObject *)__this, /*hidden argument*/NULL);
		RuntimeObject* L_0 = ___list0;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t517973450 * L_1 = (ArgumentNullException_t517973450 *)il2cpp_codegen_object_new(ArgumentNullException_t517973450_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m2607242403(L_1, (String_t*)_stringLiteral1392156319, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0017:
	{
		RuntimeObject* L_2 = ___list0;
		__this->set_list_0(L_2);
		return;
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m3636396274_gshared (ReadOnlyCollection_1_t1566706403 * __this, RuntimeObject * ___item0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m3636396274_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t404487136 * L_0 = (NotSupportedException_t404487136 *)il2cpp_codegen_object_new(NotSupportedException_t404487136_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2647670222(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::System.Collections.Generic.ICollection<T>.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m2459590670_gshared (ReadOnlyCollection_1_t1566706403 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m2459590670_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t404487136 * L_0 = (NotSupportedException_t404487136 *)il2cpp_codegen_object_new(NotSupportedException_t404487136_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2647670222(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m56048574_gshared (ReadOnlyCollection_1_t1566706403 * __this, int32_t ___index0, RuntimeObject * ___item1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m56048574_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t404487136 * L_0 = (NotSupportedException_t404487136 *)il2cpp_codegen_object_new(NotSupportedException_t404487136_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2647670222(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m2475928297_gshared (ReadOnlyCollection_1_t1566706403 * __this, RuntimeObject * ___item0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m2475928297_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t404487136 * L_0 = (NotSupportedException_t404487136 *)il2cpp_codegen_object_new(NotSupportedException_t404487136_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2647670222(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m3133058195_gshared (ReadOnlyCollection_1_t1566706403 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m3133058195_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t404487136 * L_0 = (NotSupportedException_t404487136 *)il2cpp_codegen_object_new(NotSupportedException_t404487136_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2647670222(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// T System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C"  RuntimeObject * ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m3901338013_gshared (ReadOnlyCollection_1_t1566706403 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___index0;
		NullCheck((ReadOnlyCollection_1_t1566706403 *)__this);
		RuntimeObject * L_1 = ((  RuntimeObject * (*) (ReadOnlyCollection_1_t1566706403 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((ReadOnlyCollection_1_t1566706403 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_1;
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m2396010076_gshared (ReadOnlyCollection_1_t1566706403 * __this, int32_t ___index0, RuntimeObject * ___value1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m2396010076_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t404487136 * L_0 = (NotSupportedException_t404487136 *)il2cpp_codegen_object_new(NotSupportedException_t404487136_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2647670222(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1175586706_gshared (ReadOnlyCollection_1_t1566706403 * __this, const RuntimeMethod* method)
{
	{
		return (bool)1;
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1659178407_gshared (ReadOnlyCollection_1_t1566706403 * __this, RuntimeArray * ___array0, int32_t ___index1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1659178407_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_list_0();
		RuntimeArray * L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((RuntimeObject*)((RuntimeObject*)Castclass((RuntimeObject*)L_0, ICollection_t1881172908_il2cpp_TypeInfo_var)));
		InterfaceActionInvoker2< RuntimeArray *, int32_t >::Invoke(2 /* System.Void System.Collections.ICollection::CopyTo(System.Array,System.Int32) */, ICollection_t1881172908_il2cpp_TypeInfo_var, (RuntimeObject*)((RuntimeObject*)Castclass((RuntimeObject*)L_0, ICollection_t1881172908_il2cpp_TypeInfo_var)), (RuntimeArray *)L_1, (int32_t)L_2);
		return;
	}
}
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  RuntimeObject* ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m2575411489_gshared (ReadOnlyCollection_1_t1566706403 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m2575411489_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_list_0();
		NullCheck((RuntimeObject*)L_0);
		RuntimeObject* L_1 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t1872631827_il2cpp_TypeInfo_var, (RuntimeObject*)L_0);
		return L_1;
	}
}
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m512791597_gshared (ReadOnlyCollection_1_t1566706403 * __this, RuntimeObject * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_IList_Add_m512791597_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t404487136 * L_0 = (NotSupportedException_t404487136 *)il2cpp_codegen_object_new(NotSupportedException_t404487136_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2647670222(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::System.Collections.IList.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Clear_m1723151712_gshared (ReadOnlyCollection_1_t1566706403 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_IList_Clear_m1723151712_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t404487136 * L_0 = (NotSupportedException_t404487136 *)il2cpp_codegen_object_new(NotSupportedException_t404487136_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2647670222(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::System.Collections.IList.Contains(System.Object)
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_Contains_m1550933714_gshared (ReadOnlyCollection_1_t1566706403 * __this, RuntimeObject * ___value0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___value0;
		bool L_1 = ((  bool (*) (RuntimeObject * /* static, unused */, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, (RuntimeObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		RuntimeObject* L_2 = (RuntimeObject*)__this->get_list_0();
		RuntimeObject * L_3 = ___value0;
		NullCheck((RuntimeObject*)L_2);
		bool L_4 = InterfaceFuncInvoker1< bool, RuntimeObject * >::Invoke(4 /* System.Boolean System.Collections.Generic.ICollection`1<System.Object>::Contains(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (RuntimeObject*)L_2, (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))));
		return L_4;
	}

IL_001d:
	{
		return (bool)0;
	}
}
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m3467789291_gshared (ReadOnlyCollection_1_t1566706403 * __this, RuntimeObject * ___value0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___value0;
		bool L_1 = ((  bool (*) (RuntimeObject * /* static, unused */, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, (RuntimeObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		RuntimeObject* L_2 = (RuntimeObject*)__this->get_list_0();
		RuntimeObject * L_3 = ___value0;
		NullCheck((RuntimeObject*)L_2);
		int32_t L_4 = InterfaceFuncInvoker1< int32_t, RuntimeObject * >::Invoke(0 /* System.Int32 System.Collections.Generic.IList`1<System.Object>::IndexOf(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (RuntimeObject*)L_2, (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))));
		return L_4;
	}

IL_001d:
	{
		return (-1);
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Insert_m3706834204_gshared (ReadOnlyCollection_1_t1566706403 * __this, int32_t ___index0, RuntimeObject * ___value1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_IList_Insert_m3706834204_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t404487136 * L_0 = (NotSupportedException_t404487136 *)il2cpp_codegen_object_new(NotSupportedException_t404487136_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2647670222(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::System.Collections.IList.Remove(System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Remove_m1188019359_gshared (ReadOnlyCollection_1_t1566706403 * __this, RuntimeObject * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_IList_Remove_m1188019359_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t404487136 * L_0 = (NotSupportedException_t404487136 *)il2cpp_codegen_object_new(NotSupportedException_t404487136_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2647670222(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::System.Collections.IList.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m846301158_gshared (ReadOnlyCollection_1_t1566706403 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m846301158_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t404487136 * L_0 = (NotSupportedException_t404487136 *)il2cpp_codegen_object_new(NotSupportedException_t404487136_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2647670222(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  RuntimeObject * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m1476715048_gshared (ReadOnlyCollection_1_t1566706403 * __this, const RuntimeMethod* method)
{
	{
		return __this;
	}
}
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::System.Collections.IList.get_Item(System.Int32)
extern "C"  RuntimeObject * ReadOnlyCollection_1_System_Collections_IList_get_Item_m1262503712_gshared (ReadOnlyCollection_1_t1566706403 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_list_0();
		int32_t L_1 = ___index0;
		NullCheck((RuntimeObject*)L_0);
		RuntimeObject * L_2 = InterfaceFuncInvoker1< RuntimeObject *, int32_t >::Invoke(3 /* T System.Collections.Generic.IList`1<System.Object>::get_Item(System.Int32) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (RuntimeObject*)L_0, (int32_t)L_1);
		return L_2;
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_set_Item_m2878433948_gshared (ReadOnlyCollection_1_t1566706403 * __this, int32_t ___index0, RuntimeObject * ___value1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_IList_set_Item_m2878433948_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t404487136 * L_0 = (NotSupportedException_t404487136 *)il2cpp_codegen_object_new(NotSupportedException_t404487136_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2647670222(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::Contains(T)
extern "C"  bool ReadOnlyCollection_1_Contains_m1168093728_gshared (ReadOnlyCollection_1_t1566706403 * __this, RuntimeObject * ___value0, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_list_0();
		RuntimeObject * L_1 = ___value0;
		NullCheck((RuntimeObject*)L_0);
		bool L_2 = InterfaceFuncInvoker1< bool, RuntimeObject * >::Invoke(4 /* System.Boolean System.Collections.Generic.ICollection`1<System.Object>::Contains(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (RuntimeObject*)L_0, (RuntimeObject *)L_1);
		return L_2;
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::CopyTo(T[],System.Int32)
extern "C"  void ReadOnlyCollection_1_CopyTo_m810783127_gshared (ReadOnlyCollection_1_t1566706403 * __this, ObjectU5BU5D_t4290686858* ___array0, int32_t ___index1, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_list_0();
		ObjectU5BU5D_t4290686858* L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((RuntimeObject*)L_0);
		InterfaceActionInvoker2< ObjectU5BU5D_t4290686858*, int32_t >::Invoke(5 /* System.Void System.Collections.Generic.ICollection`1<System.Object>::CopyTo(T[],System.Int32) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (RuntimeObject*)L_0, (ObjectU5BU5D_t4290686858*)L_1, (int32_t)L_2);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::GetEnumerator()
extern "C"  RuntimeObject* ReadOnlyCollection_1_GetEnumerator_m2407170745_gshared (ReadOnlyCollection_1_t1566706403 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_list_0();
		NullCheck((RuntimeObject*)L_0);
		RuntimeObject* L_1 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6), (RuntimeObject*)L_0);
		return L_1;
	}
}
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::IndexOf(T)
extern "C"  int32_t ReadOnlyCollection_1_IndexOf_m1820813718_gshared (ReadOnlyCollection_1_t1566706403 * __this, RuntimeObject * ___value0, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_list_0();
		RuntimeObject * L_1 = ___value0;
		NullCheck((RuntimeObject*)L_0);
		int32_t L_2 = InterfaceFuncInvoker1< int32_t, RuntimeObject * >::Invoke(0 /* System.Int32 System.Collections.Generic.IList`1<System.Object>::IndexOf(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (RuntimeObject*)L_0, (RuntimeObject *)L_1);
		return L_2;
	}
}
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::get_Count()
extern "C"  int32_t ReadOnlyCollection_1_get_Count_m4125478618_gshared (ReadOnlyCollection_1_t1566706403 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_list_0();
		NullCheck((RuntimeObject*)L_0);
		int32_t L_1 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Object>::get_Count() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (RuntimeObject*)L_0);
		return L_1;
	}
}
// T System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::get_Item(System.Int32)
extern "C"  RuntimeObject * ReadOnlyCollection_1_get_Item_m482926831_gshared (ReadOnlyCollection_1_t1566706403 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_list_0();
		int32_t L_1 = ___index0;
		NullCheck((RuntimeObject*)L_0);
		RuntimeObject * L_2 = InterfaceFuncInvoker1< RuntimeObject *, int32_t >::Invoke(3 /* T System.Collections.Generic.IList`1<System.Object>::get_Item(System.Int32) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (RuntimeObject*)L_0, (int32_t)L_1);
		return L_2;
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void ReadOnlyCollection_1__ctor_m2132837842_gshared (ReadOnlyCollection_1_t2991387035 * __this, RuntimeObject* ___list0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1__ctor_m2132837842_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m3446193457((RuntimeObject *)__this, /*hidden argument*/NULL);
		RuntimeObject* L_0 = ___list0;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t517973450 * L_1 = (ArgumentNullException_t517973450 *)il2cpp_codegen_object_new(ArgumentNullException_t517973450_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m2607242403(L_1, (String_t*)_stringLiteral1392156319, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0017:
	{
		RuntimeObject* L_2 = ___list0;
		__this->set_list_0(L_2);
		return;
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m722286098_gshared (ReadOnlyCollection_1_t2991387035 * __this, CustomAttributeNamedArgument_t425730339  ___item0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m722286098_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t404487136 * L_0 = (NotSupportedException_t404487136 *)il2cpp_codegen_object_new(NotSupportedException_t404487136_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2647670222(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.Generic.ICollection<T>.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m940293707_gshared (ReadOnlyCollection_1_t2991387035 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m940293707_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t404487136 * L_0 = (NotSupportedException_t404487136 *)il2cpp_codegen_object_new(NotSupportedException_t404487136_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2647670222(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m2195383377_gshared (ReadOnlyCollection_1_t2991387035 * __this, int32_t ___index0, CustomAttributeNamedArgument_t425730339  ___item1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m2195383377_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t404487136 * L_0 = (NotSupportedException_t404487136 *)il2cpp_codegen_object_new(NotSupportedException_t404487136_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2647670222(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m2723190029_gshared (ReadOnlyCollection_1_t2991387035 * __this, CustomAttributeNamedArgument_t425730339  ___item0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m2723190029_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t404487136 * L_0 = (NotSupportedException_t404487136 *)il2cpp_codegen_object_new(NotSupportedException_t404487136_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2647670222(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1126840894_gshared (ReadOnlyCollection_1_t2991387035 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1126840894_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t404487136 * L_0 = (NotSupportedException_t404487136 *)il2cpp_codegen_object_new(NotSupportedException_t404487136_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2647670222(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// T System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C"  CustomAttributeNamedArgument_t425730339  ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m2378941591_gshared (ReadOnlyCollection_1_t2991387035 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___index0;
		NullCheck((ReadOnlyCollection_1_t2991387035 *)__this);
		CustomAttributeNamedArgument_t425730339  L_1 = ((  CustomAttributeNamedArgument_t425730339  (*) (ReadOnlyCollection_1_t2991387035 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((ReadOnlyCollection_1_t2991387035 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_1;
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m4285705850_gshared (ReadOnlyCollection_1_t2991387035 * __this, int32_t ___index0, CustomAttributeNamedArgument_t425730339  ___value1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m4285705850_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t404487136 * L_0 = (NotSupportedException_t404487136 *)il2cpp_codegen_object_new(NotSupportedException_t404487136_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2647670222(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1598064875_gshared (ReadOnlyCollection_1_t2991387035 * __this, const RuntimeMethod* method)
{
	{
		return (bool)1;
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m3787892541_gshared (ReadOnlyCollection_1_t2991387035 * __this, RuntimeArray * ___array0, int32_t ___index1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m3787892541_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_list_0();
		RuntimeArray * L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((RuntimeObject*)((RuntimeObject*)Castclass((RuntimeObject*)L_0, ICollection_t1881172908_il2cpp_TypeInfo_var)));
		InterfaceActionInvoker2< RuntimeArray *, int32_t >::Invoke(2 /* System.Void System.Collections.ICollection::CopyTo(System.Array,System.Int32) */, ICollection_t1881172908_il2cpp_TypeInfo_var, (RuntimeObject*)((RuntimeObject*)Castclass((RuntimeObject*)L_0, ICollection_t1881172908_il2cpp_TypeInfo_var)), (RuntimeArray *)L_1, (int32_t)L_2);
		return;
	}
}
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  RuntimeObject* ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m1931371127_gshared (ReadOnlyCollection_1_t2991387035 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m1931371127_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_list_0();
		NullCheck((RuntimeObject*)L_0);
		RuntimeObject* L_1 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t1872631827_il2cpp_TypeInfo_var, (RuntimeObject*)L_0);
		return L_1;
	}
}
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m3318813241_gshared (ReadOnlyCollection_1_t2991387035 * __this, RuntimeObject * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_IList_Add_m3318813241_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t404487136 * L_0 = (NotSupportedException_t404487136 *)il2cpp_codegen_object_new(NotSupportedException_t404487136_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2647670222(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Clear_m1348697002_gshared (ReadOnlyCollection_1_t2991387035 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_IList_Clear_m1348697002_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t404487136 * L_0 = (NotSupportedException_t404487136 *)il2cpp_codegen_object_new(NotSupportedException_t404487136_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2647670222(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.Contains(System.Object)
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_Contains_m2425632640_gshared (ReadOnlyCollection_1_t2991387035 * __this, RuntimeObject * ___value0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___value0;
		bool L_1 = ((  bool (*) (RuntimeObject * /* static, unused */, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, (RuntimeObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		RuntimeObject* L_2 = (RuntimeObject*)__this->get_list_0();
		RuntimeObject * L_3 = ___value0;
		NullCheck((RuntimeObject*)L_2);
		bool L_4 = InterfaceFuncInvoker1< bool, CustomAttributeNamedArgument_t425730339  >::Invoke(4 /* System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeNamedArgument>::Contains(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (RuntimeObject*)L_2, (CustomAttributeNamedArgument_t425730339 )((*(CustomAttributeNamedArgument_t425730339 *)((CustomAttributeNamedArgument_t425730339 *)UnBox(L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))));
		return L_4;
	}

IL_001d:
	{
		return (bool)0;
	}
}
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m2485734546_gshared (ReadOnlyCollection_1_t2991387035 * __this, RuntimeObject * ___value0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___value0;
		bool L_1 = ((  bool (*) (RuntimeObject * /* static, unused */, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, (RuntimeObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		RuntimeObject* L_2 = (RuntimeObject*)__this->get_list_0();
		RuntimeObject * L_3 = ___value0;
		NullCheck((RuntimeObject*)L_2);
		int32_t L_4 = InterfaceFuncInvoker1< int32_t, CustomAttributeNamedArgument_t425730339  >::Invoke(0 /* System.Int32 System.Collections.Generic.IList`1<System.Reflection.CustomAttributeNamedArgument>::IndexOf(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (RuntimeObject*)L_2, (CustomAttributeNamedArgument_t425730339 )((*(CustomAttributeNamedArgument_t425730339 *)((CustomAttributeNamedArgument_t425730339 *)UnBox(L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))));
		return L_4;
	}

IL_001d:
	{
		return (-1);
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Insert_m209543415_gshared (ReadOnlyCollection_1_t2991387035 * __this, int32_t ___index0, RuntimeObject * ___value1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_IList_Insert_m209543415_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t404487136 * L_0 = (NotSupportedException_t404487136 *)il2cpp_codegen_object_new(NotSupportedException_t404487136_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2647670222(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.Remove(System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Remove_m207792272_gshared (ReadOnlyCollection_1_t2991387035 * __this, RuntimeObject * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_IList_Remove_m207792272_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t404487136 * L_0 = (NotSupportedException_t404487136 *)il2cpp_codegen_object_new(NotSupportedException_t404487136_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2647670222(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m2584752914_gshared (ReadOnlyCollection_1_t2991387035 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m2584752914_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t404487136 * L_0 = (NotSupportedException_t404487136 *)il2cpp_codegen_object_new(NotSupportedException_t404487136_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2647670222(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.ICollection.get_SyncRoot()
extern "C"  RuntimeObject * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m1358944602_gshared (ReadOnlyCollection_1_t2991387035 * __this, const RuntimeMethod* method)
{
	{
		return __this;
	}
}
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.get_Item(System.Int32)
extern "C"  RuntimeObject * ReadOnlyCollection_1_System_Collections_IList_get_Item_m1827590261_gshared (ReadOnlyCollection_1_t2991387035 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_list_0();
		int32_t L_1 = ___index0;
		NullCheck((RuntimeObject*)L_0);
		CustomAttributeNamedArgument_t425730339  L_2 = InterfaceFuncInvoker1< CustomAttributeNamedArgument_t425730339 , int32_t >::Invoke(3 /* T System.Collections.Generic.IList`1<System.Reflection.CustomAttributeNamedArgument>::get_Item(System.Int32) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (RuntimeObject*)L_0, (int32_t)L_1);
		CustomAttributeNamedArgument_t425730339  L_3 = L_2;
		RuntimeObject * L_4 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), &L_3);
		return L_4;
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_set_Item_m736915307_gshared (ReadOnlyCollection_1_t2991387035 * __this, int32_t ___index0, RuntimeObject * ___value1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_IList_set_Item_m736915307_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t404487136 * L_0 = (NotSupportedException_t404487136 *)il2cpp_codegen_object_new(NotSupportedException_t404487136_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2647670222(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::Contains(T)
extern "C"  bool ReadOnlyCollection_1_Contains_m200774649_gshared (ReadOnlyCollection_1_t2991387035 * __this, CustomAttributeNamedArgument_t425730339  ___value0, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_list_0();
		CustomAttributeNamedArgument_t425730339  L_1 = ___value0;
		NullCheck((RuntimeObject*)L_0);
		bool L_2 = InterfaceFuncInvoker1< bool, CustomAttributeNamedArgument_t425730339  >::Invoke(4 /* System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeNamedArgument>::Contains(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (RuntimeObject*)L_0, (CustomAttributeNamedArgument_t425730339 )L_1);
		return L_2;
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::CopyTo(T[],System.Int32)
extern "C"  void ReadOnlyCollection_1_CopyTo_m3175513352_gshared (ReadOnlyCollection_1_t2991387035 * __this, CustomAttributeNamedArgumentU5BU5D_t3127017202* ___array0, int32_t ___index1, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_list_0();
		CustomAttributeNamedArgumentU5BU5D_t3127017202* L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((RuntimeObject*)L_0);
		InterfaceActionInvoker2< CustomAttributeNamedArgumentU5BU5D_t3127017202*, int32_t >::Invoke(5 /* System.Void System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeNamedArgument>::CopyTo(T[],System.Int32) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (RuntimeObject*)L_0, (CustomAttributeNamedArgumentU5BU5D_t3127017202*)L_1, (int32_t)L_2);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::GetEnumerator()
extern "C"  RuntimeObject* ReadOnlyCollection_1_GetEnumerator_m1878221507_gshared (ReadOnlyCollection_1_t2991387035 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_list_0();
		NullCheck((RuntimeObject*)L_0);
		RuntimeObject* L_1 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Reflection.CustomAttributeNamedArgument>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6), (RuntimeObject*)L_0);
		return L_1;
	}
}
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::IndexOf(T)
extern "C"  int32_t ReadOnlyCollection_1_IndexOf_m246350514_gshared (ReadOnlyCollection_1_t2991387035 * __this, CustomAttributeNamedArgument_t425730339  ___value0, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_list_0();
		CustomAttributeNamedArgument_t425730339  L_1 = ___value0;
		NullCheck((RuntimeObject*)L_0);
		int32_t L_2 = InterfaceFuncInvoker1< int32_t, CustomAttributeNamedArgument_t425730339  >::Invoke(0 /* System.Int32 System.Collections.Generic.IList`1<System.Reflection.CustomAttributeNamedArgument>::IndexOf(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (RuntimeObject*)L_0, (CustomAttributeNamedArgument_t425730339 )L_1);
		return L_2;
	}
}
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::get_Count()
extern "C"  int32_t ReadOnlyCollection_1_get_Count_m2820387799_gshared (ReadOnlyCollection_1_t2991387035 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_list_0();
		NullCheck((RuntimeObject*)L_0);
		int32_t L_1 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeNamedArgument>::get_Count() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (RuntimeObject*)L_0);
		return L_1;
	}
}
// T System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::get_Item(System.Int32)
extern "C"  CustomAttributeNamedArgument_t425730339  ReadOnlyCollection_1_get_Item_m546166561_gshared (ReadOnlyCollection_1_t2991387035 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_list_0();
		int32_t L_1 = ___index0;
		NullCheck((RuntimeObject*)L_0);
		CustomAttributeNamedArgument_t425730339  L_2 = InterfaceFuncInvoker1< CustomAttributeNamedArgument_t425730339 , int32_t >::Invoke(3 /* T System.Collections.Generic.IList`1<System.Reflection.CustomAttributeNamedArgument>::get_Item(System.Int32) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (RuntimeObject*)L_0, (int32_t)L_1);
		return L_2;
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void ReadOnlyCollection_1__ctor_m3588137511_gshared (ReadOnlyCollection_1_t938392121 * __this, RuntimeObject* ___list0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1__ctor_m3588137511_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m3446193457((RuntimeObject *)__this, /*hidden argument*/NULL);
		RuntimeObject* L_0 = ___list0;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t517973450 * L_1 = (ArgumentNullException_t517973450 *)il2cpp_codegen_object_new(ArgumentNullException_t517973450_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m2607242403(L_1, (String_t*)_stringLiteral1392156319, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0017:
	{
		RuntimeObject* L_2 = ___list0;
		__this->set_list_0(L_2);
		return;
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2164904258_gshared (ReadOnlyCollection_1_t938392121 * __this, CustomAttributeTypedArgument_t2667702721  ___item0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2164904258_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t404487136 * L_0 = (NotSupportedException_t404487136 *)il2cpp_codegen_object_new(NotSupportedException_t404487136_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2647670222(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.Generic.ICollection<T>.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m3903190083_gshared (ReadOnlyCollection_1_t938392121 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m3903190083_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t404487136 * L_0 = (NotSupportedException_t404487136 *)il2cpp_codegen_object_new(NotSupportedException_t404487136_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2647670222(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m3712054434_gshared (ReadOnlyCollection_1_t938392121 * __this, int32_t ___index0, CustomAttributeTypedArgument_t2667702721  ___item1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m3712054434_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t404487136 * L_0 = (NotSupportedException_t404487136 *)il2cpp_codegen_object_new(NotSupportedException_t404487136_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2647670222(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m269180624_gshared (ReadOnlyCollection_1_t938392121 * __this, CustomAttributeTypedArgument_t2667702721  ___item0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m269180624_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t404487136 * L_0 = (NotSupportedException_t404487136 *)il2cpp_codegen_object_new(NotSupportedException_t404487136_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2647670222(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1391504284_gshared (ReadOnlyCollection_1_t938392121 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1391504284_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t404487136 * L_0 = (NotSupportedException_t404487136 *)il2cpp_codegen_object_new(NotSupportedException_t404487136_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2647670222(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// T System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C"  CustomAttributeTypedArgument_t2667702721  ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m4065393798_gshared (ReadOnlyCollection_1_t938392121 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___index0;
		NullCheck((ReadOnlyCollection_1_t938392121 *)__this);
		CustomAttributeTypedArgument_t2667702721  L_1 = ((  CustomAttributeTypedArgument_t2667702721  (*) (ReadOnlyCollection_1_t938392121 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((ReadOnlyCollection_1_t938392121 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_1;
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m3922031874_gshared (ReadOnlyCollection_1_t938392121 * __this, int32_t ___index0, CustomAttributeTypedArgument_t2667702721  ___value1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m3922031874_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t404487136 * L_0 = (NotSupportedException_t404487136 *)il2cpp_codegen_object_new(NotSupportedException_t404487136_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2647670222(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m307641447_gshared (ReadOnlyCollection_1_t938392121 * __this, const RuntimeMethod* method)
{
	{
		return (bool)1;
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m4098886227_gshared (ReadOnlyCollection_1_t938392121 * __this, RuntimeArray * ___array0, int32_t ___index1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m4098886227_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_list_0();
		RuntimeArray * L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((RuntimeObject*)((RuntimeObject*)Castclass((RuntimeObject*)L_0, ICollection_t1881172908_il2cpp_TypeInfo_var)));
		InterfaceActionInvoker2< RuntimeArray *, int32_t >::Invoke(2 /* System.Void System.Collections.ICollection::CopyTo(System.Array,System.Int32) */, ICollection_t1881172908_il2cpp_TypeInfo_var, (RuntimeObject*)((RuntimeObject*)Castclass((RuntimeObject*)L_0, ICollection_t1881172908_il2cpp_TypeInfo_var)), (RuntimeArray *)L_1, (int32_t)L_2);
		return;
	}
}
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  RuntimeObject* ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m183135228_gshared (ReadOnlyCollection_1_t938392121 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m183135228_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_list_0();
		NullCheck((RuntimeObject*)L_0);
		RuntimeObject* L_1 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t1872631827_il2cpp_TypeInfo_var, (RuntimeObject*)L_0);
		return L_1;
	}
}
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m3099590404_gshared (ReadOnlyCollection_1_t938392121 * __this, RuntimeObject * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_IList_Add_m3099590404_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t404487136 * L_0 = (NotSupportedException_t404487136 *)il2cpp_codegen_object_new(NotSupportedException_t404487136_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2647670222(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Clear_m2584525582_gshared (ReadOnlyCollection_1_t938392121 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_IList_Clear_m2584525582_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t404487136 * L_0 = (NotSupportedException_t404487136 *)il2cpp_codegen_object_new(NotSupportedException_t404487136_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2647670222(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.Contains(System.Object)
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_Contains_m351685156_gshared (ReadOnlyCollection_1_t938392121 * __this, RuntimeObject * ___value0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___value0;
		bool L_1 = ((  bool (*) (RuntimeObject * /* static, unused */, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, (RuntimeObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		RuntimeObject* L_2 = (RuntimeObject*)__this->get_list_0();
		RuntimeObject * L_3 = ___value0;
		NullCheck((RuntimeObject*)L_2);
		bool L_4 = InterfaceFuncInvoker1< bool, CustomAttributeTypedArgument_t2667702721  >::Invoke(4 /* System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeTypedArgument>::Contains(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (RuntimeObject*)L_2, (CustomAttributeTypedArgument_t2667702721 )((*(CustomAttributeTypedArgument_t2667702721 *)((CustomAttributeTypedArgument_t2667702721 *)UnBox(L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))));
		return L_4;
	}

IL_001d:
	{
		return (bool)0;
	}
}
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m1706515080_gshared (ReadOnlyCollection_1_t938392121 * __this, RuntimeObject * ___value0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___value0;
		bool L_1 = ((  bool (*) (RuntimeObject * /* static, unused */, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, (RuntimeObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		RuntimeObject* L_2 = (RuntimeObject*)__this->get_list_0();
		RuntimeObject * L_3 = ___value0;
		NullCheck((RuntimeObject*)L_2);
		int32_t L_4 = InterfaceFuncInvoker1< int32_t, CustomAttributeTypedArgument_t2667702721  >::Invoke(0 /* System.Int32 System.Collections.Generic.IList`1<System.Reflection.CustomAttributeTypedArgument>::IndexOf(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (RuntimeObject*)L_2, (CustomAttributeTypedArgument_t2667702721 )((*(CustomAttributeTypedArgument_t2667702721 *)((CustomAttributeTypedArgument_t2667702721 *)UnBox(L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))));
		return L_4;
	}

IL_001d:
	{
		return (-1);
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Insert_m3639190649_gshared (ReadOnlyCollection_1_t938392121 * __this, int32_t ___index0, RuntimeObject * ___value1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_IList_Insert_m3639190649_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t404487136 * L_0 = (NotSupportedException_t404487136 *)il2cpp_codegen_object_new(NotSupportedException_t404487136_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2647670222(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.Remove(System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Remove_m2921050119_gshared (ReadOnlyCollection_1_t938392121 * __this, RuntimeObject * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_IList_Remove_m2921050119_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t404487136 * L_0 = (NotSupportedException_t404487136 *)il2cpp_codegen_object_new(NotSupportedException_t404487136_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2647670222(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1775574839_gshared (ReadOnlyCollection_1_t938392121 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1775574839_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t404487136 * L_0 = (NotSupportedException_t404487136 *)il2cpp_codegen_object_new(NotSupportedException_t404487136_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2647670222(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.ICollection.get_SyncRoot()
extern "C"  RuntimeObject * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m696101892_gshared (ReadOnlyCollection_1_t938392121 * __this, const RuntimeMethod* method)
{
	{
		return __this;
	}
}
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.get_Item(System.Int32)
extern "C"  RuntimeObject * ReadOnlyCollection_1_System_Collections_IList_get_Item_m3698975754_gshared (ReadOnlyCollection_1_t938392121 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_list_0();
		int32_t L_1 = ___index0;
		NullCheck((RuntimeObject*)L_0);
		CustomAttributeTypedArgument_t2667702721  L_2 = InterfaceFuncInvoker1< CustomAttributeTypedArgument_t2667702721 , int32_t >::Invoke(3 /* T System.Collections.Generic.IList`1<System.Reflection.CustomAttributeTypedArgument>::get_Item(System.Int32) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (RuntimeObject*)L_0, (int32_t)L_1);
		CustomAttributeTypedArgument_t2667702721  L_3 = L_2;
		RuntimeObject * L_4 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), &L_3);
		return L_4;
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_set_Item_m1368414667_gshared (ReadOnlyCollection_1_t938392121 * __this, int32_t ___index0, RuntimeObject * ___value1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_IList_set_Item_m1368414667_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t404487136 * L_0 = (NotSupportedException_t404487136 *)il2cpp_codegen_object_new(NotSupportedException_t404487136_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2647670222(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::Contains(T)
extern "C"  bool ReadOnlyCollection_1_Contains_m3244800502_gshared (ReadOnlyCollection_1_t938392121 * __this, CustomAttributeTypedArgument_t2667702721  ___value0, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_list_0();
		CustomAttributeTypedArgument_t2667702721  L_1 = ___value0;
		NullCheck((RuntimeObject*)L_0);
		bool L_2 = InterfaceFuncInvoker1< bool, CustomAttributeTypedArgument_t2667702721  >::Invoke(4 /* System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeTypedArgument>::Contains(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (RuntimeObject*)L_0, (CustomAttributeTypedArgument_t2667702721 )L_1);
		return L_2;
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::CopyTo(T[],System.Int32)
extern "C"  void ReadOnlyCollection_1_CopyTo_m3039699443_gshared (ReadOnlyCollection_1_t938392121 * __this, CustomAttributeTypedArgumentU5BU5D_t2668173852* ___array0, int32_t ___index1, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_list_0();
		CustomAttributeTypedArgumentU5BU5D_t2668173852* L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((RuntimeObject*)L_0);
		InterfaceActionInvoker2< CustomAttributeTypedArgumentU5BU5D_t2668173852*, int32_t >::Invoke(5 /* System.Void System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeTypedArgument>::CopyTo(T[],System.Int32) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (RuntimeObject*)L_0, (CustomAttributeTypedArgumentU5BU5D_t2668173852*)L_1, (int32_t)L_2);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::GetEnumerator()
extern "C"  RuntimeObject* ReadOnlyCollection_1_GetEnumerator_m3100546923_gshared (ReadOnlyCollection_1_t938392121 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_list_0();
		NullCheck((RuntimeObject*)L_0);
		RuntimeObject* L_1 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Reflection.CustomAttributeTypedArgument>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6), (RuntimeObject*)L_0);
		return L_1;
	}
}
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::IndexOf(T)
extern "C"  int32_t ReadOnlyCollection_1_IndexOf_m3498076143_gshared (ReadOnlyCollection_1_t938392121 * __this, CustomAttributeTypedArgument_t2667702721  ___value0, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_list_0();
		CustomAttributeTypedArgument_t2667702721  L_1 = ___value0;
		NullCheck((RuntimeObject*)L_0);
		int32_t L_2 = InterfaceFuncInvoker1< int32_t, CustomAttributeTypedArgument_t2667702721  >::Invoke(0 /* System.Int32 System.Collections.Generic.IList`1<System.Reflection.CustomAttributeTypedArgument>::IndexOf(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (RuntimeObject*)L_0, (CustomAttributeTypedArgument_t2667702721 )L_1);
		return L_2;
	}
}
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::get_Count()
extern "C"  int32_t ReadOnlyCollection_1_get_Count_m1647071898_gshared (ReadOnlyCollection_1_t938392121 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_list_0();
		NullCheck((RuntimeObject*)L_0);
		int32_t L_1 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeTypedArgument>::get_Count() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (RuntimeObject*)L_0);
		return L_1;
	}
}
// T System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::get_Item(System.Int32)
extern "C"  CustomAttributeTypedArgument_t2667702721  ReadOnlyCollection_1_get_Item_m2378271940_gshared (ReadOnlyCollection_1_t938392121 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_list_0();
		int32_t L_1 = ___index0;
		NullCheck((RuntimeObject*)L_0);
		CustomAttributeTypedArgument_t2667702721  L_2 = InterfaceFuncInvoker1< CustomAttributeTypedArgument_t2667702721 , int32_t >::Invoke(3 /* T System.Collections.Generic.IList`1<System.Reflection.CustomAttributeTypedArgument>::get_Item(System.Int32) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (RuntimeObject*)L_0, (int32_t)L_1);
		return L_2;
	}
}
// System.Void System.Comparison`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Comparison_1__ctor_m2063020281_gshared (Comparison_1_t1028579740 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Int32 System.Comparison`1<System.Object>::Invoke(T,T)
extern "C"  int32_t Comparison_1_Invoke_m727370941_gshared (Comparison_1_t1028579740 * __this, RuntimeObject * ___x0, RuntimeObject * ___y1, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Comparison_1_Invoke_m727370941((Comparison_1_t1028579740 *)__this->get_prev_9(),___x0, ___y1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (RuntimeObject *, void* __this, RuntimeObject * ___x0, RuntimeObject * ___y1, const RuntimeMethod* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___x0, ___y1,(RuntimeMethod*)(__this->get_method_3()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (void* __this, RuntimeObject * ___x0, RuntimeObject * ___y1, const RuntimeMethod* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___x0, ___y1,(RuntimeMethod*)(__this->get_method_3()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, RuntimeObject * ___y1, const RuntimeMethod* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___x0, ___y1,(RuntimeMethod*)(__this->get_method_3()));
	}
}
// System.IAsyncResult System.Comparison`1<System.Object>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* Comparison_1_BeginInvoke_m2806971575_gshared (Comparison_1_t1028579740 * __this, RuntimeObject * ___x0, RuntimeObject * ___y1, AsyncCallback_t1046330627 * ___callback2, RuntimeObject * ___object3, const RuntimeMethod* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___x0;
	__d_args[1] = ___y1;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback2, (RuntimeObject*)___object3);
}
// System.Int32 System.Comparison`1<System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t Comparison_1_EndInvoke_m3264174865_gshared (Comparison_1_t1028579740 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	RuntimeObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((RuntimeObject*)__result);
}
// System.Void System.Converter`2<System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Converter_2__ctor_m1069423207_gshared (Converter_2_t4184931014 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TOutput System.Converter`2<System.Object,System.Object>::Invoke(TInput)
extern "C"  RuntimeObject * Converter_2_Invoke_m303716893_gshared (Converter_2_t4184931014 * __this, RuntimeObject * ___input0, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Converter_2_Invoke_m303716893((Converter_2_t4184931014 *)__this->get_prev_9(),___input0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef RuntimeObject * (*FunctionPointerType) (RuntimeObject *, void* __this, RuntimeObject * ___input0, const RuntimeMethod* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___input0,(RuntimeMethod*)(__this->get_method_3()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef RuntimeObject * (*FunctionPointerType) (void* __this, RuntimeObject * ___input0, const RuntimeMethod* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___input0,(RuntimeMethod*)(__this->get_method_3()));
	}
	else
	{
		typedef RuntimeObject * (*FunctionPointerType) (void* __this, const RuntimeMethod* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___input0,(RuntimeMethod*)(__this->get_method_3()));
	}
}
// System.IAsyncResult System.Converter`2<System.Object,System.Object>::BeginInvoke(TInput,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* Converter_2_BeginInvoke_m2870464565_gshared (Converter_2_t4184931014 * __this, RuntimeObject * ___input0, AsyncCallback_t1046330627 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___input0;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback1, (RuntimeObject*)___object2);
}
// TOutput System.Converter`2<System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  RuntimeObject * Converter_2_EndInvoke_m2535438830_gshared (Converter_2_t4184931014 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	RuntimeObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (RuntimeObject *)__result;
}
// System.Void System.Func`2<System.Object,System.Boolean>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_2__ctor_m1821613634_gshared (Func_2_t1469056018 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TResult System.Func`2<System.Object,System.Boolean>::Invoke(T)
extern "C"  bool Func_2_Invoke_m2527071059_gshared (Func_2_t1469056018 * __this, RuntimeObject * ___arg10, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Func_2_Invoke_m2527071059((Func_2_t1469056018 *)__this->get_prev_9(),___arg10, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (RuntimeObject *, void* __this, RuntimeObject * ___arg10, const RuntimeMethod* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg10,(RuntimeMethod*)(__this->get_method_3()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (void* __this, RuntimeObject * ___arg10, const RuntimeMethod* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg10,(RuntimeMethod*)(__this->get_method_3()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, const RuntimeMethod* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___arg10,(RuntimeMethod*)(__this->get_method_3()));
	}
}
// System.IAsyncResult System.Func`2<System.Object,System.Boolean>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* Func_2_BeginInvoke_m482654750_gshared (Func_2_t1469056018 * __this, RuntimeObject * ___arg10, AsyncCallback_t1046330627 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___arg10;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback1, (RuntimeObject*)___object2);
}
// TResult System.Func`2<System.Object,System.Boolean>::EndInvoke(System.IAsyncResult)
extern "C"  bool Func_2_EndInvoke_m110825300_gshared (Func_2_t1469056018 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	RuntimeObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((RuntimeObject*)__result);
}
// System.Void System.Func`2<System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_2__ctor_m1325525149_gshared (Func_2_t2705400122 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TResult System.Func`2<System.Object,System.Object>::Invoke(T)
extern "C"  RuntimeObject * Func_2_Invoke_m4186273240_gshared (Func_2_t2705400122 * __this, RuntimeObject * ___arg10, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Func_2_Invoke_m4186273240((Func_2_t2705400122 *)__this->get_prev_9(),___arg10, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef RuntimeObject * (*FunctionPointerType) (RuntimeObject *, void* __this, RuntimeObject * ___arg10, const RuntimeMethod* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg10,(RuntimeMethod*)(__this->get_method_3()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef RuntimeObject * (*FunctionPointerType) (void* __this, RuntimeObject * ___arg10, const RuntimeMethod* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg10,(RuntimeMethod*)(__this->get_method_3()));
	}
	else
	{
		typedef RuntimeObject * (*FunctionPointerType) (void* __this, const RuntimeMethod* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___arg10,(RuntimeMethod*)(__this->get_method_3()));
	}
}
// System.IAsyncResult System.Func`2<System.Object,System.Object>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* Func_2_BeginInvoke_m1735972450_gshared (Func_2_t2705400122 * __this, RuntimeObject * ___arg10, AsyncCallback_t1046330627 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___arg10;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback1, (RuntimeObject*)___object2);
}
// TResult System.Func`2<System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  RuntimeObject * Func_2_EndInvoke_m4200422096_gshared (Func_2_t2705400122 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	RuntimeObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (RuntimeObject *)__result;
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
