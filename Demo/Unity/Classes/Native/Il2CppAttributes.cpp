﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// System.Reflection.AssemblyTitleAttribute
struct AssemblyTitleAttribute_t2942119159;
// System.String
struct String_t;
// System.Reflection.AssemblyFileVersionAttribute
struct AssemblyFileVersionAttribute_t3786563949;
// System.Reflection.AssemblyKeyFileAttribute
struct AssemblyKeyFileAttribute_t1077071033;
// System.Reflection.AssemblyDelaySignAttribute
struct AssemblyDelaySignAttribute_t1978111684;
// System.CLSCompliantAttribute
struct CLSCompliantAttribute_t2118253839;
// System.Resources.NeutralResourcesLanguageAttribute
struct NeutralResourcesLanguageAttribute_t2446220560;
// System.Resources.SatelliteContractVersionAttribute
struct SatelliteContractVersionAttribute_t2421164883;
// System.Reflection.AssemblyInformationalVersionAttribute
struct AssemblyInformationalVersionAttribute_t2163289957;
// System.Reflection.AssemblyCopyrightAttribute
struct AssemblyCopyrightAttribute_t1775796795;
// System.Runtime.InteropServices.ComVisibleAttribute
struct ComVisibleAttribute_t2289974704;
// System.Reflection.AssemblyDefaultAliasAttribute
struct AssemblyDefaultAliasAttribute_t356814493;
// System.Reflection.AssemblyDescriptionAttribute
struct AssemblyDescriptionAttribute_t1589567422;
// System.Runtime.InteropServices.GuidAttribute
struct GuidAttribute_t3853718442;
// System.Runtime.CompilerServices.StringFreezingAttribute
struct StringFreezingAttribute_t4005798349;
// System.Runtime.CompilerServices.DefaultDependencyAttribute
struct DefaultDependencyAttribute_t2058824218;
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_t38650954;
// System.Runtime.InteropServices.TypeLibVersionAttribute
struct TypeLibVersionAttribute_t1866475684;
// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_t3717120398;
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t606593683;
// System.Reflection.AssemblyProductAttribute
struct AssemblyProductAttribute_t3365982468;
// System.Reflection.AssemblyCompanyAttribute
struct AssemblyCompanyAttribute_t272143903;
// System.Runtime.InteropServices.ClassInterfaceAttribute
struct ClassInterfaceAttribute_t1638101999;
// System.Runtime.ConstrainedExecution.ReliabilityContractAttribute
struct ReliabilityContractAttribute_t3009184403;
// System.Runtime.InteropServices.ComDefaultInterfaceAttribute
struct ComDefaultInterfaceAttribute_t2399868801;
// System.Type
struct Type_t;
// System.AttributeUsageAttribute
struct AttributeUsageAttribute_t2370971113;
// System.Runtime.InteropServices.TypeLibImportClassAttribute
struct TypeLibImportClassAttribute_t2173872460;
// System.Runtime.InteropServices.InterfaceTypeAttribute
struct InterfaceTypeAttribute_t3336632297;
// System.Runtime.InteropServices.DispIdAttribute
struct DispIdAttribute_t3503809048;
// System.Reflection.DefaultMemberAttribute
struct DefaultMemberAttribute_t2956431904;
// System.ParamArrayAttribute
struct ParamArrayAttribute_t3330428127;
// System.MonoDocumentationNoteAttribute
struct MonoDocumentationNoteAttribute_t3720325674;
// System.Runtime.CompilerServices.DecimalConstantAttribute
struct DecimalConstantAttribute_t200860697;
// System.ObsoleteAttribute
struct ObsoleteAttribute_t3443961000;
// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_t1447627238;
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t3076080542;
// System.MonoTODOAttribute
struct MonoTODOAttribute_t614642062;
// System.Diagnostics.DebuggerDisplayAttribute
struct DebuggerDisplayAttribute_t2915478477;
// System.Diagnostics.DebuggerTypeProxyAttribute
struct DebuggerTypeProxyAttribute_t2114273919;
// System.FlagsAttribute
struct FlagsAttribute_t388727259;
// System.Diagnostics.DebuggerStepThroughAttribute
struct DebuggerStepThroughAttribute_t3823612831;
// System.Security.SuppressUnmanagedCodeSecurityAttribute
struct SuppressUnmanagedCodeSecurityAttribute_t1135819281;
// System.ThreadStaticAttribute
struct ThreadStaticAttribute_t174455095;
// System.Runtime.CompilerServices.InternalsVisibleToAttribute
struct InternalsVisibleToAttribute_t2168674587;
// System.MonoTODOAttribute
struct MonoTODOAttribute_t614642063;
// System.ComponentModel.TypeConverterAttribute
struct TypeConverterAttribute_t553551222;
// System.Security.SecurityCriticalAttribute
struct SecurityCriticalAttribute_t3270381382;
// System.Security.AllowPartiallyTrustedCallersAttribute
struct AllowPartiallyTrustedCallersAttribute_t2708656030;
// System.Runtime.CompilerServices.ExtensionAttribute
struct ExtensionAttribute_t1721836986;
// System.Diagnostics.DebuggerBrowsableAttribute
struct DebuggerBrowsableAttribute_t1527328379;
// UnityEngine.Scripting.RequiredByNativeCodeAttribute
struct RequiredByNativeCodeAttribute_t3603024077;
// UnityEngine.ThreadAndSerializationSafeAttribute
struct ThreadAndSerializationSafeAttribute_t566760631;
// UnityEngine.Scripting.GeneratedByOldBindingsGeneratorAttribute
struct GeneratedByOldBindingsGeneratorAttribute_t861731860;
// UnityEngine.WritableAttribute
struct WritableAttribute_t2474002418;
// UnityEngine.Scripting.UsedByNativeCodeAttribute
struct UsedByNativeCodeAttribute_t2783261251;
// UnityEngine.RequireComponent
struct RequireComponent_t597737523;
// System.Security.SecuritySafeCriticalAttribute
struct SecuritySafeCriticalAttribute_t824959508;
// UnityEngine.Internal.DefaultValueAttribute
struct DefaultValueAttribute_t2693326634;
// UnityEngine.NativeClassAttribute
struct NativeClassAttribute_t3085471410;
// UnityEngine.Bindings.UnmarshalledAttribute
struct UnmarshalledAttribute_t2695814082;
// UnityEngine.Serialization.FormerlySerializedAsAttribute
struct FormerlySerializedAsAttribute_t1489703117;
// UnityEngine.SerializeField
struct SerializeField_t1393754034;
// System.Runtime.InteropServices.UnmanagedFunctionPointerAttribute
struct UnmanagedFunctionPointerAttribute_t1887822751;
// UnityEngine.AddComponentMenu
struct AddComponentMenu_t3529181215;
// System.Char[]
struct CharU5BU5D_t2864810077;
// System.Void
struct Void_t1078098495;
// System.Version
struct Version_t900644674;
// System.Type[]
struct TypeU5BU5D_t1830665827;
// System.Reflection.MemberFilter
struct MemberFilter_t1197335030;

extern const RuntimeType* _Attribute_t480711065_0_0_0_var;
extern const uint32_t Attribute_t1659210826_CustomAttributesCacheGenerator_MetadataUsageId;
extern const RuntimeType* Attribute_t1659210826_0_0_0_var;
extern const uint32_t _Attribute_t480711065_CustomAttributesCacheGenerator_MetadataUsageId;
extern const RuntimeType* _Type_t2913350559_0_0_0_var;
extern const uint32_t Type_t_CustomAttributesCacheGenerator_MetadataUsageId;
extern const RuntimeType* _MemberInfo_t1659175437_0_0_0_var;
extern const uint32_t MemberInfo_t_CustomAttributesCacheGenerator_MetadataUsageId;
extern const RuntimeType* MemberInfo_t_0_0_0_var;
extern const uint32_t _MemberInfo_t1659175437_CustomAttributesCacheGenerator_MetadataUsageId;
extern const RuntimeType* Type_t_0_0_0_var;
extern const uint32_t _Type_t2913350559_CustomAttributesCacheGenerator_MetadataUsageId;
extern const RuntimeType* _Exception_t274881737_0_0_0_var;
extern const uint32_t Exception_t3361176243_CustomAttributesCacheGenerator_MetadataUsageId;
extern const RuntimeType* CollectionDebuggerView_2_t92416143_0_0_0_var;
extern const uint32_t Dictionary_2_t1922834018_CustomAttributesCacheGenerator_MetadataUsageId;
extern const RuntimeType* CollectionDebuggerView_1_t1444680478_0_0_0_var;
extern const uint32_t List_1_t82844879_CustomAttributesCacheGenerator_MetadataUsageId;
extern const RuntimeType* CollectionDebuggerView_t339781370_0_0_0_var;
extern const uint32_t ArrayList_t655337682_CustomAttributesCacheGenerator_MetadataUsageId;
extern const uint32_t Hashtable_t3529848683_CustomAttributesCacheGenerator_MetadataUsageId;
extern const uint32_t HashKeys_t3304597669_CustomAttributesCacheGenerator_MetadataUsageId;
extern const uint32_t HashValues_t1675258150_CustomAttributesCacheGenerator_MetadataUsageId;
extern const uint32_t Stack_t733017594_CustomAttributesCacheGenerator_MetadataUsageId;
extern const RuntimeType* _AssemblyBuilder_t2438890953_0_0_0_var;
extern const uint32_t AssemblyBuilder_t489430578_CustomAttributesCacheGenerator_MetadataUsageId;
extern const RuntimeType* _ConstructorBuilder_t3355697174_0_0_0_var;
extern const uint32_t ConstructorBuilder_t1596923965_CustomAttributesCacheGenerator_MetadataUsageId;
extern const RuntimeType* _CustomAttributeBuilder_t2651440002_0_0_0_var;
extern const uint32_t CustomAttributeBuilder_t1432176696_CustomAttributesCacheGenerator_MetadataUsageId;
extern const RuntimeType* _EnumBuilder_t3195197362_0_0_0_var;
extern const uint32_t EnumBuilder_t617843_CustomAttributesCacheGenerator_MetadataUsageId;
extern const RuntimeType* _EventBuilder_t3169829690_0_0_0_var;
extern const uint32_t EventBuilder_t2300762438_CustomAttributesCacheGenerator_MetadataUsageId;
extern const RuntimeType* _FieldBuilder_t3486190427_0_0_0_var;
extern const uint32_t FieldBuilder_t803025013_CustomAttributesCacheGenerator_MetadataUsageId;
extern const RuntimeType* _ILGenerator_t3086005605_0_0_0_var;
extern const uint32_t ILGenerator_t1111467769_CustomAttributesCacheGenerator_MetadataUsageId;
extern const RuntimeType* _MethodBuilder_t195532601_0_0_0_var;
extern const uint32_t MethodBuilder_t2964638190_CustomAttributesCacheGenerator_MetadataUsageId;
extern const RuntimeType* _ModuleBuilder_t3149677118_0_0_0_var;
extern const uint32_t ModuleBuilder_t671559400_CustomAttributesCacheGenerator_MetadataUsageId;
extern const RuntimeType* _ParameterBuilder_t3730072709_0_0_0_var;
extern const uint32_t ParameterBuilder_t2478802094_CustomAttributesCacheGenerator_MetadataUsageId;
extern const RuntimeType* _PropertyBuilder_t2876596643_0_0_0_var;
extern const uint32_t PropertyBuilder_t1258013571_CustomAttributesCacheGenerator_MetadataUsageId;
extern const RuntimeType* _TypeBuilder_t4136446867_0_0_0_var;
extern const uint32_t TypeBuilder_t1663068587_CustomAttributesCacheGenerator_MetadataUsageId;
extern const RuntimeType* _Assembly_t3106968071_0_0_0_var;
extern const uint32_t Assembly_t2665685420_CustomAttributesCacheGenerator_MetadataUsageId;
extern const RuntimeType* _AssemblyName_t349687559_0_0_0_var;
extern const uint32_t AssemblyName_t997940415_CustomAttributesCacheGenerator_MetadataUsageId;
extern const RuntimeType* _ConstructorInfo_t1952808443_0_0_0_var;
extern const uint32_t ConstructorInfo_t3360520918_CustomAttributesCacheGenerator_MetadataUsageId;
extern const RuntimeType* _EventInfo_t1492620845_0_0_0_var;
extern const uint32_t EventInfo_t_CustomAttributesCacheGenerator_MetadataUsageId;
extern const RuntimeType* _FieldInfo_t3281021298_0_0_0_var;
extern const uint32_t FieldInfo_t_CustomAttributesCacheGenerator_MetadataUsageId;
extern const RuntimeType* _MethodBase_t3352748864_0_0_0_var;
extern const uint32_t MethodBase_t674153939_CustomAttributesCacheGenerator_MetadataUsageId;
extern const RuntimeType* _MethodInfo_t643827776_0_0_0_var;
extern const uint32_t MethodInfo_t_CustomAttributesCacheGenerator_MetadataUsageId;
extern const RuntimeType* _Module_t494242506_0_0_0_var;
extern const uint32_t Module_t2594760207_CustomAttributesCacheGenerator_MetadataUsageId;
extern const RuntimeType* _ParameterInfo_t3152035282_0_0_0_var;
extern const uint32_t ParameterInfo_t3893964560_CustomAttributesCacheGenerator_MetadataUsageId;
extern const RuntimeType* _PropertyInfo_t2517621214_0_0_0_var;
extern const uint32_t PropertyInfo_t_CustomAttributesCacheGenerator_MetadataUsageId;
extern const RuntimeType* Activator_t214179882_0_0_0_var;
extern const uint32_t _Activator_t134942278_CustomAttributesCacheGenerator_MetadataUsageId;
extern const RuntimeType* Assembly_t2665685420_0_0_0_var;
extern const uint32_t _Assembly_t3106968071_CustomAttributesCacheGenerator_MetadataUsageId;
extern const RuntimeType* AssemblyBuilder_t489430578_0_0_0_var;
extern const uint32_t _AssemblyBuilder_t2438890953_CustomAttributesCacheGenerator_MetadataUsageId;
extern const RuntimeType* AssemblyName_t997940415_0_0_0_var;
extern const uint32_t _AssemblyName_t349687559_CustomAttributesCacheGenerator_MetadataUsageId;
extern const RuntimeType* ConstructorBuilder_t1596923965_0_0_0_var;
extern const uint32_t _ConstructorBuilder_t3355697174_CustomAttributesCacheGenerator_MetadataUsageId;
extern const RuntimeType* ConstructorInfo_t3360520918_0_0_0_var;
extern const uint32_t _ConstructorInfo_t1952808443_CustomAttributesCacheGenerator_MetadataUsageId;
extern const RuntimeType* CustomAttributeBuilder_t1432176696_0_0_0_var;
extern const uint32_t _CustomAttributeBuilder_t2651440002_CustomAttributesCacheGenerator_MetadataUsageId;
extern const RuntimeType* EnumBuilder_t617843_0_0_0_var;
extern const uint32_t _EnumBuilder_t3195197362_CustomAttributesCacheGenerator_MetadataUsageId;
extern const RuntimeType* EventBuilder_t2300762438_0_0_0_var;
extern const uint32_t _EventBuilder_t3169829690_CustomAttributesCacheGenerator_MetadataUsageId;
extern const RuntimeType* EventInfo_t_0_0_0_var;
extern const uint32_t _EventInfo_t1492620845_CustomAttributesCacheGenerator_MetadataUsageId;
extern const RuntimeType* FieldBuilder_t803025013_0_0_0_var;
extern const uint32_t _FieldBuilder_t3486190427_CustomAttributesCacheGenerator_MetadataUsageId;
extern const RuntimeType* FieldInfo_t_0_0_0_var;
extern const uint32_t _FieldInfo_t3281021298_CustomAttributesCacheGenerator_MetadataUsageId;
extern const RuntimeType* ILGenerator_t1111467769_0_0_0_var;
extern const uint32_t _ILGenerator_t3086005605_CustomAttributesCacheGenerator_MetadataUsageId;
extern const RuntimeType* MethodBase_t674153939_0_0_0_var;
extern const uint32_t _MethodBase_t3352748864_CustomAttributesCacheGenerator_MetadataUsageId;
extern const RuntimeType* MethodBuilder_t2964638190_0_0_0_var;
extern const uint32_t _MethodBuilder_t195532601_CustomAttributesCacheGenerator_MetadataUsageId;
extern const RuntimeType* MethodInfo_t_0_0_0_var;
extern const uint32_t _MethodInfo_t643827776_CustomAttributesCacheGenerator_MetadataUsageId;
extern const RuntimeType* Module_t2594760207_0_0_0_var;
extern const uint32_t _Module_t494242506_CustomAttributesCacheGenerator_MetadataUsageId;
extern const RuntimeType* ModuleBuilder_t671559400_0_0_0_var;
extern const uint32_t _ModuleBuilder_t3149677118_CustomAttributesCacheGenerator_MetadataUsageId;
extern const RuntimeType* ParameterBuilder_t2478802094_0_0_0_var;
extern const uint32_t _ParameterBuilder_t3730072709_CustomAttributesCacheGenerator_MetadataUsageId;
extern const RuntimeType* ParameterInfo_t3893964560_0_0_0_var;
extern const uint32_t _ParameterInfo_t3152035282_CustomAttributesCacheGenerator_MetadataUsageId;
extern const RuntimeType* PropertyBuilder_t1258013571_0_0_0_var;
extern const uint32_t _PropertyBuilder_t2876596643_CustomAttributesCacheGenerator_MetadataUsageId;
extern const RuntimeType* PropertyInfo_t_0_0_0_var;
extern const uint32_t _PropertyInfo_t2517621214_CustomAttributesCacheGenerator_MetadataUsageId;
extern const RuntimeType* Thread_t2418682337_0_0_0_var;
extern const uint32_t _Thread_t3433524544_CustomAttributesCacheGenerator_MetadataUsageId;
extern const RuntimeType* TypeBuilder_t1663068587_0_0_0_var;
extern const uint32_t _TypeBuilder_t4136446867_CustomAttributesCacheGenerator_MetadataUsageId;
extern const RuntimeType* _Thread_t3433524544_0_0_0_var;
extern const uint32_t Thread_t2418682337_CustomAttributesCacheGenerator_MetadataUsageId;
extern const RuntimeType* _Activator_t134942278_0_0_0_var;
extern const uint32_t Activator_t214179882_CustomAttributesCacheGenerator_MetadataUsageId;
extern const RuntimeType* UriTypeConverter_t3526853594_0_0_0_var;
extern const uint32_t Uri_t1131101833_CustomAttributesCacheGenerator_MetadataUsageId;
extern const RuntimeType* Transform_t94239816_0_0_0_var;
extern const uint32_t Camera_t101201881_CustomAttributesCacheGenerator_MetadataUsageId;
extern const uint32_t GUIElement_t1292848738_CustomAttributesCacheGenerator_MetadataUsageId;
extern const RuntimeType* Camera_t101201881_0_0_0_var;
extern const uint32_t GUILayer_t1929752731_CustomAttributesCacheGenerator_MetadataUsageId;
extern const uint32_t AudioListener_t3360289753_CustomAttributesCacheGenerator_MetadataUsageId;
extern const uint32_t AudioSource_t3827284957_CustomAttributesCacheGenerator_MetadataUsageId;



#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef MEMBERINFO_T_H
#define MEMBERINFO_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MemberInfo
struct  MemberInfo_t  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERINFO_T_H
#ifndef VALUETYPE_T1920584095_H
#define VALUETYPE_T1920584095_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t1920584095  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t1920584095_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t1920584095_marshaled_com
{
};
#endif // VALUETYPE_T1920584095_H
#ifndef ATTRIBUTE_T1659210826_H
#define ATTRIBUTE_T1659210826_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_t1659210826  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_T1659210826_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::length
	int32_t ___length_0;
	// System.Char System.String::start_char
	Il2CppChar ___start_char_1;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(String_t, ___length_0)); }
	inline int32_t get_length_0() const { return ___length_0; }
	inline int32_t* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(int32_t value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_start_char_1() { return static_cast<int32_t>(offsetof(String_t, ___start_char_1)); }
	inline Il2CppChar get_start_char_1() const { return ___start_char_1; }
	inline Il2CppChar* get_address_of_start_char_1() { return &___start_char_1; }
	inline void set_start_char_1(Il2CppChar value)
	{
		___start_char_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_2;
	// System.Char[] System.String::WhiteChars
	CharU5BU5D_t2864810077* ___WhiteChars_3;

public:
	inline static int32_t get_offset_of_Empty_2() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_2)); }
	inline String_t* get_Empty_2() const { return ___Empty_2; }
	inline String_t** get_address_of_Empty_2() { return &___Empty_2; }
	inline void set_Empty_2(String_t* value)
	{
		___Empty_2 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_2), value);
	}

	inline static int32_t get_offset_of_WhiteChars_3() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___WhiteChars_3)); }
	inline CharU5BU5D_t2864810077* get_WhiteChars_3() const { return ___WhiteChars_3; }
	inline CharU5BU5D_t2864810077** get_address_of_WhiteChars_3() { return &___WhiteChars_3; }
	inline void set_WhiteChars_3(CharU5BU5D_t2864810077* value)
	{
		___WhiteChars_3 = value;
		Il2CppCodeGenWriteBarrier((&___WhiteChars_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef ASSEMBLYTITLEATTRIBUTE_T2942119159_H
#define ASSEMBLYTITLEATTRIBUTE_T2942119159_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.AssemblyTitleAttribute
struct  AssemblyTitleAttribute_t2942119159  : public Attribute_t1659210826
{
public:
	// System.String System.Reflection.AssemblyTitleAttribute::name
	String_t* ___name_0;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(AssemblyTitleAttribute_t2942119159, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSEMBLYTITLEATTRIBUTE_T2942119159_H
#ifndef MONOTODOATTRIBUTE_T614642063_H
#define MONOTODOATTRIBUTE_T614642063_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MonoTODOAttribute
struct  MonoTODOAttribute_t614642063  : public Attribute_t1659210826
{
public:
	// System.String System.MonoTODOAttribute::comment
	String_t* ___comment_0;

public:
	inline static int32_t get_offset_of_comment_0() { return static_cast<int32_t>(offsetof(MonoTODOAttribute_t614642063, ___comment_0)); }
	inline String_t* get_comment_0() const { return ___comment_0; }
	inline String_t** get_address_of_comment_0() { return &___comment_0; }
	inline void set_comment_0(String_t* value)
	{
		___comment_0 = value;
		Il2CppCodeGenWriteBarrier((&___comment_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOTODOATTRIBUTE_T614642063_H
#ifndef INTERNALSVISIBLETOATTRIBUTE_T2168674587_H
#define INTERNALSVISIBLETOATTRIBUTE_T2168674587_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.InternalsVisibleToAttribute
struct  InternalsVisibleToAttribute_t2168674587  : public Attribute_t1659210826
{
public:
	// System.String System.Runtime.CompilerServices.InternalsVisibleToAttribute::assemblyName
	String_t* ___assemblyName_0;
	// System.Boolean System.Runtime.CompilerServices.InternalsVisibleToAttribute::all_visible
	bool ___all_visible_1;

public:
	inline static int32_t get_offset_of_assemblyName_0() { return static_cast<int32_t>(offsetof(InternalsVisibleToAttribute_t2168674587, ___assemblyName_0)); }
	inline String_t* get_assemblyName_0() const { return ___assemblyName_0; }
	inline String_t** get_address_of_assemblyName_0() { return &___assemblyName_0; }
	inline void set_assemblyName_0(String_t* value)
	{
		___assemblyName_0 = value;
		Il2CppCodeGenWriteBarrier((&___assemblyName_0), value);
	}

	inline static int32_t get_offset_of_all_visible_1() { return static_cast<int32_t>(offsetof(InternalsVisibleToAttribute_t2168674587, ___all_visible_1)); }
	inline bool get_all_visible_1() const { return ___all_visible_1; }
	inline bool* get_address_of_all_visible_1() { return &___all_visible_1; }
	inline void set_all_visible_1(bool value)
	{
		___all_visible_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERNALSVISIBLETOATTRIBUTE_T2168674587_H
#ifndef THREADSTATICATTRIBUTE_T174455095_H
#define THREADSTATICATTRIBUTE_T174455095_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ThreadStaticAttribute
struct  ThreadStaticAttribute_t174455095  : public Attribute_t1659210826
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THREADSTATICATTRIBUTE_T174455095_H
#ifndef SUPPRESSUNMANAGEDCODESECURITYATTRIBUTE_T1135819281_H
#define SUPPRESSUNMANAGEDCODESECURITYATTRIBUTE_T1135819281_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.SuppressUnmanagedCodeSecurityAttribute
struct  SuppressUnmanagedCodeSecurityAttribute_t1135819281  : public Attribute_t1659210826
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUPPRESSUNMANAGEDCODESECURITYATTRIBUTE_T1135819281_H
#ifndef DEBUGGERSTEPTHROUGHATTRIBUTE_T3823612831_H
#define DEBUGGERSTEPTHROUGHATTRIBUTE_T3823612831_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Diagnostics.DebuggerStepThroughAttribute
struct  DebuggerStepThroughAttribute_t3823612831  : public Attribute_t1659210826
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGGERSTEPTHROUGHATTRIBUTE_T3823612831_H
#ifndef DEBUGGERDISPLAYATTRIBUTE_T2915478477_H
#define DEBUGGERDISPLAYATTRIBUTE_T2915478477_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Diagnostics.DebuggerDisplayAttribute
struct  DebuggerDisplayAttribute_t2915478477  : public Attribute_t1659210826
{
public:
	// System.String System.Diagnostics.DebuggerDisplayAttribute::value
	String_t* ___value_0;
	// System.String System.Diagnostics.DebuggerDisplayAttribute::type
	String_t* ___type_1;
	// System.String System.Diagnostics.DebuggerDisplayAttribute::name
	String_t* ___name_2;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(DebuggerDisplayAttribute_t2915478477, ___value_0)); }
	inline String_t* get_value_0() const { return ___value_0; }
	inline String_t** get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(String_t* value)
	{
		___value_0 = value;
		Il2CppCodeGenWriteBarrier((&___value_0), value);
	}

	inline static int32_t get_offset_of_type_1() { return static_cast<int32_t>(offsetof(DebuggerDisplayAttribute_t2915478477, ___type_1)); }
	inline String_t* get_type_1() const { return ___type_1; }
	inline String_t** get_address_of_type_1() { return &___type_1; }
	inline void set_type_1(String_t* value)
	{
		___type_1 = value;
		Il2CppCodeGenWriteBarrier((&___type_1), value);
	}

	inline static int32_t get_offset_of_name_2() { return static_cast<int32_t>(offsetof(DebuggerDisplayAttribute_t2915478477, ___name_2)); }
	inline String_t* get_name_2() const { return ___name_2; }
	inline String_t** get_address_of_name_2() { return &___name_2; }
	inline void set_name_2(String_t* value)
	{
		___name_2 = value;
		Il2CppCodeGenWriteBarrier((&___name_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGGERDISPLAYATTRIBUTE_T2915478477_H
#ifndef DEBUGGERTYPEPROXYATTRIBUTE_T2114273919_H
#define DEBUGGERTYPEPROXYATTRIBUTE_T2114273919_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Diagnostics.DebuggerTypeProxyAttribute
struct  DebuggerTypeProxyAttribute_t2114273919  : public Attribute_t1659210826
{
public:
	// System.String System.Diagnostics.DebuggerTypeProxyAttribute::proxy_type_name
	String_t* ___proxy_type_name_0;

public:
	inline static int32_t get_offset_of_proxy_type_name_0() { return static_cast<int32_t>(offsetof(DebuggerTypeProxyAttribute_t2114273919, ___proxy_type_name_0)); }
	inline String_t* get_proxy_type_name_0() const { return ___proxy_type_name_0; }
	inline String_t** get_address_of_proxy_type_name_0() { return &___proxy_type_name_0; }
	inline void set_proxy_type_name_0(String_t* value)
	{
		___proxy_type_name_0 = value;
		Il2CppCodeGenWriteBarrier((&___proxy_type_name_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGGERTYPEPROXYATTRIBUTE_T2114273919_H
#ifndef MONOTODOATTRIBUTE_T614642062_H
#define MONOTODOATTRIBUTE_T614642062_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MonoTODOAttribute
struct  MonoTODOAttribute_t614642062  : public Attribute_t1659210826
{
public:
	// System.String System.MonoTODOAttribute::comment
	String_t* ___comment_0;

public:
	inline static int32_t get_offset_of_comment_0() { return static_cast<int32_t>(offsetof(MonoTODOAttribute_t614642062, ___comment_0)); }
	inline String_t* get_comment_0() const { return ___comment_0; }
	inline String_t** get_address_of_comment_0() { return &___comment_0; }
	inline void set_comment_0(String_t* value)
	{
		___comment_0 = value;
		Il2CppCodeGenWriteBarrier((&___comment_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOTODOATTRIBUTE_T614642062_H
#ifndef COMPILERGENERATEDATTRIBUTE_T3076080542_H
#define COMPILERGENERATEDATTRIBUTE_T3076080542_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct  CompilerGeneratedAttribute_t3076080542  : public Attribute_t1659210826
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPILERGENERATEDATTRIBUTE_T3076080542_H
#ifndef DEBUGGERHIDDENATTRIBUTE_T1447627238_H
#define DEBUGGERHIDDENATTRIBUTE_T1447627238_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Diagnostics.DebuggerHiddenAttribute
struct  DebuggerHiddenAttribute_t1447627238  : public Attribute_t1659210826
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGGERHIDDENATTRIBUTE_T1447627238_H
#ifndef OBSOLETEATTRIBUTE_T3443961000_H
#define OBSOLETEATTRIBUTE_T3443961000_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ObsoleteAttribute
struct  ObsoleteAttribute_t3443961000  : public Attribute_t1659210826
{
public:
	// System.String System.ObsoleteAttribute::_message
	String_t* ____message_0;
	// System.Boolean System.ObsoleteAttribute::_error
	bool ____error_1;

public:
	inline static int32_t get_offset_of__message_0() { return static_cast<int32_t>(offsetof(ObsoleteAttribute_t3443961000, ____message_0)); }
	inline String_t* get__message_0() const { return ____message_0; }
	inline String_t** get_address_of__message_0() { return &____message_0; }
	inline void set__message_0(String_t* value)
	{
		____message_0 = value;
		Il2CppCodeGenWriteBarrier((&____message_0), value);
	}

	inline static int32_t get_offset_of__error_1() { return static_cast<int32_t>(offsetof(ObsoleteAttribute_t3443961000, ____error_1)); }
	inline bool get__error_1() const { return ____error_1; }
	inline bool* get_address_of__error_1() { return &____error_1; }
	inline void set__error_1(bool value)
	{
		____error_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBSOLETEATTRIBUTE_T3443961000_H
#ifndef FLAGSATTRIBUTE_T388727259_H
#define FLAGSATTRIBUTE_T388727259_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.FlagsAttribute
struct  FlagsAttribute_t388727259  : public Attribute_t1659210826
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLAGSATTRIBUTE_T388727259_H
#ifndef TYPECONVERTERATTRIBUTE_T553551222_H
#define TYPECONVERTERATTRIBUTE_T553551222_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.TypeConverterAttribute
struct  TypeConverterAttribute_t553551222  : public Attribute_t1659210826
{
public:
	// System.String System.ComponentModel.TypeConverterAttribute::converter_type
	String_t* ___converter_type_1;

public:
	inline static int32_t get_offset_of_converter_type_1() { return static_cast<int32_t>(offsetof(TypeConverterAttribute_t553551222, ___converter_type_1)); }
	inline String_t* get_converter_type_1() const { return ___converter_type_1; }
	inline String_t** get_address_of_converter_type_1() { return &___converter_type_1; }
	inline void set_converter_type_1(String_t* value)
	{
		___converter_type_1 = value;
		Il2CppCodeGenWriteBarrier((&___converter_type_1), value);
	}
};

struct TypeConverterAttribute_t553551222_StaticFields
{
public:
	// System.ComponentModel.TypeConverterAttribute System.ComponentModel.TypeConverterAttribute::Default
	TypeConverterAttribute_t553551222 * ___Default_0;

public:
	inline static int32_t get_offset_of_Default_0() { return static_cast<int32_t>(offsetof(TypeConverterAttribute_t553551222_StaticFields, ___Default_0)); }
	inline TypeConverterAttribute_t553551222 * get_Default_0() const { return ___Default_0; }
	inline TypeConverterAttribute_t553551222 ** get_address_of_Default_0() { return &___Default_0; }
	inline void set_Default_0(TypeConverterAttribute_t553551222 * value)
	{
		___Default_0 = value;
		Il2CppCodeGenWriteBarrier((&___Default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPECONVERTERATTRIBUTE_T553551222_H
#ifndef EXTENSIONATTRIBUTE_T1721836986_H
#define EXTENSIONATTRIBUTE_T1721836986_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.ExtensionAttribute
struct  ExtensionAttribute_t1721836986  : public Attribute_t1659210826
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTENSIONATTRIBUTE_T1721836986_H
#ifndef PARAMARRAYATTRIBUTE_T3330428127_H
#define PARAMARRAYATTRIBUTE_T3330428127_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ParamArrayAttribute
struct  ParamArrayAttribute_t3330428127  : public Attribute_t1659210826
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARAMARRAYATTRIBUTE_T3330428127_H
#ifndef ENUM_T750633987_H
#define ENUM_T750633987_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t750633987  : public ValueType_t1920584095
{
public:

public:
};

struct Enum_t750633987_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t2864810077* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t750633987_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t2864810077* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t2864810077** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t2864810077* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t750633987_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t750633987_marshaled_com
{
};
#endif // ENUM_T750633987_H
#ifndef ADDCOMPONENTMENU_T3529181215_H
#define ADDCOMPONENTMENU_T3529181215_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AddComponentMenu
struct  AddComponentMenu_t3529181215  : public Attribute_t1659210826
{
public:
	// System.String UnityEngine.AddComponentMenu::m_AddComponentMenu
	String_t* ___m_AddComponentMenu_0;
	// System.Int32 UnityEngine.AddComponentMenu::m_Ordering
	int32_t ___m_Ordering_1;

public:
	inline static int32_t get_offset_of_m_AddComponentMenu_0() { return static_cast<int32_t>(offsetof(AddComponentMenu_t3529181215, ___m_AddComponentMenu_0)); }
	inline String_t* get_m_AddComponentMenu_0() const { return ___m_AddComponentMenu_0; }
	inline String_t** get_address_of_m_AddComponentMenu_0() { return &___m_AddComponentMenu_0; }
	inline void set_m_AddComponentMenu_0(String_t* value)
	{
		___m_AddComponentMenu_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_AddComponentMenu_0), value);
	}

	inline static int32_t get_offset_of_m_Ordering_1() { return static_cast<int32_t>(offsetof(AddComponentMenu_t3529181215, ___m_Ordering_1)); }
	inline int32_t get_m_Ordering_1() const { return ___m_Ordering_1; }
	inline int32_t* get_address_of_m_Ordering_1() { return &___m_Ordering_1; }
	inline void set_m_Ordering_1(int32_t value)
	{
		___m_Ordering_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADDCOMPONENTMENU_T3529181215_H
#ifndef SERIALIZEFIELD_T1393754034_H
#define SERIALIZEFIELD_T1393754034_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SerializeField
struct  SerializeField_t1393754034  : public Attribute_t1659210826
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZEFIELD_T1393754034_H
#ifndef FORMERLYSERIALIZEDASATTRIBUTE_T1489703117_H
#define FORMERLYSERIALIZEDASATTRIBUTE_T1489703117_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Serialization.FormerlySerializedAsAttribute
struct  FormerlySerializedAsAttribute_t1489703117  : public Attribute_t1659210826
{
public:
	// System.String UnityEngine.Serialization.FormerlySerializedAsAttribute::m_oldName
	String_t* ___m_oldName_0;

public:
	inline static int32_t get_offset_of_m_oldName_0() { return static_cast<int32_t>(offsetof(FormerlySerializedAsAttribute_t1489703117, ___m_oldName_0)); }
	inline String_t* get_m_oldName_0() const { return ___m_oldName_0; }
	inline String_t** get_address_of_m_oldName_0() { return &___m_oldName_0; }
	inline void set_m_oldName_0(String_t* value)
	{
		___m_oldName_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_oldName_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORMERLYSERIALIZEDASATTRIBUTE_T1489703117_H
#ifndef UNMARSHALLEDATTRIBUTE_T2695814082_H
#define UNMARSHALLEDATTRIBUTE_T2695814082_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Bindings.UnmarshalledAttribute
struct  UnmarshalledAttribute_t2695814082  : public Attribute_t1659210826
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNMARSHALLEDATTRIBUTE_T2695814082_H
#ifndef NATIVECLASSATTRIBUTE_T3085471410_H
#define NATIVECLASSATTRIBUTE_T3085471410_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.NativeClassAttribute
struct  NativeClassAttribute_t3085471410  : public Attribute_t1659210826
{
public:
	// System.String UnityEngine.NativeClassAttribute::<QualifiedNativeName>k__BackingField
	String_t* ___U3CQualifiedNativeNameU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CQualifiedNativeNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(NativeClassAttribute_t3085471410, ___U3CQualifiedNativeNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CQualifiedNativeNameU3Ek__BackingField_0() const { return ___U3CQualifiedNativeNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CQualifiedNativeNameU3Ek__BackingField_0() { return &___U3CQualifiedNativeNameU3Ek__BackingField_0; }
	inline void set_U3CQualifiedNativeNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CQualifiedNativeNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CQualifiedNativeNameU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NATIVECLASSATTRIBUTE_T3085471410_H
#ifndef ALLOWPARTIALLYTRUSTEDCALLERSATTRIBUTE_T2708656030_H
#define ALLOWPARTIALLYTRUSTEDCALLERSATTRIBUTE_T2708656030_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.AllowPartiallyTrustedCallersAttribute
struct  AllowPartiallyTrustedCallersAttribute_t2708656030  : public Attribute_t1659210826
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ALLOWPARTIALLYTRUSTEDCALLERSATTRIBUTE_T2708656030_H
#ifndef DEFAULTVALUEATTRIBUTE_T2693326634_H
#define DEFAULTVALUEATTRIBUTE_T2693326634_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Internal.DefaultValueAttribute
struct  DefaultValueAttribute_t2693326634  : public Attribute_t1659210826
{
public:
	// System.Object UnityEngine.Internal.DefaultValueAttribute::DefaultValue
	RuntimeObject * ___DefaultValue_0;

public:
	inline static int32_t get_offset_of_DefaultValue_0() { return static_cast<int32_t>(offsetof(DefaultValueAttribute_t2693326634, ___DefaultValue_0)); }
	inline RuntimeObject * get_DefaultValue_0() const { return ___DefaultValue_0; }
	inline RuntimeObject ** get_address_of_DefaultValue_0() { return &___DefaultValue_0; }
	inline void set_DefaultValue_0(RuntimeObject * value)
	{
		___DefaultValue_0 = value;
		Il2CppCodeGenWriteBarrier((&___DefaultValue_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTVALUEATTRIBUTE_T2693326634_H
#ifndef REQUIRECOMPONENT_T597737523_H
#define REQUIRECOMPONENT_T597737523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RequireComponent
struct  RequireComponent_t597737523  : public Attribute_t1659210826
{
public:
	// System.Type UnityEngine.RequireComponent::m_Type0
	Type_t * ___m_Type0_0;
	// System.Type UnityEngine.RequireComponent::m_Type1
	Type_t * ___m_Type1_1;
	// System.Type UnityEngine.RequireComponent::m_Type2
	Type_t * ___m_Type2_2;

public:
	inline static int32_t get_offset_of_m_Type0_0() { return static_cast<int32_t>(offsetof(RequireComponent_t597737523, ___m_Type0_0)); }
	inline Type_t * get_m_Type0_0() const { return ___m_Type0_0; }
	inline Type_t ** get_address_of_m_Type0_0() { return &___m_Type0_0; }
	inline void set_m_Type0_0(Type_t * value)
	{
		___m_Type0_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Type0_0), value);
	}

	inline static int32_t get_offset_of_m_Type1_1() { return static_cast<int32_t>(offsetof(RequireComponent_t597737523, ___m_Type1_1)); }
	inline Type_t * get_m_Type1_1() const { return ___m_Type1_1; }
	inline Type_t ** get_address_of_m_Type1_1() { return &___m_Type1_1; }
	inline void set_m_Type1_1(Type_t * value)
	{
		___m_Type1_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Type1_1), value);
	}

	inline static int32_t get_offset_of_m_Type2_2() { return static_cast<int32_t>(offsetof(RequireComponent_t597737523, ___m_Type2_2)); }
	inline Type_t * get_m_Type2_2() const { return ___m_Type2_2; }
	inline Type_t ** get_address_of_m_Type2_2() { return &___m_Type2_2; }
	inline void set_m_Type2_2(Type_t * value)
	{
		___m_Type2_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Type2_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REQUIRECOMPONENT_T597737523_H
#ifndef USEDBYNATIVECODEATTRIBUTE_T2783261251_H
#define USEDBYNATIVECODEATTRIBUTE_T2783261251_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Scripting.UsedByNativeCodeAttribute
struct  UsedByNativeCodeAttribute_t2783261251  : public Attribute_t1659210826
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // USEDBYNATIVECODEATTRIBUTE_T2783261251_H
#ifndef WRITABLEATTRIBUTE_T2474002418_H
#define WRITABLEATTRIBUTE_T2474002418_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.WritableAttribute
struct  WritableAttribute_t2474002418  : public Attribute_t1659210826
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WRITABLEATTRIBUTE_T2474002418_H
#ifndef GENERATEDBYOLDBINDINGSGENERATORATTRIBUTE_T861731860_H
#define GENERATEDBYOLDBINDINGSGENERATORATTRIBUTE_T861731860_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Scripting.GeneratedByOldBindingsGeneratorAttribute
struct  GeneratedByOldBindingsGeneratorAttribute_t861731860  : public Attribute_t1659210826
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERATEDBYOLDBINDINGSGENERATORATTRIBUTE_T861731860_H
#ifndef THREADANDSERIALIZATIONSAFEATTRIBUTE_T566760631_H
#define THREADANDSERIALIZATIONSAFEATTRIBUTE_T566760631_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ThreadAndSerializationSafeAttribute
struct  ThreadAndSerializationSafeAttribute_t566760631  : public Attribute_t1659210826
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THREADANDSERIALIZATIONSAFEATTRIBUTE_T566760631_H
#ifndef REQUIREDBYNATIVECODEATTRIBUTE_T3603024077_H
#define REQUIREDBYNATIVECODEATTRIBUTE_T3603024077_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Scripting.RequiredByNativeCodeAttribute
struct  RequiredByNativeCodeAttribute_t3603024077  : public Attribute_t1659210826
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REQUIREDBYNATIVECODEATTRIBUTE_T3603024077_H
#ifndef SECURITYSAFECRITICALATTRIBUTE_T824959508_H
#define SECURITYSAFECRITICALATTRIBUTE_T824959508_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.SecuritySafeCriticalAttribute
struct  SecuritySafeCriticalAttribute_t824959508  : public Attribute_t1659210826
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECURITYSAFECRITICALATTRIBUTE_T824959508_H
#ifndef DEFAULTMEMBERATTRIBUTE_T2956431904_H
#define DEFAULTMEMBERATTRIBUTE_T2956431904_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.DefaultMemberAttribute
struct  DefaultMemberAttribute_t2956431904  : public Attribute_t1659210826
{
public:
	// System.String System.Reflection.DefaultMemberAttribute::member_name
	String_t* ___member_name_0;

public:
	inline static int32_t get_offset_of_member_name_0() { return static_cast<int32_t>(offsetof(DefaultMemberAttribute_t2956431904, ___member_name_0)); }
	inline String_t* get_member_name_0() const { return ___member_name_0; }
	inline String_t** get_address_of_member_name_0() { return &___member_name_0; }
	inline void set_member_name_0(String_t* value)
	{
		___member_name_0 = value;
		Il2CppCodeGenWriteBarrier((&___member_name_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTMEMBERATTRIBUTE_T2956431904_H
#ifndef DECIMALCONSTANTATTRIBUTE_T200860697_H
#define DECIMALCONSTANTATTRIBUTE_T200860697_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.DecimalConstantAttribute
struct  DecimalConstantAttribute_t200860697  : public Attribute_t1659210826
{
public:
	// System.Byte System.Runtime.CompilerServices.DecimalConstantAttribute::scale
	uint8_t ___scale_0;
	// System.Boolean System.Runtime.CompilerServices.DecimalConstantAttribute::sign
	bool ___sign_1;
	// System.Int32 System.Runtime.CompilerServices.DecimalConstantAttribute::hi
	int32_t ___hi_2;
	// System.Int32 System.Runtime.CompilerServices.DecimalConstantAttribute::mid
	int32_t ___mid_3;
	// System.Int32 System.Runtime.CompilerServices.DecimalConstantAttribute::low
	int32_t ___low_4;

public:
	inline static int32_t get_offset_of_scale_0() { return static_cast<int32_t>(offsetof(DecimalConstantAttribute_t200860697, ___scale_0)); }
	inline uint8_t get_scale_0() const { return ___scale_0; }
	inline uint8_t* get_address_of_scale_0() { return &___scale_0; }
	inline void set_scale_0(uint8_t value)
	{
		___scale_0 = value;
	}

	inline static int32_t get_offset_of_sign_1() { return static_cast<int32_t>(offsetof(DecimalConstantAttribute_t200860697, ___sign_1)); }
	inline bool get_sign_1() const { return ___sign_1; }
	inline bool* get_address_of_sign_1() { return &___sign_1; }
	inline void set_sign_1(bool value)
	{
		___sign_1 = value;
	}

	inline static int32_t get_offset_of_hi_2() { return static_cast<int32_t>(offsetof(DecimalConstantAttribute_t200860697, ___hi_2)); }
	inline int32_t get_hi_2() const { return ___hi_2; }
	inline int32_t* get_address_of_hi_2() { return &___hi_2; }
	inline void set_hi_2(int32_t value)
	{
		___hi_2 = value;
	}

	inline static int32_t get_offset_of_mid_3() { return static_cast<int32_t>(offsetof(DecimalConstantAttribute_t200860697, ___mid_3)); }
	inline int32_t get_mid_3() const { return ___mid_3; }
	inline int32_t* get_address_of_mid_3() { return &___mid_3; }
	inline void set_mid_3(int32_t value)
	{
		___mid_3 = value;
	}

	inline static int32_t get_offset_of_low_4() { return static_cast<int32_t>(offsetof(DecimalConstantAttribute_t200860697, ___low_4)); }
	inline int32_t get_low_4() const { return ___low_4; }
	inline int32_t* get_address_of_low_4() { return &___low_4; }
	inline void set_low_4(int32_t value)
	{
		___low_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DECIMALCONSTANTATTRIBUTE_T200860697_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef TYPELIBVERSIONATTRIBUTE_T1866475684_H
#define TYPELIBVERSIONATTRIBUTE_T1866475684_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.TypeLibVersionAttribute
struct  TypeLibVersionAttribute_t1866475684  : public Attribute_t1659210826
{
public:
	// System.Int32 System.Runtime.InteropServices.TypeLibVersionAttribute::major
	int32_t ___major_0;
	// System.Int32 System.Runtime.InteropServices.TypeLibVersionAttribute::minor
	int32_t ___minor_1;

public:
	inline static int32_t get_offset_of_major_0() { return static_cast<int32_t>(offsetof(TypeLibVersionAttribute_t1866475684, ___major_0)); }
	inline int32_t get_major_0() const { return ___major_0; }
	inline int32_t* get_address_of_major_0() { return &___major_0; }
	inline void set_major_0(int32_t value)
	{
		___major_0 = value;
	}

	inline static int32_t get_offset_of_minor_1() { return static_cast<int32_t>(offsetof(TypeLibVersionAttribute_t1866475684, ___minor_1)); }
	inline int32_t get_minor_1() const { return ___minor_1; }
	inline int32_t* get_address_of_minor_1() { return &___minor_1; }
	inline void set_minor_1(int32_t value)
	{
		___minor_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPELIBVERSIONATTRIBUTE_T1866475684_H
#ifndef DISPIDATTRIBUTE_T3503809048_H
#define DISPIDATTRIBUTE_T3503809048_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.DispIdAttribute
struct  DispIdAttribute_t3503809048  : public Attribute_t1659210826
{
public:
	// System.Int32 System.Runtime.InteropServices.DispIdAttribute::id
	int32_t ___id_0;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(DispIdAttribute_t3503809048, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISPIDATTRIBUTE_T3503809048_H
#ifndef GUIDATTRIBUTE_T3853718442_H
#define GUIDATTRIBUTE_T3853718442_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.GuidAttribute
struct  GuidAttribute_t3853718442  : public Attribute_t1659210826
{
public:
	// System.String System.Runtime.InteropServices.GuidAttribute::guidValue
	String_t* ___guidValue_0;

public:
	inline static int32_t get_offset_of_guidValue_0() { return static_cast<int32_t>(offsetof(GuidAttribute_t3853718442, ___guidValue_0)); }
	inline String_t* get_guidValue_0() const { return ___guidValue_0; }
	inline String_t** get_address_of_guidValue_0() { return &___guidValue_0; }
	inline void set_guidValue_0(String_t* value)
	{
		___guidValue_0 = value;
		Il2CppCodeGenWriteBarrier((&___guidValue_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUIDATTRIBUTE_T3853718442_H
#ifndef ASSEMBLYDESCRIPTIONATTRIBUTE_T1589567422_H
#define ASSEMBLYDESCRIPTIONATTRIBUTE_T1589567422_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.AssemblyDescriptionAttribute
struct  AssemblyDescriptionAttribute_t1589567422  : public Attribute_t1659210826
{
public:
	// System.String System.Reflection.AssemblyDescriptionAttribute::name
	String_t* ___name_0;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(AssemblyDescriptionAttribute_t1589567422, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSEMBLYDESCRIPTIONATTRIBUTE_T1589567422_H
#ifndef COMPILATIONRELAXATIONSATTRIBUTE_T606593683_H
#define COMPILATIONRELAXATIONSATTRIBUTE_T606593683_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct  CompilationRelaxationsAttribute_t606593683  : public Attribute_t1659210826
{
public:
	// System.Int32 System.Runtime.CompilerServices.CompilationRelaxationsAttribute::relax
	int32_t ___relax_0;

public:
	inline static int32_t get_offset_of_relax_0() { return static_cast<int32_t>(offsetof(CompilationRelaxationsAttribute_t606593683, ___relax_0)); }
	inline int32_t get_relax_0() const { return ___relax_0; }
	inline int32_t* get_address_of_relax_0() { return &___relax_0; }
	inline void set_relax_0(int32_t value)
	{
		___relax_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPILATIONRELAXATIONSATTRIBUTE_T606593683_H
#ifndef ASSEMBLYPRODUCTATTRIBUTE_T3365982468_H
#define ASSEMBLYPRODUCTATTRIBUTE_T3365982468_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.AssemblyProductAttribute
struct  AssemblyProductAttribute_t3365982468  : public Attribute_t1659210826
{
public:
	// System.String System.Reflection.AssemblyProductAttribute::name
	String_t* ___name_0;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(AssemblyProductAttribute_t3365982468, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSEMBLYPRODUCTATTRIBUTE_T3365982468_H
#ifndef ASSEMBLYDEFAULTALIASATTRIBUTE_T356814493_H
#define ASSEMBLYDEFAULTALIASATTRIBUTE_T356814493_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.AssemblyDefaultAliasAttribute
struct  AssemblyDefaultAliasAttribute_t356814493  : public Attribute_t1659210826
{
public:
	// System.String System.Reflection.AssemblyDefaultAliasAttribute::name
	String_t* ___name_0;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(AssemblyDefaultAliasAttribute_t356814493, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSEMBLYDEFAULTALIASATTRIBUTE_T356814493_H
#ifndef ASSEMBLYCOMPANYATTRIBUTE_T272143903_H
#define ASSEMBLYCOMPANYATTRIBUTE_T272143903_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.AssemblyCompanyAttribute
struct  AssemblyCompanyAttribute_t272143903  : public Attribute_t1659210826
{
public:
	// System.String System.Reflection.AssemblyCompanyAttribute::name
	String_t* ___name_0;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(AssemblyCompanyAttribute_t272143903, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSEMBLYCOMPANYATTRIBUTE_T272143903_H
#ifndef COMVISIBLEATTRIBUTE_T2289974704_H
#define COMVISIBLEATTRIBUTE_T2289974704_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.ComVisibleAttribute
struct  ComVisibleAttribute_t2289974704  : public Attribute_t1659210826
{
public:
	// System.Boolean System.Runtime.InteropServices.ComVisibleAttribute::Visible
	bool ___Visible_0;

public:
	inline static int32_t get_offset_of_Visible_0() { return static_cast<int32_t>(offsetof(ComVisibleAttribute_t2289974704, ___Visible_0)); }
	inline bool get_Visible_0() const { return ___Visible_0; }
	inline bool* get_address_of_Visible_0() { return &___Visible_0; }
	inline void set_Visible_0(bool value)
	{
		___Visible_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMVISIBLEATTRIBUTE_T2289974704_H
#ifndef ASSEMBLYCOPYRIGHTATTRIBUTE_T1775796795_H
#define ASSEMBLYCOPYRIGHTATTRIBUTE_T1775796795_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.AssemblyCopyrightAttribute
struct  AssemblyCopyrightAttribute_t1775796795  : public Attribute_t1659210826
{
public:
	// System.String System.Reflection.AssemblyCopyrightAttribute::name
	String_t* ___name_0;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(AssemblyCopyrightAttribute_t1775796795, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSEMBLYCOPYRIGHTATTRIBUTE_T1775796795_H
#ifndef ASSEMBLYINFORMATIONALVERSIONATTRIBUTE_T2163289957_H
#define ASSEMBLYINFORMATIONALVERSIONATTRIBUTE_T2163289957_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.AssemblyInformationalVersionAttribute
struct  AssemblyInformationalVersionAttribute_t2163289957  : public Attribute_t1659210826
{
public:
	// System.String System.Reflection.AssemblyInformationalVersionAttribute::name
	String_t* ___name_0;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(AssemblyInformationalVersionAttribute_t2163289957, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSEMBLYINFORMATIONALVERSIONATTRIBUTE_T2163289957_H
#ifndef SATELLITECONTRACTVERSIONATTRIBUTE_T2421164883_H
#define SATELLITECONTRACTVERSIONATTRIBUTE_T2421164883_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Resources.SatelliteContractVersionAttribute
struct  SatelliteContractVersionAttribute_t2421164883  : public Attribute_t1659210826
{
public:
	// System.Version System.Resources.SatelliteContractVersionAttribute::ver
	Version_t900644674 * ___ver_0;

public:
	inline static int32_t get_offset_of_ver_0() { return static_cast<int32_t>(offsetof(SatelliteContractVersionAttribute_t2421164883, ___ver_0)); }
	inline Version_t900644674 * get_ver_0() const { return ___ver_0; }
	inline Version_t900644674 ** get_address_of_ver_0() { return &___ver_0; }
	inline void set_ver_0(Version_t900644674 * value)
	{
		___ver_0 = value;
		Il2CppCodeGenWriteBarrier((&___ver_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SATELLITECONTRACTVERSIONATTRIBUTE_T2421164883_H
#ifndef NEUTRALRESOURCESLANGUAGEATTRIBUTE_T2446220560_H
#define NEUTRALRESOURCESLANGUAGEATTRIBUTE_T2446220560_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Resources.NeutralResourcesLanguageAttribute
struct  NeutralResourcesLanguageAttribute_t2446220560  : public Attribute_t1659210826
{
public:
	// System.String System.Resources.NeutralResourcesLanguageAttribute::culture
	String_t* ___culture_0;

public:
	inline static int32_t get_offset_of_culture_0() { return static_cast<int32_t>(offsetof(NeutralResourcesLanguageAttribute_t2446220560, ___culture_0)); }
	inline String_t* get_culture_0() const { return ___culture_0; }
	inline String_t** get_address_of_culture_0() { return &___culture_0; }
	inline void set_culture_0(String_t* value)
	{
		___culture_0 = value;
		Il2CppCodeGenWriteBarrier((&___culture_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NEUTRALRESOURCESLANGUAGEATTRIBUTE_T2446220560_H
#ifndef CLSCOMPLIANTATTRIBUTE_T2118253839_H
#define CLSCOMPLIANTATTRIBUTE_T2118253839_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.CLSCompliantAttribute
struct  CLSCompliantAttribute_t2118253839  : public Attribute_t1659210826
{
public:
	// System.Boolean System.CLSCompliantAttribute::is_compliant
	bool ___is_compliant_0;

public:
	inline static int32_t get_offset_of_is_compliant_0() { return static_cast<int32_t>(offsetof(CLSCompliantAttribute_t2118253839, ___is_compliant_0)); }
	inline bool get_is_compliant_0() const { return ___is_compliant_0; }
	inline bool* get_address_of_is_compliant_0() { return &___is_compliant_0; }
	inline void set_is_compliant_0(bool value)
	{
		___is_compliant_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLSCOMPLIANTATTRIBUTE_T2118253839_H
#ifndef ASSEMBLYDELAYSIGNATTRIBUTE_T1978111684_H
#define ASSEMBLYDELAYSIGNATTRIBUTE_T1978111684_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.AssemblyDelaySignAttribute
struct  AssemblyDelaySignAttribute_t1978111684  : public Attribute_t1659210826
{
public:
	// System.Boolean System.Reflection.AssemblyDelaySignAttribute::delay
	bool ___delay_0;

public:
	inline static int32_t get_offset_of_delay_0() { return static_cast<int32_t>(offsetof(AssemblyDelaySignAttribute_t1978111684, ___delay_0)); }
	inline bool get_delay_0() const { return ___delay_0; }
	inline bool* get_address_of_delay_0() { return &___delay_0; }
	inline void set_delay_0(bool value)
	{
		___delay_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSEMBLYDELAYSIGNATTRIBUTE_T1978111684_H
#ifndef COMDEFAULTINTERFACEATTRIBUTE_T2399868801_H
#define COMDEFAULTINTERFACEATTRIBUTE_T2399868801_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.ComDefaultInterfaceAttribute
struct  ComDefaultInterfaceAttribute_t2399868801  : public Attribute_t1659210826
{
public:
	// System.Type System.Runtime.InteropServices.ComDefaultInterfaceAttribute::_type
	Type_t * ____type_0;

public:
	inline static int32_t get_offset_of__type_0() { return static_cast<int32_t>(offsetof(ComDefaultInterfaceAttribute_t2399868801, ____type_0)); }
	inline Type_t * get__type_0() const { return ____type_0; }
	inline Type_t ** get_address_of__type_0() { return &____type_0; }
	inline void set__type_0(Type_t * value)
	{
		____type_0 = value;
		Il2CppCodeGenWriteBarrier((&____type_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMDEFAULTINTERFACEATTRIBUTE_T2399868801_H
#ifndef ASSEMBLYKEYFILEATTRIBUTE_T1077071033_H
#define ASSEMBLYKEYFILEATTRIBUTE_T1077071033_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.AssemblyKeyFileAttribute
struct  AssemblyKeyFileAttribute_t1077071033  : public Attribute_t1659210826
{
public:
	// System.String System.Reflection.AssemblyKeyFileAttribute::name
	String_t* ___name_0;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(AssemblyKeyFileAttribute_t1077071033, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSEMBLYKEYFILEATTRIBUTE_T1077071033_H
#ifndef ASSEMBLYFILEVERSIONATTRIBUTE_T3786563949_H
#define ASSEMBLYFILEVERSIONATTRIBUTE_T3786563949_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.AssemblyFileVersionAttribute
struct  AssemblyFileVersionAttribute_t3786563949  : public Attribute_t1659210826
{
public:
	// System.String System.Reflection.AssemblyFileVersionAttribute::name
	String_t* ___name_0;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(AssemblyFileVersionAttribute_t3786563949, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSEMBLYFILEVERSIONATTRIBUTE_T3786563949_H
#ifndef TYPELIBIMPORTCLASSATTRIBUTE_T2173872460_H
#define TYPELIBIMPORTCLASSATTRIBUTE_T2173872460_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.TypeLibImportClassAttribute
struct  TypeLibImportClassAttribute_t2173872460  : public Attribute_t1659210826
{
public:
	// System.String System.Runtime.InteropServices.TypeLibImportClassAttribute::_importClass
	String_t* ____importClass_0;

public:
	inline static int32_t get_offset_of__importClass_0() { return static_cast<int32_t>(offsetof(TypeLibImportClassAttribute_t2173872460, ____importClass_0)); }
	inline String_t* get__importClass_0() const { return ____importClass_0; }
	inline String_t** get_address_of__importClass_0() { return &____importClass_0; }
	inline void set__importClass_0(String_t* value)
	{
		____importClass_0 = value;
		Il2CppCodeGenWriteBarrier((&____importClass_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPELIBIMPORTCLASSATTRIBUTE_T2173872460_H
#ifndef RUNTIMECOMPATIBILITYATTRIBUTE_T38650954_H
#define RUNTIMECOMPATIBILITYATTRIBUTE_T38650954_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct  RuntimeCompatibilityAttribute_t38650954  : public Attribute_t1659210826
{
public:
	// System.Boolean System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::wrap_non_exception_throws
	bool ___wrap_non_exception_throws_0;

public:
	inline static int32_t get_offset_of_wrap_non_exception_throws_0() { return static_cast<int32_t>(offsetof(RuntimeCompatibilityAttribute_t38650954, ___wrap_non_exception_throws_0)); }
	inline bool get_wrap_non_exception_throws_0() const { return ___wrap_non_exception_throws_0; }
	inline bool* get_address_of_wrap_non_exception_throws_0() { return &___wrap_non_exception_throws_0; }
	inline void set_wrap_non_exception_throws_0(bool value)
	{
		___wrap_non_exception_throws_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMECOMPATIBILITYATTRIBUTE_T38650954_H
#ifndef STRINGFREEZINGATTRIBUTE_T4005798349_H
#define STRINGFREEZINGATTRIBUTE_T4005798349_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.StringFreezingAttribute
struct  StringFreezingAttribute_t4005798349  : public Attribute_t1659210826
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGFREEZINGATTRIBUTE_T4005798349_H
#ifndef CALLINGCONVENTION_T324586818_H
#define CALLINGCONVENTION_T324586818_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.CallingConvention
struct  CallingConvention_t324586818 
{
public:
	// System.Int32 System.Runtime.InteropServices.CallingConvention::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CallingConvention_t324586818, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CALLINGCONVENTION_T324586818_H
#ifndef BINDINGFLAGS_T3268090012_H
#define BINDINGFLAGS_T3268090012_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.BindingFlags
struct  BindingFlags_t3268090012 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BindingFlags_t3268090012, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDINGFLAGS_T3268090012_H
#ifndef RUNTIMETYPEHANDLE_T637624852_H
#define RUNTIMETYPEHANDLE_T637624852_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.RuntimeTypeHandle
struct  RuntimeTypeHandle_t637624852 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_t637624852, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMETYPEHANDLE_T637624852_H
#ifndef SECURITYCRITICALSCOPE_T3010960664_H
#define SECURITYCRITICALSCOPE_T3010960664_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.SecurityCriticalScope
struct  SecurityCriticalScope_t3010960664 
{
public:
	// System.Int32 System.Security.SecurityCriticalScope::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SecurityCriticalScope_t3010960664, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECURITYCRITICALSCOPE_T3010960664_H
#ifndef COMINTERFACETYPE_T1249722599_H
#define COMINTERFACETYPE_T1249722599_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.ComInterfaceType
struct  ComInterfaceType_t1249722599 
{
public:
	// System.Int32 System.Runtime.InteropServices.ComInterfaceType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ComInterfaceType_t1249722599, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMINTERFACETYPE_T1249722599_H
#ifndef DEBUGGERBROWSABLESTATE_T329731401_H
#define DEBUGGERBROWSABLESTATE_T329731401_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Diagnostics.DebuggerBrowsableState
struct  DebuggerBrowsableState_t329731401 
{
public:
	// System.Int32 System.Diagnostics.DebuggerBrowsableState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DebuggerBrowsableState_t329731401, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGGERBROWSABLESTATE_T329731401_H
#ifndef MONODOCUMENTATIONNOTEATTRIBUTE_T3720325674_H
#define MONODOCUMENTATIONNOTEATTRIBUTE_T3720325674_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MonoDocumentationNoteAttribute
struct  MonoDocumentationNoteAttribute_t3720325674  : public MonoTODOAttribute_t614642062
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONODOCUMENTATIONNOTEATTRIBUTE_T3720325674_H
#ifndef LOADHINT_T1114342081_H
#define LOADHINT_T1114342081_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.LoadHint
struct  LoadHint_t1114342081 
{
public:
	// System.Int32 System.Runtime.CompilerServices.LoadHint::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LoadHint_t1114342081, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOADHINT_T1114342081_H
#ifndef DEBUGGINGMODES_T1277403995_H
#define DEBUGGINGMODES_T1277403995_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Diagnostics.DebuggableAttribute/DebuggingModes
struct  DebuggingModes_t1277403995 
{
public:
	// System.Int32 System.Diagnostics.DebuggableAttribute/DebuggingModes::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DebuggingModes_t1277403995, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGGINGMODES_T1277403995_H
#ifndef COMPILATIONRELAXATIONS_T3691856549_H
#define COMPILATIONRELAXATIONS_T3691856549_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.CompilationRelaxations
struct  CompilationRelaxations_t3691856549 
{
public:
	// System.Int32 System.Runtime.CompilerServices.CompilationRelaxations::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CompilationRelaxations_t3691856549, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPILATIONRELAXATIONS_T3691856549_H
#ifndef ATTRIBUTETARGETS_T2512976426_H
#define ATTRIBUTETARGETS_T2512976426_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.AttributeTargets
struct  AttributeTargets_t2512976426 
{
public:
	// System.Int32 System.AttributeTargets::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AttributeTargets_t2512976426, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTETARGETS_T2512976426_H
#ifndef CLASSINTERFACETYPE_T3248553764_H
#define CLASSINTERFACETYPE_T3248553764_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.ClassInterfaceType
struct  ClassInterfaceType_t3248553764 
{
public:
	// System.Int32 System.Runtime.InteropServices.ClassInterfaceType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ClassInterfaceType_t3248553764, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLASSINTERFACETYPE_T3248553764_H
#ifndef CONSISTENCY_T1519846396_H
#define CONSISTENCY_T1519846396_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.ConstrainedExecution.Consistency
struct  Consistency_t1519846396 
{
public:
	// System.Int32 System.Runtime.ConstrainedExecution.Consistency::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Consistency_t1519846396, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONSISTENCY_T1519846396_H
#ifndef CER_T4074182961_H
#define CER_T4074182961_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.ConstrainedExecution.Cer
struct  Cer_t4074182961 
{
public:
	// System.Int32 System.Runtime.ConstrainedExecution.Cer::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Cer_t4074182961, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CER_T4074182961_H
#ifndef INTERFACETYPEATTRIBUTE_T3336632297_H
#define INTERFACETYPEATTRIBUTE_T3336632297_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.InterfaceTypeAttribute
struct  InterfaceTypeAttribute_t3336632297  : public Attribute_t1659210826
{
public:
	// System.Runtime.InteropServices.ComInterfaceType System.Runtime.InteropServices.InterfaceTypeAttribute::intType
	int32_t ___intType_0;

public:
	inline static int32_t get_offset_of_intType_0() { return static_cast<int32_t>(offsetof(InterfaceTypeAttribute_t3336632297, ___intType_0)); }
	inline int32_t get_intType_0() const { return ___intType_0; }
	inline int32_t* get_address_of_intType_0() { return &___intType_0; }
	inline void set_intType_0(int32_t value)
	{
		___intType_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERFACETYPEATTRIBUTE_T3336632297_H
#ifndef TYPE_T_H
#define TYPE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Type
struct  Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_t637624852  ____impl_1;

public:
	inline static int32_t get_offset_of__impl_1() { return static_cast<int32_t>(offsetof(Type_t, ____impl_1)); }
	inline RuntimeTypeHandle_t637624852  get__impl_1() const { return ____impl_1; }
	inline RuntimeTypeHandle_t637624852 * get_address_of__impl_1() { return &____impl_1; }
	inline void set__impl_1(RuntimeTypeHandle_t637624852  value)
	{
		____impl_1 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_2;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t1830665827* ___EmptyTypes_3;
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t1197335030 * ___FilterAttribute_4;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t1197335030 * ___FilterName_5;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t1197335030 * ___FilterNameIgnoreCase_6;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_7;

public:
	inline static int32_t get_offset_of_Delimiter_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_2)); }
	inline Il2CppChar get_Delimiter_2() const { return ___Delimiter_2; }
	inline Il2CppChar* get_address_of_Delimiter_2() { return &___Delimiter_2; }
	inline void set_Delimiter_2(Il2CppChar value)
	{
		___Delimiter_2 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_3)); }
	inline TypeU5BU5D_t1830665827* get_EmptyTypes_3() const { return ___EmptyTypes_3; }
	inline TypeU5BU5D_t1830665827** get_address_of_EmptyTypes_3() { return &___EmptyTypes_3; }
	inline void set_EmptyTypes_3(TypeU5BU5D_t1830665827* value)
	{
		___EmptyTypes_3 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyTypes_3), value);
	}

	inline static int32_t get_offset_of_FilterAttribute_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_4)); }
	inline MemberFilter_t1197335030 * get_FilterAttribute_4() const { return ___FilterAttribute_4; }
	inline MemberFilter_t1197335030 ** get_address_of_FilterAttribute_4() { return &___FilterAttribute_4; }
	inline void set_FilterAttribute_4(MemberFilter_t1197335030 * value)
	{
		___FilterAttribute_4 = value;
		Il2CppCodeGenWriteBarrier((&___FilterAttribute_4), value);
	}

	inline static int32_t get_offset_of_FilterName_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_5)); }
	inline MemberFilter_t1197335030 * get_FilterName_5() const { return ___FilterName_5; }
	inline MemberFilter_t1197335030 ** get_address_of_FilterName_5() { return &___FilterName_5; }
	inline void set_FilterName_5(MemberFilter_t1197335030 * value)
	{
		___FilterName_5 = value;
		Il2CppCodeGenWriteBarrier((&___FilterName_5), value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_6)); }
	inline MemberFilter_t1197335030 * get_FilterNameIgnoreCase_6() const { return ___FilterNameIgnoreCase_6; }
	inline MemberFilter_t1197335030 ** get_address_of_FilterNameIgnoreCase_6() { return &___FilterNameIgnoreCase_6; }
	inline void set_FilterNameIgnoreCase_6(MemberFilter_t1197335030 * value)
	{
		___FilterNameIgnoreCase_6 = value;
		Il2CppCodeGenWriteBarrier((&___FilterNameIgnoreCase_6), value);
	}

	inline static int32_t get_offset_of_Missing_7() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_7)); }
	inline RuntimeObject * get_Missing_7() const { return ___Missing_7; }
	inline RuntimeObject ** get_address_of_Missing_7() { return &___Missing_7; }
	inline void set_Missing_7(RuntimeObject * value)
	{
		___Missing_7 = value;
		Il2CppCodeGenWriteBarrier((&___Missing_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T_H
#ifndef ATTRIBUTEUSAGEATTRIBUTE_T2370971113_H
#define ATTRIBUTEUSAGEATTRIBUTE_T2370971113_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.AttributeUsageAttribute
struct  AttributeUsageAttribute_t2370971113  : public Attribute_t1659210826
{
public:
	// System.AttributeTargets System.AttributeUsageAttribute::valid_on
	int32_t ___valid_on_0;
	// System.Boolean System.AttributeUsageAttribute::allow_multiple
	bool ___allow_multiple_1;
	// System.Boolean System.AttributeUsageAttribute::inherited
	bool ___inherited_2;

public:
	inline static int32_t get_offset_of_valid_on_0() { return static_cast<int32_t>(offsetof(AttributeUsageAttribute_t2370971113, ___valid_on_0)); }
	inline int32_t get_valid_on_0() const { return ___valid_on_0; }
	inline int32_t* get_address_of_valid_on_0() { return &___valid_on_0; }
	inline void set_valid_on_0(int32_t value)
	{
		___valid_on_0 = value;
	}

	inline static int32_t get_offset_of_allow_multiple_1() { return static_cast<int32_t>(offsetof(AttributeUsageAttribute_t2370971113, ___allow_multiple_1)); }
	inline bool get_allow_multiple_1() const { return ___allow_multiple_1; }
	inline bool* get_address_of_allow_multiple_1() { return &___allow_multiple_1; }
	inline void set_allow_multiple_1(bool value)
	{
		___allow_multiple_1 = value;
	}

	inline static int32_t get_offset_of_inherited_2() { return static_cast<int32_t>(offsetof(AttributeUsageAttribute_t2370971113, ___inherited_2)); }
	inline bool get_inherited_2() const { return ___inherited_2; }
	inline bool* get_address_of_inherited_2() { return &___inherited_2; }
	inline void set_inherited_2(bool value)
	{
		___inherited_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTEUSAGEATTRIBUTE_T2370971113_H
#ifndef RELIABILITYCONTRACTATTRIBUTE_T3009184403_H
#define RELIABILITYCONTRACTATTRIBUTE_T3009184403_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.ConstrainedExecution.ReliabilityContractAttribute
struct  ReliabilityContractAttribute_t3009184403  : public Attribute_t1659210826
{
public:
	// System.Runtime.ConstrainedExecution.Consistency System.Runtime.ConstrainedExecution.ReliabilityContractAttribute::consistency
	int32_t ___consistency_0;
	// System.Runtime.ConstrainedExecution.Cer System.Runtime.ConstrainedExecution.ReliabilityContractAttribute::cer
	int32_t ___cer_1;

public:
	inline static int32_t get_offset_of_consistency_0() { return static_cast<int32_t>(offsetof(ReliabilityContractAttribute_t3009184403, ___consistency_0)); }
	inline int32_t get_consistency_0() const { return ___consistency_0; }
	inline int32_t* get_address_of_consistency_0() { return &___consistency_0; }
	inline void set_consistency_0(int32_t value)
	{
		___consistency_0 = value;
	}

	inline static int32_t get_offset_of_cer_1() { return static_cast<int32_t>(offsetof(ReliabilityContractAttribute_t3009184403, ___cer_1)); }
	inline int32_t get_cer_1() const { return ___cer_1; }
	inline int32_t* get_address_of_cer_1() { return &___cer_1; }
	inline void set_cer_1(int32_t value)
	{
		___cer_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RELIABILITYCONTRACTATTRIBUTE_T3009184403_H
#ifndef UNMANAGEDFUNCTIONPOINTERATTRIBUTE_T1887822751_H
#define UNMANAGEDFUNCTIONPOINTERATTRIBUTE_T1887822751_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.UnmanagedFunctionPointerAttribute
struct  UnmanagedFunctionPointerAttribute_t1887822751  : public Attribute_t1659210826
{
public:
	// System.Runtime.InteropServices.CallingConvention System.Runtime.InteropServices.UnmanagedFunctionPointerAttribute::call_conv
	int32_t ___call_conv_0;

public:
	inline static int32_t get_offset_of_call_conv_0() { return static_cast<int32_t>(offsetof(UnmanagedFunctionPointerAttribute_t1887822751, ___call_conv_0)); }
	inline int32_t get_call_conv_0() const { return ___call_conv_0; }
	inline int32_t* get_address_of_call_conv_0() { return &___call_conv_0; }
	inline void set_call_conv_0(int32_t value)
	{
		___call_conv_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNMANAGEDFUNCTIONPOINTERATTRIBUTE_T1887822751_H
#ifndef CLASSINTERFACEATTRIBUTE_T1638101999_H
#define CLASSINTERFACEATTRIBUTE_T1638101999_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.ClassInterfaceAttribute
struct  ClassInterfaceAttribute_t1638101999  : public Attribute_t1659210826
{
public:
	// System.Runtime.InteropServices.ClassInterfaceType System.Runtime.InteropServices.ClassInterfaceAttribute::ciType
	int32_t ___ciType_0;

public:
	inline static int32_t get_offset_of_ciType_0() { return static_cast<int32_t>(offsetof(ClassInterfaceAttribute_t1638101999, ___ciType_0)); }
	inline int32_t get_ciType_0() const { return ___ciType_0; }
	inline int32_t* get_address_of_ciType_0() { return &___ciType_0; }
	inline void set_ciType_0(int32_t value)
	{
		___ciType_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLASSINTERFACEATTRIBUTE_T1638101999_H
#ifndef SECURITYCRITICALATTRIBUTE_T3270381382_H
#define SECURITYCRITICALATTRIBUTE_T3270381382_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.SecurityCriticalAttribute
struct  SecurityCriticalAttribute_t3270381382  : public Attribute_t1659210826
{
public:
	// System.Security.SecurityCriticalScope System.Security.SecurityCriticalAttribute::_scope
	int32_t ____scope_0;

public:
	inline static int32_t get_offset_of__scope_0() { return static_cast<int32_t>(offsetof(SecurityCriticalAttribute_t3270381382, ____scope_0)); }
	inline int32_t get__scope_0() const { return ____scope_0; }
	inline int32_t* get_address_of__scope_0() { return &____scope_0; }
	inline void set__scope_0(int32_t value)
	{
		____scope_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECURITYCRITICALATTRIBUTE_T3270381382_H
#ifndef DEBUGGERBROWSABLEATTRIBUTE_T1527328379_H
#define DEBUGGERBROWSABLEATTRIBUTE_T1527328379_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Diagnostics.DebuggerBrowsableAttribute
struct  DebuggerBrowsableAttribute_t1527328379  : public Attribute_t1659210826
{
public:
	// System.Diagnostics.DebuggerBrowsableState System.Diagnostics.DebuggerBrowsableAttribute::state
	int32_t ___state_0;

public:
	inline static int32_t get_offset_of_state_0() { return static_cast<int32_t>(offsetof(DebuggerBrowsableAttribute_t1527328379, ___state_0)); }
	inline int32_t get_state_0() const { return ___state_0; }
	inline int32_t* get_address_of_state_0() { return &___state_0; }
	inline void set_state_0(int32_t value)
	{
		___state_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGGERBROWSABLEATTRIBUTE_T1527328379_H
#ifndef DEFAULTDEPENDENCYATTRIBUTE_T2058824218_H
#define DEFAULTDEPENDENCYATTRIBUTE_T2058824218_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.DefaultDependencyAttribute
struct  DefaultDependencyAttribute_t2058824218  : public Attribute_t1659210826
{
public:
	// System.Runtime.CompilerServices.LoadHint System.Runtime.CompilerServices.DefaultDependencyAttribute::hint
	int32_t ___hint_0;

public:
	inline static int32_t get_offset_of_hint_0() { return static_cast<int32_t>(offsetof(DefaultDependencyAttribute_t2058824218, ___hint_0)); }
	inline int32_t get_hint_0() const { return ___hint_0; }
	inline int32_t* get_address_of_hint_0() { return &___hint_0; }
	inline void set_hint_0(int32_t value)
	{
		___hint_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTDEPENDENCYATTRIBUTE_T2058824218_H
#ifndef DEBUGGABLEATTRIBUTE_T3717120398_H
#define DEBUGGABLEATTRIBUTE_T3717120398_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Diagnostics.DebuggableAttribute
struct  DebuggableAttribute_t3717120398  : public Attribute_t1659210826
{
public:
	// System.Boolean System.Diagnostics.DebuggableAttribute::JITTrackingEnabledFlag
	bool ___JITTrackingEnabledFlag_0;
	// System.Boolean System.Diagnostics.DebuggableAttribute::JITOptimizerDisabledFlag
	bool ___JITOptimizerDisabledFlag_1;
	// System.Diagnostics.DebuggableAttribute/DebuggingModes System.Diagnostics.DebuggableAttribute::debuggingModes
	int32_t ___debuggingModes_2;

public:
	inline static int32_t get_offset_of_JITTrackingEnabledFlag_0() { return static_cast<int32_t>(offsetof(DebuggableAttribute_t3717120398, ___JITTrackingEnabledFlag_0)); }
	inline bool get_JITTrackingEnabledFlag_0() const { return ___JITTrackingEnabledFlag_0; }
	inline bool* get_address_of_JITTrackingEnabledFlag_0() { return &___JITTrackingEnabledFlag_0; }
	inline void set_JITTrackingEnabledFlag_0(bool value)
	{
		___JITTrackingEnabledFlag_0 = value;
	}

	inline static int32_t get_offset_of_JITOptimizerDisabledFlag_1() { return static_cast<int32_t>(offsetof(DebuggableAttribute_t3717120398, ___JITOptimizerDisabledFlag_1)); }
	inline bool get_JITOptimizerDisabledFlag_1() const { return ___JITOptimizerDisabledFlag_1; }
	inline bool* get_address_of_JITOptimizerDisabledFlag_1() { return &___JITOptimizerDisabledFlag_1; }
	inline void set_JITOptimizerDisabledFlag_1(bool value)
	{
		___JITOptimizerDisabledFlag_1 = value;
	}

	inline static int32_t get_offset_of_debuggingModes_2() { return static_cast<int32_t>(offsetof(DebuggableAttribute_t3717120398, ___debuggingModes_2)); }
	inline int32_t get_debuggingModes_2() const { return ___debuggingModes_2; }
	inline int32_t* get_address_of_debuggingModes_2() { return &___debuggingModes_2; }
	inline void set_debuggingModes_2(int32_t value)
	{
		___debuggingModes_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGGABLEATTRIBUTE_T3717120398_H



// System.Void System.Reflection.AssemblyTitleAttribute::.ctor(System.String)
extern "C"  void AssemblyTitleAttribute__ctor_m4091976430 (AssemblyTitleAttribute_t2942119159 * __this, String_t* ___title0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.AssemblyFileVersionAttribute::.ctor(System.String)
extern "C"  void AssemblyFileVersionAttribute__ctor_m4281768118 (AssemblyFileVersionAttribute_t3786563949 * __this, String_t* ___version0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.AssemblyKeyFileAttribute::.ctor(System.String)
extern "C"  void AssemblyKeyFileAttribute__ctor_m1870834162 (AssemblyKeyFileAttribute_t1077071033 * __this, String_t* ___keyFile0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.AssemblyDelaySignAttribute::.ctor(System.Boolean)
extern "C"  void AssemblyDelaySignAttribute__ctor_m3802049740 (AssemblyDelaySignAttribute_t1978111684 * __this, bool ___delaySign0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.CLSCompliantAttribute::.ctor(System.Boolean)
extern "C"  void CLSCompliantAttribute__ctor_m3885160237 (CLSCompliantAttribute_t2118253839 * __this, bool ___isCompliant0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Resources.NeutralResourcesLanguageAttribute::.ctor(System.String)
extern "C"  void NeutralResourcesLanguageAttribute__ctor_m1815010898 (NeutralResourcesLanguageAttribute_t2446220560 * __this, String_t* ___cultureName0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Resources.SatelliteContractVersionAttribute::.ctor(System.String)
extern "C"  void SatelliteContractVersionAttribute__ctor_m4270552604 (SatelliteContractVersionAttribute_t2421164883 * __this, String_t* ___version0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.AssemblyInformationalVersionAttribute::.ctor(System.String)
extern "C"  void AssemblyInformationalVersionAttribute__ctor_m1415206487 (AssemblyInformationalVersionAttribute_t2163289957 * __this, String_t* ___informationalVersion0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.AssemblyCopyrightAttribute::.ctor(System.String)
extern "C"  void AssemblyCopyrightAttribute__ctor_m4282510565 (AssemblyCopyrightAttribute_t1775796795 * __this, String_t* ___copyright0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.ComVisibleAttribute::.ctor(System.Boolean)
extern "C"  void ComVisibleAttribute__ctor_m29339631 (ComVisibleAttribute_t2289974704 * __this, bool ___visibility0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.AssemblyDefaultAliasAttribute::.ctor(System.String)
extern "C"  void AssemblyDefaultAliasAttribute__ctor_m393492554 (AssemblyDefaultAliasAttribute_t356814493 * __this, String_t* ___defaultAlias0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.AssemblyDescriptionAttribute::.ctor(System.String)
extern "C"  void AssemblyDescriptionAttribute__ctor_m3502566481 (AssemblyDescriptionAttribute_t1589567422 * __this, String_t* ___description0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.GuidAttribute::.ctor(System.String)
extern "C"  void GuidAttribute__ctor_m612200210 (GuidAttribute_t3853718442 * __this, String_t* ___guid0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.CompilerServices.StringFreezingAttribute::.ctor()
extern "C"  void StringFreezingAttribute__ctor_m1275687280 (StringFreezingAttribute_t4005798349 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.CompilerServices.DefaultDependencyAttribute::.ctor(System.Runtime.CompilerServices.LoadHint)
extern "C"  void DefaultDependencyAttribute__ctor_m552191618 (DefaultDependencyAttribute_t2058824218 * __this, int32_t ___loadHintArgument0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::.ctor()
extern "C"  void RuntimeCompatibilityAttribute__ctor_m853651104 (RuntimeCompatibilityAttribute_t38650954 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::set_WrapNonExceptionThrows(System.Boolean)
extern "C"  void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m4154715430 (RuntimeCompatibilityAttribute_t38650954 * __this, bool ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.TypeLibVersionAttribute::.ctor(System.Int32,System.Int32)
extern "C"  void TypeLibVersionAttribute__ctor_m3431410536 (TypeLibVersionAttribute_t1866475684 * __this, int32_t ___major0, int32_t ___minor1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Diagnostics.DebuggableAttribute::.ctor(System.Diagnostics.DebuggableAttribute/DebuggingModes)
extern "C"  void DebuggableAttribute__ctor_m3553871532 (DebuggableAttribute_t3717120398 * __this, int32_t ___modes0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.CompilerServices.CompilationRelaxationsAttribute::.ctor(System.Runtime.CompilerServices.CompilationRelaxations)
extern "C"  void CompilationRelaxationsAttribute__ctor_m1638540989 (CompilationRelaxationsAttribute_t606593683 * __this, int32_t ___relaxations0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.AssemblyProductAttribute::.ctor(System.String)
extern "C"  void AssemblyProductAttribute__ctor_m2217606340 (AssemblyProductAttribute_t3365982468 * __this, String_t* ___product0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.AssemblyCompanyAttribute::.ctor(System.String)
extern "C"  void AssemblyCompanyAttribute__ctor_m1315167307 (AssemblyCompanyAttribute_t272143903 * __this, String_t* ___company0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.ClassInterfaceAttribute::.ctor(System.Runtime.InteropServices.ClassInterfaceType)
extern "C"  void ClassInterfaceAttribute__ctor_m2759037627 (ClassInterfaceAttribute_t1638101999 * __this, int32_t ___classInterfaceType0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.ConstrainedExecution.ReliabilityContractAttribute::.ctor(System.Runtime.ConstrainedExecution.Consistency,System.Runtime.ConstrainedExecution.Cer)
extern "C"  void ReliabilityContractAttribute__ctor_m2586077688 (ReliabilityContractAttribute_t3009184403 * __this, int32_t ___consistencyGuarantee0, int32_t ___cer1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.ComDefaultInterfaceAttribute::.ctor(System.Type)
extern "C"  void ComDefaultInterfaceAttribute__ctor_m1937741735 (ComDefaultInterfaceAttribute_t2399868801 * __this, Type_t * ___defaultInterface0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.AttributeUsageAttribute::.ctor(System.AttributeTargets)
extern "C"  void AttributeUsageAttribute__ctor_m1170715599 (AttributeUsageAttribute_t2370971113 * __this, int32_t ___validOn0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.TypeLibImportClassAttribute::.ctor(System.Type)
extern "C"  void TypeLibImportClassAttribute__ctor_m1502349925 (TypeLibImportClassAttribute_t2173872460 * __this, Type_t * ___importClass0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.InterfaceTypeAttribute::.ctor(System.Runtime.InteropServices.ComInterfaceType)
extern "C"  void InterfaceTypeAttribute__ctor_m342415882 (InterfaceTypeAttribute_t3336632297 * __this, int32_t ___interfaceType0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.AttributeUsageAttribute::set_Inherited(System.Boolean)
extern "C"  void AttributeUsageAttribute_set_Inherited_m2205070461 (AttributeUsageAttribute_t2370971113 * __this, bool ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.AttributeUsageAttribute::set_AllowMultiple(System.Boolean)
extern "C"  void AttributeUsageAttribute_set_AllowMultiple_m1313295265 (AttributeUsageAttribute_t2370971113 * __this, bool ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.DispIdAttribute::.ctor(System.Int32)
extern "C"  void DispIdAttribute__ctor_m2341845790 (DispIdAttribute_t3503809048 * __this, int32_t ___dispId0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.DefaultMemberAttribute::.ctor(System.String)
extern "C"  void DefaultMemberAttribute__ctor_m3129895323 (DefaultMemberAttribute_t2956431904 * __this, String_t* ___memberName0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.ParamArrayAttribute::.ctor()
extern "C"  void ParamArrayAttribute__ctor_m1696407647 (ParamArrayAttribute_t3330428127 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.MonoDocumentationNoteAttribute::.ctor(System.String)
extern "C"  void MonoDocumentationNoteAttribute__ctor_m3324629758 (MonoDocumentationNoteAttribute_t3720325674 * __this, String_t* ___comment0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.CompilerServices.DecimalConstantAttribute::.ctor(System.Byte,System.Byte,System.UInt32,System.UInt32,System.UInt32)
extern "C"  void DecimalConstantAttribute__ctor_m579883733 (DecimalConstantAttribute_t200860697 * __this, uint8_t ___scale0, uint8_t ___sign1, uint32_t ___hi2, uint32_t ___mid3, uint32_t ___low4, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.ObsoleteAttribute::.ctor(System.String)
extern "C"  void ObsoleteAttribute__ctor_m3274828507 (ObsoleteAttribute_t3443961000 * __this, String_t* ___message0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Diagnostics.DebuggerHiddenAttribute::.ctor()
extern "C"  void DebuggerHiddenAttribute__ctor_m749702160 (DebuggerHiddenAttribute_t1447627238 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.CompilerServices.CompilerGeneratedAttribute::.ctor()
extern "C"  void CompilerGeneratedAttribute__ctor_m903969540 (CompilerGeneratedAttribute_t3076080542 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.MonoTODOAttribute::.ctor(System.String)
extern "C"  void MonoTODOAttribute__ctor_m2689597370 (MonoTODOAttribute_t614642062 * __this, String_t* ___comment0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Diagnostics.DebuggerDisplayAttribute::.ctor(System.String)
extern "C"  void DebuggerDisplayAttribute__ctor_m685030886 (DebuggerDisplayAttribute_t2915478477 * __this, String_t* ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Diagnostics.DebuggerTypeProxyAttribute::.ctor(System.Type)
extern "C"  void DebuggerTypeProxyAttribute__ctor_m92548023 (DebuggerTypeProxyAttribute_t2114273919 * __this, Type_t * ___type0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Diagnostics.DebuggerDisplayAttribute::set_Name(System.String)
extern "C"  void DebuggerDisplayAttribute_set_Name_m2283484547 (DebuggerDisplayAttribute_t2915478477 * __this, String_t* ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.FlagsAttribute::.ctor()
extern "C"  void FlagsAttribute__ctor_m3834565173 (FlagsAttribute_t388727259 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.MonoTODOAttribute::.ctor()
extern "C"  void MonoTODOAttribute__ctor_m1347054473 (MonoTODOAttribute_t614642062 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Diagnostics.DebuggerStepThroughAttribute::.ctor()
extern "C"  void DebuggerStepThroughAttribute__ctor_m1431182322 (DebuggerStepThroughAttribute_t3823612831 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.SuppressUnmanagedCodeSecurityAttribute::.ctor()
extern "C"  void SuppressUnmanagedCodeSecurityAttribute__ctor_m1831174959 (SuppressUnmanagedCodeSecurityAttribute_t1135819281 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.ThreadStaticAttribute::.ctor()
extern "C"  void ThreadStaticAttribute__ctor_m778228724 (ThreadStaticAttribute_t174455095 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.ObsoleteAttribute::.ctor()
extern "C"  void ObsoleteAttribute__ctor_m1879591032 (ObsoleteAttribute_t3443961000 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.CompilerServices.InternalsVisibleToAttribute::.ctor(System.String)
extern "C"  void InternalsVisibleToAttribute__ctor_m2209294008 (InternalsVisibleToAttribute_t2168674587 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.MonoTODOAttribute::.ctor()
extern "C"  void MonoTODOAttribute__ctor_m423555603 (MonoTODOAttribute_t614642063 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.ObsoleteAttribute::.ctor(System.String,System.Boolean)
extern "C"  void ObsoleteAttribute__ctor_m1055576761 (ObsoleteAttribute_t3443961000 * __this, String_t* p0, bool p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.MonoTODOAttribute::.ctor(System.String)
extern "C"  void MonoTODOAttribute__ctor_m2017426088 (MonoTODOAttribute_t614642063 * __this, String_t* ___comment0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.ComponentModel.TypeConverterAttribute::.ctor(System.Type)
extern "C"  void TypeConverterAttribute__ctor_m2796508157 (TypeConverterAttribute_t553551222 * __this, Type_t * ___type0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.SecurityCriticalAttribute::.ctor()
extern "C"  void SecurityCriticalAttribute__ctor_m248669408 (SecurityCriticalAttribute_t3270381382 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AllowPartiallyTrustedCallersAttribute::.ctor()
extern "C"  void AllowPartiallyTrustedCallersAttribute__ctor_m1393518081 (AllowPartiallyTrustedCallersAttribute_t2708656030 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.CompilerServices.ExtensionAttribute::.ctor()
extern "C"  void ExtensionAttribute__ctor_m831032830 (ExtensionAttribute_t1721836986 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Diagnostics.DebuggerBrowsableAttribute::.ctor(System.Diagnostics.DebuggerBrowsableState)
extern "C"  void DebuggerBrowsableAttribute__ctor_m1784875164 (DebuggerBrowsableAttribute_t1527328379 * __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Scripting.RequiredByNativeCodeAttribute::.ctor()
extern "C"  void RequiredByNativeCodeAttribute__ctor_m3493315205 (RequiredByNativeCodeAttribute_t3603024077 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ThreadAndSerializationSafeAttribute::.ctor()
extern "C"  void ThreadAndSerializationSafeAttribute__ctor_m211248632 (ThreadAndSerializationSafeAttribute_t566760631 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Scripting.GeneratedByOldBindingsGeneratorAttribute::.ctor()
extern "C"  void GeneratedByOldBindingsGeneratorAttribute__ctor_m1819602998 (GeneratedByOldBindingsGeneratorAttribute_t861731860 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WritableAttribute::.ctor()
extern "C"  void WritableAttribute__ctor_m474281766 (WritableAttribute_t2474002418 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Scripting.UsedByNativeCodeAttribute::.ctor()
extern "C"  void UsedByNativeCodeAttribute__ctor_m4001769068 (UsedByNativeCodeAttribute_t2783261251 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RequireComponent::.ctor(System.Type)
extern "C"  void RequireComponent__ctor_m3242490219 (RequireComponent_t597737523 * __this, Type_t * ___requiredComponent0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.SecuritySafeCriticalAttribute::.ctor()
extern "C"  void SecuritySafeCriticalAttribute__ctor_m1135903908 (SecuritySafeCriticalAttribute_t824959508 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Internal.DefaultValueAttribute::.ctor(System.String)
extern "C"  void DefaultValueAttribute__ctor_m2061771615 (DefaultValueAttribute_t2693326634 * __this, String_t* ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.NativeClassAttribute::.ctor(System.String)
extern "C"  void NativeClassAttribute__ctor_m714604756 (NativeClassAttribute_t3085471410 * __this, String_t* ___qualifiedCppName0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Bindings.UnmarshalledAttribute::.ctor()
extern "C"  void UnmarshalledAttribute__ctor_m3412418881 (UnmarshalledAttribute_t2695814082 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Serialization.FormerlySerializedAsAttribute::.ctor(System.String)
extern "C"  void FormerlySerializedAsAttribute__ctor_m219264990 (FormerlySerializedAsAttribute_t1489703117 * __this, String_t* ___oldName0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SerializeField::.ctor()
extern "C"  void SerializeField__ctor_m3401225370 (SerializeField_t1393754034 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.UnmanagedFunctionPointerAttribute::.ctor(System.Runtime.InteropServices.CallingConvention)
extern "C"  void UnmanagedFunctionPointerAttribute__ctor_m323663599 (UnmanagedFunctionPointerAttribute_t1887822751 * __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AddComponentMenu::.ctor(System.String)
extern "C"  void AddComponentMenu__ctor_m3629709883 (AddComponentMenu_t3529181215 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
static void g_mscorlib_Assembly_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AssemblyTitleAttribute_t2942119159 * tmp = (AssemblyTitleAttribute_t2942119159 *)cache->attributes[0];
		AssemblyTitleAttribute__ctor_m4091976430(tmp, il2cpp_codegen_string_new_wrapper("mscorlib.dll"), NULL);
	}
	{
		AssemblyFileVersionAttribute_t3786563949 * tmp = (AssemblyFileVersionAttribute_t3786563949 *)cache->attributes[1];
		AssemblyFileVersionAttribute__ctor_m4281768118(tmp, il2cpp_codegen_string_new_wrapper("2.0.50727.1433"), NULL);
	}
	{
		AssemblyKeyFileAttribute_t1077071033 * tmp = (AssemblyKeyFileAttribute_t1077071033 *)cache->attributes[2];
		AssemblyKeyFileAttribute__ctor_m1870834162(tmp, il2cpp_codegen_string_new_wrapper("../ecma.pub"), NULL);
	}
	{
		AssemblyDelaySignAttribute_t1978111684 * tmp = (AssemblyDelaySignAttribute_t1978111684 *)cache->attributes[3];
		AssemblyDelaySignAttribute__ctor_m3802049740(tmp, true, NULL);
	}
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[4];
		CLSCompliantAttribute__ctor_m3885160237(tmp, true, NULL);
	}
	{
		NeutralResourcesLanguageAttribute_t2446220560 * tmp = (NeutralResourcesLanguageAttribute_t2446220560 *)cache->attributes[5];
		NeutralResourcesLanguageAttribute__ctor_m1815010898(tmp, il2cpp_codegen_string_new_wrapper("en-US"), NULL);
	}
	{
		SatelliteContractVersionAttribute_t2421164883 * tmp = (SatelliteContractVersionAttribute_t2421164883 *)cache->attributes[6];
		SatelliteContractVersionAttribute__ctor_m4270552604(tmp, il2cpp_codegen_string_new_wrapper("2.0.0.0"), NULL);
	}
	{
		AssemblyInformationalVersionAttribute_t2163289957 * tmp = (AssemblyInformationalVersionAttribute_t2163289957 *)cache->attributes[7];
		AssemblyInformationalVersionAttribute__ctor_m1415206487(tmp, il2cpp_codegen_string_new_wrapper("2.0.50727.1433"), NULL);
	}
	{
		AssemblyCopyrightAttribute_t1775796795 * tmp = (AssemblyCopyrightAttribute_t1775796795 *)cache->attributes[8];
		AssemblyCopyrightAttribute__ctor_m4282510565(tmp, il2cpp_codegen_string_new_wrapper("(c) various MONO Authors"), NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[9];
		ComVisibleAttribute__ctor_m29339631(tmp, false, NULL);
	}
	{
		AssemblyDefaultAliasAttribute_t356814493 * tmp = (AssemblyDefaultAliasAttribute_t356814493 *)cache->attributes[10];
		AssemblyDefaultAliasAttribute__ctor_m393492554(tmp, il2cpp_codegen_string_new_wrapper("mscorlib.dll"), NULL);
	}
	{
		AssemblyDescriptionAttribute_t1589567422 * tmp = (AssemblyDescriptionAttribute_t1589567422 *)cache->attributes[11];
		AssemblyDescriptionAttribute__ctor_m3502566481(tmp, il2cpp_codegen_string_new_wrapper("mscorlib.dll"), NULL);
	}
	{
		GuidAttribute_t3853718442 * tmp = (GuidAttribute_t3853718442 *)cache->attributes[12];
		GuidAttribute__ctor_m612200210(tmp, il2cpp_codegen_string_new_wrapper("BED7F4EA-1A96-11D2-8F08-00A0C9A6186D"), NULL);
	}
	{
		StringFreezingAttribute_t4005798349 * tmp = (StringFreezingAttribute_t4005798349 *)cache->attributes[13];
		StringFreezingAttribute__ctor_m1275687280(tmp, NULL);
	}
	{
		DefaultDependencyAttribute_t2058824218 * tmp = (DefaultDependencyAttribute_t2058824218 *)cache->attributes[14];
		DefaultDependencyAttribute__ctor_m552191618(tmp, 1LL, NULL);
	}
	{
		RuntimeCompatibilityAttribute_t38650954 * tmp = (RuntimeCompatibilityAttribute_t38650954 *)cache->attributes[15];
		RuntimeCompatibilityAttribute__ctor_m853651104(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m4154715430(tmp, true, NULL);
	}
	{
		TypeLibVersionAttribute_t1866475684 * tmp = (TypeLibVersionAttribute_t1866475684 *)cache->attributes[16];
		TypeLibVersionAttribute__ctor_m3431410536(tmp, 2LL, 0LL, NULL);
	}
	{
		DebuggableAttribute_t3717120398 * tmp = (DebuggableAttribute_t3717120398 *)cache->attributes[17];
		DebuggableAttribute__ctor_m3553871532(tmp, 2LL, NULL);
	}
	{
		CompilationRelaxationsAttribute_t606593683 * tmp = (CompilationRelaxationsAttribute_t606593683 *)cache->attributes[18];
		CompilationRelaxationsAttribute__ctor_m1638540989(tmp, 8LL, NULL);
	}
	{
		AssemblyProductAttribute_t3365982468 * tmp = (AssemblyProductAttribute_t3365982468 *)cache->attributes[19];
		AssemblyProductAttribute__ctor_m2217606340(tmp, il2cpp_codegen_string_new_wrapper("MONO Common language infrastructure"), NULL);
	}
	{
		AssemblyCompanyAttribute_t272143903 * tmp = (AssemblyCompanyAttribute_t272143903 *)cache->attributes[20];
		AssemblyCompanyAttribute__ctor_m1315167307(tmp, il2cpp_codegen_string_new_wrapper("MONO development team"), NULL);
	}
}
static void RuntimeObject_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		ClassInterfaceAttribute_t1638101999 * tmp = (ClassInterfaceAttribute_t1638101999 *)cache->attributes[1];
		ClassInterfaceAttribute__ctor_m2759037627(tmp, 2LL, NULL);
	}
}
static void RuntimeObject_CustomAttributesCacheGenerator_Object__ctor_m3446193457(CustomAttributesCache* cache)
{
	{
		ReliabilityContractAttribute_t3009184403 * tmp = (ReliabilityContractAttribute_t3009184403 *)cache->attributes[0];
		ReliabilityContractAttribute__ctor_m2586077688(tmp, 3LL, 1LL, NULL);
	}
}
static void RuntimeObject_CustomAttributesCacheGenerator_Object_Finalize_m1722074356(CustomAttributesCache* cache)
{
	{
		ReliabilityContractAttribute_t3009184403 * tmp = (ReliabilityContractAttribute_t3009184403 *)cache->attributes[0];
		ReliabilityContractAttribute__ctor_m2586077688(tmp, 3LL, 2LL, NULL);
	}
}
static void RuntimeObject_CustomAttributesCacheGenerator_Object_ReferenceEquals_m1923645570(CustomAttributesCache* cache)
{
	{
		ReliabilityContractAttribute_t3009184403 * tmp = (ReliabilityContractAttribute_t3009184403 *)cache->attributes[0];
		ReliabilityContractAttribute__ctor_m2586077688(tmp, 3LL, 2LL, NULL);
	}
}
static void ValueType_t1920584095_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void Attribute_t1659210826_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Attribute_t1659210826_CustomAttributesCacheGenerator_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		ComDefaultInterfaceAttribute_t2399868801 * tmp = (ComDefaultInterfaceAttribute_t2399868801 *)cache->attributes[1];
		ComDefaultInterfaceAttribute__ctor_m1937741735(tmp, il2cpp_codegen_type_get_object(_Attribute_t480711065_0_0_0_var), NULL);
	}
	{
		AttributeUsageAttribute_t2370971113 * tmp = (AttributeUsageAttribute_t2370971113 *)cache->attributes[2];
		AttributeUsageAttribute__ctor_m1170715599(tmp, 32767LL, NULL);
	}
	{
		ClassInterfaceAttribute_t1638101999 * tmp = (ClassInterfaceAttribute_t1638101999 *)cache->attributes[3];
		ClassInterfaceAttribute__ctor_m2759037627(tmp, 0LL, NULL);
	}
}
static void _Attribute_t480711065_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_Attribute_t480711065_CustomAttributesCacheGenerator_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		TypeLibImportClassAttribute_t2173872460 * tmp = (TypeLibImportClassAttribute_t2173872460 *)cache->attributes[0];
		TypeLibImportClassAttribute__ctor_m1502349925(tmp, il2cpp_codegen_type_get_object(Attribute_t1659210826_0_0_0_var), NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[1];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[2];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
	{
		InterfaceTypeAttribute_t3336632297 * tmp = (InterfaceTypeAttribute_t3336632297 *)cache->attributes[3];
		InterfaceTypeAttribute__ctor_m342415882(tmp, 1LL, NULL);
	}
	{
		GuidAttribute_t3853718442 * tmp = (GuidAttribute_t3853718442 *)cache->attributes[4];
		GuidAttribute__ctor_m612200210(tmp, il2cpp_codegen_string_new_wrapper("917B14D0-2D9E-38B8-92A9-381ACF52F7C0"), NULL);
	}
}
static void Int32_t2571135199_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void IFormattable_t1418953097_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void IConvertible_t1224795492_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[1];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void IComparable_t63485110_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void SerializableAttribute_t2180742604_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_t2370971113 * tmp = (AttributeUsageAttribute_t2370971113 *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m1170715599(tmp, 4124LL, NULL);
		AttributeUsageAttribute_set_Inherited_m2205070461(tmp, false, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m1313295265(tmp, false, NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[1];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void AttributeUsageAttribute_t2370971113_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		AttributeUsageAttribute_t2370971113 * tmp = (AttributeUsageAttribute_t2370971113 *)cache->attributes[1];
		AttributeUsageAttribute__ctor_m1170715599(tmp, 4LL, NULL);
	}
}
static void ComVisibleAttribute_t2289974704_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_t2370971113 * tmp = (AttributeUsageAttribute_t2370971113 *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m1170715599(tmp, 5597LL, NULL);
		AttributeUsageAttribute_set_Inherited_m2205070461(tmp, false, NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[1];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void Int64_t384086013_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void UInt32_t2739056616_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[1];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void UInt32_t2739056616_CustomAttributesCacheGenerator_UInt32_Parse_m51087447(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void UInt32_t2739056616_CustomAttributesCacheGenerator_UInt32_Parse_m1290187410(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void UInt32_t2739056616_CustomAttributesCacheGenerator_UInt32_TryParse_m3247026924(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void UInt32_t2739056616_CustomAttributesCacheGenerator_UInt32_TryParse_m1105936683(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void CLSCompliantAttribute_t2118253839_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		AttributeUsageAttribute_t2370971113 * tmp = (AttributeUsageAttribute_t2370971113 *)cache->attributes[1];
		AttributeUsageAttribute__ctor_m1170715599(tmp, 32767LL, NULL);
	}
}
static void UInt64_t2265270229_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[1];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void UInt64_t2265270229_CustomAttributesCacheGenerator_UInt64_Parse_m1110924678(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void UInt64_t2265270229_CustomAttributesCacheGenerator_UInt64_Parse_m1872577878(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void UInt64_t2265270229_CustomAttributesCacheGenerator_UInt64_TryParse_m1628442159(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void Byte_t880488320_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void SByte_t2530277047_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[1];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void SByte_t2530277047_CustomAttributesCacheGenerator_SByte_Parse_m3790253308(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void SByte_t2530277047_CustomAttributesCacheGenerator_SByte_Parse_m1842460049(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void SByte_t2530277047_CustomAttributesCacheGenerator_SByte_TryParse_m3168549138(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void Int16_t769030278_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void UInt16_t11709673_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[1];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void UInt16_t11709673_CustomAttributesCacheGenerator_UInt16_Parse_m1722685482(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void UInt16_t11709673_CustomAttributesCacheGenerator_UInt16_Parse_m1446859916(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void UInt16_t11709673_CustomAttributesCacheGenerator_UInt16_TryParse_m78750700(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void UInt16_t11709673_CustomAttributesCacheGenerator_UInt16_TryParse_m209638213(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void IEnumerator_t4262330110_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		GuidAttribute_t3853718442 * tmp = (GuidAttribute_t3853718442 *)cache->attributes[0];
		GuidAttribute__ctor_m612200210(tmp, il2cpp_codegen_string_new_wrapper("496B0ABF-CDEE-11D3-88E8-00902754C43A"), NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[1];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void IEnumerable_t1872631827_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		GuidAttribute_t3853718442 * tmp = (GuidAttribute_t3853718442 *)cache->attributes[1];
		GuidAttribute__ctor_m612200210(tmp, il2cpp_codegen_string_new_wrapper("496B0ABE-CDEE-11d3-88E8-00902754C43A"), NULL);
	}
}
static void IEnumerable_t1872631827_CustomAttributesCacheGenerator_IEnumerable_GetEnumerator_m2081137055(CustomAttributesCache* cache)
{
	{
		DispIdAttribute_t3503809048 * tmp = (DispIdAttribute_t3503809048 *)cache->attributes[0];
		DispIdAttribute__ctor_m2341845790(tmp, -4LL, NULL);
	}
}
static void IDisposable_t318870991_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void Char_t3572527572_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void String_t_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		DefaultMemberAttribute_t2956431904 * tmp = (DefaultMemberAttribute_t2956431904 *)cache->attributes[1];
		DefaultMemberAttribute__ctor_m3129895323(tmp, il2cpp_codegen_string_new_wrapper("Chars"), NULL);
	}
}
static void String_t_CustomAttributesCacheGenerator_String__ctor_m1254661065(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void String_t_CustomAttributesCacheGenerator_String_Equals_m1448975066(CustomAttributesCache* cache)
{
	{
		ReliabilityContractAttribute_t3009184403 * tmp = (ReliabilityContractAttribute_t3009184403 *)cache->attributes[0];
		ReliabilityContractAttribute__ctor_m2586077688(tmp, 3LL, 1LL, NULL);
	}
}
static void String_t_CustomAttributesCacheGenerator_String_Equals_m672522368(CustomAttributesCache* cache)
{
	{
		ReliabilityContractAttribute_t3009184403 * tmp = (ReliabilityContractAttribute_t3009184403 *)cache->attributes[0];
		ReliabilityContractAttribute__ctor_m2586077688(tmp, 3LL, 1LL, NULL);
	}
}
static void String_t_CustomAttributesCacheGenerator_String_Split_m4016464379____separator0(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t3330428127 * tmp = (ParamArrayAttribute_t3330428127 *)cache->attributes[0];
		ParamArrayAttribute__ctor_m1696407647(tmp, NULL);
	}
}
static void String_t_CustomAttributesCacheGenerator_String_Split_m2753730040(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, false, NULL);
	}
	{
		MonoDocumentationNoteAttribute_t3720325674 * tmp = (MonoDocumentationNoteAttribute_t3720325674 *)cache->attributes[1];
		MonoDocumentationNoteAttribute__ctor_m3324629758(tmp, il2cpp_codegen_string_new_wrapper("code should be moved to managed"), NULL);
	}
}
static void String_t_CustomAttributesCacheGenerator_String_Split_m2680706933(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, false, NULL);
	}
}
static void String_t_CustomAttributesCacheGenerator_String_Split_m993848168(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, false, NULL);
	}
}
static void String_t_CustomAttributesCacheGenerator_String_Trim_m361890761____trimChars0(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t3330428127 * tmp = (ParamArrayAttribute_t3330428127 *)cache->attributes[0];
		ParamArrayAttribute__ctor_m1696407647(tmp, NULL);
	}
}
static void String_t_CustomAttributesCacheGenerator_String_TrimStart_m4243963028____trimChars0(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t3330428127 * tmp = (ParamArrayAttribute_t3330428127 *)cache->attributes[0];
		ParamArrayAttribute__ctor_m1696407647(tmp, NULL);
	}
}
static void String_t_CustomAttributesCacheGenerator_String_TrimEnd_m498525724____trimChars0(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t3330428127 * tmp = (ParamArrayAttribute_t3330428127 *)cache->attributes[0];
		ParamArrayAttribute__ctor_m1696407647(tmp, NULL);
	}
}
static void String_t_CustomAttributesCacheGenerator_String_Format_m2496254338____args1(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t3330428127 * tmp = (ParamArrayAttribute_t3330428127 *)cache->attributes[0];
		ParamArrayAttribute__ctor_m1696407647(tmp, NULL);
	}
}
static void String_t_CustomAttributesCacheGenerator_String_Format_m3845522687____args2(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t3330428127 * tmp = (ParamArrayAttribute_t3330428127 *)cache->attributes[0];
		ParamArrayAttribute__ctor_m1696407647(tmp, NULL);
	}
}
static void String_t_CustomAttributesCacheGenerator_String_FormatHelper_m2208728312____args3(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t3330428127 * tmp = (ParamArrayAttribute_t3330428127 *)cache->attributes[0];
		ParamArrayAttribute__ctor_m1696407647(tmp, NULL);
	}
}
static void String_t_CustomAttributesCacheGenerator_String_Concat_m2733933624____args0(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t3330428127 * tmp = (ParamArrayAttribute_t3330428127 *)cache->attributes[0];
		ParamArrayAttribute__ctor_m1696407647(tmp, NULL);
	}
}
static void String_t_CustomAttributesCacheGenerator_String_Concat_m2040094846____values0(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t3330428127 * tmp = (ParamArrayAttribute_t3330428127 *)cache->attributes[0];
		ParamArrayAttribute__ctor_m1696407647(tmp, NULL);
	}
}
static void String_t_CustomAttributesCacheGenerator_String_GetHashCode_m3205775290(CustomAttributesCache* cache)
{
	{
		ReliabilityContractAttribute_t3009184403 * tmp = (ReliabilityContractAttribute_t3009184403 *)cache->attributes[0];
		ReliabilityContractAttribute__ctor_m2586077688(tmp, 3LL, 1LL, NULL);
	}
}
static void ICloneable_t3230708396_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void Single_t496865882_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void Single_t496865882_CustomAttributesCacheGenerator_Single_IsNaN_m1192835548(CustomAttributesCache* cache)
{
	{
		ReliabilityContractAttribute_t3009184403 * tmp = (ReliabilityContractAttribute_t3009184403 *)cache->attributes[0];
		ReliabilityContractAttribute__ctor_m2586077688(tmp, 3LL, 2LL, NULL);
	}
}
static void Double_t1454265465_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void Double_t1454265465_CustomAttributesCacheGenerator_Double_IsNaN_m2498995017(CustomAttributesCache* cache)
{
	{
		ReliabilityContractAttribute_t3009184403 * tmp = (ReliabilityContractAttribute_t3009184403 *)cache->attributes[0];
		ReliabilityContractAttribute__ctor_m2586077688(tmp, 3LL, 2LL, NULL);
	}
}
static void Decimal_t2656901032_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void Decimal_t2656901032_CustomAttributesCacheGenerator_MinValue(CustomAttributesCache* cache)
{
	{
		DecimalConstantAttribute_t200860697 * tmp = (DecimalConstantAttribute_t200860697 *)cache->attributes[0];
		DecimalConstantAttribute__ctor_m579883733(tmp, 0, 255, 4294967295LL, 4294967295LL, 4294967295LL, NULL);
	}
}
static void Decimal_t2656901032_CustomAttributesCacheGenerator_MaxValue(CustomAttributesCache* cache)
{
	{
		DecimalConstantAttribute_t200860697 * tmp = (DecimalConstantAttribute_t200860697 *)cache->attributes[0];
		DecimalConstantAttribute__ctor_m579883733(tmp, 0, 0, 4294967295LL, 4294967295LL, 4294967295LL, NULL);
	}
}
static void Decimal_t2656901032_CustomAttributesCacheGenerator_MinusOne(CustomAttributesCache* cache)
{
	{
		DecimalConstantAttribute_t200860697 * tmp = (DecimalConstantAttribute_t200860697 *)cache->attributes[0];
		DecimalConstantAttribute__ctor_m579883733(tmp, 0, 255, 0LL, 0LL, 1LL, NULL);
	}
}
static void Decimal_t2656901032_CustomAttributesCacheGenerator_One(CustomAttributesCache* cache)
{
	{
		DecimalConstantAttribute_t200860697 * tmp = (DecimalConstantAttribute_t200860697 *)cache->attributes[0];
		DecimalConstantAttribute__ctor_m579883733(tmp, 0, 0, 0LL, 0LL, 1LL, NULL);
	}
}
static void Decimal_t2656901032_CustomAttributesCacheGenerator_Decimal__ctor_m2684736918(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void Decimal_t2656901032_CustomAttributesCacheGenerator_Decimal__ctor_m700533170(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void Decimal_t2656901032_CustomAttributesCacheGenerator_Decimal_Compare_m4142387002(CustomAttributesCache* cache)
{
	{
		ReliabilityContractAttribute_t3009184403 * tmp = (ReliabilityContractAttribute_t3009184403 *)cache->attributes[0];
		ReliabilityContractAttribute__ctor_m2586077688(tmp, 3LL, 2LL, NULL);
	}
}
static void Decimal_t2656901032_CustomAttributesCacheGenerator_Decimal_op_Explicit_m2905558026(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void Decimal_t2656901032_CustomAttributesCacheGenerator_Decimal_op_Explicit_m1941964535(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void Decimal_t2656901032_CustomAttributesCacheGenerator_Decimal_op_Explicit_m801735887(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void Decimal_t2656901032_CustomAttributesCacheGenerator_Decimal_op_Explicit_m3225117811(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void Decimal_t2656901032_CustomAttributesCacheGenerator_Decimal_op_Implicit_m1059519657(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void Decimal_t2656901032_CustomAttributesCacheGenerator_Decimal_op_Implicit_m2347208944(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void Decimal_t2656901032_CustomAttributesCacheGenerator_Decimal_op_Implicit_m74114740(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void Decimal_t2656901032_CustomAttributesCacheGenerator_Decimal_op_Implicit_m2277896146(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void Boolean_t2059672899_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void IntPtr_t_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void IntPtr_t_CustomAttributesCacheGenerator_IntPtr__ctor_m1874125153(CustomAttributesCache* cache)
{
	{
		ReliabilityContractAttribute_t3009184403 * tmp = (ReliabilityContractAttribute_t3009184403 *)cache->attributes[0];
		ReliabilityContractAttribute__ctor_m2586077688(tmp, 2LL, 1LL, NULL);
	}
}
static void IntPtr_t_CustomAttributesCacheGenerator_IntPtr__ctor_m1846124433(CustomAttributesCache* cache)
{
	{
		ReliabilityContractAttribute_t3009184403 * tmp = (ReliabilityContractAttribute_t3009184403 *)cache->attributes[0];
		ReliabilityContractAttribute__ctor_m2586077688(tmp, 2LL, 1LL, NULL);
	}
}
static void IntPtr_t_CustomAttributesCacheGenerator_IntPtr__ctor_m3020263568(CustomAttributesCache* cache)
{
	{
		ReliabilityContractAttribute_t3009184403 * tmp = (ReliabilityContractAttribute_t3009184403 *)cache->attributes[0];
		ReliabilityContractAttribute__ctor_m2586077688(tmp, 2LL, 1LL, NULL);
	}
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[1];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void IntPtr_t_CustomAttributesCacheGenerator_IntPtr_get_Size_m3603629221(CustomAttributesCache* cache)
{
	{
		ReliabilityContractAttribute_t3009184403 * tmp = (ReliabilityContractAttribute_t3009184403 *)cache->attributes[0];
		ReliabilityContractAttribute__ctor_m2586077688(tmp, 3LL, 2LL, NULL);
	}
}
static void IntPtr_t_CustomAttributesCacheGenerator_IntPtr_ToInt64_m3776154157(CustomAttributesCache* cache)
{
	{
		ReliabilityContractAttribute_t3009184403 * tmp = (ReliabilityContractAttribute_t3009184403 *)cache->attributes[0];
		ReliabilityContractAttribute__ctor_m2586077688(tmp, 3LL, 2LL, NULL);
	}
}
static void IntPtr_t_CustomAttributesCacheGenerator_IntPtr_ToPointer_m565950407(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
	{
		ReliabilityContractAttribute_t3009184403 * tmp = (ReliabilityContractAttribute_t3009184403 *)cache->attributes[1];
		ReliabilityContractAttribute__ctor_m2586077688(tmp, 3LL, 2LL, NULL);
	}
}
static void IntPtr_t_CustomAttributesCacheGenerator_IntPtr_op_Equality_m3978210203(CustomAttributesCache* cache)
{
	{
		ReliabilityContractAttribute_t3009184403 * tmp = (ReliabilityContractAttribute_t3009184403 *)cache->attributes[0];
		ReliabilityContractAttribute__ctor_m2586077688(tmp, 3LL, 2LL, NULL);
	}
}
static void IntPtr_t_CustomAttributesCacheGenerator_IntPtr_op_Inequality_m1374147269(CustomAttributesCache* cache)
{
	{
		ReliabilityContractAttribute_t3009184403 * tmp = (ReliabilityContractAttribute_t3009184403 *)cache->attributes[0];
		ReliabilityContractAttribute__ctor_m2586077688(tmp, 3LL, 2LL, NULL);
	}
}
static void IntPtr_t_CustomAttributesCacheGenerator_IntPtr_op_Explicit_m3440174878(CustomAttributesCache* cache)
{
	{
		ReliabilityContractAttribute_t3009184403 * tmp = (ReliabilityContractAttribute_t3009184403 *)cache->attributes[0];
		ReliabilityContractAttribute__ctor_m2586077688(tmp, 2LL, 1LL, NULL);
	}
}
static void IntPtr_t_CustomAttributesCacheGenerator_IntPtr_op_Explicit_m1287079606(CustomAttributesCache* cache)
{
	{
		ReliabilityContractAttribute_t3009184403 * tmp = (ReliabilityContractAttribute_t3009184403 *)cache->attributes[0];
		ReliabilityContractAttribute__ctor_m2586077688(tmp, 2LL, 1LL, NULL);
	}
}
static void IntPtr_t_CustomAttributesCacheGenerator_IntPtr_op_Explicit_m913523000(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
	{
		ReliabilityContractAttribute_t3009184403 * tmp = (ReliabilityContractAttribute_t3009184403 *)cache->attributes[1];
		ReliabilityContractAttribute__ctor_m2586077688(tmp, 2LL, 1LL, NULL);
	}
}
static void IntPtr_t_CustomAttributesCacheGenerator_IntPtr_op_Explicit_m3354255807(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void ISerializable_t631501906_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void UIntPtr_t_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[1];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void MulticastDelegate_t69367374_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void Delegate_t96267039_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ClassInterfaceAttribute_t1638101999 * tmp = (ClassInterfaceAttribute_t1638101999 *)cache->attributes[0];
		ClassInterfaceAttribute__ctor_m2759037627(tmp, 2LL, NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[1];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void Enum_t750633987_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void Enum_t750633987_CustomAttributesCacheGenerator_Enum_GetName_m2871384883(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void Enum_t750633987_CustomAttributesCacheGenerator_Enum_IsDefined_m1555923783(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void Enum_t750633987_CustomAttributesCacheGenerator_Enum_GetUnderlyingType_m4160021701(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void Enum_t750633987_CustomAttributesCacheGenerator_Enum_Parse_m1598758293(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void Enum_t750633987_CustomAttributesCacheGenerator_Enum_ToString_m3329141164(CustomAttributesCache* cache)
{
	{
		ObsoleteAttribute_t3443961000 * tmp = (ObsoleteAttribute_t3443961000 *)cache->attributes[0];
		ObsoleteAttribute__ctor_m3274828507(tmp, il2cpp_codegen_string_new_wrapper("Provider is ignored, just use ToString"), NULL);
	}
}
static void Enum_t750633987_CustomAttributesCacheGenerator_Enum_ToString_m3343977455(CustomAttributesCache* cache)
{
	{
		ObsoleteAttribute_t3443961000 * tmp = (ObsoleteAttribute_t3443961000 *)cache->attributes[0];
		ObsoleteAttribute__ctor_m3274828507(tmp, il2cpp_codegen_string_new_wrapper("Provider is ignored, just use ToString"), NULL);
	}
}
static void Enum_t750633987_CustomAttributesCacheGenerator_Enum_ToObject_m412986634(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void Enum_t750633987_CustomAttributesCacheGenerator_Enum_ToObject_m1409448066(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void Enum_t750633987_CustomAttributesCacheGenerator_Enum_ToObject_m1790622089(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void Enum_t750633987_CustomAttributesCacheGenerator_Enum_ToObject_m3209825685(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void Enum_t750633987_CustomAttributesCacheGenerator_Enum_ToObject_m1546957196(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void Enum_t750633987_CustomAttributesCacheGenerator_Enum_ToObject_m2176493389(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[1];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void Enum_t750633987_CustomAttributesCacheGenerator_Enum_ToObject_m363118936(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[1];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void Enum_t750633987_CustomAttributesCacheGenerator_Enum_ToObject_m1273833310(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[1];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void Enum_t750633987_CustomAttributesCacheGenerator_Enum_ToObject_m2284871884(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[1];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void Enum_t750633987_CustomAttributesCacheGenerator_Enum_Format_m402722146(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void RuntimeArray_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void RuntimeArray_CustomAttributesCacheGenerator_Array_System_Collections_IList_IndexOf_m523778419(CustomAttributesCache* cache)
{
	{
		ReliabilityContractAttribute_t3009184403 * tmp = (ReliabilityContractAttribute_t3009184403 *)cache->attributes[0];
		ReliabilityContractAttribute__ctor_m2586077688(tmp, 3LL, 1LL, NULL);
	}
}
static void RuntimeArray_CustomAttributesCacheGenerator_Array_get_Length_m1201869234(CustomAttributesCache* cache)
{
	{
		ReliabilityContractAttribute_t3009184403 * tmp = (ReliabilityContractAttribute_t3009184403 *)cache->attributes[0];
		ReliabilityContractAttribute__ctor_m2586077688(tmp, 3LL, 2LL, NULL);
	}
}
static void RuntimeArray_CustomAttributesCacheGenerator_Array_get_LongLength_m63201949(CustomAttributesCache* cache)
{
	{
		ReliabilityContractAttribute_t3009184403 * tmp = (ReliabilityContractAttribute_t3009184403 *)cache->attributes[0];
		ReliabilityContractAttribute__ctor_m2586077688(tmp, 3LL, 2LL, NULL);
	}
}
static void RuntimeArray_CustomAttributesCacheGenerator_Array_get_Rank_m3141701998(CustomAttributesCache* cache)
{
	{
		ReliabilityContractAttribute_t3009184403 * tmp = (ReliabilityContractAttribute_t3009184403 *)cache->attributes[0];
		ReliabilityContractAttribute__ctor_m2586077688(tmp, 3LL, 2LL, NULL);
	}
}
static void RuntimeArray_CustomAttributesCacheGenerator_Array_GetLongLength_m2596545479(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, false, NULL);
	}
}
static void RuntimeArray_CustomAttributesCacheGenerator_Array_GetLowerBound_m817811428(CustomAttributesCache* cache)
{
	{
		ReliabilityContractAttribute_t3009184403 * tmp = (ReliabilityContractAttribute_t3009184403 *)cache->attributes[0];
		ReliabilityContractAttribute__ctor_m2586077688(tmp, 3LL, 2LL, NULL);
	}
}
static void RuntimeArray_CustomAttributesCacheGenerator_Array_GetValue_m1494120325____indices0(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t3330428127 * tmp = (ParamArrayAttribute_t3330428127 *)cache->attributes[0];
		ParamArrayAttribute__ctor_m1696407647(tmp, NULL);
	}
}
static void RuntimeArray_CustomAttributesCacheGenerator_Array_SetValue_m3927045595____indices1(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t3330428127 * tmp = (ParamArrayAttribute_t3330428127 *)cache->attributes[0];
		ParamArrayAttribute__ctor_m1696407647(tmp, NULL);
	}
}
static void RuntimeArray_CustomAttributesCacheGenerator_Array_GetUpperBound_m152106182(CustomAttributesCache* cache)
{
	{
		ReliabilityContractAttribute_t3009184403 * tmp = (ReliabilityContractAttribute_t3009184403 *)cache->attributes[0];
		ReliabilityContractAttribute__ctor_m2586077688(tmp, 3LL, 2LL, NULL);
	}
}
static void RuntimeArray_CustomAttributesCacheGenerator_Array_GetValue_m1430766714(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, false, NULL);
	}
}
static void RuntimeArray_CustomAttributesCacheGenerator_Array_GetValue_m3155219782(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, false, NULL);
	}
}
static void RuntimeArray_CustomAttributesCacheGenerator_Array_GetValue_m217652931(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, false, NULL);
	}
}
static void RuntimeArray_CustomAttributesCacheGenerator_Array_SetValue_m1145170836(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, false, NULL);
	}
}
static void RuntimeArray_CustomAttributesCacheGenerator_Array_SetValue_m3597386468(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, false, NULL);
	}
}
static void RuntimeArray_CustomAttributesCacheGenerator_Array_SetValue_m1904644573(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, false, NULL);
	}
}
static void RuntimeArray_CustomAttributesCacheGenerator_Array_CreateInstance_m666131206____lengths1(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t3330428127 * tmp = (ParamArrayAttribute_t3330428127 *)cache->attributes[0];
		ParamArrayAttribute__ctor_m1696407647(tmp, NULL);
	}
}
static void RuntimeArray_CustomAttributesCacheGenerator_Array_CreateInstance_m3882898988____lengths1(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t3330428127 * tmp = (ParamArrayAttribute_t3330428127 *)cache->attributes[0];
		ParamArrayAttribute__ctor_m1696407647(tmp, NULL);
	}
}
static void RuntimeArray_CustomAttributesCacheGenerator_Array_GetValue_m3754306304(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, false, NULL);
	}
}
static void RuntimeArray_CustomAttributesCacheGenerator_Array_GetValue_m3754306304____indices0(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t3330428127 * tmp = (ParamArrayAttribute_t3330428127 *)cache->attributes[0];
		ParamArrayAttribute__ctor_m1696407647(tmp, NULL);
	}
}
static void RuntimeArray_CustomAttributesCacheGenerator_Array_SetValue_m4108726102(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, false, NULL);
	}
}
static void RuntimeArray_CustomAttributesCacheGenerator_Array_SetValue_m4108726102____indices1(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t3330428127 * tmp = (ParamArrayAttribute_t3330428127 *)cache->attributes[0];
		ParamArrayAttribute__ctor_m1696407647(tmp, NULL);
	}
}
static void RuntimeArray_CustomAttributesCacheGenerator_Array_BinarySearch_m2098844084(CustomAttributesCache* cache)
{
	{
		ReliabilityContractAttribute_t3009184403 * tmp = (ReliabilityContractAttribute_t3009184403 *)cache->attributes[0];
		ReliabilityContractAttribute__ctor_m2586077688(tmp, 3LL, 1LL, NULL);
	}
}
static void RuntimeArray_CustomAttributesCacheGenerator_Array_BinarySearch_m1298651580(CustomAttributesCache* cache)
{
	{
		ReliabilityContractAttribute_t3009184403 * tmp = (ReliabilityContractAttribute_t3009184403 *)cache->attributes[0];
		ReliabilityContractAttribute__ctor_m2586077688(tmp, 3LL, 1LL, NULL);
	}
}
static void RuntimeArray_CustomAttributesCacheGenerator_Array_BinarySearch_m2500870911(CustomAttributesCache* cache)
{
	{
		ReliabilityContractAttribute_t3009184403 * tmp = (ReliabilityContractAttribute_t3009184403 *)cache->attributes[0];
		ReliabilityContractAttribute__ctor_m2586077688(tmp, 3LL, 1LL, NULL);
	}
}
static void RuntimeArray_CustomAttributesCacheGenerator_Array_BinarySearch_m1629142740(CustomAttributesCache* cache)
{
	{
		ReliabilityContractAttribute_t3009184403 * tmp = (ReliabilityContractAttribute_t3009184403 *)cache->attributes[0];
		ReliabilityContractAttribute__ctor_m2586077688(tmp, 3LL, 1LL, NULL);
	}
}
static void RuntimeArray_CustomAttributesCacheGenerator_Array_Clear_m305290227(CustomAttributesCache* cache)
{
	{
		ReliabilityContractAttribute_t3009184403 * tmp = (ReliabilityContractAttribute_t3009184403 *)cache->attributes[0];
		ReliabilityContractAttribute__ctor_m2586077688(tmp, 3LL, 2LL, NULL);
	}
}
static void RuntimeArray_CustomAttributesCacheGenerator_Array_Copy_m392395275(CustomAttributesCache* cache)
{
	{
		ReliabilityContractAttribute_t3009184403 * tmp = (ReliabilityContractAttribute_t3009184403 *)cache->attributes[0];
		ReliabilityContractAttribute__ctor_m2586077688(tmp, 2LL, 1LL, NULL);
	}
}
static void RuntimeArray_CustomAttributesCacheGenerator_Array_Copy_m1287631566(CustomAttributesCache* cache)
{
	{
		ReliabilityContractAttribute_t3009184403 * tmp = (ReliabilityContractAttribute_t3009184403 *)cache->attributes[0];
		ReliabilityContractAttribute__ctor_m2586077688(tmp, 2LL, 1LL, NULL);
	}
}
static void RuntimeArray_CustomAttributesCacheGenerator_Array_Copy_m2652655318(CustomAttributesCache* cache)
{
	{
		ReliabilityContractAttribute_t3009184403 * tmp = (ReliabilityContractAttribute_t3009184403 *)cache->attributes[0];
		ReliabilityContractAttribute__ctor_m2586077688(tmp, 2LL, 1LL, NULL);
	}
}
static void RuntimeArray_CustomAttributesCacheGenerator_Array_Copy_m3698620115(CustomAttributesCache* cache)
{
	{
		ReliabilityContractAttribute_t3009184403 * tmp = (ReliabilityContractAttribute_t3009184403 *)cache->attributes[0];
		ReliabilityContractAttribute__ctor_m2586077688(tmp, 2LL, 1LL, NULL);
	}
}
static void RuntimeArray_CustomAttributesCacheGenerator_Array_IndexOf_m3436793169(CustomAttributesCache* cache)
{
	{
		ReliabilityContractAttribute_t3009184403 * tmp = (ReliabilityContractAttribute_t3009184403 *)cache->attributes[0];
		ReliabilityContractAttribute__ctor_m2586077688(tmp, 3LL, 1LL, NULL);
	}
}
static void RuntimeArray_CustomAttributesCacheGenerator_Array_IndexOf_m3442506776(CustomAttributesCache* cache)
{
	{
		ReliabilityContractAttribute_t3009184403 * tmp = (ReliabilityContractAttribute_t3009184403 *)cache->attributes[0];
		ReliabilityContractAttribute__ctor_m2586077688(tmp, 3LL, 1LL, NULL);
	}
}
static void RuntimeArray_CustomAttributesCacheGenerator_Array_IndexOf_m1260278658(CustomAttributesCache* cache)
{
	{
		ReliabilityContractAttribute_t3009184403 * tmp = (ReliabilityContractAttribute_t3009184403 *)cache->attributes[0];
		ReliabilityContractAttribute__ctor_m2586077688(tmp, 3LL, 1LL, NULL);
	}
}
static void RuntimeArray_CustomAttributesCacheGenerator_Array_LastIndexOf_m827387485(CustomAttributesCache* cache)
{
	{
		ReliabilityContractAttribute_t3009184403 * tmp = (ReliabilityContractAttribute_t3009184403 *)cache->attributes[0];
		ReliabilityContractAttribute__ctor_m2586077688(tmp, 3LL, 1LL, NULL);
	}
}
static void RuntimeArray_CustomAttributesCacheGenerator_Array_LastIndexOf_m362377999(CustomAttributesCache* cache)
{
	{
		ReliabilityContractAttribute_t3009184403 * tmp = (ReliabilityContractAttribute_t3009184403 *)cache->attributes[0];
		ReliabilityContractAttribute__ctor_m2586077688(tmp, 3LL, 1LL, NULL);
	}
}
static void RuntimeArray_CustomAttributesCacheGenerator_Array_LastIndexOf_m234007524(CustomAttributesCache* cache)
{
	{
		ReliabilityContractAttribute_t3009184403 * tmp = (ReliabilityContractAttribute_t3009184403 *)cache->attributes[0];
		ReliabilityContractAttribute__ctor_m2586077688(tmp, 3LL, 1LL, NULL);
	}
}
static void RuntimeArray_CustomAttributesCacheGenerator_Array_Reverse_m3921569028(CustomAttributesCache* cache)
{
	{
		ReliabilityContractAttribute_t3009184403 * tmp = (ReliabilityContractAttribute_t3009184403 *)cache->attributes[0];
		ReliabilityContractAttribute__ctor_m2586077688(tmp, 2LL, 1LL, NULL);
	}
}
static void RuntimeArray_CustomAttributesCacheGenerator_Array_Reverse_m3946279211(CustomAttributesCache* cache)
{
	{
		ReliabilityContractAttribute_t3009184403 * tmp = (ReliabilityContractAttribute_t3009184403 *)cache->attributes[0];
		ReliabilityContractAttribute__ctor_m2586077688(tmp, 2LL, 1LL, NULL);
	}
}
static void RuntimeArray_CustomAttributesCacheGenerator_Array_Sort_m3032378294(CustomAttributesCache* cache)
{
	{
		ReliabilityContractAttribute_t3009184403 * tmp = (ReliabilityContractAttribute_t3009184403 *)cache->attributes[0];
		ReliabilityContractAttribute__ctor_m2586077688(tmp, 2LL, 1LL, NULL);
	}
}
static void RuntimeArray_CustomAttributesCacheGenerator_Array_Sort_m243170594(CustomAttributesCache* cache)
{
	{
		ReliabilityContractAttribute_t3009184403 * tmp = (ReliabilityContractAttribute_t3009184403 *)cache->attributes[0];
		ReliabilityContractAttribute__ctor_m2586077688(tmp, 2LL, 1LL, NULL);
	}
}
static void RuntimeArray_CustomAttributesCacheGenerator_Array_Sort_m1166375231(CustomAttributesCache* cache)
{
	{
		ReliabilityContractAttribute_t3009184403 * tmp = (ReliabilityContractAttribute_t3009184403 *)cache->attributes[0];
		ReliabilityContractAttribute__ctor_m2586077688(tmp, 2LL, 1LL, NULL);
	}
}
static void RuntimeArray_CustomAttributesCacheGenerator_Array_Sort_m1504758793(CustomAttributesCache* cache)
{
	{
		ReliabilityContractAttribute_t3009184403 * tmp = (ReliabilityContractAttribute_t3009184403 *)cache->attributes[0];
		ReliabilityContractAttribute__ctor_m2586077688(tmp, 2LL, 1LL, NULL);
	}
}
static void RuntimeArray_CustomAttributesCacheGenerator_Array_Sort_m2496372837(CustomAttributesCache* cache)
{
	{
		ReliabilityContractAttribute_t3009184403 * tmp = (ReliabilityContractAttribute_t3009184403 *)cache->attributes[0];
		ReliabilityContractAttribute__ctor_m2586077688(tmp, 2LL, 1LL, NULL);
	}
}
static void RuntimeArray_CustomAttributesCacheGenerator_Array_Sort_m2232750079(CustomAttributesCache* cache)
{
	{
		ReliabilityContractAttribute_t3009184403 * tmp = (ReliabilityContractAttribute_t3009184403 *)cache->attributes[0];
		ReliabilityContractAttribute__ctor_m2586077688(tmp, 2LL, 1LL, NULL);
	}
}
static void RuntimeArray_CustomAttributesCacheGenerator_Array_Sort_m2717880702(CustomAttributesCache* cache)
{
	{
		ReliabilityContractAttribute_t3009184403 * tmp = (ReliabilityContractAttribute_t3009184403 *)cache->attributes[0];
		ReliabilityContractAttribute__ctor_m2586077688(tmp, 2LL, 1LL, NULL);
	}
}
static void RuntimeArray_CustomAttributesCacheGenerator_Array_Sort_m2668013149(CustomAttributesCache* cache)
{
	{
		ReliabilityContractAttribute_t3009184403 * tmp = (ReliabilityContractAttribute_t3009184403 *)cache->attributes[0];
		ReliabilityContractAttribute__ctor_m2586077688(tmp, 2LL, 1LL, NULL);
	}
}
static void RuntimeArray_CustomAttributesCacheGenerator_Array_Sort_m2667677402(CustomAttributesCache* cache)
{
	{
		ReliabilityContractAttribute_t3009184403 * tmp = (ReliabilityContractAttribute_t3009184403 *)cache->attributes[0];
		ReliabilityContractAttribute__ctor_m2586077688(tmp, 2LL, 1LL, NULL);
	}
}
static void RuntimeArray_CustomAttributesCacheGenerator_Array_Sort_m724484260(CustomAttributesCache* cache)
{
	{
		ReliabilityContractAttribute_t3009184403 * tmp = (ReliabilityContractAttribute_t3009184403 *)cache->attributes[0];
		ReliabilityContractAttribute__ctor_m2586077688(tmp, 2LL, 1LL, NULL);
	}
}
static void RuntimeArray_CustomAttributesCacheGenerator_Array_Sort_m3090697542(CustomAttributesCache* cache)
{
	{
		ReliabilityContractAttribute_t3009184403 * tmp = (ReliabilityContractAttribute_t3009184403 *)cache->attributes[0];
		ReliabilityContractAttribute__ctor_m2586077688(tmp, 2LL, 1LL, NULL);
	}
}
static void RuntimeArray_CustomAttributesCacheGenerator_Array_Sort_m3597357548(CustomAttributesCache* cache)
{
	{
		ReliabilityContractAttribute_t3009184403 * tmp = (ReliabilityContractAttribute_t3009184403 *)cache->attributes[0];
		ReliabilityContractAttribute__ctor_m2586077688(tmp, 2LL, 1LL, NULL);
	}
}
static void RuntimeArray_CustomAttributesCacheGenerator_Array_Sort_m1886451243(CustomAttributesCache* cache)
{
	{
		ReliabilityContractAttribute_t3009184403 * tmp = (ReliabilityContractAttribute_t3009184403 *)cache->attributes[0];
		ReliabilityContractAttribute__ctor_m2586077688(tmp, 2LL, 1LL, NULL);
	}
}
static void RuntimeArray_CustomAttributesCacheGenerator_Array_Sort_m1562643584(CustomAttributesCache* cache)
{
	{
		ReliabilityContractAttribute_t3009184403 * tmp = (ReliabilityContractAttribute_t3009184403 *)cache->attributes[0];
		ReliabilityContractAttribute__ctor_m2586077688(tmp, 2LL, 1LL, NULL);
	}
}
static void RuntimeArray_CustomAttributesCacheGenerator_Array_Sort_m2579296346(CustomAttributesCache* cache)
{
	{
		ReliabilityContractAttribute_t3009184403 * tmp = (ReliabilityContractAttribute_t3009184403 *)cache->attributes[0];
		ReliabilityContractAttribute__ctor_m2586077688(tmp, 2LL, 1LL, NULL);
	}
}
static void RuntimeArray_CustomAttributesCacheGenerator_Array_Sort_m3476310768(CustomAttributesCache* cache)
{
	{
		ReliabilityContractAttribute_t3009184403 * tmp = (ReliabilityContractAttribute_t3009184403 *)cache->attributes[0];
		ReliabilityContractAttribute__ctor_m2586077688(tmp, 2LL, 1LL, NULL);
	}
}
static void RuntimeArray_CustomAttributesCacheGenerator_Array_CopyTo_m657485551(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, false, NULL);
	}
}
static void RuntimeArray_CustomAttributesCacheGenerator_Array_Resize_m3981285257(CustomAttributesCache* cache)
{
	{
		ReliabilityContractAttribute_t3009184403 * tmp = (ReliabilityContractAttribute_t3009184403 *)cache->attributes[0];
		ReliabilityContractAttribute__ctor_m2586077688(tmp, 3LL, 1LL, NULL);
	}
}
static void RuntimeArray_CustomAttributesCacheGenerator_Array_BinarySearch_m4248949575(CustomAttributesCache* cache)
{
	{
		ReliabilityContractAttribute_t3009184403 * tmp = (ReliabilityContractAttribute_t3009184403 *)cache->attributes[0];
		ReliabilityContractAttribute__ctor_m2586077688(tmp, 3LL, 1LL, NULL);
	}
}
static void RuntimeArray_CustomAttributesCacheGenerator_Array_BinarySearch_m1974805269(CustomAttributesCache* cache)
{
	{
		ReliabilityContractAttribute_t3009184403 * tmp = (ReliabilityContractAttribute_t3009184403 *)cache->attributes[0];
		ReliabilityContractAttribute__ctor_m2586077688(tmp, 3LL, 1LL, NULL);
	}
}
static void RuntimeArray_CustomAttributesCacheGenerator_Array_BinarySearch_m3819272786(CustomAttributesCache* cache)
{
	{
		ReliabilityContractAttribute_t3009184403 * tmp = (ReliabilityContractAttribute_t3009184403 *)cache->attributes[0];
		ReliabilityContractAttribute__ctor_m2586077688(tmp, 3LL, 1LL, NULL);
	}
}
static void RuntimeArray_CustomAttributesCacheGenerator_Array_BinarySearch_m1130814323(CustomAttributesCache* cache)
{
	{
		ReliabilityContractAttribute_t3009184403 * tmp = (ReliabilityContractAttribute_t3009184403 *)cache->attributes[0];
		ReliabilityContractAttribute__ctor_m2586077688(tmp, 3LL, 1LL, NULL);
	}
}
static void RuntimeArray_CustomAttributesCacheGenerator_Array_ConstrainedCopy_m1738878172(CustomAttributesCache* cache)
{
	{
		ReliabilityContractAttribute_t3009184403 * tmp = (ReliabilityContractAttribute_t3009184403 *)cache->attributes[0];
		ReliabilityContractAttribute__ctor_m2586077688(tmp, 3LL, 2LL, NULL);
	}
}
static void RuntimeArray_CustomAttributesCacheGenerator_RuntimeArray____LongLength_PropertyInfo(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, false, NULL);
	}
}
static void ArrayReadOnlyList_1_t3002341623_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DefaultMemberAttribute_t2956431904 * tmp = (DefaultMemberAttribute_t2956431904 *)cache->attributes[0];
		DefaultMemberAttribute__ctor_m3129895323(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
	}
}
static void ArrayReadOnlyList_1_t3002341623_CustomAttributesCacheGenerator_ArrayReadOnlyList_1_GetEnumerator_m552220748(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_t1447627238 * tmp = (DebuggerHiddenAttribute_t1447627238 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_m749702160(tmp, NULL);
	}
}
static void U3CGetEnumeratorU3Ec__Iterator0_t4287845664_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
}
static void U3CGetEnumeratorU3Ec__Iterator0_t4287845664_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m375255571(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_t1447627238 * tmp = (DebuggerHiddenAttribute_t1447627238 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_m749702160(tmp, NULL);
	}
}
static void U3CGetEnumeratorU3Ec__Iterator0_t4287845664_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1119679167(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_t1447627238 * tmp = (DebuggerHiddenAttribute_t1447627238 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_m749702160(tmp, NULL);
	}
}
static void U3CGetEnumeratorU3Ec__Iterator0_t4287845664_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ec__Iterator0_Dispose_m1394235234(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_t1447627238 * tmp = (DebuggerHiddenAttribute_t1447627238 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_m749702160(tmp, NULL);
	}
}
static void U3CGetEnumeratorU3Ec__Iterator0_t4287845664_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ec__Iterator0_Reset_m3782287660(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_t1447627238 * tmp = (DebuggerHiddenAttribute_t1447627238 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_m749702160(tmp, NULL);
	}
}
static void ICollection_t1881172908_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void IList_t2514494774_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DefaultMemberAttribute_t2956431904 * tmp = (DefaultMemberAttribute_t2956431904 *)cache->attributes[0];
		DefaultMemberAttribute__ctor_m3129895323(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[1];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void IList_1_t2409468953_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DefaultMemberAttribute_t2956431904 * tmp = (DefaultMemberAttribute_t2956431904 *)cache->attributes[0];
		DefaultMemberAttribute__ctor_m3129895323(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
	}
}
static void Void_t1078098495_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void Type_t_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Type_t_CustomAttributesCacheGenerator_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ComDefaultInterfaceAttribute_t2399868801 * tmp = (ComDefaultInterfaceAttribute_t2399868801 *)cache->attributes[0];
		ComDefaultInterfaceAttribute__ctor_m1937741735(tmp, il2cpp_codegen_type_get_object(_Type_t2913350559_0_0_0_var), NULL);
	}
	{
		ClassInterfaceAttribute_t1638101999 * tmp = (ClassInterfaceAttribute_t1638101999 *)cache->attributes[1];
		ClassInterfaceAttribute__ctor_m2759037627(tmp, 0LL, NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[2];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void Type_t_CustomAttributesCacheGenerator_Type_IsSubclassOf_m61888076(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void Type_t_CustomAttributesCacheGenerator_Type_GetConstructor_m272770726(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void Type_t_CustomAttributesCacheGenerator_Type_GetConstructor_m2099858645(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void Type_t_CustomAttributesCacheGenerator_Type_GetConstructor_m3078335309(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void Type_t_CustomAttributesCacheGenerator_Type_GetConstructors_m961791358(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void Type_t_CustomAttributesCacheGenerator_Type_MakeGenericType_m4233399084____typeArguments0(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t3330428127 * tmp = (ParamArrayAttribute_t3330428127 *)cache->attributes[0];
		ParamArrayAttribute__ctor_m1696407647(tmp, NULL);
	}
}
static void MemberInfo_t_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MemberInfo_t_CustomAttributesCacheGenerator_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ComDefaultInterfaceAttribute_t2399868801 * tmp = (ComDefaultInterfaceAttribute_t2399868801 *)cache->attributes[0];
		ComDefaultInterfaceAttribute__ctor_m1937741735(tmp, il2cpp_codegen_type_get_object(_MemberInfo_t1659175437_0_0_0_var), NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[1];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		ClassInterfaceAttribute_t1638101999 * tmp = (ClassInterfaceAttribute_t1638101999 *)cache->attributes[2];
		ClassInterfaceAttribute__ctor_m2759037627(tmp, 0LL, NULL);
	}
}
static void ICustomAttributeProvider_t4054400477_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void _MemberInfo_t1659175437_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_MemberInfo_t1659175437_CustomAttributesCacheGenerator_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GuidAttribute_t3853718442 * tmp = (GuidAttribute_t3853718442 *)cache->attributes[0];
		GuidAttribute__ctor_m612200210(tmp, il2cpp_codegen_string_new_wrapper("f7102fa9-cabb-3a74-a6da-b4567ef1b079"), NULL);
	}
	{
		InterfaceTypeAttribute_t3336632297 * tmp = (InterfaceTypeAttribute_t3336632297 *)cache->attributes[1];
		InterfaceTypeAttribute__ctor_m342415882(tmp, 1LL, NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[2];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[3];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
	{
		TypeLibImportClassAttribute_t2173872460 * tmp = (TypeLibImportClassAttribute_t2173872460 *)cache->attributes[4];
		TypeLibImportClassAttribute__ctor_m1502349925(tmp, il2cpp_codegen_type_get_object(MemberInfo_t_0_0_0_var), NULL);
	}
}
static void IReflect_t4184883566_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		GuidAttribute_t3853718442 * tmp = (GuidAttribute_t3853718442 *)cache->attributes[1];
		GuidAttribute__ctor_m612200210(tmp, il2cpp_codegen_string_new_wrapper("AFBF15E5-C37C-11d2-B88E-00A0C9B471B8"), NULL);
	}
}
static void _Type_t2913350559_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_Type_t2913350559_CustomAttributesCacheGenerator_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		TypeLibImportClassAttribute_t2173872460 * tmp = (TypeLibImportClassAttribute_t2173872460 *)cache->attributes[0];
		TypeLibImportClassAttribute__ctor_m1502349925(tmp, il2cpp_codegen_type_get_object(Type_t_0_0_0_var), NULL);
	}
	{
		GuidAttribute_t3853718442 * tmp = (GuidAttribute_t3853718442 *)cache->attributes[1];
		GuidAttribute__ctor_m612200210(tmp, il2cpp_codegen_string_new_wrapper("BCA8B44D-AAD6-3A86-8AB7-03349F4F2DA2"), NULL);
	}
	{
		InterfaceTypeAttribute_t3336632297 * tmp = (InterfaceTypeAttribute_t3336632297 *)cache->attributes[2];
		InterfaceTypeAttribute__ctor_m342415882(tmp, 1LL, NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[3];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[4];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void Exception_t3361176243_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Exception_t3361176243_CustomAttributesCacheGenerator_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ClassInterfaceAttribute_t1638101999 * tmp = (ClassInterfaceAttribute_t1638101999 *)cache->attributes[0];
		ClassInterfaceAttribute__ctor_m2759037627(tmp, 0LL, NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[1];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		ComDefaultInterfaceAttribute_t2399868801 * tmp = (ComDefaultInterfaceAttribute_t2399868801 *)cache->attributes[2];
		ComDefaultInterfaceAttribute__ctor_m1937741735(tmp, il2cpp_codegen_type_get_object(_Exception_t274881737_0_0_0_var), NULL);
	}
}
static void _Exception_t274881737_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
	{
		GuidAttribute_t3853718442 * tmp = (GuidAttribute_t3853718442 *)cache->attributes[1];
		GuidAttribute__ctor_m612200210(tmp, il2cpp_codegen_string_new_wrapper("b36b5c63-42ef-38bc-a07e-0b34c98f164a"), NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[2];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		InterfaceTypeAttribute_t3336632297 * tmp = (InterfaceTypeAttribute_t3336632297 *)cache->attributes[3];
		InterfaceTypeAttribute__ctor_m342415882(tmp, 0LL, NULL);
	}
}
static void RuntimeFieldHandle_t1782795956_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		MonoTODOAttribute_t614642062 * tmp = (MonoTODOAttribute_t614642062 *)cache->attributes[1];
		MonoTODOAttribute__ctor_m2689597370(tmp, il2cpp_codegen_string_new_wrapper("Serialization needs tests"), NULL);
	}
}
static void RuntimeFieldHandle_t1782795956_CustomAttributesCacheGenerator_RuntimeFieldHandle_Equals_m1824119433(CustomAttributesCache* cache)
{
	{
		ReliabilityContractAttribute_t3009184403 * tmp = (ReliabilityContractAttribute_t3009184403 *)cache->attributes[0];
		ReliabilityContractAttribute__ctor_m2586077688(tmp, 3LL, 2LL, NULL);
	}
}
static void RuntimeTypeHandle_t637624852_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		MonoTODOAttribute_t614642062 * tmp = (MonoTODOAttribute_t614642062 *)cache->attributes[0];
		MonoTODOAttribute__ctor_m2689597370(tmp, il2cpp_codegen_string_new_wrapper("Serialization needs tests"), NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[1];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void RuntimeTypeHandle_t637624852_CustomAttributesCacheGenerator_RuntimeTypeHandle_Equals_m3288488752(CustomAttributesCache* cache)
{
	{
		ReliabilityContractAttribute_t3009184403 * tmp = (ReliabilityContractAttribute_t3009184403 *)cache->attributes[0];
		ReliabilityContractAttribute__ctor_m2586077688(tmp, 3LL, 2LL, NULL);
	}
}
static void ParamArrayAttribute_t3330428127_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		AttributeUsageAttribute_t2370971113 * tmp = (AttributeUsageAttribute_t2370971113 *)cache->attributes[1];
		AttributeUsageAttribute__ctor_m1170715599(tmp, 2048LL, NULL);
	}
}
static void OutAttribute_t2163224115_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		AttributeUsageAttribute_t2370971113 * tmp = (AttributeUsageAttribute_t2370971113 *)cache->attributes[1];
		AttributeUsageAttribute__ctor_m1170715599(tmp, 2048LL, NULL);
		AttributeUsageAttribute_set_Inherited_m2205070461(tmp, false, NULL);
	}
}
static void ObsoleteAttribute_t3443961000_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		AttributeUsageAttribute_t2370971113 * tmp = (AttributeUsageAttribute_t2370971113 *)cache->attributes[1];
		AttributeUsageAttribute__ctor_m1170715599(tmp, 6140LL, NULL);
		AttributeUsageAttribute_set_Inherited_m2205070461(tmp, false, NULL);
	}
}
static void DllImportAttribute_t3217467128_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		AttributeUsageAttribute_t2370971113 * tmp = (AttributeUsageAttribute_t2370971113 *)cache->attributes[1];
		AttributeUsageAttribute__ctor_m1170715599(tmp, 64LL, NULL);
		AttributeUsageAttribute_set_Inherited_m2205070461(tmp, false, NULL);
	}
}
static void MarshalAsAttribute_t763260220_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_t2370971113 * tmp = (AttributeUsageAttribute_t2370971113 *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m1170715599(tmp, 10496LL, NULL);
		AttributeUsageAttribute_set_Inherited_m2205070461(tmp, false, NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[1];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void MarshalAsAttribute_t763260220_CustomAttributesCacheGenerator_MarshalType(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void MarshalAsAttribute_t763260220_CustomAttributesCacheGenerator_MarshalTypeRef(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void InAttribute_t159416145_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		AttributeUsageAttribute_t2370971113 * tmp = (AttributeUsageAttribute_t2370971113 *)cache->attributes[1];
		AttributeUsageAttribute__ctor_m1170715599(tmp, 2048LL, NULL);
		AttributeUsageAttribute_set_Inherited_m2205070461(tmp, false, NULL);
	}
}
static void SecurityAttribute_t552991472_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		ObsoleteAttribute_t3443961000 * tmp = (ObsoleteAttribute_t3443961000 *)cache->attributes[1];
		ObsoleteAttribute__ctor_m3274828507(tmp, il2cpp_codegen_string_new_wrapper("CAS support is not available with Silverlight applications."), NULL);
	}
	{
		AttributeUsageAttribute_t2370971113 * tmp = (AttributeUsageAttribute_t2370971113 *)cache->attributes[2];
		AttributeUsageAttribute__ctor_m1170715599(tmp, 109LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m1313295265(tmp, true, NULL);
		AttributeUsageAttribute_set_Inherited_m2205070461(tmp, false, NULL);
	}
}
static void GuidAttribute_t3853718442_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_t2370971113 * tmp = (AttributeUsageAttribute_t2370971113 *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m1170715599(tmp, 5149LL, NULL);
		AttributeUsageAttribute_set_Inherited_m2205070461(tmp, false, NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[1];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void ComImportAttribute_t753419596_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_t2370971113 * tmp = (AttributeUsageAttribute_t2370971113 *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m1170715599(tmp, 1028LL, NULL);
		AttributeUsageAttribute_set_Inherited_m2205070461(tmp, false, NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[1];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void OptionalAttribute_t14204161_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		AttributeUsageAttribute_t2370971113 * tmp = (AttributeUsageAttribute_t2370971113 *)cache->attributes[1];
		AttributeUsageAttribute__ctor_m1170715599(tmp, 2048LL, NULL);
		AttributeUsageAttribute_set_Inherited_m2205070461(tmp, false, NULL);
	}
}
static void FixedBufferAttribute_t568550708_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_t2370971113 * tmp = (AttributeUsageAttribute_t2370971113 *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m1170715599(tmp, 256LL, NULL);
		AttributeUsageAttribute_set_Inherited_m2205070461(tmp, false, NULL);
	}
}
static void CompilerGeneratedAttribute_t3076080542_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_t2370971113 * tmp = (AttributeUsageAttribute_t2370971113 *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m1170715599(tmp, 32767LL, NULL);
	}
}
static void InternalsVisibleToAttribute_t2168674587_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_t2370971113 * tmp = (AttributeUsageAttribute_t2370971113 *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m1170715599(tmp, 1LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m1313295265(tmp, true, NULL);
		AttributeUsageAttribute_set_Inherited_m2205070461(tmp, false, NULL);
	}
}
static void RuntimeCompatibilityAttribute_t38650954_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_t2370971113 * tmp = (AttributeUsageAttribute_t2370971113 *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m1170715599(tmp, 1LL, NULL);
		AttributeUsageAttribute_set_Inherited_m2205070461(tmp, false, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m1313295265(tmp, false, NULL);
	}
}
static void DebuggerHiddenAttribute_t1447627238_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_t2370971113 * tmp = (AttributeUsageAttribute_t2370971113 *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m1170715599(tmp, 224LL, NULL);
		AttributeUsageAttribute_set_Inherited_m2205070461(tmp, false, NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[1];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void DefaultMemberAttribute_t2956431904_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_t2370971113 * tmp = (AttributeUsageAttribute_t2370971113 *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m1170715599(tmp, 1036LL, NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[1];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void DecimalConstantAttribute_t200860697_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_t2370971113 * tmp = (AttributeUsageAttribute_t2370971113 *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m1170715599(tmp, 2304LL, NULL);
		AttributeUsageAttribute_set_Inherited_m2205070461(tmp, false, NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[1];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void DecimalConstantAttribute_t200860697_CustomAttributesCacheGenerator_DecimalConstantAttribute__ctor_m579883733(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void FieldOffsetAttribute_t1678641465_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_t2370971113 * tmp = (AttributeUsageAttribute_t2370971113 *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m1170715599(tmp, 256LL, NULL);
		AttributeUsageAttribute_set_Inherited_m2205070461(tmp, false, NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[1];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void RuntimeArgumentHandle_t1816759199_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void AsyncCallback_t1046330627_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void IAsyncResult_t2277996523_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void TypedReference_t1527243294_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[1];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void MarshalByRefObject_t1929168109_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void Locale_t2448440936_CustomAttributesCacheGenerator_Locale_GetText_m659940240____args1(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t3330428127 * tmp = (ParamArrayAttribute_t3330428127 *)cache->attributes[0];
		ParamArrayAttribute__ctor_m1696407647(tmp, NULL);
	}
}
static void MonoTODOAttribute_t614642062_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_t2370971113 * tmp = (AttributeUsageAttribute_t2370971113 *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m1170715599(tmp, 32767LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m1313295265(tmp, true, NULL);
	}
}
static void MonoDocumentationNoteAttribute_t3720325674_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_t2370971113 * tmp = (AttributeUsageAttribute_t2370971113 *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m1170715599(tmp, 32767LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m1313295265(tmp, true, NULL);
	}
}
static void SafeHandleZeroOrMinusOneIsInvalid_t1802040518_CustomAttributesCacheGenerator_SafeHandleZeroOrMinusOneIsInvalid__ctor_m3262641181(CustomAttributesCache* cache)
{
	{
		ReliabilityContractAttribute_t3009184403 * tmp = (ReliabilityContractAttribute_t3009184403 *)cache->attributes[0];
		ReliabilityContractAttribute__ctor_m2586077688(tmp, 3LL, 1LL, NULL);
	}
}
static void SafeWaitHandle_t1875095495_CustomAttributesCacheGenerator_SafeWaitHandle__ctor_m1097298895(CustomAttributesCache* cache)
{
	{
		ReliabilityContractAttribute_t3009184403 * tmp = (ReliabilityContractAttribute_t3009184403 *)cache->attributes[0];
		ReliabilityContractAttribute__ctor_m2586077688(tmp, 3LL, 1LL, NULL);
	}
}
static void MSCompatUnicodeTable_t900664312_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map2(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
}
static void MSCompatUnicodeTable_t900664312_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map3(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
}
static void MSCompatUnicodeTable_t900664312_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map4(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
}
static void SortKey_t2188656382_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void PKCS12_t2451188494_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map8(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
}
static void PKCS12_t2451188494_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map9(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
}
static void PKCS12_t2451188494_CustomAttributesCacheGenerator_U3CU3Ef__switchU24mapA(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
}
static void PKCS12_t2451188494_CustomAttributesCacheGenerator_U3CU3Ef__switchU24mapB(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
}
static void PKCS12_t2451188494_CustomAttributesCacheGenerator_U3CU3Ef__switchU24mapF(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
}
static void X509CertificateCollection_t1815624850_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DefaultMemberAttribute_t2956431904 * tmp = (DefaultMemberAttribute_t2956431904 *)cache->attributes[0];
		DefaultMemberAttribute__ctor_m3129895323(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
	}
}
static void X509ExtensionCollection_t3055523517_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DefaultMemberAttribute_t2956431904 * tmp = (DefaultMemberAttribute_t2956431904 *)cache->attributes[0];
		DefaultMemberAttribute__ctor_m3129895323(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
	}
}
static void ASN1_t450259932_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DefaultMemberAttribute_t2956431904 * tmp = (DefaultMemberAttribute_t2956431904 *)cache->attributes[0];
		DefaultMemberAttribute__ctor_m3129895323(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
	}
}
static void SmallXmlParser_t1266336429_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map18(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
}
static void Dictionary_2_t1922834018_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_t1922834018_CustomAttributesCacheGenerator_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		DebuggerDisplayAttribute_t2915478477 * tmp = (DebuggerDisplayAttribute_t2915478477 *)cache->attributes[0];
		DebuggerDisplayAttribute__ctor_m685030886(tmp, il2cpp_codegen_string_new_wrapper("Count={Count}"), NULL);
	}
	{
		DebuggerTypeProxyAttribute_t2114273919 * tmp = (DebuggerTypeProxyAttribute_t2114273919 *)cache->attributes[1];
		DebuggerTypeProxyAttribute__ctor_m92548023(tmp, il2cpp_codegen_type_get_object(CollectionDebuggerView_2_t92416143_0_0_0_var), NULL);
	}
	{
		DefaultMemberAttribute_t2956431904 * tmp = (DefaultMemberAttribute_t2956431904 *)cache->attributes[2];
		DefaultMemberAttribute__ctor_m3129895323(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[3];
		ComVisibleAttribute__ctor_m29339631(tmp, false, NULL);
	}
}
static void Dictionary_2_t1922834018_CustomAttributesCacheGenerator_U3CU3Ef__amU24cacheB(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
}
static void Dictionary_2_t1922834018_CustomAttributesCacheGenerator_Dictionary_2_U3CCopyToU3Em__0_m90548668(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
}
static void IDictionary_2_t3413473948_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DefaultMemberAttribute_t2956431904 * tmp = (DefaultMemberAttribute_t2956431904 *)cache->attributes[0];
		DefaultMemberAttribute__ctor_m3129895323(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
	}
}
static void KeyNotFoundException_t3514277852_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void KeyValuePair_2_t776898938_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DebuggerDisplayAttribute_t2915478477 * tmp = (DebuggerDisplayAttribute_t2915478477 *)cache->attributes[0];
		DebuggerDisplayAttribute__ctor_m685030886(tmp, il2cpp_codegen_string_new_wrapper("{value}"), NULL);
		DebuggerDisplayAttribute_set_Name_m2283484547(tmp, il2cpp_codegen_string_new_wrapper("[{key}]"), NULL);
	}
}
static void List_1_t82844879_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_t82844879_CustomAttributesCacheGenerator_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		DefaultMemberAttribute_t2956431904 * tmp = (DefaultMemberAttribute_t2956431904 *)cache->attributes[0];
		DefaultMemberAttribute__ctor_m3129895323(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
	}
	{
		DebuggerDisplayAttribute_t2915478477 * tmp = (DebuggerDisplayAttribute_t2915478477 *)cache->attributes[1];
		DebuggerDisplayAttribute__ctor_m685030886(tmp, il2cpp_codegen_string_new_wrapper("Count={Count}"), NULL);
	}
	{
		DebuggerTypeProxyAttribute_t2114273919 * tmp = (DebuggerTypeProxyAttribute_t2114273919 *)cache->attributes[2];
		DebuggerTypeProxyAttribute__ctor_m92548023(tmp, il2cpp_codegen_type_get_object(CollectionDebuggerView_1_t1444680478_0_0_0_var), NULL);
	}
}
static void Collection_1_t3543120677_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, false, NULL);
	}
	{
		DefaultMemberAttribute_t2956431904 * tmp = (DefaultMemberAttribute_t2956431904 *)cache->attributes[1];
		DefaultMemberAttribute__ctor_m3129895323(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
	}
}
static void ReadOnlyCollection_1_t891526187_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, false, NULL);
	}
	{
		DefaultMemberAttribute_t2956431904 * tmp = (DefaultMemberAttribute_t2956431904 *)cache->attributes[1];
		DefaultMemberAttribute__ctor_m3129895323(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
	}
}
static void ArrayList_t655337682_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ArrayList_t655337682_CustomAttributesCacheGenerator_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		DefaultMemberAttribute_t2956431904 * tmp = (DefaultMemberAttribute_t2956431904 *)cache->attributes[0];
		DefaultMemberAttribute__ctor_m3129895323(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
	}
	{
		DebuggerTypeProxyAttribute_t2114273919 * tmp = (DebuggerTypeProxyAttribute_t2114273919 *)cache->attributes[1];
		DebuggerTypeProxyAttribute__ctor_m92548023(tmp, il2cpp_codegen_type_get_object(CollectionDebuggerView_t339781370_0_0_0_var), NULL);
	}
	{
		DebuggerDisplayAttribute_t2915478477 * tmp = (DebuggerDisplayAttribute_t2915478477 *)cache->attributes[2];
		DebuggerDisplayAttribute__ctor_m685030886(tmp, il2cpp_codegen_string_new_wrapper("Count={Count}"), NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[3];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void ArrayListWrapper_t3417622355_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DefaultMemberAttribute_t2956431904 * tmp = (DefaultMemberAttribute_t2956431904 *)cache->attributes[0];
		DefaultMemberAttribute__ctor_m3129895323(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
	}
}
static void SynchronizedArrayListWrapper_t4247483582_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DefaultMemberAttribute_t2956431904 * tmp = (DefaultMemberAttribute_t2956431904 *)cache->attributes[0];
		DefaultMemberAttribute__ctor_m3129895323(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
	}
}
static void ReadOnlyArrayListWrapper_t3800520942_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DefaultMemberAttribute_t2956431904 * tmp = (DefaultMemberAttribute_t2956431904 *)cache->attributes[0];
		DefaultMemberAttribute__ctor_m3129895323(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
	}
}
static void BitArray_t3352714871_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DefaultMemberAttribute_t2956431904 * tmp = (DefaultMemberAttribute_t2956431904 *)cache->attributes[0];
		DefaultMemberAttribute__ctor_m3129895323(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[1];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void CaseInsensitiveComparer_t2034744907_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void CaseInsensitiveHashCodeProvider_t2832089094_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ObsoleteAttribute_t3443961000 * tmp = (ObsoleteAttribute_t3443961000 *)cache->attributes[0];
		ObsoleteAttribute__ctor_m3274828507(tmp, il2cpp_codegen_string_new_wrapper("Please use StringComparer instead."), NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[1];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void CollectionBase_t559446181_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void Comparer_t2783508821_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void DictionaryEntry_t1385909157_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		DebuggerDisplayAttribute_t2915478477 * tmp = (DebuggerDisplayAttribute_t2915478477 *)cache->attributes[1];
		DebuggerDisplayAttribute__ctor_m685030886(tmp, il2cpp_codegen_string_new_wrapper("{_value}"), NULL);
		DebuggerDisplayAttribute_set_Name_m2283484547(tmp, il2cpp_codegen_string_new_wrapper("[{_key}]"), NULL);
	}
}
static void Hashtable_t3529848683_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Hashtable_t3529848683_CustomAttributesCacheGenerator_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		DefaultMemberAttribute_t2956431904 * tmp = (DefaultMemberAttribute_t2956431904 *)cache->attributes[0];
		DefaultMemberAttribute__ctor_m3129895323(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
	}
	{
		DebuggerTypeProxyAttribute_t2114273919 * tmp = (DebuggerTypeProxyAttribute_t2114273919 *)cache->attributes[1];
		DebuggerTypeProxyAttribute__ctor_m92548023(tmp, il2cpp_codegen_type_get_object(CollectionDebuggerView_t339781370_0_0_0_var), NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[2];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		DebuggerDisplayAttribute_t2915478477 * tmp = (DebuggerDisplayAttribute_t2915478477 *)cache->attributes[3];
		DebuggerDisplayAttribute__ctor_m685030886(tmp, il2cpp_codegen_string_new_wrapper("Count={Count}"), NULL);
	}
}
static void Hashtable_t3529848683_CustomAttributesCacheGenerator_Hashtable__ctor_m3292572634(CustomAttributesCache* cache)
{
	{
		ObsoleteAttribute_t3443961000 * tmp = (ObsoleteAttribute_t3443961000 *)cache->attributes[0];
		ObsoleteAttribute__ctor_m3274828507(tmp, il2cpp_codegen_string_new_wrapper("Please use Hashtable(int, float, IEqualityComparer) instead"), NULL);
	}
}
static void Hashtable_t3529848683_CustomAttributesCacheGenerator_Hashtable__ctor_m1202039326(CustomAttributesCache* cache)
{
	{
		ObsoleteAttribute_t3443961000 * tmp = (ObsoleteAttribute_t3443961000 *)cache->attributes[0];
		ObsoleteAttribute__ctor_m3274828507(tmp, il2cpp_codegen_string_new_wrapper("Please use Hashtable(int, IEqualityComparer) instead"), NULL);
	}
}
static void Hashtable_t3529848683_CustomAttributesCacheGenerator_Hashtable__ctor_m132973949(CustomAttributesCache* cache)
{
	{
		ObsoleteAttribute_t3443961000 * tmp = (ObsoleteAttribute_t3443961000 *)cache->attributes[0];
		ObsoleteAttribute__ctor_m3274828507(tmp, il2cpp_codegen_string_new_wrapper("Please use Hashtable(IDictionary, float, IEqualityComparer) instead"), NULL);
	}
}
static void Hashtable_t3529848683_CustomAttributesCacheGenerator_Hashtable__ctor_m347947491(CustomAttributesCache* cache)
{
	{
		ObsoleteAttribute_t3443961000 * tmp = (ObsoleteAttribute_t3443961000 *)cache->attributes[0];
		ObsoleteAttribute__ctor_m3274828507(tmp, il2cpp_codegen_string_new_wrapper("Please use Hashtable(IDictionary, IEqualityComparer) instead"), NULL);
	}
}
static void Hashtable_t3529848683_CustomAttributesCacheGenerator_Hashtable__ctor_m1435436894(CustomAttributesCache* cache)
{
	{
		ObsoleteAttribute_t3443961000 * tmp = (ObsoleteAttribute_t3443961000 *)cache->attributes[0];
		ObsoleteAttribute__ctor_m3274828507(tmp, il2cpp_codegen_string_new_wrapper("Please use Hashtable(IEqualityComparer) instead"), NULL);
	}
}
static void Hashtable_t3529848683_CustomAttributesCacheGenerator_Hashtable_Clear_m2559356064(CustomAttributesCache* cache)
{
	{
		ReliabilityContractAttribute_t3009184403 * tmp = (ReliabilityContractAttribute_t3009184403 *)cache->attributes[0];
		ReliabilityContractAttribute__ctor_m2586077688(tmp, 3LL, 2LL, NULL);
	}
}
static void Hashtable_t3529848683_CustomAttributesCacheGenerator_Hashtable_Remove_m3981333743(CustomAttributesCache* cache)
{
	{
		ReliabilityContractAttribute_t3009184403 * tmp = (ReliabilityContractAttribute_t3009184403 *)cache->attributes[0];
		ReliabilityContractAttribute__ctor_m2586077688(tmp, 3LL, 1LL, NULL);
	}
}
static void Hashtable_t3529848683_CustomAttributesCacheGenerator_Hashtable_OnDeserialization_m3313245466(CustomAttributesCache* cache)
{
	{
		MonoTODOAttribute_t614642062 * tmp = (MonoTODOAttribute_t614642062 *)cache->attributes[0];
		MonoTODOAttribute__ctor_m2689597370(tmp, il2cpp_codegen_string_new_wrapper("Serialize equalityComparer"), NULL);
	}
}
static void Hashtable_t3529848683_CustomAttributesCacheGenerator_Hashtable_t3529848683____comparer_PropertyInfo(CustomAttributesCache* cache)
{
	{
		ObsoleteAttribute_t3443961000 * tmp = (ObsoleteAttribute_t3443961000 *)cache->attributes[0];
		ObsoleteAttribute__ctor_m3274828507(tmp, il2cpp_codegen_string_new_wrapper("Please use EqualityComparer property."), NULL);
	}
}
static void Hashtable_t3529848683_CustomAttributesCacheGenerator_Hashtable_t3529848683____hcp_PropertyInfo(CustomAttributesCache* cache)
{
	{
		ObsoleteAttribute_t3443961000 * tmp = (ObsoleteAttribute_t3443961000 *)cache->attributes[0];
		ObsoleteAttribute__ctor_m3274828507(tmp, il2cpp_codegen_string_new_wrapper("Please use EqualityComparer property."), NULL);
	}
}
static void HashKeys_t3304597669_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HashKeys_t3304597669_CustomAttributesCacheGenerator_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		DebuggerTypeProxyAttribute_t2114273919 * tmp = (DebuggerTypeProxyAttribute_t2114273919 *)cache->attributes[0];
		DebuggerTypeProxyAttribute__ctor_m92548023(tmp, il2cpp_codegen_type_get_object(CollectionDebuggerView_t339781370_0_0_0_var), NULL);
	}
	{
		DebuggerDisplayAttribute_t2915478477 * tmp = (DebuggerDisplayAttribute_t2915478477 *)cache->attributes[1];
		DebuggerDisplayAttribute__ctor_m685030886(tmp, il2cpp_codegen_string_new_wrapper("Count={Count}"), NULL);
	}
}
static void HashValues_t1675258150_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HashValues_t1675258150_CustomAttributesCacheGenerator_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		DebuggerTypeProxyAttribute_t2114273919 * tmp = (DebuggerTypeProxyAttribute_t2114273919 *)cache->attributes[0];
		DebuggerTypeProxyAttribute__ctor_m92548023(tmp, il2cpp_codegen_type_get_object(CollectionDebuggerView_t339781370_0_0_0_var), NULL);
	}
	{
		DebuggerDisplayAttribute_t2915478477 * tmp = (DebuggerDisplayAttribute_t2915478477 *)cache->attributes[1];
		DebuggerDisplayAttribute__ctor_m685030886(tmp, il2cpp_codegen_string_new_wrapper("Count={Count}"), NULL);
	}
}
static void SyncHashtable_t3347591716_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DefaultMemberAttribute_t2956431904 * tmp = (DefaultMemberAttribute_t2956431904 *)cache->attributes[0];
		DefaultMemberAttribute__ctor_m3129895323(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
	}
}
static void IComparer_t237179357_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void IDictionary_t1911576662_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		DefaultMemberAttribute_t2956431904 * tmp = (DefaultMemberAttribute_t2956431904 *)cache->attributes[1];
		DefaultMemberAttribute__ctor_m3129895323(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
	}
}
static void IDictionaryEnumerator_t2742074902_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void IEqualityComparer_t1867799194_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void IHashCodeProvider_t692107493_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		ObsoleteAttribute_t3443961000 * tmp = (ObsoleteAttribute_t3443961000 *)cache->attributes[1];
		ObsoleteAttribute__ctor_m3274828507(tmp, il2cpp_codegen_string_new_wrapper("Please use IEqualityComparer instead."), NULL);
	}
}
static void SortedList_t2047223347_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		DefaultMemberAttribute_t2956431904 * tmp = (DefaultMemberAttribute_t2956431904 *)cache->attributes[1];
		DefaultMemberAttribute__ctor_m3129895323(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
	}
	{
		DebuggerDisplayAttribute_t2915478477 * tmp = (DebuggerDisplayAttribute_t2915478477 *)cache->attributes[2];
		DebuggerDisplayAttribute__ctor_m685030886(tmp, il2cpp_codegen_string_new_wrapper("Count={Count}"), NULL);
	}
}
static void Stack_t733017594_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Stack_t733017594_CustomAttributesCacheGenerator_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		DebuggerDisplayAttribute_t2915478477 * tmp = (DebuggerDisplayAttribute_t2915478477 *)cache->attributes[0];
		DebuggerDisplayAttribute__ctor_m685030886(tmp, il2cpp_codegen_string_new_wrapper("Count={Count}"), NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[1];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		DebuggerTypeProxyAttribute_t2114273919 * tmp = (DebuggerTypeProxyAttribute_t2114273919 *)cache->attributes[2];
		DebuggerTypeProxyAttribute__ctor_m92548023(tmp, il2cpp_codegen_type_get_object(CollectionDebuggerView_t339781370_0_0_0_var), NULL);
	}
}
static void AssemblyHashAlgorithm_t2803938925_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void AssemblyVersionCompatibility_t1122309428_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void ISymbolWriter_t1307945709_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void DebuggableAttribute_t3717120398_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		AttributeUsageAttribute_t2370971113 * tmp = (AttributeUsageAttribute_t2370971113 *)cache->attributes[1];
		AttributeUsageAttribute__ctor_m1170715599(tmp, 3LL, NULL);
	}
}
static void DebuggingModes_t1277403995_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		FlagsAttribute_t388727259 * tmp = (FlagsAttribute_t388727259 *)cache->attributes[0];
		FlagsAttribute__ctor_m3834565173(tmp, NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[1];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void DebuggerBrowsableAttribute_t1527328379_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_t2370971113 * tmp = (AttributeUsageAttribute_t2370971113 *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m1170715599(tmp, 384LL, NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[1];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void DebuggerBrowsableState_t329731401_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void DebuggerDisplayAttribute_t2915478477_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_t2370971113 * tmp = (AttributeUsageAttribute_t2370971113 *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m1170715599(tmp, 4509LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m1313295265(tmp, true, NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[1];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void DebuggerStepThroughAttribute_t3823612831_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		AttributeUsageAttribute_t2370971113 * tmp = (AttributeUsageAttribute_t2370971113 *)cache->attributes[1];
		AttributeUsageAttribute__ctor_m1170715599(tmp, 108LL, NULL);
		AttributeUsageAttribute_set_Inherited_m2205070461(tmp, false, NULL);
	}
}
static void DebuggerTypeProxyAttribute_t2114273919_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		AttributeUsageAttribute_t2370971113 * tmp = (AttributeUsageAttribute_t2370971113 *)cache->attributes[1];
		AttributeUsageAttribute__ctor_m1170715599(tmp, 13LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m1313295265(tmp, true, NULL);
	}
}
static void StackFrame_t4156368350_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		MonoTODOAttribute_t614642062 * tmp = (MonoTODOAttribute_t614642062 *)cache->attributes[1];
		MonoTODOAttribute__ctor_m2689597370(tmp, il2cpp_codegen_string_new_wrapper("Serialized objects are not compatible with MS.NET"), NULL);
	}
}
static void StackTrace_t1923435704_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		MonoTODOAttribute_t614642062 * tmp = (MonoTODOAttribute_t614642062 *)cache->attributes[0];
		MonoTODOAttribute__ctor_m2689597370(tmp, il2cpp_codegen_string_new_wrapper("Serialized objects are not compatible with .NET"), NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[1];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void Calendar_t585695848_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void Calendar_t585695848_CustomAttributesCacheGenerator_Calendar_Clone_m56132940(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, false, NULL);
	}
}
static void CompareInfo_t2715256550_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void CompareOptions_t3933814440_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		FlagsAttribute_t388727259 * tmp = (FlagsAttribute_t388727259 *)cache->attributes[0];
		FlagsAttribute__ctor_m3834565173(tmp, NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[1];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void CultureInfo_t3456976115_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void CultureInfo_t3456976115_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map19(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
}
static void CultureInfo_t3456976115_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map1A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
}
static void DateTimeFormatFlags_t2573759006_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		FlagsAttribute_t388727259 * tmp = (FlagsAttribute_t388727259 *)cache->attributes[0];
		FlagsAttribute__ctor_m3834565173(tmp, NULL);
	}
}
static void DateTimeFormatInfo_t2645956038_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void DateTimeStyles_t1026389726_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		FlagsAttribute_t388727259 * tmp = (FlagsAttribute_t388727259 *)cache->attributes[1];
		FlagsAttribute__ctor_m3834565173(tmp, NULL);
	}
}
static void DaylightTime_t4218775295_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void GregorianCalendar_t1127892050_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		MonoTODOAttribute_t614642062 * tmp = (MonoTODOAttribute_t614642062 *)cache->attributes[0];
		MonoTODOAttribute__ctor_m2689597370(tmp, il2cpp_codegen_string_new_wrapper("Serialization format not compatible with .NET"), NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[1];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void GregorianCalendarTypes_t4096157752_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void NumberFormatInfo_t299001223_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void NumberStyles_t2413170484_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		FlagsAttribute_t388727259 * tmp = (FlagsAttribute_t388727259 *)cache->attributes[0];
		FlagsAttribute__ctor_m3834565173(tmp, NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[1];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void RegionInfo_t1443449959_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void RegionInfo_t1443449959_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map1B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
}
static void RegionInfo_t1443449959_CustomAttributesCacheGenerator_RegionInfo_t1443449959____CurrencyEnglishName_PropertyInfo(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, false, NULL);
	}
}
static void RegionInfo_t1443449959_CustomAttributesCacheGenerator_RegionInfo_t1443449959____DisplayName_PropertyInfo(CustomAttributesCache* cache)
{
	{
		MonoTODOAttribute_t614642062 * tmp = (MonoTODOAttribute_t614642062 *)cache->attributes[0];
		MonoTODOAttribute__ctor_m2689597370(tmp, il2cpp_codegen_string_new_wrapper("DisplayName currently only returns the EnglishName"), NULL);
	}
}
static void RegionInfo_t1443449959_CustomAttributesCacheGenerator_RegionInfo_t1443449959____GeoId_PropertyInfo(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, false, NULL);
	}
}
static void RegionInfo_t1443449959_CustomAttributesCacheGenerator_RegionInfo_t1443449959____NativeName_PropertyInfo(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, false, NULL);
	}
}
static void RegionInfo_t1443449959_CustomAttributesCacheGenerator_RegionInfo_t1443449959____CurrencyNativeName_PropertyInfo(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, false, NULL);
	}
	{
		MonoTODOAttribute_t614642062 * tmp = (MonoTODOAttribute_t614642062 *)cache->attributes[1];
		MonoTODOAttribute__ctor_m2689597370(tmp, il2cpp_codegen_string_new_wrapper("Not implemented"), NULL);
	}
}
static void TextInfo_t3641999658_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		MonoTODOAttribute_t614642062 * tmp = (MonoTODOAttribute_t614642062 *)cache->attributes[1];
		MonoTODOAttribute__ctor_m2689597370(tmp, il2cpp_codegen_string_new_wrapper("IDeserializationCallback isn't implemented."), NULL);
	}
}
static void TextInfo_t3641999658_CustomAttributesCacheGenerator_TextInfo_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m3013407797(CustomAttributesCache* cache)
{
	{
		MonoTODOAttribute_t614642062 * tmp = (MonoTODOAttribute_t614642062 *)cache->attributes[0];
		MonoTODOAttribute__ctor_m1347054473(tmp, NULL);
	}
}
static void TextInfo_t3641999658_CustomAttributesCacheGenerator_TextInfo_Clone_m2167794002(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, false, NULL);
	}
}
static void TextInfo_t3641999658_CustomAttributesCacheGenerator_TextInfo_t3641999658____CultureName_PropertyInfo(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, false, NULL);
	}
}
static void UnicodeCategory_t1362072295_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void IsolatedStorageException_t899218601_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void BinaryReader_t2596923309_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void BinaryReader_t2596923309_CustomAttributesCacheGenerator_BinaryReader_ReadSByte_m3116176050(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void BinaryReader_t2596923309_CustomAttributesCacheGenerator_BinaryReader_ReadUInt16_m3883529060(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void BinaryReader_t2596923309_CustomAttributesCacheGenerator_BinaryReader_ReadUInt32_m4084076630(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void BinaryReader_t2596923309_CustomAttributesCacheGenerator_BinaryReader_ReadUInt64_m181663393(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void Directory_t1254319787_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void DirectoryInfo_t1919733806_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void DirectoryNotFoundException_t1050841406_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void EndOfStreamException_t2896906840_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void File_t3279382285_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void FileAccess_t3630081704_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		FlagsAttribute_t388727259 * tmp = (FlagsAttribute_t388727259 *)cache->attributes[1];
		FlagsAttribute__ctor_m3834565173(tmp, NULL);
	}
}
static void FileAttributes_t1797260272_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		FlagsAttribute_t388727259 * tmp = (FlagsAttribute_t388727259 *)cache->attributes[1];
		FlagsAttribute__ctor_m3834565173(tmp, NULL);
	}
}
static void FileLoadException_t2508169678_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void FileMode_t1488306634_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void FileNotFoundException_t142405926_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void FileOptions_t1643520207_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		FlagsAttribute_t388727259 * tmp = (FlagsAttribute_t388727259 *)cache->attributes[0];
		FlagsAttribute__ctor_m3834565173(tmp, NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[1];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void FileShare_t3223761390_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		FlagsAttribute_t388727259 * tmp = (FlagsAttribute_t388727259 *)cache->attributes[1];
		FlagsAttribute__ctor_m3834565173(tmp, NULL);
	}
}
static void FileStream_t3385056842_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void FileSystemInfo_t1149434941_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void IOException_t2406898860_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void MemoryStream_t1999701249_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		MonoTODOAttribute_t614642062 * tmp = (MonoTODOAttribute_t614642062 *)cache->attributes[0];
		MonoTODOAttribute__ctor_m2689597370(tmp, il2cpp_codegen_string_new_wrapper("Serialization format not compatible with .NET"), NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[1];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void Path_t1320551609_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void Path_t1320551609_CustomAttributesCacheGenerator_InvalidPathChars(CustomAttributesCache* cache)
{
	{
		ObsoleteAttribute_t3443961000 * tmp = (ObsoleteAttribute_t3443961000 *)cache->attributes[0];
		ObsoleteAttribute__ctor_m3274828507(tmp, il2cpp_codegen_string_new_wrapper("see GetInvalidPathChars and GetInvalidFileNameChars methods."), NULL);
	}
}
static void PathTooLongException_t4135805839_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void SeekOrigin_t2616381620_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void Stream_t2349536632_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void StreamReader_t275244658_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void StreamWriter_t4173970735_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void StringReader_t1882904841_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void TextReader_t600326831_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void TextWriter_t2166895762_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void UnmanagedMemoryStream_t1345777584_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void AssemblyBuilder_t489430578_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AssemblyBuilder_t489430578_CustomAttributesCacheGenerator_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ClassInterfaceAttribute_t1638101999 * tmp = (ClassInterfaceAttribute_t1638101999 *)cache->attributes[0];
		ClassInterfaceAttribute__ctor_m2759037627(tmp, 0LL, NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[1];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		ComDefaultInterfaceAttribute_t2399868801 * tmp = (ComDefaultInterfaceAttribute_t2399868801 *)cache->attributes[2];
		ComDefaultInterfaceAttribute__ctor_m1937741735(tmp, il2cpp_codegen_type_get_object(_AssemblyBuilder_t2438890953_0_0_0_var), NULL);
	}
}
static void ConstructorBuilder_t1596923965_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ConstructorBuilder_t1596923965_CustomAttributesCacheGenerator_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ClassInterfaceAttribute_t1638101999 * tmp = (ClassInterfaceAttribute_t1638101999 *)cache->attributes[0];
		ClassInterfaceAttribute__ctor_m2759037627(tmp, 0LL, NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[1];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		ComDefaultInterfaceAttribute_t2399868801 * tmp = (ComDefaultInterfaceAttribute_t2399868801 *)cache->attributes[2];
		ComDefaultInterfaceAttribute__ctor_m1937741735(tmp, il2cpp_codegen_type_get_object(_ConstructorBuilder_t3355697174_0_0_0_var), NULL);
	}
}
static void ConstructorBuilder_t1596923965_CustomAttributesCacheGenerator_ConstructorBuilder_t1596923965____CallingConvention_PropertyInfo(CustomAttributesCache* cache)
{
	{
		MonoTODOAttribute_t614642062 * tmp = (MonoTODOAttribute_t614642062 *)cache->attributes[0];
		MonoTODOAttribute__ctor_m1347054473(tmp, NULL);
	}
}
static void CustomAttributeBuilder_t1432176696_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CustomAttributeBuilder_t1432176696_CustomAttributesCacheGenerator_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ClassInterfaceAttribute_t1638101999 * tmp = (ClassInterfaceAttribute_t1638101999 *)cache->attributes[0];
		ClassInterfaceAttribute__ctor_m2759037627(tmp, 0LL, NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[1];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		ComDefaultInterfaceAttribute_t2399868801 * tmp = (ComDefaultInterfaceAttribute_t2399868801 *)cache->attributes[2];
		ComDefaultInterfaceAttribute__ctor_m1937741735(tmp, il2cpp_codegen_type_get_object(_CustomAttributeBuilder_t2651440002_0_0_0_var), NULL);
	}
}
static void EnumBuilder_t617843_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EnumBuilder_t617843_CustomAttributesCacheGenerator_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ComDefaultInterfaceAttribute_t2399868801 * tmp = (ComDefaultInterfaceAttribute_t2399868801 *)cache->attributes[0];
		ComDefaultInterfaceAttribute__ctor_m1937741735(tmp, il2cpp_codegen_type_get_object(_EnumBuilder_t3195197362_0_0_0_var), NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[1];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		ClassInterfaceAttribute_t1638101999 * tmp = (ClassInterfaceAttribute_t1638101999 *)cache->attributes[2];
		ClassInterfaceAttribute__ctor_m2759037627(tmp, 0LL, NULL);
	}
}
static void EnumBuilder_t617843_CustomAttributesCacheGenerator_EnumBuilder_GetConstructors_m3085338377(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void EventBuilder_t2300762438_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EventBuilder_t2300762438_CustomAttributesCacheGenerator_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ComDefaultInterfaceAttribute_t2399868801 * tmp = (ComDefaultInterfaceAttribute_t2399868801 *)cache->attributes[0];
		ComDefaultInterfaceAttribute__ctor_m1937741735(tmp, il2cpp_codegen_type_get_object(_EventBuilder_t3169829690_0_0_0_var), NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[1];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		ClassInterfaceAttribute_t1638101999 * tmp = (ClassInterfaceAttribute_t1638101999 *)cache->attributes[2];
		ClassInterfaceAttribute__ctor_m2759037627(tmp, 0LL, NULL);
	}
}
static void FieldBuilder_t803025013_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FieldBuilder_t803025013_CustomAttributesCacheGenerator_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ClassInterfaceAttribute_t1638101999 * tmp = (ClassInterfaceAttribute_t1638101999 *)cache->attributes[0];
		ClassInterfaceAttribute__ctor_m2759037627(tmp, 0LL, NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[1];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		ComDefaultInterfaceAttribute_t2399868801 * tmp = (ComDefaultInterfaceAttribute_t2399868801 *)cache->attributes[2];
		ComDefaultInterfaceAttribute__ctor_m1937741735(tmp, il2cpp_codegen_type_get_object(_FieldBuilder_t3486190427_0_0_0_var), NULL);
	}
}
static void GenericTypeParameterBuilder_t193954652_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void GenericTypeParameterBuilder_t193954652_CustomAttributesCacheGenerator_GenericTypeParameterBuilder_IsSubclassOf_m2493609931(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void GenericTypeParameterBuilder_t193954652_CustomAttributesCacheGenerator_GenericTypeParameterBuilder_GetConstructors_m2634453164(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void GenericTypeParameterBuilder_t193954652_CustomAttributesCacheGenerator_GenericTypeParameterBuilder_Equals_m3732267147(CustomAttributesCache* cache)
{
	{
		MonoTODOAttribute_t614642062 * tmp = (MonoTODOAttribute_t614642062 *)cache->attributes[0];
		MonoTODOAttribute__ctor_m1347054473(tmp, NULL);
	}
}
static void GenericTypeParameterBuilder_t193954652_CustomAttributesCacheGenerator_GenericTypeParameterBuilder_GetHashCode_m3376400717(CustomAttributesCache* cache)
{
	{
		MonoTODOAttribute_t614642062 * tmp = (MonoTODOAttribute_t614642062 *)cache->attributes[0];
		MonoTODOAttribute__ctor_m1347054473(tmp, NULL);
	}
}
static void GenericTypeParameterBuilder_t193954652_CustomAttributesCacheGenerator_GenericTypeParameterBuilder_MakeGenericType_m3812059673(CustomAttributesCache* cache)
{
	{
		MonoTODOAttribute_t614642062 * tmp = (MonoTODOAttribute_t614642062 *)cache->attributes[0];
		MonoTODOAttribute__ctor_m1347054473(tmp, NULL);
	}
}
static void GenericTypeParameterBuilder_t193954652_CustomAttributesCacheGenerator_GenericTypeParameterBuilder_MakeGenericType_m3812059673____typeArguments0(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t3330428127 * tmp = (ParamArrayAttribute_t3330428127 *)cache->attributes[0];
		ParamArrayAttribute__ctor_m1696407647(tmp, NULL);
	}
}
static void ILGenerator_t1111467769_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ILGenerator_t1111467769_CustomAttributesCacheGenerator_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		ClassInterfaceAttribute_t1638101999 * tmp = (ClassInterfaceAttribute_t1638101999 *)cache->attributes[1];
		ClassInterfaceAttribute__ctor_m2759037627(tmp, 0LL, NULL);
	}
	{
		ComDefaultInterfaceAttribute_t2399868801 * tmp = (ComDefaultInterfaceAttribute_t2399868801 *)cache->attributes[2];
		ComDefaultInterfaceAttribute__ctor_m1937741735(tmp, il2cpp_codegen_type_get_object(_ILGenerator_t3086005605_0_0_0_var), NULL);
	}
}
static void ILGenerator_t1111467769_CustomAttributesCacheGenerator_ILGenerator_Emit_m1154166167(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void ILGenerator_t1111467769_CustomAttributesCacheGenerator_ILGenerator_Mono_GetCurrentOffset_m66508256(CustomAttributesCache* cache)
{
	{
		ObsoleteAttribute_t3443961000 * tmp = (ObsoleteAttribute_t3443961000 *)cache->attributes[0];
		ObsoleteAttribute__ctor_m3274828507(tmp, il2cpp_codegen_string_new_wrapper("Use ILOffset"), NULL);
	}
}
static void MethodBuilder_t2964638190_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MethodBuilder_t2964638190_CustomAttributesCacheGenerator_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ComDefaultInterfaceAttribute_t2399868801 * tmp = (ComDefaultInterfaceAttribute_t2399868801 *)cache->attributes[0];
		ComDefaultInterfaceAttribute__ctor_m1937741735(tmp, il2cpp_codegen_type_get_object(_MethodBuilder_t195532601_0_0_0_var), NULL);
	}
	{
		ClassInterfaceAttribute_t1638101999 * tmp = (ClassInterfaceAttribute_t1638101999 *)cache->attributes[1];
		ClassInterfaceAttribute__ctor_m2759037627(tmp, 0LL, NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[2];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void MethodBuilder_t2964638190_CustomAttributesCacheGenerator_MethodBuilder_Equals_m303455341(CustomAttributesCache* cache)
{
	{
		MonoTODOAttribute_t614642062 * tmp = (MonoTODOAttribute_t614642062 *)cache->attributes[0];
		MonoTODOAttribute__ctor_m1347054473(tmp, NULL);
	}
}
static void MethodBuilder_t2964638190_CustomAttributesCacheGenerator_MethodBuilder_MakeGenericMethod_m1039446169____typeArguments0(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t3330428127 * tmp = (ParamArrayAttribute_t3330428127 *)cache->attributes[0];
		ParamArrayAttribute__ctor_m1696407647(tmp, NULL);
	}
}
static void MethodToken_t1430837147_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void ModuleBuilder_t671559400_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ModuleBuilder_t671559400_CustomAttributesCacheGenerator_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ClassInterfaceAttribute_t1638101999 * tmp = (ClassInterfaceAttribute_t1638101999 *)cache->attributes[0];
		ClassInterfaceAttribute__ctor_m2759037627(tmp, 0LL, NULL);
	}
	{
		ComDefaultInterfaceAttribute_t2399868801 * tmp = (ComDefaultInterfaceAttribute_t2399868801 *)cache->attributes[1];
		ComDefaultInterfaceAttribute__ctor_m1937741735(tmp, il2cpp_codegen_type_get_object(_ModuleBuilder_t3149677118_0_0_0_var), NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[2];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void OpCode_t376486760_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void OpCodes_t1550470625_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void OpCodes_t1550470625_CustomAttributesCacheGenerator_Castclass(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void PackingSize_t3859335692_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void ParameterBuilder_t2478802094_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ParameterBuilder_t2478802094_CustomAttributesCacheGenerator_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		ComDefaultInterfaceAttribute_t2399868801 * tmp = (ComDefaultInterfaceAttribute_t2399868801 *)cache->attributes[1];
		ComDefaultInterfaceAttribute__ctor_m1937741735(tmp, il2cpp_codegen_type_get_object(_ParameterBuilder_t3730072709_0_0_0_var), NULL);
	}
	{
		ClassInterfaceAttribute_t1638101999 * tmp = (ClassInterfaceAttribute_t1638101999 *)cache->attributes[2];
		ClassInterfaceAttribute__ctor_m2759037627(tmp, 0LL, NULL);
	}
}
static void PropertyBuilder_t1258013571_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PropertyBuilder_t1258013571_CustomAttributesCacheGenerator_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ClassInterfaceAttribute_t1638101999 * tmp = (ClassInterfaceAttribute_t1638101999 *)cache->attributes[0];
		ClassInterfaceAttribute__ctor_m2759037627(tmp, 0LL, NULL);
	}
	{
		ComDefaultInterfaceAttribute_t2399868801 * tmp = (ComDefaultInterfaceAttribute_t2399868801 *)cache->attributes[1];
		ComDefaultInterfaceAttribute__ctor_m1937741735(tmp, il2cpp_codegen_type_get_object(_PropertyBuilder_t2876596643_0_0_0_var), NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[2];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void StackBehaviour_t1946418481_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void TypeBuilder_t1663068587_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TypeBuilder_t1663068587_CustomAttributesCacheGenerator_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		ComDefaultInterfaceAttribute_t2399868801 * tmp = (ComDefaultInterfaceAttribute_t2399868801 *)cache->attributes[1];
		ComDefaultInterfaceAttribute__ctor_m1937741735(tmp, il2cpp_codegen_type_get_object(_TypeBuilder_t4136446867_0_0_0_var), NULL);
	}
	{
		ClassInterfaceAttribute_t1638101999 * tmp = (ClassInterfaceAttribute_t1638101999 *)cache->attributes[2];
		ClassInterfaceAttribute__ctor_m2759037627(tmp, 0LL, NULL);
	}
}
static void TypeBuilder_t1663068587_CustomAttributesCacheGenerator_TypeBuilder_DefineConstructor_m1162809555(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void TypeBuilder_t1663068587_CustomAttributesCacheGenerator_TypeBuilder_DefineConstructor_m2260052946(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void TypeBuilder_t1663068587_CustomAttributesCacheGenerator_TypeBuilder_DefineDefaultConstructor_m3505124928(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void TypeBuilder_t1663068587_CustomAttributesCacheGenerator_TypeBuilder_GetConstructors_m55221742(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void TypeBuilder_t1663068587_CustomAttributesCacheGenerator_TypeBuilder_MakeGenericType_m2575213963(CustomAttributesCache* cache)
{
	{
		MonoTODOAttribute_t614642062 * tmp = (MonoTODOAttribute_t614642062 *)cache->attributes[0];
		MonoTODOAttribute__ctor_m1347054473(tmp, NULL);
	}
}
static void TypeBuilder_t1663068587_CustomAttributesCacheGenerator_TypeBuilder_MakeGenericType_m2575213963____typeArguments0(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t3330428127 * tmp = (ParamArrayAttribute_t3330428127 *)cache->attributes[0];
		ParamArrayAttribute__ctor_m1696407647(tmp, NULL);
	}
}
static void TypeBuilder_t1663068587_CustomAttributesCacheGenerator_TypeBuilder_IsAssignableFrom_m1068163903(CustomAttributesCache* cache)
{
	{
		MonoTODOAttribute_t614642062 * tmp = (MonoTODOAttribute_t614642062 *)cache->attributes[0];
		MonoTODOAttribute__ctor_m1347054473(tmp, NULL);
	}
}
static void TypeBuilder_t1663068587_CustomAttributesCacheGenerator_TypeBuilder_IsSubclassOf_m2364973326(CustomAttributesCache* cache)
{
	{
		MonoTODOAttribute_t614642062 * tmp = (MonoTODOAttribute_t614642062 *)cache->attributes[0];
		MonoTODOAttribute__ctor_m1347054473(tmp, NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[1];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void TypeBuilder_t1663068587_CustomAttributesCacheGenerator_TypeBuilder_IsAssignableTo_m326333311(CustomAttributesCache* cache)
{
	{
		MonoTODOAttribute_t614642062 * tmp = (MonoTODOAttribute_t614642062 *)cache->attributes[0];
		MonoTODOAttribute__ctor_m2689597370(tmp, il2cpp_codegen_string_new_wrapper("arrays"), NULL);
	}
}
static void UnmanagedMarshal_t1503927065_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ObsoleteAttribute_t3443961000 * tmp = (ObsoleteAttribute_t3443961000 *)cache->attributes[0];
		ObsoleteAttribute__ctor_m3274828507(tmp, il2cpp_codegen_string_new_wrapper("An alternate API is available: Emit the MarshalAs custom attribute instead."), NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[1];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void AmbiguousMatchException_t1921891757_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void Assembly_t2665685420_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Assembly_t2665685420_CustomAttributesCacheGenerator_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ComDefaultInterfaceAttribute_t2399868801 * tmp = (ComDefaultInterfaceAttribute_t2399868801 *)cache->attributes[0];
		ComDefaultInterfaceAttribute__ctor_m1937741735(tmp, il2cpp_codegen_type_get_object(_Assembly_t3106968071_0_0_0_var), NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[1];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		ClassInterfaceAttribute_t1638101999 * tmp = (ClassInterfaceAttribute_t1638101999 *)cache->attributes[2];
		ClassInterfaceAttribute__ctor_m2759037627(tmp, 0LL, NULL);
	}
}
static void Assembly_t2665685420_CustomAttributesCacheGenerator_Assembly_GetName_m3194382543(CustomAttributesCache* cache)
{
	{
		MonoTODOAttribute_t614642062 * tmp = (MonoTODOAttribute_t614642062 *)cache->attributes[0];
		MonoTODOAttribute__ctor_m2689597370(tmp, il2cpp_codegen_string_new_wrapper("copiedName == true is not supported"), NULL);
	}
}
static void AssemblyCompanyAttribute_t272143903_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		AttributeUsageAttribute_t2370971113 * tmp = (AttributeUsageAttribute_t2370971113 *)cache->attributes[1];
		AttributeUsageAttribute__ctor_m1170715599(tmp, 1LL, NULL);
		AttributeUsageAttribute_set_Inherited_m2205070461(tmp, false, NULL);
	}
}
static void AssemblyCopyrightAttribute_t1775796795_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_t2370971113 * tmp = (AttributeUsageAttribute_t2370971113 *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m1170715599(tmp, 1LL, NULL);
		AttributeUsageAttribute_set_Inherited_m2205070461(tmp, false, NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[1];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void AssemblyDefaultAliasAttribute_t356814493_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_t2370971113 * tmp = (AttributeUsageAttribute_t2370971113 *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m1170715599(tmp, 1LL, NULL);
		AttributeUsageAttribute_set_Inherited_m2205070461(tmp, false, NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[1];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void AssemblyDelaySignAttribute_t1978111684_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		AttributeUsageAttribute_t2370971113 * tmp = (AttributeUsageAttribute_t2370971113 *)cache->attributes[1];
		AttributeUsageAttribute__ctor_m1170715599(tmp, 1LL, NULL);
		AttributeUsageAttribute_set_Inherited_m2205070461(tmp, false, NULL);
	}
}
static void AssemblyDescriptionAttribute_t1589567422_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		AttributeUsageAttribute_t2370971113 * tmp = (AttributeUsageAttribute_t2370971113 *)cache->attributes[1];
		AttributeUsageAttribute__ctor_m1170715599(tmp, 1LL, NULL);
		AttributeUsageAttribute_set_Inherited_m2205070461(tmp, false, NULL);
	}
}
static void AssemblyFileVersionAttribute_t3786563949_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		AttributeUsageAttribute_t2370971113 * tmp = (AttributeUsageAttribute_t2370971113 *)cache->attributes[1];
		AttributeUsageAttribute__ctor_m1170715599(tmp, 1LL, NULL);
		AttributeUsageAttribute_set_Inherited_m2205070461(tmp, false, NULL);
	}
}
static void AssemblyInformationalVersionAttribute_t2163289957_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		AttributeUsageAttribute_t2370971113 * tmp = (AttributeUsageAttribute_t2370971113 *)cache->attributes[1];
		AttributeUsageAttribute__ctor_m1170715599(tmp, 1LL, NULL);
		AttributeUsageAttribute_set_Inherited_m2205070461(tmp, false, NULL);
	}
}
static void AssemblyKeyFileAttribute_t1077071033_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_t2370971113 * tmp = (AttributeUsageAttribute_t2370971113 *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m1170715599(tmp, 1LL, NULL);
		AttributeUsageAttribute_set_Inherited_m2205070461(tmp, false, NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[1];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void AssemblyName_t997940415_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AssemblyName_t997940415_CustomAttributesCacheGenerator_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ComDefaultInterfaceAttribute_t2399868801 * tmp = (ComDefaultInterfaceAttribute_t2399868801 *)cache->attributes[0];
		ComDefaultInterfaceAttribute__ctor_m1937741735(tmp, il2cpp_codegen_type_get_object(_AssemblyName_t349687559_0_0_0_var), NULL);
	}
	{
		ClassInterfaceAttribute_t1638101999 * tmp = (ClassInterfaceAttribute_t1638101999 *)cache->attributes[1];
		ClassInterfaceAttribute__ctor_m2759037627(tmp, 0LL, NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[2];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void AssemblyNameFlags_t899890916_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		FlagsAttribute_t388727259 * tmp = (FlagsAttribute_t388727259 *)cache->attributes[0];
		FlagsAttribute__ctor_m3834565173(tmp, NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[1];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void AssemblyProductAttribute_t3365982468_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_t2370971113 * tmp = (AttributeUsageAttribute_t2370971113 *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m1170715599(tmp, 1LL, NULL);
		AttributeUsageAttribute_set_Inherited_m2205070461(tmp, false, NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[1];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void AssemblyTitleAttribute_t2942119159_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_t2370971113 * tmp = (AttributeUsageAttribute_t2370971113 *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m1170715599(tmp, 1LL, NULL);
		AttributeUsageAttribute_set_Inherited_m2205070461(tmp, false, NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[1];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void Binder_t2856902408_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		ClassInterfaceAttribute_t1638101999 * tmp = (ClassInterfaceAttribute_t1638101999 *)cache->attributes[1];
		ClassInterfaceAttribute__ctor_m2759037627(tmp, 2LL, NULL);
	}
}
static void Default_t611799287_CustomAttributesCacheGenerator_Default_ReorderArgumentArray_m266338901(CustomAttributesCache* cache)
{
	{
		MonoTODOAttribute_t614642062 * tmp = (MonoTODOAttribute_t614642062 *)cache->attributes[0];
		MonoTODOAttribute__ctor_m2689597370(tmp, il2cpp_codegen_string_new_wrapper("This method does not do anything in Mono"), NULL);
	}
}
static void BindingFlags_t3268090012_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		FlagsAttribute_t388727259 * tmp = (FlagsAttribute_t388727259 *)cache->attributes[0];
		FlagsAttribute__ctor_m3834565173(tmp, NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[1];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void CallingConventions_t1168634181_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		FlagsAttribute_t388727259 * tmp = (FlagsAttribute_t388727259 *)cache->attributes[0];
		FlagsAttribute__ctor_m3834565173(tmp, NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[1];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void ConstructorInfo_t3360520918_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ConstructorInfo_t3360520918_CustomAttributesCacheGenerator_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		ComDefaultInterfaceAttribute_t2399868801 * tmp = (ComDefaultInterfaceAttribute_t2399868801 *)cache->attributes[1];
		ComDefaultInterfaceAttribute__ctor_m1937741735(tmp, il2cpp_codegen_type_get_object(_ConstructorInfo_t1952808443_0_0_0_var), NULL);
	}
	{
		ClassInterfaceAttribute_t1638101999 * tmp = (ClassInterfaceAttribute_t1638101999 *)cache->attributes[2];
		ClassInterfaceAttribute__ctor_m2759037627(tmp, 0LL, NULL);
	}
}
static void ConstructorInfo_t3360520918_CustomAttributesCacheGenerator_ConstructorName(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void ConstructorInfo_t3360520918_CustomAttributesCacheGenerator_TypeConstructorName(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void ConstructorInfo_t3360520918_CustomAttributesCacheGenerator_ConstructorInfo_Invoke_m4178483106(CustomAttributesCache* cache)
{
	{
		DebuggerStepThroughAttribute_t3823612831 * tmp = (DebuggerStepThroughAttribute_t3823612831 *)cache->attributes[0];
		DebuggerStepThroughAttribute__ctor_m1431182322(tmp, NULL);
	}
	{
		DebuggerHiddenAttribute_t1447627238 * tmp = (DebuggerHiddenAttribute_t1447627238 *)cache->attributes[1];
		DebuggerHiddenAttribute__ctor_m749702160(tmp, NULL);
	}
}
static void ConstructorInfo_t3360520918_CustomAttributesCacheGenerator_ConstructorInfo_t3360520918____MemberType_PropertyInfo(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void CustomAttributeData_t3408840380_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void CustomAttributeData_t3408840380_CustomAttributesCacheGenerator_CustomAttributeData_t3408840380____Constructor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void CustomAttributeData_t3408840380_CustomAttributesCacheGenerator_CustomAttributeData_t3408840380____ConstructorArguments_PropertyInfo(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void CustomAttributeNamedArgument_t425730339_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void CustomAttributeTypedArgument_t2667702721_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void EventAttributes_t1621333428_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		FlagsAttribute_t388727259 * tmp = (FlagsAttribute_t388727259 *)cache->attributes[1];
		FlagsAttribute__ctor_m3834565173(tmp, NULL);
	}
}
static void EventInfo_t_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EventInfo_t_CustomAttributesCacheGenerator_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		ComDefaultInterfaceAttribute_t2399868801 * tmp = (ComDefaultInterfaceAttribute_t2399868801 *)cache->attributes[1];
		ComDefaultInterfaceAttribute__ctor_m1937741735(tmp, il2cpp_codegen_type_get_object(_EventInfo_t1492620845_0_0_0_var), NULL);
	}
	{
		ClassInterfaceAttribute_t1638101999 * tmp = (ClassInterfaceAttribute_t1638101999 *)cache->attributes[2];
		ClassInterfaceAttribute__ctor_m2759037627(tmp, 0LL, NULL);
	}
}
static void FieldAttributes_t1107365117_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		FlagsAttribute_t388727259 * tmp = (FlagsAttribute_t388727259 *)cache->attributes[0];
		FlagsAttribute__ctor_m3834565173(tmp, NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[1];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void FieldInfo_t_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FieldInfo_t_CustomAttributesCacheGenerator_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ClassInterfaceAttribute_t1638101999 * tmp = (ClassInterfaceAttribute_t1638101999 *)cache->attributes[0];
		ClassInterfaceAttribute__ctor_m2759037627(tmp, 0LL, NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[1];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		ComDefaultInterfaceAttribute_t2399868801 * tmp = (ComDefaultInterfaceAttribute_t2399868801 *)cache->attributes[2];
		ComDefaultInterfaceAttribute__ctor_m1937741735(tmp, il2cpp_codegen_type_get_object(_FieldInfo_t3281021298_0_0_0_var), NULL);
	}
}
static void FieldInfo_t_CustomAttributesCacheGenerator_FieldInfo_SetValue_m1839240114(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_t1447627238 * tmp = (DebuggerHiddenAttribute_t1447627238 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_m749702160(tmp, NULL);
	}
	{
		DebuggerStepThroughAttribute_t3823612831 * tmp = (DebuggerStepThroughAttribute_t3823612831 *)cache->attributes[1];
		DebuggerStepThroughAttribute__ctor_m1431182322(tmp, NULL);
	}
}
static void MemberTypes_t2339173058_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		FlagsAttribute_t388727259 * tmp = (FlagsAttribute_t388727259 *)cache->attributes[0];
		FlagsAttribute__ctor_m3834565173(tmp, NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[1];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void MethodAttributes_t3855721989_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		FlagsAttribute_t388727259 * tmp = (FlagsAttribute_t388727259 *)cache->attributes[1];
		FlagsAttribute__ctor_m3834565173(tmp, NULL);
	}
}
static void MethodBase_t674153939_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MethodBase_t674153939_CustomAttributesCacheGenerator_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ClassInterfaceAttribute_t1638101999 * tmp = (ClassInterfaceAttribute_t1638101999 *)cache->attributes[0];
		ClassInterfaceAttribute__ctor_m2759037627(tmp, 0LL, NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[1];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		ComDefaultInterfaceAttribute_t2399868801 * tmp = (ComDefaultInterfaceAttribute_t2399868801 *)cache->attributes[2];
		ComDefaultInterfaceAttribute__ctor_m1937741735(tmp, il2cpp_codegen_type_get_object(_MethodBase_t3352748864_0_0_0_var), NULL);
	}
}
static void MethodBase_t674153939_CustomAttributesCacheGenerator_MethodBase_Invoke_m3351713316(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_t1447627238 * tmp = (DebuggerHiddenAttribute_t1447627238 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_m749702160(tmp, NULL);
	}
	{
		DebuggerStepThroughAttribute_t3823612831 * tmp = (DebuggerStepThroughAttribute_t3823612831 *)cache->attributes[1];
		DebuggerStepThroughAttribute__ctor_m1431182322(tmp, NULL);
	}
}
static void MethodBase_t674153939_CustomAttributesCacheGenerator_MethodBase_GetGenericArguments_m3457019161(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void MethodImplAttributes_t3639404534_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void MethodInfo_t_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MethodInfo_t_CustomAttributesCacheGenerator_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ClassInterfaceAttribute_t1638101999 * tmp = (ClassInterfaceAttribute_t1638101999 *)cache->attributes[0];
		ClassInterfaceAttribute__ctor_m2759037627(tmp, 0LL, NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[1];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		ComDefaultInterfaceAttribute_t2399868801 * tmp = (ComDefaultInterfaceAttribute_t2399868801 *)cache->attributes[2];
		ComDefaultInterfaceAttribute__ctor_m1937741735(tmp, il2cpp_codegen_type_get_object(_MethodInfo_t643827776_0_0_0_var), NULL);
	}
}
static void MethodInfo_t_CustomAttributesCacheGenerator_MethodInfo_MakeGenericMethod_m1848918275____typeArguments0(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t3330428127 * tmp = (ParamArrayAttribute_t3330428127 *)cache->attributes[0];
		ParamArrayAttribute__ctor_m1696407647(tmp, NULL);
	}
}
static void MethodInfo_t_CustomAttributesCacheGenerator_MethodInfo_GetGenericArguments_m254251600(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void Missing_t4029427245_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void Module_t2594760207_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Module_t2594760207_CustomAttributesCacheGenerator_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ComDefaultInterfaceAttribute_t2399868801 * tmp = (ComDefaultInterfaceAttribute_t2399868801 *)cache->attributes[0];
		ComDefaultInterfaceAttribute__ctor_m1937741735(tmp, il2cpp_codegen_type_get_object(_Module_t494242506_0_0_0_var), NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[1];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		ClassInterfaceAttribute_t1638101999 * tmp = (ClassInterfaceAttribute_t1638101999 *)cache->attributes[2];
		ClassInterfaceAttribute__ctor_m2759037627(tmp, 0LL, NULL);
	}
}
static void PInfo_t3368652995_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		FlagsAttribute_t388727259 * tmp = (FlagsAttribute_t388727259 *)cache->attributes[0];
		FlagsAttribute__ctor_m3834565173(tmp, NULL);
	}
}
static void ParameterAttributes_t3383706087_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		FlagsAttribute_t388727259 * tmp = (FlagsAttribute_t388727259 *)cache->attributes[1];
		FlagsAttribute__ctor_m3834565173(tmp, NULL);
	}
}
static void ParameterInfo_t3893964560_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ParameterInfo_t3893964560_CustomAttributesCacheGenerator_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ComDefaultInterfaceAttribute_t2399868801 * tmp = (ComDefaultInterfaceAttribute_t2399868801 *)cache->attributes[0];
		ComDefaultInterfaceAttribute__ctor_m1937741735(tmp, il2cpp_codegen_type_get_object(_ParameterInfo_t3152035282_0_0_0_var), NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[1];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		ClassInterfaceAttribute_t1638101999 * tmp = (ClassInterfaceAttribute_t1638101999 *)cache->attributes[2];
		ClassInterfaceAttribute__ctor_m2759037627(tmp, 0LL, NULL);
	}
}
static void ParameterModifier_t969812002_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DefaultMemberAttribute_t2956431904 * tmp = (DefaultMemberAttribute_t2956431904 *)cache->attributes[0];
		DefaultMemberAttribute__ctor_m3129895323(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[1];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void Pointer_t3648858837_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[1];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void ProcessorArchitecture_t1185490558_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void PropertyAttributes_t820793723_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		FlagsAttribute_t388727259 * tmp = (FlagsAttribute_t388727259 *)cache->attributes[0];
		FlagsAttribute__ctor_m3834565173(tmp, NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[1];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void PropertyInfo_t_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PropertyInfo_t_CustomAttributesCacheGenerator_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ComDefaultInterfaceAttribute_t2399868801 * tmp = (ComDefaultInterfaceAttribute_t2399868801 *)cache->attributes[0];
		ComDefaultInterfaceAttribute__ctor_m1937741735(tmp, il2cpp_codegen_type_get_object(_PropertyInfo_t2517621214_0_0_0_var), NULL);
	}
	{
		ClassInterfaceAttribute_t1638101999 * tmp = (ClassInterfaceAttribute_t1638101999 *)cache->attributes[1];
		ClassInterfaceAttribute__ctor_m2759037627(tmp, 0LL, NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[2];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void PropertyInfo_t_CustomAttributesCacheGenerator_PropertyInfo_GetValue_m1153076048(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_t1447627238 * tmp = (DebuggerHiddenAttribute_t1447627238 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_m749702160(tmp, NULL);
	}
	{
		DebuggerStepThroughAttribute_t3823612831 * tmp = (DebuggerStepThroughAttribute_t3823612831 *)cache->attributes[1];
		DebuggerStepThroughAttribute__ctor_m1431182322(tmp, NULL);
	}
}
static void PropertyInfo_t_CustomAttributesCacheGenerator_PropertyInfo_SetValue_m3460960658(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_t1447627238 * tmp = (DebuggerHiddenAttribute_t1447627238 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_m749702160(tmp, NULL);
	}
	{
		DebuggerStepThroughAttribute_t3823612831 * tmp = (DebuggerStepThroughAttribute_t3823612831 *)cache->attributes[1];
		DebuggerStepThroughAttribute__ctor_m1431182322(tmp, NULL);
	}
}
static void ResourceAttributes_t860636606_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		FlagsAttribute_t388727259 * tmp = (FlagsAttribute_t388727259 *)cache->attributes[0];
		FlagsAttribute__ctor_m3834565173(tmp, NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[1];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void StrongNameKeyPair_t4000448818_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void TargetException_t2386226798_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void TargetInvocationException_t2320458230_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void TargetParameterCountException_t26661376_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void TypeAttributes_t3122562390_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		FlagsAttribute_t388727259 * tmp = (FlagsAttribute_t388727259 *)cache->attributes[0];
		FlagsAttribute__ctor_m3834565173(tmp, NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[1];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void IResourceReader_t3322992291_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void NeutralResourcesLanguageAttribute_t2446220560_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_t2370971113 * tmp = (AttributeUsageAttribute_t2370971113 *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m1170715599(tmp, 1LL, NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[1];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void ResourceManager_t360577983_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void ResourceReader_t974344632_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void ResourceSet_t2731525910_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void ResourceSet_t2731525910_CustomAttributesCacheGenerator_ResourceSet_GetEnumerator_m2033896022(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, false, NULL);
	}
}
static void SatelliteContractVersionAttribute_t2421164883_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		AttributeUsageAttribute_t2370971113 * tmp = (AttributeUsageAttribute_t2370971113 *)cache->attributes[1];
		AttributeUsageAttribute__ctor_m1170715599(tmp, 1LL, NULL);
	}
}
static void CompilationRelaxations_t3691856549_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		FlagsAttribute_t388727259 * tmp = (FlagsAttribute_t388727259 *)cache->attributes[0];
		FlagsAttribute__ctor_m3834565173(tmp, NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[1];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void CompilationRelaxationsAttribute_t606593683_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		AttributeUsageAttribute_t2370971113 * tmp = (AttributeUsageAttribute_t2370971113 *)cache->attributes[1];
		AttributeUsageAttribute__ctor_m1170715599(tmp, 71LL, NULL);
	}
}
static void DefaultDependencyAttribute_t2058824218_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_t2370971113 * tmp = (AttributeUsageAttribute_t2370971113 *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m1170715599(tmp, 1LL, NULL);
	}
}
static void IsVolatile_t2822106667_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void StringFreezingAttribute_t4005798349_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_t2370971113 * tmp = (AttributeUsageAttribute_t2370971113 *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m1170715599(tmp, 1LL, NULL);
		AttributeUsageAttribute_set_Inherited_m2205070461(tmp, false, NULL);
	}
}
static void CriticalFinalizerObject_t2148808298_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void CriticalFinalizerObject_t2148808298_CustomAttributesCacheGenerator_CriticalFinalizerObject__ctor_m1083616025(CustomAttributesCache* cache)
{
	{
		ReliabilityContractAttribute_t3009184403 * tmp = (ReliabilityContractAttribute_t3009184403 *)cache->attributes[0];
		ReliabilityContractAttribute__ctor_m2586077688(tmp, 3LL, 1LL, NULL);
	}
}
static void CriticalFinalizerObject_t2148808298_CustomAttributesCacheGenerator_CriticalFinalizerObject_Finalize_m82872096(CustomAttributesCache* cache)
{
	{
		ReliabilityContractAttribute_t3009184403 * tmp = (ReliabilityContractAttribute_t3009184403 *)cache->attributes[0];
		ReliabilityContractAttribute__ctor_m2586077688(tmp, 3LL, 2LL, NULL);
	}
}
static void ReliabilityContractAttribute_t3009184403_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_t2370971113 * tmp = (AttributeUsageAttribute_t2370971113 *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m1170715599(tmp, 1133LL, NULL);
		AttributeUsageAttribute_set_Inherited_m2205070461(tmp, false, NULL);
	}
}
static void ActivationArguments_t3282869617_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void COMException_t2943129929_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void CallingConvention_t324586818_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void CharSet_t2499940461_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void ClassInterfaceAttribute_t1638101999_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_t2370971113 * tmp = (AttributeUsageAttribute_t2370971113 *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m1170715599(tmp, 5LL, NULL);
		AttributeUsageAttribute_set_Inherited_m2205070461(tmp, false, NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[1];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void ClassInterfaceType_t3248553764_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void ComDefaultInterfaceAttribute_t2399868801_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		AttributeUsageAttribute_t2370971113 * tmp = (AttributeUsageAttribute_t2370971113 *)cache->attributes[1];
		AttributeUsageAttribute__ctor_m1170715599(tmp, 4LL, NULL);
		AttributeUsageAttribute_set_Inherited_m2205070461(tmp, false, NULL);
	}
}
static void ComInterfaceType_t1249722599_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void DispIdAttribute_t3503809048_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_t2370971113 * tmp = (AttributeUsageAttribute_t2370971113 *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m1170715599(tmp, 960LL, NULL);
		AttributeUsageAttribute_set_Inherited_m2205070461(tmp, false, NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[1];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void ErrorWrapper_t2333809499_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void ExternalException_t1814919774_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void GCHandle_t4215464244_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		MonoTODOAttribute_t614642062 * tmp = (MonoTODOAttribute_t614642062 *)cache->attributes[0];
		MonoTODOAttribute__ctor_m2689597370(tmp, il2cpp_codegen_string_new_wrapper("Struct should be [StructLayout(LayoutKind.Sequential)] but will need to be reordered for that."), NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[1];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void GCHandleType_t1285395773_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void InterfaceTypeAttribute_t3336632297_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		AttributeUsageAttribute_t2370971113 * tmp = (AttributeUsageAttribute_t2370971113 *)cache->attributes[1];
		AttributeUsageAttribute__ctor_m1170715599(tmp, 1024LL, NULL);
		AttributeUsageAttribute_set_Inherited_m2205070461(tmp, false, NULL);
	}
}
static void Marshal_t4207306895_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		SuppressUnmanagedCodeSecurityAttribute_t1135819281 * tmp = (SuppressUnmanagedCodeSecurityAttribute_t1135819281 *)cache->attributes[0];
		SuppressUnmanagedCodeSecurityAttribute__ctor_m1831174959(tmp, NULL);
	}
}
static void MarshalDirectiveException_t4051754843_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void PreserveSigAttribute_t877897876_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_t2370971113 * tmp = (AttributeUsageAttribute_t2370971113 *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m1170715599(tmp, 64LL, NULL);
		AttributeUsageAttribute_set_Inherited_m2205070461(tmp, false, NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[1];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void SafeHandle_t1062625530_CustomAttributesCacheGenerator_SafeHandle__ctor_m20409610(CustomAttributesCache* cache)
{
	{
		ReliabilityContractAttribute_t3009184403 * tmp = (ReliabilityContractAttribute_t3009184403 *)cache->attributes[0];
		ReliabilityContractAttribute__ctor_m2586077688(tmp, 3LL, 1LL, NULL);
	}
}
static void SafeHandle_t1062625530_CustomAttributesCacheGenerator_SafeHandle_Close_m106241507(CustomAttributesCache* cache)
{
	{
		ReliabilityContractAttribute_t3009184403 * tmp = (ReliabilityContractAttribute_t3009184403 *)cache->attributes[0];
		ReliabilityContractAttribute__ctor_m2586077688(tmp, 3LL, 2LL, NULL);
	}
}
static void SafeHandle_t1062625530_CustomAttributesCacheGenerator_SafeHandle_DangerousAddRef_m3068760737(CustomAttributesCache* cache)
{
	{
		ReliabilityContractAttribute_t3009184403 * tmp = (ReliabilityContractAttribute_t3009184403 *)cache->attributes[0];
		ReliabilityContractAttribute__ctor_m2586077688(tmp, 3LL, 1LL, NULL);
	}
}
static void SafeHandle_t1062625530_CustomAttributesCacheGenerator_SafeHandle_DangerousGetHandle_m987171088(CustomAttributesCache* cache)
{
	{
		ReliabilityContractAttribute_t3009184403 * tmp = (ReliabilityContractAttribute_t3009184403 *)cache->attributes[0];
		ReliabilityContractAttribute__ctor_m2586077688(tmp, 3LL, 2LL, NULL);
	}
}
static void SafeHandle_t1062625530_CustomAttributesCacheGenerator_SafeHandle_DangerousRelease_m2343451392(CustomAttributesCache* cache)
{
	{
		ReliabilityContractAttribute_t3009184403 * tmp = (ReliabilityContractAttribute_t3009184403 *)cache->attributes[0];
		ReliabilityContractAttribute__ctor_m2586077688(tmp, 3LL, 2LL, NULL);
	}
}
static void SafeHandle_t1062625530_CustomAttributesCacheGenerator_SafeHandle_Dispose_m3045814524(CustomAttributesCache* cache)
{
	{
		ReliabilityContractAttribute_t3009184403 * tmp = (ReliabilityContractAttribute_t3009184403 *)cache->attributes[0];
		ReliabilityContractAttribute__ctor_m2586077688(tmp, 3LL, 2LL, NULL);
	}
}
static void SafeHandle_t1062625530_CustomAttributesCacheGenerator_SafeHandle_Dispose_m1778975602(CustomAttributesCache* cache)
{
	{
		ReliabilityContractAttribute_t3009184403 * tmp = (ReliabilityContractAttribute_t3009184403 *)cache->attributes[0];
		ReliabilityContractAttribute__ctor_m2586077688(tmp, 3LL, 2LL, NULL);
	}
}
static void SafeHandle_t1062625530_CustomAttributesCacheGenerator_SafeHandle_ReleaseHandle_m3593420879(CustomAttributesCache* cache)
{
	{
		ReliabilityContractAttribute_t3009184403 * tmp = (ReliabilityContractAttribute_t3009184403 *)cache->attributes[0];
		ReliabilityContractAttribute__ctor_m2586077688(tmp, 3LL, 2LL, NULL);
	}
}
static void SafeHandle_t1062625530_CustomAttributesCacheGenerator_SafeHandle_SetHandle_m105306455(CustomAttributesCache* cache)
{
	{
		ReliabilityContractAttribute_t3009184403 * tmp = (ReliabilityContractAttribute_t3009184403 *)cache->attributes[0];
		ReliabilityContractAttribute__ctor_m2586077688(tmp, 3LL, 2LL, NULL);
	}
}
static void SafeHandle_t1062625530_CustomAttributesCacheGenerator_SafeHandle_get_IsInvalid_m3128616911(CustomAttributesCache* cache)
{
	{
		ReliabilityContractAttribute_t3009184403 * tmp = (ReliabilityContractAttribute_t3009184403 *)cache->attributes[0];
		ReliabilityContractAttribute__ctor_m2586077688(tmp, 3LL, 2LL, NULL);
	}
}
static void TypeLibImportClassAttribute_t2173872460_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		AttributeUsageAttribute_t2370971113 * tmp = (AttributeUsageAttribute_t2370971113 *)cache->attributes[1];
		AttributeUsageAttribute__ctor_m1170715599(tmp, 1024LL, NULL);
		AttributeUsageAttribute_set_Inherited_m2205070461(tmp, false, NULL);
	}
}
static void TypeLibVersionAttribute_t1866475684_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_t2370971113 * tmp = (AttributeUsageAttribute_t2370971113 *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m1170715599(tmp, 1LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m1313295265(tmp, false, NULL);
		AttributeUsageAttribute_set_Inherited_m2205070461(tmp, false, NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[1];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void UnmanagedFunctionPointerAttribute_t1887822751_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		AttributeUsageAttribute_t2370971113 * tmp = (AttributeUsageAttribute_t2370971113 *)cache->attributes[1];
		AttributeUsageAttribute__ctor_m1170715599(tmp, 4096LL, NULL);
		AttributeUsageAttribute_set_Inherited_m2205070461(tmp, false, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m1313295265(tmp, false, NULL);
	}
}
static void UnmanagedType_t723771350_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void _Activator_t134942278_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_Activator_t134942278_CustomAttributesCacheGenerator_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[1];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
	{
		GuidAttribute_t3853718442 * tmp = (GuidAttribute_t3853718442 *)cache->attributes[2];
		GuidAttribute__ctor_m612200210(tmp, il2cpp_codegen_string_new_wrapper("03973551-57A1-3900-A2B5-9083E3FF2943"), NULL);
	}
	{
		InterfaceTypeAttribute_t3336632297 * tmp = (InterfaceTypeAttribute_t3336632297 *)cache->attributes[3];
		InterfaceTypeAttribute__ctor_m342415882(tmp, 1LL, NULL);
	}
	{
		TypeLibImportClassAttribute_t2173872460 * tmp = (TypeLibImportClassAttribute_t2173872460 *)cache->attributes[4];
		TypeLibImportClassAttribute__ctor_m1502349925(tmp, il2cpp_codegen_type_get_object(Activator_t214179882_0_0_0_var), NULL);
	}
}
static void _Assembly_t3106968071_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_Assembly_t3106968071_CustomAttributesCacheGenerator_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GuidAttribute_t3853718442 * tmp = (GuidAttribute_t3853718442 *)cache->attributes[0];
		GuidAttribute__ctor_m612200210(tmp, il2cpp_codegen_string_new_wrapper("17156360-2F1A-384A-BC52-FDE93C215C5B"), NULL);
	}
	{
		InterfaceTypeAttribute_t3336632297 * tmp = (InterfaceTypeAttribute_t3336632297 *)cache->attributes[1];
		InterfaceTypeAttribute__ctor_m342415882(tmp, 0LL, NULL);
	}
	{
		TypeLibImportClassAttribute_t2173872460 * tmp = (TypeLibImportClassAttribute_t2173872460 *)cache->attributes[2];
		TypeLibImportClassAttribute__ctor_m1502349925(tmp, il2cpp_codegen_type_get_object(Assembly_t2665685420_0_0_0_var), NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[3];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[4];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void _AssemblyBuilder_t2438890953_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_AssemblyBuilder_t2438890953_CustomAttributesCacheGenerator_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		TypeLibImportClassAttribute_t2173872460 * tmp = (TypeLibImportClassAttribute_t2173872460 *)cache->attributes[0];
		TypeLibImportClassAttribute__ctor_m1502349925(tmp, il2cpp_codegen_type_get_object(AssemblyBuilder_t489430578_0_0_0_var), NULL);
	}
	{
		GuidAttribute_t3853718442 * tmp = (GuidAttribute_t3853718442 *)cache->attributes[1];
		GuidAttribute__ctor_m612200210(tmp, il2cpp_codegen_string_new_wrapper("BEBB2505-8B54-3443-AEAD-142A16DD9CC7"), NULL);
	}
	{
		InterfaceTypeAttribute_t3336632297 * tmp = (InterfaceTypeAttribute_t3336632297 *)cache->attributes[2];
		InterfaceTypeAttribute__ctor_m342415882(tmp, 1LL, NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[3];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[4];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void _AssemblyName_t349687559_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_AssemblyName_t349687559_CustomAttributesCacheGenerator_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		TypeLibImportClassAttribute_t2173872460 * tmp = (TypeLibImportClassAttribute_t2173872460 *)cache->attributes[0];
		TypeLibImportClassAttribute__ctor_m1502349925(tmp, il2cpp_codegen_type_get_object(AssemblyName_t997940415_0_0_0_var), NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[1];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		InterfaceTypeAttribute_t3336632297 * tmp = (InterfaceTypeAttribute_t3336632297 *)cache->attributes[2];
		InterfaceTypeAttribute__ctor_m342415882(tmp, 1LL, NULL);
	}
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[3];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
	{
		GuidAttribute_t3853718442 * tmp = (GuidAttribute_t3853718442 *)cache->attributes[4];
		GuidAttribute__ctor_m612200210(tmp, il2cpp_codegen_string_new_wrapper("B42B6AAC-317E-34D5-9FA9-093BB4160C50"), NULL);
	}
}
static void _ConstructorBuilder_t3355697174_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_ConstructorBuilder_t3355697174_CustomAttributesCacheGenerator_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		TypeLibImportClassAttribute_t2173872460 * tmp = (TypeLibImportClassAttribute_t2173872460 *)cache->attributes[0];
		TypeLibImportClassAttribute__ctor_m1502349925(tmp, il2cpp_codegen_type_get_object(ConstructorBuilder_t1596923965_0_0_0_var), NULL);
	}
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[1];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
	{
		InterfaceTypeAttribute_t3336632297 * tmp = (InterfaceTypeAttribute_t3336632297 *)cache->attributes[2];
		InterfaceTypeAttribute__ctor_m342415882(tmp, 1LL, NULL);
	}
	{
		GuidAttribute_t3853718442 * tmp = (GuidAttribute_t3853718442 *)cache->attributes[3];
		GuidAttribute__ctor_m612200210(tmp, il2cpp_codegen_string_new_wrapper("ED3E4384-D7E2-3FA7-8FFD-8940D330519A"), NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[4];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void _ConstructorInfo_t1952808443_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_ConstructorInfo_t1952808443_CustomAttributesCacheGenerator_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		TypeLibImportClassAttribute_t2173872460 * tmp = (TypeLibImportClassAttribute_t2173872460 *)cache->attributes[0];
		TypeLibImportClassAttribute__ctor_m1502349925(tmp, il2cpp_codegen_type_get_object(ConstructorInfo_t3360520918_0_0_0_var), NULL);
	}
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[1];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
	{
		GuidAttribute_t3853718442 * tmp = (GuidAttribute_t3853718442 *)cache->attributes[2];
		GuidAttribute__ctor_m612200210(tmp, il2cpp_codegen_string_new_wrapper("E9A19478-9646-3679-9B10-8411AE1FD57D"), NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[3];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		InterfaceTypeAttribute_t3336632297 * tmp = (InterfaceTypeAttribute_t3336632297 *)cache->attributes[4];
		InterfaceTypeAttribute__ctor_m342415882(tmp, 1LL, NULL);
	}
}
static void _CustomAttributeBuilder_t2651440002_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_CustomAttributeBuilder_t2651440002_CustomAttributesCacheGenerator_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		TypeLibImportClassAttribute_t2173872460 * tmp = (TypeLibImportClassAttribute_t2173872460 *)cache->attributes[1];
		TypeLibImportClassAttribute__ctor_m1502349925(tmp, il2cpp_codegen_type_get_object(CustomAttributeBuilder_t1432176696_0_0_0_var), NULL);
	}
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[2];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
	{
		GuidAttribute_t3853718442 * tmp = (GuidAttribute_t3853718442 *)cache->attributes[3];
		GuidAttribute__ctor_m612200210(tmp, il2cpp_codegen_string_new_wrapper("BE9ACCE8-AAFF-3B91-81AE-8211663F5CAD"), NULL);
	}
	{
		InterfaceTypeAttribute_t3336632297 * tmp = (InterfaceTypeAttribute_t3336632297 *)cache->attributes[4];
		InterfaceTypeAttribute__ctor_m342415882(tmp, 1LL, NULL);
	}
}
static void _EnumBuilder_t3195197362_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_EnumBuilder_t3195197362_CustomAttributesCacheGenerator_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		TypeLibImportClassAttribute_t2173872460 * tmp = (TypeLibImportClassAttribute_t2173872460 *)cache->attributes[0];
		TypeLibImportClassAttribute__ctor_m1502349925(tmp, il2cpp_codegen_type_get_object(EnumBuilder_t617843_0_0_0_var), NULL);
	}
	{
		GuidAttribute_t3853718442 * tmp = (GuidAttribute_t3853718442 *)cache->attributes[1];
		GuidAttribute__ctor_m612200210(tmp, il2cpp_codegen_string_new_wrapper("C7BD73DE-9F85-3290-88EE-090B8BDFE2DF"), NULL);
	}
	{
		InterfaceTypeAttribute_t3336632297 * tmp = (InterfaceTypeAttribute_t3336632297 *)cache->attributes[2];
		InterfaceTypeAttribute__ctor_m342415882(tmp, 1LL, NULL);
	}
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[3];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[4];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void _EventBuilder_t3169829690_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_EventBuilder_t3169829690_CustomAttributesCacheGenerator_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GuidAttribute_t3853718442 * tmp = (GuidAttribute_t3853718442 *)cache->attributes[0];
		GuidAttribute__ctor_m612200210(tmp, il2cpp_codegen_string_new_wrapper("AADABA99-895D-3D65-9760-B1F12621FAE8"), NULL);
	}
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[1];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
	{
		InterfaceTypeAttribute_t3336632297 * tmp = (InterfaceTypeAttribute_t3336632297 *)cache->attributes[2];
		InterfaceTypeAttribute__ctor_m342415882(tmp, 1LL, NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[3];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		TypeLibImportClassAttribute_t2173872460 * tmp = (TypeLibImportClassAttribute_t2173872460 *)cache->attributes[4];
		TypeLibImportClassAttribute__ctor_m1502349925(tmp, il2cpp_codegen_type_get_object(EventBuilder_t2300762438_0_0_0_var), NULL);
	}
}
static void _EventInfo_t1492620845_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_EventInfo_t1492620845_CustomAttributesCacheGenerator_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		InterfaceTypeAttribute_t3336632297 * tmp = (InterfaceTypeAttribute_t3336632297 *)cache->attributes[0];
		InterfaceTypeAttribute__ctor_m342415882(tmp, 1LL, NULL);
	}
	{
		GuidAttribute_t3853718442 * tmp = (GuidAttribute_t3853718442 *)cache->attributes[1];
		GuidAttribute__ctor_m612200210(tmp, il2cpp_codegen_string_new_wrapper("9DE59C64-D889-35A1-B897-587D74469E5B"), NULL);
	}
	{
		TypeLibImportClassAttribute_t2173872460 * tmp = (TypeLibImportClassAttribute_t2173872460 *)cache->attributes[2];
		TypeLibImportClassAttribute__ctor_m1502349925(tmp, il2cpp_codegen_type_get_object(EventInfo_t_0_0_0_var), NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[3];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[4];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void _FieldBuilder_t3486190427_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_FieldBuilder_t3486190427_CustomAttributesCacheGenerator_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GuidAttribute_t3853718442 * tmp = (GuidAttribute_t3853718442 *)cache->attributes[0];
		GuidAttribute__ctor_m612200210(tmp, il2cpp_codegen_string_new_wrapper("CE1A3BF5-975E-30CC-97C9-1EF70F8F3993"), NULL);
	}
	{
		InterfaceTypeAttribute_t3336632297 * tmp = (InterfaceTypeAttribute_t3336632297 *)cache->attributes[1];
		InterfaceTypeAttribute__ctor_m342415882(tmp, 1LL, NULL);
	}
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[2];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
	{
		TypeLibImportClassAttribute_t2173872460 * tmp = (TypeLibImportClassAttribute_t2173872460 *)cache->attributes[3];
		TypeLibImportClassAttribute__ctor_m1502349925(tmp, il2cpp_codegen_type_get_object(FieldBuilder_t803025013_0_0_0_var), NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[4];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void _FieldInfo_t3281021298_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_FieldInfo_t3281021298_CustomAttributesCacheGenerator_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
	{
		TypeLibImportClassAttribute_t2173872460 * tmp = (TypeLibImportClassAttribute_t2173872460 *)cache->attributes[1];
		TypeLibImportClassAttribute__ctor_m1502349925(tmp, il2cpp_codegen_type_get_object(FieldInfo_t_0_0_0_var), NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[2];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		InterfaceTypeAttribute_t3336632297 * tmp = (InterfaceTypeAttribute_t3336632297 *)cache->attributes[3];
		InterfaceTypeAttribute__ctor_m342415882(tmp, 1LL, NULL);
	}
	{
		GuidAttribute_t3853718442 * tmp = (GuidAttribute_t3853718442 *)cache->attributes[4];
		GuidAttribute__ctor_m612200210(tmp, il2cpp_codegen_string_new_wrapper("8A7C1442-A9FB-366B-80D8-4939FFA6DBE0"), NULL);
	}
}
static void _ILGenerator_t3086005605_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_ILGenerator_t3086005605_CustomAttributesCacheGenerator_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		InterfaceTypeAttribute_t3336632297 * tmp = (InterfaceTypeAttribute_t3336632297 *)cache->attributes[0];
		InterfaceTypeAttribute__ctor_m342415882(tmp, 1LL, NULL);
	}
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[1];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[2];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		TypeLibImportClassAttribute_t2173872460 * tmp = (TypeLibImportClassAttribute_t2173872460 *)cache->attributes[3];
		TypeLibImportClassAttribute__ctor_m1502349925(tmp, il2cpp_codegen_type_get_object(ILGenerator_t1111467769_0_0_0_var), NULL);
	}
	{
		GuidAttribute_t3853718442 * tmp = (GuidAttribute_t3853718442 *)cache->attributes[4];
		GuidAttribute__ctor_m612200210(tmp, il2cpp_codegen_string_new_wrapper("A4924B27-6E3B-37F7-9B83-A4501955E6A7"), NULL);
	}
}
static void _MethodBase_t3352748864_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_MethodBase_t3352748864_CustomAttributesCacheGenerator_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GuidAttribute_t3853718442 * tmp = (GuidAttribute_t3853718442 *)cache->attributes[0];
		GuidAttribute__ctor_m612200210(tmp, il2cpp_codegen_string_new_wrapper("6240837A-707F-3181-8E98-A36AE086766B"), NULL);
	}
	{
		InterfaceTypeAttribute_t3336632297 * tmp = (InterfaceTypeAttribute_t3336632297 *)cache->attributes[1];
		InterfaceTypeAttribute__ctor_m342415882(tmp, 1LL, NULL);
	}
	{
		TypeLibImportClassAttribute_t2173872460 * tmp = (TypeLibImportClassAttribute_t2173872460 *)cache->attributes[2];
		TypeLibImportClassAttribute__ctor_m1502349925(tmp, il2cpp_codegen_type_get_object(MethodBase_t674153939_0_0_0_var), NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[3];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[4];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void _MethodBuilder_t195532601_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_MethodBuilder_t195532601_CustomAttributesCacheGenerator_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		TypeLibImportClassAttribute_t2173872460 * tmp = (TypeLibImportClassAttribute_t2173872460 *)cache->attributes[0];
		TypeLibImportClassAttribute__ctor_m1502349925(tmp, il2cpp_codegen_type_get_object(MethodBuilder_t2964638190_0_0_0_var), NULL);
	}
	{
		GuidAttribute_t3853718442 * tmp = (GuidAttribute_t3853718442 *)cache->attributes[1];
		GuidAttribute__ctor_m612200210(tmp, il2cpp_codegen_string_new_wrapper("007D8A14-FDF3-363E-9A0B-FEC0618260A2"), NULL);
	}
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[2];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[3];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		InterfaceTypeAttribute_t3336632297 * tmp = (InterfaceTypeAttribute_t3336632297 *)cache->attributes[4];
		InterfaceTypeAttribute__ctor_m342415882(tmp, 1LL, NULL);
	}
}
static void _MethodInfo_t643827776_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_MethodInfo_t643827776_CustomAttributesCacheGenerator_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		InterfaceTypeAttribute_t3336632297 * tmp = (InterfaceTypeAttribute_t3336632297 *)cache->attributes[0];
		InterfaceTypeAttribute__ctor_m342415882(tmp, 1LL, NULL);
	}
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[1];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
	{
		GuidAttribute_t3853718442 * tmp = (GuidAttribute_t3853718442 *)cache->attributes[2];
		GuidAttribute__ctor_m612200210(tmp, il2cpp_codegen_string_new_wrapper("FFCC1B5D-ECB8-38DD-9B01-3DC8ABC2AA5F"), NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[3];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		TypeLibImportClassAttribute_t2173872460 * tmp = (TypeLibImportClassAttribute_t2173872460 *)cache->attributes[4];
		TypeLibImportClassAttribute__ctor_m1502349925(tmp, il2cpp_codegen_type_get_object(MethodInfo_t_0_0_0_var), NULL);
	}
}
static void _Module_t494242506_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_Module_t494242506_CustomAttributesCacheGenerator_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		TypeLibImportClassAttribute_t2173872460 * tmp = (TypeLibImportClassAttribute_t2173872460 *)cache->attributes[0];
		TypeLibImportClassAttribute__ctor_m1502349925(tmp, il2cpp_codegen_type_get_object(Module_t2594760207_0_0_0_var), NULL);
	}
	{
		GuidAttribute_t3853718442 * tmp = (GuidAttribute_t3853718442 *)cache->attributes[1];
		GuidAttribute__ctor_m612200210(tmp, il2cpp_codegen_string_new_wrapper("D002E9BA-D9E3-3749-B1D3-D565A08B13E7"), NULL);
	}
	{
		InterfaceTypeAttribute_t3336632297 * tmp = (InterfaceTypeAttribute_t3336632297 *)cache->attributes[2];
		InterfaceTypeAttribute__ctor_m342415882(tmp, 1LL, NULL);
	}
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[3];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[4];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void _ModuleBuilder_t3149677118_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_ModuleBuilder_t3149677118_CustomAttributesCacheGenerator_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[1];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
	{
		GuidAttribute_t3853718442 * tmp = (GuidAttribute_t3853718442 *)cache->attributes[2];
		GuidAttribute__ctor_m612200210(tmp, il2cpp_codegen_string_new_wrapper("D05FFA9A-04AF-3519-8EE1-8D93AD73430B"), NULL);
	}
	{
		TypeLibImportClassAttribute_t2173872460 * tmp = (TypeLibImportClassAttribute_t2173872460 *)cache->attributes[3];
		TypeLibImportClassAttribute__ctor_m1502349925(tmp, il2cpp_codegen_type_get_object(ModuleBuilder_t671559400_0_0_0_var), NULL);
	}
	{
		InterfaceTypeAttribute_t3336632297 * tmp = (InterfaceTypeAttribute_t3336632297 *)cache->attributes[4];
		InterfaceTypeAttribute__ctor_m342415882(tmp, 1LL, NULL);
	}
}
static void _ParameterBuilder_t3730072709_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_ParameterBuilder_t3730072709_CustomAttributesCacheGenerator_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		TypeLibImportClassAttribute_t2173872460 * tmp = (TypeLibImportClassAttribute_t2173872460 *)cache->attributes[0];
		TypeLibImportClassAttribute__ctor_m1502349925(tmp, il2cpp_codegen_type_get_object(ParameterBuilder_t2478802094_0_0_0_var), NULL);
	}
	{
		GuidAttribute_t3853718442 * tmp = (GuidAttribute_t3853718442 *)cache->attributes[1];
		GuidAttribute__ctor_m612200210(tmp, il2cpp_codegen_string_new_wrapper("36329EBA-F97A-3565-BC07-0ED5C6EF19FC"), NULL);
	}
	{
		InterfaceTypeAttribute_t3336632297 * tmp = (InterfaceTypeAttribute_t3336632297 *)cache->attributes[2];
		InterfaceTypeAttribute__ctor_m342415882(tmp, 1LL, NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[3];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[4];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void _ParameterInfo_t3152035282_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_ParameterInfo_t3152035282_CustomAttributesCacheGenerator_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GuidAttribute_t3853718442 * tmp = (GuidAttribute_t3853718442 *)cache->attributes[0];
		GuidAttribute__ctor_m612200210(tmp, il2cpp_codegen_string_new_wrapper("993634C4-E47A-32CC-BE08-85F567DC27D6"), NULL);
	}
	{
		InterfaceTypeAttribute_t3336632297 * tmp = (InterfaceTypeAttribute_t3336632297 *)cache->attributes[1];
		InterfaceTypeAttribute__ctor_m342415882(tmp, 1LL, NULL);
	}
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[2];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[3];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		TypeLibImportClassAttribute_t2173872460 * tmp = (TypeLibImportClassAttribute_t2173872460 *)cache->attributes[4];
		TypeLibImportClassAttribute__ctor_m1502349925(tmp, il2cpp_codegen_type_get_object(ParameterInfo_t3893964560_0_0_0_var), NULL);
	}
}
static void _PropertyBuilder_t2876596643_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_PropertyBuilder_t2876596643_CustomAttributesCacheGenerator_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
	{
		GuidAttribute_t3853718442 * tmp = (GuidAttribute_t3853718442 *)cache->attributes[1];
		GuidAttribute__ctor_m612200210(tmp, il2cpp_codegen_string_new_wrapper("15F9A479-9397-3A63-ACBD-F51977FB0F02"), NULL);
	}
	{
		TypeLibImportClassAttribute_t2173872460 * tmp = (TypeLibImportClassAttribute_t2173872460 *)cache->attributes[2];
		TypeLibImportClassAttribute__ctor_m1502349925(tmp, il2cpp_codegen_type_get_object(PropertyBuilder_t1258013571_0_0_0_var), NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[3];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		InterfaceTypeAttribute_t3336632297 * tmp = (InterfaceTypeAttribute_t3336632297 *)cache->attributes[4];
		InterfaceTypeAttribute__ctor_m342415882(tmp, 1LL, NULL);
	}
}
static void _PropertyInfo_t2517621214_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_PropertyInfo_t2517621214_CustomAttributesCacheGenerator_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		InterfaceTypeAttribute_t3336632297 * tmp = (InterfaceTypeAttribute_t3336632297 *)cache->attributes[0];
		InterfaceTypeAttribute__ctor_m342415882(tmp, 1LL, NULL);
	}
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[1];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
	{
		GuidAttribute_t3853718442 * tmp = (GuidAttribute_t3853718442 *)cache->attributes[2];
		GuidAttribute__ctor_m612200210(tmp, il2cpp_codegen_string_new_wrapper("F59ED4E4-E68F-3218-BD77-061AA82824BF"), NULL);
	}
	{
		TypeLibImportClassAttribute_t2173872460 * tmp = (TypeLibImportClassAttribute_t2173872460 *)cache->attributes[3];
		TypeLibImportClassAttribute__ctor_m1502349925(tmp, il2cpp_codegen_type_get_object(PropertyInfo_t_0_0_0_var), NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[4];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void _Thread_t3433524544_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_Thread_t3433524544_CustomAttributesCacheGenerator_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		InterfaceTypeAttribute_t3336632297 * tmp = (InterfaceTypeAttribute_t3336632297 *)cache->attributes[0];
		InterfaceTypeAttribute__ctor_m342415882(tmp, 1LL, NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[1];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[2];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
	{
		TypeLibImportClassAttribute_t2173872460 * tmp = (TypeLibImportClassAttribute_t2173872460 *)cache->attributes[3];
		TypeLibImportClassAttribute__ctor_m1502349925(tmp, il2cpp_codegen_type_get_object(Thread_t2418682337_0_0_0_var), NULL);
	}
	{
		GuidAttribute_t3853718442 * tmp = (GuidAttribute_t3853718442 *)cache->attributes[4];
		GuidAttribute__ctor_m612200210(tmp, il2cpp_codegen_string_new_wrapper("C281C7F1-4AA9-3517-961A-463CFED57E75"), NULL);
	}
}
static void _TypeBuilder_t4136446867_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_TypeBuilder_t4136446867_CustomAttributesCacheGenerator_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
	{
		InterfaceTypeAttribute_t3336632297 * tmp = (InterfaceTypeAttribute_t3336632297 *)cache->attributes[1];
		InterfaceTypeAttribute__ctor_m342415882(tmp, 1LL, NULL);
	}
	{
		GuidAttribute_t3853718442 * tmp = (GuidAttribute_t3853718442 *)cache->attributes[2];
		GuidAttribute__ctor_m612200210(tmp, il2cpp_codegen_string_new_wrapper("7E5678EE-48B3-3F83-B076-C58543498A58"), NULL);
	}
	{
		TypeLibImportClassAttribute_t2173872460 * tmp = (TypeLibImportClassAttribute_t2173872460 *)cache->attributes[3];
		TypeLibImportClassAttribute__ctor_m1502349925(tmp, il2cpp_codegen_type_get_object(TypeBuilder_t1663068587_0_0_0_var), NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[4];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void IActivator_t3392549374_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void IConstructionCallMessage_t3774005762_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void UrlAttribute_t3550867946_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void UrlAttribute_t3550867946_CustomAttributesCacheGenerator_UrlAttribute_GetPropertiesForNewContext_m3068588212(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void UrlAttribute_t3550867946_CustomAttributesCacheGenerator_UrlAttribute_IsContextOK_m4070904995(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void ChannelServices_t2227652707_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void ChannelServices_t2227652707_CustomAttributesCacheGenerator_ChannelServices_RegisterChannel_m1635994151(CustomAttributesCache* cache)
{
	{
		ObsoleteAttribute_t3443961000 * tmp = (ObsoleteAttribute_t3443961000 *)cache->attributes[0];
		ObsoleteAttribute__ctor_m3274828507(tmp, il2cpp_codegen_string_new_wrapper("Use RegisterChannel(IChannel,Boolean)"), NULL);
	}
}
static void CrossAppDomainSink_t3016970110_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		MonoTODOAttribute_t614642062 * tmp = (MonoTODOAttribute_t614642062 *)cache->attributes[0];
		MonoTODOAttribute__ctor_m2689597370(tmp, il2cpp_codegen_string_new_wrapper("Handle domain unloading?"), NULL);
	}
}
static void IChannel_t120806715_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void IChannelDataStore_t3140645294_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DefaultMemberAttribute_t2956431904 * tmp = (DefaultMemberAttribute_t2956431904 *)cache->attributes[0];
		DefaultMemberAttribute__ctor_m3129895323(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[1];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void IChannelReceiver_t1072542950_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void IChannelSender_t883237208_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void IClientChannelSinkProvider_t2704567566_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void IServerChannelSinkProvider_t1291603674_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void SinkProviderData_t758756700_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void Context_t2108226161_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void ContextAttribute_t4260694153_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_t2370971113 * tmp = (AttributeUsageAttribute_t2370971113 *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m1170715599(tmp, 4LL, NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[1];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void IContextAttribute_t632650489_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void IContextProperty_t2748911791_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void IContributeClientContextSink_t1070229284_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void IContributeDynamicSink_t1830500540_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void IContributeEnvoySink_t656644779_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void IContributeObjectSink_t133918256_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void IContributeServerContextSink_t3033167428_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void IDynamicMessageSink_t3469533509_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void IDynamicProperty_t2412334468_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void SynchronizationAttribute_t1579862423_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		AttributeUsageAttribute_t2370971113 * tmp = (AttributeUsageAttribute_t2370971113 *)cache->attributes[1];
		AttributeUsageAttribute__ctor_m1170715599(tmp, 4LL, NULL);
	}
}
static void SynchronizationAttribute_t1579862423_CustomAttributesCacheGenerator_SynchronizationAttribute_GetPropertiesForNewContext_m1138224242(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void SynchronizationAttribute_t1579862423_CustomAttributesCacheGenerator_SynchronizationAttribute_IsContextOK_m3271333896(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void LifetimeServices_t2072599424_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void AsyncResult_t1601560416_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void ConstructionCall_t3460915224_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[1];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void ConstructionCall_t3460915224_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map20(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
}
static void ConstructionCallDictionary_t3674406133_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map23(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
}
static void ConstructionCallDictionary_t3674406133_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map24(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
}
static void Header_t2445250629_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void IMessage_t3609193861_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void IMessageCtrl_t3563004285_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void IMessageSink_t2987155855_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void IMethodCallMessage_t1057394468_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void IMethodMessage_t658825243_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void IMethodReturnMessage_t3578412901_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void IRemotingFormatter_t3301862813_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void LogicalCallContext_t2541050671_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void MethodCall_t1058940400_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[1];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void MethodCall_t1058940400_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map1F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
}
static void MethodDictionary_t4290091627_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DefaultMemberAttribute_t2956431904 * tmp = (DefaultMemberAttribute_t2956431904 *)cache->attributes[0];
		DefaultMemberAttribute__ctor_m3129895323(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
	}
}
static void MethodDictionary_t4290091627_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map21(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
}
static void MethodDictionary_t4290091627_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map22(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
}
static void RemotingSurrogateSelector_t1285344342_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void ReturnMessage_t3469761868_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void SoapAttribute_t2706012165_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void SoapFieldAttribute_t2115119642_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		AttributeUsageAttribute_t2370971113 * tmp = (AttributeUsageAttribute_t2370971113 *)cache->attributes[1];
		AttributeUsageAttribute__ctor_m1170715599(tmp, 256LL, NULL);
	}
}
static void SoapMethodAttribute_t2520568184_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		AttributeUsageAttribute_t2370971113 * tmp = (AttributeUsageAttribute_t2370971113 *)cache->attributes[1];
		AttributeUsageAttribute__ctor_m1170715599(tmp, 64LL, NULL);
	}
}
static void SoapParameterAttribute_t9664292_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		AttributeUsageAttribute_t2370971113 * tmp = (AttributeUsageAttribute_t2370971113 *)cache->attributes[1];
		AttributeUsageAttribute__ctor_m1170715599(tmp, 2048LL, NULL);
	}
}
static void SoapTypeAttribute_t3635447107_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		AttributeUsageAttribute_t2370971113 * tmp = (AttributeUsageAttribute_t2370971113 *)cache->attributes[1];
		AttributeUsageAttribute__ctor_m1170715599(tmp, 1052LL, NULL);
	}
}
static void ProxyAttribute_t2439831892_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		AttributeUsageAttribute_t2370971113 * tmp = (AttributeUsageAttribute_t2370971113 *)cache->attributes[1];
		AttributeUsageAttribute__ctor_m1170715599(tmp, 4LL, NULL);
	}
}
static void ProxyAttribute_t2439831892_CustomAttributesCacheGenerator_ProxyAttribute_GetPropertiesForNewContext_m3937741509(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void ProxyAttribute_t2439831892_CustomAttributesCacheGenerator_ProxyAttribute_IsContextOK_m4171219307(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void RealProxy_t3984737357_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void ITrackingHandler_t2297406515_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void TrackingServices_t535905686_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void ActivatedClientTypeEntry_t1901086295_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void ActivatedServiceTypeEntry_t1623752528_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void IChannelInfo_t1594721423_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void IEnvoyInfo_t1188604488_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void IRemotingTypeInfo_t1645923797_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void InternalRemotingServices_t2693007168_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void ObjRef_t4239724039_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void ObjRef_t4239724039_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map26(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
}
static void ObjRef_t4239724039_CustomAttributesCacheGenerator_ObjRef_get_ChannelInfo_m3336748132(CustomAttributesCache* cache)
{
	{
		ReliabilityContractAttribute_t3009184403 * tmp = (ReliabilityContractAttribute_t3009184403 *)cache->attributes[0];
		ReliabilityContractAttribute__ctor_m2586077688(tmp, 3LL, 2LL, NULL);
	}
}
static void RemotingConfiguration_t3648899533_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void ConfigHandler_t4180847873_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map27(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
}
static void ConfigHandler_t4180847873_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map28(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
}
static void ConfigHandler_t4180847873_CustomAttributesCacheGenerator_ConfigHandler_ValidatePath_m3545750813____paths1(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t3330428127 * tmp = (ParamArrayAttribute_t3330428127 *)cache->attributes[0];
		ParamArrayAttribute__ctor_m1696407647(tmp, NULL);
	}
}
static void RemotingException_t747282334_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void RemotingServices_t303445091_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void RemotingServices_t303445091_CustomAttributesCacheGenerator_RemotingServices_IsTransparentProxy_m744747325(CustomAttributesCache* cache)
{
	{
		ReliabilityContractAttribute_t3009184403 * tmp = (ReliabilityContractAttribute_t3009184403 *)cache->attributes[0];
		ReliabilityContractAttribute__ctor_m2586077688(tmp, 3LL, 2LL, NULL);
	}
}
static void RemotingServices_t303445091_CustomAttributesCacheGenerator_RemotingServices_GetRealProxy_m256026011(CustomAttributesCache* cache)
{
	{
		ReliabilityContractAttribute_t3009184403 * tmp = (ReliabilityContractAttribute_t3009184403 *)cache->attributes[0];
		ReliabilityContractAttribute__ctor_m2586077688(tmp, 3LL, 2LL, NULL);
	}
}
static void SoapServices_t362502010_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void TypeEntry_t4003469904_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void WellKnownClientTypeEntry_t3040846894_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void WellKnownObjectMode_t2386524380_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void WellKnownServiceTypeEntry_t3884894085_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void BinaryFormatter_t2021242092_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void BinaryFormatter_t2021242092_CustomAttributesCacheGenerator_U3CDefaultSurrogateSelectorU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
}
static void BinaryFormatter_t2021242092_CustomAttributesCacheGenerator_BinaryFormatter_get_DefaultSurrogateSelector_m1118519797(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
}
static void FormatterAssemblyStyle_t624754737_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void FormatterTypeStyle_t358799495_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void TypeFilterLevel_t3266183943_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void FormatterConverter_t3333246427_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void FormatterServices_t2039552394_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void IDeserializationCallback_t1559034278_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void IFormatter_t2842043891_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void IFormatterConverter_t1975945934_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[1];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void IObjectReference_t2008307571_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void ISerializationSurrogate_t1041104378_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void ISurrogateSelector_t1684653753_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void ObjectManager_t1722973918_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void OnDeserializedAttribute_t2300437449_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_t2370971113 * tmp = (AttributeUsageAttribute_t2370971113 *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m1170715599(tmp, 64LL, NULL);
		AttributeUsageAttribute_set_Inherited_m2205070461(tmp, false, NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[1];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void OnDeserializingAttribute_t2405775403_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_t2370971113 * tmp = (AttributeUsageAttribute_t2370971113 *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m1170715599(tmp, 64LL, NULL);
		AttributeUsageAttribute_set_Inherited_m2205070461(tmp, false, NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[1];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void OnSerializedAttribute_t3606325723_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_t2370971113 * tmp = (AttributeUsageAttribute_t2370971113 *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m1170715599(tmp, 64LL, NULL);
		AttributeUsageAttribute_set_Inherited_m2205070461(tmp, false, NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[1];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void OnSerializingAttribute_t2302066511_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		AttributeUsageAttribute_t2370971113 * tmp = (AttributeUsageAttribute_t2370971113 *)cache->attributes[1];
		AttributeUsageAttribute__ctor_m1170715599(tmp, 64LL, NULL);
		AttributeUsageAttribute_set_Inherited_m2205070461(tmp, false, NULL);
	}
}
static void SerializationBinder_t1981625145_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void SerializationEntry_t4251740542_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void SerializationException_t3740332507_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void SerializationInfo_t3667883434_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void SerializationInfo_t3667883434_CustomAttributesCacheGenerator_SerializationInfo__ctor_m283007395(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void SerializationInfoEnumerator_t2005386294_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void StreamingContext_t1316667400_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void StreamingContextStates_t4185925513_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		FlagsAttribute_t388727259 * tmp = (FlagsAttribute_t388727259 *)cache->attributes[0];
		FlagsAttribute__ctor_m3834565173(tmp, NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[1];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void X509Certificate_t3231070314_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		MonoTODOAttribute_t614642062 * tmp = (MonoTODOAttribute_t614642062 *)cache->attributes[0];
		MonoTODOAttribute__ctor_m2689597370(tmp, il2cpp_codegen_string_new_wrapper("X509ContentType.SerializedCert isn't supported (anywhere in the class)"), NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[1];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void X509Certificate_t3231070314_CustomAttributesCacheGenerator_X509Certificate_GetIssuerName_m1334954263(CustomAttributesCache* cache)
{
	{
		ObsoleteAttribute_t3443961000 * tmp = (ObsoleteAttribute_t3443961000 *)cache->attributes[0];
		ObsoleteAttribute__ctor_m3274828507(tmp, il2cpp_codegen_string_new_wrapper("Use the Issuer property."), NULL);
	}
}
static void X509Certificate_t3231070314_CustomAttributesCacheGenerator_X509Certificate_GetName_m865911396(CustomAttributesCache* cache)
{
	{
		ObsoleteAttribute_t3443961000 * tmp = (ObsoleteAttribute_t3443961000 *)cache->attributes[0];
		ObsoleteAttribute__ctor_m3274828507(tmp, il2cpp_codegen_string_new_wrapper("Use the Subject property."), NULL);
	}
}
static void X509Certificate_t3231070314_CustomAttributesCacheGenerator_X509Certificate_Equals_m2980846026(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, false, NULL);
	}
}
static void X509Certificate_t3231070314_CustomAttributesCacheGenerator_X509Certificate_Import_m1670206153(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, false, NULL);
	}
	{
		MonoTODOAttribute_t614642062 * tmp = (MonoTODOAttribute_t614642062 *)cache->attributes[1];
		MonoTODOAttribute__ctor_m2689597370(tmp, il2cpp_codegen_string_new_wrapper("missing KeyStorageFlags support"), NULL);
	}
}
static void X509Certificate_t3231070314_CustomAttributesCacheGenerator_X509Certificate_Reset_m2166992089(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, false, NULL);
	}
}
static void X509KeyStorageFlags_t402552296_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		FlagsAttribute_t388727259 * tmp = (FlagsAttribute_t388727259 *)cache->attributes[0];
		FlagsAttribute__ctor_m3834565173(tmp, NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[1];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void AsymmetricAlgorithm_t1520670838_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void AsymmetricKeyExchangeFormatter_t3886653799_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void AsymmetricSignatureDeformatter_t456279265_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void AsymmetricSignatureFormatter_t9512622_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void CipherMode_t1257099160_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void CryptoConfig_t2397213964_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void CryptoConfig_t2397213964_CustomAttributesCacheGenerator_CryptoConfig_CreateFromName_m1179263245____args1(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t3330428127 * tmp = (ParamArrayAttribute_t3330428127 *)cache->attributes[0];
		ParamArrayAttribute__ctor_m1696407647(tmp, NULL);
	}
}
static void CryptographicException_t3454124556_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void CryptographicUnexpectedOperationException_t3109796652_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void CspParameters_t397939019_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void CspProviderFlags_t2530719326_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		FlagsAttribute_t388727259 * tmp = (FlagsAttribute_t388727259 *)cache->attributes[0];
		FlagsAttribute__ctor_m3834565173(tmp, NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[1];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void DES_t4084254726_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void DESCryptoServiceProvider_t2527584960_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void DSA_t3534207242_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void DSACryptoServiceProvider_t1989336279_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void DSACryptoServiceProvider_t1989336279_CustomAttributesCacheGenerator_DSACryptoServiceProvider_t1989336279____PublicOnly_PropertyInfo(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, false, NULL);
	}
}
static void DSAParameters_t2781005375_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void DSASignatureDeformatter_t3417499579_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void DSASignatureFormatter_t514505739_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void HMAC_t1427971378_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void HMACMD5_t2888238498_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void HMACRIPEMD160_t1354245691_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void HMACSHA1_t882816891_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void HMACSHA256_t351978820_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void HMACSHA384_t4100829916_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void HMACSHA512_t1403483577_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void HashAlgorithm_t3481041717_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void ICryptoTransform_t725015005_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void ICspAsymmetricAlgorithm_t1974207366_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void KeyedHashAlgorithm_t4279500660_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void MACTripleDES_t3052039254_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void MD5_t184894568_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void MD5CryptoServiceProvider_t2301558652_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void PaddingMode_t218725588_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void RC2_t963760615_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void RC2CryptoServiceProvider_t1519414973_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void RIPEMD160_t482559745_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void RIPEMD160Managed_t662892897_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void RSA_t2561376050_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void RSACryptoServiceProvider_t393426716_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void RSACryptoServiceProvider_t393426716_CustomAttributesCacheGenerator_RSACryptoServiceProvider_t393426716____PublicOnly_PropertyInfo(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, false, NULL);
	}
}
static void RSAPKCS1KeyExchangeFormatter_t1995405421_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void RSAPKCS1SignatureDeformatter_t1271325812_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void RSAPKCS1SignatureFormatter_t3136547357_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void RSAParameters_t2749614896_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void Rijndael_t2194267518_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void RijndaelManaged_t3688703751_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void RijndaelManagedTransform_t3825137234_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void SHA1_t3760328239_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void SHA1CryptoServiceProvider_t3974373694_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void SHA1Managed_t2877123146_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void SHA256_t94154448_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void SHA256Managed_t316933500_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void SHA384_t1458639100_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void SHA384Managed_t3276463015_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void SHA512_t2457760952_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void SHA512Managed_t3922712332_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void SignatureDescription_t4263538145_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void SymmetricAlgorithm_t1428778343_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void ToBase64Transform_t1095090031_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void TripleDES_t1638718720_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void TripleDESCryptoServiceProvider_t3718652277_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void CodeAccessSecurityAttribute_t1836922066_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ObsoleteAttribute_t3443961000 * tmp = (ObsoleteAttribute_t3443961000 *)cache->attributes[0];
		ObsoleteAttribute__ctor_m3274828507(tmp, il2cpp_codegen_string_new_wrapper("CAS support is not available with Silverlight applications."), NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[1];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		AttributeUsageAttribute_t2370971113 * tmp = (AttributeUsageAttribute_t2370971113 *)cache->attributes[2];
		AttributeUsageAttribute__ctor_m1170715599(tmp, 109LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m1313295265(tmp, true, NULL);
		AttributeUsageAttribute_set_Inherited_m2205070461(tmp, false, NULL);
	}
}
static void IUnrestrictedPermission_t4141731025_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void SecurityAction_t4061768731_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		ObsoleteAttribute_t3443961000 * tmp = (ObsoleteAttribute_t3443961000 *)cache->attributes[1];
		ObsoleteAttribute__ctor_m3274828507(tmp, il2cpp_codegen_string_new_wrapper("CAS support is not available with Silverlight applications."), NULL);
	}
}
static void SecurityPermission_t2467225011_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void SecurityPermissionAttribute_t1827074234_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ObsoleteAttribute_t3443961000 * tmp = (ObsoleteAttribute_t3443961000 *)cache->attributes[0];
		ObsoleteAttribute__ctor_m3274828507(tmp, il2cpp_codegen_string_new_wrapper("CAS support is not available with Silverlight applications."), NULL);
	}
	{
		AttributeUsageAttribute_t2370971113 * tmp = (AttributeUsageAttribute_t2370971113 *)cache->attributes[1];
		AttributeUsageAttribute__ctor_m1170715599(tmp, 109LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m1313295265(tmp, true, NULL);
		AttributeUsageAttribute_set_Inherited_m2205070461(tmp, false, NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[2];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void SecurityPermissionFlag_t4236913982_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		ObsoleteAttribute_t3443961000 * tmp = (ObsoleteAttribute_t3443961000 *)cache->attributes[1];
		ObsoleteAttribute__ctor_m3274828507(tmp, il2cpp_codegen_string_new_wrapper("CAS support is not available with Silverlight applications."), NULL);
	}
	{
		FlagsAttribute_t388727259 * tmp = (FlagsAttribute_t388727259 *)cache->attributes[2];
		FlagsAttribute__ctor_m3834565173(tmp, NULL);
	}
}
static void StrongNamePublicKeyBlob_t759939843_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void ApplicationTrust_t3070296692_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void Evidence_t2634340723_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		MonoTODOAttribute_t614642062 * tmp = (MonoTODOAttribute_t614642062 *)cache->attributes[1];
		MonoTODOAttribute__ctor_m2689597370(tmp, il2cpp_codegen_string_new_wrapper("Serialization format not compatible with .NET"), NULL);
	}
}
static void Evidence_t2634340723_CustomAttributesCacheGenerator_Evidence_Equals_m846405073(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, false, NULL);
	}
}
static void Evidence_t2634340723_CustomAttributesCacheGenerator_Evidence_GetHashCode_m1726229036(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, false, NULL);
	}
}
static void Hash_t1979402678_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void IIdentityPermissionFactory_t233160095_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void StrongName_t848839738_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void IIdentity_t4205433510_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void IPrincipal_t1738737119_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void PrincipalPolicy_t3910466358_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void WindowsAccountType_t3074501293_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void WindowsIdentity_t1567274306_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void WindowsIdentity_t1567274306_CustomAttributesCacheGenerator_WindowsIdentity_Dispose_m953714069(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, false, NULL);
	}
}
static void AllowPartiallyTrustedCallersAttribute_t2708656030_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		AttributeUsageAttribute_t2370971113 * tmp = (AttributeUsageAttribute_t2370971113 *)cache->attributes[1];
		AttributeUsageAttribute__ctor_m1170715599(tmp, 1LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m1313295265(tmp, false, NULL);
		AttributeUsageAttribute_set_Inherited_m2205070461(tmp, false, NULL);
	}
}
static void CodeAccessPermission_t1591093589_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		MonoTODOAttribute_t614642062 * tmp = (MonoTODOAttribute_t614642062 *)cache->attributes[1];
		MonoTODOAttribute__ctor_m2689597370(tmp, il2cpp_codegen_string_new_wrapper("CAS support is experimental (and unsupported)."), NULL);
	}
}
static void CodeAccessPermission_t1591093589_CustomAttributesCacheGenerator_CodeAccessPermission_Equals_m593433957(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, false, NULL);
	}
}
static void CodeAccessPermission_t1591093589_CustomAttributesCacheGenerator_CodeAccessPermission_GetHashCode_m2903672344(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, false, NULL);
	}
}
static void IPermission_t3649357080_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void ISecurityEncodable_t3776590316_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void IStackWalk_t303383304_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void PermissionSet_t1654582346_CustomAttributesCacheGenerator_U3CDeclarativeSecurityU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
}
static void PermissionSet_t1654582346_CustomAttributesCacheGenerator_PermissionSet_set_DeclarativeSecurity_m84390744(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
}
static void SecurityCriticalAttribute_t3270381382_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		MonoTODOAttribute_t614642062 * tmp = (MonoTODOAttribute_t614642062 *)cache->attributes[0];
		MonoTODOAttribute__ctor_m2689597370(tmp, il2cpp_codegen_string_new_wrapper("Only supported by the runtime when CoreCLR is enabled"), NULL);
	}
	{
		AttributeUsageAttribute_t2370971113 * tmp = (AttributeUsageAttribute_t2370971113 *)cache->attributes[1];
		AttributeUsageAttribute__ctor_m1170715599(tmp, 6143LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m1313295265(tmp, false, NULL);
		AttributeUsageAttribute_set_Inherited_m2205070461(tmp, false, NULL);
	}
}
static void SecurityElement_t2714688033_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void SecurityException_t791478456_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void SecurityException_t791478456_CustomAttributesCacheGenerator_SecurityException_t791478456____Demanded_PropertyInfo(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, false, NULL);
	}
}
static void SecurityManager_t3149513615_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void SecurityManager_t3149513615_CustomAttributesCacheGenerator_SecurityManager_t3149513615____SecurityEnabled_PropertyInfo(CustomAttributesCache* cache)
{
	{
		ObsoleteAttribute_t3443961000 * tmp = (ObsoleteAttribute_t3443961000 *)cache->attributes[0];
		ObsoleteAttribute__ctor_m3274828507(tmp, il2cpp_codegen_string_new_wrapper("The security manager cannot be turned off on MS runtime"), NULL);
	}
}
static void SecuritySafeCriticalAttribute_t824959508_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_t2370971113 * tmp = (AttributeUsageAttribute_t2370971113 *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m1170715599(tmp, 32767LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m1313295265(tmp, false, NULL);
		AttributeUsageAttribute_set_Inherited_m2205070461(tmp, false, NULL);
	}
	{
		MonoTODOAttribute_t614642062 * tmp = (MonoTODOAttribute_t614642062 *)cache->attributes[1];
		MonoTODOAttribute__ctor_m2689597370(tmp, il2cpp_codegen_string_new_wrapper("Only supported by the runtime when CoreCLR is enabled"), NULL);
	}
}
static void SuppressUnmanagedCodeSecurityAttribute_t1135819281_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		AttributeUsageAttribute_t2370971113 * tmp = (AttributeUsageAttribute_t2370971113 *)cache->attributes[1];
		AttributeUsageAttribute__ctor_m1170715599(tmp, 5188LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m1313295265(tmp, true, NULL);
		AttributeUsageAttribute_set_Inherited_m2205070461(tmp, false, NULL);
	}
}
static void UnverifiableCodeAttribute_t404930905_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_t2370971113 * tmp = (AttributeUsageAttribute_t2370971113 *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m1170715599(tmp, 2LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m1313295265(tmp, true, NULL);
		AttributeUsageAttribute_set_Inherited_m2205070461(tmp, false, NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[1];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void ASCIIEncoding_t3246903780_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		MonoTODOAttribute_t614642062 * tmp = (MonoTODOAttribute_t614642062 *)cache->attributes[1];
		MonoTODOAttribute__ctor_m2689597370(tmp, il2cpp_codegen_string_new_wrapper("Serialization format not compatible with .NET"), NULL);
	}
}
static void ASCIIEncoding_t3246903780_CustomAttributesCacheGenerator_ASCIIEncoding_GetBytes_m687126587(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, false, NULL);
	}
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[1];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void ASCIIEncoding_t3246903780_CustomAttributesCacheGenerator_ASCIIEncoding_GetByteCount_m1478581917(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, false, NULL);
	}
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[1];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void ASCIIEncoding_t3246903780_CustomAttributesCacheGenerator_ASCIIEncoding_GetDecoder_m763127219(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, false, NULL);
	}
	{
		MonoTODOAttribute_t614642062 * tmp = (MonoTODOAttribute_t614642062 *)cache->attributes[1];
		MonoTODOAttribute__ctor_m2689597370(tmp, il2cpp_codegen_string_new_wrapper("we have simple override to match method signature."), NULL);
	}
}
static void Decoder_t1525265844_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void Decoder_t1525265844_CustomAttributesCacheGenerator_Decoder_t1525265844____Fallback_PropertyInfo(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, false, NULL);
	}
}
static void Decoder_t1525265844_CustomAttributesCacheGenerator_Decoder_t1525265844____FallbackBuffer_PropertyInfo(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, false, NULL);
	}
}
static void DecoderReplacementFallback_t4051900696_CustomAttributesCacheGenerator_DecoderReplacementFallback__ctor_m3316979574(CustomAttributesCache* cache)
{
	{
		MonoTODOAttribute_t614642062 * tmp = (MonoTODOAttribute_t614642062 *)cache->attributes[0];
		MonoTODOAttribute__ctor_m1347054473(tmp, NULL);
	}
}
static void EncoderReplacementFallback_t1692097091_CustomAttributesCacheGenerator_EncoderReplacementFallback__ctor_m1548856421(CustomAttributesCache* cache)
{
	{
		MonoTODOAttribute_t614642062 * tmp = (MonoTODOAttribute_t614642062 *)cache->attributes[0];
		MonoTODOAttribute__ctor_m1347054473(tmp, NULL);
	}
}
static void Encoding_t1422240108_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void Encoding_t1422240108_CustomAttributesCacheGenerator_Encoding_InvokeI18N_m3158856541____args1(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t3330428127 * tmp = (ParamArrayAttribute_t3330428127 *)cache->attributes[0];
		ParamArrayAttribute__ctor_m1696407647(tmp, NULL);
	}
}
static void Encoding_t1422240108_CustomAttributesCacheGenerator_Encoding_Clone_m1538624630(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, false, NULL);
	}
}
static void Encoding_t1422240108_CustomAttributesCacheGenerator_Encoding_GetByteCount_m2900855870(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, false, NULL);
	}
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[1];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void Encoding_t1422240108_CustomAttributesCacheGenerator_Encoding_GetBytes_m3914031873(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[1];
		ComVisibleAttribute__ctor_m29339631(tmp, false, NULL);
	}
}
static void Encoding_t1422240108_CustomAttributesCacheGenerator_Encoding_t1422240108____IsReadOnly_PropertyInfo(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, false, NULL);
	}
}
static void Encoding_t1422240108_CustomAttributesCacheGenerator_Encoding_t1422240108____DecoderFallback_PropertyInfo(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, false, NULL);
	}
}
static void Encoding_t1422240108_CustomAttributesCacheGenerator_Encoding_t1422240108____EncoderFallback_PropertyInfo(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, false, NULL);
	}
}
static void StringBuilder_t2076519010_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		MonoTODOAttribute_t614642062 * tmp = (MonoTODOAttribute_t614642062 *)cache->attributes[0];
		MonoTODOAttribute__ctor_m2689597370(tmp, il2cpp_codegen_string_new_wrapper("Serialization format not compatible with .NET"), NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[1];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		DefaultMemberAttribute_t2956431904 * tmp = (DefaultMemberAttribute_t2956431904 *)cache->attributes[2];
		DefaultMemberAttribute__ctor_m3129895323(tmp, il2cpp_codegen_string_new_wrapper("Chars"), NULL);
	}
}
static void StringBuilder_t2076519010_CustomAttributesCacheGenerator_StringBuilder_AppendFormat_m2847832545____args1(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t3330428127 * tmp = (ParamArrayAttribute_t3330428127 *)cache->attributes[0];
		ParamArrayAttribute__ctor_m1696407647(tmp, NULL);
	}
}
static void StringBuilder_t2076519010_CustomAttributesCacheGenerator_StringBuilder_AppendFormat_m2246691282____args2(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t3330428127 * tmp = (ParamArrayAttribute_t3330428127 *)cache->attributes[0];
		ParamArrayAttribute__ctor_m1696407647(tmp, NULL);
	}
}
static void UTF32Encoding_t108799848_CustomAttributesCacheGenerator_UTF32Encoding_GetByteCount_m1402347958(CustomAttributesCache* cache)
{
	{
		MonoTODOAttribute_t614642062 * tmp = (MonoTODOAttribute_t614642062 *)cache->attributes[0];
		MonoTODOAttribute__ctor_m2689597370(tmp, il2cpp_codegen_string_new_wrapper("handle fallback"), NULL);
	}
}
static void UTF32Encoding_t108799848_CustomAttributesCacheGenerator_UTF32Encoding_GetBytes_m4274563278(CustomAttributesCache* cache)
{
	{
		MonoTODOAttribute_t614642062 * tmp = (MonoTODOAttribute_t614642062 *)cache->attributes[0];
		MonoTODOAttribute__ctor_m2689597370(tmp, il2cpp_codegen_string_new_wrapper("handle fallback"), NULL);
	}
}
static void UTF32Encoding_t108799848_CustomAttributesCacheGenerator_UTF32Encoding_GetByteCount_m2178484365(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void UTF32Encoding_t108799848_CustomAttributesCacheGenerator_UTF32Encoding_GetBytes_m3528139859(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void UTF7Encoding_t2298099273_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		MonoTODOAttribute_t614642062 * tmp = (MonoTODOAttribute_t614642062 *)cache->attributes[1];
		MonoTODOAttribute__ctor_m2689597370(tmp, il2cpp_codegen_string_new_wrapper("Serialization format not compatible with .NET"), NULL);
	}
}
static void UTF7Encoding_t2298099273_CustomAttributesCacheGenerator_UTF7Encoding_GetHashCode_m3728423310(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, false, NULL);
	}
}
static void UTF7Encoding_t2298099273_CustomAttributesCacheGenerator_UTF7Encoding_Equals_m3440788742(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, false, NULL);
	}
}
static void UTF7Encoding_t2298099273_CustomAttributesCacheGenerator_UTF7Encoding_GetByteCount_m4153636640(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, false, NULL);
	}
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[1];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void UTF7Encoding_t2298099273_CustomAttributesCacheGenerator_UTF7Encoding_GetByteCount_m260339288(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, false, NULL);
	}
}
static void UTF7Encoding_t2298099273_CustomAttributesCacheGenerator_UTF7Encoding_GetBytes_m3276108052(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, false, NULL);
	}
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[1];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void UTF7Encoding_t2298099273_CustomAttributesCacheGenerator_UTF7Encoding_GetBytes_m565634380(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, false, NULL);
	}
}
static void UTF7Encoding_t2298099273_CustomAttributesCacheGenerator_UTF7Encoding_GetString_m753427559(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, false, NULL);
	}
}
static void UTF8Encoding_t2311876230_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		MonoTODOAttribute_t614642062 * tmp = (MonoTODOAttribute_t614642062 *)cache->attributes[0];
		MonoTODOAttribute__ctor_m2689597370(tmp, il2cpp_codegen_string_new_wrapper("EncoderFallback is not handled"), NULL);
	}
	{
		MonoTODOAttribute_t614642062 * tmp = (MonoTODOAttribute_t614642062 *)cache->attributes[1];
		MonoTODOAttribute__ctor_m2689597370(tmp, il2cpp_codegen_string_new_wrapper("Serialization format not compatible with .NET"), NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[2];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void UTF8Encoding_t2311876230_CustomAttributesCacheGenerator_UTF8Encoding_GetByteCount_m2144817935(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[1];
		ComVisibleAttribute__ctor_m29339631(tmp, false, NULL);
	}
}
static void UTF8Encoding_t2311876230_CustomAttributesCacheGenerator_UTF8Encoding_GetBytes_m1154207544(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, false, NULL);
	}
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[1];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void UTF8Encoding_t2311876230_CustomAttributesCacheGenerator_UTF8Encoding_GetString_m774016146(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, false, NULL);
	}
}
static void UnicodeEncoding_t4077319695_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		MonoTODOAttribute_t614642062 * tmp = (MonoTODOAttribute_t614642062 *)cache->attributes[0];
		MonoTODOAttribute__ctor_m2689597370(tmp, il2cpp_codegen_string_new_wrapper("Serialization format not compatible with .NET"), NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[1];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void UnicodeEncoding_t4077319695_CustomAttributesCacheGenerator_UnicodeEncoding_GetByteCount_m3150074527(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, false, NULL);
	}
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[1];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void UnicodeEncoding_t4077319695_CustomAttributesCacheGenerator_UnicodeEncoding_GetBytes_m3268668497(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, false, NULL);
	}
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[1];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void UnicodeEncoding_t4077319695_CustomAttributesCacheGenerator_UnicodeEncoding_GetString_m1015926560(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, false, NULL);
	}
}
static void CompressedStack_t2985122284_CustomAttributesCacheGenerator_CompressedStack_CreateCopy_m233976566(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, false, NULL);
	}
}
static void EventResetMode_t613571177_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, false, NULL);
	}
}
static void EventWaitHandle_t651679404_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void ExecutionContext_t2619615533_CustomAttributesCacheGenerator_ExecutionContext__ctor_m184114300(CustomAttributesCache* cache)
{
	{
		MonoTODOAttribute_t614642062 * tmp = (MonoTODOAttribute_t614642062 *)cache->attributes[0];
		MonoTODOAttribute__ctor_m1347054473(tmp, NULL);
	}
}
static void Interlocked_t2817274411_CustomAttributesCacheGenerator_Interlocked_CompareExchange_m2168439704(CustomAttributesCache* cache)
{
	{
		ReliabilityContractAttribute_t3009184403 * tmp = (ReliabilityContractAttribute_t3009184403 *)cache->attributes[0];
		ReliabilityContractAttribute__ctor_m2586077688(tmp, 3LL, 2LL, NULL);
	}
}
static void Interlocked_t2817274411_CustomAttributesCacheGenerator_Interlocked_CompareExchange_m4154459173(CustomAttributesCache* cache)
{
	{
		ReliabilityContractAttribute_t3009184403 * tmp = (ReliabilityContractAttribute_t3009184403 *)cache->attributes[0];
		ReliabilityContractAttribute__ctor_m2586077688(tmp, 3LL, 2LL, NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[1];
		ComVisibleAttribute__ctor_m29339631(tmp, false, NULL);
	}
}
static void ManualResetEvent_t3770519771_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void Monitor_t2324682247_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void Monitor_t2324682247_CustomAttributesCacheGenerator_Monitor_Exit_m1372154428(CustomAttributesCache* cache)
{
	{
		ReliabilityContractAttribute_t3009184403 * tmp = (ReliabilityContractAttribute_t3009184403 *)cache->attributes[0];
		ReliabilityContractAttribute__ctor_m2586077688(tmp, 3LL, 2LL, NULL);
	}
}
static void Mutex_t312945462_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void Mutex_t312945462_CustomAttributesCacheGenerator_Mutex__ctor_m2870296590(CustomAttributesCache* cache)
{
	{
		ReliabilityContractAttribute_t3009184403 * tmp = (ReliabilityContractAttribute_t3009184403 *)cache->attributes[0];
		ReliabilityContractAttribute__ctor_m2586077688(tmp, 3LL, 1LL, NULL);
	}
}
static void Mutex_t312945462_CustomAttributesCacheGenerator_Mutex_ReleaseMutex_m1972465564(CustomAttributesCache* cache)
{
	{
		ReliabilityContractAttribute_t3009184403 * tmp = (ReliabilityContractAttribute_t3009184403 *)cache->attributes[0];
		ReliabilityContractAttribute__ctor_m2586077688(tmp, 3LL, 1LL, NULL);
	}
}
static void SynchronizationContext_t3863215862_CustomAttributesCacheGenerator_currentContext(CustomAttributesCache* cache)
{
	{
		ThreadStaticAttribute_t174455095 * tmp = (ThreadStaticAttribute_t174455095 *)cache->attributes[0];
		ThreadStaticAttribute__ctor_m778228724(tmp, NULL);
	}
}
static void SynchronizationLockException_t461259377_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void Thread_t2418682337_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Thread_t2418682337_CustomAttributesCacheGenerator_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ComDefaultInterfaceAttribute_t2399868801 * tmp = (ComDefaultInterfaceAttribute_t2399868801 *)cache->attributes[0];
		ComDefaultInterfaceAttribute__ctor_m1937741735(tmp, il2cpp_codegen_type_get_object(_Thread_t3433524544_0_0_0_var), NULL);
	}
	{
		ClassInterfaceAttribute_t1638101999 * tmp = (ClassInterfaceAttribute_t1638101999 *)cache->attributes[1];
		ClassInterfaceAttribute__ctor_m2759037627(tmp, 0LL, NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[2];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void Thread_t2418682337_CustomAttributesCacheGenerator_local_slots(CustomAttributesCache* cache)
{
	{
		ThreadStaticAttribute_t174455095 * tmp = (ThreadStaticAttribute_t174455095 *)cache->attributes[0];
		ThreadStaticAttribute__ctor_m778228724(tmp, NULL);
	}
}
static void Thread_t2418682337_CustomAttributesCacheGenerator__ec(CustomAttributesCache* cache)
{
	{
		ThreadStaticAttribute_t174455095 * tmp = (ThreadStaticAttribute_t174455095 *)cache->attributes[0];
		ThreadStaticAttribute__ctor_m778228724(tmp, NULL);
	}
}
static void Thread_t2418682337_CustomAttributesCacheGenerator_Thread_get_CurrentThread_m3312024903(CustomAttributesCache* cache)
{
	{
		ReliabilityContractAttribute_t3009184403 * tmp = (ReliabilityContractAttribute_t3009184403 *)cache->attributes[0];
		ReliabilityContractAttribute__ctor_m2586077688(tmp, 3LL, 1LL, NULL);
	}
}
static void Thread_t2418682337_CustomAttributesCacheGenerator_Thread_Finalize_m773434565(CustomAttributesCache* cache)
{
	{
		ReliabilityContractAttribute_t3009184403 * tmp = (ReliabilityContractAttribute_t3009184403 *)cache->attributes[0];
		ReliabilityContractAttribute__ctor_m2586077688(tmp, 3LL, 2LL, NULL);
	}
}
static void Thread_t2418682337_CustomAttributesCacheGenerator_Thread_get_ExecutionContext_m2718260421(CustomAttributesCache* cache)
{
	{
		ReliabilityContractAttribute_t3009184403 * tmp = (ReliabilityContractAttribute_t3009184403 *)cache->attributes[0];
		ReliabilityContractAttribute__ctor_m2586077688(tmp, 3LL, 1LL, NULL);
	}
}
static void Thread_t2418682337_CustomAttributesCacheGenerator_Thread_get_ManagedThreadId_m2540698636(CustomAttributesCache* cache)
{
	{
		ReliabilityContractAttribute_t3009184403 * tmp = (ReliabilityContractAttribute_t3009184403 *)cache->attributes[0];
		ReliabilityContractAttribute__ctor_m2586077688(tmp, 3LL, 2LL, NULL);
	}
}
static void Thread_t2418682337_CustomAttributesCacheGenerator_Thread_GetHashCode_m1763772112(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, false, NULL);
	}
}
static void Thread_t2418682337_CustomAttributesCacheGenerator_Thread_GetCompressedStack_m2176559759(CustomAttributesCache* cache)
{
	{
		ObsoleteAttribute_t3443961000 * tmp = (ObsoleteAttribute_t3443961000 *)cache->attributes[0];
		ObsoleteAttribute__ctor_m3274828507(tmp, il2cpp_codegen_string_new_wrapper("see CompressedStack class"), NULL);
	}
}
static void Thread_t2418682337_CustomAttributesCacheGenerator_Thread_t2418682337____ExecutionContext_PropertyInfo(CustomAttributesCache* cache)
{
	{
		MonoTODOAttribute_t614642062 * tmp = (MonoTODOAttribute_t614642062 *)cache->attributes[0];
		MonoTODOAttribute__ctor_m2689597370(tmp, il2cpp_codegen_string_new_wrapper("limited to CompressedStack support"), NULL);
	}
}
static void ThreadAbortException_t2477337875_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void ThreadInterruptedException_t1245490379_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void ThreadState_t231270902_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		FlagsAttribute_t388727259 * tmp = (FlagsAttribute_t388727259 *)cache->attributes[0];
		FlagsAttribute__ctor_m3834565173(tmp, NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[1];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void ThreadStateException_t671479770_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void Timer_t876405921_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void WaitHandle_t2964044060_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void WaitHandle_t2964044060_CustomAttributesCacheGenerator_WaitHandle_t2964044060____Handle_PropertyInfo(CustomAttributesCache* cache)
{
	{
		ObsoleteAttribute_t3443961000 * tmp = (ObsoleteAttribute_t3443961000 *)cache->attributes[0];
		ObsoleteAttribute__ctor_m3274828507(tmp, il2cpp_codegen_string_new_wrapper("In the profiles > 2.x, use SafeHandle instead of Handle"), NULL);
	}
}
static void AccessViolationException_t3196137127_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void ActivationContext_t4070894218_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, false, NULL);
	}
}
static void Activator_t214179882_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Activator_t214179882_CustomAttributesCacheGenerator_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		ClassInterfaceAttribute_t1638101999 * tmp = (ClassInterfaceAttribute_t1638101999 *)cache->attributes[1];
		ClassInterfaceAttribute__ctor_m2759037627(tmp, 0LL, NULL);
	}
	{
		ComDefaultInterfaceAttribute_t2399868801 * tmp = (ComDefaultInterfaceAttribute_t2399868801 *)cache->attributes[2];
		ComDefaultInterfaceAttribute__ctor_m1937741735(tmp, il2cpp_codegen_type_get_object(_Activator_t134942278_0_0_0_var), NULL);
	}
}
static void Activator_t214179882_CustomAttributesCacheGenerator_Activator_CreateInstance_m291998472____args1(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t3330428127 * tmp = (ParamArrayAttribute_t3330428127 *)cache->attributes[0];
		ParamArrayAttribute__ctor_m1696407647(tmp, NULL);
	}
}
static void AppDomain_t3770931252_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		ClassInterfaceAttribute_t1638101999 * tmp = (ClassInterfaceAttribute_t1638101999 *)cache->attributes[1];
		ClassInterfaceAttribute__ctor_m2759037627(tmp, 0LL, NULL);
	}
}
static void AppDomain_t3770931252_CustomAttributesCacheGenerator_type_resolve_in_progress(CustomAttributesCache* cache)
{
	{
		ThreadStaticAttribute_t174455095 * tmp = (ThreadStaticAttribute_t174455095 *)cache->attributes[0];
		ThreadStaticAttribute__ctor_m778228724(tmp, NULL);
	}
}
static void AppDomain_t3770931252_CustomAttributesCacheGenerator_assembly_resolve_in_progress(CustomAttributesCache* cache)
{
	{
		ThreadStaticAttribute_t174455095 * tmp = (ThreadStaticAttribute_t174455095 *)cache->attributes[0];
		ThreadStaticAttribute__ctor_m778228724(tmp, NULL);
	}
}
static void AppDomain_t3770931252_CustomAttributesCacheGenerator_assembly_resolve_in_progress_refonly(CustomAttributesCache* cache)
{
	{
		ThreadStaticAttribute_t174455095 * tmp = (ThreadStaticAttribute_t174455095 *)cache->attributes[0];
		ThreadStaticAttribute__ctor_m778228724(tmp, NULL);
	}
}
static void AppDomain_t3770931252_CustomAttributesCacheGenerator__principal(CustomAttributesCache* cache)
{
	{
		ThreadStaticAttribute_t174455095 * tmp = (ThreadStaticAttribute_t174455095 *)cache->attributes[0];
		ThreadStaticAttribute__ctor_m778228724(tmp, NULL);
	}
}
static void AppDomainManager_t58958733_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void AppDomainSetup_t2748923811_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		ClassInterfaceAttribute_t1638101999 * tmp = (ClassInterfaceAttribute_t1638101999 *)cache->attributes[1];
		ClassInterfaceAttribute__ctor_m2759037627(tmp, 0LL, NULL);
	}
}
static void ApplicationException_t899029688_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void ApplicationIdentity_t2437177209_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, false, NULL);
	}
}
static void ArgumentException_t1372035057_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void ArgumentNullException_t517973450_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void ArgumentOutOfRangeException_t1745463579_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void ArithmeticException_t3212484507_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void ArrayTypeMismatchException_t1927459263_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void AssemblyLoadEventArgs_t4063075709_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void AttributeTargets_t2512976426_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		FlagsAttribute_t388727259 * tmp = (FlagsAttribute_t388727259 *)cache->attributes[1];
		FlagsAttribute__ctor_m3834565173(tmp, NULL);
	}
}
static void Buffer_t127642776_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void CharEnumerator_t1625757873_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void ContextBoundObject_t2718001193_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToBoolean_m3849780071(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToBoolean_m1821365203(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToBoolean_m3541395848(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToBoolean_m3686882876(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToByte_m1998596307(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToByte_m627183151(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToByte_m1655664624(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToByte_m2693208695(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToChar_m1651289017(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToChar_m263958929(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToChar_m2065393292(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToChar_m1385558794(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToDateTime_m1240677581(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToDateTime_m2178216354(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToDateTime_m1691902540(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToDateTime_m1899241014(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToDecimal_m2105281924(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToDecimal_m4051859286(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToDecimal_m1174043729(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToDecimal_m1566524000(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToDouble_m3582443749(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToDouble_m3414375849(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToDouble_m1338516771(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToDouble_m1798060040(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToInt16_m1268881642(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToInt16_m4195031634(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToInt16_m3052677612(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToInt16_m4149411228(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToInt32_m1206626159(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToInt32_m2420320894(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToInt32_m575912531(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToInt32_m349566814(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToInt64_m1063954313(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToInt64_m1102749412(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToInt64_m781390160(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToInt64_m2202161021(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToSByte_m863025055(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToSByte_m1485386302(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToSByte_m1209019644(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToSByte_m1294608859(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToSByte_m966005355(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToSByte_m2780065447(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToSByte_m3186677260(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToSByte_m763994626(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToSByte_m3414187568(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToSByte_m1378762269(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToSByte_m1804217813(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToSByte_m3354070798(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToSByte_m2415307335(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToSByte_m1950141290(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToSingle_m1511457299(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToSingle_m3480326864(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToSingle_m2260413742(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToSingle_m3787160430(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToUInt16_m3506724663(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToUInt16_m115274073(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToUInt16_m3490324414(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToUInt16_m3912085078(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToUInt16_m207185530(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToUInt16_m722491632(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToUInt16_m1222063852(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToUInt16_m4146807859(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToUInt16_m1828753276(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToUInt16_m2948714162(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToUInt16_m2011096748(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToUInt16_m418187698(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToUInt16_m3930851433(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToUInt16_m996906177(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToUInt32_m4249819190(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToUInt32_m823261086(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToUInt32_m3279699603(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToUInt32_m837934666(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToUInt32_m3154368742(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToUInt32_m3499511866(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToUInt32_m3902982566(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToUInt32_m1630022459(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToUInt32_m2888961780(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToUInt32_m3875914063(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToUInt32_m2993243305(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToUInt32_m2675516930(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToUInt32_m3219512886(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToUInt32_m731182432(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToUInt64_m2793460560(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToUInt64_m3441470682(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToUInt64_m651291461(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToUInt64_m1016581210(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToUInt64_m4062218541(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToUInt64_m1587419655(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToUInt64_m1283785253(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToUInt64_m3177931015(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToUInt64_m3453577592(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToUInt64_m371719104(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToUInt64_m1691859135(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToUInt64_m3212089508(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToUInt64_m1360931664(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToUInt64_m2801452660(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToUInt64_m3389700845(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void DBNull_t2963649720_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void DateTimeKind_t3511769276_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void DateTimeOffset_t2220763929_CustomAttributesCacheGenerator_DateTimeOffset_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m2312432265(CustomAttributesCache* cache)
{
	{
		MonoTODOAttribute_t614642062 * tmp = (MonoTODOAttribute_t614642062 *)cache->attributes[0];
		MonoTODOAttribute__ctor_m1347054473(tmp, NULL);
	}
}
static void DayOfWeek_t61775933_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void DivideByZeroException_t1878407045_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void DllNotFoundException_t2724339085_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void EntryPointNotFoundException_t2358315754_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void MonoEnumInfo_t776332500_CustomAttributesCacheGenerator_cache(CustomAttributesCache* cache)
{
	{
		ThreadStaticAttribute_t174455095 * tmp = (ThreadStaticAttribute_t174455095 *)cache->attributes[0];
		ThreadStaticAttribute__ctor_m778228724(tmp, NULL);
	}
}
static void Environment_t3969540094_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void SpecialFolder_t878893316_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void EventArgs_t2765042921_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void ExecutionEngineException_t3097126791_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void FieldAccessException_t3546361566_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void FlagsAttribute_t388727259_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_t2370971113 * tmp = (AttributeUsageAttribute_t2370971113 *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m1170715599(tmp, 16LL, NULL);
		AttributeUsageAttribute_set_Inherited_m2205070461(tmp, false, NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[1];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void FormatException_t4223230285_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void GC_t2835493519_CustomAttributesCacheGenerator_GC_SuppressFinalize_m3884757609(CustomAttributesCache* cache)
{
	{
		ReliabilityContractAttribute_t3009184403 * tmp = (ReliabilityContractAttribute_t3009184403 *)cache->attributes[0];
		ReliabilityContractAttribute__ctor_m2586077688(tmp, 3LL, 2LL, NULL);
	}
}
static void Guid_t_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void ICustomFormatter_t3941648126_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void IFormatProvider_t2002930665_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void IndexOutOfRangeException_t3041194587_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void InvalidCastException_t1009887698_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void InvalidOperationException_t1956282378_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void LoaderOptimization_t3391871335_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void LoaderOptimization_t3391871335_CustomAttributesCacheGenerator_DomainMask(CustomAttributesCache* cache)
{
	{
		ObsoleteAttribute_t3443961000 * tmp = (ObsoleteAttribute_t3443961000 *)cache->attributes[0];
		ObsoleteAttribute__ctor_m1879591032(tmp, NULL);
	}
}
static void LoaderOptimization_t3391871335_CustomAttributesCacheGenerator_DisallowBindings(CustomAttributesCache* cache)
{
	{
		ObsoleteAttribute_t3443961000 * tmp = (ObsoleteAttribute_t3443961000 *)cache->attributes[0];
		ObsoleteAttribute__ctor_m1879591032(tmp, NULL);
	}
}
static void LocalDataStoreSlot_t2417262534_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void Math_t808000742_CustomAttributesCacheGenerator_Math_Max_m3051295998(CustomAttributesCache* cache)
{
	{
		ReliabilityContractAttribute_t3009184403 * tmp = (ReliabilityContractAttribute_t3009184403 *)cache->attributes[0];
		ReliabilityContractAttribute__ctor_m2586077688(tmp, 3LL, 2LL, NULL);
	}
}
static void Math_t808000742_CustomAttributesCacheGenerator_Math_Min_m1276480861(CustomAttributesCache* cache)
{
	{
		ReliabilityContractAttribute_t3009184403 * tmp = (ReliabilityContractAttribute_t3009184403 *)cache->attributes[0];
		ReliabilityContractAttribute__ctor_m2586077688(tmp, 3LL, 2LL, NULL);
	}
}
static void Math_t808000742_CustomAttributesCacheGenerator_Math_Sqrt_m121065893(CustomAttributesCache* cache)
{
	{
		ReliabilityContractAttribute_t3009184403 * tmp = (ReliabilityContractAttribute_t3009184403 *)cache->attributes[0];
		ReliabilityContractAttribute__ctor_m2586077688(tmp, 3LL, 2LL, NULL);
	}
}
static void MemberAccessException_t2381943930_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void MethodAccessException_t3541341797_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void MissingFieldException_t1705577432_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void MissingMemberException_t2177990328_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void MissingMethodException_t924411028_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void MulticastNotSupportedException_t396845527_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void NonSerializedAttribute_t565753174_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_t2370971113 * tmp = (AttributeUsageAttribute_t2370971113 *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m1170715599(tmp, 256LL, NULL);
		AttributeUsageAttribute_set_Inherited_m2205070461(tmp, false, NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[1];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void NotImplementedException_t2045372761_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void NotSupportedException_t404487136_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void NullReferenceException_t2549690990_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void NumberFormatter_t4186985114_CustomAttributesCacheGenerator_threadNumberFormatter(CustomAttributesCache* cache)
{
	{
		ThreadStaticAttribute_t174455095 * tmp = (ThreadStaticAttribute_t174455095 *)cache->attributes[0];
		ThreadStaticAttribute__ctor_m778228724(tmp, NULL);
	}
}
static void ObjectDisposedException_t363085662_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void OperatingSystem_t2121465664_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void OutOfMemoryException_t2147619140_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void OverflowException_t666911278_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void PlatformID_t2641264986_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void PlatformNotSupportedException_t186987067_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void RankException_t2421855710_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void ResolveEventArgs_t960278168_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void RuntimeMethodHandle_t2877888302_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		MonoTODOAttribute_t614642062 * tmp = (MonoTODOAttribute_t614642062 *)cache->attributes[0];
		MonoTODOAttribute__ctor_m2689597370(tmp, il2cpp_codegen_string_new_wrapper("Serialization needs tests"), NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[1];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void RuntimeMethodHandle_t2877888302_CustomAttributesCacheGenerator_RuntimeMethodHandle_Equals_m2047619131(CustomAttributesCache* cache)
{
	{
		ReliabilityContractAttribute_t3009184403 * tmp = (ReliabilityContractAttribute_t3009184403 *)cache->attributes[0];
		ReliabilityContractAttribute__ctor_m2586077688(tmp, 3LL, 2LL, NULL);
	}
}
static void StackOverflowException_t2790203850_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void StringComparer_t124344716_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void StringComparison_t3628495144_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void StringSplitOptions_t1539223423_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		FlagsAttribute_t388727259 * tmp = (FlagsAttribute_t388727259 *)cache->attributes[0];
		FlagsAttribute__ctor_m3834565173(tmp, NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[1];
		ComVisibleAttribute__ctor_m29339631(tmp, false, NULL);
	}
}
static void SystemException_t3635077273_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void ThreadStaticAttribute_t174455095_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_t2370971113 * tmp = (AttributeUsageAttribute_t2370971113 *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m1170715599(tmp, 256LL, NULL);
		AttributeUsageAttribute_set_Inherited_m2205070461(tmp, false, NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[1];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void TimeSpan_t2986675223_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void TimeZone_t2898390368_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void TypeCode_t2052347577_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void TypeInitializationException_t4108067351_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void TypeLoadException_t2681750783_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void UnauthorizedAccessException_t934748042_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void UnhandledExceptionEventArgs_t3669884462_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void UnhandledExceptionEventArgs_t3669884462_CustomAttributesCacheGenerator_UnhandledExceptionEventArgs_get_ExceptionObject_m2012398684(CustomAttributesCache* cache)
{
	{
		ReliabilityContractAttribute_t3009184403 * tmp = (ReliabilityContractAttribute_t3009184403 *)cache->attributes[0];
		ReliabilityContractAttribute__ctor_m2586077688(tmp, 3LL, 2LL, NULL);
	}
}
static void UnhandledExceptionEventArgs_t3669884462_CustomAttributesCacheGenerator_UnhandledExceptionEventArgs_get_IsTerminating_m3281711421(CustomAttributesCache* cache)
{
	{
		ReliabilityContractAttribute_t3009184403 * tmp = (ReliabilityContractAttribute_t3009184403 *)cache->attributes[0];
		ReliabilityContractAttribute__ctor_m2586077688(tmp, 3LL, 2LL, NULL);
	}
}
static void Version_t900644674_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void WeakReference_t3998924468_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void MemberFilter_t1197335030_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void TypeFilter_t139072584_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void CrossContextDelegate_t642168454_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void HeaderHandler_t3384099523_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void ThreadStart_t8027522_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void TimerCallback_t2264714419_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void WaitCallback_t3170229073_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void AppDomainInitializer_t1811168608_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void AssemblyLoadEventHandler_t877953269_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void EventHandler_t2094584875_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void ResolveEventHandler_t2027096794_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void UnhandledExceptionEventHandler_t3129202591_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void U3CPrivateImplementationDetailsU3E_t3250056442_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
}
static void g_System_Assembly_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		NeutralResourcesLanguageAttribute_t2446220560 * tmp = (NeutralResourcesLanguageAttribute_t2446220560 *)cache->attributes[0];
		NeutralResourcesLanguageAttribute__ctor_m1815010898(tmp, il2cpp_codegen_string_new_wrapper("en-US"), NULL);
	}
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[1];
		CLSCompliantAttribute__ctor_m3885160237(tmp, true, NULL);
	}
	{
		AssemblyInformationalVersionAttribute_t2163289957 * tmp = (AssemblyInformationalVersionAttribute_t2163289957 *)cache->attributes[2];
		AssemblyInformationalVersionAttribute__ctor_m1415206487(tmp, il2cpp_codegen_string_new_wrapper("2.0.50727.1433"), NULL);
	}
	{
		SatelliteContractVersionAttribute_t2421164883 * tmp = (SatelliteContractVersionAttribute_t2421164883 *)cache->attributes[3];
		SatelliteContractVersionAttribute__ctor_m4270552604(tmp, il2cpp_codegen_string_new_wrapper("2.0.0.0"), NULL);
	}
	{
		AssemblyCopyrightAttribute_t1775796795 * tmp = (AssemblyCopyrightAttribute_t1775796795 *)cache->attributes[4];
		AssemblyCopyrightAttribute__ctor_m4282510565(tmp, il2cpp_codegen_string_new_wrapper("(c) various MONO Authors"), NULL);
	}
	{
		AssemblyProductAttribute_t3365982468 * tmp = (AssemblyProductAttribute_t3365982468 *)cache->attributes[5];
		AssemblyProductAttribute__ctor_m2217606340(tmp, il2cpp_codegen_string_new_wrapper("MONO Common language infrastructure"), NULL);
	}
	{
		AssemblyCompanyAttribute_t272143903 * tmp = (AssemblyCompanyAttribute_t272143903 *)cache->attributes[6];
		AssemblyCompanyAttribute__ctor_m1315167307(tmp, il2cpp_codegen_string_new_wrapper("MONO development team"), NULL);
	}
	{
		AssemblyDefaultAliasAttribute_t356814493 * tmp = (AssemblyDefaultAliasAttribute_t356814493 *)cache->attributes[7];
		AssemblyDefaultAliasAttribute__ctor_m393492554(tmp, il2cpp_codegen_string_new_wrapper("System.dll"), NULL);
	}
	{
		AssemblyDescriptionAttribute_t1589567422 * tmp = (AssemblyDescriptionAttribute_t1589567422 *)cache->attributes[8];
		AssemblyDescriptionAttribute__ctor_m3502566481(tmp, il2cpp_codegen_string_new_wrapper("System.dll"), NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[9];
		ComVisibleAttribute__ctor_m29339631(tmp, false, NULL);
	}
	{
		AssemblyTitleAttribute_t2942119159 * tmp = (AssemblyTitleAttribute_t2942119159 *)cache->attributes[10];
		AssemblyTitleAttribute__ctor_m4091976430(tmp, il2cpp_codegen_string_new_wrapper("System.dll"), NULL);
	}
	{
		RuntimeCompatibilityAttribute_t38650954 * tmp = (RuntimeCompatibilityAttribute_t38650954 *)cache->attributes[11];
		RuntimeCompatibilityAttribute__ctor_m853651104(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m4154715430(tmp, true, NULL);
	}
	{
		CompilationRelaxationsAttribute_t606593683 * tmp = (CompilationRelaxationsAttribute_t606593683 *)cache->attributes[12];
		CompilationRelaxationsAttribute__ctor_m1638540989(tmp, 8LL, NULL);
	}
	{
		DebuggableAttribute_t3717120398 * tmp = (DebuggableAttribute_t3717120398 *)cache->attributes[13];
		DebuggableAttribute__ctor_m3553871532(tmp, 2LL, NULL);
	}
	{
		AssemblyDelaySignAttribute_t1978111684 * tmp = (AssemblyDelaySignAttribute_t1978111684 *)cache->attributes[14];
		AssemblyDelaySignAttribute__ctor_m3802049740(tmp, true, NULL);
	}
	{
		AssemblyKeyFileAttribute_t1077071033 * tmp = (AssemblyKeyFileAttribute_t1077071033 *)cache->attributes[15];
		AssemblyKeyFileAttribute__ctor_m1870834162(tmp, il2cpp_codegen_string_new_wrapper("../ecma.pub"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[16];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("System.Net, PublicKey=00240000048000009400000006020000002400005253413100040000010001008D56C76F9E8649383049F383C44BE0EC204181822A6C31CF5EB7EF486944D032188EA1D3920763712CCB12D75FB77E9811149E6148E5D32FBAAB37611C1878DDC19E20EF135D0CB2CFF2BFEC3D115810C3D9069638FE4BE215DBF795861920E5AB6F7DB2E2CEEF136AC23D5DD2BF031700AEC232F6C6B1C785B4305C123B37AB"), NULL);
	}
	{
		AssemblyFileVersionAttribute_t3786563949 * tmp = (AssemblyFileVersionAttribute_t3786563949 *)cache->attributes[17];
		AssemblyFileVersionAttribute__ctor_m4281768118(tmp, il2cpp_codegen_string_new_wrapper("2.0.50727.1433"), NULL);
	}
}
static void Locale_t2448440937_CustomAttributesCacheGenerator_Locale_GetText_m3635108345____args1(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t3330428127 * tmp = (ParamArrayAttribute_t3330428127 *)cache->attributes[0];
		ParamArrayAttribute__ctor_m1696407647(tmp, NULL);
	}
}
static void MonoTODOAttribute_t614642063_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_t2370971113 * tmp = (AttributeUsageAttribute_t2370971113 *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m1170715599(tmp, 32767LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m1313295265(tmp, true, NULL);
	}
}
static void Queue_1_t542587364_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, false, NULL);
	}
}
static void Stack_1_t2711020695_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, false, NULL);
	}
}
static void HybridDictionary_t1698560403_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DefaultMemberAttribute_t2956431904 * tmp = (DefaultMemberAttribute_t2956431904 *)cache->attributes[0];
		DefaultMemberAttribute__ctor_m3129895323(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
	}
}
static void ListDictionary_t1976214390_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DefaultMemberAttribute_t2956431904 * tmp = (DefaultMemberAttribute_t2956431904 *)cache->attributes[0];
		DefaultMemberAttribute__ctor_m3129895323(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
	}
}
static void NameObjectCollectionBase_t1648796668_CustomAttributesCacheGenerator_NameObjectCollectionBase_FindFirstMatchedItem_m2942906622(CustomAttributesCache* cache)
{
	{
		MonoTODOAttribute_t614642063 * tmp = (MonoTODOAttribute_t614642063 *)cache->attributes[0];
		MonoTODOAttribute__ctor_m423555603(tmp, NULL);
	}
}
static void KeysCollection_t14636749_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DefaultMemberAttribute_t2956431904 * tmp = (DefaultMemberAttribute_t2956431904 *)cache->attributes[0];
		DefaultMemberAttribute__ctor_m3129895323(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
	}
}
static void NameValueCollection_t2484058343_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DefaultMemberAttribute_t2956431904 * tmp = (DefaultMemberAttribute_t2956431904 *)cache->attributes[0];
		DefaultMemberAttribute__ctor_m3129895323(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
	}
}
static void TypeConverter_t927856623_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
}
static void TypeConverterAttribute_t553551222_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		AttributeUsageAttribute_t2370971113 * tmp = (AttributeUsageAttribute_t2370971113 *)cache->attributes[1];
		AttributeUsageAttribute__ctor_m1170715599(tmp, 32767LL, NULL);
	}
}
static void SslPolicyErrors_t2398368690_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		FlagsAttribute_t388727259 * tmp = (FlagsAttribute_t388727259 *)cache->attributes[0];
		FlagsAttribute__ctor_m3834565173(tmp, NULL);
	}
}
static void FileWebRequest_t445810259_CustomAttributesCacheGenerator_FileWebRequest__ctor_m2545559098(CustomAttributesCache* cache)
{
	{
		ObsoleteAttribute_t3443961000 * tmp = (ObsoleteAttribute_t3443961000 *)cache->attributes[0];
		ObsoleteAttribute__ctor_m1055576761(tmp, il2cpp_codegen_string_new_wrapper("Serialization is obsoleted for this type"), false, NULL);
	}
}
static void FtpWebRequest_t1919013084_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache1C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
}
static void FtpWebRequest_t1919013084_CustomAttributesCacheGenerator_FtpWebRequest_U3CcallbackU3Em__B_m1047401003(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
}
static void GlobalProxySelection_t2070377985_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ObsoleteAttribute_t3443961000 * tmp = (ObsoleteAttribute_t3443961000 *)cache->attributes[0];
		ObsoleteAttribute__ctor_m3274828507(tmp, il2cpp_codegen_string_new_wrapper("Use WebRequest.DefaultProxy instead"), NULL);
	}
}
static void HttpWebRequest_t3674333275_CustomAttributesCacheGenerator_HttpWebRequest__ctor_m3658280394(CustomAttributesCache* cache)
{
	{
		ObsoleteAttribute_t3443961000 * tmp = (ObsoleteAttribute_t3443961000 *)cache->attributes[0];
		ObsoleteAttribute__ctor_m1055576761(tmp, il2cpp_codegen_string_new_wrapper("Serialization is obsoleted for this type"), false, NULL);
	}
}
static void IPv6Address_t2439851581_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DefaultMemberAttribute_t2956431904 * tmp = (DefaultMemberAttribute_t2956431904 *)cache->attributes[0];
		DefaultMemberAttribute__ctor_m3129895323(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
	}
}
static void SecurityProtocolType_t26325249_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		FlagsAttribute_t388727259 * tmp = (FlagsAttribute_t388727259 *)cache->attributes[0];
		FlagsAttribute__ctor_m3834565173(tmp, NULL);
	}
}
static void ServicePointManager_t86738999_CustomAttributesCacheGenerator_ServicePointManager_t86738999____CertificatePolicy_PropertyInfo(CustomAttributesCache* cache)
{
	{
		ObsoleteAttribute_t3443961000 * tmp = (ObsoleteAttribute_t3443961000 *)cache->attributes[0];
		ObsoleteAttribute__ctor_m1055576761(tmp, il2cpp_codegen_string_new_wrapper("Use ServerCertificateValidationCallback instead"), false, NULL);
	}
}
static void ServicePointManager_t86738999_CustomAttributesCacheGenerator_ServicePointManager_t86738999____CheckCertificateRevocationList_PropertyInfo(CustomAttributesCache* cache)
{
	{
		MonoTODOAttribute_t614642063 * tmp = (MonoTODOAttribute_t614642063 *)cache->attributes[0];
		MonoTODOAttribute__ctor_m2017426088(tmp, il2cpp_codegen_string_new_wrapper("CRL checks not implemented"), NULL);
	}
}
static void WebHeaderCollection_t2266125739_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[0];
		ComVisibleAttribute__ctor_m29339631(tmp, true, NULL);
	}
	{
		DefaultMemberAttribute_t2956431904 * tmp = (DefaultMemberAttribute_t2956431904 *)cache->attributes[1];
		DefaultMemberAttribute__ctor_m3129895323(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
	}
}
static void WebRequest_t3724865973_CustomAttributesCacheGenerator_WebRequest_GetDefaultWebProxy_m291030002(CustomAttributesCache* cache)
{
	{
		MonoTODOAttribute_t614642063 * tmp = (MonoTODOAttribute_t614642063 *)cache->attributes[0];
		MonoTODOAttribute__ctor_m2017426088(tmp, il2cpp_codegen_string_new_wrapper("Needs to respect Module, Proxy.AutoDetect, and Proxy.ScriptLocation config settings"), NULL);
	}
}
static void OpenFlags_t3269158238_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		FlagsAttribute_t388727259 * tmp = (FlagsAttribute_t388727259 *)cache->attributes[0];
		FlagsAttribute__ctor_m3834565173(tmp, NULL);
	}
}
static void PublicKey_t1535790316_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map9(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
}
static void X500DistinguishedName_t2651651201_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		MonoTODOAttribute_t614642063 * tmp = (MonoTODOAttribute_t614642063 *)cache->attributes[0];
		MonoTODOAttribute__ctor_m2017426088(tmp, il2cpp_codegen_string_new_wrapper("Some X500DistinguishedNameFlags options aren't supported, like DoNotUsePlusSign, DoNotUseQuotes and ForceUTF8Encoding"), NULL);
	}
}
static void X500DistinguishedNameFlags_t2114626771_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		FlagsAttribute_t388727259 * tmp = (FlagsAttribute_t388727259 *)cache->attributes[0];
		FlagsAttribute__ctor_m3834565173(tmp, NULL);
	}
}
static void X509Certificate2_t3206508497_CustomAttributesCacheGenerator_X509Certificate2_GetNameInfo_m3374661201(CustomAttributesCache* cache)
{
	{
		MonoTODOAttribute_t614642063 * tmp = (MonoTODOAttribute_t614642063 *)cache->attributes[0];
		MonoTODOAttribute__ctor_m2017426088(tmp, il2cpp_codegen_string_new_wrapper("always return String.Empty for UpnName, DnsFromAlternativeName and UrlName"), NULL);
	}
}
static void X509Certificate2_t3206508497_CustomAttributesCacheGenerator_X509Certificate2_Import_m2733344782(CustomAttributesCache* cache)
{
	{
		MonoTODOAttribute_t614642063 * tmp = (MonoTODOAttribute_t614642063 *)cache->attributes[0];
		MonoTODOAttribute__ctor_m2017426088(tmp, il2cpp_codegen_string_new_wrapper("missing KeyStorageFlags support"), NULL);
	}
}
static void X509Certificate2_t3206508497_CustomAttributesCacheGenerator_X509Certificate2_Verify_m1956073113(CustomAttributesCache* cache)
{
	{
		MonoTODOAttribute_t614642063 * tmp = (MonoTODOAttribute_t614642063 *)cache->attributes[0];
		MonoTODOAttribute__ctor_m2017426088(tmp, il2cpp_codegen_string_new_wrapper("by default this depends on the incomplete X509Chain"), NULL);
	}
}
static void X509Certificate2Collection_t1044733850_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DefaultMemberAttribute_t2956431904 * tmp = (DefaultMemberAttribute_t2956431904 *)cache->attributes[0];
		DefaultMemberAttribute__ctor_m3129895323(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
	}
}
static void X509Certificate2Collection_t1044733850_CustomAttributesCacheGenerator_X509Certificate2Collection_AddRange_m3259897552(CustomAttributesCache* cache)
{
	{
		MonoTODOAttribute_t614642063 * tmp = (MonoTODOAttribute_t614642063 *)cache->attributes[0];
		MonoTODOAttribute__ctor_m2017426088(tmp, il2cpp_codegen_string_new_wrapper("Method isn't transactional (like documented)"), NULL);
	}
}
static void X509Certificate2Collection_t1044733850_CustomAttributesCacheGenerator_X509Certificate2Collection_Find_m1361050413(CustomAttributesCache* cache)
{
	{
		MonoTODOAttribute_t614642063 * tmp = (MonoTODOAttribute_t614642063 *)cache->attributes[0];
		MonoTODOAttribute__ctor_m2017426088(tmp, il2cpp_codegen_string_new_wrapper("Does not support X509FindType.FindByTemplateName, FindByApplicationPolicy and FindByCertificatePolicy"), NULL);
	}
}
static void X509CertificateCollection_t3853537301_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DefaultMemberAttribute_t2956431904 * tmp = (DefaultMemberAttribute_t2956431904 *)cache->attributes[0];
		DefaultMemberAttribute__ctor_m3129895323(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
	}
}
static void X509Chain_t3606654252_CustomAttributesCacheGenerator_U3CU3Ef__switchU24mapB(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
}
static void X509Chain_t3606654252_CustomAttributesCacheGenerator_U3CU3Ef__switchU24mapC(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
}
static void X509Chain_t3606654252_CustomAttributesCacheGenerator_U3CU3Ef__switchU24mapD(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
}
static void X509Chain_t3606654252_CustomAttributesCacheGenerator_X509Chain_Build_m3453334197(CustomAttributesCache* cache)
{
	{
		MonoTODOAttribute_t614642063 * tmp = (MonoTODOAttribute_t614642063 *)cache->attributes[0];
		MonoTODOAttribute__ctor_m2017426088(tmp, il2cpp_codegen_string_new_wrapper("Not totally RFC3280 compliant, but neither is MS implementation..."), NULL);
	}
}
static void X509ChainElementCollection_t1402597378_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DefaultMemberAttribute_t2956431904 * tmp = (DefaultMemberAttribute_t2956431904 *)cache->attributes[0];
		DefaultMemberAttribute__ctor_m3129895323(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
	}
}
static void X509ChainStatusFlags_t2184184987_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		FlagsAttribute_t388727259 * tmp = (FlagsAttribute_t388727259 *)cache->attributes[0];
		FlagsAttribute__ctor_m3834565173(tmp, NULL);
	}
}
static void X509EnhancedKeyUsageExtension_t4248079241_CustomAttributesCacheGenerator_U3CU3Ef__switchU24mapE(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
}
static void X509ExtensionCollection_t2858212424_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DefaultMemberAttribute_t2956431904 * tmp = (DefaultMemberAttribute_t2956431904 *)cache->attributes[0];
		DefaultMemberAttribute__ctor_m3129895323(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
	}
}
static void X509KeyUsageFlags_t1189859534_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		FlagsAttribute_t388727259 * tmp = (FlagsAttribute_t388727259 *)cache->attributes[0];
		FlagsAttribute__ctor_m3834565173(tmp, NULL);
	}
}
static void X509Store_t2224578885_CustomAttributesCacheGenerator_U3CU3Ef__switchU24mapF(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
}
static void X509VerificationFlags_t1372417108_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		FlagsAttribute_t388727259 * tmp = (FlagsAttribute_t388727259 *)cache->attributes[0];
		FlagsAttribute__ctor_m3834565173(tmp, NULL);
	}
}
static void AsnEncodedData_t1210003190_CustomAttributesCacheGenerator_U3CU3Ef__switchU24mapA(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
}
static void Oid_t3889750512_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map10(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
}
static void OidCollection_t2947752789_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DefaultMemberAttribute_t2956431904 * tmp = (DefaultMemberAttribute_t2956431904 *)cache->attributes[0];
		DefaultMemberAttribute__ctor_m3129895323(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
	}
}
static void CaptureCollection_t556975539_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DefaultMemberAttribute_t2956431904 * tmp = (DefaultMemberAttribute_t2956431904 *)cache->attributes[0];
		DefaultMemberAttribute__ctor_m3129895323(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
	}
}
static void GroupCollection_t310655671_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DefaultMemberAttribute_t2956431904 * tmp = (DefaultMemberAttribute_t2956431904 *)cache->attributes[0];
		DefaultMemberAttribute__ctor_m3129895323(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
	}
}
static void MatchCollection_t3986081353_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DefaultMemberAttribute_t2956431904 * tmp = (DefaultMemberAttribute_t2956431904 *)cache->attributes[0];
		DefaultMemberAttribute__ctor_m3129895323(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
	}
}
static void RegexOptions_t2920399184_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		FlagsAttribute_t388727259 * tmp = (FlagsAttribute_t388727259 *)cache->attributes[0];
		FlagsAttribute__ctor_m3834565173(tmp, NULL);
	}
}
static void OpFlags_t3924773783_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		FlagsAttribute_t388727259 * tmp = (FlagsAttribute_t388727259 *)cache->attributes[0];
		FlagsAttribute__ctor_m3834565173(tmp, NULL);
	}
}
static void IntervalCollection_t3277036616_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DefaultMemberAttribute_t2956431904 * tmp = (DefaultMemberAttribute_t2956431904 *)cache->attributes[0];
		DefaultMemberAttribute__ctor_m3129895323(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
	}
}
static void ExpressionCollection_t929422539_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DefaultMemberAttribute_t2956431904 * tmp = (DefaultMemberAttribute_t2956431904 *)cache->attributes[0];
		DefaultMemberAttribute__ctor_m3129895323(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
	}
}
static void Uri_t1131101833_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Uri_t1131101833_CustomAttributesCacheGenerator_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		TypeConverterAttribute_t553551222 * tmp = (TypeConverterAttribute_t553551222 *)cache->attributes[0];
		TypeConverterAttribute__ctor_m2796508157(tmp, il2cpp_codegen_type_get_object(UriTypeConverter_t3526853594_0_0_0_var), NULL);
	}
}
static void Uri_t1131101833_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map14(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
}
static void Uri_t1131101833_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map15(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
}
static void Uri_t1131101833_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map16(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
}
static void Uri_t1131101833_CustomAttributesCacheGenerator_Uri__ctor_m3032927579(CustomAttributesCache* cache)
{
	{
		ObsoleteAttribute_t3443961000 * tmp = (ObsoleteAttribute_t3443961000 *)cache->attributes[0];
		ObsoleteAttribute__ctor_m1879591032(tmp, NULL);
	}
}
static void Uri_t1131101833_CustomAttributesCacheGenerator_Uri_EscapeString_m4163751340(CustomAttributesCache* cache)
{
	{
		ObsoleteAttribute_t3443961000 * tmp = (ObsoleteAttribute_t3443961000 *)cache->attributes[0];
		ObsoleteAttribute__ctor_m1879591032(tmp, NULL);
	}
}
static void Uri_t1131101833_CustomAttributesCacheGenerator_Uri_Unescape_m2753918802(CustomAttributesCache* cache)
{
	{
		ObsoleteAttribute_t3443961000 * tmp = (ObsoleteAttribute_t3443961000 *)cache->attributes[0];
		ObsoleteAttribute__ctor_m1879591032(tmp, NULL);
	}
}
static void UriParser_t1608860109_CustomAttributesCacheGenerator_UriParser_OnRegister_m3456593883(CustomAttributesCache* cache)
{
	{
		MonoTODOAttribute_t614642063 * tmp = (MonoTODOAttribute_t614642063 *)cache->attributes[0];
		MonoTODOAttribute__ctor_m423555603(tmp, NULL);
	}
}
static void U3CPrivateImplementationDetailsU3E_t3250056443_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
}
static void g_Mono_Security_Assembly_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AssemblyCopyrightAttribute_t1775796795 * tmp = (AssemblyCopyrightAttribute_t1775796795 *)cache->attributes[0];
		AssemblyCopyrightAttribute__ctor_m4282510565(tmp, il2cpp_codegen_string_new_wrapper("(c) 2003-2004 Various Authors"), NULL);
	}
	{
		AssemblyDescriptionAttribute_t1589567422 * tmp = (AssemblyDescriptionAttribute_t1589567422 *)cache->attributes[1];
		AssemblyDescriptionAttribute__ctor_m3502566481(tmp, il2cpp_codegen_string_new_wrapper("Mono.Security.dll"), NULL);
	}
	{
		AssemblyProductAttribute_t3365982468 * tmp = (AssemblyProductAttribute_t3365982468 *)cache->attributes[2];
		AssemblyProductAttribute__ctor_m2217606340(tmp, il2cpp_codegen_string_new_wrapper("MONO CLI"), NULL);
	}
	{
		AssemblyTitleAttribute_t2942119159 * tmp = (AssemblyTitleAttribute_t2942119159 *)cache->attributes[3];
		AssemblyTitleAttribute__ctor_m4091976430(tmp, il2cpp_codegen_string_new_wrapper("Mono.Security.dll"), NULL);
	}
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[4];
		CLSCompliantAttribute__ctor_m3885160237(tmp, true, NULL);
	}
	{
		AssemblyCompanyAttribute_t272143903 * tmp = (AssemblyCompanyAttribute_t272143903 *)cache->attributes[5];
		AssemblyCompanyAttribute__ctor_m1315167307(tmp, il2cpp_codegen_string_new_wrapper("MONO development team"), NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[6];
		ComVisibleAttribute__ctor_m29339631(tmp, false, NULL);
	}
	{
		RuntimeCompatibilityAttribute_t38650954 * tmp = (RuntimeCompatibilityAttribute_t38650954 *)cache->attributes[7];
		RuntimeCompatibilityAttribute__ctor_m853651104(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m4154715430(tmp, true, NULL);
	}
	{
		AssemblyKeyFileAttribute_t1077071033 * tmp = (AssemblyKeyFileAttribute_t1077071033 *)cache->attributes[8];
		AssemblyKeyFileAttribute__ctor_m1870834162(tmp, il2cpp_codegen_string_new_wrapper("../mono.pub"), NULL);
	}
	{
		AssemblyDelaySignAttribute_t1978111684 * tmp = (AssemblyDelaySignAttribute_t1978111684 *)cache->attributes[9];
		AssemblyDelaySignAttribute__ctor_m3802049740(tmp, true, NULL);
	}
	{
		NeutralResourcesLanguageAttribute_t2446220560 * tmp = (NeutralResourcesLanguageAttribute_t2446220560 *)cache->attributes[10];
		NeutralResourcesLanguageAttribute__ctor_m1815010898(tmp, il2cpp_codegen_string_new_wrapper("en-US"), NULL);
	}
}
static void BigInteger_t1715200597_CustomAttributesCacheGenerator_BigInteger__ctor_m2308028736(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void BigInteger_t1715200597_CustomAttributesCacheGenerator_BigInteger__ctor_m615970763(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void BigInteger_t1715200597_CustomAttributesCacheGenerator_BigInteger__ctor_m243072073(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void BigInteger_t1715200597_CustomAttributesCacheGenerator_BigInteger_SetBit_m1692600095(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void BigInteger_t1715200597_CustomAttributesCacheGenerator_BigInteger_SetBit_m1046987020(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void BigInteger_t1715200597_CustomAttributesCacheGenerator_BigInteger_ToString_m1741067599(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void BigInteger_t1715200597_CustomAttributesCacheGenerator_BigInteger_ToString_m2080477611(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void BigInteger_t1715200597_CustomAttributesCacheGenerator_BigInteger_op_Implicit_m2994795725(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void BigInteger_t1715200597_CustomAttributesCacheGenerator_BigInteger_op_Modulus_m1177843781(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void BigInteger_t1715200597_CustomAttributesCacheGenerator_BigInteger_op_Equality_m4238039561(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void BigInteger_t1715200597_CustomAttributesCacheGenerator_BigInteger_op_Inequality_m1859198082(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void ModulusRing_t2599161149_CustomAttributesCacheGenerator_ModulusRing_Pow_m3064208772(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m3885160237(tmp, false, NULL);
	}
}
static void ASN1_t450259933_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DefaultMemberAttribute_t2956431904 * tmp = (DefaultMemberAttribute_t2956431904 *)cache->attributes[0];
		DefaultMemberAttribute__ctor_m3129895323(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
	}
}
static void PKCS12_t2451188495_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map5(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
}
static void PKCS12_t2451188495_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map6(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
}
static void PKCS12_t2451188495_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map7(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
}
static void PKCS12_t2451188495_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map8(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
}
static void PKCS12_t2451188495_CustomAttributesCacheGenerator_U3CU3Ef__switchU24mapC(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
}
static void X509Certificate_t2235220323_CustomAttributesCacheGenerator_U3CU3Ef__switchU24mapF(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
}
static void X509Certificate_t2235220323_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map10(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
}
static void X509Certificate_t2235220323_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map11(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
}
static void X509CertificateCollection_t1815624851_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DefaultMemberAttribute_t2956431904 * tmp = (DefaultMemberAttribute_t2956431904 *)cache->attributes[0];
		DefaultMemberAttribute__ctor_m3129895323(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
	}
}
static void X509ChainStatusFlags_t991867662_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		FlagsAttribute_t388727259 * tmp = (FlagsAttribute_t388727259 *)cache->attributes[0];
		FlagsAttribute__ctor_m3834565173(tmp, NULL);
	}
}
static void X509Crl_t2767467075_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DefaultMemberAttribute_t2956431904 * tmp = (DefaultMemberAttribute_t2956431904 *)cache->attributes[0];
		DefaultMemberAttribute__ctor_m3129895323(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
	}
}
static void X509Crl_t2767467075_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map13(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
}
static void X509ExtensionCollection_t3055523518_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DefaultMemberAttribute_t2956431904 * tmp = (DefaultMemberAttribute_t2956431904 *)cache->attributes[0];
		DefaultMemberAttribute__ctor_m3129895323(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
	}
}
static void ExtendedKeyUsageExtension_t450188887_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map14(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
}
static void KeyUsages_t3469123324_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		FlagsAttribute_t388727259 * tmp = (FlagsAttribute_t388727259 *)cache->attributes[0];
		FlagsAttribute__ctor_m3834565173(tmp, NULL);
	}
}
static void CertTypes_t480212372_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		FlagsAttribute_t388727259 * tmp = (FlagsAttribute_t388727259 *)cache->attributes[0];
		FlagsAttribute__ctor_m3834565173(tmp, NULL);
	}
}
static void CipherSuiteCollection_t3340801564_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DefaultMemberAttribute_t2956431904 * tmp = (DefaultMemberAttribute_t2956431904 *)cache->attributes[0];
		DefaultMemberAttribute__ctor_m3129895323(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
	}
}
static void HttpsClientStream_t1900802475_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache2(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
}
static void HttpsClientStream_t1900802475_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache3(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
}
static void HttpsClientStream_t1900802475_CustomAttributesCacheGenerator_HttpsClientStream_U3CHttpsClientStreamU3Em__0_m4013616965(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
}
static void HttpsClientStream_t1900802475_CustomAttributesCacheGenerator_HttpsClientStream_U3CHttpsClientStreamU3Em__1_m845478519(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
}
static void RSASslSignatureDeformatter_t3859776653_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map15(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
}
static void RSASslSignatureFormatter_t3826029953_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map16(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
}
static void SecurityProtocolType_t4172395504_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		FlagsAttribute_t388727259 * tmp = (FlagsAttribute_t388727259 *)cache->attributes[0];
		FlagsAttribute__ctor_m3834565173(tmp, NULL);
	}
}
static void U3CPrivateImplementationDetailsU3E_t3250056444_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
}
static void g_System_Core_Assembly_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AssemblyCompanyAttribute_t272143903 * tmp = (AssemblyCompanyAttribute_t272143903 *)cache->attributes[0];
		AssemblyCompanyAttribute__ctor_m1315167307(tmp, il2cpp_codegen_string_new_wrapper("MONO development team"), NULL);
	}
	{
		AssemblyDelaySignAttribute_t1978111684 * tmp = (AssemblyDelaySignAttribute_t1978111684 *)cache->attributes[1];
		AssemblyDelaySignAttribute__ctor_m3802049740(tmp, true, NULL);
	}
	{
		CLSCompliantAttribute_t2118253839 * tmp = (CLSCompliantAttribute_t2118253839 *)cache->attributes[2];
		CLSCompliantAttribute__ctor_m3885160237(tmp, true, NULL);
	}
	{
		NeutralResourcesLanguageAttribute_t2446220560 * tmp = (NeutralResourcesLanguageAttribute_t2446220560 *)cache->attributes[3];
		NeutralResourcesLanguageAttribute__ctor_m1815010898(tmp, il2cpp_codegen_string_new_wrapper("en-US"), NULL);
	}
	{
		AssemblyFileVersionAttribute_t3786563949 * tmp = (AssemblyFileVersionAttribute_t3786563949 *)cache->attributes[4];
		AssemblyFileVersionAttribute__ctor_m4281768118(tmp, il2cpp_codegen_string_new_wrapper("3.5.21022.8"), NULL);
	}
	{
		AssemblyInformationalVersionAttribute_t2163289957 * tmp = (AssemblyInformationalVersionAttribute_t2163289957 *)cache->attributes[5];
		AssemblyInformationalVersionAttribute__ctor_m1415206487(tmp, il2cpp_codegen_string_new_wrapper("3.5.21022.8"), NULL);
	}
	{
		SatelliteContractVersionAttribute_t2421164883 * tmp = (SatelliteContractVersionAttribute_t2421164883 *)cache->attributes[6];
		SatelliteContractVersionAttribute__ctor_m4270552604(tmp, il2cpp_codegen_string_new_wrapper("3.5.0.0"), NULL);
	}
	{
		AssemblyCopyrightAttribute_t1775796795 * tmp = (AssemblyCopyrightAttribute_t1775796795 *)cache->attributes[7];
		AssemblyCopyrightAttribute__ctor_m4282510565(tmp, il2cpp_codegen_string_new_wrapper("(c) various MONO Authors"), NULL);
	}
	{
		AssemblyKeyFileAttribute_t1077071033 * tmp = (AssemblyKeyFileAttribute_t1077071033 *)cache->attributes[8];
		AssemblyKeyFileAttribute__ctor_m1870834162(tmp, il2cpp_codegen_string_new_wrapper("../ecma.pub"), NULL);
	}
	{
		RuntimeCompatibilityAttribute_t38650954 * tmp = (RuntimeCompatibilityAttribute_t38650954 *)cache->attributes[9];
		RuntimeCompatibilityAttribute__ctor_m853651104(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m4154715430(tmp, true, NULL);
	}
	{
		DebuggableAttribute_t3717120398 * tmp = (DebuggableAttribute_t3717120398 *)cache->attributes[10];
		DebuggableAttribute__ctor_m3553871532(tmp, 2LL, NULL);
	}
	{
		CompilationRelaxationsAttribute_t606593683 * tmp = (CompilationRelaxationsAttribute_t606593683 *)cache->attributes[11];
		CompilationRelaxationsAttribute__ctor_m1638540989(tmp, 8LL, NULL);
	}
	{
		ComVisibleAttribute_t2289974704 * tmp = (ComVisibleAttribute_t2289974704 *)cache->attributes[12];
		ComVisibleAttribute__ctor_m29339631(tmp, false, NULL);
	}
	{
		StringFreezingAttribute_t4005798349 * tmp = (StringFreezingAttribute_t4005798349 *)cache->attributes[13];
		StringFreezingAttribute__ctor_m1275687280(tmp, NULL);
	}
	{
		SecurityCriticalAttribute_t3270381382 * tmp = (SecurityCriticalAttribute_t3270381382 *)cache->attributes[14];
		SecurityCriticalAttribute__ctor_m248669408(tmp, NULL);
	}
	{
		DefaultDependencyAttribute_t2058824218 * tmp = (DefaultDependencyAttribute_t2058824218 *)cache->attributes[15];
		DefaultDependencyAttribute__ctor_m552191618(tmp, 1LL, NULL);
	}
	{
		AllowPartiallyTrustedCallersAttribute_t2708656030 * tmp = (AllowPartiallyTrustedCallersAttribute_t2708656030 *)cache->attributes[16];
		AllowPartiallyTrustedCallersAttribute__ctor_m1393518081(tmp, NULL);
	}
	{
		AssemblyProductAttribute_t3365982468 * tmp = (AssemblyProductAttribute_t3365982468 *)cache->attributes[17];
		AssemblyProductAttribute__ctor_m2217606340(tmp, il2cpp_codegen_string_new_wrapper("MONO Common language infrastructure"), NULL);
	}
	{
		ExtensionAttribute_t1721836986 * tmp = (ExtensionAttribute_t1721836986 *)cache->attributes[18];
		ExtensionAttribute__ctor_m831032830(tmp, NULL);
	}
	{
		AssemblyDefaultAliasAttribute_t356814493 * tmp = (AssemblyDefaultAliasAttribute_t356814493 *)cache->attributes[19];
		AssemblyDefaultAliasAttribute__ctor_m393492554(tmp, il2cpp_codegen_string_new_wrapper("System.Core.dll"), NULL);
	}
	{
		AssemblyDescriptionAttribute_t1589567422 * tmp = (AssemblyDescriptionAttribute_t1589567422 *)cache->attributes[20];
		AssemblyDescriptionAttribute__ctor_m3502566481(tmp, il2cpp_codegen_string_new_wrapper("System.Core.dll"), NULL);
	}
	{
		AssemblyTitleAttribute_t2942119159 * tmp = (AssemblyTitleAttribute_t2942119159 *)cache->attributes[21];
		AssemblyTitleAttribute__ctor_m4091976430(tmp, il2cpp_codegen_string_new_wrapper("System.Core.dll"), NULL);
	}
}
static void ExtensionAttribute_t1721836986_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_t2370971113 * tmp = (AttributeUsageAttribute_t2370971113 *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m1170715599(tmp, 69LL, NULL);
	}
}
static void Locale_t2448440939_CustomAttributesCacheGenerator_Locale_GetText_m2618335777____args1(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t3330428127 * tmp = (ParamArrayAttribute_t3330428127 *)cache->attributes[0];
		ParamArrayAttribute__ctor_m1696407647(tmp, NULL);
	}
}
static void Enumerable_t1126428382_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t1721836986 * tmp = (ExtensionAttribute_t1721836986 *)cache->attributes[0];
		ExtensionAttribute__ctor_m831032830(tmp, NULL);
	}
}
static void Enumerable_t1126428382_CustomAttributesCacheGenerator_Enumerable_Any_m1770862959(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t1721836986 * tmp = (ExtensionAttribute_t1721836986 *)cache->attributes[0];
		ExtensionAttribute__ctor_m831032830(tmp, NULL);
	}
}
static void Enumerable_t1126428382_CustomAttributesCacheGenerator_Enumerable_Where_m2049181036(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t1721836986 * tmp = (ExtensionAttribute_t1721836986 *)cache->attributes[0];
		ExtensionAttribute__ctor_m831032830(tmp, NULL);
	}
}
static void Enumerable_t1126428382_CustomAttributesCacheGenerator_Enumerable_CreateWhereIterator_m1295165955(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_t1447627238 * tmp = (DebuggerHiddenAttribute_t1447627238 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_m749702160(tmp, NULL);
	}
}
static void U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1910290221_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
}
static void U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1910290221_CustomAttributesCacheGenerator_U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m3788286129(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_t1447627238 * tmp = (DebuggerHiddenAttribute_t1447627238 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_m749702160(tmp, NULL);
	}
}
static void U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1910290221_CustomAttributesCacheGenerator_U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerator_get_Current_m1779517307(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_t1447627238 * tmp = (DebuggerHiddenAttribute_t1447627238 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_m749702160(tmp, NULL);
	}
}
static void U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1910290221_CustomAttributesCacheGenerator_U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerable_GetEnumerator_m2163469919(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_t1447627238 * tmp = (DebuggerHiddenAttribute_t1447627238 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_m749702160(tmp, NULL);
	}
}
static void U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1910290221_CustomAttributesCacheGenerator_U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m273512516(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_t1447627238 * tmp = (DebuggerHiddenAttribute_t1447627238 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_m749702160(tmp, NULL);
	}
}
static void U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1910290221_CustomAttributesCacheGenerator_U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m1120635176(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_t1447627238 * tmp = (DebuggerHiddenAttribute_t1447627238 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_m749702160(tmp, NULL);
	}
}
static void U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1910290221_CustomAttributesCacheGenerator_U3CCreateWhereIteratorU3Ec__Iterator1D_1_Reset_m2396585947(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_t1447627238 * tmp = (DebuggerHiddenAttribute_t1447627238 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_m749702160(tmp, NULL);
	}
}
static void U3CPrivateImplementationDetailsU3E_t3250056445_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
}
static void g_UnityEngine_CoreModule_Assembly_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[0];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.UmbraModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[1];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.VideoModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[2];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.WindModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[3];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine"), NULL);
	}
	{
		ExtensionAttribute_t1721836986 * tmp = (ExtensionAttribute_t1721836986 *)cache->attributes[4];
		ExtensionAttribute__ctor_m831032830(tmp, NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[5];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.TerrainPhysicsModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[6];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.TilemapModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[7];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.ScreenCaptureModule"), NULL);
	}
	{
		RuntimeCompatibilityAttribute_t38650954 * tmp = (RuntimeCompatibilityAttribute_t38650954 *)cache->attributes[8];
		RuntimeCompatibilityAttribute__ctor_m853651104(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m4154715430(tmp, true, NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[9];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.UnityWebRequestModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[10];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.IMGUIModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[11];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.GameCenterModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[12];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.TerrainModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[13];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.UIModule"), NULL);
	}
	{
		DebuggableAttribute_t3717120398 * tmp = (DebuggableAttribute_t3717120398 *)cache->attributes[14];
		DebuggableAttribute__ctor_m3553871532(tmp, 258LL, NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[15];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.SpatialTracking"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[16];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("GoogleAR.UnityNative"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[17];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("Unity.RuntimeTests.Framework.Tests"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[18];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("Unity.RuntimeTests.Framework"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[19];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("Unity.RuntimeTests"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[20];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.AccessibilityModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[21];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.TextRenderingModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[22];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.AnimationModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[23];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.AIModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[24];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.ClothModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[25];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.VehiclesModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[26];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.PhysicsModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[27];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.ParticleSystemModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[28];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.UnityWebRequestAudioModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[29];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.UIElementsModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[30];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.StyleSheetsModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[31];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.AudioModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[32];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.BaselibModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[33];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.PerformanceReportingModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[34];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.SpriteMaskModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[35];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.CloudWebServicesModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[36];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.CrashReportingModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[37];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.UnityConnectModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[38];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.WebModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[39];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.ARModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[40];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.SpatialTrackingModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[41];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.UnityAnalyticsModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[42];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.VRModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[43];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.GridModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[44];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.ClusterRendererModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[45];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.ClusterInputModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[46];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.UnityWebRequestWWWModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[47];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.UnityWebRequestTextureModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[48];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.ParticlesLegacyModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[49];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.Physics2DModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[50];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.FacebookModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[51];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.TimelineModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[52];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.ImageConversionModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[53];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.InputModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[54];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.DirectorModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[55];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.JSONSerializeModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[56];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.NScreenModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[57];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.UNETModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[58];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("Unity.IntegrationTests.Framework"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[59];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.Timeline"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[60];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.Purchasing"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[61];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.Advertisements"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[62];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.Analytics"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[63];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.Cloud.Service"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[64];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.Terrain"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[65];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.TerrainPhysics"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[66];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.Cloud"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[67];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.Networking"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[68];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.GoogleAudioSpatializer"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[69];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("Unity.IntegrationTests.UnityAnalytics"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[70];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("Unity.IntegrationTests"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[71];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("Unity.IntegrationTests.Timeline"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[72];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("Unity.DeploymentTests.Services"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[73];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.Physics"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[74];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("Unity.Automation"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[75];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.TestRunner"), NULL);
	}
}
static void Application_t2657524047_CustomAttributesCacheGenerator_lowMemory(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
	{
		DebuggerBrowsableAttribute_t1527328379 * tmp = (DebuggerBrowsableAttribute_t1527328379 *)cache->attributes[1];
		DebuggerBrowsableAttribute__ctor_m1784875164(tmp, 0LL, NULL);
	}
}
static void Application_t2657524047_CustomAttributesCacheGenerator_onBeforeRender(CustomAttributesCache* cache)
{
	{
		DebuggerBrowsableAttribute_t1527328379 * tmp = (DebuggerBrowsableAttribute_t1527328379 *)cache->attributes[0];
		DebuggerBrowsableAttribute__ctor_m1784875164(tmp, 0LL, NULL);
	}
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[1];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
}
static void Application_t2657524047_CustomAttributesCacheGenerator_Application_CallLowMemory_m1626726542(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void Application_t2657524047_CustomAttributesCacheGenerator_Application_CallLogCallback_m3808806632(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void Application_t2657524047_CustomAttributesCacheGenerator_Application_InvokeOnBeforeRender_m818804610(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void AssetBundleCreateRequest_t3985900355_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void AssetBundleRequest_t2873072919_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void AsyncOperation_t4233810744_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void AsyncOperation_t4233810744_CustomAttributesCacheGenerator_AsyncOperation_InternalDestroy_m1747224711(CustomAttributesCache* cache)
{
	{
		ThreadAndSerializationSafeAttribute_t566760631 * tmp = (ThreadAndSerializationSafeAttribute_t566760631 *)cache->attributes[0];
		ThreadAndSerializationSafeAttribute__ctor_m211248632(tmp, NULL);
	}
	{
		GeneratedByOldBindingsGeneratorAttribute_t861731860 * tmp = (GeneratedByOldBindingsGeneratorAttribute_t861731860 *)cache->attributes[1];
		GeneratedByOldBindingsGeneratorAttribute__ctor_m1819602998(tmp, NULL);
	}
}
static void AsyncOperation_t4233810744_CustomAttributesCacheGenerator_AsyncOperation_InvokeCompletionEvent_m737953261(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void WaitForSeconds_t727047372_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void WaitForFixedUpdate_t870903979_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void WaitForEndOfFrame_t337473950_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void Coroutine_t3619068392_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void Coroutine_t3619068392_CustomAttributesCacheGenerator_Coroutine_ReleaseCoroutine_m329501834(CustomAttributesCache* cache)
{
	{
		ThreadAndSerializationSafeAttribute_t566760631 * tmp = (ThreadAndSerializationSafeAttribute_t566760631 *)cache->attributes[0];
		ThreadAndSerializationSafeAttribute__ctor_m211248632(tmp, NULL);
	}
	{
		GeneratedByOldBindingsGeneratorAttribute_t861731860 * tmp = (GeneratedByOldBindingsGeneratorAttribute_t861731860 *)cache->attributes[1];
		GeneratedByOldBindingsGeneratorAttribute__ctor_m1819602998(tmp, NULL);
	}
}
static void ScriptableObject_t476655885_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void ScriptableObject_t476655885_CustomAttributesCacheGenerator_ScriptableObject_Internal_CreateScriptableObject_m3652231296(CustomAttributesCache* cache)
{
	{
		GeneratedByOldBindingsGeneratorAttribute_t861731860 * tmp = (GeneratedByOldBindingsGeneratorAttribute_t861731860 *)cache->attributes[0];
		GeneratedByOldBindingsGeneratorAttribute__ctor_m1819602998(tmp, NULL);
	}
	{
		ThreadAndSerializationSafeAttribute_t566760631 * tmp = (ThreadAndSerializationSafeAttribute_t566760631 *)cache->attributes[1];
		ThreadAndSerializationSafeAttribute__ctor_m211248632(tmp, NULL);
	}
}
static void ScriptableObject_t476655885_CustomAttributesCacheGenerator_ScriptableObject_Internal_CreateScriptableObject_m3652231296____self0(CustomAttributesCache* cache)
{
	{
		WritableAttribute_t2474002418 * tmp = (WritableAttribute_t2474002418 *)cache->attributes[0];
		WritableAttribute__ctor_m474281766(tmp, NULL);
	}
}
static void ScriptableObject_t476655885_CustomAttributesCacheGenerator_ScriptableObject_CreateInstanceFromType_m1134094777(CustomAttributesCache* cache)
{
	{
		GeneratedByOldBindingsGeneratorAttribute_t861731860 * tmp = (GeneratedByOldBindingsGeneratorAttribute_t861731860 *)cache->attributes[0];
		GeneratedByOldBindingsGeneratorAttribute__ctor_m1819602998(tmp, NULL);
	}
}
static void FailedToLoadScriptObject_t2431108707_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void Behaviour_t4110946901_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		UsedByNativeCodeAttribute_t2783261251 * tmp = (UsedByNativeCodeAttribute_t2783261251 *)cache->attributes[0];
		UsedByNativeCodeAttribute__ctor_m4001769068(tmp, NULL);
	}
}
static void Behaviour_t4110946901_CustomAttributesCacheGenerator_Behaviour_get_enabled_m1304335371(CustomAttributesCache* cache)
{
	{
		GeneratedByOldBindingsGeneratorAttribute_t861731860 * tmp = (GeneratedByOldBindingsGeneratorAttribute_t861731860 *)cache->attributes[0];
		GeneratedByOldBindingsGeneratorAttribute__ctor_m1819602998(tmp, NULL);
	}
}
static void Camera_t101201881_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Camera_t101201881_CustomAttributesCacheGenerator_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		UsedByNativeCodeAttribute_t2783261251 * tmp = (UsedByNativeCodeAttribute_t2783261251 *)cache->attributes[0];
		UsedByNativeCodeAttribute__ctor_m4001769068(tmp, NULL);
	}
	{
		RequireComponent_t597737523 * tmp = (RequireComponent_t597737523 *)cache->attributes[1];
		RequireComponent__ctor_m3242490219(tmp, il2cpp_codegen_type_get_object(Transform_t94239816_0_0_0_var), NULL);
	}
}
static void Camera_t101201881_CustomAttributesCacheGenerator_Camera_get_nearClipPlane_m167155657(CustomAttributesCache* cache)
{
	{
		GeneratedByOldBindingsGeneratorAttribute_t861731860 * tmp = (GeneratedByOldBindingsGeneratorAttribute_t861731860 *)cache->attributes[0];
		GeneratedByOldBindingsGeneratorAttribute__ctor_m1819602998(tmp, NULL);
	}
}
static void Camera_t101201881_CustomAttributesCacheGenerator_Camera_get_farClipPlane_m1366251422(CustomAttributesCache* cache)
{
	{
		GeneratedByOldBindingsGeneratorAttribute_t861731860 * tmp = (GeneratedByOldBindingsGeneratorAttribute_t861731860 *)cache->attributes[0];
		GeneratedByOldBindingsGeneratorAttribute__ctor_m1819602998(tmp, NULL);
	}
}
static void Camera_t101201881_CustomAttributesCacheGenerator_Camera_get_cullingMask_m2771596515(CustomAttributesCache* cache)
{
	{
		GeneratedByOldBindingsGeneratorAttribute_t861731860 * tmp = (GeneratedByOldBindingsGeneratorAttribute_t861731860 *)cache->attributes[0];
		GeneratedByOldBindingsGeneratorAttribute__ctor_m1819602998(tmp, NULL);
	}
}
static void Camera_t101201881_CustomAttributesCacheGenerator_Camera_get_eventMask_m3958975071(CustomAttributesCache* cache)
{
	{
		GeneratedByOldBindingsGeneratorAttribute_t861731860 * tmp = (GeneratedByOldBindingsGeneratorAttribute_t861731860 *)cache->attributes[0];
		GeneratedByOldBindingsGeneratorAttribute__ctor_m1819602998(tmp, NULL);
	}
}
static void Camera_t101201881_CustomAttributesCacheGenerator_Camera_INTERNAL_get_pixelRect_m3102036321(CustomAttributesCache* cache)
{
	{
		GeneratedByOldBindingsGeneratorAttribute_t861731860 * tmp = (GeneratedByOldBindingsGeneratorAttribute_t861731860 *)cache->attributes[0];
		GeneratedByOldBindingsGeneratorAttribute__ctor_m1819602998(tmp, NULL);
	}
}
static void Camera_t101201881_CustomAttributesCacheGenerator_Camera_get_targetTexture_m903246907(CustomAttributesCache* cache)
{
	{
		GeneratedByOldBindingsGeneratorAttribute_t861731860 * tmp = (GeneratedByOldBindingsGeneratorAttribute_t861731860 *)cache->attributes[0];
		GeneratedByOldBindingsGeneratorAttribute__ctor_m1819602998(tmp, NULL);
	}
}
static void Camera_t101201881_CustomAttributesCacheGenerator_Camera_get_clearFlags_m3577200564(CustomAttributesCache* cache)
{
	{
		GeneratedByOldBindingsGeneratorAttribute_t861731860 * tmp = (GeneratedByOldBindingsGeneratorAttribute_t861731860 *)cache->attributes[0];
		GeneratedByOldBindingsGeneratorAttribute__ctor_m1819602998(tmp, NULL);
	}
}
static void Camera_t101201881_CustomAttributesCacheGenerator_Camera_INTERNAL_CALL_ScreenPointToRay_m244366202(CustomAttributesCache* cache)
{
	{
		GeneratedByOldBindingsGeneratorAttribute_t861731860 * tmp = (GeneratedByOldBindingsGeneratorAttribute_t861731860 *)cache->attributes[0];
		GeneratedByOldBindingsGeneratorAttribute__ctor_m1819602998(tmp, NULL);
	}
}
static void Camera_t101201881_CustomAttributesCacheGenerator_Camera_get_allCamerasCount_m3527489092(CustomAttributesCache* cache)
{
	{
		GeneratedByOldBindingsGeneratorAttribute_t861731860 * tmp = (GeneratedByOldBindingsGeneratorAttribute_t861731860 *)cache->attributes[0];
		GeneratedByOldBindingsGeneratorAttribute__ctor_m1819602998(tmp, NULL);
	}
}
static void Camera_t101201881_CustomAttributesCacheGenerator_Camera_GetAllCameras_m557431570(CustomAttributesCache* cache)
{
	{
		GeneratedByOldBindingsGeneratorAttribute_t861731860 * tmp = (GeneratedByOldBindingsGeneratorAttribute_t861731860 *)cache->attributes[0];
		GeneratedByOldBindingsGeneratorAttribute__ctor_m1819602998(tmp, NULL);
	}
}
static void Camera_t101201881_CustomAttributesCacheGenerator_Camera_FireOnPreCull_m2186829015(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void Camera_t101201881_CustomAttributesCacheGenerator_Camera_FireOnPreRender_m1563217163(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void Camera_t101201881_CustomAttributesCacheGenerator_Camera_FireOnPostRender_m4290449375(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void Camera_t101201881_CustomAttributesCacheGenerator_Camera_INTERNAL_CALL_RaycastTry_m2903709223(CustomAttributesCache* cache)
{
	{
		GeneratedByOldBindingsGeneratorAttribute_t861731860 * tmp = (GeneratedByOldBindingsGeneratorAttribute_t861731860 *)cache->attributes[0];
		GeneratedByOldBindingsGeneratorAttribute__ctor_m1819602998(tmp, NULL);
	}
}
static void Camera_t101201881_CustomAttributesCacheGenerator_Camera_INTERNAL_CALL_RaycastTry2D_m700194433(CustomAttributesCache* cache)
{
	{
		GeneratedByOldBindingsGeneratorAttribute_t861731860 * tmp = (GeneratedByOldBindingsGeneratorAttribute_t861731860 *)cache->attributes[0];
		GeneratedByOldBindingsGeneratorAttribute__ctor_m1819602998(tmp, NULL);
	}
}
static void Component_t2160255836_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void Component_t2160255836_CustomAttributesCacheGenerator_Component_get_gameObject_m2710707649(CustomAttributesCache* cache)
{
	{
		GeneratedByOldBindingsGeneratorAttribute_t861731860 * tmp = (GeneratedByOldBindingsGeneratorAttribute_t861731860 *)cache->attributes[0];
		GeneratedByOldBindingsGeneratorAttribute__ctor_m1819602998(tmp, NULL);
	}
}
static void Component_t2160255836_CustomAttributesCacheGenerator_Component_GetComponentFastPath_m3479205869(CustomAttributesCache* cache)
{
	{
		GeneratedByOldBindingsGeneratorAttribute_t861731860 * tmp = (GeneratedByOldBindingsGeneratorAttribute_t861731860 *)cache->attributes[0];
		GeneratedByOldBindingsGeneratorAttribute__ctor_m1819602998(tmp, NULL);
	}
}
static void Component_t2160255836_CustomAttributesCacheGenerator_Component_GetComponent_m685032955(CustomAttributesCache* cache)
{
	{
		SecuritySafeCriticalAttribute_t824959508 * tmp = (SecuritySafeCriticalAttribute_t824959508 *)cache->attributes[0];
		SecuritySafeCriticalAttribute__ctor_m1135903908(tmp, NULL);
	}
}
static void UnhandledExceptionHandler_t2845673226_CustomAttributesCacheGenerator_U3CU3Ef__mgU24cache0(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
}
static void UnhandledExceptionHandler_t2845673226_CustomAttributesCacheGenerator_UnhandledExceptionHandler_RegisterUECatcher_m663990726(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void UnhandledExceptionHandler_t2845673226_CustomAttributesCacheGenerator_UnhandledExceptionHandler_NativeUnhandledExceptionHandler_m4249568766(CustomAttributesCache* cache)
{
	{
		GeneratedByOldBindingsGeneratorAttribute_t861731860 * tmp = (GeneratedByOldBindingsGeneratorAttribute_t861731860 *)cache->attributes[0];
		GeneratedByOldBindingsGeneratorAttribute__ctor_m1819602998(tmp, NULL);
	}
	{
		ThreadAndSerializationSafeAttribute_t566760631 * tmp = (ThreadAndSerializationSafeAttribute_t566760631 *)cache->attributes[1];
		ThreadAndSerializationSafeAttribute__ctor_m211248632(tmp, NULL);
	}
}
static void CullingGroup_t1255328198_CustomAttributesCacheGenerator_CullingGroup_Dispose_m1436928443(CustomAttributesCache* cache)
{
	{
		GeneratedByOldBindingsGeneratorAttribute_t861731860 * tmp = (GeneratedByOldBindingsGeneratorAttribute_t861731860 *)cache->attributes[0];
		GeneratedByOldBindingsGeneratorAttribute__ctor_m1819602998(tmp, NULL);
	}
}
static void CullingGroup_t1255328198_CustomAttributesCacheGenerator_CullingGroup_SendEvents_m3781512400(CustomAttributesCache* cache)
{
	{
		SecuritySafeCriticalAttribute_t824959508 * tmp = (SecuritySafeCriticalAttribute_t824959508 *)cache->attributes[0];
		SecuritySafeCriticalAttribute__ctor_m1135903908(tmp, NULL);
	}
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[1];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void CullingGroup_t1255328198_CustomAttributesCacheGenerator_CullingGroup_FinalizerFailure_m3512716862(CustomAttributesCache* cache)
{
	{
		GeneratedByOldBindingsGeneratorAttribute_t861731860 * tmp = (GeneratedByOldBindingsGeneratorAttribute_t861731860 *)cache->attributes[0];
		GeneratedByOldBindingsGeneratorAttribute__ctor_m1819602998(tmp, NULL);
	}
	{
		ThreadAndSerializationSafeAttribute_t566760631 * tmp = (ThreadAndSerializationSafeAttribute_t566760631 *)cache->attributes[1];
		ThreadAndSerializationSafeAttribute__ctor_m211248632(tmp, NULL);
	}
}
static void DebugLogHandler_t1401667417_CustomAttributesCacheGenerator_DebugLogHandler_Internal_Log_m1682417589(CustomAttributesCache* cache)
{
	{
		ThreadAndSerializationSafeAttribute_t566760631 * tmp = (ThreadAndSerializationSafeAttribute_t566760631 *)cache->attributes[0];
		ThreadAndSerializationSafeAttribute__ctor_m211248632(tmp, NULL);
	}
	{
		GeneratedByOldBindingsGeneratorAttribute_t861731860 * tmp = (GeneratedByOldBindingsGeneratorAttribute_t861731860 *)cache->attributes[1];
		GeneratedByOldBindingsGeneratorAttribute__ctor_m1819602998(tmp, NULL);
	}
}
static void DebugLogHandler_t1401667417_CustomAttributesCacheGenerator_DebugLogHandler_Internal_Log_m1682417589____obj2(CustomAttributesCache* cache)
{
	{
		WritableAttribute_t2474002418 * tmp = (WritableAttribute_t2474002418 *)cache->attributes[0];
		WritableAttribute__ctor_m474281766(tmp, NULL);
	}
}
static void DebugLogHandler_t1401667417_CustomAttributesCacheGenerator_DebugLogHandler_Internal_LogException_m3264502143(CustomAttributesCache* cache)
{
	{
		GeneratedByOldBindingsGeneratorAttribute_t861731860 * tmp = (GeneratedByOldBindingsGeneratorAttribute_t861731860 *)cache->attributes[0];
		GeneratedByOldBindingsGeneratorAttribute__ctor_m1819602998(tmp, NULL);
	}
	{
		ThreadAndSerializationSafeAttribute_t566760631 * tmp = (ThreadAndSerializationSafeAttribute_t566760631 *)cache->attributes[1];
		ThreadAndSerializationSafeAttribute__ctor_m211248632(tmp, NULL);
	}
}
static void DebugLogHandler_t1401667417_CustomAttributesCacheGenerator_DebugLogHandler_Internal_LogException_m3264502143____obj1(CustomAttributesCache* cache)
{
	{
		WritableAttribute_t2474002418 * tmp = (WritableAttribute_t2474002418 *)cache->attributes[0];
		WritableAttribute__ctor_m474281766(tmp, NULL);
	}
}
static void DebugLogHandler_t1401667417_CustomAttributesCacheGenerator_DebugLogHandler_LogFormat_m3907003566____args3(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t3330428127 * tmp = (ParamArrayAttribute_t3330428127 *)cache->attributes[0];
		ParamArrayAttribute__ctor_m1696407647(tmp, NULL);
	}
}
static void Display_t4002355288_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		UsedByNativeCodeAttribute_t2783261251 * tmp = (UsedByNativeCodeAttribute_t2783261251 *)cache->attributes[0];
		UsedByNativeCodeAttribute__ctor_m4001769068(tmp, NULL);
	}
}
static void Display_t4002355288_CustomAttributesCacheGenerator_onDisplaysUpdated(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
	{
		DebuggerBrowsableAttribute_t1527328379 * tmp = (DebuggerBrowsableAttribute_t1527328379 *)cache->attributes[1];
		DebuggerBrowsableAttribute__ctor_m1784875164(tmp, 0LL, NULL);
	}
}
static void Display_t4002355288_CustomAttributesCacheGenerator_Display_RecreateDisplayList_m3512407007(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void Display_t4002355288_CustomAttributesCacheGenerator_Display_FireDisplaysUpdated_m3194723814(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void GameObject_t3732643618_CustomAttributesCacheGenerator_GameObject_SendMessage_m239132133(CustomAttributesCache* cache)
{
	{
		GeneratedByOldBindingsGeneratorAttribute_t861731860 * tmp = (GeneratedByOldBindingsGeneratorAttribute_t861731860 *)cache->attributes[0];
		GeneratedByOldBindingsGeneratorAttribute__ctor_m1819602998(tmp, NULL);
	}
}
static void GameObject_t3732643618_CustomAttributesCacheGenerator_GameObject_SendMessage_m239132133____value1(CustomAttributesCache* cache)
{
	{
		DefaultValueAttribute_t2693326634 * tmp = (DefaultValueAttribute_t2693326634 *)cache->attributes[0];
		DefaultValueAttribute__ctor_m2061771615(tmp, il2cpp_codegen_string_new_wrapper("null"), NULL);
	}
}
static void GameObject_t3732643618_CustomAttributesCacheGenerator_GameObject_SendMessage_m239132133____options2(CustomAttributesCache* cache)
{
	{
		DefaultValueAttribute_t2693326634 * tmp = (DefaultValueAttribute_t2693326634 *)cache->attributes[0];
		DefaultValueAttribute__ctor_m2061771615(tmp, il2cpp_codegen_string_new_wrapper("SendMessageOptions.RequireReceiver"), NULL);
	}
}
static void GameObject_t3732643618_CustomAttributesCacheGenerator_GameObject_Internal_CreateGameObject_m1823697062(CustomAttributesCache* cache)
{
	{
		GeneratedByOldBindingsGeneratorAttribute_t861731860 * tmp = (GeneratedByOldBindingsGeneratorAttribute_t861731860 *)cache->attributes[0];
		GeneratedByOldBindingsGeneratorAttribute__ctor_m1819602998(tmp, NULL);
	}
}
static void GameObject_t3732643618_CustomAttributesCacheGenerator_GameObject_Internal_CreateGameObject_m1823697062____mono0(CustomAttributesCache* cache)
{
	{
		WritableAttribute_t2474002418 * tmp = (WritableAttribute_t2474002418 *)cache->attributes[0];
		WritableAttribute__ctor_m474281766(tmp, NULL);
	}
}
static void Gradient_t130030130_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void Gradient_t130030130_CustomAttributesCacheGenerator_Gradient__ctor_m2009183950(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void Gradient_t130030130_CustomAttributesCacheGenerator_Gradient_Init_m1188969969(CustomAttributesCache* cache)
{
	{
		GeneratedByOldBindingsGeneratorAttribute_t861731860 * tmp = (GeneratedByOldBindingsGeneratorAttribute_t861731860 *)cache->attributes[0];
		GeneratedByOldBindingsGeneratorAttribute__ctor_m1819602998(tmp, NULL);
	}
	{
		ThreadAndSerializationSafeAttribute_t566760631 * tmp = (ThreadAndSerializationSafeAttribute_t566760631 *)cache->attributes[1];
		ThreadAndSerializationSafeAttribute__ctor_m211248632(tmp, NULL);
	}
}
static void Gradient_t130030130_CustomAttributesCacheGenerator_Gradient_Cleanup_m2195794132(CustomAttributesCache* cache)
{
	{
		GeneratedByOldBindingsGeneratorAttribute_t861731860 * tmp = (GeneratedByOldBindingsGeneratorAttribute_t861731860 *)cache->attributes[0];
		GeneratedByOldBindingsGeneratorAttribute__ctor_m1819602998(tmp, NULL);
	}
	{
		ThreadAndSerializationSafeAttribute_t566760631 * tmp = (ThreadAndSerializationSafeAttribute_t566760631 *)cache->attributes[1];
		ThreadAndSerializationSafeAttribute__ctor_m211248632(tmp, NULL);
	}
}
static void GUIElement_t1292848738_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GUIElement_t1292848738_CustomAttributesCacheGenerator_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_t597737523 * tmp = (RequireComponent_t597737523 *)cache->attributes[0];
		RequireComponent__ctor_m3242490219(tmp, il2cpp_codegen_type_get_object(Transform_t94239816_0_0_0_var), NULL);
	}
}
static void GUILayer_t1929752731_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GUILayer_t1929752731_CustomAttributesCacheGenerator_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_t597737523 * tmp = (RequireComponent_t597737523 *)cache->attributes[0];
		RequireComponent__ctor_m3242490219(tmp, il2cpp_codegen_type_get_object(Camera_t101201881_0_0_0_var), NULL);
	}
	{
		ObsoleteAttribute_t3443961000 * tmp = (ObsoleteAttribute_t3443961000 *)cache->attributes[1];
		ObsoleteAttribute__ctor_m3274828507(tmp, il2cpp_codegen_string_new_wrapper("This component is part of the legacy UI system and will be removed in a future release."), NULL);
	}
}
static void GUILayer_t1929752731_CustomAttributesCacheGenerator_GUILayer_INTERNAL_CALL_HitTest_m422396558(CustomAttributesCache* cache)
{
	{
		GeneratedByOldBindingsGeneratorAttribute_t861731860 * tmp = (GeneratedByOldBindingsGeneratorAttribute_t861731860 *)cache->attributes[0];
		GeneratedByOldBindingsGeneratorAttribute__ctor_m1819602998(tmp, NULL);
	}
}
static void Input_t2650329352_CustomAttributesCacheGenerator_Input_GetMouseButton_m294091231(CustomAttributesCache* cache)
{
	{
		GeneratedByOldBindingsGeneratorAttribute_t861731860 * tmp = (GeneratedByOldBindingsGeneratorAttribute_t861731860 *)cache->attributes[0];
		GeneratedByOldBindingsGeneratorAttribute__ctor_m1819602998(tmp, NULL);
	}
}
static void Input_t2650329352_CustomAttributesCacheGenerator_Input_GetMouseButtonDown_m3867833682(CustomAttributesCache* cache)
{
	{
		GeneratedByOldBindingsGeneratorAttribute_t861731860 * tmp = (GeneratedByOldBindingsGeneratorAttribute_t861731860 *)cache->attributes[0];
		GeneratedByOldBindingsGeneratorAttribute__ctor_m1819602998(tmp, NULL);
	}
}
static void Input_t2650329352_CustomAttributesCacheGenerator_Input_INTERNAL_get_mousePosition_m3613716531(CustomAttributesCache* cache)
{
	{
		GeneratedByOldBindingsGeneratorAttribute_t861731860 * tmp = (GeneratedByOldBindingsGeneratorAttribute_t861731860 *)cache->attributes[0];
		GeneratedByOldBindingsGeneratorAttribute__ctor_m1819602998(tmp, NULL);
	}
}
static void Vector3_t3239555143_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		UsedByNativeCodeAttribute_t2783261251 * tmp = (UsedByNativeCodeAttribute_t2783261251 *)cache->attributes[0];
		UsedByNativeCodeAttribute__ctor_m4001769068(tmp, NULL);
	}
	{
		DefaultMemberAttribute_t2956431904 * tmp = (DefaultMemberAttribute_t2956431904 *)cache->attributes[1];
		DefaultMemberAttribute__ctor_m3129895323(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
	}
}
static void Matrix4x4_t3534180298_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		UsedByNativeCodeAttribute_t2783261251 * tmp = (UsedByNativeCodeAttribute_t2783261251 *)cache->attributes[0];
		UsedByNativeCodeAttribute__ctor_m4001769068(tmp, NULL);
	}
	{
		DefaultMemberAttribute_t2956431904 * tmp = (DefaultMemberAttribute_t2956431904 *)cache->attributes[1];
		DefaultMemberAttribute__ctor_m3129895323(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
	}
}
static void Keyframe_t3396755990_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void AnimationCurve_t1726276845_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
	{
		DefaultMemberAttribute_t2956431904 * tmp = (DefaultMemberAttribute_t2956431904 *)cache->attributes[1];
		DefaultMemberAttribute__ctor_m3129895323(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
	}
}
static void AnimationCurve_t1726276845_CustomAttributesCacheGenerator_AnimationCurve__ctor_m3076316537____keys0(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t3330428127 * tmp = (ParamArrayAttribute_t3330428127 *)cache->attributes[0];
		ParamArrayAttribute__ctor_m1696407647(tmp, NULL);
	}
}
static void AnimationCurve_t1726276845_CustomAttributesCacheGenerator_AnimationCurve__ctor_m566668622(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void AnimationCurve_t1726276845_CustomAttributesCacheGenerator_AnimationCurve_Cleanup_m243787505(CustomAttributesCache* cache)
{
	{
		ThreadAndSerializationSafeAttribute_t566760631 * tmp = (ThreadAndSerializationSafeAttribute_t566760631 *)cache->attributes[0];
		ThreadAndSerializationSafeAttribute__ctor_m211248632(tmp, NULL);
	}
	{
		GeneratedByOldBindingsGeneratorAttribute_t861731860 * tmp = (GeneratedByOldBindingsGeneratorAttribute_t861731860 *)cache->attributes[1];
		GeneratedByOldBindingsGeneratorAttribute__ctor_m1819602998(tmp, NULL);
	}
}
static void AnimationCurve_t1726276845_CustomAttributesCacheGenerator_AnimationCurve_Init_m1956642396(CustomAttributesCache* cache)
{
	{
		GeneratedByOldBindingsGeneratorAttribute_t861731860 * tmp = (GeneratedByOldBindingsGeneratorAttribute_t861731860 *)cache->attributes[0];
		GeneratedByOldBindingsGeneratorAttribute__ctor_m1819602998(tmp, NULL);
	}
	{
		ThreadAndSerializationSafeAttribute_t566760631 * tmp = (ThreadAndSerializationSafeAttribute_t566760631 *)cache->attributes[1];
		ThreadAndSerializationSafeAttribute__ctor_m211248632(tmp, NULL);
	}
}
static void MonoBehaviour_t2904604993_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void ResourceRequest_t3543572403_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void Texture_t238546388_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		UsedByNativeCodeAttribute_t2783261251 * tmp = (UsedByNativeCodeAttribute_t2783261251 *)cache->attributes[0];
		UsedByNativeCodeAttribute__ctor_m4001769068(tmp, NULL);
	}
}
static void Texture2D_t359445654_CustomAttributesCacheGenerator_Texture2D_Internal_Create_m1993173660(CustomAttributesCache* cache)
{
	{
		GeneratedByOldBindingsGeneratorAttribute_t861731860 * tmp = (GeneratedByOldBindingsGeneratorAttribute_t861731860 *)cache->attributes[0];
		GeneratedByOldBindingsGeneratorAttribute__ctor_m1819602998(tmp, NULL);
	}
}
static void Texture2D_t359445654_CustomAttributesCacheGenerator_Texture2D_Internal_Create_m1993173660____mono0(CustomAttributesCache* cache)
{
	{
		WritableAttribute_t2474002418 * tmp = (WritableAttribute_t2474002418 *)cache->attributes[0];
		WritableAttribute__ctor_m474281766(tmp, NULL);
	}
}
static void RenderTexture_t2667189599_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		UsedByNativeCodeAttribute_t2783261251 * tmp = (UsedByNativeCodeAttribute_t2783261251 *)cache->attributes[0];
		UsedByNativeCodeAttribute__ctor_m4001769068(tmp, NULL);
	}
}
static void HideFlags_t1155951039_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		FlagsAttribute_t388727259 * tmp = (FlagsAttribute_t388727259 *)cache->attributes[0];
		FlagsAttribute__ctor_m3834565173(tmp, NULL);
	}
}
static void Object_t2461733472_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void Object_t2461733472_CustomAttributesCacheGenerator_Object_set_hideFlags_m3010783544(CustomAttributesCache* cache)
{
	{
		GeneratedByOldBindingsGeneratorAttribute_t861731860 * tmp = (GeneratedByOldBindingsGeneratorAttribute_t861731860 *)cache->attributes[0];
		GeneratedByOldBindingsGeneratorAttribute__ctor_m1819602998(tmp, NULL);
	}
}
static void Object_t2461733472_CustomAttributesCacheGenerator_Object_ToString_m842583452(CustomAttributesCache* cache)
{
	{
		GeneratedByOldBindingsGeneratorAttribute_t861731860 * tmp = (GeneratedByOldBindingsGeneratorAttribute_t861731860 *)cache->attributes[0];
		GeneratedByOldBindingsGeneratorAttribute__ctor_m1819602998(tmp, NULL);
	}
}
static void UnityLogWriter_t3231359731_CustomAttributesCacheGenerator_UnityLogWriter_WriteStringToUnityLog_m237899344(CustomAttributesCache* cache)
{
	{
		ThreadAndSerializationSafeAttribute_t566760631 * tmp = (ThreadAndSerializationSafeAttribute_t566760631 *)cache->attributes[0];
		ThreadAndSerializationSafeAttribute__ctor_m211248632(tmp, NULL);
	}
	{
		GeneratedByOldBindingsGeneratorAttribute_t861731860 * tmp = (GeneratedByOldBindingsGeneratorAttribute_t861731860 *)cache->attributes[1];
		GeneratedByOldBindingsGeneratorAttribute__ctor_m1819602998(tmp, NULL);
	}
}
static void YieldInstruction_t3999321895_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		UsedByNativeCodeAttribute_t2783261251 * tmp = (UsedByNativeCodeAttribute_t2783261251 *)cache->attributes[0];
		UsedByNativeCodeAttribute__ctor_m4001769068(tmp, NULL);
	}
}
static void PlayableHandle_t3938927722_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		UsedByNativeCodeAttribute_t2783261251 * tmp = (UsedByNativeCodeAttribute_t2783261251 *)cache->attributes[0];
		UsedByNativeCodeAttribute__ctor_m4001769068(tmp, NULL);
	}
}
static void Playable_t1760619938_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void PlayableGraph_t1897216728_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		UsedByNativeCodeAttribute_t2783261251 * tmp = (UsedByNativeCodeAttribute_t2783261251 *)cache->attributes[0];
		UsedByNativeCodeAttribute__ctor_m4001769068(tmp, NULL);
	}
}
static void PlayableOutputHandle_t2719521870_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		UsedByNativeCodeAttribute_t2783261251 * tmp = (UsedByNativeCodeAttribute_t2783261251 *)cache->attributes[0];
		UsedByNativeCodeAttribute__ctor_m4001769068(tmp, NULL);
	}
}
static void PlayableOutput_t3840687608_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void LocalNotification_t2133447199_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void LocalNotification_t2133447199_CustomAttributesCacheGenerator_LocalNotification_Destroy_m1982161855(CustomAttributesCache* cache)
{
	{
		GeneratedByOldBindingsGeneratorAttribute_t861731860 * tmp = (GeneratedByOldBindingsGeneratorAttribute_t861731860 *)cache->attributes[0];
		GeneratedByOldBindingsGeneratorAttribute__ctor_m1819602998(tmp, NULL);
	}
	{
		ThreadAndSerializationSafeAttribute_t566760631 * tmp = (ThreadAndSerializationSafeAttribute_t566760631 *)cache->attributes[1];
		ThreadAndSerializationSafeAttribute__ctor_m211248632(tmp, NULL);
	}
}
static void RemoteNotification_t414806740_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void RemoteNotification_t414806740_CustomAttributesCacheGenerator_RemoteNotification_Destroy_m4186758914(CustomAttributesCache* cache)
{
	{
		GeneratedByOldBindingsGeneratorAttribute_t861731860 * tmp = (GeneratedByOldBindingsGeneratorAttribute_t861731860 *)cache->attributes[0];
		GeneratedByOldBindingsGeneratorAttribute__ctor_m1819602998(tmp, NULL);
	}
	{
		ThreadAndSerializationSafeAttribute_t566760631 * tmp = (ThreadAndSerializationSafeAttribute_t566760631 *)cache->attributes[1];
		ThreadAndSerializationSafeAttribute__ctor_m211248632(tmp, NULL);
	}
}
static void SceneManager_t2258132803_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void SceneManager_t2258132803_CustomAttributesCacheGenerator_sceneLoaded(CustomAttributesCache* cache)
{
	{
		DebuggerBrowsableAttribute_t1527328379 * tmp = (DebuggerBrowsableAttribute_t1527328379 *)cache->attributes[0];
		DebuggerBrowsableAttribute__ctor_m1784875164(tmp, 0LL, NULL);
	}
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[1];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
}
static void SceneManager_t2258132803_CustomAttributesCacheGenerator_sceneUnloaded(CustomAttributesCache* cache)
{
	{
		DebuggerBrowsableAttribute_t1527328379 * tmp = (DebuggerBrowsableAttribute_t1527328379 *)cache->attributes[0];
		DebuggerBrowsableAttribute__ctor_m1784875164(tmp, 0LL, NULL);
	}
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[1];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
}
static void SceneManager_t2258132803_CustomAttributesCacheGenerator_activeSceneChanged(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
	{
		DebuggerBrowsableAttribute_t1527328379 * tmp = (DebuggerBrowsableAttribute_t1527328379 *)cache->attributes[1];
		DebuggerBrowsableAttribute__ctor_m1784875164(tmp, 0LL, NULL);
	}
}
static void SceneManager_t2258132803_CustomAttributesCacheGenerator_SceneManager_Internal_SceneLoaded_m2309132009(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void SceneManager_t2258132803_CustomAttributesCacheGenerator_SceneManager_Internal_SceneUnloaded_m2410300733(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void SceneManager_t2258132803_CustomAttributesCacheGenerator_SceneManager_Internal_ActiveSceneChanged_m667757325(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void Transform_t94239816_CustomAttributesCacheGenerator_Transform_get_childCount_m849482613(CustomAttributesCache* cache)
{
	{
		GeneratedByOldBindingsGeneratorAttribute_t861731860 * tmp = (GeneratedByOldBindingsGeneratorAttribute_t861731860 *)cache->attributes[0];
		GeneratedByOldBindingsGeneratorAttribute__ctor_m1819602998(tmp, NULL);
	}
}
static void Transform_t94239816_CustomAttributesCacheGenerator_Transform_GetChild_m4117681253(CustomAttributesCache* cache)
{
	{
		GeneratedByOldBindingsGeneratorAttribute_t861731860 * tmp = (GeneratedByOldBindingsGeneratorAttribute_t861731860 *)cache->attributes[0];
		GeneratedByOldBindingsGeneratorAttribute__ctor_m1819602998(tmp, NULL);
	}
}
static void RectTransform_t3950565364_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		NativeClassAttribute_t3085471410 * tmp = (NativeClassAttribute_t3085471410 *)cache->attributes[0];
		NativeClassAttribute__ctor_m714604756(tmp, il2cpp_codegen_string_new_wrapper("UI::RectTransform"), NULL);
	}
}
static void RectTransform_t3950565364_CustomAttributesCacheGenerator_reapplyDrivenProperties(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
	{
		DebuggerBrowsableAttribute_t1527328379 * tmp = (DebuggerBrowsableAttribute_t1527328379 *)cache->attributes[1];
		DebuggerBrowsableAttribute__ctor_m1784875164(tmp, 0LL, NULL);
	}
}
static void RectTransform_t3950565364_CustomAttributesCacheGenerator_RectTransform_SendReapplyDrivenProperties_m2257690066(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void SpriteAtlasManager_t1881977649_CustomAttributesCacheGenerator_atlasRequested(CustomAttributesCache* cache)
{
	{
		DebuggerBrowsableAttribute_t1527328379 * tmp = (DebuggerBrowsableAttribute_t1527328379 *)cache->attributes[0];
		DebuggerBrowsableAttribute__ctor_m1784875164(tmp, 0LL, NULL);
	}
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[1];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
}
static void SpriteAtlasManager_t1881977649_CustomAttributesCacheGenerator_U3CU3Ef__mgU24cache0(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
}
static void SpriteAtlasManager_t1881977649_CustomAttributesCacheGenerator_SpriteAtlasManager_RequestAtlas_m2195264182(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void SpriteAtlasManager_t1881977649_CustomAttributesCacheGenerator_SpriteAtlasManager_Register_m1381418004(CustomAttributesCache* cache)
{
	{
		GeneratedByOldBindingsGeneratorAttribute_t861731860 * tmp = (GeneratedByOldBindingsGeneratorAttribute_t861731860 *)cache->attributes[0];
		GeneratedByOldBindingsGeneratorAttribute__ctor_m1819602998(tmp, NULL);
	}
}
static void AttributeHelperEngine_t3367628457_CustomAttributesCacheGenerator_AttributeHelperEngine_GetParentTypeDisallowingMultipleInclusion_m1021838127(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void AttributeHelperEngine_t3367628457_CustomAttributesCacheGenerator_AttributeHelperEngine_GetRequiredComponents_m2091293497(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void AttributeHelperEngine_t3367628457_CustomAttributesCacheGenerator_AttributeHelperEngine_CheckIsEditorScript_m2212222995(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void AttributeHelperEngine_t3367628457_CustomAttributesCacheGenerator_AttributeHelperEngine_GetDefaultExecutionOrderFor_m1362990272(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void DisallowMultipleComponent_t2957315911_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_t2370971113 * tmp = (AttributeUsageAttribute_t2370971113 *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m1170715599(tmp, 4LL, NULL);
		AttributeUsageAttribute_set_Inherited_m2205070461(tmp, false, NULL);
	}
}
static void RequireComponent_t597737523_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_t2370971113 * tmp = (AttributeUsageAttribute_t2370971113 *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m1170715599(tmp, 4LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m1313295265(tmp, true, NULL);
	}
}
static void ContextMenu_t3503312229_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_t2370971113 * tmp = (AttributeUsageAttribute_t2370971113 *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m1170715599(tmp, 64LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m1313295265(tmp, true, NULL);
	}
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[1];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void DefaultExecutionOrder_t3862449190_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_t2370971113 * tmp = (AttributeUsageAttribute_t2370971113 *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m1170715599(tmp, 4LL, NULL);
	}
	{
		UsedByNativeCodeAttribute_t2783261251 * tmp = (UsedByNativeCodeAttribute_t2783261251 *)cache->attributes[1];
		UsedByNativeCodeAttribute__ctor_m4001769068(tmp, NULL);
	}
}
static void DefaultExecutionOrder_t3862449190_CustomAttributesCacheGenerator_U3CorderU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
	{
		DebuggerBrowsableAttribute_t1527328379 * tmp = (DebuggerBrowsableAttribute_t1527328379 *)cache->attributes[1];
		DebuggerBrowsableAttribute__ctor_m1784875164(tmp, 0LL, NULL);
	}
}
static void DefaultExecutionOrder_t3862449190_CustomAttributesCacheGenerator_DefaultExecutionOrder_get_order_m3989798846(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
}
static void NativeClassAttribute_t3085471410_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_t2370971113 * tmp = (AttributeUsageAttribute_t2370971113 *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m1170715599(tmp, 12LL, NULL);
		AttributeUsageAttribute_set_Inherited_m2205070461(tmp, false, NULL);
	}
}
static void NativeClassAttribute_t3085471410_CustomAttributesCacheGenerator_U3CQualifiedNativeNameU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		DebuggerBrowsableAttribute_t1527328379 * tmp = (DebuggerBrowsableAttribute_t1527328379 *)cache->attributes[0];
		DebuggerBrowsableAttribute__ctor_m1784875164(tmp, 0LL, NULL);
	}
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[1];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
}
static void NativeClassAttribute_t3085471410_CustomAttributesCacheGenerator_NativeClassAttribute_set_QualifiedNativeName_m875392448(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
}
static void AssemblyIsEditorAssembly_t958181164_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_t2370971113 * tmp = (AttributeUsageAttribute_t2370971113 *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m1170715599(tmp, 1LL, NULL);
	}
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[1];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void WritableAttribute_t2474002418_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_t2370971113 * tmp = (AttributeUsageAttribute_t2370971113 *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m1170715599(tmp, 2048LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m1313295265(tmp, false, NULL);
	}
}
static void ClassLibraryInitializer_t4091167741_CustomAttributesCacheGenerator_ClassLibraryInitializer_Init_m4274939016(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void Color_t4024113822_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DefaultMemberAttribute_t2956431904 * tmp = (DefaultMemberAttribute_t2956431904 *)cache->attributes[0];
		DefaultMemberAttribute__ctor_m3129895323(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
	}
	{
		UsedByNativeCodeAttribute_t2783261251 * tmp = (UsedByNativeCodeAttribute_t2783261251 *)cache->attributes[1];
		UsedByNativeCodeAttribute__ctor_m4001769068(tmp, NULL);
	}
}
static void SetupCoroutine_t912333859_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void SetupCoroutine_t912333859_CustomAttributesCacheGenerator_SetupCoroutine_InvokeMoveNext_m1274895524(CustomAttributesCache* cache)
{
	{
		SecuritySafeCriticalAttribute_t824959508 * tmp = (SecuritySafeCriticalAttribute_t824959508 *)cache->attributes[0];
		SecuritySafeCriticalAttribute__ctor_m1135903908(tmp, NULL);
	}
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[1];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void SetupCoroutine_t912333859_CustomAttributesCacheGenerator_SetupCoroutine_InvokeMember_m3346816490(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void ManagedStreamHelpers_t1729740455_CustomAttributesCacheGenerator_ManagedStreamHelpers_ManagedStreamRead_m4024070239(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void ManagedStreamHelpers_t1729740455_CustomAttributesCacheGenerator_ManagedStreamHelpers_ManagedStreamSeek_m504681536(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void ManagedStreamHelpers_t1729740455_CustomAttributesCacheGenerator_ManagedStreamHelpers_ManagedStreamLength_m1632106524(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void SendMouseEvents_t2410128589_CustomAttributesCacheGenerator_SendMouseEvents_SetMouseMoved_m2508391903(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void SendMouseEvents_t2410128589_CustomAttributesCacheGenerator_SendMouseEvents_DoSendMouseEvents_m3686682366(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void PropertyNameUtils_t1618685890_CustomAttributesCacheGenerator_PropertyNameUtils_PropertyNameFromString_m3539940257____name0(CustomAttributesCache* cache)
{
	{
		UnmarshalledAttribute_t2695814082 * tmp = (UnmarshalledAttribute_t2695814082 *)cache->attributes[0];
		UnmarshalledAttribute__ctor_m3412418881(tmp, NULL);
	}
}
static void PropertyName_t666104458_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		UsedByNativeCodeAttribute_t2783261251 * tmp = (UsedByNativeCodeAttribute_t2783261251 *)cache->attributes[0];
		UsedByNativeCodeAttribute__ctor_m4001769068(tmp, NULL);
	}
}
static void Rect_t1111852907_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		UsedByNativeCodeAttribute_t2783261251 * tmp = (UsedByNativeCodeAttribute_t2783261251 *)cache->attributes[0];
		UsedByNativeCodeAttribute__ctor_m4001769068(tmp, NULL);
	}
}
static void SerializePrivateVariables_t1291349895_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ObsoleteAttribute_t3443961000 * tmp = (ObsoleteAttribute_t3443961000 *)cache->attributes[0];
		ObsoleteAttribute__ctor_m3274828507(tmp, il2cpp_codegen_string_new_wrapper("Use SerializeField on the private variables that you want to be serialized instead"), NULL);
	}
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[1];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void SerializeField_t1393754034_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void PreferBinarySerialization_t2730910840_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
	{
		AttributeUsageAttribute_t2370971113 * tmp = (AttributeUsageAttribute_t2370971113 *)cache->attributes[1];
		AttributeUsageAttribute__ctor_m1170715599(tmp, 4LL, NULL);
	}
}
static void ISerializationCallbackReceiver_t859186888_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void ISerializationCallbackReceiver_t859186888_CustomAttributesCacheGenerator_ISerializationCallbackReceiver_OnBeforeSerialize_m1887418163(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void ISerializationCallbackReceiver_t859186888_CustomAttributesCacheGenerator_ISerializationCallbackReceiver_OnAfterDeserialize_m173705070(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void StackTraceUtility_t787969096_CustomAttributesCacheGenerator_StackTraceUtility_SetProjectFolder_m3603953530(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void StackTraceUtility_t787969096_CustomAttributesCacheGenerator_StackTraceUtility_ExtractStackTrace_m1291404532(CustomAttributesCache* cache)
{
	{
		SecuritySafeCriticalAttribute_t824959508 * tmp = (SecuritySafeCriticalAttribute_t824959508 *)cache->attributes[0];
		SecuritySafeCriticalAttribute__ctor_m1135903908(tmp, NULL);
	}
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[1];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void StackTraceUtility_t787969096_CustomAttributesCacheGenerator_StackTraceUtility_ExtractStringFromExceptionInternal_m3699450383(CustomAttributesCache* cache)
{
	{
		SecuritySafeCriticalAttribute_t824959508 * tmp = (SecuritySafeCriticalAttribute_t824959508 *)cache->attributes[0];
		SecuritySafeCriticalAttribute__ctor_m1135903908(tmp, NULL);
	}
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[1];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void StackTraceUtility_t787969096_CustomAttributesCacheGenerator_StackTraceUtility_PostprocessStacktrace_m4246116829(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void StackTraceUtility_t787969096_CustomAttributesCacheGenerator_StackTraceUtility_ExtractFormattedStackTrace_m382049542(CustomAttributesCache* cache)
{
	{
		SecuritySafeCriticalAttribute_t824959508 * tmp = (SecuritySafeCriticalAttribute_t824959508 *)cache->attributes[0];
		SecuritySafeCriticalAttribute__ctor_m1135903908(tmp, NULL);
	}
}
static void UnityException_t760375625_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void ArgumentCache_t3682645796_CustomAttributesCacheGenerator_m_ObjectArgument(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t1489703117 * tmp = (FormerlySerializedAsAttribute_t1489703117 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m219264990(tmp, il2cpp_codegen_string_new_wrapper("objectArgument"), NULL);
	}
	{
		SerializeField_t1393754034 * tmp = (SerializeField_t1393754034 *)cache->attributes[1];
		SerializeField__ctor_m3401225370(tmp, NULL);
	}
}
static void ArgumentCache_t3682645796_CustomAttributesCacheGenerator_m_ObjectArgumentAssemblyTypeName(CustomAttributesCache* cache)
{
	{
		SerializeField_t1393754034 * tmp = (SerializeField_t1393754034 *)cache->attributes[0];
		SerializeField__ctor_m3401225370(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t1489703117 * tmp = (FormerlySerializedAsAttribute_t1489703117 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m219264990(tmp, il2cpp_codegen_string_new_wrapper("objectArgumentAssemblyTypeName"), NULL);
	}
}
static void ArgumentCache_t3682645796_CustomAttributesCacheGenerator_m_IntArgument(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t1489703117 * tmp = (FormerlySerializedAsAttribute_t1489703117 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m219264990(tmp, il2cpp_codegen_string_new_wrapper("intArgument"), NULL);
	}
	{
		SerializeField_t1393754034 * tmp = (SerializeField_t1393754034 *)cache->attributes[1];
		SerializeField__ctor_m3401225370(tmp, NULL);
	}
}
static void ArgumentCache_t3682645796_CustomAttributesCacheGenerator_m_FloatArgument(CustomAttributesCache* cache)
{
	{
		SerializeField_t1393754034 * tmp = (SerializeField_t1393754034 *)cache->attributes[0];
		SerializeField__ctor_m3401225370(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t1489703117 * tmp = (FormerlySerializedAsAttribute_t1489703117 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m219264990(tmp, il2cpp_codegen_string_new_wrapper("floatArgument"), NULL);
	}
}
static void ArgumentCache_t3682645796_CustomAttributesCacheGenerator_m_StringArgument(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t1489703117 * tmp = (FormerlySerializedAsAttribute_t1489703117 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m219264990(tmp, il2cpp_codegen_string_new_wrapper("stringArgument"), NULL);
	}
	{
		SerializeField_t1393754034 * tmp = (SerializeField_t1393754034 *)cache->attributes[1];
		SerializeField__ctor_m3401225370(tmp, NULL);
	}
}
static void ArgumentCache_t3682645796_CustomAttributesCacheGenerator_m_BoolArgument(CustomAttributesCache* cache)
{
	{
		SerializeField_t1393754034 * tmp = (SerializeField_t1393754034 *)cache->attributes[0];
		SerializeField__ctor_m3401225370(tmp, NULL);
	}
}
static void InvokableCall_t2876658856_CustomAttributesCacheGenerator_Delegate(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
	{
		DebuggerBrowsableAttribute_t1527328379 * tmp = (DebuggerBrowsableAttribute_t1527328379 *)cache->attributes[1];
		DebuggerBrowsableAttribute__ctor_m1784875164(tmp, 0LL, NULL);
	}
}
static void InvokableCall_1_t1604090555_CustomAttributesCacheGenerator_Delegate(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
	{
		DebuggerBrowsableAttribute_t1527328379 * tmp = (DebuggerBrowsableAttribute_t1527328379 *)cache->attributes[1];
		DebuggerBrowsableAttribute__ctor_m1784875164(tmp, 0LL, NULL);
	}
}
static void InvokableCall_2_t3976188983_CustomAttributesCacheGenerator_Delegate(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
	{
		DebuggerBrowsableAttribute_t1527328379 * tmp = (DebuggerBrowsableAttribute_t1527328379 *)cache->attributes[1];
		DebuggerBrowsableAttribute__ctor_m1784875164(tmp, 0LL, NULL);
	}
}
static void InvokableCall_3_t3917817895_CustomAttributesCacheGenerator_Delegate(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
	{
		DebuggerBrowsableAttribute_t1527328379 * tmp = (DebuggerBrowsableAttribute_t1527328379 *)cache->attributes[1];
		DebuggerBrowsableAttribute__ctor_m1784875164(tmp, 0LL, NULL);
	}
}
static void InvokableCall_4_t2387264047_CustomAttributesCacheGenerator_Delegate(CustomAttributesCache* cache)
{
	{
		DebuggerBrowsableAttribute_t1527328379 * tmp = (DebuggerBrowsableAttribute_t1527328379 *)cache->attributes[0];
		DebuggerBrowsableAttribute__ctor_m1784875164(tmp, 0LL, NULL);
	}
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[1];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
}
static void PersistentCall_t2687057912_CustomAttributesCacheGenerator_m_Target(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t1489703117 * tmp = (FormerlySerializedAsAttribute_t1489703117 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m219264990(tmp, il2cpp_codegen_string_new_wrapper("instance"), NULL);
	}
	{
		SerializeField_t1393754034 * tmp = (SerializeField_t1393754034 *)cache->attributes[1];
		SerializeField__ctor_m3401225370(tmp, NULL);
	}
}
static void PersistentCall_t2687057912_CustomAttributesCacheGenerator_m_MethodName(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t1489703117 * tmp = (FormerlySerializedAsAttribute_t1489703117 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m219264990(tmp, il2cpp_codegen_string_new_wrapper("methodName"), NULL);
	}
	{
		SerializeField_t1393754034 * tmp = (SerializeField_t1393754034 *)cache->attributes[1];
		SerializeField__ctor_m3401225370(tmp, NULL);
	}
}
static void PersistentCall_t2687057912_CustomAttributesCacheGenerator_m_Mode(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t1489703117 * tmp = (FormerlySerializedAsAttribute_t1489703117 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m219264990(tmp, il2cpp_codegen_string_new_wrapper("mode"), NULL);
	}
	{
		SerializeField_t1393754034 * tmp = (SerializeField_t1393754034 *)cache->attributes[1];
		SerializeField__ctor_m3401225370(tmp, NULL);
	}
}
static void PersistentCall_t2687057912_CustomAttributesCacheGenerator_m_Arguments(CustomAttributesCache* cache)
{
	{
		SerializeField_t1393754034 * tmp = (SerializeField_t1393754034 *)cache->attributes[0];
		SerializeField__ctor_m3401225370(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t1489703117 * tmp = (FormerlySerializedAsAttribute_t1489703117 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m219264990(tmp, il2cpp_codegen_string_new_wrapper("arguments"), NULL);
	}
}
static void PersistentCall_t2687057912_CustomAttributesCacheGenerator_m_CallState(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t1489703117 * tmp = (FormerlySerializedAsAttribute_t1489703117 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m219264990(tmp, il2cpp_codegen_string_new_wrapper("m_Enabled"), NULL);
	}
	{
		FormerlySerializedAsAttribute_t1489703117 * tmp = (FormerlySerializedAsAttribute_t1489703117 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m219264990(tmp, il2cpp_codegen_string_new_wrapper("enabled"), NULL);
	}
	{
		SerializeField_t1393754034 * tmp = (SerializeField_t1393754034 *)cache->attributes[2];
		SerializeField__ctor_m3401225370(tmp, NULL);
	}
}
static void PersistentCallGroup_t586105043_CustomAttributesCacheGenerator_m_Calls(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t1489703117 * tmp = (FormerlySerializedAsAttribute_t1489703117 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m219264990(tmp, il2cpp_codegen_string_new_wrapper("m_Listeners"), NULL);
	}
	{
		SerializeField_t1393754034 * tmp = (SerializeField_t1393754034 *)cache->attributes[1];
		SerializeField__ctor_m3401225370(tmp, NULL);
	}
}
static void UnityEventBase_t3171537072_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		UsedByNativeCodeAttribute_t2783261251 * tmp = (UsedByNativeCodeAttribute_t2783261251 *)cache->attributes[0];
		UsedByNativeCodeAttribute__ctor_m4001769068(tmp, NULL);
	}
}
static void UnityEventBase_t3171537072_CustomAttributesCacheGenerator_m_PersistentCalls(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t1489703117 * tmp = (FormerlySerializedAsAttribute_t1489703117 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m219264990(tmp, il2cpp_codegen_string_new_wrapper("m_PersistentListeners"), NULL);
	}
	{
		SerializeField_t1393754034 * tmp = (SerializeField_t1393754034 *)cache->attributes[1];
		SerializeField__ctor_m3401225370(tmp, NULL);
	}
}
static void UnityEventBase_t3171537072_CustomAttributesCacheGenerator_m_TypeName(CustomAttributesCache* cache)
{
	{
		SerializeField_t1393754034 * tmp = (SerializeField_t1393754034 *)cache->attributes[0];
		SerializeField__ctor_m3401225370(tmp, NULL);
	}
}
static void UnityEvent_t1464431504_CustomAttributesCacheGenerator_UnityEvent__ctor_m811779962(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void UnityEvent_1_t4023850743_CustomAttributesCacheGenerator_UnityEvent_1__ctor_m3496433407(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void UnityEvent_2_t1533051435_CustomAttributesCacheGenerator_UnityEvent_2__ctor_m565043222(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void UnityEvent_3_t4091795572_CustomAttributesCacheGenerator_UnityEvent_3__ctor_m1441002641(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void UnityEvent_4_t1002767761_CustomAttributesCacheGenerator_UnityEvent_4__ctor_m3177569170(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void UnityString_t2219893151_CustomAttributesCacheGenerator_UnityString_Format_m2513837565____args1(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t3330428127 * tmp = (ParamArrayAttribute_t3330428127 *)cache->attributes[0];
		ParamArrayAttribute__ctor_m1696407647(tmp, NULL);
	}
}
static void UnitySynchronizationContext_t90163264_CustomAttributesCacheGenerator_UnitySynchronizationContext_InitializeSynchronizationContext_m2662403408(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void UnitySynchronizationContext_t90163264_CustomAttributesCacheGenerator_UnitySynchronizationContext_ExecuteTasks_m3600858646(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void Vector4_t4052901136_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		UsedByNativeCodeAttribute_t2783261251 * tmp = (UsedByNativeCodeAttribute_t2783261251 *)cache->attributes[0];
		UsedByNativeCodeAttribute__ctor_m4001769068(tmp, NULL);
	}
	{
		DefaultMemberAttribute_t2956431904 * tmp = (DefaultMemberAttribute_t2956431904 *)cache->attributes[1];
		DefaultMemberAttribute__ctor_m3129895323(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
	}
}
static void UnmarshalledAttribute_t2695814082_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_t2370971113 * tmp = (AttributeUsageAttribute_t2370971113 *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m1170715599(tmp, 2048LL, NULL);
	}
}
static void ReadOnlyAttribute_t3185780063_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_t2370971113 * tmp = (AttributeUsageAttribute_t2370971113 *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m1170715599(tmp, 256LL, NULL);
	}
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[1];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void ReadWriteAttribute_t1843161347_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_t2370971113 * tmp = (AttributeUsageAttribute_t2370971113 *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m1170715599(tmp, 256LL, NULL);
	}
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[1];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void WriteOnlyAttribute_t1011568951_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_t2370971113 * tmp = (AttributeUsageAttribute_t2370971113 *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m1170715599(tmp, 256LL, NULL);
	}
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[1];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void DeallocateOnJobCompletionAttribute_t2226375310_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_t2370971113 * tmp = (AttributeUsageAttribute_t2370971113 *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m1170715599(tmp, 256LL, NULL);
	}
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[1];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void NativeContainerAttribute_t1940066792_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_t2370971113 * tmp = (AttributeUsageAttribute_t2370971113 *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m1170715599(tmp, 8LL, NULL);
	}
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[1];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void NativeContainerSupportsAtomicWriteAttribute_t1199220324_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
	{
		AttributeUsageAttribute_t2370971113 * tmp = (AttributeUsageAttribute_t2370971113 *)cache->attributes[1];
		AttributeUsageAttribute__ctor_m1170715599(tmp, 8LL, NULL);
	}
}
static void NativeContainerSupportsMinMaxWriteRestrictionAttribute_t3950250497_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_t2370971113 * tmp = (AttributeUsageAttribute_t2370971113 *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m1170715599(tmp, 8LL, NULL);
	}
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[1];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void PlayableBinding_t136667248_CustomAttributesCacheGenerator_U3CstreamNameU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
	{
		DebuggerBrowsableAttribute_t1527328379 * tmp = (DebuggerBrowsableAttribute_t1527328379 *)cache->attributes[1];
		DebuggerBrowsableAttribute__ctor_m1784875164(tmp, 0LL, NULL);
	}
}
static void PlayableBinding_t136667248_CustomAttributesCacheGenerator_U3CstreamTypeU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
	{
		DebuggerBrowsableAttribute_t1527328379 * tmp = (DebuggerBrowsableAttribute_t1527328379 *)cache->attributes[1];
		DebuggerBrowsableAttribute__ctor_m1784875164(tmp, 0LL, NULL);
	}
}
static void PlayableBinding_t136667248_CustomAttributesCacheGenerator_U3CsourceObjectU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		DebuggerBrowsableAttribute_t1527328379 * tmp = (DebuggerBrowsableAttribute_t1527328379 *)cache->attributes[0];
		DebuggerBrowsableAttribute__ctor_m1784875164(tmp, 0LL, NULL);
	}
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[1];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
}
static void PlayableBinding_t136667248_CustomAttributesCacheGenerator_U3CsourceBindingTypeU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
	{
		DebuggerBrowsableAttribute_t1527328379 * tmp = (DebuggerBrowsableAttribute_t1527328379 *)cache->attributes[1];
		DebuggerBrowsableAttribute__ctor_m1784875164(tmp, 0LL, NULL);
	}
}
static void PlayableAsset_t2436284446_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void PlayableAsset_t2436284446_CustomAttributesCacheGenerator_PlayableAsset_Internal_CreatePlayable_m4149673157(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void PlayableAsset_t2436284446_CustomAttributesCacheGenerator_PlayableAsset_Internal_GetPlayableAssetDuration_m3126328874(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void PlayableBehaviour_t4011616635_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void ScriptPlayableOutput_t149882980_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void DefaultValueAttribute_t2693326634_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_t2370971113 * tmp = (AttributeUsageAttribute_t2370971113 *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m1170715599(tmp, 18432LL, NULL);
	}
}
static void ILogHandler_t2217299658_CustomAttributesCacheGenerator_ILogHandler_LogFormat_m3088184411____args3(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t3330428127 * tmp = (ParamArrayAttribute_t3330428127 *)cache->attributes[0];
		ParamArrayAttribute__ctor_m1696407647(tmp, NULL);
	}
}
static void Logger_t605719344_CustomAttributesCacheGenerator_U3ClogHandlerU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		DebuggerBrowsableAttribute_t1527328379 * tmp = (DebuggerBrowsableAttribute_t1527328379 *)cache->attributes[0];
		DebuggerBrowsableAttribute__ctor_m1784875164(tmp, 0LL, NULL);
	}
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[1];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
}
static void Logger_t605719344_CustomAttributesCacheGenerator_U3ClogEnabledU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		DebuggerBrowsableAttribute_t1527328379 * tmp = (DebuggerBrowsableAttribute_t1527328379 *)cache->attributes[0];
		DebuggerBrowsableAttribute__ctor_m1784875164(tmp, 0LL, NULL);
	}
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[1];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
}
static void Logger_t605719344_CustomAttributesCacheGenerator_U3CfilterLogTypeU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
	{
		DebuggerBrowsableAttribute_t1527328379 * tmp = (DebuggerBrowsableAttribute_t1527328379 *)cache->attributes[1];
		DebuggerBrowsableAttribute__ctor_m1784875164(tmp, 0LL, NULL);
	}
}
static void Logger_t605719344_CustomAttributesCacheGenerator_Logger_get_logHandler_m1719487699(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
}
static void Logger_t605719344_CustomAttributesCacheGenerator_Logger_set_logHandler_m6066634(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
}
static void Logger_t605719344_CustomAttributesCacheGenerator_Logger_get_logEnabled_m2290808316(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
}
static void Logger_t605719344_CustomAttributesCacheGenerator_Logger_set_logEnabled_m1662937540(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
}
static void Logger_t605719344_CustomAttributesCacheGenerator_Logger_get_filterLogType_m426955796(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
}
static void Logger_t605719344_CustomAttributesCacheGenerator_Logger_set_filterLogType_m2745985065(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
}
static void Logger_t605719344_CustomAttributesCacheGenerator_Logger_LogFormat_m1581286573____args3(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t3330428127 * tmp = (ParamArrayAttribute_t3330428127 *)cache->attributes[0];
		ParamArrayAttribute__ctor_m1696407647(tmp, NULL);
	}
}
static void PlayerConnection_t3559457672_CustomAttributesCacheGenerator_m_PlayerEditorConnectionEvents(CustomAttributesCache* cache)
{
	{
		SerializeField_t1393754034 * tmp = (SerializeField_t1393754034 *)cache->attributes[0];
		SerializeField__ctor_m3401225370(tmp, NULL);
	}
}
static void PlayerConnection_t3559457672_CustomAttributesCacheGenerator_m_connectedPlayers(CustomAttributesCache* cache)
{
	{
		SerializeField_t1393754034 * tmp = (SerializeField_t1393754034 *)cache->attributes[0];
		SerializeField__ctor_m3401225370(tmp, NULL);
	}
}
static void PlayerConnection_t3559457672_CustomAttributesCacheGenerator_PlayerConnection_MessageCallbackInternal_m469132778(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void PlayerConnection_t3559457672_CustomAttributesCacheGenerator_PlayerConnection_ConnectedCallbackInternal_m1135070508(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void PlayerConnection_t3559457672_CustomAttributesCacheGenerator_PlayerConnection_DisconnectedCallback_m2631702132(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void PlayerEditorConnectionEvents_t2151463461_CustomAttributesCacheGenerator_messageTypeSubscribers(CustomAttributesCache* cache)
{
	{
		SerializeField_t1393754034 * tmp = (SerializeField_t1393754034 *)cache->attributes[0];
		SerializeField__ctor_m3401225370(tmp, NULL);
	}
}
static void PlayerEditorConnectionEvents_t2151463461_CustomAttributesCacheGenerator_connectionEvent(CustomAttributesCache* cache)
{
	{
		SerializeField_t1393754034 * tmp = (SerializeField_t1393754034 *)cache->attributes[0];
		SerializeField__ctor_m3401225370(tmp, NULL);
	}
}
static void PlayerEditorConnectionEvents_t2151463461_CustomAttributesCacheGenerator_disconnectionEvent(CustomAttributesCache* cache)
{
	{
		SerializeField_t1393754034 * tmp = (SerializeField_t1393754034 *)cache->attributes[0];
		SerializeField__ctor_m3401225370(tmp, NULL);
	}
}
static void MessageTypeSubscribers_t1657992506_CustomAttributesCacheGenerator_m_messageTypeId(CustomAttributesCache* cache)
{
	{
		SerializeField_t1393754034 * tmp = (SerializeField_t1393754034 *)cache->attributes[0];
		SerializeField__ctor_m3401225370(tmp, NULL);
	}
}
static void U3CInvokeMessageIdSubscribersU3Ec__AnonStorey0_t1928087868_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
}
static void RenderPipelineManager_t3681178706_CustomAttributesCacheGenerator_U3CcurrentPipelineU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
	{
		DebuggerBrowsableAttribute_t1527328379 * tmp = (DebuggerBrowsableAttribute_t1527328379 *)cache->attributes[1];
		DebuggerBrowsableAttribute__ctor_m1784875164(tmp, 0LL, NULL);
	}
}
static void RenderPipelineManager_t3681178706_CustomAttributesCacheGenerator_RenderPipelineManager_get_currentPipeline_m3103495424(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
}
static void RenderPipelineManager_t3681178706_CustomAttributesCacheGenerator_RenderPipelineManager_set_currentPipeline_m3602131277(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
}
static void RenderPipelineManager_t3681178706_CustomAttributesCacheGenerator_RenderPipelineManager_CleanupRenderPipeline_m309528094(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void RenderPipelineManager_t3681178706_CustomAttributesCacheGenerator_RenderPipelineManager_DoRenderLoop_Internal_m124400281(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void UsedByNativeCodeAttribute_t2783261251_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_t2370971113 * tmp = (AttributeUsageAttribute_t2370971113 *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m1170715599(tmp, 1532LL, NULL);
		AttributeUsageAttribute_set_Inherited_m2205070461(tmp, false, NULL);
	}
}
static void RequiredByNativeCodeAttribute_t3603024077_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_t2370971113 * tmp = (AttributeUsageAttribute_t2370971113 *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m1170715599(tmp, 1532LL, NULL);
		AttributeUsageAttribute_set_Inherited_m2205070461(tmp, false, NULL);
	}
}
static void FormerlySerializedAsAttribute_t1489703117_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
	{
		AttributeUsageAttribute_t2370971113 * tmp = (AttributeUsageAttribute_t2370971113 *)cache->attributes[1];
		AttributeUsageAttribute__ctor_m1170715599(tmp, 256LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m1313295265(tmp, true, NULL);
		AttributeUsageAttribute_set_Inherited_m2205070461(tmp, false, NULL);
	}
}
static void NetFxCoreExtensions_t2215717558_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t1721836986 * tmp = (ExtensionAttribute_t1721836986 *)cache->attributes[0];
		ExtensionAttribute__ctor_m831032830(tmp, NULL);
	}
}
static void NetFxCoreExtensions_t2215717558_CustomAttributesCacheGenerator_NetFxCoreExtensions_CreateDelegate_m3544893108(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t1721836986 * tmp = (ExtensionAttribute_t1721836986 *)cache->attributes[0];
		ExtensionAttribute__ctor_m831032830(tmp, NULL);
	}
}
static void CSSMeasureFunc_t2162796896_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		UnmanagedFunctionPointerAttribute_t1887822751 * tmp = (UnmanagedFunctionPointerAttribute_t1887822751 *)cache->attributes[0];
		UnmanagedFunctionPointerAttribute__ctor_m323663599(tmp, 2LL, NULL);
	}
}
static void Native_t4028515775_CustomAttributesCacheGenerator_Native_CSSNodeMeasureInvoke_m1142609737(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void g_UnityEngine_AudioModule_Assembly_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[0];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.UnityWebRequestAudioModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[1];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("GoogleAR.UnityNative"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[2];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("Unity.RuntimeTests.Framework.Tests"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[3];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("Unity.RuntimeTests.Framework"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[4];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("Unity.RuntimeTests"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[5];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("Unity.IntegrationTests.Framework"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[6];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("Unity.IntegrationTests.Timeline"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[7];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.ScreenCaptureModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[8];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.UmbraModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[9];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("Unity.Automation"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[10];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("Unity.DeploymentTests.Services"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[11];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("Unity.IntegrationTests"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[12];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("Unity.IntegrationTests.UnityAnalytics"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[13];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.SpatialTracking"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[14];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.GoogleAudioSpatializer"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[15];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.Timeline"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[16];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.Purchasing"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[17];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.Advertisements"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[18];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.Analytics"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[19];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.Cloud.Service"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[20];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.TestRunner"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[21];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.Physics"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[22];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.Terrain"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[23];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.TerrainPhysics"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[24];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.Networking"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[25];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.Cloud"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[26];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.Physics2DModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[27];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.ARModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[28];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.WebModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[29];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.UnityConnectModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[30];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.PerformanceReportingModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[31];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.CloudWebServicesModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[32];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.ImageConversionModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[33];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.UIElementsModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[34];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.StyleSheetsModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[35];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.AudioModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[36];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.BaselibModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[37];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.GameCenterModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[38];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.GridModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[39];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.SpatialTrackingModule"), NULL);
	}
	{
		DebuggableAttribute_t3717120398 * tmp = (DebuggableAttribute_t3717120398 *)cache->attributes[40];
		DebuggableAttribute__ctor_m3553871532(tmp, 258LL, NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[41];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.SpriteMaskModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[42];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.JSONSerializeModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[43];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.NScreenModule"), NULL);
	}
	{
		RuntimeCompatibilityAttribute_t38650954 * tmp = (RuntimeCompatibilityAttribute_t38650954 *)cache->attributes[44];
		RuntimeCompatibilityAttribute__ctor_m853651104(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m4154715430(tmp, true, NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[45];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.ParticlesLegacyModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[46];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[47];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.VRModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[48];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.InputModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[49];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.TerrainModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[50];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.TilemapModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[51];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.VideoModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[52];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.WindModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[53];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.AccessibilityModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[54];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.ParticleSystemModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[55];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.PhysicsModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[56];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.VehiclesModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[57];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.ClothModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[58];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.AIModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[59];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.AnimationModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[60];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.TextRenderingModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[61];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.UIModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[62];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.TerrainPhysicsModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[63];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.IMGUIModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[64];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.UnityWebRequestModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[65];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.UnityWebRequestWWWModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[66];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.UnityWebRequestTextureModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[67];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.ClusterInputModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[68];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.ClusterRendererModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[69];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.FacebookModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[70];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.UNETModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[71];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.DirectorModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[72];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.TimelineModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[73];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.CrashReportingModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[74];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.UnityAnalyticsModule"), NULL);
	}
}
static void AudioSettings_t2831187810_CustomAttributesCacheGenerator_OnAudioConfigurationChanged(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
	{
		DebuggerBrowsableAttribute_t1527328379 * tmp = (DebuggerBrowsableAttribute_t1527328379 *)cache->attributes[1];
		DebuggerBrowsableAttribute__ctor_m1784875164(tmp, 0LL, NULL);
	}
}
static void AudioSettings_t2831187810_CustomAttributesCacheGenerator_AudioSettings_GetSpatializerPluginName_m2848677236(CustomAttributesCache* cache)
{
	{
		GeneratedByOldBindingsGeneratorAttribute_t861731860 * tmp = (GeneratedByOldBindingsGeneratorAttribute_t861731860 *)cache->attributes[0];
		GeneratedByOldBindingsGeneratorAttribute__ctor_m1819602998(tmp, NULL);
	}
}
static void AudioSettings_t2831187810_CustomAttributesCacheGenerator_AudioSettings_InvokeOnAudioConfigurationChanged_m3184710774(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void AudioSettings_t2831187810_CustomAttributesCacheGenerator_AudioSettings_InvokeOnAudioManagerUpdate_m2154005943(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void AudioSettings_t2831187810_CustomAttributesCacheGenerator_AudioSettings_InvokeOnAudioSourcePlay_m2060412953(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void AudioSettings_t2831187810_CustomAttributesCacheGenerator_AudioSettings_GetAmbisonicDecoderPluginName_m70128080(CustomAttributesCache* cache)
{
	{
		GeneratedByOldBindingsGeneratorAttribute_t861731860 * tmp = (GeneratedByOldBindingsGeneratorAttribute_t861731860 *)cache->attributes[0];
		GeneratedByOldBindingsGeneratorAttribute__ctor_m1819602998(tmp, NULL);
	}
}
static void AudioExtensionManager_t3128192687_CustomAttributesCacheGenerator_AudioExtensionManager_GetAudioListener_m3189978351(CustomAttributesCache* cache)
{
	{
		GeneratedByOldBindingsGeneratorAttribute_t861731860 * tmp = (GeneratedByOldBindingsGeneratorAttribute_t861731860 *)cache->attributes[0];
		GeneratedByOldBindingsGeneratorAttribute__ctor_m1819602998(tmp, NULL);
	}
}
static void AudioClip_t961166807_CustomAttributesCacheGenerator_m_PCMReaderCallback(CustomAttributesCache* cache)
{
	{
		DebuggerBrowsableAttribute_t1527328379 * tmp = (DebuggerBrowsableAttribute_t1527328379 *)cache->attributes[0];
		DebuggerBrowsableAttribute__ctor_m1784875164(tmp, 0LL, NULL);
	}
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[1];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
}
static void AudioClip_t961166807_CustomAttributesCacheGenerator_m_PCMSetPositionCallback(CustomAttributesCache* cache)
{
	{
		DebuggerBrowsableAttribute_t1527328379 * tmp = (DebuggerBrowsableAttribute_t1527328379 *)cache->attributes[0];
		DebuggerBrowsableAttribute__ctor_m1784875164(tmp, 0LL, NULL);
	}
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[1];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
}
static void AudioClip_t961166807_CustomAttributesCacheGenerator_AudioClip_get_ambisonic_m2163265490(CustomAttributesCache* cache)
{
	{
		GeneratedByOldBindingsGeneratorAttribute_t861731860 * tmp = (GeneratedByOldBindingsGeneratorAttribute_t861731860 *)cache->attributes[0];
		GeneratedByOldBindingsGeneratorAttribute__ctor_m1819602998(tmp, NULL);
	}
}
static void AudioClip_t961166807_CustomAttributesCacheGenerator_AudioClip_InvokePCMReaderCallback_Internal_m779593384(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void AudioClip_t961166807_CustomAttributesCacheGenerator_AudioClip_InvokePCMSetPositionCallback_Internal_m1609205883(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void AudioListener_t3360289753_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AudioListener_t3360289753_CustomAttributesCacheGenerator_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_t597737523 * tmp = (RequireComponent_t597737523 *)cache->attributes[0];
		RequireComponent__ctor_m3242490219(tmp, il2cpp_codegen_type_get_object(Transform_t94239816_0_0_0_var), NULL);
	}
}
static void AudioListener_t3360289753_CustomAttributesCacheGenerator_AudioListener_GetNumExtensionProperties_m2279481642(CustomAttributesCache* cache)
{
	{
		GeneratedByOldBindingsGeneratorAttribute_t861731860 * tmp = (GeneratedByOldBindingsGeneratorAttribute_t861731860 *)cache->attributes[0];
		GeneratedByOldBindingsGeneratorAttribute__ctor_m1819602998(tmp, NULL);
	}
}
static void AudioListener_t3360289753_CustomAttributesCacheGenerator_AudioListener_INTERNAL_CALL_ReadExtensionName_m3825001381(CustomAttributesCache* cache)
{
	{
		GeneratedByOldBindingsGeneratorAttribute_t861731860 * tmp = (GeneratedByOldBindingsGeneratorAttribute_t861731860 *)cache->attributes[0];
		GeneratedByOldBindingsGeneratorAttribute__ctor_m1819602998(tmp, NULL);
	}
}
static void AudioListener_t3360289753_CustomAttributesCacheGenerator_AudioListener_INTERNAL_CALL_ReadExtensionPropertyName_m3514206420(CustomAttributesCache* cache)
{
	{
		GeneratedByOldBindingsGeneratorAttribute_t861731860 * tmp = (GeneratedByOldBindingsGeneratorAttribute_t861731860 *)cache->attributes[0];
		GeneratedByOldBindingsGeneratorAttribute__ctor_m1819602998(tmp, NULL);
	}
}
static void AudioListener_t3360289753_CustomAttributesCacheGenerator_AudioListener_ReadExtensionPropertyValue_m2214914024(CustomAttributesCache* cache)
{
	{
		GeneratedByOldBindingsGeneratorAttribute_t861731860 * tmp = (GeneratedByOldBindingsGeneratorAttribute_t861731860 *)cache->attributes[0];
		GeneratedByOldBindingsGeneratorAttribute__ctor_m1819602998(tmp, NULL);
	}
}
static void AudioListener_t3360289753_CustomAttributesCacheGenerator_AudioListener_INTERNAL_CALL_ClearExtensionProperties_m1492170042(CustomAttributesCache* cache)
{
	{
		GeneratedByOldBindingsGeneratorAttribute_t861731860 * tmp = (GeneratedByOldBindingsGeneratorAttribute_t861731860 *)cache->attributes[0];
		GeneratedByOldBindingsGeneratorAttribute__ctor_m1819602998(tmp, NULL);
	}
}
static void AudioSource_t3827284957_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AudioSource_t3827284957_CustomAttributesCacheGenerator_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_t597737523 * tmp = (RequireComponent_t597737523 *)cache->attributes[0];
		RequireComponent__ctor_m3242490219(tmp, il2cpp_codegen_type_get_object(Transform_t94239816_0_0_0_var), NULL);
	}
}
static void AudioSource_t3827284957_CustomAttributesCacheGenerator_AudioSource_get_clip_m2340175708(CustomAttributesCache* cache)
{
	{
		GeneratedByOldBindingsGeneratorAttribute_t861731860 * tmp = (GeneratedByOldBindingsGeneratorAttribute_t861731860 *)cache->attributes[0];
		GeneratedByOldBindingsGeneratorAttribute__ctor_m1819602998(tmp, NULL);
	}
}
static void AudioSource_t3827284957_CustomAttributesCacheGenerator_AudioSource_get_isPlaying_m1796357815(CustomAttributesCache* cache)
{
	{
		GeneratedByOldBindingsGeneratorAttribute_t861731860 * tmp = (GeneratedByOldBindingsGeneratorAttribute_t861731860 *)cache->attributes[0];
		GeneratedByOldBindingsGeneratorAttribute__ctor_m1819602998(tmp, NULL);
	}
}
static void AudioSource_t3827284957_CustomAttributesCacheGenerator_AudioSource_get_spatializeInternal_m3026556515(CustomAttributesCache* cache)
{
	{
		GeneratedByOldBindingsGeneratorAttribute_t861731860 * tmp = (GeneratedByOldBindingsGeneratorAttribute_t861731860 *)cache->attributes[0];
		GeneratedByOldBindingsGeneratorAttribute__ctor_m1819602998(tmp, NULL);
	}
}
static void AudioSource_t3827284957_CustomAttributesCacheGenerator_AudioSource_GetNumExtensionProperties_m709284950(CustomAttributesCache* cache)
{
	{
		GeneratedByOldBindingsGeneratorAttribute_t861731860 * tmp = (GeneratedByOldBindingsGeneratorAttribute_t861731860 *)cache->attributes[0];
		GeneratedByOldBindingsGeneratorAttribute__ctor_m1819602998(tmp, NULL);
	}
}
static void AudioSource_t3827284957_CustomAttributesCacheGenerator_AudioSource_INTERNAL_CALL_ReadExtensionName_m940874956(CustomAttributesCache* cache)
{
	{
		GeneratedByOldBindingsGeneratorAttribute_t861731860 * tmp = (GeneratedByOldBindingsGeneratorAttribute_t861731860 *)cache->attributes[0];
		GeneratedByOldBindingsGeneratorAttribute__ctor_m1819602998(tmp, NULL);
	}
}
static void AudioSource_t3827284957_CustomAttributesCacheGenerator_AudioSource_INTERNAL_CALL_ReadExtensionPropertyName_m1096137561(CustomAttributesCache* cache)
{
	{
		GeneratedByOldBindingsGeneratorAttribute_t861731860 * tmp = (GeneratedByOldBindingsGeneratorAttribute_t861731860 *)cache->attributes[0];
		GeneratedByOldBindingsGeneratorAttribute__ctor_m1819602998(tmp, NULL);
	}
}
static void AudioSource_t3827284957_CustomAttributesCacheGenerator_AudioSource_ReadExtensionPropertyValue_m1555844817(CustomAttributesCache* cache)
{
	{
		GeneratedByOldBindingsGeneratorAttribute_t861731860 * tmp = (GeneratedByOldBindingsGeneratorAttribute_t861731860 *)cache->attributes[0];
		GeneratedByOldBindingsGeneratorAttribute__ctor_m1819602998(tmp, NULL);
	}
}
static void AudioSource_t3827284957_CustomAttributesCacheGenerator_AudioSource_INTERNAL_CALL_ClearExtensionProperties_m3670533364(CustomAttributesCache* cache)
{
	{
		GeneratedByOldBindingsGeneratorAttribute_t861731860 * tmp = (GeneratedByOldBindingsGeneratorAttribute_t861731860 *)cache->attributes[0];
		GeneratedByOldBindingsGeneratorAttribute__ctor_m1819602998(tmp, NULL);
	}
}
static void AudioSource_t3827284957_CustomAttributesCacheGenerator_AudioSource_t3827284957____clip_PropertyInfo(CustomAttributesCache* cache)
{
	{
		ThreadAndSerializationSafeAttribute_t566760631 * tmp = (ThreadAndSerializationSafeAttribute_t566760631 *)cache->attributes[0];
		ThreadAndSerializationSafeAttribute__ctor_m211248632(tmp, NULL);
	}
}
static void AudioMixerPlayable_t1017334687_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void AudioClipPlayable_t3812371941_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void AudioPlayableOutput_t3386959622_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void AudioListenerExtension_t75428788_CustomAttributesCacheGenerator_m_audioListener(CustomAttributesCache* cache)
{
	{
		SerializeField_t1393754034 * tmp = (SerializeField_t1393754034 *)cache->attributes[0];
		SerializeField__ctor_m3401225370(tmp, NULL);
	}
}
static void AudioSourceExtension_t3778551013_CustomAttributesCacheGenerator_m_audioSource(CustomAttributesCache* cache)
{
	{
		SerializeField_t1393754034 * tmp = (SerializeField_t1393754034 *)cache->attributes[0];
		SerializeField__ctor_m3401225370(tmp, NULL);
	}
}
static void g_UnityEngine_GameCenterModule_Assembly_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[0];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.Physics"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[1];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.Terrain"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[2];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.TerrainPhysics"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[3];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.Networking"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[4];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.Cloud"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[5];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.Cloud.Service"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[6];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.Analytics"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[7];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.Advertisements"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[8];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.Purchasing"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[9];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.Timeline"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[10];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.GoogleAudioSpatializer"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[11];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.TestRunner"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[12];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("Unity.Automation"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[13];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("Unity.DeploymentTests.Services"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[14];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("Unity.IntegrationTests"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[15];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("Unity.IntegrationTests.UnityAnalytics"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[16];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("Unity.IntegrationTests.Timeline"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[17];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("Unity.IntegrationTests.Framework"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[18];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("Unity.RuntimeTests"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[19];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("Unity.RuntimeTests.Framework"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[20];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("Unity.RuntimeTests.Framework.Tests"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[21];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("GoogleAR.UnityNative"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[22];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.SpatialTracking"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[23];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.AccessibilityModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[24];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.ParticleSystemModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[25];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.PhysicsModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[26];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.VehiclesModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[27];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.ClothModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[28];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.AIModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[29];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.AnimationModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[30];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.TextRenderingModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[31];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.UIModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[32];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.TerrainPhysicsModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[33];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.IMGUIModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[34];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.UnityWebRequestModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[35];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.UnityWebRequestAudioModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[36];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.UnityWebRequestTextureModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[37];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.UnityWebRequestWWWModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[38];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.ClusterInputModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[39];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.ClusterRendererModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[40];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.FacebookModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[41];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.UNETModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[42];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.DirectorModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[43];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.TimelineModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[44];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.UnityAnalyticsModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[45];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.CrashReportingModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[46];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.PerformanceReportingModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[47];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.UnityConnectModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[48];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.WebModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[49];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.ARModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[50];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.SpatialTrackingModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[51];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.VRModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[52];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.CloudWebServicesModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[53];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.UIElementsModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[54];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.StyleSheetsModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[55];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.AudioModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[56];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.BaselibModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[57];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.GameCenterModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[58];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.GridModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[59];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.ImageConversionModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[60];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.InputModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[61];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.JSONSerializeModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[62];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.NScreenModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[63];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.ParticlesLegacyModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[64];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.Physics2DModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[65];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.ScreenCaptureModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[66];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.SpriteMaskModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[67];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.TerrainModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[68];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.TilemapModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[69];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.UmbraModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[70];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.VideoModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[71];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.WindModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[72];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine"), NULL);
	}
	{
		DebuggableAttribute_t3717120398 * tmp = (DebuggableAttribute_t3717120398 *)cache->attributes[73];
		DebuggableAttribute__ctor_m3553871532(tmp, 258LL, NULL);
	}
	{
		RuntimeCompatibilityAttribute_t38650954 * tmp = (RuntimeCompatibilityAttribute_t38650954 *)cache->attributes[74];
		RuntimeCompatibilityAttribute__ctor_m853651104(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m4154715430(tmp, true, NULL);
	}
}
static void GameCenterPlatform_t366411994_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void GameCenterPlatform_t366411994_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_Authenticate_m2072365377(CustomAttributesCache* cache)
{
	{
		GeneratedByOldBindingsGeneratorAttribute_t861731860 * tmp = (GeneratedByOldBindingsGeneratorAttribute_t861731860 *)cache->attributes[0];
		GeneratedByOldBindingsGeneratorAttribute__ctor_m1819602998(tmp, NULL);
	}
}
static void GameCenterPlatform_t366411994_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_Authenticated_m2873448009(CustomAttributesCache* cache)
{
	{
		GeneratedByOldBindingsGeneratorAttribute_t861731860 * tmp = (GeneratedByOldBindingsGeneratorAttribute_t861731860 *)cache->attributes[0];
		GeneratedByOldBindingsGeneratorAttribute__ctor_m1819602998(tmp, NULL);
	}
}
static void GameCenterPlatform_t366411994_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_UserName_m2866948064(CustomAttributesCache* cache)
{
	{
		GeneratedByOldBindingsGeneratorAttribute_t861731860 * tmp = (GeneratedByOldBindingsGeneratorAttribute_t861731860 *)cache->attributes[0];
		GeneratedByOldBindingsGeneratorAttribute__ctor_m1819602998(tmp, NULL);
	}
}
static void GameCenterPlatform_t366411994_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_UserID_m1391210441(CustomAttributesCache* cache)
{
	{
		GeneratedByOldBindingsGeneratorAttribute_t861731860 * tmp = (GeneratedByOldBindingsGeneratorAttribute_t861731860 *)cache->attributes[0];
		GeneratedByOldBindingsGeneratorAttribute__ctor_m1819602998(tmp, NULL);
	}
}
static void GameCenterPlatform_t366411994_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_Underage_m3153493531(CustomAttributesCache* cache)
{
	{
		GeneratedByOldBindingsGeneratorAttribute_t861731860 * tmp = (GeneratedByOldBindingsGeneratorAttribute_t861731860 *)cache->attributes[0];
		GeneratedByOldBindingsGeneratorAttribute__ctor_m1819602998(tmp, NULL);
	}
}
static void GameCenterPlatform_t366411994_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_UserImage_m3635042813(CustomAttributesCache* cache)
{
	{
		GeneratedByOldBindingsGeneratorAttribute_t861731860 * tmp = (GeneratedByOldBindingsGeneratorAttribute_t861731860 *)cache->attributes[0];
		GeneratedByOldBindingsGeneratorAttribute__ctor_m1819602998(tmp, NULL);
	}
}
static void GameCenterPlatform_t366411994_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_LoadFriends_m2096768347(CustomAttributesCache* cache)
{
	{
		GeneratedByOldBindingsGeneratorAttribute_t861731860 * tmp = (GeneratedByOldBindingsGeneratorAttribute_t861731860 *)cache->attributes[0];
		GeneratedByOldBindingsGeneratorAttribute__ctor_m1819602998(tmp, NULL);
	}
}
static void GameCenterPlatform_t366411994_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_LoadAchievementDescriptions_m4285687138(CustomAttributesCache* cache)
{
	{
		GeneratedByOldBindingsGeneratorAttribute_t861731860 * tmp = (GeneratedByOldBindingsGeneratorAttribute_t861731860 *)cache->attributes[0];
		GeneratedByOldBindingsGeneratorAttribute__ctor_m1819602998(tmp, NULL);
	}
}
static void GameCenterPlatform_t366411994_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_LoadAchievements_m1476802759(CustomAttributesCache* cache)
{
	{
		GeneratedByOldBindingsGeneratorAttribute_t861731860 * tmp = (GeneratedByOldBindingsGeneratorAttribute_t861731860 *)cache->attributes[0];
		GeneratedByOldBindingsGeneratorAttribute__ctor_m1819602998(tmp, NULL);
	}
}
static void GameCenterPlatform_t366411994_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_ReportProgress_m4247572756(CustomAttributesCache* cache)
{
	{
		GeneratedByOldBindingsGeneratorAttribute_t861731860 * tmp = (GeneratedByOldBindingsGeneratorAttribute_t861731860 *)cache->attributes[0];
		GeneratedByOldBindingsGeneratorAttribute__ctor_m1819602998(tmp, NULL);
	}
}
static void GameCenterPlatform_t366411994_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_ReportScore_m3392134640(CustomAttributesCache* cache)
{
	{
		GeneratedByOldBindingsGeneratorAttribute_t861731860 * tmp = (GeneratedByOldBindingsGeneratorAttribute_t861731860 *)cache->attributes[0];
		GeneratedByOldBindingsGeneratorAttribute__ctor_m1819602998(tmp, NULL);
	}
}
static void GameCenterPlatform_t366411994_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_LoadScores_m3934381776(CustomAttributesCache* cache)
{
	{
		GeneratedByOldBindingsGeneratorAttribute_t861731860 * tmp = (GeneratedByOldBindingsGeneratorAttribute_t861731860 *)cache->attributes[0];
		GeneratedByOldBindingsGeneratorAttribute__ctor_m1819602998(tmp, NULL);
	}
}
static void GameCenterPlatform_t366411994_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_ShowAchievementsUI_m2604671237(CustomAttributesCache* cache)
{
	{
		GeneratedByOldBindingsGeneratorAttribute_t861731860 * tmp = (GeneratedByOldBindingsGeneratorAttribute_t861731860 *)cache->attributes[0];
		GeneratedByOldBindingsGeneratorAttribute__ctor_m1819602998(tmp, NULL);
	}
}
static void GameCenterPlatform_t366411994_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_ShowLeaderboardUI_m2609988910(CustomAttributesCache* cache)
{
	{
		GeneratedByOldBindingsGeneratorAttribute_t861731860 * tmp = (GeneratedByOldBindingsGeneratorAttribute_t861731860 *)cache->attributes[0];
		GeneratedByOldBindingsGeneratorAttribute__ctor_m1819602998(tmp, NULL);
	}
}
static void GameCenterPlatform_t366411994_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_LoadUsers_m2376904235(CustomAttributesCache* cache)
{
	{
		GeneratedByOldBindingsGeneratorAttribute_t861731860 * tmp = (GeneratedByOldBindingsGeneratorAttribute_t861731860 *)cache->attributes[0];
		GeneratedByOldBindingsGeneratorAttribute__ctor_m1819602998(tmp, NULL);
	}
}
static void GameCenterPlatform_t366411994_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_ResetAllAchievements_m860406903(CustomAttributesCache* cache)
{
	{
		GeneratedByOldBindingsGeneratorAttribute_t861731860 * tmp = (GeneratedByOldBindingsGeneratorAttribute_t861731860 *)cache->attributes[0];
		GeneratedByOldBindingsGeneratorAttribute__ctor_m1819602998(tmp, NULL);
	}
}
static void GameCenterPlatform_t366411994_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_ShowDefaultAchievementBanner_m3807438363(CustomAttributesCache* cache)
{
	{
		GeneratedByOldBindingsGeneratorAttribute_t861731860 * tmp = (GeneratedByOldBindingsGeneratorAttribute_t861731860 *)cache->attributes[0];
		GeneratedByOldBindingsGeneratorAttribute__ctor_m1819602998(tmp, NULL);
	}
}
static void GameCenterPlatform_t366411994_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_ShowSpecificLeaderboardUI_m1324819955(CustomAttributesCache* cache)
{
	{
		GeneratedByOldBindingsGeneratorAttribute_t861731860 * tmp = (GeneratedByOldBindingsGeneratorAttribute_t861731860 *)cache->attributes[0];
		GeneratedByOldBindingsGeneratorAttribute__ctor_m1819602998(tmp, NULL);
	}
}
static void GameCenterPlatform_t366411994_CustomAttributesCacheGenerator_GameCenterPlatform_ClearAchievementDescriptions_m1201462552(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void GameCenterPlatform_t366411994_CustomAttributesCacheGenerator_GameCenterPlatform_SetAchievementDescription_m3265147368(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void GameCenterPlatform_t366411994_CustomAttributesCacheGenerator_GameCenterPlatform_SetAchievementDescriptionImage_m433180119(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void GameCenterPlatform_t366411994_CustomAttributesCacheGenerator_GameCenterPlatform_TriggerAchievementDescriptionCallback_m1368418878(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void GameCenterPlatform_t366411994_CustomAttributesCacheGenerator_GameCenterPlatform_AuthenticateCallbackWrapper_m3518532938(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void GameCenterPlatform_t366411994_CustomAttributesCacheGenerator_GameCenterPlatform_ClearFriends_m3997297748(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void GameCenterPlatform_t366411994_CustomAttributesCacheGenerator_GameCenterPlatform_SetFriends_m3156453279(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void GameCenterPlatform_t366411994_CustomAttributesCacheGenerator_GameCenterPlatform_SetFriendImage_m2565530221(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void GameCenterPlatform_t366411994_CustomAttributesCacheGenerator_GameCenterPlatform_TriggerFriendsCallbackWrapper_m3549927236(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void GameCenterPlatform_t366411994_CustomAttributesCacheGenerator_GameCenterPlatform_AchievementCallbackWrapper_m2393842948(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void GameCenterPlatform_t366411994_CustomAttributesCacheGenerator_GameCenterPlatform_ProgressCallbackWrapper_m1761357634(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void GameCenterPlatform_t366411994_CustomAttributesCacheGenerator_GameCenterPlatform_ScoreCallbackWrapper_m1017495084(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void GameCenterPlatform_t366411994_CustomAttributesCacheGenerator_GameCenterPlatform_ScoreLoaderCallbackWrapper_m2497892497(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void GameCenterPlatform_t366411994_CustomAttributesCacheGenerator_GameCenterPlatform_PopulateLocalUser_m3608849642(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void GameCenterPlatform_t366411994_CustomAttributesCacheGenerator_GameCenterPlatform_LeaderboardCallbackWrapper_m1512666017(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void GameCenterPlatform_t366411994_CustomAttributesCacheGenerator_GameCenterPlatform_ClearUsers_m694297429(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void GameCenterPlatform_t366411994_CustomAttributesCacheGenerator_GameCenterPlatform_SetUser_m1396993377(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void GameCenterPlatform_t366411994_CustomAttributesCacheGenerator_GameCenterPlatform_SetUserImage_m102480673(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void GameCenterPlatform_t366411994_CustomAttributesCacheGenerator_GameCenterPlatform_TriggerUsersCallbackWrapper_m741337649(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void GameCenterPlatform_t366411994_CustomAttributesCacheGenerator_GameCenterPlatform_SafeSetUserImage_m4217127318(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void GameCenterPlatform_t366411994_CustomAttributesCacheGenerator_GameCenterPlatform_TriggerResetAchievementCallback_m1901797103(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void U3CUnityEngine_SocialPlatforms_ISocialPlatform_AuthenticateU3Ec__AnonStorey0_t3375275809_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
}
static void GcLeaderboard_t617530396_CustomAttributesCacheGenerator_GcLeaderboard_Internal_LoadScores_m30788333(CustomAttributesCache* cache)
{
	{
		GeneratedByOldBindingsGeneratorAttribute_t861731860 * tmp = (GeneratedByOldBindingsGeneratorAttribute_t861731860 *)cache->attributes[0];
		GeneratedByOldBindingsGeneratorAttribute__ctor_m1819602998(tmp, NULL);
	}
}
static void GcLeaderboard_t617530396_CustomAttributesCacheGenerator_GcLeaderboard_Loading_m2486631462(CustomAttributesCache* cache)
{
	{
		GeneratedByOldBindingsGeneratorAttribute_t861731860 * tmp = (GeneratedByOldBindingsGeneratorAttribute_t861731860 *)cache->attributes[0];
		GeneratedByOldBindingsGeneratorAttribute__ctor_m1819602998(tmp, NULL);
	}
}
static void GcLeaderboard_t617530396_CustomAttributesCacheGenerator_GcLeaderboard_Dispose_m4178548762(CustomAttributesCache* cache)
{
	{
		GeneratedByOldBindingsGeneratorAttribute_t861731860 * tmp = (GeneratedByOldBindingsGeneratorAttribute_t861731860 *)cache->attributes[0];
		GeneratedByOldBindingsGeneratorAttribute__ctor_m1819602998(tmp, NULL);
	}
	{
		ThreadAndSerializationSafeAttribute_t566760631 * tmp = (ThreadAndSerializationSafeAttribute_t566760631 *)cache->attributes[1];
		ThreadAndSerializationSafeAttribute__ctor_m211248632(tmp, NULL);
	}
}
static void GcUserProfileData_t3262991188_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void GcAchievementDescriptionData_t2898970914_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void GcAchievementData_t3167706508_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void GcScoreData_t3955401213_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void Achievement_t2517894267_CustomAttributesCacheGenerator_U3CidU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
	{
		DebuggerBrowsableAttribute_t1527328379 * tmp = (DebuggerBrowsableAttribute_t1527328379 *)cache->attributes[1];
		DebuggerBrowsableAttribute__ctor_m1784875164(tmp, 0LL, NULL);
	}
}
static void Achievement_t2517894267_CustomAttributesCacheGenerator_U3CpercentCompletedU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		DebuggerBrowsableAttribute_t1527328379 * tmp = (DebuggerBrowsableAttribute_t1527328379 *)cache->attributes[0];
		DebuggerBrowsableAttribute__ctor_m1784875164(tmp, 0LL, NULL);
	}
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[1];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
}
static void Achievement_t2517894267_CustomAttributesCacheGenerator_Achievement_get_id_m352935039(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
}
static void Achievement_t2517894267_CustomAttributesCacheGenerator_Achievement_set_id_m2592364518(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
}
static void Achievement_t2517894267_CustomAttributesCacheGenerator_Achievement_get_percentCompleted_m3640585090(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
}
static void Achievement_t2517894267_CustomAttributesCacheGenerator_Achievement_set_percentCompleted_m1848274796(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
}
static void AchievementDescription_t130800866_CustomAttributesCacheGenerator_U3CidU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
	{
		DebuggerBrowsableAttribute_t1527328379 * tmp = (DebuggerBrowsableAttribute_t1527328379 *)cache->attributes[1];
		DebuggerBrowsableAttribute__ctor_m1784875164(tmp, 0LL, NULL);
	}
}
static void AchievementDescription_t130800866_CustomAttributesCacheGenerator_AchievementDescription_get_id_m1271036810(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
}
static void AchievementDescription_t130800866_CustomAttributesCacheGenerator_AchievementDescription_set_id_m3879160604(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
}
static void Score_t1135675058_CustomAttributesCacheGenerator_U3CleaderboardIDU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		DebuggerBrowsableAttribute_t1527328379 * tmp = (DebuggerBrowsableAttribute_t1527328379 *)cache->attributes[0];
		DebuggerBrowsableAttribute__ctor_m1784875164(tmp, 0LL, NULL);
	}
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[1];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
}
static void Score_t1135675058_CustomAttributesCacheGenerator_U3CvalueU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		DebuggerBrowsableAttribute_t1527328379 * tmp = (DebuggerBrowsableAttribute_t1527328379 *)cache->attributes[0];
		DebuggerBrowsableAttribute__ctor_m1784875164(tmp, 0LL, NULL);
	}
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[1];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
}
static void Score_t1135675058_CustomAttributesCacheGenerator_Score_get_leaderboardID_m274030957(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
}
static void Score_t1135675058_CustomAttributesCacheGenerator_Score_set_leaderboardID_m2942345544(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
}
static void Score_t1135675058_CustomAttributesCacheGenerator_Score_get_value_m3908827736(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
}
static void Score_t1135675058_CustomAttributesCacheGenerator_Score_set_value_m2016376629(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
}
static void Leaderboard_t3421936738_CustomAttributesCacheGenerator_U3CidU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		DebuggerBrowsableAttribute_t1527328379 * tmp = (DebuggerBrowsableAttribute_t1527328379 *)cache->attributes[0];
		DebuggerBrowsableAttribute__ctor_m1784875164(tmp, 0LL, NULL);
	}
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[1];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
}
static void Leaderboard_t3421936738_CustomAttributesCacheGenerator_U3CuserScopeU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
	{
		DebuggerBrowsableAttribute_t1527328379 * tmp = (DebuggerBrowsableAttribute_t1527328379 *)cache->attributes[1];
		DebuggerBrowsableAttribute__ctor_m1784875164(tmp, 0LL, NULL);
	}
}
static void Leaderboard_t3421936738_CustomAttributesCacheGenerator_U3CrangeU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
	{
		DebuggerBrowsableAttribute_t1527328379 * tmp = (DebuggerBrowsableAttribute_t1527328379 *)cache->attributes[1];
		DebuggerBrowsableAttribute__ctor_m1784875164(tmp, 0LL, NULL);
	}
}
static void Leaderboard_t3421936738_CustomAttributesCacheGenerator_U3CtimeScopeU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
	{
		DebuggerBrowsableAttribute_t1527328379 * tmp = (DebuggerBrowsableAttribute_t1527328379 *)cache->attributes[1];
		DebuggerBrowsableAttribute__ctor_m1784875164(tmp, 0LL, NULL);
	}
}
static void Leaderboard_t3421936738_CustomAttributesCacheGenerator_Leaderboard_get_id_m523269624(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
}
static void Leaderboard_t3421936738_CustomAttributesCacheGenerator_Leaderboard_set_id_m1452185904(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
}
static void Leaderboard_t3421936738_CustomAttributesCacheGenerator_Leaderboard_get_userScope_m3651500032(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
}
static void Leaderboard_t3421936738_CustomAttributesCacheGenerator_Leaderboard_set_userScope_m1479044752(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
}
static void Leaderboard_t3421936738_CustomAttributesCacheGenerator_Leaderboard_get_range_m4014757670(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
}
static void Leaderboard_t3421936738_CustomAttributesCacheGenerator_Leaderboard_set_range_m3228718273(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
}
static void Leaderboard_t3421936738_CustomAttributesCacheGenerator_Leaderboard_get_timeScope_m2150671680(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
}
static void Leaderboard_t3421936738_CustomAttributesCacheGenerator_Leaderboard_set_timeScope_m1836561867(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
}
static void g_UnityEngine_TilemapModule_Assembly_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		RuntimeCompatibilityAttribute_t38650954 * tmp = (RuntimeCompatibilityAttribute_t38650954 *)cache->attributes[0];
		RuntimeCompatibilityAttribute__ctor_m853651104(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m4154715430(tmp, true, NULL);
	}
	{
		DebuggableAttribute_t3717120398 * tmp = (DebuggableAttribute_t3717120398 *)cache->attributes[1];
		DebuggableAttribute__ctor_m3553871532(tmp, 258LL, NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[2];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[3];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.WindModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[4];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.VideoModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[5];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.UmbraModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[6];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.TilemapModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[7];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.TerrainModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[8];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.GameCenterModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[9];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.SpriteMaskModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[10];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.ClusterInputModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[11];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.ScreenCaptureModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[12];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.Physics2DModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[13];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.ParticlesLegacyModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[14];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.NScreenModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[15];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.JSONSerializeModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[16];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.InputModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[17];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.ImageConversionModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[18];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.GridModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[19];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.PerformanceReportingModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[20];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.UIElementsModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[21];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.CloudWebServicesModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[22];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.VRModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[23];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.SpatialTrackingModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[24];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.ARModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[25];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.WebModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[26];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.TerrainPhysics"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[27];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.Terrain"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[28];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.Physics"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[29];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.StyleSheetsModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[30];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.CrashReportingModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[31];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.UnityAnalyticsModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[32];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.TimelineModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[33];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.DirectorModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[34];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.UNETModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[35];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.FacebookModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[36];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.ClusterRendererModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[37];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.UnityConnectModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[38];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.BaselibModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[39];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.AudioModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[40];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("Unity.Automation"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[41];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.UnityWebRequestWWWModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[42];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.UnityWebRequestTextureModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[43];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.UnityWebRequestAudioModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[44];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.UnityWebRequestModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[45];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.IMGUIModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[46];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.TerrainPhysicsModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[47];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.UIModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[48];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.AnimationModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[49];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.AIModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[50];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.ClothModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[51];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.VehiclesModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[52];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.PhysicsModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[53];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.ParticleSystemModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[54];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.AccessibilityModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[55];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.SpatialTracking"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[56];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("GoogleAR.UnityNative"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[57];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.TextRenderingModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[58];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.TestRunner"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[59];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("Unity.RuntimeTests.Framework"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[60];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("Unity.RuntimeTests"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[61];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("Unity.IntegrationTests.Framework"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[62];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("Unity.IntegrationTests.Timeline"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[63];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("Unity.IntegrationTests.UnityAnalytics"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[64];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("Unity.IntegrationTests"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[65];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("Unity.DeploymentTests.Services"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[66];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.Networking"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[67];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("Unity.RuntimeTests.Framework.Tests"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[68];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.GoogleAudioSpatializer"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[69];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.Timeline"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[70];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.Purchasing"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[71];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.Advertisements"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[72];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.Analytics"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[73];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.Cloud.Service"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[74];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.Cloud"), NULL);
	}
}
static void TileFlags_t2545855820_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		FlagsAttribute_t388727259 * tmp = (FlagsAttribute_t388727259 *)cache->attributes[0];
		FlagsAttribute__ctor_m3834565173(tmp, NULL);
	}
}
static void Tile_t160380049_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void Tile_t160380049_CustomAttributesCacheGenerator_m_Color(CustomAttributesCache* cache)
{
	{
		SerializeField_t1393754034 * tmp = (SerializeField_t1393754034 *)cache->attributes[0];
		SerializeField__ctor_m3401225370(tmp, NULL);
	}
}
static void Tile_t160380049_CustomAttributesCacheGenerator_m_Transform(CustomAttributesCache* cache)
{
	{
		SerializeField_t1393754034 * tmp = (SerializeField_t1393754034 *)cache->attributes[0];
		SerializeField__ctor_m3401225370(tmp, NULL);
	}
}
static void Tile_t160380049_CustomAttributesCacheGenerator_m_Flags(CustomAttributesCache* cache)
{
	{
		SerializeField_t1393754034 * tmp = (SerializeField_t1393754034 *)cache->attributes[0];
		SerializeField__ctor_m3401225370(tmp, NULL);
	}
}
static void Tile_t160380049_CustomAttributesCacheGenerator_m_ColliderType(CustomAttributesCache* cache)
{
	{
		SerializeField_t1393754034 * tmp = (SerializeField_t1393754034 *)cache->attributes[0];
		SerializeField__ctor_m3401225370(tmp, NULL);
	}
}
static void TileBase_t2845917970_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void g_UnityEngine_UnityAnalyticsModule_Assembly_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[0];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.CloudWebServicesModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[1];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.VRModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[2];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.SpatialTrackingModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[3];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.ARModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[4];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.AudioModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[5];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.JSONSerializeModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[6];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.InputModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[7];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.ImageConversionModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[8];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.GridModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[9];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.GameCenterModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[10];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.UIElementsModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[11];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.SpriteMaskModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[12];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.TerrainModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[13];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.TilemapModule"), NULL);
	}
	{
		RuntimeCompatibilityAttribute_t38650954 * tmp = (RuntimeCompatibilityAttribute_t38650954 *)cache->attributes[14];
		RuntimeCompatibilityAttribute__ctor_m853651104(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m4154715430(tmp, true, NULL);
	}
	{
		DebuggableAttribute_t3717120398 * tmp = (DebuggableAttribute_t3717120398 *)cache->attributes[15];
		DebuggableAttribute__ctor_m3553871532(tmp, 258LL, NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[16];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[17];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.WindModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[18];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.VideoModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[19];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.UmbraModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[20];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.NScreenModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[21];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.StyleSheetsModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[22];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.BaselibModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[23];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("GoogleAR.UnityNative"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[24];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("Unity.RuntimeTests.Framework.Tests"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[25];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("Unity.RuntimeTests.Framework"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[26];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("Unity.RuntimeTests"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[27];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("Unity.IntegrationTests.Framework"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[28];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("Unity.IntegrationTests.Timeline"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[29];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.TestRunner"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[30];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("Unity.Automation"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[31];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("Unity.DeploymentTests.Services"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[32];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("Unity.IntegrationTests"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[33];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("Unity.IntegrationTests.UnityAnalytics"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[34];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.GoogleAudioSpatializer"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[35];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.SpatialTracking"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[36];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.Timeline"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[37];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.Purchasing"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[38];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.Advertisements"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[39];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.Analytics"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[40];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.Cloud.Service"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[41];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.Cloud"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[42];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.Networking"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[43];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.TerrainPhysics"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[44];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.Terrain"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[45];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.Physics"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[46];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.ScreenCaptureModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[47];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.PerformanceReportingModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[48];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.CrashReportingModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[49];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.UnityAnalyticsModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[50];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.TimelineModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[51];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.DirectorModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[52];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.UnityWebRequestAudioModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[53];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.UnityConnectModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[54];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.UnityWebRequestTextureModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[55];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.UnityWebRequestWWWModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[56];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.ClusterInputModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[57];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.ClusterRendererModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[58];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.FacebookModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[59];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.UNETModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[60];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.AccessibilityModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[61];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.UIModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[62];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.TextRenderingModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[63];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.AnimationModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[64];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.WebModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[65];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.ParticlesLegacyModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[66];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.Physics2DModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[67];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.TerrainPhysicsModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[68];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.ParticleSystemModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[69];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.PhysicsModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[70];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.VehiclesModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[71];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.ClothModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[72];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.AIModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[73];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.UnityWebRequestModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[74];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.IMGUIModule"), NULL);
	}
}
static void CustomEventData_t868048441_CustomAttributesCacheGenerator_CustomEventData_InternalCreate_m782104979(CustomAttributesCache* cache)
{
	{
		GeneratedByOldBindingsGeneratorAttribute_t861731860 * tmp = (GeneratedByOldBindingsGeneratorAttribute_t861731860 *)cache->attributes[0];
		GeneratedByOldBindingsGeneratorAttribute__ctor_m1819602998(tmp, NULL);
	}
}
static void CustomEventData_t868048441_CustomAttributesCacheGenerator_CustomEventData_InternalDestroy_m998346656(CustomAttributesCache* cache)
{
	{
		ThreadAndSerializationSafeAttribute_t566760631 * tmp = (ThreadAndSerializationSafeAttribute_t566760631 *)cache->attributes[0];
		ThreadAndSerializationSafeAttribute__ctor_m211248632(tmp, NULL);
	}
	{
		GeneratedByOldBindingsGeneratorAttribute_t861731860 * tmp = (GeneratedByOldBindingsGeneratorAttribute_t861731860 *)cache->attributes[1];
		GeneratedByOldBindingsGeneratorAttribute__ctor_m1819602998(tmp, NULL);
	}
}
static void CustomEventData_t868048441_CustomAttributesCacheGenerator_CustomEventData_AddString_m2143902135(CustomAttributesCache* cache)
{
	{
		GeneratedByOldBindingsGeneratorAttribute_t861731860 * tmp = (GeneratedByOldBindingsGeneratorAttribute_t861731860 *)cache->attributes[0];
		GeneratedByOldBindingsGeneratorAttribute__ctor_m1819602998(tmp, NULL);
	}
}
static void CustomEventData_t868048441_CustomAttributesCacheGenerator_CustomEventData_AddBool_m436037313(CustomAttributesCache* cache)
{
	{
		GeneratedByOldBindingsGeneratorAttribute_t861731860 * tmp = (GeneratedByOldBindingsGeneratorAttribute_t861731860 *)cache->attributes[0];
		GeneratedByOldBindingsGeneratorAttribute__ctor_m1819602998(tmp, NULL);
	}
}
static void CustomEventData_t868048441_CustomAttributesCacheGenerator_CustomEventData_AddChar_m2437065113(CustomAttributesCache* cache)
{
	{
		GeneratedByOldBindingsGeneratorAttribute_t861731860 * tmp = (GeneratedByOldBindingsGeneratorAttribute_t861731860 *)cache->attributes[0];
		GeneratedByOldBindingsGeneratorAttribute__ctor_m1819602998(tmp, NULL);
	}
}
static void CustomEventData_t868048441_CustomAttributesCacheGenerator_CustomEventData_AddByte_m210020965(CustomAttributesCache* cache)
{
	{
		GeneratedByOldBindingsGeneratorAttribute_t861731860 * tmp = (GeneratedByOldBindingsGeneratorAttribute_t861731860 *)cache->attributes[0];
		GeneratedByOldBindingsGeneratorAttribute__ctor_m1819602998(tmp, NULL);
	}
}
static void CustomEventData_t868048441_CustomAttributesCacheGenerator_CustomEventData_AddSByte_m3889282772(CustomAttributesCache* cache)
{
	{
		GeneratedByOldBindingsGeneratorAttribute_t861731860 * tmp = (GeneratedByOldBindingsGeneratorAttribute_t861731860 *)cache->attributes[0];
		GeneratedByOldBindingsGeneratorAttribute__ctor_m1819602998(tmp, NULL);
	}
}
static void CustomEventData_t868048441_CustomAttributesCacheGenerator_CustomEventData_AddInt16_m1607163544(CustomAttributesCache* cache)
{
	{
		GeneratedByOldBindingsGeneratorAttribute_t861731860 * tmp = (GeneratedByOldBindingsGeneratorAttribute_t861731860 *)cache->attributes[0];
		GeneratedByOldBindingsGeneratorAttribute__ctor_m1819602998(tmp, NULL);
	}
}
static void CustomEventData_t868048441_CustomAttributesCacheGenerator_CustomEventData_AddUInt16_m3424486269(CustomAttributesCache* cache)
{
	{
		GeneratedByOldBindingsGeneratorAttribute_t861731860 * tmp = (GeneratedByOldBindingsGeneratorAttribute_t861731860 *)cache->attributes[0];
		GeneratedByOldBindingsGeneratorAttribute__ctor_m1819602998(tmp, NULL);
	}
}
static void CustomEventData_t868048441_CustomAttributesCacheGenerator_CustomEventData_AddInt32_m2136872868(CustomAttributesCache* cache)
{
	{
		GeneratedByOldBindingsGeneratorAttribute_t861731860 * tmp = (GeneratedByOldBindingsGeneratorAttribute_t861731860 *)cache->attributes[0];
		GeneratedByOldBindingsGeneratorAttribute__ctor_m1819602998(tmp, NULL);
	}
}
static void CustomEventData_t868048441_CustomAttributesCacheGenerator_CustomEventData_AddUInt32_m911556407(CustomAttributesCache* cache)
{
	{
		GeneratedByOldBindingsGeneratorAttribute_t861731860 * tmp = (GeneratedByOldBindingsGeneratorAttribute_t861731860 *)cache->attributes[0];
		GeneratedByOldBindingsGeneratorAttribute__ctor_m1819602998(tmp, NULL);
	}
}
static void CustomEventData_t868048441_CustomAttributesCacheGenerator_CustomEventData_AddInt64_m1450877831(CustomAttributesCache* cache)
{
	{
		GeneratedByOldBindingsGeneratorAttribute_t861731860 * tmp = (GeneratedByOldBindingsGeneratorAttribute_t861731860 *)cache->attributes[0];
		GeneratedByOldBindingsGeneratorAttribute__ctor_m1819602998(tmp, NULL);
	}
}
static void CustomEventData_t868048441_CustomAttributesCacheGenerator_CustomEventData_AddUInt64_m33970678(CustomAttributesCache* cache)
{
	{
		GeneratedByOldBindingsGeneratorAttribute_t861731860 * tmp = (GeneratedByOldBindingsGeneratorAttribute_t861731860 *)cache->attributes[0];
		GeneratedByOldBindingsGeneratorAttribute__ctor_m1819602998(tmp, NULL);
	}
}
static void CustomEventData_t868048441_CustomAttributesCacheGenerator_CustomEventData_AddDouble_m1838257319(CustomAttributesCache* cache)
{
	{
		GeneratedByOldBindingsGeneratorAttribute_t861731860 * tmp = (GeneratedByOldBindingsGeneratorAttribute_t861731860 *)cache->attributes[0];
		GeneratedByOldBindingsGeneratorAttribute__ctor_m1819602998(tmp, NULL);
	}
}
static void UnityAnalyticsHandler_t1984834895_CustomAttributesCacheGenerator_UnityAnalyticsHandler_InternalCreate_m3939206337(CustomAttributesCache* cache)
{
	{
		GeneratedByOldBindingsGeneratorAttribute_t861731860 * tmp = (GeneratedByOldBindingsGeneratorAttribute_t861731860 *)cache->attributes[0];
		GeneratedByOldBindingsGeneratorAttribute__ctor_m1819602998(tmp, NULL);
	}
}
static void UnityAnalyticsHandler_t1984834895_CustomAttributesCacheGenerator_UnityAnalyticsHandler_InternalDestroy_m316722488(CustomAttributesCache* cache)
{
	{
		GeneratedByOldBindingsGeneratorAttribute_t861731860 * tmp = (GeneratedByOldBindingsGeneratorAttribute_t861731860 *)cache->attributes[0];
		GeneratedByOldBindingsGeneratorAttribute__ctor_m1819602998(tmp, NULL);
	}
	{
		ThreadAndSerializationSafeAttribute_t566760631 * tmp = (ThreadAndSerializationSafeAttribute_t566760631 *)cache->attributes[1];
		ThreadAndSerializationSafeAttribute__ctor_m211248632(tmp, NULL);
	}
}
static void UnityAnalyticsHandler_t1984834895_CustomAttributesCacheGenerator_UnityAnalyticsHandler_SendCustomEventName_m2761615078(CustomAttributesCache* cache)
{
	{
		GeneratedByOldBindingsGeneratorAttribute_t861731860 * tmp = (GeneratedByOldBindingsGeneratorAttribute_t861731860 *)cache->attributes[0];
		GeneratedByOldBindingsGeneratorAttribute__ctor_m1819602998(tmp, NULL);
	}
}
static void UnityAnalyticsHandler_t1984834895_CustomAttributesCacheGenerator_UnityAnalyticsHandler_SendCustomEvent_m3141620171(CustomAttributesCache* cache)
{
	{
		GeneratedByOldBindingsGeneratorAttribute_t861731860 * tmp = (GeneratedByOldBindingsGeneratorAttribute_t861731860 *)cache->attributes[0];
		GeneratedByOldBindingsGeneratorAttribute__ctor_m1819602998(tmp, NULL);
	}
}
static void g_UnityEngine_UnityConnectModule_Assembly_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[0];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.JSONSerializeModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[1];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.ARModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[2];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.WebModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[3];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.UnityConnectModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[4];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.PerformanceReportingModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[5];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.CloudWebServicesModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[6];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.ImageConversionModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[7];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.GridModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[8];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.GameCenterModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[9];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.SpatialTrackingModule"), NULL);
	}
	{
		RuntimeCompatibilityAttribute_t38650954 * tmp = (RuntimeCompatibilityAttribute_t38650954 *)cache->attributes[10];
		RuntimeCompatibilityAttribute__ctor_m853651104(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m4154715430(tmp, true, NULL);
	}
	{
		DebuggableAttribute_t3717120398 * tmp = (DebuggableAttribute_t3717120398 *)cache->attributes[11];
		DebuggableAttribute__ctor_m3553871532(tmp, 258LL, NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[12];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[13];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.WindModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[14];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.VideoModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[15];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.UmbraModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[16];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.TilemapModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[17];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.InputModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[18];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.VRModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[19];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.BaselibModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[20];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.Physics"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[21];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.Advertisements"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[22];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("Unity.IntegrationTests"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[23];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("Unity.DeploymentTests.Services"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[24];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("Unity.Automation"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[25];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.TestRunner"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[26];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.GoogleAudioSpatializer"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[27];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.Timeline"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[28];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.Purchasing"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[29];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.Terrain"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[30];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.AudioModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[31];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.StyleSheetsModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[32];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.UIElementsModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[33];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("Unity.IntegrationTests.UnityAnalytics"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[34];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.Analytics"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[35];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.Cloud.Service"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[36];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.Cloud"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[37];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.Networking"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[38];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.TerrainPhysics"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[39];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.TerrainModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[40];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.UNETModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[41];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.FacebookModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[42];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.ClusterRendererModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[43];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.ClusterInputModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[44];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.UnityWebRequestWWWModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[45];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.TextRenderingModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[46];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("GoogleAR.UnityNative"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[47];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("Unity.RuntimeTests.Framework.Tests"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[48];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.DirectorModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[49];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.UnityWebRequestAudioModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[50];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.UnityWebRequestModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[51];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.IMGUIModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[52];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.TerrainPhysicsModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[53];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.UIModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[54];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.UnityWebRequestTextureModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[55];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.UnityAnalyticsModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[56];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.TimelineModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[57];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("Unity.RuntimeTests.Framework"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[58];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.ParticleSystemModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[59];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.AccessibilityModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[60];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.CrashReportingModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[61];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.SpriteMaskModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[62];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.ScreenCaptureModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[63];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.Physics2DModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[64];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.NScreenModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[65];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.ParticlesLegacyModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[66];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.PhysicsModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[67];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("Unity.RuntimeTests"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[68];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("Unity.IntegrationTests.Framework"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[69];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("Unity.IntegrationTests.Timeline"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[70];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.SpatialTracking"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[71];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.VehiclesModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[72];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.ClothModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[73];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.AIModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[74];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.AnimationModule"), NULL);
	}
}
static void RemoteSettings_t2754304463_CustomAttributesCacheGenerator_Updated(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
	{
		DebuggerBrowsableAttribute_t1527328379 * tmp = (DebuggerBrowsableAttribute_t1527328379 *)cache->attributes[1];
		DebuggerBrowsableAttribute__ctor_m1784875164(tmp, 0LL, NULL);
	}
}
static void RemoteSettings_t2754304463_CustomAttributesCacheGenerator_RemoteSettings_CallOnUpdate_m837272010(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void AnalyticsSessionState_t95894832_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void AnalyticsSessionInfo_t1065651873_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void AnalyticsSessionInfo_t1065651873_CustomAttributesCacheGenerator_sessionStateChanged(CustomAttributesCache* cache)
{
	{
		DebuggerBrowsableAttribute_t1527328379 * tmp = (DebuggerBrowsableAttribute_t1527328379 *)cache->attributes[0];
		DebuggerBrowsableAttribute__ctor_m1784875164(tmp, 0LL, NULL);
	}
	{
		CompilerGeneratedAttribute_t3076080542 * tmp = (CompilerGeneratedAttribute_t3076080542 *)cache->attributes[1];
		CompilerGeneratedAttribute__ctor_m903969540(tmp, NULL);
	}
}
static void AnalyticsSessionInfo_t1065651873_CustomAttributesCacheGenerator_AnalyticsSessionInfo_CallSessionStateChanged_m2908054006(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void g_UnityEngine_UnityWebRequestModule_Assembly_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[0];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.PhysicsModule"), NULL);
	}
	{
		DebuggableAttribute_t3717120398 * tmp = (DebuggableAttribute_t3717120398 *)cache->attributes[1];
		DebuggableAttribute__ctor_m3553871532(tmp, 258LL, NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[2];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[3];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.WindModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[4];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.VideoModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[5];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.UmbraModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[6];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.TilemapModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[7];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.TerrainModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[8];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.SpriteMaskModule"), NULL);
	}
	{
		RuntimeCompatibilityAttribute_t38650954 * tmp = (RuntimeCompatibilityAttribute_t38650954 *)cache->attributes[9];
		RuntimeCompatibilityAttribute__ctor_m853651104(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m4154715430(tmp, true, NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[10];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.ParticlesLegacyModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[11];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.NScreenModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[12];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.JSONSerializeModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[13];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.InputModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[14];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.ImageConversionModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[15];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.GridModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[16];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.GameCenterModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[17];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.ScreenCaptureModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[18];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.BaselibModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[19];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.StyleSheetsModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[20];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.UIElementsModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[21];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.CloudWebServicesModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[22];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.VRModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[23];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.SpatialTrackingModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[24];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.ARModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[25];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.WebModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[26];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.UnityConnectModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[27];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.AudioModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[28];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.CrashReportingModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[29];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.UnityAnalyticsModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[30];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.TimelineModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[31];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.DirectorModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[32];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.UNETModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[33];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.FacebookModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[34];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.ClusterRendererModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[35];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.ClusterInputModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[36];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.PerformanceReportingModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[37];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.Physics2DModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[38];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.UnityWebRequestModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[39];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.IMGUIModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[40];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.TerrainPhysicsModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[41];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.UIModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[42];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.TextRenderingModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[43];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.AnimationModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[44];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.AIModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[45];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("Unity.RuntimeTests"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[46];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.UnityWebRequestAudioModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[47];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.ParticleSystemModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[48];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.AccessibilityModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[49];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.SpatialTracking"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[50];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("GoogleAR.UnityNative"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[51];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("Unity.RuntimeTests.Framework.Tests"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[52];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("Unity.RuntimeTests.Framework"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[53];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.ClothModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[54];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.UnityWebRequestTextureModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[55];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.Advertisements"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[56];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("Unity.IntegrationTests.UnityAnalytics"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[57];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("Unity.IntegrationTests"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[58];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("Unity.DeploymentTests.Services"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[59];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("Unity.Automation"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[60];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.TestRunner"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[61];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.GoogleAudioSpatializer"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[62];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.Timeline"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[63];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.VehiclesModule"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[64];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("Unity.IntegrationTests.Timeline"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[65];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.Analytics"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[66];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.Cloud.Service"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[67];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.Cloud"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[68];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.Networking"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[69];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.TerrainPhysics"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[70];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.Terrain"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[71];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.Physics"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[72];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.Purchasing"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[73];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("Unity.IntegrationTests.Framework"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[74];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.UnityWebRequestWWWModule"), NULL);
	}
}
static void WebRequestUtils_t669258994_CustomAttributesCacheGenerator_WebRequestUtils_RedirectTo_m1499419735(CustomAttributesCache* cache)
{
	{
		RequiredByNativeCodeAttribute_t3603024077 * tmp = (RequiredByNativeCodeAttribute_t3603024077 *)cache->attributes[0];
		RequiredByNativeCodeAttribute__ctor_m3493315205(tmp, NULL);
	}
}
static void g_UnityEngine_Assembly_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DebuggableAttribute_t3717120398 * tmp = (DebuggableAttribute_t3717120398 *)cache->attributes[0];
		DebuggableAttribute__ctor_m3553871532(tmp, 258LL, NULL);
	}
	{
		RuntimeCompatibilityAttribute_t38650954 * tmp = (RuntimeCompatibilityAttribute_t38650954 *)cache->attributes[1];
		RuntimeCompatibilityAttribute__ctor_m853651104(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m4154715430(tmp, true, NULL);
	}
}
static void g_UnityEngine_Analytics_Assembly_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[0];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("Unity.Automation"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[1];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("Unity.IntegrationTests"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[2];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("Unity.IntegrationTests.Framework"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[3];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("Unity.RuntimeTests"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[4];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("Unity.RuntimeTests.Framework"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[5];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("Unity.RuntimeTests.Framework.Tests"), NULL);
	}
	{
		InternalsVisibleToAttribute_t2168674587 * tmp = (InternalsVisibleToAttribute_t2168674587 *)cache->attributes[6];
		InternalsVisibleToAttribute__ctor_m2209294008(tmp, il2cpp_codegen_string_new_wrapper("UnityEditor.Analytics"), NULL);
	}
	{
		DebuggableAttribute_t3717120398 * tmp = (DebuggableAttribute_t3717120398 *)cache->attributes[7];
		DebuggableAttribute__ctor_m3553871532(tmp, 258LL, NULL);
	}
	{
		RuntimeCompatibilityAttribute_t38650954 * tmp = (RuntimeCompatibilityAttribute_t38650954 *)cache->attributes[8];
		RuntimeCompatibilityAttribute__ctor_m853651104(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m4154715430(tmp, true, NULL);
	}
}
static void AnalyticsTracker_t1058519078_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3529181215 * tmp = (AddComponentMenu_t3529181215 *)cache->attributes[0];
		AddComponentMenu__ctor_m3629709883(tmp, il2cpp_codegen_string_new_wrapper("Analytics/AnalyticsTracker"), NULL);
	}
}
static void AnalyticsTracker_t1058519078_CustomAttributesCacheGenerator_m_EventName(CustomAttributesCache* cache)
{
	{
		SerializeField_t1393754034 * tmp = (SerializeField_t1393754034 *)cache->attributes[0];
		SerializeField__ctor_m3401225370(tmp, NULL);
	}
}
static void AnalyticsTracker_t1058519078_CustomAttributesCacheGenerator_m_TrackableProperty(CustomAttributesCache* cache)
{
	{
		SerializeField_t1393754034 * tmp = (SerializeField_t1393754034 *)cache->attributes[0];
		SerializeField__ctor_m3401225370(tmp, NULL);
	}
}
static void AnalyticsTracker_t1058519078_CustomAttributesCacheGenerator_m_Trigger(CustomAttributesCache* cache)
{
	{
		SerializeField_t1393754034 * tmp = (SerializeField_t1393754034 *)cache->attributes[0];
		SerializeField__ctor_m3401225370(tmp, NULL);
	}
}
static void TrackableProperty_t52271239_CustomAttributesCacheGenerator_m_Fields(CustomAttributesCache* cache)
{
	{
		SerializeField_t1393754034 * tmp = (SerializeField_t1393754034 *)cache->attributes[0];
		SerializeField__ctor_m3401225370(tmp, NULL);
	}
}
static void FieldWithTarget_t1883299082_CustomAttributesCacheGenerator_m_ParamName(CustomAttributesCache* cache)
{
	{
		SerializeField_t1393754034 * tmp = (SerializeField_t1393754034 *)cache->attributes[0];
		SerializeField__ctor_m3401225370(tmp, NULL);
	}
}
static void FieldWithTarget_t1883299082_CustomAttributesCacheGenerator_m_Target(CustomAttributesCache* cache)
{
	{
		SerializeField_t1393754034 * tmp = (SerializeField_t1393754034 *)cache->attributes[0];
		SerializeField__ctor_m3401225370(tmp, NULL);
	}
}
static void FieldWithTarget_t1883299082_CustomAttributesCacheGenerator_m_FieldPath(CustomAttributesCache* cache)
{
	{
		SerializeField_t1393754034 * tmp = (SerializeField_t1393754034 *)cache->attributes[0];
		SerializeField__ctor_m3401225370(tmp, NULL);
	}
}
static void FieldWithTarget_t1883299082_CustomAttributesCacheGenerator_m_TypeString(CustomAttributesCache* cache)
{
	{
		SerializeField_t1393754034 * tmp = (SerializeField_t1393754034 *)cache->attributes[0];
		SerializeField__ctor_m3401225370(tmp, NULL);
	}
}
static void FieldWithTarget_t1883299082_CustomAttributesCacheGenerator_m_DoStatic(CustomAttributesCache* cache)
{
	{
		SerializeField_t1393754034 * tmp = (SerializeField_t1393754034 *)cache->attributes[0];
		SerializeField__ctor_m3401225370(tmp, NULL);
	}
}
static void FieldWithTarget_t1883299082_CustomAttributesCacheGenerator_m_StaticString(CustomAttributesCache* cache)
{
	{
		SerializeField_t1393754034 * tmp = (SerializeField_t1393754034 *)cache->attributes[0];
		SerializeField__ctor_m3401225370(tmp, NULL);
	}
}
extern const CustomAttributesCacheGenerator g_AttributeGenerators[1542] = 
{
	NULL,
	g_mscorlib_Assembly_CustomAttributesCacheGenerator,
	RuntimeObject_CustomAttributesCacheGenerator,
	RuntimeObject_CustomAttributesCacheGenerator_Object__ctor_m3446193457,
	RuntimeObject_CustomAttributesCacheGenerator_Object_Finalize_m1722074356,
	RuntimeObject_CustomAttributesCacheGenerator_Object_ReferenceEquals_m1923645570,
	ValueType_t1920584095_CustomAttributesCacheGenerator,
	Attribute_t1659210826_CustomAttributesCacheGenerator,
	_Attribute_t480711065_CustomAttributesCacheGenerator,
	Int32_t2571135199_CustomAttributesCacheGenerator,
	IFormattable_t1418953097_CustomAttributesCacheGenerator,
	IConvertible_t1224795492_CustomAttributesCacheGenerator,
	IComparable_t63485110_CustomAttributesCacheGenerator,
	SerializableAttribute_t2180742604_CustomAttributesCacheGenerator,
	AttributeUsageAttribute_t2370971113_CustomAttributesCacheGenerator,
	ComVisibleAttribute_t2289974704_CustomAttributesCacheGenerator,
	Int64_t384086013_CustomAttributesCacheGenerator,
	UInt32_t2739056616_CustomAttributesCacheGenerator,
	UInt32_t2739056616_CustomAttributesCacheGenerator_UInt32_Parse_m51087447,
	UInt32_t2739056616_CustomAttributesCacheGenerator_UInt32_Parse_m1290187410,
	UInt32_t2739056616_CustomAttributesCacheGenerator_UInt32_TryParse_m3247026924,
	UInt32_t2739056616_CustomAttributesCacheGenerator_UInt32_TryParse_m1105936683,
	CLSCompliantAttribute_t2118253839_CustomAttributesCacheGenerator,
	UInt64_t2265270229_CustomAttributesCacheGenerator,
	UInt64_t2265270229_CustomAttributesCacheGenerator_UInt64_Parse_m1110924678,
	UInt64_t2265270229_CustomAttributesCacheGenerator_UInt64_Parse_m1872577878,
	UInt64_t2265270229_CustomAttributesCacheGenerator_UInt64_TryParse_m1628442159,
	Byte_t880488320_CustomAttributesCacheGenerator,
	SByte_t2530277047_CustomAttributesCacheGenerator,
	SByte_t2530277047_CustomAttributesCacheGenerator_SByte_Parse_m3790253308,
	SByte_t2530277047_CustomAttributesCacheGenerator_SByte_Parse_m1842460049,
	SByte_t2530277047_CustomAttributesCacheGenerator_SByte_TryParse_m3168549138,
	Int16_t769030278_CustomAttributesCacheGenerator,
	UInt16_t11709673_CustomAttributesCacheGenerator,
	UInt16_t11709673_CustomAttributesCacheGenerator_UInt16_Parse_m1722685482,
	UInt16_t11709673_CustomAttributesCacheGenerator_UInt16_Parse_m1446859916,
	UInt16_t11709673_CustomAttributesCacheGenerator_UInt16_TryParse_m78750700,
	UInt16_t11709673_CustomAttributesCacheGenerator_UInt16_TryParse_m209638213,
	IEnumerator_t4262330110_CustomAttributesCacheGenerator,
	IEnumerable_t1872631827_CustomAttributesCacheGenerator,
	IEnumerable_t1872631827_CustomAttributesCacheGenerator_IEnumerable_GetEnumerator_m2081137055,
	IDisposable_t318870991_CustomAttributesCacheGenerator,
	Char_t3572527572_CustomAttributesCacheGenerator,
	String_t_CustomAttributesCacheGenerator,
	String_t_CustomAttributesCacheGenerator_String__ctor_m1254661065,
	String_t_CustomAttributesCacheGenerator_String_Equals_m1448975066,
	String_t_CustomAttributesCacheGenerator_String_Equals_m672522368,
	String_t_CustomAttributesCacheGenerator_String_Split_m4016464379____separator0,
	String_t_CustomAttributesCacheGenerator_String_Split_m2753730040,
	String_t_CustomAttributesCacheGenerator_String_Split_m2680706933,
	String_t_CustomAttributesCacheGenerator_String_Split_m993848168,
	String_t_CustomAttributesCacheGenerator_String_Trim_m361890761____trimChars0,
	String_t_CustomAttributesCacheGenerator_String_TrimStart_m4243963028____trimChars0,
	String_t_CustomAttributesCacheGenerator_String_TrimEnd_m498525724____trimChars0,
	String_t_CustomAttributesCacheGenerator_String_Format_m2496254338____args1,
	String_t_CustomAttributesCacheGenerator_String_Format_m3845522687____args2,
	String_t_CustomAttributesCacheGenerator_String_FormatHelper_m2208728312____args3,
	String_t_CustomAttributesCacheGenerator_String_Concat_m2733933624____args0,
	String_t_CustomAttributesCacheGenerator_String_Concat_m2040094846____values0,
	String_t_CustomAttributesCacheGenerator_String_GetHashCode_m3205775290,
	ICloneable_t3230708396_CustomAttributesCacheGenerator,
	Single_t496865882_CustomAttributesCacheGenerator,
	Single_t496865882_CustomAttributesCacheGenerator_Single_IsNaN_m1192835548,
	Double_t1454265465_CustomAttributesCacheGenerator,
	Double_t1454265465_CustomAttributesCacheGenerator_Double_IsNaN_m2498995017,
	Decimal_t2656901032_CustomAttributesCacheGenerator,
	Decimal_t2656901032_CustomAttributesCacheGenerator_MinValue,
	Decimal_t2656901032_CustomAttributesCacheGenerator_MaxValue,
	Decimal_t2656901032_CustomAttributesCacheGenerator_MinusOne,
	Decimal_t2656901032_CustomAttributesCacheGenerator_One,
	Decimal_t2656901032_CustomAttributesCacheGenerator_Decimal__ctor_m2684736918,
	Decimal_t2656901032_CustomAttributesCacheGenerator_Decimal__ctor_m700533170,
	Decimal_t2656901032_CustomAttributesCacheGenerator_Decimal_Compare_m4142387002,
	Decimal_t2656901032_CustomAttributesCacheGenerator_Decimal_op_Explicit_m2905558026,
	Decimal_t2656901032_CustomAttributesCacheGenerator_Decimal_op_Explicit_m1941964535,
	Decimal_t2656901032_CustomAttributesCacheGenerator_Decimal_op_Explicit_m801735887,
	Decimal_t2656901032_CustomAttributesCacheGenerator_Decimal_op_Explicit_m3225117811,
	Decimal_t2656901032_CustomAttributesCacheGenerator_Decimal_op_Implicit_m1059519657,
	Decimal_t2656901032_CustomAttributesCacheGenerator_Decimal_op_Implicit_m2347208944,
	Decimal_t2656901032_CustomAttributesCacheGenerator_Decimal_op_Implicit_m74114740,
	Decimal_t2656901032_CustomAttributesCacheGenerator_Decimal_op_Implicit_m2277896146,
	Boolean_t2059672899_CustomAttributesCacheGenerator,
	IntPtr_t_CustomAttributesCacheGenerator,
	IntPtr_t_CustomAttributesCacheGenerator_IntPtr__ctor_m1874125153,
	IntPtr_t_CustomAttributesCacheGenerator_IntPtr__ctor_m1846124433,
	IntPtr_t_CustomAttributesCacheGenerator_IntPtr__ctor_m3020263568,
	IntPtr_t_CustomAttributesCacheGenerator_IntPtr_get_Size_m3603629221,
	IntPtr_t_CustomAttributesCacheGenerator_IntPtr_ToInt64_m3776154157,
	IntPtr_t_CustomAttributesCacheGenerator_IntPtr_ToPointer_m565950407,
	IntPtr_t_CustomAttributesCacheGenerator_IntPtr_op_Equality_m3978210203,
	IntPtr_t_CustomAttributesCacheGenerator_IntPtr_op_Inequality_m1374147269,
	IntPtr_t_CustomAttributesCacheGenerator_IntPtr_op_Explicit_m3440174878,
	IntPtr_t_CustomAttributesCacheGenerator_IntPtr_op_Explicit_m1287079606,
	IntPtr_t_CustomAttributesCacheGenerator_IntPtr_op_Explicit_m913523000,
	IntPtr_t_CustomAttributesCacheGenerator_IntPtr_op_Explicit_m3354255807,
	ISerializable_t631501906_CustomAttributesCacheGenerator,
	UIntPtr_t_CustomAttributesCacheGenerator,
	MulticastDelegate_t69367374_CustomAttributesCacheGenerator,
	Delegate_t96267039_CustomAttributesCacheGenerator,
	Enum_t750633987_CustomAttributesCacheGenerator,
	Enum_t750633987_CustomAttributesCacheGenerator_Enum_GetName_m2871384883,
	Enum_t750633987_CustomAttributesCacheGenerator_Enum_IsDefined_m1555923783,
	Enum_t750633987_CustomAttributesCacheGenerator_Enum_GetUnderlyingType_m4160021701,
	Enum_t750633987_CustomAttributesCacheGenerator_Enum_Parse_m1598758293,
	Enum_t750633987_CustomAttributesCacheGenerator_Enum_ToString_m3329141164,
	Enum_t750633987_CustomAttributesCacheGenerator_Enum_ToString_m3343977455,
	Enum_t750633987_CustomAttributesCacheGenerator_Enum_ToObject_m412986634,
	Enum_t750633987_CustomAttributesCacheGenerator_Enum_ToObject_m1409448066,
	Enum_t750633987_CustomAttributesCacheGenerator_Enum_ToObject_m1790622089,
	Enum_t750633987_CustomAttributesCacheGenerator_Enum_ToObject_m3209825685,
	Enum_t750633987_CustomAttributesCacheGenerator_Enum_ToObject_m1546957196,
	Enum_t750633987_CustomAttributesCacheGenerator_Enum_ToObject_m2176493389,
	Enum_t750633987_CustomAttributesCacheGenerator_Enum_ToObject_m363118936,
	Enum_t750633987_CustomAttributesCacheGenerator_Enum_ToObject_m1273833310,
	Enum_t750633987_CustomAttributesCacheGenerator_Enum_ToObject_m2284871884,
	Enum_t750633987_CustomAttributesCacheGenerator_Enum_Format_m402722146,
	RuntimeArray_CustomAttributesCacheGenerator,
	RuntimeArray_CustomAttributesCacheGenerator_Array_System_Collections_IList_IndexOf_m523778419,
	RuntimeArray_CustomAttributesCacheGenerator_Array_get_Length_m1201869234,
	RuntimeArray_CustomAttributesCacheGenerator_Array_get_LongLength_m63201949,
	RuntimeArray_CustomAttributesCacheGenerator_Array_get_Rank_m3141701998,
	RuntimeArray_CustomAttributesCacheGenerator_Array_GetLongLength_m2596545479,
	RuntimeArray_CustomAttributesCacheGenerator_Array_GetLowerBound_m817811428,
	RuntimeArray_CustomAttributesCacheGenerator_Array_GetValue_m1494120325____indices0,
	RuntimeArray_CustomAttributesCacheGenerator_Array_SetValue_m3927045595____indices1,
	RuntimeArray_CustomAttributesCacheGenerator_Array_GetUpperBound_m152106182,
	RuntimeArray_CustomAttributesCacheGenerator_Array_GetValue_m1430766714,
	RuntimeArray_CustomAttributesCacheGenerator_Array_GetValue_m3155219782,
	RuntimeArray_CustomAttributesCacheGenerator_Array_GetValue_m217652931,
	RuntimeArray_CustomAttributesCacheGenerator_Array_SetValue_m1145170836,
	RuntimeArray_CustomAttributesCacheGenerator_Array_SetValue_m3597386468,
	RuntimeArray_CustomAttributesCacheGenerator_Array_SetValue_m1904644573,
	RuntimeArray_CustomAttributesCacheGenerator_Array_CreateInstance_m666131206____lengths1,
	RuntimeArray_CustomAttributesCacheGenerator_Array_CreateInstance_m3882898988____lengths1,
	RuntimeArray_CustomAttributesCacheGenerator_Array_GetValue_m3754306304,
	RuntimeArray_CustomAttributesCacheGenerator_Array_GetValue_m3754306304____indices0,
	RuntimeArray_CustomAttributesCacheGenerator_Array_SetValue_m4108726102,
	RuntimeArray_CustomAttributesCacheGenerator_Array_SetValue_m4108726102____indices1,
	RuntimeArray_CustomAttributesCacheGenerator_Array_BinarySearch_m2098844084,
	RuntimeArray_CustomAttributesCacheGenerator_Array_BinarySearch_m1298651580,
	RuntimeArray_CustomAttributesCacheGenerator_Array_BinarySearch_m2500870911,
	RuntimeArray_CustomAttributesCacheGenerator_Array_BinarySearch_m1629142740,
	RuntimeArray_CustomAttributesCacheGenerator_Array_Clear_m305290227,
	RuntimeArray_CustomAttributesCacheGenerator_Array_Copy_m392395275,
	RuntimeArray_CustomAttributesCacheGenerator_Array_Copy_m1287631566,
	RuntimeArray_CustomAttributesCacheGenerator_Array_Copy_m2652655318,
	RuntimeArray_CustomAttributesCacheGenerator_Array_Copy_m3698620115,
	RuntimeArray_CustomAttributesCacheGenerator_Array_IndexOf_m3436793169,
	RuntimeArray_CustomAttributesCacheGenerator_Array_IndexOf_m3442506776,
	RuntimeArray_CustomAttributesCacheGenerator_Array_IndexOf_m1260278658,
	RuntimeArray_CustomAttributesCacheGenerator_Array_LastIndexOf_m827387485,
	RuntimeArray_CustomAttributesCacheGenerator_Array_LastIndexOf_m362377999,
	RuntimeArray_CustomAttributesCacheGenerator_Array_LastIndexOf_m234007524,
	RuntimeArray_CustomAttributesCacheGenerator_Array_Reverse_m3921569028,
	RuntimeArray_CustomAttributesCacheGenerator_Array_Reverse_m3946279211,
	RuntimeArray_CustomAttributesCacheGenerator_Array_Sort_m3032378294,
	RuntimeArray_CustomAttributesCacheGenerator_Array_Sort_m243170594,
	RuntimeArray_CustomAttributesCacheGenerator_Array_Sort_m1166375231,
	RuntimeArray_CustomAttributesCacheGenerator_Array_Sort_m1504758793,
	RuntimeArray_CustomAttributesCacheGenerator_Array_Sort_m2496372837,
	RuntimeArray_CustomAttributesCacheGenerator_Array_Sort_m2232750079,
	RuntimeArray_CustomAttributesCacheGenerator_Array_Sort_m2717880702,
	RuntimeArray_CustomAttributesCacheGenerator_Array_Sort_m2668013149,
	RuntimeArray_CustomAttributesCacheGenerator_Array_Sort_m2667677402,
	RuntimeArray_CustomAttributesCacheGenerator_Array_Sort_m724484260,
	RuntimeArray_CustomAttributesCacheGenerator_Array_Sort_m3090697542,
	RuntimeArray_CustomAttributesCacheGenerator_Array_Sort_m3597357548,
	RuntimeArray_CustomAttributesCacheGenerator_Array_Sort_m1886451243,
	RuntimeArray_CustomAttributesCacheGenerator_Array_Sort_m1562643584,
	RuntimeArray_CustomAttributesCacheGenerator_Array_Sort_m2579296346,
	RuntimeArray_CustomAttributesCacheGenerator_Array_Sort_m3476310768,
	RuntimeArray_CustomAttributesCacheGenerator_Array_CopyTo_m657485551,
	RuntimeArray_CustomAttributesCacheGenerator_Array_Resize_m3981285257,
	RuntimeArray_CustomAttributesCacheGenerator_Array_BinarySearch_m4248949575,
	RuntimeArray_CustomAttributesCacheGenerator_Array_BinarySearch_m1974805269,
	RuntimeArray_CustomAttributesCacheGenerator_Array_BinarySearch_m3819272786,
	RuntimeArray_CustomAttributesCacheGenerator_Array_BinarySearch_m1130814323,
	RuntimeArray_CustomAttributesCacheGenerator_Array_ConstrainedCopy_m1738878172,
	RuntimeArray_CustomAttributesCacheGenerator_RuntimeArray____LongLength_PropertyInfo,
	ArrayReadOnlyList_1_t3002341623_CustomAttributesCacheGenerator,
	ArrayReadOnlyList_1_t3002341623_CustomAttributesCacheGenerator_ArrayReadOnlyList_1_GetEnumerator_m552220748,
	U3CGetEnumeratorU3Ec__Iterator0_t4287845664_CustomAttributesCacheGenerator,
	U3CGetEnumeratorU3Ec__Iterator0_t4287845664_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m375255571,
	U3CGetEnumeratorU3Ec__Iterator0_t4287845664_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1119679167,
	U3CGetEnumeratorU3Ec__Iterator0_t4287845664_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ec__Iterator0_Dispose_m1394235234,
	U3CGetEnumeratorU3Ec__Iterator0_t4287845664_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ec__Iterator0_Reset_m3782287660,
	ICollection_t1881172908_CustomAttributesCacheGenerator,
	IList_t2514494774_CustomAttributesCacheGenerator,
	IList_1_t2409468953_CustomAttributesCacheGenerator,
	Void_t1078098495_CustomAttributesCacheGenerator,
	Type_t_CustomAttributesCacheGenerator,
	Type_t_CustomAttributesCacheGenerator_Type_IsSubclassOf_m61888076,
	Type_t_CustomAttributesCacheGenerator_Type_GetConstructor_m272770726,
	Type_t_CustomAttributesCacheGenerator_Type_GetConstructor_m2099858645,
	Type_t_CustomAttributesCacheGenerator_Type_GetConstructor_m3078335309,
	Type_t_CustomAttributesCacheGenerator_Type_GetConstructors_m961791358,
	Type_t_CustomAttributesCacheGenerator_Type_MakeGenericType_m4233399084____typeArguments0,
	MemberInfo_t_CustomAttributesCacheGenerator,
	ICustomAttributeProvider_t4054400477_CustomAttributesCacheGenerator,
	_MemberInfo_t1659175437_CustomAttributesCacheGenerator,
	IReflect_t4184883566_CustomAttributesCacheGenerator,
	_Type_t2913350559_CustomAttributesCacheGenerator,
	Exception_t3361176243_CustomAttributesCacheGenerator,
	_Exception_t274881737_CustomAttributesCacheGenerator,
	RuntimeFieldHandle_t1782795956_CustomAttributesCacheGenerator,
	RuntimeFieldHandle_t1782795956_CustomAttributesCacheGenerator_RuntimeFieldHandle_Equals_m1824119433,
	RuntimeTypeHandle_t637624852_CustomAttributesCacheGenerator,
	RuntimeTypeHandle_t637624852_CustomAttributesCacheGenerator_RuntimeTypeHandle_Equals_m3288488752,
	ParamArrayAttribute_t3330428127_CustomAttributesCacheGenerator,
	OutAttribute_t2163224115_CustomAttributesCacheGenerator,
	ObsoleteAttribute_t3443961000_CustomAttributesCacheGenerator,
	DllImportAttribute_t3217467128_CustomAttributesCacheGenerator,
	MarshalAsAttribute_t763260220_CustomAttributesCacheGenerator,
	MarshalAsAttribute_t763260220_CustomAttributesCacheGenerator_MarshalType,
	MarshalAsAttribute_t763260220_CustomAttributesCacheGenerator_MarshalTypeRef,
	InAttribute_t159416145_CustomAttributesCacheGenerator,
	SecurityAttribute_t552991472_CustomAttributesCacheGenerator,
	GuidAttribute_t3853718442_CustomAttributesCacheGenerator,
	ComImportAttribute_t753419596_CustomAttributesCacheGenerator,
	OptionalAttribute_t14204161_CustomAttributesCacheGenerator,
	FixedBufferAttribute_t568550708_CustomAttributesCacheGenerator,
	CompilerGeneratedAttribute_t3076080542_CustomAttributesCacheGenerator,
	InternalsVisibleToAttribute_t2168674587_CustomAttributesCacheGenerator,
	RuntimeCompatibilityAttribute_t38650954_CustomAttributesCacheGenerator,
	DebuggerHiddenAttribute_t1447627238_CustomAttributesCacheGenerator,
	DefaultMemberAttribute_t2956431904_CustomAttributesCacheGenerator,
	DecimalConstantAttribute_t200860697_CustomAttributesCacheGenerator,
	DecimalConstantAttribute_t200860697_CustomAttributesCacheGenerator_DecimalConstantAttribute__ctor_m579883733,
	FieldOffsetAttribute_t1678641465_CustomAttributesCacheGenerator,
	RuntimeArgumentHandle_t1816759199_CustomAttributesCacheGenerator,
	AsyncCallback_t1046330627_CustomAttributesCacheGenerator,
	IAsyncResult_t2277996523_CustomAttributesCacheGenerator,
	TypedReference_t1527243294_CustomAttributesCacheGenerator,
	MarshalByRefObject_t1929168109_CustomAttributesCacheGenerator,
	Locale_t2448440936_CustomAttributesCacheGenerator_Locale_GetText_m659940240____args1,
	MonoTODOAttribute_t614642062_CustomAttributesCacheGenerator,
	MonoDocumentationNoteAttribute_t3720325674_CustomAttributesCacheGenerator,
	SafeHandleZeroOrMinusOneIsInvalid_t1802040518_CustomAttributesCacheGenerator_SafeHandleZeroOrMinusOneIsInvalid__ctor_m3262641181,
	SafeWaitHandle_t1875095495_CustomAttributesCacheGenerator_SafeWaitHandle__ctor_m1097298895,
	MSCompatUnicodeTable_t900664312_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map2,
	MSCompatUnicodeTable_t900664312_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map3,
	MSCompatUnicodeTable_t900664312_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map4,
	SortKey_t2188656382_CustomAttributesCacheGenerator,
	PKCS12_t2451188494_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map8,
	PKCS12_t2451188494_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map9,
	PKCS12_t2451188494_CustomAttributesCacheGenerator_U3CU3Ef__switchU24mapA,
	PKCS12_t2451188494_CustomAttributesCacheGenerator_U3CU3Ef__switchU24mapB,
	PKCS12_t2451188494_CustomAttributesCacheGenerator_U3CU3Ef__switchU24mapF,
	X509CertificateCollection_t1815624850_CustomAttributesCacheGenerator,
	X509ExtensionCollection_t3055523517_CustomAttributesCacheGenerator,
	ASN1_t450259932_CustomAttributesCacheGenerator,
	SmallXmlParser_t1266336429_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map18,
	Dictionary_2_t1922834018_CustomAttributesCacheGenerator,
	Dictionary_2_t1922834018_CustomAttributesCacheGenerator_U3CU3Ef__amU24cacheB,
	Dictionary_2_t1922834018_CustomAttributesCacheGenerator_Dictionary_2_U3CCopyToU3Em__0_m90548668,
	IDictionary_2_t3413473948_CustomAttributesCacheGenerator,
	KeyNotFoundException_t3514277852_CustomAttributesCacheGenerator,
	KeyValuePair_2_t776898938_CustomAttributesCacheGenerator,
	List_1_t82844879_CustomAttributesCacheGenerator,
	Collection_1_t3543120677_CustomAttributesCacheGenerator,
	ReadOnlyCollection_1_t891526187_CustomAttributesCacheGenerator,
	ArrayList_t655337682_CustomAttributesCacheGenerator,
	ArrayListWrapper_t3417622355_CustomAttributesCacheGenerator,
	SynchronizedArrayListWrapper_t4247483582_CustomAttributesCacheGenerator,
	ReadOnlyArrayListWrapper_t3800520942_CustomAttributesCacheGenerator,
	BitArray_t3352714871_CustomAttributesCacheGenerator,
	CaseInsensitiveComparer_t2034744907_CustomAttributesCacheGenerator,
	CaseInsensitiveHashCodeProvider_t2832089094_CustomAttributesCacheGenerator,
	CollectionBase_t559446181_CustomAttributesCacheGenerator,
	Comparer_t2783508821_CustomAttributesCacheGenerator,
	DictionaryEntry_t1385909157_CustomAttributesCacheGenerator,
	Hashtable_t3529848683_CustomAttributesCacheGenerator,
	Hashtable_t3529848683_CustomAttributesCacheGenerator_Hashtable__ctor_m3292572634,
	Hashtable_t3529848683_CustomAttributesCacheGenerator_Hashtable__ctor_m1202039326,
	Hashtable_t3529848683_CustomAttributesCacheGenerator_Hashtable__ctor_m132973949,
	Hashtable_t3529848683_CustomAttributesCacheGenerator_Hashtable__ctor_m347947491,
	Hashtable_t3529848683_CustomAttributesCacheGenerator_Hashtable__ctor_m1435436894,
	Hashtable_t3529848683_CustomAttributesCacheGenerator_Hashtable_Clear_m2559356064,
	Hashtable_t3529848683_CustomAttributesCacheGenerator_Hashtable_Remove_m3981333743,
	Hashtable_t3529848683_CustomAttributesCacheGenerator_Hashtable_OnDeserialization_m3313245466,
	Hashtable_t3529848683_CustomAttributesCacheGenerator_Hashtable_t3529848683____comparer_PropertyInfo,
	Hashtable_t3529848683_CustomAttributesCacheGenerator_Hashtable_t3529848683____hcp_PropertyInfo,
	HashKeys_t3304597669_CustomAttributesCacheGenerator,
	HashValues_t1675258150_CustomAttributesCacheGenerator,
	SyncHashtable_t3347591716_CustomAttributesCacheGenerator,
	IComparer_t237179357_CustomAttributesCacheGenerator,
	IDictionary_t1911576662_CustomAttributesCacheGenerator,
	IDictionaryEnumerator_t2742074902_CustomAttributesCacheGenerator,
	IEqualityComparer_t1867799194_CustomAttributesCacheGenerator,
	IHashCodeProvider_t692107493_CustomAttributesCacheGenerator,
	SortedList_t2047223347_CustomAttributesCacheGenerator,
	Stack_t733017594_CustomAttributesCacheGenerator,
	AssemblyHashAlgorithm_t2803938925_CustomAttributesCacheGenerator,
	AssemblyVersionCompatibility_t1122309428_CustomAttributesCacheGenerator,
	ISymbolWriter_t1307945709_CustomAttributesCacheGenerator,
	DebuggableAttribute_t3717120398_CustomAttributesCacheGenerator,
	DebuggingModes_t1277403995_CustomAttributesCacheGenerator,
	DebuggerBrowsableAttribute_t1527328379_CustomAttributesCacheGenerator,
	DebuggerBrowsableState_t329731401_CustomAttributesCacheGenerator,
	DebuggerDisplayAttribute_t2915478477_CustomAttributesCacheGenerator,
	DebuggerStepThroughAttribute_t3823612831_CustomAttributesCacheGenerator,
	DebuggerTypeProxyAttribute_t2114273919_CustomAttributesCacheGenerator,
	StackFrame_t4156368350_CustomAttributesCacheGenerator,
	StackTrace_t1923435704_CustomAttributesCacheGenerator,
	Calendar_t585695848_CustomAttributesCacheGenerator,
	Calendar_t585695848_CustomAttributesCacheGenerator_Calendar_Clone_m56132940,
	CompareInfo_t2715256550_CustomAttributesCacheGenerator,
	CompareOptions_t3933814440_CustomAttributesCacheGenerator,
	CultureInfo_t3456976115_CustomAttributesCacheGenerator,
	CultureInfo_t3456976115_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map19,
	CultureInfo_t3456976115_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map1A,
	DateTimeFormatFlags_t2573759006_CustomAttributesCacheGenerator,
	DateTimeFormatInfo_t2645956038_CustomAttributesCacheGenerator,
	DateTimeStyles_t1026389726_CustomAttributesCacheGenerator,
	DaylightTime_t4218775295_CustomAttributesCacheGenerator,
	GregorianCalendar_t1127892050_CustomAttributesCacheGenerator,
	GregorianCalendarTypes_t4096157752_CustomAttributesCacheGenerator,
	NumberFormatInfo_t299001223_CustomAttributesCacheGenerator,
	NumberStyles_t2413170484_CustomAttributesCacheGenerator,
	RegionInfo_t1443449959_CustomAttributesCacheGenerator,
	RegionInfo_t1443449959_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map1B,
	RegionInfo_t1443449959_CustomAttributesCacheGenerator_RegionInfo_t1443449959____CurrencyEnglishName_PropertyInfo,
	RegionInfo_t1443449959_CustomAttributesCacheGenerator_RegionInfo_t1443449959____DisplayName_PropertyInfo,
	RegionInfo_t1443449959_CustomAttributesCacheGenerator_RegionInfo_t1443449959____GeoId_PropertyInfo,
	RegionInfo_t1443449959_CustomAttributesCacheGenerator_RegionInfo_t1443449959____NativeName_PropertyInfo,
	RegionInfo_t1443449959_CustomAttributesCacheGenerator_RegionInfo_t1443449959____CurrencyNativeName_PropertyInfo,
	TextInfo_t3641999658_CustomAttributesCacheGenerator,
	TextInfo_t3641999658_CustomAttributesCacheGenerator_TextInfo_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m3013407797,
	TextInfo_t3641999658_CustomAttributesCacheGenerator_TextInfo_Clone_m2167794002,
	TextInfo_t3641999658_CustomAttributesCacheGenerator_TextInfo_t3641999658____CultureName_PropertyInfo,
	UnicodeCategory_t1362072295_CustomAttributesCacheGenerator,
	IsolatedStorageException_t899218601_CustomAttributesCacheGenerator,
	BinaryReader_t2596923309_CustomAttributesCacheGenerator,
	BinaryReader_t2596923309_CustomAttributesCacheGenerator_BinaryReader_ReadSByte_m3116176050,
	BinaryReader_t2596923309_CustomAttributesCacheGenerator_BinaryReader_ReadUInt16_m3883529060,
	BinaryReader_t2596923309_CustomAttributesCacheGenerator_BinaryReader_ReadUInt32_m4084076630,
	BinaryReader_t2596923309_CustomAttributesCacheGenerator_BinaryReader_ReadUInt64_m181663393,
	Directory_t1254319787_CustomAttributesCacheGenerator,
	DirectoryInfo_t1919733806_CustomAttributesCacheGenerator,
	DirectoryNotFoundException_t1050841406_CustomAttributesCacheGenerator,
	EndOfStreamException_t2896906840_CustomAttributesCacheGenerator,
	File_t3279382285_CustomAttributesCacheGenerator,
	FileAccess_t3630081704_CustomAttributesCacheGenerator,
	FileAttributes_t1797260272_CustomAttributesCacheGenerator,
	FileLoadException_t2508169678_CustomAttributesCacheGenerator,
	FileMode_t1488306634_CustomAttributesCacheGenerator,
	FileNotFoundException_t142405926_CustomAttributesCacheGenerator,
	FileOptions_t1643520207_CustomAttributesCacheGenerator,
	FileShare_t3223761390_CustomAttributesCacheGenerator,
	FileStream_t3385056842_CustomAttributesCacheGenerator,
	FileSystemInfo_t1149434941_CustomAttributesCacheGenerator,
	IOException_t2406898860_CustomAttributesCacheGenerator,
	MemoryStream_t1999701249_CustomAttributesCacheGenerator,
	Path_t1320551609_CustomAttributesCacheGenerator,
	Path_t1320551609_CustomAttributesCacheGenerator_InvalidPathChars,
	PathTooLongException_t4135805839_CustomAttributesCacheGenerator,
	SeekOrigin_t2616381620_CustomAttributesCacheGenerator,
	Stream_t2349536632_CustomAttributesCacheGenerator,
	StreamReader_t275244658_CustomAttributesCacheGenerator,
	StreamWriter_t4173970735_CustomAttributesCacheGenerator,
	StringReader_t1882904841_CustomAttributesCacheGenerator,
	TextReader_t600326831_CustomAttributesCacheGenerator,
	TextWriter_t2166895762_CustomAttributesCacheGenerator,
	UnmanagedMemoryStream_t1345777584_CustomAttributesCacheGenerator,
	AssemblyBuilder_t489430578_CustomAttributesCacheGenerator,
	ConstructorBuilder_t1596923965_CustomAttributesCacheGenerator,
	ConstructorBuilder_t1596923965_CustomAttributesCacheGenerator_ConstructorBuilder_t1596923965____CallingConvention_PropertyInfo,
	CustomAttributeBuilder_t1432176696_CustomAttributesCacheGenerator,
	EnumBuilder_t617843_CustomAttributesCacheGenerator,
	EnumBuilder_t617843_CustomAttributesCacheGenerator_EnumBuilder_GetConstructors_m3085338377,
	EventBuilder_t2300762438_CustomAttributesCacheGenerator,
	FieldBuilder_t803025013_CustomAttributesCacheGenerator,
	GenericTypeParameterBuilder_t193954652_CustomAttributesCacheGenerator,
	GenericTypeParameterBuilder_t193954652_CustomAttributesCacheGenerator_GenericTypeParameterBuilder_IsSubclassOf_m2493609931,
	GenericTypeParameterBuilder_t193954652_CustomAttributesCacheGenerator_GenericTypeParameterBuilder_GetConstructors_m2634453164,
	GenericTypeParameterBuilder_t193954652_CustomAttributesCacheGenerator_GenericTypeParameterBuilder_Equals_m3732267147,
	GenericTypeParameterBuilder_t193954652_CustomAttributesCacheGenerator_GenericTypeParameterBuilder_GetHashCode_m3376400717,
	GenericTypeParameterBuilder_t193954652_CustomAttributesCacheGenerator_GenericTypeParameterBuilder_MakeGenericType_m3812059673,
	GenericTypeParameterBuilder_t193954652_CustomAttributesCacheGenerator_GenericTypeParameterBuilder_MakeGenericType_m3812059673____typeArguments0,
	ILGenerator_t1111467769_CustomAttributesCacheGenerator,
	ILGenerator_t1111467769_CustomAttributesCacheGenerator_ILGenerator_Emit_m1154166167,
	ILGenerator_t1111467769_CustomAttributesCacheGenerator_ILGenerator_Mono_GetCurrentOffset_m66508256,
	MethodBuilder_t2964638190_CustomAttributesCacheGenerator,
	MethodBuilder_t2964638190_CustomAttributesCacheGenerator_MethodBuilder_Equals_m303455341,
	MethodBuilder_t2964638190_CustomAttributesCacheGenerator_MethodBuilder_MakeGenericMethod_m1039446169____typeArguments0,
	MethodToken_t1430837147_CustomAttributesCacheGenerator,
	ModuleBuilder_t671559400_CustomAttributesCacheGenerator,
	OpCode_t376486760_CustomAttributesCacheGenerator,
	OpCodes_t1550470625_CustomAttributesCacheGenerator,
	OpCodes_t1550470625_CustomAttributesCacheGenerator_Castclass,
	PackingSize_t3859335692_CustomAttributesCacheGenerator,
	ParameterBuilder_t2478802094_CustomAttributesCacheGenerator,
	PropertyBuilder_t1258013571_CustomAttributesCacheGenerator,
	StackBehaviour_t1946418481_CustomAttributesCacheGenerator,
	TypeBuilder_t1663068587_CustomAttributesCacheGenerator,
	TypeBuilder_t1663068587_CustomAttributesCacheGenerator_TypeBuilder_DefineConstructor_m1162809555,
	TypeBuilder_t1663068587_CustomAttributesCacheGenerator_TypeBuilder_DefineConstructor_m2260052946,
	TypeBuilder_t1663068587_CustomAttributesCacheGenerator_TypeBuilder_DefineDefaultConstructor_m3505124928,
	TypeBuilder_t1663068587_CustomAttributesCacheGenerator_TypeBuilder_GetConstructors_m55221742,
	TypeBuilder_t1663068587_CustomAttributesCacheGenerator_TypeBuilder_MakeGenericType_m2575213963,
	TypeBuilder_t1663068587_CustomAttributesCacheGenerator_TypeBuilder_MakeGenericType_m2575213963____typeArguments0,
	TypeBuilder_t1663068587_CustomAttributesCacheGenerator_TypeBuilder_IsAssignableFrom_m1068163903,
	TypeBuilder_t1663068587_CustomAttributesCacheGenerator_TypeBuilder_IsSubclassOf_m2364973326,
	TypeBuilder_t1663068587_CustomAttributesCacheGenerator_TypeBuilder_IsAssignableTo_m326333311,
	UnmanagedMarshal_t1503927065_CustomAttributesCacheGenerator,
	AmbiguousMatchException_t1921891757_CustomAttributesCacheGenerator,
	Assembly_t2665685420_CustomAttributesCacheGenerator,
	Assembly_t2665685420_CustomAttributesCacheGenerator_Assembly_GetName_m3194382543,
	AssemblyCompanyAttribute_t272143903_CustomAttributesCacheGenerator,
	AssemblyCopyrightAttribute_t1775796795_CustomAttributesCacheGenerator,
	AssemblyDefaultAliasAttribute_t356814493_CustomAttributesCacheGenerator,
	AssemblyDelaySignAttribute_t1978111684_CustomAttributesCacheGenerator,
	AssemblyDescriptionAttribute_t1589567422_CustomAttributesCacheGenerator,
	AssemblyFileVersionAttribute_t3786563949_CustomAttributesCacheGenerator,
	AssemblyInformationalVersionAttribute_t2163289957_CustomAttributesCacheGenerator,
	AssemblyKeyFileAttribute_t1077071033_CustomAttributesCacheGenerator,
	AssemblyName_t997940415_CustomAttributesCacheGenerator,
	AssemblyNameFlags_t899890916_CustomAttributesCacheGenerator,
	AssemblyProductAttribute_t3365982468_CustomAttributesCacheGenerator,
	AssemblyTitleAttribute_t2942119159_CustomAttributesCacheGenerator,
	Binder_t2856902408_CustomAttributesCacheGenerator,
	Default_t611799287_CustomAttributesCacheGenerator_Default_ReorderArgumentArray_m266338901,
	BindingFlags_t3268090012_CustomAttributesCacheGenerator,
	CallingConventions_t1168634181_CustomAttributesCacheGenerator,
	ConstructorInfo_t3360520918_CustomAttributesCacheGenerator,
	ConstructorInfo_t3360520918_CustomAttributesCacheGenerator_ConstructorName,
	ConstructorInfo_t3360520918_CustomAttributesCacheGenerator_TypeConstructorName,
	ConstructorInfo_t3360520918_CustomAttributesCacheGenerator_ConstructorInfo_Invoke_m4178483106,
	ConstructorInfo_t3360520918_CustomAttributesCacheGenerator_ConstructorInfo_t3360520918____MemberType_PropertyInfo,
	CustomAttributeData_t3408840380_CustomAttributesCacheGenerator,
	CustomAttributeData_t3408840380_CustomAttributesCacheGenerator_CustomAttributeData_t3408840380____Constructor_PropertyInfo,
	CustomAttributeData_t3408840380_CustomAttributesCacheGenerator_CustomAttributeData_t3408840380____ConstructorArguments_PropertyInfo,
	CustomAttributeNamedArgument_t425730339_CustomAttributesCacheGenerator,
	CustomAttributeTypedArgument_t2667702721_CustomAttributesCacheGenerator,
	EventAttributes_t1621333428_CustomAttributesCacheGenerator,
	EventInfo_t_CustomAttributesCacheGenerator,
	FieldAttributes_t1107365117_CustomAttributesCacheGenerator,
	FieldInfo_t_CustomAttributesCacheGenerator,
	FieldInfo_t_CustomAttributesCacheGenerator_FieldInfo_SetValue_m1839240114,
	MemberTypes_t2339173058_CustomAttributesCacheGenerator,
	MethodAttributes_t3855721989_CustomAttributesCacheGenerator,
	MethodBase_t674153939_CustomAttributesCacheGenerator,
	MethodBase_t674153939_CustomAttributesCacheGenerator_MethodBase_Invoke_m3351713316,
	MethodBase_t674153939_CustomAttributesCacheGenerator_MethodBase_GetGenericArguments_m3457019161,
	MethodImplAttributes_t3639404534_CustomAttributesCacheGenerator,
	MethodInfo_t_CustomAttributesCacheGenerator,
	MethodInfo_t_CustomAttributesCacheGenerator_MethodInfo_MakeGenericMethod_m1848918275____typeArguments0,
	MethodInfo_t_CustomAttributesCacheGenerator_MethodInfo_GetGenericArguments_m254251600,
	Missing_t4029427245_CustomAttributesCacheGenerator,
	Module_t2594760207_CustomAttributesCacheGenerator,
	PInfo_t3368652995_CustomAttributesCacheGenerator,
	ParameterAttributes_t3383706087_CustomAttributesCacheGenerator,
	ParameterInfo_t3893964560_CustomAttributesCacheGenerator,
	ParameterModifier_t969812002_CustomAttributesCacheGenerator,
	Pointer_t3648858837_CustomAttributesCacheGenerator,
	ProcessorArchitecture_t1185490558_CustomAttributesCacheGenerator,
	PropertyAttributes_t820793723_CustomAttributesCacheGenerator,
	PropertyInfo_t_CustomAttributesCacheGenerator,
	PropertyInfo_t_CustomAttributesCacheGenerator_PropertyInfo_GetValue_m1153076048,
	PropertyInfo_t_CustomAttributesCacheGenerator_PropertyInfo_SetValue_m3460960658,
	ResourceAttributes_t860636606_CustomAttributesCacheGenerator,
	StrongNameKeyPair_t4000448818_CustomAttributesCacheGenerator,
	TargetException_t2386226798_CustomAttributesCacheGenerator,
	TargetInvocationException_t2320458230_CustomAttributesCacheGenerator,
	TargetParameterCountException_t26661376_CustomAttributesCacheGenerator,
	TypeAttributes_t3122562390_CustomAttributesCacheGenerator,
	IResourceReader_t3322992291_CustomAttributesCacheGenerator,
	NeutralResourcesLanguageAttribute_t2446220560_CustomAttributesCacheGenerator,
	ResourceManager_t360577983_CustomAttributesCacheGenerator,
	ResourceReader_t974344632_CustomAttributesCacheGenerator,
	ResourceSet_t2731525910_CustomAttributesCacheGenerator,
	ResourceSet_t2731525910_CustomAttributesCacheGenerator_ResourceSet_GetEnumerator_m2033896022,
	SatelliteContractVersionAttribute_t2421164883_CustomAttributesCacheGenerator,
	CompilationRelaxations_t3691856549_CustomAttributesCacheGenerator,
	CompilationRelaxationsAttribute_t606593683_CustomAttributesCacheGenerator,
	DefaultDependencyAttribute_t2058824218_CustomAttributesCacheGenerator,
	IsVolatile_t2822106667_CustomAttributesCacheGenerator,
	StringFreezingAttribute_t4005798349_CustomAttributesCacheGenerator,
	CriticalFinalizerObject_t2148808298_CustomAttributesCacheGenerator,
	CriticalFinalizerObject_t2148808298_CustomAttributesCacheGenerator_CriticalFinalizerObject__ctor_m1083616025,
	CriticalFinalizerObject_t2148808298_CustomAttributesCacheGenerator_CriticalFinalizerObject_Finalize_m82872096,
	ReliabilityContractAttribute_t3009184403_CustomAttributesCacheGenerator,
	ActivationArguments_t3282869617_CustomAttributesCacheGenerator,
	COMException_t2943129929_CustomAttributesCacheGenerator,
	CallingConvention_t324586818_CustomAttributesCacheGenerator,
	CharSet_t2499940461_CustomAttributesCacheGenerator,
	ClassInterfaceAttribute_t1638101999_CustomAttributesCacheGenerator,
	ClassInterfaceType_t3248553764_CustomAttributesCacheGenerator,
	ComDefaultInterfaceAttribute_t2399868801_CustomAttributesCacheGenerator,
	ComInterfaceType_t1249722599_CustomAttributesCacheGenerator,
	DispIdAttribute_t3503809048_CustomAttributesCacheGenerator,
	ErrorWrapper_t2333809499_CustomAttributesCacheGenerator,
	ExternalException_t1814919774_CustomAttributesCacheGenerator,
	GCHandle_t4215464244_CustomAttributesCacheGenerator,
	GCHandleType_t1285395773_CustomAttributesCacheGenerator,
	InterfaceTypeAttribute_t3336632297_CustomAttributesCacheGenerator,
	Marshal_t4207306895_CustomAttributesCacheGenerator,
	MarshalDirectiveException_t4051754843_CustomAttributesCacheGenerator,
	PreserveSigAttribute_t877897876_CustomAttributesCacheGenerator,
	SafeHandle_t1062625530_CustomAttributesCacheGenerator_SafeHandle__ctor_m20409610,
	SafeHandle_t1062625530_CustomAttributesCacheGenerator_SafeHandle_Close_m106241507,
	SafeHandle_t1062625530_CustomAttributesCacheGenerator_SafeHandle_DangerousAddRef_m3068760737,
	SafeHandle_t1062625530_CustomAttributesCacheGenerator_SafeHandle_DangerousGetHandle_m987171088,
	SafeHandle_t1062625530_CustomAttributesCacheGenerator_SafeHandle_DangerousRelease_m2343451392,
	SafeHandle_t1062625530_CustomAttributesCacheGenerator_SafeHandle_Dispose_m3045814524,
	SafeHandle_t1062625530_CustomAttributesCacheGenerator_SafeHandle_Dispose_m1778975602,
	SafeHandle_t1062625530_CustomAttributesCacheGenerator_SafeHandle_ReleaseHandle_m3593420879,
	SafeHandle_t1062625530_CustomAttributesCacheGenerator_SafeHandle_SetHandle_m105306455,
	SafeHandle_t1062625530_CustomAttributesCacheGenerator_SafeHandle_get_IsInvalid_m3128616911,
	TypeLibImportClassAttribute_t2173872460_CustomAttributesCacheGenerator,
	TypeLibVersionAttribute_t1866475684_CustomAttributesCacheGenerator,
	UnmanagedFunctionPointerAttribute_t1887822751_CustomAttributesCacheGenerator,
	UnmanagedType_t723771350_CustomAttributesCacheGenerator,
	_Activator_t134942278_CustomAttributesCacheGenerator,
	_Assembly_t3106968071_CustomAttributesCacheGenerator,
	_AssemblyBuilder_t2438890953_CustomAttributesCacheGenerator,
	_AssemblyName_t349687559_CustomAttributesCacheGenerator,
	_ConstructorBuilder_t3355697174_CustomAttributesCacheGenerator,
	_ConstructorInfo_t1952808443_CustomAttributesCacheGenerator,
	_CustomAttributeBuilder_t2651440002_CustomAttributesCacheGenerator,
	_EnumBuilder_t3195197362_CustomAttributesCacheGenerator,
	_EventBuilder_t3169829690_CustomAttributesCacheGenerator,
	_EventInfo_t1492620845_CustomAttributesCacheGenerator,
	_FieldBuilder_t3486190427_CustomAttributesCacheGenerator,
	_FieldInfo_t3281021298_CustomAttributesCacheGenerator,
	_ILGenerator_t3086005605_CustomAttributesCacheGenerator,
	_MethodBase_t3352748864_CustomAttributesCacheGenerator,
	_MethodBuilder_t195532601_CustomAttributesCacheGenerator,
	_MethodInfo_t643827776_CustomAttributesCacheGenerator,
	_Module_t494242506_CustomAttributesCacheGenerator,
	_ModuleBuilder_t3149677118_CustomAttributesCacheGenerator,
	_ParameterBuilder_t3730072709_CustomAttributesCacheGenerator,
	_ParameterInfo_t3152035282_CustomAttributesCacheGenerator,
	_PropertyBuilder_t2876596643_CustomAttributesCacheGenerator,
	_PropertyInfo_t2517621214_CustomAttributesCacheGenerator,
	_Thread_t3433524544_CustomAttributesCacheGenerator,
	_TypeBuilder_t4136446867_CustomAttributesCacheGenerator,
	IActivator_t3392549374_CustomAttributesCacheGenerator,
	IConstructionCallMessage_t3774005762_CustomAttributesCacheGenerator,
	UrlAttribute_t3550867946_CustomAttributesCacheGenerator,
	UrlAttribute_t3550867946_CustomAttributesCacheGenerator_UrlAttribute_GetPropertiesForNewContext_m3068588212,
	UrlAttribute_t3550867946_CustomAttributesCacheGenerator_UrlAttribute_IsContextOK_m4070904995,
	ChannelServices_t2227652707_CustomAttributesCacheGenerator,
	ChannelServices_t2227652707_CustomAttributesCacheGenerator_ChannelServices_RegisterChannel_m1635994151,
	CrossAppDomainSink_t3016970110_CustomAttributesCacheGenerator,
	IChannel_t120806715_CustomAttributesCacheGenerator,
	IChannelDataStore_t3140645294_CustomAttributesCacheGenerator,
	IChannelReceiver_t1072542950_CustomAttributesCacheGenerator,
	IChannelSender_t883237208_CustomAttributesCacheGenerator,
	IClientChannelSinkProvider_t2704567566_CustomAttributesCacheGenerator,
	IServerChannelSinkProvider_t1291603674_CustomAttributesCacheGenerator,
	SinkProviderData_t758756700_CustomAttributesCacheGenerator,
	Context_t2108226161_CustomAttributesCacheGenerator,
	ContextAttribute_t4260694153_CustomAttributesCacheGenerator,
	IContextAttribute_t632650489_CustomAttributesCacheGenerator,
	IContextProperty_t2748911791_CustomAttributesCacheGenerator,
	IContributeClientContextSink_t1070229284_CustomAttributesCacheGenerator,
	IContributeDynamicSink_t1830500540_CustomAttributesCacheGenerator,
	IContributeEnvoySink_t656644779_CustomAttributesCacheGenerator,
	IContributeObjectSink_t133918256_CustomAttributesCacheGenerator,
	IContributeServerContextSink_t3033167428_CustomAttributesCacheGenerator,
	IDynamicMessageSink_t3469533509_CustomAttributesCacheGenerator,
	IDynamicProperty_t2412334468_CustomAttributesCacheGenerator,
	SynchronizationAttribute_t1579862423_CustomAttributesCacheGenerator,
	SynchronizationAttribute_t1579862423_CustomAttributesCacheGenerator_SynchronizationAttribute_GetPropertiesForNewContext_m1138224242,
	SynchronizationAttribute_t1579862423_CustomAttributesCacheGenerator_SynchronizationAttribute_IsContextOK_m3271333896,
	LifetimeServices_t2072599424_CustomAttributesCacheGenerator,
	AsyncResult_t1601560416_CustomAttributesCacheGenerator,
	ConstructionCall_t3460915224_CustomAttributesCacheGenerator,
	ConstructionCall_t3460915224_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map20,
	ConstructionCallDictionary_t3674406133_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map23,
	ConstructionCallDictionary_t3674406133_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map24,
	Header_t2445250629_CustomAttributesCacheGenerator,
	IMessage_t3609193861_CustomAttributesCacheGenerator,
	IMessageCtrl_t3563004285_CustomAttributesCacheGenerator,
	IMessageSink_t2987155855_CustomAttributesCacheGenerator,
	IMethodCallMessage_t1057394468_CustomAttributesCacheGenerator,
	IMethodMessage_t658825243_CustomAttributesCacheGenerator,
	IMethodReturnMessage_t3578412901_CustomAttributesCacheGenerator,
	IRemotingFormatter_t3301862813_CustomAttributesCacheGenerator,
	LogicalCallContext_t2541050671_CustomAttributesCacheGenerator,
	MethodCall_t1058940400_CustomAttributesCacheGenerator,
	MethodCall_t1058940400_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map1F,
	MethodDictionary_t4290091627_CustomAttributesCacheGenerator,
	MethodDictionary_t4290091627_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map21,
	MethodDictionary_t4290091627_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map22,
	RemotingSurrogateSelector_t1285344342_CustomAttributesCacheGenerator,
	ReturnMessage_t3469761868_CustomAttributesCacheGenerator,
	SoapAttribute_t2706012165_CustomAttributesCacheGenerator,
	SoapFieldAttribute_t2115119642_CustomAttributesCacheGenerator,
	SoapMethodAttribute_t2520568184_CustomAttributesCacheGenerator,
	SoapParameterAttribute_t9664292_CustomAttributesCacheGenerator,
	SoapTypeAttribute_t3635447107_CustomAttributesCacheGenerator,
	ProxyAttribute_t2439831892_CustomAttributesCacheGenerator,
	ProxyAttribute_t2439831892_CustomAttributesCacheGenerator_ProxyAttribute_GetPropertiesForNewContext_m3937741509,
	ProxyAttribute_t2439831892_CustomAttributesCacheGenerator_ProxyAttribute_IsContextOK_m4171219307,
	RealProxy_t3984737357_CustomAttributesCacheGenerator,
	ITrackingHandler_t2297406515_CustomAttributesCacheGenerator,
	TrackingServices_t535905686_CustomAttributesCacheGenerator,
	ActivatedClientTypeEntry_t1901086295_CustomAttributesCacheGenerator,
	ActivatedServiceTypeEntry_t1623752528_CustomAttributesCacheGenerator,
	IChannelInfo_t1594721423_CustomAttributesCacheGenerator,
	IEnvoyInfo_t1188604488_CustomAttributesCacheGenerator,
	IRemotingTypeInfo_t1645923797_CustomAttributesCacheGenerator,
	InternalRemotingServices_t2693007168_CustomAttributesCacheGenerator,
	ObjRef_t4239724039_CustomAttributesCacheGenerator,
	ObjRef_t4239724039_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map26,
	ObjRef_t4239724039_CustomAttributesCacheGenerator_ObjRef_get_ChannelInfo_m3336748132,
	RemotingConfiguration_t3648899533_CustomAttributesCacheGenerator,
	ConfigHandler_t4180847873_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map27,
	ConfigHandler_t4180847873_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map28,
	ConfigHandler_t4180847873_CustomAttributesCacheGenerator_ConfigHandler_ValidatePath_m3545750813____paths1,
	RemotingException_t747282334_CustomAttributesCacheGenerator,
	RemotingServices_t303445091_CustomAttributesCacheGenerator,
	RemotingServices_t303445091_CustomAttributesCacheGenerator_RemotingServices_IsTransparentProxy_m744747325,
	RemotingServices_t303445091_CustomAttributesCacheGenerator_RemotingServices_GetRealProxy_m256026011,
	SoapServices_t362502010_CustomAttributesCacheGenerator,
	TypeEntry_t4003469904_CustomAttributesCacheGenerator,
	WellKnownClientTypeEntry_t3040846894_CustomAttributesCacheGenerator,
	WellKnownObjectMode_t2386524380_CustomAttributesCacheGenerator,
	WellKnownServiceTypeEntry_t3884894085_CustomAttributesCacheGenerator,
	BinaryFormatter_t2021242092_CustomAttributesCacheGenerator,
	BinaryFormatter_t2021242092_CustomAttributesCacheGenerator_U3CDefaultSurrogateSelectorU3Ek__BackingField,
	BinaryFormatter_t2021242092_CustomAttributesCacheGenerator_BinaryFormatter_get_DefaultSurrogateSelector_m1118519797,
	FormatterAssemblyStyle_t624754737_CustomAttributesCacheGenerator,
	FormatterTypeStyle_t358799495_CustomAttributesCacheGenerator,
	TypeFilterLevel_t3266183943_CustomAttributesCacheGenerator,
	FormatterConverter_t3333246427_CustomAttributesCacheGenerator,
	FormatterServices_t2039552394_CustomAttributesCacheGenerator,
	IDeserializationCallback_t1559034278_CustomAttributesCacheGenerator,
	IFormatter_t2842043891_CustomAttributesCacheGenerator,
	IFormatterConverter_t1975945934_CustomAttributesCacheGenerator,
	IObjectReference_t2008307571_CustomAttributesCacheGenerator,
	ISerializationSurrogate_t1041104378_CustomAttributesCacheGenerator,
	ISurrogateSelector_t1684653753_CustomAttributesCacheGenerator,
	ObjectManager_t1722973918_CustomAttributesCacheGenerator,
	OnDeserializedAttribute_t2300437449_CustomAttributesCacheGenerator,
	OnDeserializingAttribute_t2405775403_CustomAttributesCacheGenerator,
	OnSerializedAttribute_t3606325723_CustomAttributesCacheGenerator,
	OnSerializingAttribute_t2302066511_CustomAttributesCacheGenerator,
	SerializationBinder_t1981625145_CustomAttributesCacheGenerator,
	SerializationEntry_t4251740542_CustomAttributesCacheGenerator,
	SerializationException_t3740332507_CustomAttributesCacheGenerator,
	SerializationInfo_t3667883434_CustomAttributesCacheGenerator,
	SerializationInfo_t3667883434_CustomAttributesCacheGenerator_SerializationInfo__ctor_m283007395,
	SerializationInfoEnumerator_t2005386294_CustomAttributesCacheGenerator,
	StreamingContext_t1316667400_CustomAttributesCacheGenerator,
	StreamingContextStates_t4185925513_CustomAttributesCacheGenerator,
	X509Certificate_t3231070314_CustomAttributesCacheGenerator,
	X509Certificate_t3231070314_CustomAttributesCacheGenerator_X509Certificate_GetIssuerName_m1334954263,
	X509Certificate_t3231070314_CustomAttributesCacheGenerator_X509Certificate_GetName_m865911396,
	X509Certificate_t3231070314_CustomAttributesCacheGenerator_X509Certificate_Equals_m2980846026,
	X509Certificate_t3231070314_CustomAttributesCacheGenerator_X509Certificate_Import_m1670206153,
	X509Certificate_t3231070314_CustomAttributesCacheGenerator_X509Certificate_Reset_m2166992089,
	X509KeyStorageFlags_t402552296_CustomAttributesCacheGenerator,
	AsymmetricAlgorithm_t1520670838_CustomAttributesCacheGenerator,
	AsymmetricKeyExchangeFormatter_t3886653799_CustomAttributesCacheGenerator,
	AsymmetricSignatureDeformatter_t456279265_CustomAttributesCacheGenerator,
	AsymmetricSignatureFormatter_t9512622_CustomAttributesCacheGenerator,
	CipherMode_t1257099160_CustomAttributesCacheGenerator,
	CryptoConfig_t2397213964_CustomAttributesCacheGenerator,
	CryptoConfig_t2397213964_CustomAttributesCacheGenerator_CryptoConfig_CreateFromName_m1179263245____args1,
	CryptographicException_t3454124556_CustomAttributesCacheGenerator,
	CryptographicUnexpectedOperationException_t3109796652_CustomAttributesCacheGenerator,
	CspParameters_t397939019_CustomAttributesCacheGenerator,
	CspProviderFlags_t2530719326_CustomAttributesCacheGenerator,
	DES_t4084254726_CustomAttributesCacheGenerator,
	DESCryptoServiceProvider_t2527584960_CustomAttributesCacheGenerator,
	DSA_t3534207242_CustomAttributesCacheGenerator,
	DSACryptoServiceProvider_t1989336279_CustomAttributesCacheGenerator,
	DSACryptoServiceProvider_t1989336279_CustomAttributesCacheGenerator_DSACryptoServiceProvider_t1989336279____PublicOnly_PropertyInfo,
	DSAParameters_t2781005375_CustomAttributesCacheGenerator,
	DSASignatureDeformatter_t3417499579_CustomAttributesCacheGenerator,
	DSASignatureFormatter_t514505739_CustomAttributesCacheGenerator,
	HMAC_t1427971378_CustomAttributesCacheGenerator,
	HMACMD5_t2888238498_CustomAttributesCacheGenerator,
	HMACRIPEMD160_t1354245691_CustomAttributesCacheGenerator,
	HMACSHA1_t882816891_CustomAttributesCacheGenerator,
	HMACSHA256_t351978820_CustomAttributesCacheGenerator,
	HMACSHA384_t4100829916_CustomAttributesCacheGenerator,
	HMACSHA512_t1403483577_CustomAttributesCacheGenerator,
	HashAlgorithm_t3481041717_CustomAttributesCacheGenerator,
	ICryptoTransform_t725015005_CustomAttributesCacheGenerator,
	ICspAsymmetricAlgorithm_t1974207366_CustomAttributesCacheGenerator,
	KeyedHashAlgorithm_t4279500660_CustomAttributesCacheGenerator,
	MACTripleDES_t3052039254_CustomAttributesCacheGenerator,
	MD5_t184894568_CustomAttributesCacheGenerator,
	MD5CryptoServiceProvider_t2301558652_CustomAttributesCacheGenerator,
	PaddingMode_t218725588_CustomAttributesCacheGenerator,
	RC2_t963760615_CustomAttributesCacheGenerator,
	RC2CryptoServiceProvider_t1519414973_CustomAttributesCacheGenerator,
	RIPEMD160_t482559745_CustomAttributesCacheGenerator,
	RIPEMD160Managed_t662892897_CustomAttributesCacheGenerator,
	RSA_t2561376050_CustomAttributesCacheGenerator,
	RSACryptoServiceProvider_t393426716_CustomAttributesCacheGenerator,
	RSACryptoServiceProvider_t393426716_CustomAttributesCacheGenerator_RSACryptoServiceProvider_t393426716____PublicOnly_PropertyInfo,
	RSAPKCS1KeyExchangeFormatter_t1995405421_CustomAttributesCacheGenerator,
	RSAPKCS1SignatureDeformatter_t1271325812_CustomAttributesCacheGenerator,
	RSAPKCS1SignatureFormatter_t3136547357_CustomAttributesCacheGenerator,
	RSAParameters_t2749614896_CustomAttributesCacheGenerator,
	Rijndael_t2194267518_CustomAttributesCacheGenerator,
	RijndaelManaged_t3688703751_CustomAttributesCacheGenerator,
	RijndaelManagedTransform_t3825137234_CustomAttributesCacheGenerator,
	SHA1_t3760328239_CustomAttributesCacheGenerator,
	SHA1CryptoServiceProvider_t3974373694_CustomAttributesCacheGenerator,
	SHA1Managed_t2877123146_CustomAttributesCacheGenerator,
	SHA256_t94154448_CustomAttributesCacheGenerator,
	SHA256Managed_t316933500_CustomAttributesCacheGenerator,
	SHA384_t1458639100_CustomAttributesCacheGenerator,
	SHA384Managed_t3276463015_CustomAttributesCacheGenerator,
	SHA512_t2457760952_CustomAttributesCacheGenerator,
	SHA512Managed_t3922712332_CustomAttributesCacheGenerator,
	SignatureDescription_t4263538145_CustomAttributesCacheGenerator,
	SymmetricAlgorithm_t1428778343_CustomAttributesCacheGenerator,
	ToBase64Transform_t1095090031_CustomAttributesCacheGenerator,
	TripleDES_t1638718720_CustomAttributesCacheGenerator,
	TripleDESCryptoServiceProvider_t3718652277_CustomAttributesCacheGenerator,
	CodeAccessSecurityAttribute_t1836922066_CustomAttributesCacheGenerator,
	IUnrestrictedPermission_t4141731025_CustomAttributesCacheGenerator,
	SecurityAction_t4061768731_CustomAttributesCacheGenerator,
	SecurityPermission_t2467225011_CustomAttributesCacheGenerator,
	SecurityPermissionAttribute_t1827074234_CustomAttributesCacheGenerator,
	SecurityPermissionFlag_t4236913982_CustomAttributesCacheGenerator,
	StrongNamePublicKeyBlob_t759939843_CustomAttributesCacheGenerator,
	ApplicationTrust_t3070296692_CustomAttributesCacheGenerator,
	Evidence_t2634340723_CustomAttributesCacheGenerator,
	Evidence_t2634340723_CustomAttributesCacheGenerator_Evidence_Equals_m846405073,
	Evidence_t2634340723_CustomAttributesCacheGenerator_Evidence_GetHashCode_m1726229036,
	Hash_t1979402678_CustomAttributesCacheGenerator,
	IIdentityPermissionFactory_t233160095_CustomAttributesCacheGenerator,
	StrongName_t848839738_CustomAttributesCacheGenerator,
	IIdentity_t4205433510_CustomAttributesCacheGenerator,
	IPrincipal_t1738737119_CustomAttributesCacheGenerator,
	PrincipalPolicy_t3910466358_CustomAttributesCacheGenerator,
	WindowsAccountType_t3074501293_CustomAttributesCacheGenerator,
	WindowsIdentity_t1567274306_CustomAttributesCacheGenerator,
	WindowsIdentity_t1567274306_CustomAttributesCacheGenerator_WindowsIdentity_Dispose_m953714069,
	AllowPartiallyTrustedCallersAttribute_t2708656030_CustomAttributesCacheGenerator,
	CodeAccessPermission_t1591093589_CustomAttributesCacheGenerator,
	CodeAccessPermission_t1591093589_CustomAttributesCacheGenerator_CodeAccessPermission_Equals_m593433957,
	CodeAccessPermission_t1591093589_CustomAttributesCacheGenerator_CodeAccessPermission_GetHashCode_m2903672344,
	IPermission_t3649357080_CustomAttributesCacheGenerator,
	ISecurityEncodable_t3776590316_CustomAttributesCacheGenerator,
	IStackWalk_t303383304_CustomAttributesCacheGenerator,
	PermissionSet_t1654582346_CustomAttributesCacheGenerator_U3CDeclarativeSecurityU3Ek__BackingField,
	PermissionSet_t1654582346_CustomAttributesCacheGenerator_PermissionSet_set_DeclarativeSecurity_m84390744,
	SecurityCriticalAttribute_t3270381382_CustomAttributesCacheGenerator,
	SecurityElement_t2714688033_CustomAttributesCacheGenerator,
	SecurityException_t791478456_CustomAttributesCacheGenerator,
	SecurityException_t791478456_CustomAttributesCacheGenerator_SecurityException_t791478456____Demanded_PropertyInfo,
	SecurityManager_t3149513615_CustomAttributesCacheGenerator,
	SecurityManager_t3149513615_CustomAttributesCacheGenerator_SecurityManager_t3149513615____SecurityEnabled_PropertyInfo,
	SecuritySafeCriticalAttribute_t824959508_CustomAttributesCacheGenerator,
	SuppressUnmanagedCodeSecurityAttribute_t1135819281_CustomAttributesCacheGenerator,
	UnverifiableCodeAttribute_t404930905_CustomAttributesCacheGenerator,
	ASCIIEncoding_t3246903780_CustomAttributesCacheGenerator,
	ASCIIEncoding_t3246903780_CustomAttributesCacheGenerator_ASCIIEncoding_GetBytes_m687126587,
	ASCIIEncoding_t3246903780_CustomAttributesCacheGenerator_ASCIIEncoding_GetByteCount_m1478581917,
	ASCIIEncoding_t3246903780_CustomAttributesCacheGenerator_ASCIIEncoding_GetDecoder_m763127219,
	Decoder_t1525265844_CustomAttributesCacheGenerator,
	Decoder_t1525265844_CustomAttributesCacheGenerator_Decoder_t1525265844____Fallback_PropertyInfo,
	Decoder_t1525265844_CustomAttributesCacheGenerator_Decoder_t1525265844____FallbackBuffer_PropertyInfo,
	DecoderReplacementFallback_t4051900696_CustomAttributesCacheGenerator_DecoderReplacementFallback__ctor_m3316979574,
	EncoderReplacementFallback_t1692097091_CustomAttributesCacheGenerator_EncoderReplacementFallback__ctor_m1548856421,
	Encoding_t1422240108_CustomAttributesCacheGenerator,
	Encoding_t1422240108_CustomAttributesCacheGenerator_Encoding_InvokeI18N_m3158856541____args1,
	Encoding_t1422240108_CustomAttributesCacheGenerator_Encoding_Clone_m1538624630,
	Encoding_t1422240108_CustomAttributesCacheGenerator_Encoding_GetByteCount_m2900855870,
	Encoding_t1422240108_CustomAttributesCacheGenerator_Encoding_GetBytes_m3914031873,
	Encoding_t1422240108_CustomAttributesCacheGenerator_Encoding_t1422240108____IsReadOnly_PropertyInfo,
	Encoding_t1422240108_CustomAttributesCacheGenerator_Encoding_t1422240108____DecoderFallback_PropertyInfo,
	Encoding_t1422240108_CustomAttributesCacheGenerator_Encoding_t1422240108____EncoderFallback_PropertyInfo,
	StringBuilder_t2076519010_CustomAttributesCacheGenerator,
	StringBuilder_t2076519010_CustomAttributesCacheGenerator_StringBuilder_AppendFormat_m2847832545____args1,
	StringBuilder_t2076519010_CustomAttributesCacheGenerator_StringBuilder_AppendFormat_m2246691282____args2,
	UTF32Encoding_t108799848_CustomAttributesCacheGenerator_UTF32Encoding_GetByteCount_m1402347958,
	UTF32Encoding_t108799848_CustomAttributesCacheGenerator_UTF32Encoding_GetBytes_m4274563278,
	UTF32Encoding_t108799848_CustomAttributesCacheGenerator_UTF32Encoding_GetByteCount_m2178484365,
	UTF32Encoding_t108799848_CustomAttributesCacheGenerator_UTF32Encoding_GetBytes_m3528139859,
	UTF7Encoding_t2298099273_CustomAttributesCacheGenerator,
	UTF7Encoding_t2298099273_CustomAttributesCacheGenerator_UTF7Encoding_GetHashCode_m3728423310,
	UTF7Encoding_t2298099273_CustomAttributesCacheGenerator_UTF7Encoding_Equals_m3440788742,
	UTF7Encoding_t2298099273_CustomAttributesCacheGenerator_UTF7Encoding_GetByteCount_m4153636640,
	UTF7Encoding_t2298099273_CustomAttributesCacheGenerator_UTF7Encoding_GetByteCount_m260339288,
	UTF7Encoding_t2298099273_CustomAttributesCacheGenerator_UTF7Encoding_GetBytes_m3276108052,
	UTF7Encoding_t2298099273_CustomAttributesCacheGenerator_UTF7Encoding_GetBytes_m565634380,
	UTF7Encoding_t2298099273_CustomAttributesCacheGenerator_UTF7Encoding_GetString_m753427559,
	UTF8Encoding_t2311876230_CustomAttributesCacheGenerator,
	UTF8Encoding_t2311876230_CustomAttributesCacheGenerator_UTF8Encoding_GetByteCount_m2144817935,
	UTF8Encoding_t2311876230_CustomAttributesCacheGenerator_UTF8Encoding_GetBytes_m1154207544,
	UTF8Encoding_t2311876230_CustomAttributesCacheGenerator_UTF8Encoding_GetString_m774016146,
	UnicodeEncoding_t4077319695_CustomAttributesCacheGenerator,
	UnicodeEncoding_t4077319695_CustomAttributesCacheGenerator_UnicodeEncoding_GetByteCount_m3150074527,
	UnicodeEncoding_t4077319695_CustomAttributesCacheGenerator_UnicodeEncoding_GetBytes_m3268668497,
	UnicodeEncoding_t4077319695_CustomAttributesCacheGenerator_UnicodeEncoding_GetString_m1015926560,
	CompressedStack_t2985122284_CustomAttributesCacheGenerator_CompressedStack_CreateCopy_m233976566,
	EventResetMode_t613571177_CustomAttributesCacheGenerator,
	EventWaitHandle_t651679404_CustomAttributesCacheGenerator,
	ExecutionContext_t2619615533_CustomAttributesCacheGenerator_ExecutionContext__ctor_m184114300,
	Interlocked_t2817274411_CustomAttributesCacheGenerator_Interlocked_CompareExchange_m2168439704,
	Interlocked_t2817274411_CustomAttributesCacheGenerator_Interlocked_CompareExchange_m4154459173,
	ManualResetEvent_t3770519771_CustomAttributesCacheGenerator,
	Monitor_t2324682247_CustomAttributesCacheGenerator,
	Monitor_t2324682247_CustomAttributesCacheGenerator_Monitor_Exit_m1372154428,
	Mutex_t312945462_CustomAttributesCacheGenerator,
	Mutex_t312945462_CustomAttributesCacheGenerator_Mutex__ctor_m2870296590,
	Mutex_t312945462_CustomAttributesCacheGenerator_Mutex_ReleaseMutex_m1972465564,
	SynchronizationContext_t3863215862_CustomAttributesCacheGenerator_currentContext,
	SynchronizationLockException_t461259377_CustomAttributesCacheGenerator,
	Thread_t2418682337_CustomAttributesCacheGenerator,
	Thread_t2418682337_CustomAttributesCacheGenerator_local_slots,
	Thread_t2418682337_CustomAttributesCacheGenerator__ec,
	Thread_t2418682337_CustomAttributesCacheGenerator_Thread_get_CurrentThread_m3312024903,
	Thread_t2418682337_CustomAttributesCacheGenerator_Thread_Finalize_m773434565,
	Thread_t2418682337_CustomAttributesCacheGenerator_Thread_get_ExecutionContext_m2718260421,
	Thread_t2418682337_CustomAttributesCacheGenerator_Thread_get_ManagedThreadId_m2540698636,
	Thread_t2418682337_CustomAttributesCacheGenerator_Thread_GetHashCode_m1763772112,
	Thread_t2418682337_CustomAttributesCacheGenerator_Thread_GetCompressedStack_m2176559759,
	Thread_t2418682337_CustomAttributesCacheGenerator_Thread_t2418682337____ExecutionContext_PropertyInfo,
	ThreadAbortException_t2477337875_CustomAttributesCacheGenerator,
	ThreadInterruptedException_t1245490379_CustomAttributesCacheGenerator,
	ThreadState_t231270902_CustomAttributesCacheGenerator,
	ThreadStateException_t671479770_CustomAttributesCacheGenerator,
	Timer_t876405921_CustomAttributesCacheGenerator,
	WaitHandle_t2964044060_CustomAttributesCacheGenerator,
	WaitHandle_t2964044060_CustomAttributesCacheGenerator_WaitHandle_t2964044060____Handle_PropertyInfo,
	AccessViolationException_t3196137127_CustomAttributesCacheGenerator,
	ActivationContext_t4070894218_CustomAttributesCacheGenerator,
	Activator_t214179882_CustomAttributesCacheGenerator,
	Activator_t214179882_CustomAttributesCacheGenerator_Activator_CreateInstance_m291998472____args1,
	AppDomain_t3770931252_CustomAttributesCacheGenerator,
	AppDomain_t3770931252_CustomAttributesCacheGenerator_type_resolve_in_progress,
	AppDomain_t3770931252_CustomAttributesCacheGenerator_assembly_resolve_in_progress,
	AppDomain_t3770931252_CustomAttributesCacheGenerator_assembly_resolve_in_progress_refonly,
	AppDomain_t3770931252_CustomAttributesCacheGenerator__principal,
	AppDomainManager_t58958733_CustomAttributesCacheGenerator,
	AppDomainSetup_t2748923811_CustomAttributesCacheGenerator,
	ApplicationException_t899029688_CustomAttributesCacheGenerator,
	ApplicationIdentity_t2437177209_CustomAttributesCacheGenerator,
	ArgumentException_t1372035057_CustomAttributesCacheGenerator,
	ArgumentNullException_t517973450_CustomAttributesCacheGenerator,
	ArgumentOutOfRangeException_t1745463579_CustomAttributesCacheGenerator,
	ArithmeticException_t3212484507_CustomAttributesCacheGenerator,
	ArrayTypeMismatchException_t1927459263_CustomAttributesCacheGenerator,
	AssemblyLoadEventArgs_t4063075709_CustomAttributesCacheGenerator,
	AttributeTargets_t2512976426_CustomAttributesCacheGenerator,
	Buffer_t127642776_CustomAttributesCacheGenerator,
	CharEnumerator_t1625757873_CustomAttributesCacheGenerator,
	ContextBoundObject_t2718001193_CustomAttributesCacheGenerator,
	Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToBoolean_m3849780071,
	Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToBoolean_m1821365203,
	Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToBoolean_m3541395848,
	Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToBoolean_m3686882876,
	Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToByte_m1998596307,
	Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToByte_m627183151,
	Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToByte_m1655664624,
	Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToByte_m2693208695,
	Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToChar_m1651289017,
	Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToChar_m263958929,
	Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToChar_m2065393292,
	Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToChar_m1385558794,
	Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToDateTime_m1240677581,
	Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToDateTime_m2178216354,
	Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToDateTime_m1691902540,
	Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToDateTime_m1899241014,
	Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToDecimal_m2105281924,
	Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToDecimal_m4051859286,
	Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToDecimal_m1174043729,
	Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToDecimal_m1566524000,
	Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToDouble_m3582443749,
	Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToDouble_m3414375849,
	Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToDouble_m1338516771,
	Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToDouble_m1798060040,
	Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToInt16_m1268881642,
	Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToInt16_m4195031634,
	Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToInt16_m3052677612,
	Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToInt16_m4149411228,
	Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToInt32_m1206626159,
	Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToInt32_m2420320894,
	Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToInt32_m575912531,
	Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToInt32_m349566814,
	Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToInt64_m1063954313,
	Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToInt64_m1102749412,
	Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToInt64_m781390160,
	Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToInt64_m2202161021,
	Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToSByte_m863025055,
	Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToSByte_m1485386302,
	Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToSByte_m1209019644,
	Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToSByte_m1294608859,
	Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToSByte_m966005355,
	Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToSByte_m2780065447,
	Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToSByte_m3186677260,
	Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToSByte_m763994626,
	Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToSByte_m3414187568,
	Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToSByte_m1378762269,
	Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToSByte_m1804217813,
	Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToSByte_m3354070798,
	Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToSByte_m2415307335,
	Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToSByte_m1950141290,
	Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToSingle_m1511457299,
	Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToSingle_m3480326864,
	Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToSingle_m2260413742,
	Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToSingle_m3787160430,
	Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToUInt16_m3506724663,
	Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToUInt16_m115274073,
	Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToUInt16_m3490324414,
	Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToUInt16_m3912085078,
	Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToUInt16_m207185530,
	Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToUInt16_m722491632,
	Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToUInt16_m1222063852,
	Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToUInt16_m4146807859,
	Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToUInt16_m1828753276,
	Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToUInt16_m2948714162,
	Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToUInt16_m2011096748,
	Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToUInt16_m418187698,
	Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToUInt16_m3930851433,
	Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToUInt16_m996906177,
	Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToUInt32_m4249819190,
	Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToUInt32_m823261086,
	Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToUInt32_m3279699603,
	Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToUInt32_m837934666,
	Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToUInt32_m3154368742,
	Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToUInt32_m3499511866,
	Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToUInt32_m3902982566,
	Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToUInt32_m1630022459,
	Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToUInt32_m2888961780,
	Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToUInt32_m3875914063,
	Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToUInt32_m2993243305,
	Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToUInt32_m2675516930,
	Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToUInt32_m3219512886,
	Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToUInt32_m731182432,
	Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToUInt64_m2793460560,
	Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToUInt64_m3441470682,
	Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToUInt64_m651291461,
	Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToUInt64_m1016581210,
	Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToUInt64_m4062218541,
	Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToUInt64_m1587419655,
	Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToUInt64_m1283785253,
	Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToUInt64_m3177931015,
	Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToUInt64_m3453577592,
	Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToUInt64_m371719104,
	Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToUInt64_m1691859135,
	Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToUInt64_m3212089508,
	Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToUInt64_m1360931664,
	Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToUInt64_m2801452660,
	Convert_t714015227_CustomAttributesCacheGenerator_Convert_ToUInt64_m3389700845,
	DBNull_t2963649720_CustomAttributesCacheGenerator,
	DateTimeKind_t3511769276_CustomAttributesCacheGenerator,
	DateTimeOffset_t2220763929_CustomAttributesCacheGenerator_DateTimeOffset_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m2312432265,
	DayOfWeek_t61775933_CustomAttributesCacheGenerator,
	DivideByZeroException_t1878407045_CustomAttributesCacheGenerator,
	DllNotFoundException_t2724339085_CustomAttributesCacheGenerator,
	EntryPointNotFoundException_t2358315754_CustomAttributesCacheGenerator,
	MonoEnumInfo_t776332500_CustomAttributesCacheGenerator_cache,
	Environment_t3969540094_CustomAttributesCacheGenerator,
	SpecialFolder_t878893316_CustomAttributesCacheGenerator,
	EventArgs_t2765042921_CustomAttributesCacheGenerator,
	ExecutionEngineException_t3097126791_CustomAttributesCacheGenerator,
	FieldAccessException_t3546361566_CustomAttributesCacheGenerator,
	FlagsAttribute_t388727259_CustomAttributesCacheGenerator,
	FormatException_t4223230285_CustomAttributesCacheGenerator,
	GC_t2835493519_CustomAttributesCacheGenerator_GC_SuppressFinalize_m3884757609,
	Guid_t_CustomAttributesCacheGenerator,
	ICustomFormatter_t3941648126_CustomAttributesCacheGenerator,
	IFormatProvider_t2002930665_CustomAttributesCacheGenerator,
	IndexOutOfRangeException_t3041194587_CustomAttributesCacheGenerator,
	InvalidCastException_t1009887698_CustomAttributesCacheGenerator,
	InvalidOperationException_t1956282378_CustomAttributesCacheGenerator,
	LoaderOptimization_t3391871335_CustomAttributesCacheGenerator,
	LoaderOptimization_t3391871335_CustomAttributesCacheGenerator_DomainMask,
	LoaderOptimization_t3391871335_CustomAttributesCacheGenerator_DisallowBindings,
	LocalDataStoreSlot_t2417262534_CustomAttributesCacheGenerator,
	Math_t808000742_CustomAttributesCacheGenerator_Math_Max_m3051295998,
	Math_t808000742_CustomAttributesCacheGenerator_Math_Min_m1276480861,
	Math_t808000742_CustomAttributesCacheGenerator_Math_Sqrt_m121065893,
	MemberAccessException_t2381943930_CustomAttributesCacheGenerator,
	MethodAccessException_t3541341797_CustomAttributesCacheGenerator,
	MissingFieldException_t1705577432_CustomAttributesCacheGenerator,
	MissingMemberException_t2177990328_CustomAttributesCacheGenerator,
	MissingMethodException_t924411028_CustomAttributesCacheGenerator,
	MulticastNotSupportedException_t396845527_CustomAttributesCacheGenerator,
	NonSerializedAttribute_t565753174_CustomAttributesCacheGenerator,
	NotImplementedException_t2045372761_CustomAttributesCacheGenerator,
	NotSupportedException_t404487136_CustomAttributesCacheGenerator,
	NullReferenceException_t2549690990_CustomAttributesCacheGenerator,
	NumberFormatter_t4186985114_CustomAttributesCacheGenerator_threadNumberFormatter,
	ObjectDisposedException_t363085662_CustomAttributesCacheGenerator,
	OperatingSystem_t2121465664_CustomAttributesCacheGenerator,
	OutOfMemoryException_t2147619140_CustomAttributesCacheGenerator,
	OverflowException_t666911278_CustomAttributesCacheGenerator,
	PlatformID_t2641264986_CustomAttributesCacheGenerator,
	PlatformNotSupportedException_t186987067_CustomAttributesCacheGenerator,
	RankException_t2421855710_CustomAttributesCacheGenerator,
	ResolveEventArgs_t960278168_CustomAttributesCacheGenerator,
	RuntimeMethodHandle_t2877888302_CustomAttributesCacheGenerator,
	RuntimeMethodHandle_t2877888302_CustomAttributesCacheGenerator_RuntimeMethodHandle_Equals_m2047619131,
	StackOverflowException_t2790203850_CustomAttributesCacheGenerator,
	StringComparer_t124344716_CustomAttributesCacheGenerator,
	StringComparison_t3628495144_CustomAttributesCacheGenerator,
	StringSplitOptions_t1539223423_CustomAttributesCacheGenerator,
	SystemException_t3635077273_CustomAttributesCacheGenerator,
	ThreadStaticAttribute_t174455095_CustomAttributesCacheGenerator,
	TimeSpan_t2986675223_CustomAttributesCacheGenerator,
	TimeZone_t2898390368_CustomAttributesCacheGenerator,
	TypeCode_t2052347577_CustomAttributesCacheGenerator,
	TypeInitializationException_t4108067351_CustomAttributesCacheGenerator,
	TypeLoadException_t2681750783_CustomAttributesCacheGenerator,
	UnauthorizedAccessException_t934748042_CustomAttributesCacheGenerator,
	UnhandledExceptionEventArgs_t3669884462_CustomAttributesCacheGenerator,
	UnhandledExceptionEventArgs_t3669884462_CustomAttributesCacheGenerator_UnhandledExceptionEventArgs_get_ExceptionObject_m2012398684,
	UnhandledExceptionEventArgs_t3669884462_CustomAttributesCacheGenerator_UnhandledExceptionEventArgs_get_IsTerminating_m3281711421,
	Version_t900644674_CustomAttributesCacheGenerator,
	WeakReference_t3998924468_CustomAttributesCacheGenerator,
	MemberFilter_t1197335030_CustomAttributesCacheGenerator,
	TypeFilter_t139072584_CustomAttributesCacheGenerator,
	CrossContextDelegate_t642168454_CustomAttributesCacheGenerator,
	HeaderHandler_t3384099523_CustomAttributesCacheGenerator,
	ThreadStart_t8027522_CustomAttributesCacheGenerator,
	TimerCallback_t2264714419_CustomAttributesCacheGenerator,
	WaitCallback_t3170229073_CustomAttributesCacheGenerator,
	AppDomainInitializer_t1811168608_CustomAttributesCacheGenerator,
	AssemblyLoadEventHandler_t877953269_CustomAttributesCacheGenerator,
	EventHandler_t2094584875_CustomAttributesCacheGenerator,
	ResolveEventHandler_t2027096794_CustomAttributesCacheGenerator,
	UnhandledExceptionEventHandler_t3129202591_CustomAttributesCacheGenerator,
	U3CPrivateImplementationDetailsU3E_t3250056442_CustomAttributesCacheGenerator,
	g_System_Assembly_CustomAttributesCacheGenerator,
	Locale_t2448440937_CustomAttributesCacheGenerator_Locale_GetText_m3635108345____args1,
	MonoTODOAttribute_t614642063_CustomAttributesCacheGenerator,
	Queue_1_t542587364_CustomAttributesCacheGenerator,
	Stack_1_t2711020695_CustomAttributesCacheGenerator,
	HybridDictionary_t1698560403_CustomAttributesCacheGenerator,
	ListDictionary_t1976214390_CustomAttributesCacheGenerator,
	NameObjectCollectionBase_t1648796668_CustomAttributesCacheGenerator_NameObjectCollectionBase_FindFirstMatchedItem_m2942906622,
	KeysCollection_t14636749_CustomAttributesCacheGenerator,
	NameValueCollection_t2484058343_CustomAttributesCacheGenerator,
	TypeConverter_t927856623_CustomAttributesCacheGenerator,
	TypeConverterAttribute_t553551222_CustomAttributesCacheGenerator,
	SslPolicyErrors_t2398368690_CustomAttributesCacheGenerator,
	FileWebRequest_t445810259_CustomAttributesCacheGenerator_FileWebRequest__ctor_m2545559098,
	FtpWebRequest_t1919013084_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache1C,
	FtpWebRequest_t1919013084_CustomAttributesCacheGenerator_FtpWebRequest_U3CcallbackU3Em__B_m1047401003,
	GlobalProxySelection_t2070377985_CustomAttributesCacheGenerator,
	HttpWebRequest_t3674333275_CustomAttributesCacheGenerator_HttpWebRequest__ctor_m3658280394,
	IPv6Address_t2439851581_CustomAttributesCacheGenerator,
	SecurityProtocolType_t26325249_CustomAttributesCacheGenerator,
	ServicePointManager_t86738999_CustomAttributesCacheGenerator_ServicePointManager_t86738999____CertificatePolicy_PropertyInfo,
	ServicePointManager_t86738999_CustomAttributesCacheGenerator_ServicePointManager_t86738999____CheckCertificateRevocationList_PropertyInfo,
	WebHeaderCollection_t2266125739_CustomAttributesCacheGenerator,
	WebRequest_t3724865973_CustomAttributesCacheGenerator_WebRequest_GetDefaultWebProxy_m291030002,
	OpenFlags_t3269158238_CustomAttributesCacheGenerator,
	PublicKey_t1535790316_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map9,
	X500DistinguishedName_t2651651201_CustomAttributesCacheGenerator,
	X500DistinguishedNameFlags_t2114626771_CustomAttributesCacheGenerator,
	X509Certificate2_t3206508497_CustomAttributesCacheGenerator_X509Certificate2_GetNameInfo_m3374661201,
	X509Certificate2_t3206508497_CustomAttributesCacheGenerator_X509Certificate2_Import_m2733344782,
	X509Certificate2_t3206508497_CustomAttributesCacheGenerator_X509Certificate2_Verify_m1956073113,
	X509Certificate2Collection_t1044733850_CustomAttributesCacheGenerator,
	X509Certificate2Collection_t1044733850_CustomAttributesCacheGenerator_X509Certificate2Collection_AddRange_m3259897552,
	X509Certificate2Collection_t1044733850_CustomAttributesCacheGenerator_X509Certificate2Collection_Find_m1361050413,
	X509CertificateCollection_t3853537301_CustomAttributesCacheGenerator,
	X509Chain_t3606654252_CustomAttributesCacheGenerator_U3CU3Ef__switchU24mapB,
	X509Chain_t3606654252_CustomAttributesCacheGenerator_U3CU3Ef__switchU24mapC,
	X509Chain_t3606654252_CustomAttributesCacheGenerator_U3CU3Ef__switchU24mapD,
	X509Chain_t3606654252_CustomAttributesCacheGenerator_X509Chain_Build_m3453334197,
	X509ChainElementCollection_t1402597378_CustomAttributesCacheGenerator,
	X509ChainStatusFlags_t2184184987_CustomAttributesCacheGenerator,
	X509EnhancedKeyUsageExtension_t4248079241_CustomAttributesCacheGenerator_U3CU3Ef__switchU24mapE,
	X509ExtensionCollection_t2858212424_CustomAttributesCacheGenerator,
	X509KeyUsageFlags_t1189859534_CustomAttributesCacheGenerator,
	X509Store_t2224578885_CustomAttributesCacheGenerator_U3CU3Ef__switchU24mapF,
	X509VerificationFlags_t1372417108_CustomAttributesCacheGenerator,
	AsnEncodedData_t1210003190_CustomAttributesCacheGenerator_U3CU3Ef__switchU24mapA,
	Oid_t3889750512_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map10,
	OidCollection_t2947752789_CustomAttributesCacheGenerator,
	CaptureCollection_t556975539_CustomAttributesCacheGenerator,
	GroupCollection_t310655671_CustomAttributesCacheGenerator,
	MatchCollection_t3986081353_CustomAttributesCacheGenerator,
	RegexOptions_t2920399184_CustomAttributesCacheGenerator,
	OpFlags_t3924773783_CustomAttributesCacheGenerator,
	IntervalCollection_t3277036616_CustomAttributesCacheGenerator,
	ExpressionCollection_t929422539_CustomAttributesCacheGenerator,
	Uri_t1131101833_CustomAttributesCacheGenerator,
	Uri_t1131101833_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map14,
	Uri_t1131101833_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map15,
	Uri_t1131101833_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map16,
	Uri_t1131101833_CustomAttributesCacheGenerator_Uri__ctor_m3032927579,
	Uri_t1131101833_CustomAttributesCacheGenerator_Uri_EscapeString_m4163751340,
	Uri_t1131101833_CustomAttributesCacheGenerator_Uri_Unescape_m2753918802,
	UriParser_t1608860109_CustomAttributesCacheGenerator_UriParser_OnRegister_m3456593883,
	U3CPrivateImplementationDetailsU3E_t3250056443_CustomAttributesCacheGenerator,
	g_Mono_Security_Assembly_CustomAttributesCacheGenerator,
	BigInteger_t1715200597_CustomAttributesCacheGenerator_BigInteger__ctor_m2308028736,
	BigInteger_t1715200597_CustomAttributesCacheGenerator_BigInteger__ctor_m615970763,
	BigInteger_t1715200597_CustomAttributesCacheGenerator_BigInteger__ctor_m243072073,
	BigInteger_t1715200597_CustomAttributesCacheGenerator_BigInteger_SetBit_m1692600095,
	BigInteger_t1715200597_CustomAttributesCacheGenerator_BigInteger_SetBit_m1046987020,
	BigInteger_t1715200597_CustomAttributesCacheGenerator_BigInteger_ToString_m1741067599,
	BigInteger_t1715200597_CustomAttributesCacheGenerator_BigInteger_ToString_m2080477611,
	BigInteger_t1715200597_CustomAttributesCacheGenerator_BigInteger_op_Implicit_m2994795725,
	BigInteger_t1715200597_CustomAttributesCacheGenerator_BigInteger_op_Modulus_m1177843781,
	BigInteger_t1715200597_CustomAttributesCacheGenerator_BigInteger_op_Equality_m4238039561,
	BigInteger_t1715200597_CustomAttributesCacheGenerator_BigInteger_op_Inequality_m1859198082,
	ModulusRing_t2599161149_CustomAttributesCacheGenerator_ModulusRing_Pow_m3064208772,
	ASN1_t450259933_CustomAttributesCacheGenerator,
	PKCS12_t2451188495_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map5,
	PKCS12_t2451188495_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map6,
	PKCS12_t2451188495_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map7,
	PKCS12_t2451188495_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map8,
	PKCS12_t2451188495_CustomAttributesCacheGenerator_U3CU3Ef__switchU24mapC,
	X509Certificate_t2235220323_CustomAttributesCacheGenerator_U3CU3Ef__switchU24mapF,
	X509Certificate_t2235220323_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map10,
	X509Certificate_t2235220323_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map11,
	X509CertificateCollection_t1815624851_CustomAttributesCacheGenerator,
	X509ChainStatusFlags_t991867662_CustomAttributesCacheGenerator,
	X509Crl_t2767467075_CustomAttributesCacheGenerator,
	X509Crl_t2767467075_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map13,
	X509ExtensionCollection_t3055523518_CustomAttributesCacheGenerator,
	ExtendedKeyUsageExtension_t450188887_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map14,
	KeyUsages_t3469123324_CustomAttributesCacheGenerator,
	CertTypes_t480212372_CustomAttributesCacheGenerator,
	CipherSuiteCollection_t3340801564_CustomAttributesCacheGenerator,
	HttpsClientStream_t1900802475_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache2,
	HttpsClientStream_t1900802475_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache3,
	HttpsClientStream_t1900802475_CustomAttributesCacheGenerator_HttpsClientStream_U3CHttpsClientStreamU3Em__0_m4013616965,
	HttpsClientStream_t1900802475_CustomAttributesCacheGenerator_HttpsClientStream_U3CHttpsClientStreamU3Em__1_m845478519,
	RSASslSignatureDeformatter_t3859776653_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map15,
	RSASslSignatureFormatter_t3826029953_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map16,
	SecurityProtocolType_t4172395504_CustomAttributesCacheGenerator,
	U3CPrivateImplementationDetailsU3E_t3250056444_CustomAttributesCacheGenerator,
	g_System_Core_Assembly_CustomAttributesCacheGenerator,
	ExtensionAttribute_t1721836986_CustomAttributesCacheGenerator,
	Locale_t2448440939_CustomAttributesCacheGenerator_Locale_GetText_m2618335777____args1,
	Enumerable_t1126428382_CustomAttributesCacheGenerator,
	Enumerable_t1126428382_CustomAttributesCacheGenerator_Enumerable_Any_m1770862959,
	Enumerable_t1126428382_CustomAttributesCacheGenerator_Enumerable_Where_m2049181036,
	Enumerable_t1126428382_CustomAttributesCacheGenerator_Enumerable_CreateWhereIterator_m1295165955,
	U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1910290221_CustomAttributesCacheGenerator,
	U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1910290221_CustomAttributesCacheGenerator_U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m3788286129,
	U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1910290221_CustomAttributesCacheGenerator_U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerator_get_Current_m1779517307,
	U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1910290221_CustomAttributesCacheGenerator_U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerable_GetEnumerator_m2163469919,
	U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1910290221_CustomAttributesCacheGenerator_U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m273512516,
	U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1910290221_CustomAttributesCacheGenerator_U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m1120635176,
	U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1910290221_CustomAttributesCacheGenerator_U3CCreateWhereIteratorU3Ec__Iterator1D_1_Reset_m2396585947,
	U3CPrivateImplementationDetailsU3E_t3250056445_CustomAttributesCacheGenerator,
	g_UnityEngine_CoreModule_Assembly_CustomAttributesCacheGenerator,
	Application_t2657524047_CustomAttributesCacheGenerator_lowMemory,
	Application_t2657524047_CustomAttributesCacheGenerator_onBeforeRender,
	Application_t2657524047_CustomAttributesCacheGenerator_Application_CallLowMemory_m1626726542,
	Application_t2657524047_CustomAttributesCacheGenerator_Application_CallLogCallback_m3808806632,
	Application_t2657524047_CustomAttributesCacheGenerator_Application_InvokeOnBeforeRender_m818804610,
	AssetBundleCreateRequest_t3985900355_CustomAttributesCacheGenerator,
	AssetBundleRequest_t2873072919_CustomAttributesCacheGenerator,
	AsyncOperation_t4233810744_CustomAttributesCacheGenerator,
	AsyncOperation_t4233810744_CustomAttributesCacheGenerator_AsyncOperation_InternalDestroy_m1747224711,
	AsyncOperation_t4233810744_CustomAttributesCacheGenerator_AsyncOperation_InvokeCompletionEvent_m737953261,
	WaitForSeconds_t727047372_CustomAttributesCacheGenerator,
	WaitForFixedUpdate_t870903979_CustomAttributesCacheGenerator,
	WaitForEndOfFrame_t337473950_CustomAttributesCacheGenerator,
	Coroutine_t3619068392_CustomAttributesCacheGenerator,
	Coroutine_t3619068392_CustomAttributesCacheGenerator_Coroutine_ReleaseCoroutine_m329501834,
	ScriptableObject_t476655885_CustomAttributesCacheGenerator,
	ScriptableObject_t476655885_CustomAttributesCacheGenerator_ScriptableObject_Internal_CreateScriptableObject_m3652231296,
	ScriptableObject_t476655885_CustomAttributesCacheGenerator_ScriptableObject_Internal_CreateScriptableObject_m3652231296____self0,
	ScriptableObject_t476655885_CustomAttributesCacheGenerator_ScriptableObject_CreateInstanceFromType_m1134094777,
	FailedToLoadScriptObject_t2431108707_CustomAttributesCacheGenerator,
	Behaviour_t4110946901_CustomAttributesCacheGenerator,
	Behaviour_t4110946901_CustomAttributesCacheGenerator_Behaviour_get_enabled_m1304335371,
	Camera_t101201881_CustomAttributesCacheGenerator,
	Camera_t101201881_CustomAttributesCacheGenerator_Camera_get_nearClipPlane_m167155657,
	Camera_t101201881_CustomAttributesCacheGenerator_Camera_get_farClipPlane_m1366251422,
	Camera_t101201881_CustomAttributesCacheGenerator_Camera_get_cullingMask_m2771596515,
	Camera_t101201881_CustomAttributesCacheGenerator_Camera_get_eventMask_m3958975071,
	Camera_t101201881_CustomAttributesCacheGenerator_Camera_INTERNAL_get_pixelRect_m3102036321,
	Camera_t101201881_CustomAttributesCacheGenerator_Camera_get_targetTexture_m903246907,
	Camera_t101201881_CustomAttributesCacheGenerator_Camera_get_clearFlags_m3577200564,
	Camera_t101201881_CustomAttributesCacheGenerator_Camera_INTERNAL_CALL_ScreenPointToRay_m244366202,
	Camera_t101201881_CustomAttributesCacheGenerator_Camera_get_allCamerasCount_m3527489092,
	Camera_t101201881_CustomAttributesCacheGenerator_Camera_GetAllCameras_m557431570,
	Camera_t101201881_CustomAttributesCacheGenerator_Camera_FireOnPreCull_m2186829015,
	Camera_t101201881_CustomAttributesCacheGenerator_Camera_FireOnPreRender_m1563217163,
	Camera_t101201881_CustomAttributesCacheGenerator_Camera_FireOnPostRender_m4290449375,
	Camera_t101201881_CustomAttributesCacheGenerator_Camera_INTERNAL_CALL_RaycastTry_m2903709223,
	Camera_t101201881_CustomAttributesCacheGenerator_Camera_INTERNAL_CALL_RaycastTry2D_m700194433,
	Component_t2160255836_CustomAttributesCacheGenerator,
	Component_t2160255836_CustomAttributesCacheGenerator_Component_get_gameObject_m2710707649,
	Component_t2160255836_CustomAttributesCacheGenerator_Component_GetComponentFastPath_m3479205869,
	Component_t2160255836_CustomAttributesCacheGenerator_Component_GetComponent_m685032955,
	UnhandledExceptionHandler_t2845673226_CustomAttributesCacheGenerator_U3CU3Ef__mgU24cache0,
	UnhandledExceptionHandler_t2845673226_CustomAttributesCacheGenerator_UnhandledExceptionHandler_RegisterUECatcher_m663990726,
	UnhandledExceptionHandler_t2845673226_CustomAttributesCacheGenerator_UnhandledExceptionHandler_NativeUnhandledExceptionHandler_m4249568766,
	CullingGroup_t1255328198_CustomAttributesCacheGenerator_CullingGroup_Dispose_m1436928443,
	CullingGroup_t1255328198_CustomAttributesCacheGenerator_CullingGroup_SendEvents_m3781512400,
	CullingGroup_t1255328198_CustomAttributesCacheGenerator_CullingGroup_FinalizerFailure_m3512716862,
	DebugLogHandler_t1401667417_CustomAttributesCacheGenerator_DebugLogHandler_Internal_Log_m1682417589,
	DebugLogHandler_t1401667417_CustomAttributesCacheGenerator_DebugLogHandler_Internal_Log_m1682417589____obj2,
	DebugLogHandler_t1401667417_CustomAttributesCacheGenerator_DebugLogHandler_Internal_LogException_m3264502143,
	DebugLogHandler_t1401667417_CustomAttributesCacheGenerator_DebugLogHandler_Internal_LogException_m3264502143____obj1,
	DebugLogHandler_t1401667417_CustomAttributesCacheGenerator_DebugLogHandler_LogFormat_m3907003566____args3,
	Display_t4002355288_CustomAttributesCacheGenerator,
	Display_t4002355288_CustomAttributesCacheGenerator_onDisplaysUpdated,
	Display_t4002355288_CustomAttributesCacheGenerator_Display_RecreateDisplayList_m3512407007,
	Display_t4002355288_CustomAttributesCacheGenerator_Display_FireDisplaysUpdated_m3194723814,
	GameObject_t3732643618_CustomAttributesCacheGenerator_GameObject_SendMessage_m239132133,
	GameObject_t3732643618_CustomAttributesCacheGenerator_GameObject_SendMessage_m239132133____value1,
	GameObject_t3732643618_CustomAttributesCacheGenerator_GameObject_SendMessage_m239132133____options2,
	GameObject_t3732643618_CustomAttributesCacheGenerator_GameObject_Internal_CreateGameObject_m1823697062,
	GameObject_t3732643618_CustomAttributesCacheGenerator_GameObject_Internal_CreateGameObject_m1823697062____mono0,
	Gradient_t130030130_CustomAttributesCacheGenerator,
	Gradient_t130030130_CustomAttributesCacheGenerator_Gradient__ctor_m2009183950,
	Gradient_t130030130_CustomAttributesCacheGenerator_Gradient_Init_m1188969969,
	Gradient_t130030130_CustomAttributesCacheGenerator_Gradient_Cleanup_m2195794132,
	GUIElement_t1292848738_CustomAttributesCacheGenerator,
	GUILayer_t1929752731_CustomAttributesCacheGenerator,
	GUILayer_t1929752731_CustomAttributesCacheGenerator_GUILayer_INTERNAL_CALL_HitTest_m422396558,
	Input_t2650329352_CustomAttributesCacheGenerator_Input_GetMouseButton_m294091231,
	Input_t2650329352_CustomAttributesCacheGenerator_Input_GetMouseButtonDown_m3867833682,
	Input_t2650329352_CustomAttributesCacheGenerator_Input_INTERNAL_get_mousePosition_m3613716531,
	Vector3_t3239555143_CustomAttributesCacheGenerator,
	Matrix4x4_t3534180298_CustomAttributesCacheGenerator,
	Keyframe_t3396755990_CustomAttributesCacheGenerator,
	AnimationCurve_t1726276845_CustomAttributesCacheGenerator,
	AnimationCurve_t1726276845_CustomAttributesCacheGenerator_AnimationCurve__ctor_m3076316537____keys0,
	AnimationCurve_t1726276845_CustomAttributesCacheGenerator_AnimationCurve__ctor_m566668622,
	AnimationCurve_t1726276845_CustomAttributesCacheGenerator_AnimationCurve_Cleanup_m243787505,
	AnimationCurve_t1726276845_CustomAttributesCacheGenerator_AnimationCurve_Init_m1956642396,
	MonoBehaviour_t2904604993_CustomAttributesCacheGenerator,
	ResourceRequest_t3543572403_CustomAttributesCacheGenerator,
	Texture_t238546388_CustomAttributesCacheGenerator,
	Texture2D_t359445654_CustomAttributesCacheGenerator_Texture2D_Internal_Create_m1993173660,
	Texture2D_t359445654_CustomAttributesCacheGenerator_Texture2D_Internal_Create_m1993173660____mono0,
	RenderTexture_t2667189599_CustomAttributesCacheGenerator,
	HideFlags_t1155951039_CustomAttributesCacheGenerator,
	Object_t2461733472_CustomAttributesCacheGenerator,
	Object_t2461733472_CustomAttributesCacheGenerator_Object_set_hideFlags_m3010783544,
	Object_t2461733472_CustomAttributesCacheGenerator_Object_ToString_m842583452,
	UnityLogWriter_t3231359731_CustomAttributesCacheGenerator_UnityLogWriter_WriteStringToUnityLog_m237899344,
	YieldInstruction_t3999321895_CustomAttributesCacheGenerator,
	PlayableHandle_t3938927722_CustomAttributesCacheGenerator,
	Playable_t1760619938_CustomAttributesCacheGenerator,
	PlayableGraph_t1897216728_CustomAttributesCacheGenerator,
	PlayableOutputHandle_t2719521870_CustomAttributesCacheGenerator,
	PlayableOutput_t3840687608_CustomAttributesCacheGenerator,
	LocalNotification_t2133447199_CustomAttributesCacheGenerator,
	LocalNotification_t2133447199_CustomAttributesCacheGenerator_LocalNotification_Destroy_m1982161855,
	RemoteNotification_t414806740_CustomAttributesCacheGenerator,
	RemoteNotification_t414806740_CustomAttributesCacheGenerator_RemoteNotification_Destroy_m4186758914,
	SceneManager_t2258132803_CustomAttributesCacheGenerator,
	SceneManager_t2258132803_CustomAttributesCacheGenerator_sceneLoaded,
	SceneManager_t2258132803_CustomAttributesCacheGenerator_sceneUnloaded,
	SceneManager_t2258132803_CustomAttributesCacheGenerator_activeSceneChanged,
	SceneManager_t2258132803_CustomAttributesCacheGenerator_SceneManager_Internal_SceneLoaded_m2309132009,
	SceneManager_t2258132803_CustomAttributesCacheGenerator_SceneManager_Internal_SceneUnloaded_m2410300733,
	SceneManager_t2258132803_CustomAttributesCacheGenerator_SceneManager_Internal_ActiveSceneChanged_m667757325,
	Transform_t94239816_CustomAttributesCacheGenerator_Transform_get_childCount_m849482613,
	Transform_t94239816_CustomAttributesCacheGenerator_Transform_GetChild_m4117681253,
	RectTransform_t3950565364_CustomAttributesCacheGenerator,
	RectTransform_t3950565364_CustomAttributesCacheGenerator_reapplyDrivenProperties,
	RectTransform_t3950565364_CustomAttributesCacheGenerator_RectTransform_SendReapplyDrivenProperties_m2257690066,
	SpriteAtlasManager_t1881977649_CustomAttributesCacheGenerator_atlasRequested,
	SpriteAtlasManager_t1881977649_CustomAttributesCacheGenerator_U3CU3Ef__mgU24cache0,
	SpriteAtlasManager_t1881977649_CustomAttributesCacheGenerator_SpriteAtlasManager_RequestAtlas_m2195264182,
	SpriteAtlasManager_t1881977649_CustomAttributesCacheGenerator_SpriteAtlasManager_Register_m1381418004,
	AttributeHelperEngine_t3367628457_CustomAttributesCacheGenerator_AttributeHelperEngine_GetParentTypeDisallowingMultipleInclusion_m1021838127,
	AttributeHelperEngine_t3367628457_CustomAttributesCacheGenerator_AttributeHelperEngine_GetRequiredComponents_m2091293497,
	AttributeHelperEngine_t3367628457_CustomAttributesCacheGenerator_AttributeHelperEngine_CheckIsEditorScript_m2212222995,
	AttributeHelperEngine_t3367628457_CustomAttributesCacheGenerator_AttributeHelperEngine_GetDefaultExecutionOrderFor_m1362990272,
	DisallowMultipleComponent_t2957315911_CustomAttributesCacheGenerator,
	RequireComponent_t597737523_CustomAttributesCacheGenerator,
	ContextMenu_t3503312229_CustomAttributesCacheGenerator,
	DefaultExecutionOrder_t3862449190_CustomAttributesCacheGenerator,
	DefaultExecutionOrder_t3862449190_CustomAttributesCacheGenerator_U3CorderU3Ek__BackingField,
	DefaultExecutionOrder_t3862449190_CustomAttributesCacheGenerator_DefaultExecutionOrder_get_order_m3989798846,
	NativeClassAttribute_t3085471410_CustomAttributesCacheGenerator,
	NativeClassAttribute_t3085471410_CustomAttributesCacheGenerator_U3CQualifiedNativeNameU3Ek__BackingField,
	NativeClassAttribute_t3085471410_CustomAttributesCacheGenerator_NativeClassAttribute_set_QualifiedNativeName_m875392448,
	AssemblyIsEditorAssembly_t958181164_CustomAttributesCacheGenerator,
	WritableAttribute_t2474002418_CustomAttributesCacheGenerator,
	ClassLibraryInitializer_t4091167741_CustomAttributesCacheGenerator_ClassLibraryInitializer_Init_m4274939016,
	Color_t4024113822_CustomAttributesCacheGenerator,
	SetupCoroutine_t912333859_CustomAttributesCacheGenerator,
	SetupCoroutine_t912333859_CustomAttributesCacheGenerator_SetupCoroutine_InvokeMoveNext_m1274895524,
	SetupCoroutine_t912333859_CustomAttributesCacheGenerator_SetupCoroutine_InvokeMember_m3346816490,
	ManagedStreamHelpers_t1729740455_CustomAttributesCacheGenerator_ManagedStreamHelpers_ManagedStreamRead_m4024070239,
	ManagedStreamHelpers_t1729740455_CustomAttributesCacheGenerator_ManagedStreamHelpers_ManagedStreamSeek_m504681536,
	ManagedStreamHelpers_t1729740455_CustomAttributesCacheGenerator_ManagedStreamHelpers_ManagedStreamLength_m1632106524,
	SendMouseEvents_t2410128589_CustomAttributesCacheGenerator_SendMouseEvents_SetMouseMoved_m2508391903,
	SendMouseEvents_t2410128589_CustomAttributesCacheGenerator_SendMouseEvents_DoSendMouseEvents_m3686682366,
	PropertyNameUtils_t1618685890_CustomAttributesCacheGenerator_PropertyNameUtils_PropertyNameFromString_m3539940257____name0,
	PropertyName_t666104458_CustomAttributesCacheGenerator,
	Rect_t1111852907_CustomAttributesCacheGenerator,
	SerializePrivateVariables_t1291349895_CustomAttributesCacheGenerator,
	SerializeField_t1393754034_CustomAttributesCacheGenerator,
	PreferBinarySerialization_t2730910840_CustomAttributesCacheGenerator,
	ISerializationCallbackReceiver_t859186888_CustomAttributesCacheGenerator,
	ISerializationCallbackReceiver_t859186888_CustomAttributesCacheGenerator_ISerializationCallbackReceiver_OnBeforeSerialize_m1887418163,
	ISerializationCallbackReceiver_t859186888_CustomAttributesCacheGenerator_ISerializationCallbackReceiver_OnAfterDeserialize_m173705070,
	StackTraceUtility_t787969096_CustomAttributesCacheGenerator_StackTraceUtility_SetProjectFolder_m3603953530,
	StackTraceUtility_t787969096_CustomAttributesCacheGenerator_StackTraceUtility_ExtractStackTrace_m1291404532,
	StackTraceUtility_t787969096_CustomAttributesCacheGenerator_StackTraceUtility_ExtractStringFromExceptionInternal_m3699450383,
	StackTraceUtility_t787969096_CustomAttributesCacheGenerator_StackTraceUtility_PostprocessStacktrace_m4246116829,
	StackTraceUtility_t787969096_CustomAttributesCacheGenerator_StackTraceUtility_ExtractFormattedStackTrace_m382049542,
	UnityException_t760375625_CustomAttributesCacheGenerator,
	ArgumentCache_t3682645796_CustomAttributesCacheGenerator_m_ObjectArgument,
	ArgumentCache_t3682645796_CustomAttributesCacheGenerator_m_ObjectArgumentAssemblyTypeName,
	ArgumentCache_t3682645796_CustomAttributesCacheGenerator_m_IntArgument,
	ArgumentCache_t3682645796_CustomAttributesCacheGenerator_m_FloatArgument,
	ArgumentCache_t3682645796_CustomAttributesCacheGenerator_m_StringArgument,
	ArgumentCache_t3682645796_CustomAttributesCacheGenerator_m_BoolArgument,
	InvokableCall_t2876658856_CustomAttributesCacheGenerator_Delegate,
	InvokableCall_1_t1604090555_CustomAttributesCacheGenerator_Delegate,
	InvokableCall_2_t3976188983_CustomAttributesCacheGenerator_Delegate,
	InvokableCall_3_t3917817895_CustomAttributesCacheGenerator_Delegate,
	InvokableCall_4_t2387264047_CustomAttributesCacheGenerator_Delegate,
	PersistentCall_t2687057912_CustomAttributesCacheGenerator_m_Target,
	PersistentCall_t2687057912_CustomAttributesCacheGenerator_m_MethodName,
	PersistentCall_t2687057912_CustomAttributesCacheGenerator_m_Mode,
	PersistentCall_t2687057912_CustomAttributesCacheGenerator_m_Arguments,
	PersistentCall_t2687057912_CustomAttributesCacheGenerator_m_CallState,
	PersistentCallGroup_t586105043_CustomAttributesCacheGenerator_m_Calls,
	UnityEventBase_t3171537072_CustomAttributesCacheGenerator,
	UnityEventBase_t3171537072_CustomAttributesCacheGenerator_m_PersistentCalls,
	UnityEventBase_t3171537072_CustomAttributesCacheGenerator_m_TypeName,
	UnityEvent_t1464431504_CustomAttributesCacheGenerator_UnityEvent__ctor_m811779962,
	UnityEvent_1_t4023850743_CustomAttributesCacheGenerator_UnityEvent_1__ctor_m3496433407,
	UnityEvent_2_t1533051435_CustomAttributesCacheGenerator_UnityEvent_2__ctor_m565043222,
	UnityEvent_3_t4091795572_CustomAttributesCacheGenerator_UnityEvent_3__ctor_m1441002641,
	UnityEvent_4_t1002767761_CustomAttributesCacheGenerator_UnityEvent_4__ctor_m3177569170,
	UnityString_t2219893151_CustomAttributesCacheGenerator_UnityString_Format_m2513837565____args1,
	UnitySynchronizationContext_t90163264_CustomAttributesCacheGenerator_UnitySynchronizationContext_InitializeSynchronizationContext_m2662403408,
	UnitySynchronizationContext_t90163264_CustomAttributesCacheGenerator_UnitySynchronizationContext_ExecuteTasks_m3600858646,
	Vector4_t4052901136_CustomAttributesCacheGenerator,
	UnmarshalledAttribute_t2695814082_CustomAttributesCacheGenerator,
	ReadOnlyAttribute_t3185780063_CustomAttributesCacheGenerator,
	ReadWriteAttribute_t1843161347_CustomAttributesCacheGenerator,
	WriteOnlyAttribute_t1011568951_CustomAttributesCacheGenerator,
	DeallocateOnJobCompletionAttribute_t2226375310_CustomAttributesCacheGenerator,
	NativeContainerAttribute_t1940066792_CustomAttributesCacheGenerator,
	NativeContainerSupportsAtomicWriteAttribute_t1199220324_CustomAttributesCacheGenerator,
	NativeContainerSupportsMinMaxWriteRestrictionAttribute_t3950250497_CustomAttributesCacheGenerator,
	PlayableBinding_t136667248_CustomAttributesCacheGenerator_U3CstreamNameU3Ek__BackingField,
	PlayableBinding_t136667248_CustomAttributesCacheGenerator_U3CstreamTypeU3Ek__BackingField,
	PlayableBinding_t136667248_CustomAttributesCacheGenerator_U3CsourceObjectU3Ek__BackingField,
	PlayableBinding_t136667248_CustomAttributesCacheGenerator_U3CsourceBindingTypeU3Ek__BackingField,
	PlayableAsset_t2436284446_CustomAttributesCacheGenerator,
	PlayableAsset_t2436284446_CustomAttributesCacheGenerator_PlayableAsset_Internal_CreatePlayable_m4149673157,
	PlayableAsset_t2436284446_CustomAttributesCacheGenerator_PlayableAsset_Internal_GetPlayableAssetDuration_m3126328874,
	PlayableBehaviour_t4011616635_CustomAttributesCacheGenerator,
	ScriptPlayableOutput_t149882980_CustomAttributesCacheGenerator,
	DefaultValueAttribute_t2693326634_CustomAttributesCacheGenerator,
	ILogHandler_t2217299658_CustomAttributesCacheGenerator_ILogHandler_LogFormat_m3088184411____args3,
	Logger_t605719344_CustomAttributesCacheGenerator_U3ClogHandlerU3Ek__BackingField,
	Logger_t605719344_CustomAttributesCacheGenerator_U3ClogEnabledU3Ek__BackingField,
	Logger_t605719344_CustomAttributesCacheGenerator_U3CfilterLogTypeU3Ek__BackingField,
	Logger_t605719344_CustomAttributesCacheGenerator_Logger_get_logHandler_m1719487699,
	Logger_t605719344_CustomAttributesCacheGenerator_Logger_set_logHandler_m6066634,
	Logger_t605719344_CustomAttributesCacheGenerator_Logger_get_logEnabled_m2290808316,
	Logger_t605719344_CustomAttributesCacheGenerator_Logger_set_logEnabled_m1662937540,
	Logger_t605719344_CustomAttributesCacheGenerator_Logger_get_filterLogType_m426955796,
	Logger_t605719344_CustomAttributesCacheGenerator_Logger_set_filterLogType_m2745985065,
	Logger_t605719344_CustomAttributesCacheGenerator_Logger_LogFormat_m1581286573____args3,
	PlayerConnection_t3559457672_CustomAttributesCacheGenerator_m_PlayerEditorConnectionEvents,
	PlayerConnection_t3559457672_CustomAttributesCacheGenerator_m_connectedPlayers,
	PlayerConnection_t3559457672_CustomAttributesCacheGenerator_PlayerConnection_MessageCallbackInternal_m469132778,
	PlayerConnection_t3559457672_CustomAttributesCacheGenerator_PlayerConnection_ConnectedCallbackInternal_m1135070508,
	PlayerConnection_t3559457672_CustomAttributesCacheGenerator_PlayerConnection_DisconnectedCallback_m2631702132,
	PlayerEditorConnectionEvents_t2151463461_CustomAttributesCacheGenerator_messageTypeSubscribers,
	PlayerEditorConnectionEvents_t2151463461_CustomAttributesCacheGenerator_connectionEvent,
	PlayerEditorConnectionEvents_t2151463461_CustomAttributesCacheGenerator_disconnectionEvent,
	MessageTypeSubscribers_t1657992506_CustomAttributesCacheGenerator_m_messageTypeId,
	U3CInvokeMessageIdSubscribersU3Ec__AnonStorey0_t1928087868_CustomAttributesCacheGenerator,
	RenderPipelineManager_t3681178706_CustomAttributesCacheGenerator_U3CcurrentPipelineU3Ek__BackingField,
	RenderPipelineManager_t3681178706_CustomAttributesCacheGenerator_RenderPipelineManager_get_currentPipeline_m3103495424,
	RenderPipelineManager_t3681178706_CustomAttributesCacheGenerator_RenderPipelineManager_set_currentPipeline_m3602131277,
	RenderPipelineManager_t3681178706_CustomAttributesCacheGenerator_RenderPipelineManager_CleanupRenderPipeline_m309528094,
	RenderPipelineManager_t3681178706_CustomAttributesCacheGenerator_RenderPipelineManager_DoRenderLoop_Internal_m124400281,
	UsedByNativeCodeAttribute_t2783261251_CustomAttributesCacheGenerator,
	RequiredByNativeCodeAttribute_t3603024077_CustomAttributesCacheGenerator,
	FormerlySerializedAsAttribute_t1489703117_CustomAttributesCacheGenerator,
	NetFxCoreExtensions_t2215717558_CustomAttributesCacheGenerator,
	NetFxCoreExtensions_t2215717558_CustomAttributesCacheGenerator_NetFxCoreExtensions_CreateDelegate_m3544893108,
	CSSMeasureFunc_t2162796896_CustomAttributesCacheGenerator,
	Native_t4028515775_CustomAttributesCacheGenerator_Native_CSSNodeMeasureInvoke_m1142609737,
	g_UnityEngine_AudioModule_Assembly_CustomAttributesCacheGenerator,
	AudioSettings_t2831187810_CustomAttributesCacheGenerator_OnAudioConfigurationChanged,
	AudioSettings_t2831187810_CustomAttributesCacheGenerator_AudioSettings_GetSpatializerPluginName_m2848677236,
	AudioSettings_t2831187810_CustomAttributesCacheGenerator_AudioSettings_InvokeOnAudioConfigurationChanged_m3184710774,
	AudioSettings_t2831187810_CustomAttributesCacheGenerator_AudioSettings_InvokeOnAudioManagerUpdate_m2154005943,
	AudioSettings_t2831187810_CustomAttributesCacheGenerator_AudioSettings_InvokeOnAudioSourcePlay_m2060412953,
	AudioSettings_t2831187810_CustomAttributesCacheGenerator_AudioSettings_GetAmbisonicDecoderPluginName_m70128080,
	AudioExtensionManager_t3128192687_CustomAttributesCacheGenerator_AudioExtensionManager_GetAudioListener_m3189978351,
	AudioClip_t961166807_CustomAttributesCacheGenerator_m_PCMReaderCallback,
	AudioClip_t961166807_CustomAttributesCacheGenerator_m_PCMSetPositionCallback,
	AudioClip_t961166807_CustomAttributesCacheGenerator_AudioClip_get_ambisonic_m2163265490,
	AudioClip_t961166807_CustomAttributesCacheGenerator_AudioClip_InvokePCMReaderCallback_Internal_m779593384,
	AudioClip_t961166807_CustomAttributesCacheGenerator_AudioClip_InvokePCMSetPositionCallback_Internal_m1609205883,
	AudioListener_t3360289753_CustomAttributesCacheGenerator,
	AudioListener_t3360289753_CustomAttributesCacheGenerator_AudioListener_GetNumExtensionProperties_m2279481642,
	AudioListener_t3360289753_CustomAttributesCacheGenerator_AudioListener_INTERNAL_CALL_ReadExtensionName_m3825001381,
	AudioListener_t3360289753_CustomAttributesCacheGenerator_AudioListener_INTERNAL_CALL_ReadExtensionPropertyName_m3514206420,
	AudioListener_t3360289753_CustomAttributesCacheGenerator_AudioListener_ReadExtensionPropertyValue_m2214914024,
	AudioListener_t3360289753_CustomAttributesCacheGenerator_AudioListener_INTERNAL_CALL_ClearExtensionProperties_m1492170042,
	AudioSource_t3827284957_CustomAttributesCacheGenerator,
	AudioSource_t3827284957_CustomAttributesCacheGenerator_AudioSource_get_clip_m2340175708,
	AudioSource_t3827284957_CustomAttributesCacheGenerator_AudioSource_get_isPlaying_m1796357815,
	AudioSource_t3827284957_CustomAttributesCacheGenerator_AudioSource_get_spatializeInternal_m3026556515,
	AudioSource_t3827284957_CustomAttributesCacheGenerator_AudioSource_GetNumExtensionProperties_m709284950,
	AudioSource_t3827284957_CustomAttributesCacheGenerator_AudioSource_INTERNAL_CALL_ReadExtensionName_m940874956,
	AudioSource_t3827284957_CustomAttributesCacheGenerator_AudioSource_INTERNAL_CALL_ReadExtensionPropertyName_m1096137561,
	AudioSource_t3827284957_CustomAttributesCacheGenerator_AudioSource_ReadExtensionPropertyValue_m1555844817,
	AudioSource_t3827284957_CustomAttributesCacheGenerator_AudioSource_INTERNAL_CALL_ClearExtensionProperties_m3670533364,
	AudioSource_t3827284957_CustomAttributesCacheGenerator_AudioSource_t3827284957____clip_PropertyInfo,
	AudioMixerPlayable_t1017334687_CustomAttributesCacheGenerator,
	AudioClipPlayable_t3812371941_CustomAttributesCacheGenerator,
	AudioPlayableOutput_t3386959622_CustomAttributesCacheGenerator,
	AudioListenerExtension_t75428788_CustomAttributesCacheGenerator_m_audioListener,
	AudioSourceExtension_t3778551013_CustomAttributesCacheGenerator_m_audioSource,
	g_UnityEngine_GameCenterModule_Assembly_CustomAttributesCacheGenerator,
	GameCenterPlatform_t366411994_CustomAttributesCacheGenerator,
	GameCenterPlatform_t366411994_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_Authenticate_m2072365377,
	GameCenterPlatform_t366411994_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_Authenticated_m2873448009,
	GameCenterPlatform_t366411994_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_UserName_m2866948064,
	GameCenterPlatform_t366411994_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_UserID_m1391210441,
	GameCenterPlatform_t366411994_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_Underage_m3153493531,
	GameCenterPlatform_t366411994_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_UserImage_m3635042813,
	GameCenterPlatform_t366411994_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_LoadFriends_m2096768347,
	GameCenterPlatform_t366411994_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_LoadAchievementDescriptions_m4285687138,
	GameCenterPlatform_t366411994_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_LoadAchievements_m1476802759,
	GameCenterPlatform_t366411994_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_ReportProgress_m4247572756,
	GameCenterPlatform_t366411994_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_ReportScore_m3392134640,
	GameCenterPlatform_t366411994_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_LoadScores_m3934381776,
	GameCenterPlatform_t366411994_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_ShowAchievementsUI_m2604671237,
	GameCenterPlatform_t366411994_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_ShowLeaderboardUI_m2609988910,
	GameCenterPlatform_t366411994_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_LoadUsers_m2376904235,
	GameCenterPlatform_t366411994_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_ResetAllAchievements_m860406903,
	GameCenterPlatform_t366411994_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_ShowDefaultAchievementBanner_m3807438363,
	GameCenterPlatform_t366411994_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_ShowSpecificLeaderboardUI_m1324819955,
	GameCenterPlatform_t366411994_CustomAttributesCacheGenerator_GameCenterPlatform_ClearAchievementDescriptions_m1201462552,
	GameCenterPlatform_t366411994_CustomAttributesCacheGenerator_GameCenterPlatform_SetAchievementDescription_m3265147368,
	GameCenterPlatform_t366411994_CustomAttributesCacheGenerator_GameCenterPlatform_SetAchievementDescriptionImage_m433180119,
	GameCenterPlatform_t366411994_CustomAttributesCacheGenerator_GameCenterPlatform_TriggerAchievementDescriptionCallback_m1368418878,
	GameCenterPlatform_t366411994_CustomAttributesCacheGenerator_GameCenterPlatform_AuthenticateCallbackWrapper_m3518532938,
	GameCenterPlatform_t366411994_CustomAttributesCacheGenerator_GameCenterPlatform_ClearFriends_m3997297748,
	GameCenterPlatform_t366411994_CustomAttributesCacheGenerator_GameCenterPlatform_SetFriends_m3156453279,
	GameCenterPlatform_t366411994_CustomAttributesCacheGenerator_GameCenterPlatform_SetFriendImage_m2565530221,
	GameCenterPlatform_t366411994_CustomAttributesCacheGenerator_GameCenterPlatform_TriggerFriendsCallbackWrapper_m3549927236,
	GameCenterPlatform_t366411994_CustomAttributesCacheGenerator_GameCenterPlatform_AchievementCallbackWrapper_m2393842948,
	GameCenterPlatform_t366411994_CustomAttributesCacheGenerator_GameCenterPlatform_ProgressCallbackWrapper_m1761357634,
	GameCenterPlatform_t366411994_CustomAttributesCacheGenerator_GameCenterPlatform_ScoreCallbackWrapper_m1017495084,
	GameCenterPlatform_t366411994_CustomAttributesCacheGenerator_GameCenterPlatform_ScoreLoaderCallbackWrapper_m2497892497,
	GameCenterPlatform_t366411994_CustomAttributesCacheGenerator_GameCenterPlatform_PopulateLocalUser_m3608849642,
	GameCenterPlatform_t366411994_CustomAttributesCacheGenerator_GameCenterPlatform_LeaderboardCallbackWrapper_m1512666017,
	GameCenterPlatform_t366411994_CustomAttributesCacheGenerator_GameCenterPlatform_ClearUsers_m694297429,
	GameCenterPlatform_t366411994_CustomAttributesCacheGenerator_GameCenterPlatform_SetUser_m1396993377,
	GameCenterPlatform_t366411994_CustomAttributesCacheGenerator_GameCenterPlatform_SetUserImage_m102480673,
	GameCenterPlatform_t366411994_CustomAttributesCacheGenerator_GameCenterPlatform_TriggerUsersCallbackWrapper_m741337649,
	GameCenterPlatform_t366411994_CustomAttributesCacheGenerator_GameCenterPlatform_SafeSetUserImage_m4217127318,
	GameCenterPlatform_t366411994_CustomAttributesCacheGenerator_GameCenterPlatform_TriggerResetAchievementCallback_m1901797103,
	U3CUnityEngine_SocialPlatforms_ISocialPlatform_AuthenticateU3Ec__AnonStorey0_t3375275809_CustomAttributesCacheGenerator,
	GcLeaderboard_t617530396_CustomAttributesCacheGenerator_GcLeaderboard_Internal_LoadScores_m30788333,
	GcLeaderboard_t617530396_CustomAttributesCacheGenerator_GcLeaderboard_Loading_m2486631462,
	GcLeaderboard_t617530396_CustomAttributesCacheGenerator_GcLeaderboard_Dispose_m4178548762,
	GcUserProfileData_t3262991188_CustomAttributesCacheGenerator,
	GcAchievementDescriptionData_t2898970914_CustomAttributesCacheGenerator,
	GcAchievementData_t3167706508_CustomAttributesCacheGenerator,
	GcScoreData_t3955401213_CustomAttributesCacheGenerator,
	Achievement_t2517894267_CustomAttributesCacheGenerator_U3CidU3Ek__BackingField,
	Achievement_t2517894267_CustomAttributesCacheGenerator_U3CpercentCompletedU3Ek__BackingField,
	Achievement_t2517894267_CustomAttributesCacheGenerator_Achievement_get_id_m352935039,
	Achievement_t2517894267_CustomAttributesCacheGenerator_Achievement_set_id_m2592364518,
	Achievement_t2517894267_CustomAttributesCacheGenerator_Achievement_get_percentCompleted_m3640585090,
	Achievement_t2517894267_CustomAttributesCacheGenerator_Achievement_set_percentCompleted_m1848274796,
	AchievementDescription_t130800866_CustomAttributesCacheGenerator_U3CidU3Ek__BackingField,
	AchievementDescription_t130800866_CustomAttributesCacheGenerator_AchievementDescription_get_id_m1271036810,
	AchievementDescription_t130800866_CustomAttributesCacheGenerator_AchievementDescription_set_id_m3879160604,
	Score_t1135675058_CustomAttributesCacheGenerator_U3CleaderboardIDU3Ek__BackingField,
	Score_t1135675058_CustomAttributesCacheGenerator_U3CvalueU3Ek__BackingField,
	Score_t1135675058_CustomAttributesCacheGenerator_Score_get_leaderboardID_m274030957,
	Score_t1135675058_CustomAttributesCacheGenerator_Score_set_leaderboardID_m2942345544,
	Score_t1135675058_CustomAttributesCacheGenerator_Score_get_value_m3908827736,
	Score_t1135675058_CustomAttributesCacheGenerator_Score_set_value_m2016376629,
	Leaderboard_t3421936738_CustomAttributesCacheGenerator_U3CidU3Ek__BackingField,
	Leaderboard_t3421936738_CustomAttributesCacheGenerator_U3CuserScopeU3Ek__BackingField,
	Leaderboard_t3421936738_CustomAttributesCacheGenerator_U3CrangeU3Ek__BackingField,
	Leaderboard_t3421936738_CustomAttributesCacheGenerator_U3CtimeScopeU3Ek__BackingField,
	Leaderboard_t3421936738_CustomAttributesCacheGenerator_Leaderboard_get_id_m523269624,
	Leaderboard_t3421936738_CustomAttributesCacheGenerator_Leaderboard_set_id_m1452185904,
	Leaderboard_t3421936738_CustomAttributesCacheGenerator_Leaderboard_get_userScope_m3651500032,
	Leaderboard_t3421936738_CustomAttributesCacheGenerator_Leaderboard_set_userScope_m1479044752,
	Leaderboard_t3421936738_CustomAttributesCacheGenerator_Leaderboard_get_range_m4014757670,
	Leaderboard_t3421936738_CustomAttributesCacheGenerator_Leaderboard_set_range_m3228718273,
	Leaderboard_t3421936738_CustomAttributesCacheGenerator_Leaderboard_get_timeScope_m2150671680,
	Leaderboard_t3421936738_CustomAttributesCacheGenerator_Leaderboard_set_timeScope_m1836561867,
	g_UnityEngine_TilemapModule_Assembly_CustomAttributesCacheGenerator,
	TileFlags_t2545855820_CustomAttributesCacheGenerator,
	Tile_t160380049_CustomAttributesCacheGenerator,
	Tile_t160380049_CustomAttributesCacheGenerator_m_Color,
	Tile_t160380049_CustomAttributesCacheGenerator_m_Transform,
	Tile_t160380049_CustomAttributesCacheGenerator_m_Flags,
	Tile_t160380049_CustomAttributesCacheGenerator_m_ColliderType,
	TileBase_t2845917970_CustomAttributesCacheGenerator,
	g_UnityEngine_UnityAnalyticsModule_Assembly_CustomAttributesCacheGenerator,
	CustomEventData_t868048441_CustomAttributesCacheGenerator_CustomEventData_InternalCreate_m782104979,
	CustomEventData_t868048441_CustomAttributesCacheGenerator_CustomEventData_InternalDestroy_m998346656,
	CustomEventData_t868048441_CustomAttributesCacheGenerator_CustomEventData_AddString_m2143902135,
	CustomEventData_t868048441_CustomAttributesCacheGenerator_CustomEventData_AddBool_m436037313,
	CustomEventData_t868048441_CustomAttributesCacheGenerator_CustomEventData_AddChar_m2437065113,
	CustomEventData_t868048441_CustomAttributesCacheGenerator_CustomEventData_AddByte_m210020965,
	CustomEventData_t868048441_CustomAttributesCacheGenerator_CustomEventData_AddSByte_m3889282772,
	CustomEventData_t868048441_CustomAttributesCacheGenerator_CustomEventData_AddInt16_m1607163544,
	CustomEventData_t868048441_CustomAttributesCacheGenerator_CustomEventData_AddUInt16_m3424486269,
	CustomEventData_t868048441_CustomAttributesCacheGenerator_CustomEventData_AddInt32_m2136872868,
	CustomEventData_t868048441_CustomAttributesCacheGenerator_CustomEventData_AddUInt32_m911556407,
	CustomEventData_t868048441_CustomAttributesCacheGenerator_CustomEventData_AddInt64_m1450877831,
	CustomEventData_t868048441_CustomAttributesCacheGenerator_CustomEventData_AddUInt64_m33970678,
	CustomEventData_t868048441_CustomAttributesCacheGenerator_CustomEventData_AddDouble_m1838257319,
	UnityAnalyticsHandler_t1984834895_CustomAttributesCacheGenerator_UnityAnalyticsHandler_InternalCreate_m3939206337,
	UnityAnalyticsHandler_t1984834895_CustomAttributesCacheGenerator_UnityAnalyticsHandler_InternalDestroy_m316722488,
	UnityAnalyticsHandler_t1984834895_CustomAttributesCacheGenerator_UnityAnalyticsHandler_SendCustomEventName_m2761615078,
	UnityAnalyticsHandler_t1984834895_CustomAttributesCacheGenerator_UnityAnalyticsHandler_SendCustomEvent_m3141620171,
	g_UnityEngine_UnityConnectModule_Assembly_CustomAttributesCacheGenerator,
	RemoteSettings_t2754304463_CustomAttributesCacheGenerator_Updated,
	RemoteSettings_t2754304463_CustomAttributesCacheGenerator_RemoteSettings_CallOnUpdate_m837272010,
	AnalyticsSessionState_t95894832_CustomAttributesCacheGenerator,
	AnalyticsSessionInfo_t1065651873_CustomAttributesCacheGenerator,
	AnalyticsSessionInfo_t1065651873_CustomAttributesCacheGenerator_sessionStateChanged,
	AnalyticsSessionInfo_t1065651873_CustomAttributesCacheGenerator_AnalyticsSessionInfo_CallSessionStateChanged_m2908054006,
	g_UnityEngine_UnityWebRequestModule_Assembly_CustomAttributesCacheGenerator,
	WebRequestUtils_t669258994_CustomAttributesCacheGenerator_WebRequestUtils_RedirectTo_m1499419735,
	g_UnityEngine_Assembly_CustomAttributesCacheGenerator,
	g_UnityEngine_Analytics_Assembly_CustomAttributesCacheGenerator,
	AnalyticsTracker_t1058519078_CustomAttributesCacheGenerator,
	AnalyticsTracker_t1058519078_CustomAttributesCacheGenerator_m_EventName,
	AnalyticsTracker_t1058519078_CustomAttributesCacheGenerator_m_TrackableProperty,
	AnalyticsTracker_t1058519078_CustomAttributesCacheGenerator_m_Trigger,
	TrackableProperty_t52271239_CustomAttributesCacheGenerator_m_Fields,
	FieldWithTarget_t1883299082_CustomAttributesCacheGenerator_m_ParamName,
	FieldWithTarget_t1883299082_CustomAttributesCacheGenerator_m_Target,
	FieldWithTarget_t1883299082_CustomAttributesCacheGenerator_m_FieldPath,
	FieldWithTarget_t1883299082_CustomAttributesCacheGenerator_m_TypeString,
	FieldWithTarget_t1883299082_CustomAttributesCacheGenerator_m_DoStatic,
	FieldWithTarget_t1883299082_CustomAttributesCacheGenerator_m_StaticString,
};
