﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"








extern "C" void Context_t3303670129_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Context_t3303670129_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Context_t3303670129_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Context_t3303670129_0_0_0;
extern "C" void Escape_t1794542247_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Escape_t1794542247_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Escape_t1794542247_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Escape_t1794542247_0_0_0;
extern "C" void PreviousInfo_t2292384560_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void PreviousInfo_t2292384560_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void PreviousInfo_t2292384560_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType PreviousInfo_t2292384560_0_0_0;
extern "C" void DelegatePInvokeWrapper_AppDomainInitializer_t1811168608();
extern const RuntimeType AppDomainInitializer_t1811168608_0_0_0;
extern "C" void DelegatePInvokeWrapper_Swapper_t587993322();
extern const RuntimeType Swapper_t587993322_0_0_0;
extern "C" void DictionaryEntry_t1385909157_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void DictionaryEntry_t1385909157_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void DictionaryEntry_t1385909157_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType DictionaryEntry_t1385909157_0_0_0;
extern "C" void Slot_t2102621701_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Slot_t2102621701_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Slot_t2102621701_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Slot_t2102621701_0_0_0;
extern "C" void Slot_t1141617207_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Slot_t1141617207_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Slot_t1141617207_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Slot_t1141617207_0_0_0;
extern "C" void Enum_t750633987_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Enum_t750633987_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Enum_t750633987_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Enum_t750633987_0_0_0;
extern "C" void DelegatePInvokeWrapper_ReadDelegate_t3703664279();
extern const RuntimeType ReadDelegate_t3703664279_0_0_0;
extern "C" void DelegatePInvokeWrapper_WriteDelegate_t105187226();
extern const RuntimeType WriteDelegate_t105187226_0_0_0;
extern "C" void MonoIOStat_t421315798_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MonoIOStat_t421315798_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MonoIOStat_t421315798_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType MonoIOStat_t421315798_0_0_0;
extern "C" void MonoEnumInfo_t776332500_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MonoEnumInfo_t776332500_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MonoEnumInfo_t776332500_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType MonoEnumInfo_t776332500_0_0_0;
extern "C" void CustomAttributeNamedArgument_t425730339_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void CustomAttributeNamedArgument_t425730339_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void CustomAttributeNamedArgument_t425730339_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType CustomAttributeNamedArgument_t425730339_0_0_0;
extern "C" void CustomAttributeTypedArgument_t2667702721_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void CustomAttributeTypedArgument_t2667702721_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void CustomAttributeTypedArgument_t2667702721_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType CustomAttributeTypedArgument_t2667702721_0_0_0;
extern "C" void ILTokenInfo_t281295013_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ILTokenInfo_t281295013_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ILTokenInfo_t281295013_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ILTokenInfo_t281295013_0_0_0;
extern "C" void MonoResource_t2965441204_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MonoResource_t2965441204_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MonoResource_t2965441204_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType MonoResource_t2965441204_0_0_0;
extern "C" void RefEmitPermissionSet_t945207422_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RefEmitPermissionSet_t945207422_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RefEmitPermissionSet_t945207422_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType RefEmitPermissionSet_t945207422_0_0_0;
extern "C" void MonoEventInfo_t802398473_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MonoEventInfo_t802398473_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MonoEventInfo_t802398473_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType MonoEventInfo_t802398473_0_0_0;
extern "C" void MonoMethodInfo_t1860908649_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MonoMethodInfo_t1860908649_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MonoMethodInfo_t1860908649_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType MonoMethodInfo_t1860908649_0_0_0;
extern "C" void MonoPropertyInfo_t3452547385_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MonoPropertyInfo_t3452547385_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MonoPropertyInfo_t3452547385_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType MonoPropertyInfo_t3452547385_0_0_0;
extern "C" void ParameterModifier_t969812002_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ParameterModifier_t969812002_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ParameterModifier_t969812002_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ParameterModifier_t969812002_0_0_0;
extern "C" void ResourceCacheItem_t3632973466_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ResourceCacheItem_t3632973466_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ResourceCacheItem_t3632973466_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ResourceCacheItem_t3632973466_0_0_0;
extern "C" void ResourceInfo_t1773833009_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ResourceInfo_t1773833009_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ResourceInfo_t1773833009_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ResourceInfo_t1773833009_0_0_0;
extern "C" void DelegatePInvokeWrapper_CrossContextDelegate_t642168454();
extern const RuntimeType CrossContextDelegate_t642168454_0_0_0;
extern "C" void DelegatePInvokeWrapper_CallbackHandler_t3393668368();
extern const RuntimeType CallbackHandler_t3393668368_0_0_0;
extern "C" void SerializationEntry_t4251740542_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SerializationEntry_t4251740542_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SerializationEntry_t4251740542_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType SerializationEntry_t4251740542_0_0_0;
extern "C" void StreamingContext_t1316667400_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void StreamingContext_t1316667400_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void StreamingContext_t1316667400_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType StreamingContext_t1316667400_0_0_0;
extern "C" void DSAParameters_t2781005375_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void DSAParameters_t2781005375_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void DSAParameters_t2781005375_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType DSAParameters_t2781005375_0_0_0;
extern "C" void RSAParameters_t2749614896_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RSAParameters_t2749614896_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RSAParameters_t2749614896_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType RSAParameters_t2749614896_0_0_0;
extern "C" void SecurityFrame_t3753960101_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SecurityFrame_t3753960101_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SecurityFrame_t3753960101_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType SecurityFrame_t3753960101_0_0_0;
extern "C" void DelegatePInvokeWrapper_ThreadStart_t8027522();
extern const RuntimeType ThreadStart_t8027522_0_0_0;
extern "C" void ValueType_t1920584095_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ValueType_t1920584095_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ValueType_t1920584095_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ValueType_t1920584095_0_0_0;
extern "C" void X509ChainStatus_t177750891_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void X509ChainStatus_t177750891_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void X509ChainStatus_t177750891_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType X509ChainStatus_t177750891_0_0_0;
extern "C" void IntStack_t1698647071_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void IntStack_t1698647071_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void IntStack_t1698647071_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType IntStack_t1698647071_0_0_0;
extern "C" void Interval_t797373189_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Interval_t797373189_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Interval_t797373189_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Interval_t797373189_0_0_0;
extern "C" void DelegatePInvokeWrapper_CostDelegate_t49582558();
extern const RuntimeType CostDelegate_t49582558_0_0_0;
extern "C" void UriScheme_t2622429923_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void UriScheme_t2622429923_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void UriScheme_t2622429923_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType UriScheme_t2622429923_0_0_0;
extern "C" void AnimationCurve_t1726276845_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AnimationCurve_t1726276845_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AnimationCurve_t1726276845_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType AnimationCurve_t1726276845_0_0_0;
extern "C" void DelegatePInvokeWrapper_LogCallback_t3381938013();
extern const RuntimeType LogCallback_t3381938013_0_0_0;
extern "C" void DelegatePInvokeWrapper_LowMemoryCallback_t454721290();
extern const RuntimeType LowMemoryCallback_t454721290_0_0_0;
extern "C" void AssetBundleCreateRequest_t3985900355_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AssetBundleCreateRequest_t3985900355_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AssetBundleCreateRequest_t3985900355_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType AssetBundleCreateRequest_t3985900355_0_0_0;
extern "C" void AssetBundleRequest_t2873072919_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AssetBundleRequest_t2873072919_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AssetBundleRequest_t2873072919_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType AssetBundleRequest_t2873072919_0_0_0;
extern "C" void AsyncOperation_t4233810744_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AsyncOperation_t4233810744_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AsyncOperation_t4233810744_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType AsyncOperation_t4233810744_0_0_0;
extern "C" void Coroutine_t3619068392_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Coroutine_t3619068392_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Coroutine_t3619068392_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Coroutine_t3619068392_0_0_0;
extern "C" void DelegatePInvokeWrapper_CSSMeasureFunc_t2162796896();
extern const RuntimeType CSSMeasureFunc_t2162796896_0_0_0;
extern "C" void CullingGroup_t1255328198_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void CullingGroup_t1255328198_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void CullingGroup_t1255328198_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType CullingGroup_t1255328198_0_0_0;
extern "C" void DelegatePInvokeWrapper_StateChanged_t2514413929();
extern const RuntimeType StateChanged_t2514413929_0_0_0;
extern "C" void DelegatePInvokeWrapper_DisplaysUpdatedDelegate_t2805775822();
extern const RuntimeType DisplaysUpdatedDelegate_t2805775822_0_0_0;
extern "C" void DelegatePInvokeWrapper_UnityAction_t35329723();
extern const RuntimeType UnityAction_t35329723_0_0_0;
extern "C" void FailedToLoadScriptObject_t2431108707_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void FailedToLoadScriptObject_t2431108707_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void FailedToLoadScriptObject_t2431108707_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType FailedToLoadScriptObject_t2431108707_0_0_0;
extern "C" void Gradient_t130030130_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Gradient_t130030130_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Gradient_t130030130_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Gradient_t130030130_0_0_0;
extern "C" void Object_t2461733472_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Object_t2461733472_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Object_t2461733472_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Object_t2461733472_0_0_0;
extern "C" void PlayableBinding_t136667248_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void PlayableBinding_t136667248_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void PlayableBinding_t136667248_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType PlayableBinding_t136667248_0_0_0;
extern "C" void ResourceRequest_t3543572403_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ResourceRequest_t3543572403_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ResourceRequest_t3543572403_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ResourceRequest_t3543572403_0_0_0;
extern "C" void ScriptableObject_t476655885_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ScriptableObject_t476655885_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ScriptableObject_t476655885_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ScriptableObject_t476655885_0_0_0;
extern "C" void HitInfo_t184839221_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void HitInfo_t184839221_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void HitInfo_t184839221_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType HitInfo_t184839221_0_0_0;
extern "C" void DelegatePInvokeWrapper_RequestAtlasCallback_t3221397704();
extern const RuntimeType RequestAtlasCallback_t3221397704_0_0_0;
extern "C" void WorkRequest_t2547429811_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void WorkRequest_t2547429811_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void WorkRequest_t2547429811_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType WorkRequest_t2547429811_0_0_0;
extern "C" void WaitForSeconds_t727047372_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void WaitForSeconds_t727047372_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void WaitForSeconds_t727047372_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType WaitForSeconds_t727047372_0_0_0;
extern "C" void YieldInstruction_t3999321895_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void YieldInstruction_t3999321895_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void YieldInstruction_t3999321895_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType YieldInstruction_t3999321895_0_0_0;
extern "C" void DelegatePInvokeWrapper_PCMReaderCallback_t219306701();
extern const RuntimeType PCMReaderCallback_t219306701_0_0_0;
extern "C" void DelegatePInvokeWrapper_PCMSetPositionCallback_t4291439443();
extern const RuntimeType PCMSetPositionCallback_t4291439443_0_0_0;
extern "C" void DelegatePInvokeWrapper_AudioConfigurationChangeHandler_t3382825605();
extern const RuntimeType AudioConfigurationChangeHandler_t3382825605_0_0_0;
extern "C" void GcAchievementData_t3167706508_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GcAchievementData_t3167706508_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GcAchievementData_t3167706508_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GcAchievementData_t3167706508_0_0_0;
extern "C" void GcAchievementDescriptionData_t2898970914_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GcAchievementDescriptionData_t2898970914_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GcAchievementDescriptionData_t2898970914_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GcAchievementDescriptionData_t2898970914_0_0_0;
extern "C" void GcLeaderboard_t617530396_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GcLeaderboard_t617530396_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GcLeaderboard_t617530396_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GcLeaderboard_t617530396_0_0_0;
extern "C" void GcScoreData_t3955401213_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GcScoreData_t3955401213_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GcScoreData_t3955401213_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GcScoreData_t3955401213_0_0_0;
extern "C" void GcUserProfileData_t3262991188_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GcUserProfileData_t3262991188_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GcUserProfileData_t3262991188_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GcUserProfileData_t3262991188_0_0_0;
extern "C" void CustomEventData_t868048441_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void CustomEventData_t868048441_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void CustomEventData_t868048441_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType CustomEventData_t868048441_0_0_0;
extern "C" void UnityAnalyticsHandler_t1984834895_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void UnityAnalyticsHandler_t1984834895_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void UnityAnalyticsHandler_t1984834895_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType UnityAnalyticsHandler_t1984834895_0_0_0;
extern "C" void DelegatePInvokeWrapper_SessionStateChanged_t633318234();
extern const RuntimeType SessionStateChanged_t633318234_0_0_0;
extern "C" void DelegatePInvokeWrapper_UpdatedEventHandler_t921819193();
extern const RuntimeType UpdatedEventHandler_t921819193_0_0_0;
extern Il2CppInteropData g_Il2CppInteropData[74] = 
{
	{ NULL, Context_t3303670129_marshal_pinvoke, Context_t3303670129_marshal_pinvoke_back, Context_t3303670129_marshal_pinvoke_cleanup, NULL, NULL, &Context_t3303670129_0_0_0 } /* Mono.Globalization.Unicode.SimpleCollator/Context */,
	{ NULL, Escape_t1794542247_marshal_pinvoke, Escape_t1794542247_marshal_pinvoke_back, Escape_t1794542247_marshal_pinvoke_cleanup, NULL, NULL, &Escape_t1794542247_0_0_0 } /* Mono.Globalization.Unicode.SimpleCollator/Escape */,
	{ NULL, PreviousInfo_t2292384560_marshal_pinvoke, PreviousInfo_t2292384560_marshal_pinvoke_back, PreviousInfo_t2292384560_marshal_pinvoke_cleanup, NULL, NULL, &PreviousInfo_t2292384560_0_0_0 } /* Mono.Globalization.Unicode.SimpleCollator/PreviousInfo */,
	{ DelegatePInvokeWrapper_AppDomainInitializer_t1811168608, NULL, NULL, NULL, NULL, NULL, &AppDomainInitializer_t1811168608_0_0_0 } /* System.AppDomainInitializer */,
	{ DelegatePInvokeWrapper_Swapper_t587993322, NULL, NULL, NULL, NULL, NULL, &Swapper_t587993322_0_0_0 } /* System.Array/Swapper */,
	{ NULL, DictionaryEntry_t1385909157_marshal_pinvoke, DictionaryEntry_t1385909157_marshal_pinvoke_back, DictionaryEntry_t1385909157_marshal_pinvoke_cleanup, NULL, NULL, &DictionaryEntry_t1385909157_0_0_0 } /* System.Collections.DictionaryEntry */,
	{ NULL, Slot_t2102621701_marshal_pinvoke, Slot_t2102621701_marshal_pinvoke_back, Slot_t2102621701_marshal_pinvoke_cleanup, NULL, NULL, &Slot_t2102621701_0_0_0 } /* System.Collections.Hashtable/Slot */,
	{ NULL, Slot_t1141617207_marshal_pinvoke, Slot_t1141617207_marshal_pinvoke_back, Slot_t1141617207_marshal_pinvoke_cleanup, NULL, NULL, &Slot_t1141617207_0_0_0 } /* System.Collections.SortedList/Slot */,
	{ NULL, Enum_t750633987_marshal_pinvoke, Enum_t750633987_marshal_pinvoke_back, Enum_t750633987_marshal_pinvoke_cleanup, NULL, NULL, &Enum_t750633987_0_0_0 } /* System.Enum */,
	{ DelegatePInvokeWrapper_ReadDelegate_t3703664279, NULL, NULL, NULL, NULL, NULL, &ReadDelegate_t3703664279_0_0_0 } /* System.IO.FileStream/ReadDelegate */,
	{ DelegatePInvokeWrapper_WriteDelegate_t105187226, NULL, NULL, NULL, NULL, NULL, &WriteDelegate_t105187226_0_0_0 } /* System.IO.FileStream/WriteDelegate */,
	{ NULL, MonoIOStat_t421315798_marshal_pinvoke, MonoIOStat_t421315798_marshal_pinvoke_back, MonoIOStat_t421315798_marshal_pinvoke_cleanup, NULL, NULL, &MonoIOStat_t421315798_0_0_0 } /* System.IO.MonoIOStat */,
	{ NULL, MonoEnumInfo_t776332500_marshal_pinvoke, MonoEnumInfo_t776332500_marshal_pinvoke_back, MonoEnumInfo_t776332500_marshal_pinvoke_cleanup, NULL, NULL, &MonoEnumInfo_t776332500_0_0_0 } /* System.MonoEnumInfo */,
	{ NULL, CustomAttributeNamedArgument_t425730339_marshal_pinvoke, CustomAttributeNamedArgument_t425730339_marshal_pinvoke_back, CustomAttributeNamedArgument_t425730339_marshal_pinvoke_cleanup, NULL, NULL, &CustomAttributeNamedArgument_t425730339_0_0_0 } /* System.Reflection.CustomAttributeNamedArgument */,
	{ NULL, CustomAttributeTypedArgument_t2667702721_marshal_pinvoke, CustomAttributeTypedArgument_t2667702721_marshal_pinvoke_back, CustomAttributeTypedArgument_t2667702721_marshal_pinvoke_cleanup, NULL, NULL, &CustomAttributeTypedArgument_t2667702721_0_0_0 } /* System.Reflection.CustomAttributeTypedArgument */,
	{ NULL, ILTokenInfo_t281295013_marshal_pinvoke, ILTokenInfo_t281295013_marshal_pinvoke_back, ILTokenInfo_t281295013_marshal_pinvoke_cleanup, NULL, NULL, &ILTokenInfo_t281295013_0_0_0 } /* System.Reflection.Emit.ILTokenInfo */,
	{ NULL, MonoResource_t2965441204_marshal_pinvoke, MonoResource_t2965441204_marshal_pinvoke_back, MonoResource_t2965441204_marshal_pinvoke_cleanup, NULL, NULL, &MonoResource_t2965441204_0_0_0 } /* System.Reflection.Emit.MonoResource */,
	{ NULL, RefEmitPermissionSet_t945207422_marshal_pinvoke, RefEmitPermissionSet_t945207422_marshal_pinvoke_back, RefEmitPermissionSet_t945207422_marshal_pinvoke_cleanup, NULL, NULL, &RefEmitPermissionSet_t945207422_0_0_0 } /* System.Reflection.Emit.RefEmitPermissionSet */,
	{ NULL, MonoEventInfo_t802398473_marshal_pinvoke, MonoEventInfo_t802398473_marshal_pinvoke_back, MonoEventInfo_t802398473_marshal_pinvoke_cleanup, NULL, NULL, &MonoEventInfo_t802398473_0_0_0 } /* System.Reflection.MonoEventInfo */,
	{ NULL, MonoMethodInfo_t1860908649_marshal_pinvoke, MonoMethodInfo_t1860908649_marshal_pinvoke_back, MonoMethodInfo_t1860908649_marshal_pinvoke_cleanup, NULL, NULL, &MonoMethodInfo_t1860908649_0_0_0 } /* System.Reflection.MonoMethodInfo */,
	{ NULL, MonoPropertyInfo_t3452547385_marshal_pinvoke, MonoPropertyInfo_t3452547385_marshal_pinvoke_back, MonoPropertyInfo_t3452547385_marshal_pinvoke_cleanup, NULL, NULL, &MonoPropertyInfo_t3452547385_0_0_0 } /* System.Reflection.MonoPropertyInfo */,
	{ NULL, ParameterModifier_t969812002_marshal_pinvoke, ParameterModifier_t969812002_marshal_pinvoke_back, ParameterModifier_t969812002_marshal_pinvoke_cleanup, NULL, NULL, &ParameterModifier_t969812002_0_0_0 } /* System.Reflection.ParameterModifier */,
	{ NULL, ResourceCacheItem_t3632973466_marshal_pinvoke, ResourceCacheItem_t3632973466_marshal_pinvoke_back, ResourceCacheItem_t3632973466_marshal_pinvoke_cleanup, NULL, NULL, &ResourceCacheItem_t3632973466_0_0_0 } /* System.Resources.ResourceReader/ResourceCacheItem */,
	{ NULL, ResourceInfo_t1773833009_marshal_pinvoke, ResourceInfo_t1773833009_marshal_pinvoke_back, ResourceInfo_t1773833009_marshal_pinvoke_cleanup, NULL, NULL, &ResourceInfo_t1773833009_0_0_0 } /* System.Resources.ResourceReader/ResourceInfo */,
	{ DelegatePInvokeWrapper_CrossContextDelegate_t642168454, NULL, NULL, NULL, NULL, NULL, &CrossContextDelegate_t642168454_0_0_0 } /* System.Runtime.Remoting.Contexts.CrossContextDelegate */,
	{ DelegatePInvokeWrapper_CallbackHandler_t3393668368, NULL, NULL, NULL, NULL, NULL, &CallbackHandler_t3393668368_0_0_0 } /* System.Runtime.Serialization.SerializationCallbacks/CallbackHandler */,
	{ NULL, SerializationEntry_t4251740542_marshal_pinvoke, SerializationEntry_t4251740542_marshal_pinvoke_back, SerializationEntry_t4251740542_marshal_pinvoke_cleanup, NULL, NULL, &SerializationEntry_t4251740542_0_0_0 } /* System.Runtime.Serialization.SerializationEntry */,
	{ NULL, StreamingContext_t1316667400_marshal_pinvoke, StreamingContext_t1316667400_marshal_pinvoke_back, StreamingContext_t1316667400_marshal_pinvoke_cleanup, NULL, NULL, &StreamingContext_t1316667400_0_0_0 } /* System.Runtime.Serialization.StreamingContext */,
	{ NULL, DSAParameters_t2781005375_marshal_pinvoke, DSAParameters_t2781005375_marshal_pinvoke_back, DSAParameters_t2781005375_marshal_pinvoke_cleanup, NULL, NULL, &DSAParameters_t2781005375_0_0_0 } /* System.Security.Cryptography.DSAParameters */,
	{ NULL, RSAParameters_t2749614896_marshal_pinvoke, RSAParameters_t2749614896_marshal_pinvoke_back, RSAParameters_t2749614896_marshal_pinvoke_cleanup, NULL, NULL, &RSAParameters_t2749614896_0_0_0 } /* System.Security.Cryptography.RSAParameters */,
	{ NULL, SecurityFrame_t3753960101_marshal_pinvoke, SecurityFrame_t3753960101_marshal_pinvoke_back, SecurityFrame_t3753960101_marshal_pinvoke_cleanup, NULL, NULL, &SecurityFrame_t3753960101_0_0_0 } /* System.Security.SecurityFrame */,
	{ DelegatePInvokeWrapper_ThreadStart_t8027522, NULL, NULL, NULL, NULL, NULL, &ThreadStart_t8027522_0_0_0 } /* System.Threading.ThreadStart */,
	{ NULL, ValueType_t1920584095_marshal_pinvoke, ValueType_t1920584095_marshal_pinvoke_back, ValueType_t1920584095_marshal_pinvoke_cleanup, NULL, NULL, &ValueType_t1920584095_0_0_0 } /* System.ValueType */,
	{ NULL, X509ChainStatus_t177750891_marshal_pinvoke, X509ChainStatus_t177750891_marshal_pinvoke_back, X509ChainStatus_t177750891_marshal_pinvoke_cleanup, NULL, NULL, &X509ChainStatus_t177750891_0_0_0 } /* System.Security.Cryptography.X509Certificates.X509ChainStatus */,
	{ NULL, IntStack_t1698647071_marshal_pinvoke, IntStack_t1698647071_marshal_pinvoke_back, IntStack_t1698647071_marshal_pinvoke_cleanup, NULL, NULL, &IntStack_t1698647071_0_0_0 } /* System.Text.RegularExpressions.Interpreter/IntStack */,
	{ NULL, Interval_t797373189_marshal_pinvoke, Interval_t797373189_marshal_pinvoke_back, Interval_t797373189_marshal_pinvoke_cleanup, NULL, NULL, &Interval_t797373189_0_0_0 } /* System.Text.RegularExpressions.Interval */,
	{ DelegatePInvokeWrapper_CostDelegate_t49582558, NULL, NULL, NULL, NULL, NULL, &CostDelegate_t49582558_0_0_0 } /* System.Text.RegularExpressions.IntervalCollection/CostDelegate */,
	{ NULL, UriScheme_t2622429923_marshal_pinvoke, UriScheme_t2622429923_marshal_pinvoke_back, UriScheme_t2622429923_marshal_pinvoke_cleanup, NULL, NULL, &UriScheme_t2622429923_0_0_0 } /* System.Uri/UriScheme */,
	{ NULL, AnimationCurve_t1726276845_marshal_pinvoke, AnimationCurve_t1726276845_marshal_pinvoke_back, AnimationCurve_t1726276845_marshal_pinvoke_cleanup, NULL, NULL, &AnimationCurve_t1726276845_0_0_0 } /* UnityEngine.AnimationCurve */,
	{ DelegatePInvokeWrapper_LogCallback_t3381938013, NULL, NULL, NULL, NULL, NULL, &LogCallback_t3381938013_0_0_0 } /* UnityEngine.Application/LogCallback */,
	{ DelegatePInvokeWrapper_LowMemoryCallback_t454721290, NULL, NULL, NULL, NULL, NULL, &LowMemoryCallback_t454721290_0_0_0 } /* UnityEngine.Application/LowMemoryCallback */,
	{ NULL, AssetBundleCreateRequest_t3985900355_marshal_pinvoke, AssetBundleCreateRequest_t3985900355_marshal_pinvoke_back, AssetBundleCreateRequest_t3985900355_marshal_pinvoke_cleanup, NULL, NULL, &AssetBundleCreateRequest_t3985900355_0_0_0 } /* UnityEngine.AssetBundleCreateRequest */,
	{ NULL, AssetBundleRequest_t2873072919_marshal_pinvoke, AssetBundleRequest_t2873072919_marshal_pinvoke_back, AssetBundleRequest_t2873072919_marshal_pinvoke_cleanup, NULL, NULL, &AssetBundleRequest_t2873072919_0_0_0 } /* UnityEngine.AssetBundleRequest */,
	{ NULL, AsyncOperation_t4233810744_marshal_pinvoke, AsyncOperation_t4233810744_marshal_pinvoke_back, AsyncOperation_t4233810744_marshal_pinvoke_cleanup, NULL, NULL, &AsyncOperation_t4233810744_0_0_0 } /* UnityEngine.AsyncOperation */,
	{ NULL, Coroutine_t3619068392_marshal_pinvoke, Coroutine_t3619068392_marshal_pinvoke_back, Coroutine_t3619068392_marshal_pinvoke_cleanup, NULL, NULL, &Coroutine_t3619068392_0_0_0 } /* UnityEngine.Coroutine */,
	{ DelegatePInvokeWrapper_CSSMeasureFunc_t2162796896, NULL, NULL, NULL, NULL, NULL, &CSSMeasureFunc_t2162796896_0_0_0 } /* UnityEngine.CSSLayout.CSSMeasureFunc */,
	{ NULL, CullingGroup_t1255328198_marshal_pinvoke, CullingGroup_t1255328198_marshal_pinvoke_back, CullingGroup_t1255328198_marshal_pinvoke_cleanup, NULL, NULL, &CullingGroup_t1255328198_0_0_0 } /* UnityEngine.CullingGroup */,
	{ DelegatePInvokeWrapper_StateChanged_t2514413929, NULL, NULL, NULL, NULL, NULL, &StateChanged_t2514413929_0_0_0 } /* UnityEngine.CullingGroup/StateChanged */,
	{ DelegatePInvokeWrapper_DisplaysUpdatedDelegate_t2805775822, NULL, NULL, NULL, NULL, NULL, &DisplaysUpdatedDelegate_t2805775822_0_0_0 } /* UnityEngine.Display/DisplaysUpdatedDelegate */,
	{ DelegatePInvokeWrapper_UnityAction_t35329723, NULL, NULL, NULL, NULL, NULL, &UnityAction_t35329723_0_0_0 } /* UnityEngine.Events.UnityAction */,
	{ NULL, FailedToLoadScriptObject_t2431108707_marshal_pinvoke, FailedToLoadScriptObject_t2431108707_marshal_pinvoke_back, FailedToLoadScriptObject_t2431108707_marshal_pinvoke_cleanup, NULL, NULL, &FailedToLoadScriptObject_t2431108707_0_0_0 } /* UnityEngine.FailedToLoadScriptObject */,
	{ NULL, Gradient_t130030130_marshal_pinvoke, Gradient_t130030130_marshal_pinvoke_back, Gradient_t130030130_marshal_pinvoke_cleanup, NULL, NULL, &Gradient_t130030130_0_0_0 } /* UnityEngine.Gradient */,
	{ NULL, Object_t2461733472_marshal_pinvoke, Object_t2461733472_marshal_pinvoke_back, Object_t2461733472_marshal_pinvoke_cleanup, NULL, NULL, &Object_t2461733472_0_0_0 } /* UnityEngine.Object */,
	{ NULL, PlayableBinding_t136667248_marshal_pinvoke, PlayableBinding_t136667248_marshal_pinvoke_back, PlayableBinding_t136667248_marshal_pinvoke_cleanup, NULL, NULL, &PlayableBinding_t136667248_0_0_0 } /* UnityEngine.Playables.PlayableBinding */,
	{ NULL, ResourceRequest_t3543572403_marshal_pinvoke, ResourceRequest_t3543572403_marshal_pinvoke_back, ResourceRequest_t3543572403_marshal_pinvoke_cleanup, NULL, NULL, &ResourceRequest_t3543572403_0_0_0 } /* UnityEngine.ResourceRequest */,
	{ NULL, ScriptableObject_t476655885_marshal_pinvoke, ScriptableObject_t476655885_marshal_pinvoke_back, ScriptableObject_t476655885_marshal_pinvoke_cleanup, NULL, NULL, &ScriptableObject_t476655885_0_0_0 } /* UnityEngine.ScriptableObject */,
	{ NULL, HitInfo_t184839221_marshal_pinvoke, HitInfo_t184839221_marshal_pinvoke_back, HitInfo_t184839221_marshal_pinvoke_cleanup, NULL, NULL, &HitInfo_t184839221_0_0_0 } /* UnityEngine.SendMouseEvents/HitInfo */,
	{ DelegatePInvokeWrapper_RequestAtlasCallback_t3221397704, NULL, NULL, NULL, NULL, NULL, &RequestAtlasCallback_t3221397704_0_0_0 } /* UnityEngine.U2D.SpriteAtlasManager/RequestAtlasCallback */,
	{ NULL, WorkRequest_t2547429811_marshal_pinvoke, WorkRequest_t2547429811_marshal_pinvoke_back, WorkRequest_t2547429811_marshal_pinvoke_cleanup, NULL, NULL, &WorkRequest_t2547429811_0_0_0 } /* UnityEngine.UnitySynchronizationContext/WorkRequest */,
	{ NULL, WaitForSeconds_t727047372_marshal_pinvoke, WaitForSeconds_t727047372_marshal_pinvoke_back, WaitForSeconds_t727047372_marshal_pinvoke_cleanup, NULL, NULL, &WaitForSeconds_t727047372_0_0_0 } /* UnityEngine.WaitForSeconds */,
	{ NULL, YieldInstruction_t3999321895_marshal_pinvoke, YieldInstruction_t3999321895_marshal_pinvoke_back, YieldInstruction_t3999321895_marshal_pinvoke_cleanup, NULL, NULL, &YieldInstruction_t3999321895_0_0_0 } /* UnityEngine.YieldInstruction */,
	{ DelegatePInvokeWrapper_PCMReaderCallback_t219306701, NULL, NULL, NULL, NULL, NULL, &PCMReaderCallback_t219306701_0_0_0 } /* UnityEngine.AudioClip/PCMReaderCallback */,
	{ DelegatePInvokeWrapper_PCMSetPositionCallback_t4291439443, NULL, NULL, NULL, NULL, NULL, &PCMSetPositionCallback_t4291439443_0_0_0 } /* UnityEngine.AudioClip/PCMSetPositionCallback */,
	{ DelegatePInvokeWrapper_AudioConfigurationChangeHandler_t3382825605, NULL, NULL, NULL, NULL, NULL, &AudioConfigurationChangeHandler_t3382825605_0_0_0 } /* UnityEngine.AudioSettings/AudioConfigurationChangeHandler */,
	{ NULL, GcAchievementData_t3167706508_marshal_pinvoke, GcAchievementData_t3167706508_marshal_pinvoke_back, GcAchievementData_t3167706508_marshal_pinvoke_cleanup, NULL, NULL, &GcAchievementData_t3167706508_0_0_0 } /* UnityEngine.SocialPlatforms.GameCenter.GcAchievementData */,
	{ NULL, GcAchievementDescriptionData_t2898970914_marshal_pinvoke, GcAchievementDescriptionData_t2898970914_marshal_pinvoke_back, GcAchievementDescriptionData_t2898970914_marshal_pinvoke_cleanup, NULL, NULL, &GcAchievementDescriptionData_t2898970914_0_0_0 } /* UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData */,
	{ NULL, GcLeaderboard_t617530396_marshal_pinvoke, GcLeaderboard_t617530396_marshal_pinvoke_back, GcLeaderboard_t617530396_marshal_pinvoke_cleanup, NULL, NULL, &GcLeaderboard_t617530396_0_0_0 } /* UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard */,
	{ NULL, GcScoreData_t3955401213_marshal_pinvoke, GcScoreData_t3955401213_marshal_pinvoke_back, GcScoreData_t3955401213_marshal_pinvoke_cleanup, NULL, NULL, &GcScoreData_t3955401213_0_0_0 } /* UnityEngine.SocialPlatforms.GameCenter.GcScoreData */,
	{ NULL, GcUserProfileData_t3262991188_marshal_pinvoke, GcUserProfileData_t3262991188_marshal_pinvoke_back, GcUserProfileData_t3262991188_marshal_pinvoke_cleanup, NULL, NULL, &GcUserProfileData_t3262991188_0_0_0 } /* UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData */,
	{ NULL, CustomEventData_t868048441_marshal_pinvoke, CustomEventData_t868048441_marshal_pinvoke_back, CustomEventData_t868048441_marshal_pinvoke_cleanup, NULL, NULL, &CustomEventData_t868048441_0_0_0 } /* UnityEngine.Analytics.CustomEventData */,
	{ NULL, UnityAnalyticsHandler_t1984834895_marshal_pinvoke, UnityAnalyticsHandler_t1984834895_marshal_pinvoke_back, UnityAnalyticsHandler_t1984834895_marshal_pinvoke_cleanup, NULL, NULL, &UnityAnalyticsHandler_t1984834895_0_0_0 } /* UnityEngine.Analytics.UnityAnalyticsHandler */,
	{ DelegatePInvokeWrapper_SessionStateChanged_t633318234, NULL, NULL, NULL, NULL, NULL, &SessionStateChanged_t633318234_0_0_0 } /* UnityEngine.Analytics.AnalyticsSessionInfo/SessionStateChanged */,
	{ DelegatePInvokeWrapper_UpdatedEventHandler_t921819193, NULL, NULL, NULL, NULL, NULL, &UpdatedEventHandler_t921819193_0_0_0 } /* UnityEngine.RemoteSettings/UpdatedEventHandler */,
	NULL,
};
