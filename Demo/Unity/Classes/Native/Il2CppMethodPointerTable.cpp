﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"








extern "C" void Locale_GetText_m1500108805 ();
extern "C" void Locale_GetText_m659940240 ();
extern "C" void SafeHandleZeroOrMinusOneIsInvalid__ctor_m3262641181 ();
extern "C" void SafeHandleZeroOrMinusOneIsInvalid_get_IsInvalid_m891415827 ();
extern "C" void SafeWaitHandle__ctor_m1097298895 ();
extern "C" void SafeWaitHandle_ReleaseHandle_m124655883 ();
extern "C" void CodePointIndexer__ctor_m641528607 ();
extern "C" void CodePointIndexer_ToIndex_m2746420843 ();
extern "C" void TableRange__ctor_m336013596_AdjustorThunk ();
extern "C" void Contraction__ctor_m4145481304 ();
extern "C" void ContractionComparer__ctor_m1527695121 ();
extern "C" void ContractionComparer__cctor_m2310355412 ();
extern "C" void ContractionComparer_Compare_m2517703923 ();
extern "C" void Level2Map__ctor_m2637101496 ();
extern "C" void Level2MapComparer__ctor_m3314476384 ();
extern "C" void Level2MapComparer__cctor_m1504678356 ();
extern "C" void Level2MapComparer_Compare_m2948085554 ();
extern "C" void MSCompatUnicodeTable__cctor_m2698856002 ();
extern "C" void MSCompatUnicodeTable_GetTailoringInfo_m2751095510 ();
extern "C" void MSCompatUnicodeTable_BuildTailoringTables_m4143751451 ();
extern "C" void MSCompatUnicodeTable_SetCJKReferences_m3476394623 ();
extern "C" void MSCompatUnicodeTable_Category_m3438761433 ();
extern "C" void MSCompatUnicodeTable_Level1_m3928110760 ();
extern "C" void MSCompatUnicodeTable_Level2_m351989625 ();
extern "C" void MSCompatUnicodeTable_Level3_m714284458 ();
extern "C" void MSCompatUnicodeTable_IsIgnorable_m4153158312 ();
extern "C" void MSCompatUnicodeTable_IsIgnorableNonSpacing_m4192456948 ();
extern "C" void MSCompatUnicodeTable_ToKanaTypeInsensitive_m2988229978 ();
extern "C" void MSCompatUnicodeTable_ToWidthCompat_m2059231301 ();
extern "C" void MSCompatUnicodeTable_HasSpecialWeight_m1783500999 ();
extern "C" void MSCompatUnicodeTable_IsHalfWidthKana_m2452526595 ();
extern "C" void MSCompatUnicodeTable_IsHiragana_m2045669619 ();
extern "C" void MSCompatUnicodeTable_IsJapaneseSmallLetter_m2872205620 ();
extern "C" void MSCompatUnicodeTable_get_IsReady_m690130938 ();
extern "C" void MSCompatUnicodeTable_GetResource_m2433291647 ();
extern "C" void MSCompatUnicodeTable_UInt32FromBytePtr_m3054783212 ();
extern "C" void MSCompatUnicodeTable_FillCJK_m1585672419 ();
extern "C" void MSCompatUnicodeTable_FillCJKCore_m709191614 ();
extern "C" void MSCompatUnicodeTableUtil__cctor_m1217423538 ();
extern "C" void SimpleCollator__ctor_m3070788168 ();
extern "C" void SimpleCollator__cctor_m919560922 ();
extern "C" void SimpleCollator_SetCJKTable_m2575392057 ();
extern "C" void SimpleCollator_GetNeutralCulture_m1445005125 ();
extern "C" void SimpleCollator_Category_m2664735685 ();
extern "C" void SimpleCollator_Level1_m514441013 ();
extern "C" void SimpleCollator_Level2_m201463002 ();
extern "C" void SimpleCollator_IsHalfKana_m1012930504 ();
extern "C" void SimpleCollator_GetContraction_m740789706 ();
extern "C" void SimpleCollator_GetContraction_m111857746 ();
extern "C" void SimpleCollator_GetTailContraction_m1366162196 ();
extern "C" void SimpleCollator_GetTailContraction_m3938007728 ();
extern "C" void SimpleCollator_FilterOptions_m343004938 ();
extern "C" void SimpleCollator_GetExtenderType_m1669010917 ();
extern "C" void SimpleCollator_ToDashTypeValue_m4105598342 ();
extern "C" void SimpleCollator_FilterExtender_m2713511102 ();
extern "C" void SimpleCollator_IsIgnorable_m3327547977 ();
extern "C" void SimpleCollator_IsSafe_m2235906205 ();
extern "C" void SimpleCollator_GetSortKey_m2918984438 ();
extern "C" void SimpleCollator_GetSortKey_m4279889981 ();
extern "C" void SimpleCollator_GetSortKey_m881222275 ();
extern "C" void SimpleCollator_FillSortKeyRaw_m3825701976 ();
extern "C" void SimpleCollator_FillSurrogateSortKeyRaw_m3498490842 ();
extern "C" void SimpleCollator_CompareOrdinal_m256887360 ();
extern "C" void SimpleCollator_CompareQuick_m300970728 ();
extern "C" void SimpleCollator_CompareOrdinalIgnoreCase_m3541555993 ();
extern "C" void SimpleCollator_Compare_m3639527517 ();
extern "C" void SimpleCollator_ClearBuffer_m1493829777 ();
extern "C" void SimpleCollator_QuickCheckPossible_m2336776935 ();
extern "C" void SimpleCollator_CompareInternal_m2566514740 ();
extern "C" void SimpleCollator_CompareFlagPair_m2760041594 ();
extern "C" void SimpleCollator_IsPrefix_m3900513470 ();
extern "C" void SimpleCollator_IsPrefix_m622163661 ();
extern "C" void SimpleCollator_IsPrefix_m2024733508 ();
extern "C" void SimpleCollator_IsSuffix_m3042779660 ();
extern "C" void SimpleCollator_IsSuffix_m432518715 ();
extern "C" void SimpleCollator_QuickIndexOf_m864247376 ();
extern "C" void SimpleCollator_IndexOf_m1766454037 ();
extern "C" void SimpleCollator_IndexOfOrdinal_m2136019101 ();
extern "C" void SimpleCollator_IndexOfOrdinalIgnoreCase_m4064724991 ();
extern "C" void SimpleCollator_IndexOfSortKey_m708779630 ();
extern "C" void SimpleCollator_IndexOf_m3590481976 ();
extern "C" void SimpleCollator_LastIndexOf_m3772036014 ();
extern "C" void SimpleCollator_LastIndexOfOrdinal_m1415648312 ();
extern "C" void SimpleCollator_LastIndexOfOrdinalIgnoreCase_m2855724054 ();
extern "C" void SimpleCollator_LastIndexOfSortKey_m257786269 ();
extern "C" void SimpleCollator_LastIndexOf_m4081169027 ();
extern "C" void SimpleCollator_MatchesForward_m3769579644 ();
extern "C" void SimpleCollator_MatchesForwardCore_m1489578655 ();
extern "C" void SimpleCollator_MatchesPrimitive_m4222486953 ();
extern "C" void SimpleCollator_MatchesBackward_m3447956182 ();
extern "C" void SimpleCollator_MatchesBackwardCore_m415114982 ();
extern "C" void Context__ctor_m1030643923_AdjustorThunk ();
extern "C" void PreviousInfo__ctor_m2851947467_AdjustorThunk ();
extern "C" void SortKeyBuffer__ctor_m1945108209 ();
extern "C" void SortKeyBuffer_Reset_m3773372570 ();
extern "C" void SortKeyBuffer_Initialize_m611850055 ();
extern "C" void SortKeyBuffer_AppendCJKExtension_m1152522895 ();
extern "C" void SortKeyBuffer_AppendKana_m4023417654 ();
extern "C" void SortKeyBuffer_AppendNormal_m3224853385 ();
extern "C" void SortKeyBuffer_AppendLevel5_m1744389528 ();
extern "C" void SortKeyBuffer_AppendBufferPrimitive_m1397593205 ();
extern "C" void SortKeyBuffer_GetResultAndReset_m1229988782 ();
extern "C" void SortKeyBuffer_GetOptimizedLength_m4230725576 ();
extern "C" void SortKeyBuffer_GetResult_m3225648020 ();
extern "C" void TailoringInfo__ctor_m2655216332 ();
extern "C" void BigInteger__ctor_m1163451324 ();
extern "C" void BigInteger__ctor_m4276013935 ();
extern "C" void BigInteger__ctor_m1204451283 ();
extern "C" void BigInteger__ctor_m602686585 ();
extern "C" void BigInteger__ctor_m2767934931 ();
extern "C" void BigInteger__cctor_m4083811350 ();
extern "C" void BigInteger_get_Rng_m618323497 ();
extern "C" void BigInteger_GenerateRandom_m2914817005 ();
extern "C" void BigInteger_GenerateRandom_m3518857267 ();
extern "C" void BigInteger_Randomize_m1383479350 ();
extern "C" void BigInteger_Randomize_m3583174950 ();
extern "C" void BigInteger_BitCount_m1373717292 ();
extern "C" void BigInteger_TestBit_m2163967392 ();
extern "C" void BigInteger_TestBit_m3545384379 ();
extern "C" void BigInteger_SetBit_m1201518092 ();
extern "C" void BigInteger_SetBit_m3612418635 ();
extern "C" void BigInteger_LowestSetBit_m1526675678 ();
extern "C" void BigInteger_GetBytes_m1296189873 ();
extern "C" void BigInteger_ToString_m3484377 ();
extern "C" void BigInteger_ToString_m1522914732 ();
extern "C" void BigInteger_Normalize_m1829363676 ();
extern "C" void BigInteger_Clear_m365822215 ();
extern "C" void BigInteger_GetHashCode_m2393929432 ();
extern "C" void BigInteger_ToString_m653860377 ();
extern "C" void BigInteger_Equals_m2136904561 ();
extern "C" void BigInteger_ModInverse_m3474433369 ();
extern "C" void BigInteger_ModPow_m3085084927 ();
extern "C" void BigInteger_IsProbablePrime_m1133942801 ();
extern "C" void BigInteger_GeneratePseudoPrime_m3283596575 ();
extern "C" void BigInteger_Incr2_m2251347137 ();
extern "C" void BigInteger_op_Implicit_m932569639 ();
extern "C" void BigInteger_op_Implicit_m285994883 ();
extern "C" void BigInteger_op_Addition_m2673463548 ();
extern "C" void BigInteger_op_Subtraction_m2191246285 ();
extern "C" void BigInteger_op_Modulus_m319664938 ();
extern "C" void BigInteger_op_Modulus_m2212009832 ();
extern "C" void BigInteger_op_Division_m2255171013 ();
extern "C" void BigInteger_op_Multiply_m4220659470 ();
extern "C" void BigInteger_op_Multiply_m496362754 ();
extern "C" void BigInteger_op_LeftShift_m1113089594 ();
extern "C" void BigInteger_op_RightShift_m3254229068 ();
extern "C" void BigInteger_op_Equality_m1834303355 ();
extern "C" void BigInteger_op_Inequality_m2912048490 ();
extern "C" void BigInteger_op_Equality_m498942008 ();
extern "C" void BigInteger_op_Inequality_m206477400 ();
extern "C" void BigInteger_op_GreaterThan_m3015037594 ();
extern "C" void BigInteger_op_LessThan_m2208065797 ();
extern "C" void BigInteger_op_GreaterThanOrEqual_m2434423199 ();
extern "C" void BigInteger_op_LessThanOrEqual_m1554543193 ();
extern "C" void Kernel_AddSameSign_m3443248778 ();
extern "C" void Kernel_Subtract_m1850203865 ();
extern "C" void Kernel_MinusEq_m4012755338 ();
extern "C" void Kernel_PlusEq_m2110508277 ();
extern "C" void Kernel_Compare_m3502948456 ();
extern "C" void Kernel_SingleByteDivideInPlace_m4237050077 ();
extern "C" void Kernel_DwordMod_m1683999714 ();
extern "C" void Kernel_DwordDivMod_m3417043239 ();
extern "C" void Kernel_multiByteDivide_m3706099675 ();
extern "C" void Kernel_LeftShift_m1129695651 ();
extern "C" void Kernel_RightShift_m1033490646 ();
extern "C" void Kernel_MultiplyByDword_m1419153077 ();
extern "C" void Kernel_Multiply_m3461261708 ();
extern "C" void Kernel_MultiplyMod2p32pmod_m110351456 ();
extern "C" void Kernel_modInverse_m663909403 ();
extern "C" void Kernel_modInverse_m2047049350 ();
extern "C" void ModulusRing__ctor_m935358764 ();
extern "C" void ModulusRing_BarrettReduction_m3666592548 ();
extern "C" void ModulusRing_Multiply_m1084630745 ();
extern "C" void ModulusRing_Difference_m3162597472 ();
extern "C" void ModulusRing_Pow_m2581654810 ();
extern "C" void ModulusRing_Pow_m2784243082 ();
extern "C" void PrimeGeneratorBase__ctor_m2106328266 ();
extern "C" void PrimeGeneratorBase_get_Confidence_m2792095523 ();
extern "C" void PrimeGeneratorBase_get_PrimalityTest_m665017960 ();
extern "C" void PrimeGeneratorBase_get_TrialDivisionBounds_m1980307417 ();
extern "C" void SequentialSearchPrimeGeneratorBase__ctor_m2428287067 ();
extern "C" void SequentialSearchPrimeGeneratorBase_GenerateSearchBase_m1992894226 ();
extern "C" void SequentialSearchPrimeGeneratorBase_GenerateNewPrime_m3382928048 ();
extern "C" void SequentialSearchPrimeGeneratorBase_GenerateNewPrime_m1222965799 ();
extern "C" void SequentialSearchPrimeGeneratorBase_IsPrimeAcceptable_m1765180 ();
extern "C" void PrimalityTest__ctor_m3603898030 ();
extern "C" void PrimalityTest_Invoke_m1441281691 ();
extern "C" void PrimalityTest_BeginInvoke_m4069941682 ();
extern "C" void PrimalityTest_EndInvoke_m1063884147 ();
extern "C" void PrimalityTests_GetSPPRounds_m1689044817 ();
extern "C" void PrimalityTests_Test_m2914274961 ();
extern "C" void PrimalityTests_RabinMillerTest_m3119022782 ();
extern "C" void PrimalityTests_SmallPrimeSppTest_m2762617466 ();
extern "C" void Runtime_GetDisplayName_m896344743 ();
extern "C" void ASN1__ctor_m3803179655 ();
extern "C" void ASN1__ctor_m2577696247 ();
extern "C" void ASN1__ctor_m3795696753 ();
extern "C" void ASN1_get_Count_m510274956 ();
extern "C" void ASN1_get_Tag_m3065754366 ();
extern "C" void ASN1_get_Length_m1747607096 ();
extern "C" void ASN1_get_Value_m994047287 ();
extern "C" void ASN1_set_Value_m2844359354 ();
extern "C" void ASN1_CompareArray_m1850577781 ();
extern "C" void ASN1_CompareValue_m4047113686 ();
extern "C" void ASN1_Add_m893163025 ();
extern "C" void ASN1_GetBytes_m1657979790 ();
extern "C" void ASN1_Decode_m894249739 ();
extern "C" void ASN1_DecodeTLV_m3135978438 ();
extern "C" void ASN1_get_Item_m1761258769 ();
extern "C" void ASN1_Element_m443211517 ();
extern "C" void ASN1_ToString_m3567391814 ();
extern "C" void ASN1Convert_FromInt32_m2943578612 ();
extern "C" void ASN1Convert_FromOid_m1591878611 ();
extern "C" void ASN1Convert_ToInt32_m2142816542 ();
extern "C" void ASN1Convert_ToOid_m3041377917 ();
extern "C" void ASN1Convert_ToDateTime_m3552379352 ();
extern "C" void BitConverterLE_GetUIntBytes_m2947082112 ();
extern "C" void BitConverterLE_GetBytes_m3351943523 ();
extern "C" void BitConverterLE_UShortFromBytes_m489294661 ();
extern "C" void BitConverterLE_UIntFromBytes_m3748439035 ();
extern "C" void BitConverterLE_ULongFromBytes_m268960415 ();
extern "C" void BitConverterLE_ToInt16_m474007711 ();
extern "C" void BitConverterLE_ToInt32_m2972956219 ();
extern "C" void BitConverterLE_ToSingle_m1696218306 ();
extern "C" void BitConverterLE_ToDouble_m3522125088 ();
extern "C" void BlockProcessor__ctor_m3923969706 ();
extern "C" void BlockProcessor_Finalize_m2837824247 ();
extern "C" void BlockProcessor_Initialize_m3421916057 ();
extern "C" void BlockProcessor_Core_m3319995037 ();
extern "C" void BlockProcessor_Core_m1962461118 ();
extern "C" void BlockProcessor_Final_m1037678916 ();
extern "C" void CryptoConvert_ToInt32LE_m1342791765 ();
extern "C" void CryptoConvert_ToUInt32LE_m3548106783 ();
extern "C" void CryptoConvert_GetBytesLE_m1103380998 ();
extern "C" void CryptoConvert_ToCapiPrivateKeyBlob_m2393656989 ();
extern "C" void CryptoConvert_FromCapiPublicKeyBlob_m59550354 ();
extern "C" void CryptoConvert_FromCapiPublicKeyBlob_m4230898509 ();
extern "C" void CryptoConvert_ToCapiPublicKeyBlob_m435972191 ();
extern "C" void CryptoConvert_ToCapiKeyBlob_m3562765047 ();
extern "C" void DSAManaged__ctor_m825313286 ();
extern "C" void DSAManaged_add_KeyGenerated_m2286829514 ();
extern "C" void DSAManaged_remove_KeyGenerated_m413000032 ();
extern "C" void DSAManaged_Finalize_m4258386870 ();
extern "C" void DSAManaged_Generate_m2124060824 ();
extern "C" void DSAManaged_GenerateKeyPair_m2354969948 ();
extern "C" void DSAManaged_add_m366118670 ();
extern "C" void DSAManaged_GenerateParams_m384040316 ();
extern "C" void DSAManaged_get_Random_m2411009448 ();
extern "C" void DSAManaged_get_KeySize_m489524026 ();
extern "C" void DSAManaged_get_PublicOnly_m4157486105 ();
extern "C" void DSAManaged_NormalizeArray_m2844092920 ();
extern "C" void DSAManaged_ExportParameters_m3125165151 ();
extern "C" void DSAManaged_ImportParameters_m4236950267 ();
extern "C" void DSAManaged_CreateSignature_m1543369866 ();
extern "C" void DSAManaged_VerifySignature_m2542559486 ();
extern "C" void DSAManaged_Dispose_m885681596 ();
extern "C" void KeyGeneratedEventHandler__ctor_m3788308642 ();
extern "C" void KeyGeneratedEventHandler_Invoke_m3199187352 ();
extern "C" void KeyGeneratedEventHandler_BeginInvoke_m2401238559 ();
extern "C" void KeyGeneratedEventHandler_EndInvoke_m1876986809 ();
extern "C" void KeyBuilder_get_Rng_m2329308910 ();
extern "C" void KeyBuilder_Key_m1324353598 ();
extern "C" void KeyBuilder_IV_m3167732669 ();
extern "C" void KeyPairPersistence__ctor_m1451632529 ();
extern "C" void KeyPairPersistence__ctor_m282806890 ();
extern "C" void KeyPairPersistence__cctor_m1615101261 ();
extern "C" void KeyPairPersistence_get_Filename_m3817558798 ();
extern "C" void KeyPairPersistence_get_KeyValue_m913559137 ();
extern "C" void KeyPairPersistence_set_KeyValue_m2997429917 ();
extern "C" void KeyPairPersistence_Load_m3650468242 ();
extern "C" void KeyPairPersistence_Save_m2616030637 ();
extern "C" void KeyPairPersistence_Remove_m2462309025 ();
extern "C" void KeyPairPersistence_get_UserPath_m3263003313 ();
extern "C" void KeyPairPersistence_get_MachinePath_m3279896206 ();
extern "C" void KeyPairPersistence__CanSecure_m2460431986 ();
extern "C" void KeyPairPersistence__ProtectUser_m3343002560 ();
extern "C" void KeyPairPersistence__ProtectMachine_m775740381 ();
extern "C" void KeyPairPersistence__IsUserProtected_m2266257435 ();
extern "C" void KeyPairPersistence__IsMachineProtected_m2666443600 ();
extern "C" void KeyPairPersistence_CanSecure_m1918655102 ();
extern "C" void KeyPairPersistence_ProtectUser_m691605374 ();
extern "C" void KeyPairPersistence_ProtectMachine_m2277236355 ();
extern "C" void KeyPairPersistence_IsUserProtected_m1144613255 ();
extern "C" void KeyPairPersistence_IsMachineProtected_m2149987251 ();
extern "C" void KeyPairPersistence_get_CanChange_m2639804921 ();
extern "C" void KeyPairPersistence_get_UseDefaultKeyContainer_m3169890059 ();
extern "C" void KeyPairPersistence_get_UseMachineKeyStore_m1676827332 ();
extern "C" void KeyPairPersistence_get_ContainerName_m1970806748 ();
extern "C" void KeyPairPersistence_Copy_m2514706484 ();
extern "C" void KeyPairPersistence_FromXml_m615148467 ();
extern "C" void KeyPairPersistence_ToXml_m507669548 ();
extern "C" void MACAlgorithm__ctor_m1327376633 ();
extern "C" void MACAlgorithm_Initialize_m2073140225 ();
extern "C" void MACAlgorithm_Core_m1291994500 ();
extern "C" void MACAlgorithm_Final_m295519667 ();
extern "C" void PKCS1__cctor_m2725722605 ();
extern "C" void PKCS1_Compare_m3440795783 ();
extern "C" void PKCS1_I2OSP_m2868122941 ();
extern "C" void PKCS1_OS2IP_m1068728329 ();
extern "C" void PKCS1_RSAEP_m4105321444 ();
extern "C" void PKCS1_RSASP1_m447018256 ();
extern "C" void PKCS1_RSAVP1_m3458839365 ();
extern "C" void PKCS1_Encrypt_v15_m385206638 ();
extern "C" void PKCS1_Sign_v15_m2716890696 ();
extern "C" void PKCS1_Verify_v15_m527674405 ();
extern "C" void PKCS1_Verify_v15_m2131582328 ();
extern "C" void PKCS1_Encode_v15_m424069037 ();
extern "C" void EncryptedPrivateKeyInfo__ctor_m299012888 ();
extern "C" void EncryptedPrivateKeyInfo__ctor_m647322062 ();
extern "C" void EncryptedPrivateKeyInfo_get_Algorithm_m3741750336 ();
extern "C" void EncryptedPrivateKeyInfo_get_EncryptedData_m2865828442 ();
extern "C" void EncryptedPrivateKeyInfo_get_Salt_m267749150 ();
extern "C" void EncryptedPrivateKeyInfo_get_IterationCount_m3882266506 ();
extern "C" void EncryptedPrivateKeyInfo_Decode_m4290686938 ();
extern "C" void PrivateKeyInfo__ctor_m2289224748 ();
extern "C" void PrivateKeyInfo__ctor_m2773314483 ();
extern "C" void PrivateKeyInfo_get_PrivateKey_m2864070573 ();
extern "C" void PrivateKeyInfo_Decode_m2782298882 ();
extern "C" void PrivateKeyInfo_RemoveLeadingZero_m3554138665 ();
extern "C" void PrivateKeyInfo_Normalize_m2688416120 ();
extern "C" void PrivateKeyInfo_DecodeRSA_m1604240262 ();
extern "C" void PrivateKeyInfo_DecodeDSA_m2761492714 ();
extern "C" void RSAManaged__ctor_m4104718293 ();
extern "C" void RSAManaged_add_KeyGenerated_m2743778230 ();
extern "C" void RSAManaged_remove_KeyGenerated_m1304981015 ();
extern "C" void RSAManaged_Finalize_m996292420 ();
extern "C" void RSAManaged_GenerateKeyPair_m1691973565 ();
extern "C" void RSAManaged_get_KeySize_m388882796 ();
extern "C" void RSAManaged_get_PublicOnly_m3311538867 ();
extern "C" void RSAManaged_DecryptValue_m4061971567 ();
extern "C" void RSAManaged_EncryptValue_m4114751905 ();
extern "C" void RSAManaged_ExportParameters_m2767124364 ();
extern "C" void RSAManaged_ImportParameters_m1077493558 ();
extern "C" void RSAManaged_Dispose_m1706711259 ();
extern "C" void RSAManaged_ToXmlString_m3234843640 ();
extern "C" void RSAManaged_get_IsCrtPossible_m1210928227 ();
extern "C" void RSAManaged_GetPaddedValue_m1049410827 ();
extern "C" void KeyGeneratedEventHandler__ctor_m1299821668 ();
extern "C" void KeyGeneratedEventHandler_Invoke_m3228903531 ();
extern "C" void KeyGeneratedEventHandler_BeginInvoke_m714135090 ();
extern "C" void KeyGeneratedEventHandler_EndInvoke_m1786385152 ();
extern "C" void SymmetricTransform__ctor_m1975106537 ();
extern "C" void SymmetricTransform_System_IDisposable_Dispose_m1742260198 ();
extern "C" void SymmetricTransform_Finalize_m538552370 ();
extern "C" void SymmetricTransform_Dispose_m596126426 ();
extern "C" void SymmetricTransform_get_CanReuseTransform_m271089955 ();
extern "C" void SymmetricTransform_Transform_m174988247 ();
extern "C" void SymmetricTransform_CBC_m3692369966 ();
extern "C" void SymmetricTransform_CFB_m709939617 ();
extern "C" void SymmetricTransform_OFB_m3913775331 ();
extern "C" void SymmetricTransform_CTS_m2019337593 ();
extern "C" void SymmetricTransform_CheckInput_m1396199888 ();
extern "C" void SymmetricTransform_TransformBlock_m2138610695 ();
extern "C" void SymmetricTransform_get_KeepLastBlock_m3387053162 ();
extern "C" void SymmetricTransform_InternalTransformBlock_m640960295 ();
extern "C" void SymmetricTransform_Random_m2149136044 ();
extern "C" void SymmetricTransform_ThrowBadPaddingException_m3379056639 ();
extern "C" void SymmetricTransform_FinalEncrypt_m4159191400 ();
extern "C" void SymmetricTransform_FinalDecrypt_m147017762 ();
extern "C" void SymmetricTransform_TransformFinalBlock_m1249457332 ();
extern "C" void ContentInfo__ctor_m2175703642 ();
extern "C" void ContentInfo__ctor_m3806313524 ();
extern "C" void ContentInfo__ctor_m4254357018 ();
extern "C" void ContentInfo__ctor_m3774822752 ();
extern "C" void ContentInfo_get_ASN1_m1622402385 ();
extern "C" void ContentInfo_get_Content_m3812643298 ();
extern "C" void ContentInfo_set_Content_m2819593409 ();
extern "C" void ContentInfo_get_ContentType_m705567708 ();
extern "C" void ContentInfo_set_ContentType_m1623352580 ();
extern "C" void ContentInfo_GetASN1_m2294371716 ();
extern "C" void EncryptedData__ctor_m3652070480 ();
extern "C" void EncryptedData__ctor_m3289444422 ();
extern "C" void EncryptedData_get_EncryptionAlgorithm_m1688795622 ();
extern "C" void EncryptedData_get_EncryptedContent_m3858156333 ();
extern "C" void StrongName__cctor_m3439962771 ();
extern "C" void StrongName_get_PublicKey_m47263336 ();
extern "C" void StrongName_get_PublicKeyToken_m1524306997 ();
extern "C" void StrongName_get_TokenAlgorithm_m3765006973 ();
extern "C" void PKCS12__ctor_m2188309115 ();
extern "C" void PKCS12__ctor_m4293971484 ();
extern "C" void PKCS12__ctor_m3397759167 ();
extern "C" void PKCS12__cctor_m3109607630 ();
extern "C" void PKCS12_Decode_m2321190257 ();
extern "C" void PKCS12_Finalize_m3111395487 ();
extern "C" void PKCS12_set_Password_m4004509738 ();
extern "C" void PKCS12_get_IterationCount_m1734635322 ();
extern "C" void PKCS12_set_IterationCount_m3469889302 ();
extern "C" void PKCS12_get_Certificates_m1819594382 ();
extern "C" void PKCS12_get_RNG_m403016427 ();
extern "C" void PKCS12_Compare_m2821129482 ();
extern "C" void PKCS12_GetSymmetricAlgorithm_m3630792258 ();
extern "C" void PKCS12_Decrypt_m2054700421 ();
extern "C" void PKCS12_Decrypt_m4089729530 ();
extern "C" void PKCS12_Encrypt_m172278618 ();
extern "C" void PKCS12_GetExistingParameters_m1811502439 ();
extern "C" void PKCS12_AddPrivateKey_m627015820 ();
extern "C" void PKCS12_ReadSafeBag_m2230528012 ();
extern "C" void PKCS12_CertificateSafeBag_m1158114043 ();
extern "C" void PKCS12_MAC_m3266389236 ();
extern "C" void PKCS12_GetBytes_m3948157025 ();
extern "C" void PKCS12_EncryptedContentInfo_m335804703 ();
extern "C" void PKCS12_AddCertificate_m1113610247 ();
extern "C" void PKCS12_AddCertificate_m1603297645 ();
extern "C" void PKCS12_RemoveCertificate_m3573824403 ();
extern "C" void PKCS12_RemoveCertificate_m4279394528 ();
extern "C" void PKCS12_Clone_m1907616559 ();
extern "C" void PKCS12_get_MaximumPasswordLength_m1622540742 ();
extern "C" void DeriveBytes__ctor_m1630711249 ();
extern "C" void DeriveBytes__cctor_m3363162864 ();
extern "C" void DeriveBytes_set_HashName_m1466264544 ();
extern "C" void DeriveBytes_set_IterationCount_m1012983914 ();
extern "C" void DeriveBytes_set_Password_m1776827631 ();
extern "C" void DeriveBytes_set_Salt_m4247292830 ();
extern "C" void DeriveBytes_Adjust_m73010620 ();
extern "C" void DeriveBytes_Derive_m1432759067 ();
extern "C" void DeriveBytes_DeriveKey_m1070693331 ();
extern "C" void DeriveBytes_DeriveIV_m1377512391 ();
extern "C" void DeriveBytes_DeriveMAC_m333834467 ();
extern "C" void SafeBag__ctor_m885430396 ();
extern "C" void SafeBag_get_BagOID_m3200989054 ();
extern "C" void SafeBag_get_ASN1_m3927983005 ();
extern "C" void X501__cctor_m176587435 ();
extern "C" void X501_ToString_m606787655 ();
extern "C" void X501_ToString_m4265468278 ();
extern "C" void X501_AppendEntry_m4126472065 ();
extern "C" void X509Certificate__ctor_m3974010651 ();
extern "C" void X509Certificate__cctor_m2281686218 ();
extern "C" void X509Certificate_Parse_m2601788492 ();
extern "C" void X509Certificate_GetUnsignedBigInteger_m125969945 ();
extern "C" void X509Certificate_get_DSA_m2379531329 ();
extern "C" void X509Certificate_get_IssuerName_m1190634728 ();
extern "C" void X509Certificate_get_KeyAlgorithmParameters_m3328452325 ();
extern "C" void X509Certificate_get_PublicKey_m1559630494 ();
extern "C" void X509Certificate_get_RawData_m2126743996 ();
extern "C" void X509Certificate_get_SubjectName_m4016633973 ();
extern "C" void X509Certificate_get_ValidFrom_m1391977265 ();
extern "C" void X509Certificate_get_ValidUntil_m4052955887 ();
extern "C" void X509Certificate_GetIssuerName_m1761647748 ();
extern "C" void X509Certificate_GetSubjectName_m196414200 ();
extern "C" void X509Certificate_PEM_m2377423875 ();
extern "C" void X509CertificateCollection__ctor_m4087327610 ();
extern "C" void X509CertificateCollection_System_Collections_IEnumerable_GetEnumerator_m3571738219 ();
extern "C" void X509CertificateCollection_get_Item_m3973324547 ();
extern "C" void X509CertificateCollection_Add_m1343769477 ();
extern "C" void X509CertificateCollection_GetEnumerator_m2813315757 ();
extern "C" void X509CertificateCollection_GetHashCode_m3353991612 ();
extern "C" void X509CertificateEnumerator__ctor_m2380153920 ();
extern "C" void X509CertificateEnumerator_System_Collections_IEnumerator_get_Current_m425597426 ();
extern "C" void X509CertificateEnumerator_System_Collections_IEnumerator_MoveNext_m1313749686 ();
extern "C" void X509CertificateEnumerator_System_Collections_IEnumerator_Reset_m1280518935 ();
extern "C" void X509CertificateEnumerator_get_Current_m3401736547 ();
extern "C" void X509CertificateEnumerator_MoveNext_m470429900 ();
extern "C" void X509CertificateEnumerator_Reset_m3629273990 ();
extern "C" void X509Extension__ctor_m592474861 ();
extern "C" void X509Extension_Decode_m3340505567 ();
extern "C" void X509Extension_Equals_m1026821796 ();
extern "C" void X509Extension_GetHashCode_m3905824097 ();
extern "C" void X509Extension_WriteLine_m400107663 ();
extern "C" void X509Extension_ToString_m1108071185 ();
extern "C" void X509ExtensionCollection__ctor_m2229994607 ();
extern "C" void X509ExtensionCollection__ctor_m1964702411 ();
extern "C" void X509ExtensionCollection_System_Collections_IEnumerable_GetEnumerator_m3539740658 ();
extern "C" void SecurityParser__ctor_m2190542591 ();
extern "C" void SecurityParser_LoadXml_m2527250266 ();
extern "C" void SecurityParser_ToXml_m2002718429 ();
extern "C" void SecurityParser_OnStartParsing_m713460294 ();
extern "C" void SecurityParser_OnProcessingInstruction_m156777986 ();
extern "C" void SecurityParser_OnIgnorableWhitespace_m916920983 ();
extern "C" void SecurityParser_OnStartElement_m1212613838 ();
extern "C" void SecurityParser_OnEndElement_m2404776146 ();
extern "C" void SecurityParser_OnChars_m1741899660 ();
extern "C" void SecurityParser_OnEndParsing_m630820298 ();
extern "C" void SmallXmlParser__ctor_m2019585097 ();
extern "C" void SmallXmlParser_Error_m429104818 ();
extern "C" void SmallXmlParser_UnexpectedEndError_m1157145880 ();
extern "C" void SmallXmlParser_IsNameChar_m1965865578 ();
extern "C" void SmallXmlParser_IsWhitespace_m2316828733 ();
extern "C" void SmallXmlParser_SkipWhitespaces_m1707474564 ();
extern "C" void SmallXmlParser_HandleWhitespaces_m1447467011 ();
extern "C" void SmallXmlParser_SkipWhitespaces_m785728345 ();
extern "C" void SmallXmlParser_Peek_m3745074368 ();
extern "C" void SmallXmlParser_Read_m937609928 ();
extern "C" void SmallXmlParser_Expect_m1449932214 ();
extern "C" void SmallXmlParser_ReadUntil_m1248365356 ();
extern "C" void SmallXmlParser_ReadName_m1261648197 ();
extern "C" void SmallXmlParser_Parse_m781232591 ();
extern "C" void SmallXmlParser_Cleanup_m814219253 ();
extern "C" void SmallXmlParser_ReadContent_m1339062023 ();
extern "C" void SmallXmlParser_HandleBufferedContent_m38070252 ();
extern "C" void SmallXmlParser_ReadCharacters_m3036429763 ();
extern "C" void SmallXmlParser_ReadReference_m2147555252 ();
extern "C" void SmallXmlParser_ReadCharacterReference_m956624000 ();
extern "C" void SmallXmlParser_ReadAttribute_m143848528 ();
extern "C" void SmallXmlParser_ReadCDATASection_m181360986 ();
extern "C" void SmallXmlParser_ReadComment_m4172889697 ();
extern "C" void AttrListImpl__ctor_m2113556480 ();
extern "C" void AttrListImpl_get_Length_m2184336069 ();
extern "C" void AttrListImpl_GetName_m1423383098 ();
extern "C" void AttrListImpl_GetValue_m119139426 ();
extern "C" void AttrListImpl_GetValue_m525211908 ();
extern "C" void AttrListImpl_get_Names_m2552952956 ();
extern "C" void AttrListImpl_get_Values_m3882443736 ();
extern "C" void AttrListImpl_Clear_m4127928365 ();
extern "C" void AttrListImpl_Add_m3390079384 ();
extern "C" void SmallXmlParserException__ctor_m3660590675 ();
extern "C" void __Il2CppComDelegate_Finalize_m4081530528 ();
extern "C" void __Il2CppComObject_Finalize_m542937421 ();
extern "C" void AccessViolationException__ctor_m1988585420 ();
extern "C" void AccessViolationException__ctor_m2115873943 ();
extern "C" void ActivationContext_Finalize_m4190316793 ();
extern "C" void ActivationContext_Dispose_m458277660 ();
extern "C" void ActivationContext_Dispose_m3745846357 ();
extern "C" void Activator_CreateInstance_m88608359 ();
extern "C" void Activator_CreateInstance_m291998472 ();
extern "C" void Activator_CreateInstance_m3002907732 ();
extern "C" void Activator_CreateInstance_m3926027424 ();
extern "C" void Activator_CreateInstance_m630832716 ();
extern "C" void Activator_CheckType_m728321487 ();
extern "C" void Activator_CheckAbstractType_m2922392263 ();
extern "C" void Activator_CreateInstanceInternal_m1316031764 ();
extern "C" void AppDomain__ctor_m858955160 ();
extern "C" void AppDomain_add_UnhandledException_m1681775214 ();
extern "C" void AppDomain_remove_UnhandledException_m1958459414 ();
extern "C" void AppDomain_getFriendlyName_m2263966558 ();
extern "C" void AppDomain_getCurDomain_m617417482 ();
extern "C" void AppDomain_get_CurrentDomain_m4216043527 ();
extern "C" void AppDomain_LoadAssembly_m1223519328 ();
extern "C" void AppDomain_Load_m1049886220 ();
extern "C" void AppDomain_Load_m2889673107 ();
extern "C" void AppDomain_InternalSetContext_m3764293861 ();
extern "C" void AppDomain_InternalGetContext_m4048436193 ();
extern "C" void AppDomain_InternalGetDefaultContext_m2984334519 ();
extern "C" void AppDomain_InternalGetProcessGuid_m4049533258 ();
extern "C" void AppDomain_GetProcessGuid_m246918367 ();
extern "C" void AppDomain_ToString_m2545816157 ();
extern "C" void AppDomain_DoTypeResolve_m3724057253 ();
extern "C" void AppDomainInitializer__ctor_m1409537730 ();
extern "C" void AppDomainInitializer_Invoke_m1518556050 ();
extern "C" void AppDomainInitializer_BeginInvoke_m3107563474 ();
extern "C" void AppDomainInitializer_EndInvoke_m1844416365 ();
extern "C" void AppDomainSetup__ctor_m2376813191 ();
extern "C" void ApplicationException__ctor_m2375465202 ();
extern "C" void ApplicationException__ctor_m3389601759 ();
extern "C" void ApplicationException__ctor_m1140634725 ();
extern "C" void ApplicationIdentity_ToString_m3720773450 ();
extern "C" void ArgIterator_Equals_m2080485439_AdjustorThunk ();
extern "C" void ArgIterator_GetHashCode_m3119876063_AdjustorThunk ();
extern "C" void ArgumentException__ctor_m996376509 ();
extern "C" void ArgumentException__ctor_m775170528 ();
extern "C" void ArgumentException__ctor_m59237838 ();
extern "C" void ArgumentException__ctor_m2239500235 ();
extern "C" void ArgumentException__ctor_m2550830142 ();
extern "C" void ArgumentException__ctor_m2187783241 ();
extern "C" void ArgumentException_get_ParamName_m2634504495 ();
extern "C" void ArgumentException_get_Message_m2068856061 ();
extern "C" void ArgumentNullException__ctor_m348650132 ();
extern "C" void ArgumentNullException__ctor_m2607242403 ();
extern "C" void ArgumentNullException__ctor_m532419484 ();
extern "C" void ArgumentNullException__ctor_m367278376 ();
extern "C" void ArgumentOutOfRangeException__ctor_m3684908528 ();
extern "C" void ArgumentOutOfRangeException__ctor_m2829693980 ();
extern "C" void ArgumentOutOfRangeException__ctor_m1438035441 ();
extern "C" void ArgumentOutOfRangeException__ctor_m2416666627 ();
extern "C" void ArgumentOutOfRangeException__ctor_m3822258770 ();
extern "C" void ArgumentOutOfRangeException_get_Message_m373040981 ();
extern "C" void ArithmeticException__ctor_m3165890130 ();
extern "C" void ArithmeticException__ctor_m2007013933 ();
extern "C" void ArithmeticException__ctor_m1134843527 ();
extern "C" void Array__ctor_m3029133333 ();
extern "C" void Array_System_Collections_IList_get_Item_m3019299456 ();
extern "C" void Array_System_Collections_IList_set_Item_m3504915579 ();
extern "C" void Array_System_Collections_IList_Add_m991819160 ();
extern "C" void Array_System_Collections_IList_Clear_m2159605959 ();
extern "C" void Array_System_Collections_IList_Contains_m3113295757 ();
extern "C" void Array_System_Collections_IList_IndexOf_m523778419 ();
extern "C" void Array_System_Collections_IList_Insert_m2028037964 ();
extern "C" void Array_System_Collections_IList_Remove_m912497296 ();
extern "C" void Array_System_Collections_IList_RemoveAt_m866728844 ();
extern "C" void Array_System_Collections_ICollection_get_Count_m1623835074 ();
extern "C" void Array_InternalArray__ICollection_get_Count_m1984938948 ();
extern "C" void Array_InternalArray__ICollection_get_IsReadOnly_m179114617 ();
extern "C" void Array_InternalArray__ICollection_Clear_m3667028958 ();
extern "C" void Array_InternalArray__RemoveAt_m2893298614 ();
extern "C" void Array_get_Length_m1201869234 ();
extern "C" void Array_get_LongLength_m63201949 ();
extern "C" void Array_get_Rank_m3141701998 ();
extern "C" void Array_GetRank_m1304432205 ();
extern "C" void Array_GetLength_m3490892354 ();
extern "C" void Array_GetLongLength_m2596545479 ();
extern "C" void Array_GetLowerBound_m817811428 ();
extern "C" void Array_GetValue_m1494120325 ();
extern "C" void Array_SetValue_m3927045595 ();
extern "C" void Array_GetValueImpl_m3982717948 ();
extern "C" void Array_SetValueImpl_m1024233787 ();
extern "C" void Array_FastCopy_m4105485745 ();
extern "C" void Array_CreateInstanceImpl_m709568271 ();
extern "C" void Array_get_IsSynchronized_m3379284387 ();
extern "C" void Array_get_SyncRoot_m2565384370 ();
extern "C" void Array_get_IsFixedSize_m693448514 ();
extern "C" void Array_get_IsReadOnly_m3136842013 ();
extern "C" void Array_GetEnumerator_m591799487 ();
extern "C" void Array_GetUpperBound_m152106182 ();
extern "C" void Array_GetValue_m610923591 ();
extern "C" void Array_GetValue_m2708821274 ();
extern "C" void Array_GetValue_m3357507770 ();
extern "C" void Array_GetValue_m1430766714 ();
extern "C" void Array_GetValue_m3155219782 ();
extern "C" void Array_GetValue_m217652931 ();
extern "C" void Array_SetValue_m1145170836 ();
extern "C" void Array_SetValue_m3597386468 ();
extern "C" void Array_SetValue_m1904644573 ();
extern "C" void Array_SetValue_m2050226905 ();
extern "C" void Array_SetValue_m2680909523 ();
extern "C" void Array_SetValue_m682265580 ();
extern "C" void Array_CreateInstance_m3751849812 ();
extern "C" void Array_CreateInstance_m3124107544 ();
extern "C" void Array_CreateInstance_m573036287 ();
extern "C" void Array_CreateInstance_m666131206 ();
extern "C" void Array_CreateInstance_m1673959785 ();
extern "C" void Array_GetIntArray_m1192539842 ();
extern "C" void Array_CreateInstance_m3882898988 ();
extern "C" void Array_GetValue_m3754306304 ();
extern "C" void Array_SetValue_m4108726102 ();
extern "C" void Array_BinarySearch_m2098844084 ();
extern "C" void Array_BinarySearch_m1298651580 ();
extern "C" void Array_BinarySearch_m2500870911 ();
extern "C" void Array_BinarySearch_m1629142740 ();
extern "C" void Array_DoBinarySearch_m199061391 ();
extern "C" void Array_Clear_m305290227 ();
extern "C" void Array_ClearInternal_m2225062862 ();
extern "C" void Array_Clone_m1115294170 ();
extern "C" void Array_Copy_m392395275 ();
extern "C" void Array_Copy_m1287631566 ();
extern "C" void Array_Copy_m2652655318 ();
extern "C" void Array_Copy_m3698620115 ();
extern "C" void Array_IndexOf_m3436793169 ();
extern "C" void Array_IndexOf_m3442506776 ();
extern "C" void Array_IndexOf_m1260278658 ();
extern "C" void Array_Initialize_m486260706 ();
extern "C" void Array_LastIndexOf_m827387485 ();
extern "C" void Array_LastIndexOf_m362377999 ();
extern "C" void Array_LastIndexOf_m234007524 ();
extern "C" void Array_get_swapper_m168880643 ();
extern "C" void Array_Reverse_m3921569028 ();
extern "C" void Array_Reverse_m3946279211 ();
extern "C" void Array_Sort_m3032378294 ();
extern "C" void Array_Sort_m243170594 ();
extern "C" void Array_Sort_m1166375231 ();
extern "C" void Array_Sort_m1504758793 ();
extern "C" void Array_Sort_m2496372837 ();
extern "C" void Array_Sort_m2232750079 ();
extern "C" void Array_Sort_m2717880702 ();
extern "C" void Array_Sort_m2668013149 ();
extern "C" void Array_int_swapper_m683323463 ();
extern "C" void Array_obj_swapper_m619320533 ();
extern "C" void Array_slow_swapper_m285402653 ();
extern "C" void Array_double_swapper_m1990147288 ();
extern "C" void Array_new_gap_m3865331624 ();
extern "C" void Array_combsort_m4188724368 ();
extern "C" void Array_combsort_m6485776 ();
extern "C" void Array_combsort_m1932213202 ();
extern "C" void Array_qsort_m2839882233 ();
extern "C" void Array_swap_m3590469174 ();
extern "C" void Array_compare_m1305042596 ();
extern "C" void Array_CopyTo_m332099161 ();
extern "C" void Array_CopyTo_m657485551 ();
extern "C" void Array_ConstrainedCopy_m1738878172 ();
extern "C" void SimpleEnumerator__ctor_m3957398836 ();
extern "C" void SimpleEnumerator_get_Current_m2189869981 ();
extern "C" void SimpleEnumerator_MoveNext_m3424188367 ();
extern "C" void SimpleEnumerator_Reset_m569135910 ();
extern "C" void SimpleEnumerator_Clone_m1124827893 ();
extern "C" void Swapper__ctor_m3765048366 ();
extern "C" void Swapper_Invoke_m1478879831 ();
extern "C" void Swapper_BeginInvoke_m2144994511 ();
extern "C" void Swapper_EndInvoke_m539706856 ();
extern "C" void ArrayTypeMismatchException__ctor_m3940730502 ();
extern "C" void ArrayTypeMismatchException__ctor_m2955763082 ();
extern "C" void ArrayTypeMismatchException__ctor_m847384952 ();
extern "C" void AssemblyLoadEventHandler__ctor_m1692116705 ();
extern "C" void AssemblyLoadEventHandler_Invoke_m3765877321 ();
extern "C" void AssemblyLoadEventHandler_BeginInvoke_m368817848 ();
extern "C" void AssemblyLoadEventHandler_EndInvoke_m240517751 ();
extern "C" void AsyncCallback__ctor_m242081609 ();
extern "C" void AsyncCallback_Invoke_m2380865724 ();
extern "C" void AsyncCallback_BeginInvoke_m188169670 ();
extern "C" void AsyncCallback_EndInvoke_m2524004693 ();
extern "C" void Attribute__ctor_m2897993467 ();
extern "C" void Attribute_CheckParameters_m2682049816 ();
extern "C" void Attribute_GetCustomAttribute_m3805733629 ();
extern "C" void Attribute_GetCustomAttribute_m135430056 ();
extern "C" void Attribute_GetHashCode_m2188337821 ();
extern "C" void Attribute_IsDefined_m1167411862 ();
extern "C" void Attribute_IsDefined_m936380774 ();
extern "C" void Attribute_IsDefined_m3771639299 ();
extern "C" void Attribute_IsDefined_m2408875580 ();
extern "C" void Attribute_Equals_m4006452586 ();
extern "C" void AttributeUsageAttribute__ctor_m1170715599 ();
extern "C" void AttributeUsageAttribute_get_AllowMultiple_m112078363 ();
extern "C" void AttributeUsageAttribute_set_AllowMultiple_m1313295265 ();
extern "C" void AttributeUsageAttribute_get_Inherited_m1370482972 ();
extern "C" void AttributeUsageAttribute_set_Inherited_m2205070461 ();
extern "C" void BitConverter__cctor_m3477343659 ();
extern "C" void BitConverter_AmILittleEndian_m1543963744 ();
extern "C" void BitConverter_DoubleWordsAreSwapped_m4151660403 ();
extern "C" void BitConverter_DoubleToInt64Bits_m1839328798 ();
extern "C" void BitConverter_GetBytes_m639363113 ();
extern "C" void BitConverter_GetBytes_m927302127 ();
extern "C" void BitConverter_PutBytes_m3569650077 ();
extern "C" void BitConverter_ToInt64_m2454337822 ();
extern "C" void BitConverter_ToString_m1278078580 ();
extern "C" void BitConverter_ToString_m3550237875 ();
extern "C" void Boolean__cctor_m1118817891 ();
extern "C" void Boolean_System_IConvertible_ToType_m1911714260_AdjustorThunk ();
extern "C" void Boolean_System_IConvertible_ToBoolean_m3968084210_AdjustorThunk ();
extern "C" void Boolean_System_IConvertible_ToByte_m2164451010_AdjustorThunk ();
extern "C" void Boolean_System_IConvertible_ToChar_m172360724_AdjustorThunk ();
extern "C" void Boolean_System_IConvertible_ToDateTime_m420405767_AdjustorThunk ();
extern "C" void Boolean_System_IConvertible_ToDecimal_m3994102133_AdjustorThunk ();
extern "C" void Boolean_System_IConvertible_ToDouble_m3984970665_AdjustorThunk ();
extern "C" void Boolean_System_IConvertible_ToInt16_m1273677989_AdjustorThunk ();
extern "C" void Boolean_System_IConvertible_ToInt32_m3450098220_AdjustorThunk ();
extern "C" void Boolean_System_IConvertible_ToInt64_m3901326996_AdjustorThunk ();
extern "C" void Boolean_System_IConvertible_ToSByte_m2808560259_AdjustorThunk ();
extern "C" void Boolean_System_IConvertible_ToSingle_m3283266050_AdjustorThunk ();
extern "C" void Boolean_System_IConvertible_ToUInt16_m1513902759_AdjustorThunk ();
extern "C" void Boolean_System_IConvertible_ToUInt32_m2868542407_AdjustorThunk ();
extern "C" void Boolean_System_IConvertible_ToUInt64_m1116486545_AdjustorThunk ();
extern "C" void Boolean_CompareTo_m2978809512_AdjustorThunk ();
extern "C" void Boolean_Equals_m870298236_AdjustorThunk ();
extern "C" void Boolean_CompareTo_m384261152_AdjustorThunk ();
extern "C" void Boolean_Equals_m67531367_AdjustorThunk ();
extern "C" void Boolean_GetHashCode_m1880341182_AdjustorThunk ();
extern "C" void Boolean_Parse_m2649862989 ();
extern "C" void Boolean_ToString_m3488188482_AdjustorThunk ();
extern "C" void Boolean_ToString_m2245119515_AdjustorThunk ();
extern "C" void Buffer_ByteLength_m2749019809 ();
extern "C" void Buffer_BlockCopy_m3811116228 ();
extern "C" void Buffer_ByteLengthInternal_m1933246543 ();
extern "C" void Buffer_BlockCopyInternal_m2382420234 ();
extern "C" void Byte_System_IConvertible_ToType_m88250113_AdjustorThunk ();
extern "C" void Byte_System_IConvertible_ToBoolean_m3245241636_AdjustorThunk ();
extern "C" void Byte_System_IConvertible_ToByte_m4110595847_AdjustorThunk ();
extern "C" void Byte_System_IConvertible_ToChar_m4280618669_AdjustorThunk ();
extern "C" void Byte_System_IConvertible_ToDateTime_m1684858944_AdjustorThunk ();
extern "C" void Byte_System_IConvertible_ToDecimal_m3821981855_AdjustorThunk ();
extern "C" void Byte_System_IConvertible_ToDouble_m3197564149_AdjustorThunk ();
extern "C" void Byte_System_IConvertible_ToInt16_m2803371991_AdjustorThunk ();
extern "C" void Byte_System_IConvertible_ToInt32_m2431199899_AdjustorThunk ();
extern "C" void Byte_System_IConvertible_ToInt64_m278954931_AdjustorThunk ();
extern "C" void Byte_System_IConvertible_ToSByte_m1717368897_AdjustorThunk ();
extern "C" void Byte_System_IConvertible_ToSingle_m1632252365_AdjustorThunk ();
extern "C" void Byte_System_IConvertible_ToUInt16_m1350916608_AdjustorThunk ();
extern "C" void Byte_System_IConvertible_ToUInt32_m4179074805_AdjustorThunk ();
extern "C" void Byte_System_IConvertible_ToUInt64_m1816849640_AdjustorThunk ();
extern "C" void Byte_CompareTo_m4289415996_AdjustorThunk ();
extern "C" void Byte_Equals_m737883369_AdjustorThunk ();
extern "C" void Byte_GetHashCode_m4191668055_AdjustorThunk ();
extern "C" void Byte_CompareTo_m223998094_AdjustorThunk ();
extern "C" void Byte_Equals_m3117312609_AdjustorThunk ();
extern "C" void Byte_Parse_m1111312940 ();
extern "C" void Byte_Parse_m1682496037 ();
extern "C" void Byte_Parse_m1988164567 ();
extern "C" void Byte_TryParse_m939492826 ();
extern "C" void Byte_TryParse_m2940174653 ();
extern "C" void Byte_ToString_m1557822775_AdjustorThunk ();
extern "C" void Byte_ToString_m2608387306_AdjustorThunk ();
extern "C" void Byte_ToString_m1549340082_AdjustorThunk ();
extern "C" void Byte_ToString_m3614819476_AdjustorThunk ();
extern "C" void Char__cctor_m1102732931 ();
extern "C" void Char_System_IConvertible_ToType_m2492435064_AdjustorThunk ();
extern "C" void Char_System_IConvertible_ToBoolean_m804699894_AdjustorThunk ();
extern "C" void Char_System_IConvertible_ToByte_m3404443014_AdjustorThunk ();
extern "C" void Char_System_IConvertible_ToChar_m3017032234_AdjustorThunk ();
extern "C" void Char_System_IConvertible_ToDateTime_m773762781_AdjustorThunk ();
extern "C" void Char_System_IConvertible_ToDecimal_m2171301353_AdjustorThunk ();
extern "C" void Char_System_IConvertible_ToDouble_m4144442232_AdjustorThunk ();
extern "C" void Char_System_IConvertible_ToInt16_m1057658865_AdjustorThunk ();
extern "C" void Char_System_IConvertible_ToInt32_m2846754021_AdjustorThunk ();
extern "C" void Char_System_IConvertible_ToInt64_m2958737617_AdjustorThunk ();
extern "C" void Char_System_IConvertible_ToSByte_m4109623252_AdjustorThunk ();
extern "C" void Char_System_IConvertible_ToSingle_m3590465548_AdjustorThunk ();
extern "C" void Char_System_IConvertible_ToUInt16_m2694042926_AdjustorThunk ();
extern "C" void Char_System_IConvertible_ToUInt32_m386846991_AdjustorThunk ();
extern "C" void Char_System_IConvertible_ToUInt64_m3445554083_AdjustorThunk ();
extern "C" void Char_GetDataTablePointers_m2369532848 ();
extern "C" void Char_CompareTo_m1319704274_AdjustorThunk ();
extern "C" void Char_Equals_m3975975134_AdjustorThunk ();
extern "C" void Char_CompareTo_m1094532493_AdjustorThunk ();
extern "C" void Char_Equals_m2148257832_AdjustorThunk ();
extern "C" void Char_GetHashCode_m1812214884_AdjustorThunk ();
extern "C" void Char_GetUnicodeCategory_m2960412812 ();
extern "C" void Char_IsDigit_m2282338421 ();
extern "C" void Char_IsLetter_m4005249031 ();
extern "C" void Char_IsLetterOrDigit_m537099147 ();
extern "C" void Char_IsLower_m4136845248 ();
extern "C" void Char_IsSurrogate_m2311845190 ();
extern "C" void Char_IsWhiteSpace_m3221414563 ();
extern "C" void Char_IsWhiteSpace_m1530390784 ();
extern "C" void Char_CheckParameter_m132596558 ();
extern "C" void Char_Parse_m3752310711 ();
extern "C" void Char_ToLower_m3379788551 ();
extern "C" void Char_ToLowerInvariant_m726107324 ();
extern "C" void Char_ToLower_m1387747883 ();
extern "C" void Char_ToUpper_m295432202 ();
extern "C" void Char_ToUpperInvariant_m1232897003 ();
extern "C" void Char_ToString_m2639631148_AdjustorThunk ();
extern "C" void Char_ToString_m3751942386_AdjustorThunk ();
extern "C" void CharEnumerator__ctor_m1229783824 ();
extern "C" void CharEnumerator_System_Collections_IEnumerator_get_Current_m1145526303 ();
extern "C" void CharEnumerator_System_IDisposable_Dispose_m3098382966 ();
extern "C" void CharEnumerator_get_Current_m1662192463 ();
extern "C" void CharEnumerator_Clone_m1869712149 ();
extern "C" void CharEnumerator_MoveNext_m3514895061 ();
extern "C" void CharEnumerator_Reset_m42259193 ();
extern "C" void CLSCompliantAttribute__ctor_m3885160237 ();
extern "C" void ArrayList__ctor_m2794524866 ();
extern "C" void ArrayList__ctor_m1887728087 ();
extern "C" void ArrayList__ctor_m154510267 ();
extern "C" void ArrayList__ctor_m4194149953 ();
extern "C" void ArrayList__cctor_m4132398432 ();
extern "C" void ArrayList_get_Item_m2930495086 ();
extern "C" void ArrayList_set_Item_m2576483835 ();
extern "C" void ArrayList_get_Count_m887745903 ();
extern "C" void ArrayList_get_Capacity_m4080213801 ();
extern "C" void ArrayList_set_Capacity_m3248783950 ();
extern "C" void ArrayList_get_IsReadOnly_m973064083 ();
extern "C" void ArrayList_get_IsSynchronized_m3511241072 ();
extern "C" void ArrayList_get_SyncRoot_m2760316478 ();
extern "C" void ArrayList_EnsureCapacity_m4174950996 ();
extern "C" void ArrayList_Shift_m1776187330 ();
extern "C" void ArrayList_Add_m1032699014 ();
extern "C" void ArrayList_Clear_m1311080298 ();
extern "C" void ArrayList_Contains_m4195983465 ();
extern "C" void ArrayList_IndexOf_m883124782 ();
extern "C" void ArrayList_IndexOf_m2275492146 ();
extern "C" void ArrayList_IndexOf_m3703138119 ();
extern "C" void ArrayList_Insert_m1094798812 ();
extern "C" void ArrayList_InsertRange_m2253703207 ();
extern "C" void ArrayList_Remove_m799697853 ();
extern "C" void ArrayList_RemoveAt_m3690146969 ();
extern "C" void ArrayList_CopyTo_m757982293 ();
extern "C" void ArrayList_CopyTo_m2576273666 ();
extern "C" void ArrayList_CopyTo_m2945543338 ();
extern "C" void ArrayList_GetEnumerator_m487894342 ();
extern "C" void ArrayList_AddRange_m2663779696 ();
extern "C" void ArrayList_Sort_m2874495278 ();
extern "C" void ArrayList_Sort_m886700160 ();
extern "C" void ArrayList_ToArray_m727107442 ();
extern "C" void ArrayList_ToArray_m3807389525 ();
extern "C" void ArrayList_Clone_m3646965770 ();
extern "C" void ArrayList_ThrowNewArgumentOutOfRangeException_m3066565130 ();
extern "C" void ArrayList_Synchronized_m3794807713 ();
extern "C" void ArrayList_ReadOnly_m540923450 ();
extern "C" void ArrayListWrapper__ctor_m4026520920 ();
extern "C" void ArrayListWrapper_get_Item_m2058811544 ();
extern "C" void ArrayListWrapper_set_Item_m813945246 ();
extern "C" void ArrayListWrapper_get_Count_m780777509 ();
extern "C" void ArrayListWrapper_get_Capacity_m2286970773 ();
extern "C" void ArrayListWrapper_set_Capacity_m1257029602 ();
extern "C" void ArrayListWrapper_get_IsReadOnly_m1675681686 ();
extern "C" void ArrayListWrapper_get_IsSynchronized_m169618214 ();
extern "C" void ArrayListWrapper_get_SyncRoot_m1638586154 ();
extern "C" void ArrayListWrapper_Add_m2263171382 ();
extern "C" void ArrayListWrapper_Clear_m3915659884 ();
extern "C" void ArrayListWrapper_Contains_m3377454897 ();
extern "C" void ArrayListWrapper_IndexOf_m2599761010 ();
extern "C" void ArrayListWrapper_IndexOf_m1717139328 ();
extern "C" void ArrayListWrapper_IndexOf_m3471093427 ();
extern "C" void ArrayListWrapper_Insert_m3262528890 ();
extern "C" void ArrayListWrapper_InsertRange_m2133272360 ();
extern "C" void ArrayListWrapper_Remove_m3546682274 ();
extern "C" void ArrayListWrapper_RemoveAt_m1795282462 ();
extern "C" void ArrayListWrapper_CopyTo_m1200013472 ();
extern "C" void ArrayListWrapper_CopyTo_m1723112735 ();
extern "C" void ArrayListWrapper_CopyTo_m2793294031 ();
extern "C" void ArrayListWrapper_GetEnumerator_m3997423037 ();
extern "C" void ArrayListWrapper_AddRange_m1210497169 ();
extern "C" void ArrayListWrapper_Clone_m4220701673 ();
extern "C" void ArrayListWrapper_Sort_m3808628793 ();
extern "C" void ArrayListWrapper_Sort_m3407883998 ();
extern "C" void ArrayListWrapper_ToArray_m3335508598 ();
extern "C" void ArrayListWrapper_ToArray_m441257990 ();
extern "C" void FixedSizeArrayListWrapper__ctor_m3391186433 ();
extern "C" void FixedSizeArrayListWrapper_get_ErrorMessage_m336185562 ();
extern "C" void FixedSizeArrayListWrapper_get_Capacity_m2425305815 ();
extern "C" void FixedSizeArrayListWrapper_set_Capacity_m538834318 ();
extern "C" void FixedSizeArrayListWrapper_Add_m664694773 ();
extern "C" void FixedSizeArrayListWrapper_AddRange_m3736495698 ();
extern "C" void FixedSizeArrayListWrapper_Clear_m678082358 ();
extern "C" void FixedSizeArrayListWrapper_Insert_m1499567299 ();
extern "C" void FixedSizeArrayListWrapper_InsertRange_m1361439377 ();
extern "C" void FixedSizeArrayListWrapper_Remove_m194226442 ();
extern "C" void FixedSizeArrayListWrapper_RemoveAt_m283739216 ();
extern "C" void ReadOnlyArrayListWrapper__ctor_m1765923926 ();
extern "C" void ReadOnlyArrayListWrapper_get_ErrorMessage_m1735536626 ();
extern "C" void ReadOnlyArrayListWrapper_get_IsReadOnly_m2043964128 ();
extern "C" void ReadOnlyArrayListWrapper_get_Item_m736811773 ();
extern "C" void ReadOnlyArrayListWrapper_set_Item_m1018028581 ();
extern "C" void ReadOnlyArrayListWrapper_Sort_m2106818998 ();
extern "C" void ReadOnlyArrayListWrapper_Sort_m3997210685 ();
extern "C" void SimpleEnumerator__ctor_m3507490065 ();
extern "C" void SimpleEnumerator__cctor_m4103124766 ();
extern "C" void SimpleEnumerator_Clone_m3144817616 ();
extern "C" void SimpleEnumerator_MoveNext_m1877966209 ();
extern "C" void SimpleEnumerator_get_Current_m695847894 ();
extern "C" void SimpleEnumerator_Reset_m601343442 ();
extern "C" void SynchronizedArrayListWrapper__ctor_m2601384036 ();
extern "C" void SynchronizedArrayListWrapper_get_Item_m1962441151 ();
extern "C" void SynchronizedArrayListWrapper_set_Item_m2014675391 ();
extern "C" void SynchronizedArrayListWrapper_get_Count_m2036547373 ();
extern "C" void SynchronizedArrayListWrapper_get_Capacity_m3829125592 ();
extern "C" void SynchronizedArrayListWrapper_set_Capacity_m1655621538 ();
extern "C" void SynchronizedArrayListWrapper_get_IsReadOnly_m1029404986 ();
extern "C" void SynchronizedArrayListWrapper_get_IsSynchronized_m3728150270 ();
extern "C" void SynchronizedArrayListWrapper_get_SyncRoot_m2309850440 ();
extern "C" void SynchronizedArrayListWrapper_Add_m1538663151 ();
extern "C" void SynchronizedArrayListWrapper_Clear_m1283139032 ();
extern "C" void SynchronizedArrayListWrapper_Contains_m2913803557 ();
extern "C" void SynchronizedArrayListWrapper_IndexOf_m2677505790 ();
extern "C" void SynchronizedArrayListWrapper_IndexOf_m2939937472 ();
extern "C" void SynchronizedArrayListWrapper_IndexOf_m3214999213 ();
extern "C" void SynchronizedArrayListWrapper_Insert_m1479935893 ();
extern "C" void SynchronizedArrayListWrapper_InsertRange_m122501967 ();
extern "C" void SynchronizedArrayListWrapper_Remove_m1131132228 ();
extern "C" void SynchronizedArrayListWrapper_RemoveAt_m3605945430 ();
extern "C" void SynchronizedArrayListWrapper_CopyTo_m1089777810 ();
extern "C" void SynchronizedArrayListWrapper_CopyTo_m1685175620 ();
extern "C" void SynchronizedArrayListWrapper_CopyTo_m1755929818 ();
extern "C" void SynchronizedArrayListWrapper_GetEnumerator_m1244012135 ();
extern "C" void SynchronizedArrayListWrapper_AddRange_m3358444564 ();
extern "C" void SynchronizedArrayListWrapper_Clone_m1224772954 ();
extern "C" void SynchronizedArrayListWrapper_Sort_m1477226622 ();
extern "C" void SynchronizedArrayListWrapper_Sort_m1118104978 ();
extern "C" void SynchronizedArrayListWrapper_ToArray_m2041291651 ();
extern "C" void SynchronizedArrayListWrapper_ToArray_m4286441432 ();
extern "C" void BitArray__ctor_m1601208956 ();
extern "C" void BitArray__ctor_m2858832404 ();
extern "C" void BitArray_getByte_m2751132649 ();
extern "C" void BitArray_get_Count_m1638551981 ();
extern "C" void BitArray_get_Item_m2283229009 ();
extern "C" void BitArray_set_Item_m2424022219 ();
extern "C" void BitArray_get_Length_m3419123068 ();
extern "C" void BitArray_get_SyncRoot_m3960743097 ();
extern "C" void BitArray_Clone_m1167667406 ();
extern "C" void BitArray_CopyTo_m862538104 ();
extern "C" void BitArray_Get_m669637547 ();
extern "C" void BitArray_Set_m936522109 ();
extern "C" void BitArray_GetEnumerator_m1592407388 ();
extern "C" void BitArrayEnumerator__ctor_m4116091493 ();
extern "C" void BitArrayEnumerator_Clone_m3844344686 ();
extern "C" void BitArrayEnumerator_get_Current_m1633960344 ();
extern "C" void BitArrayEnumerator_MoveNext_m298769991 ();
extern "C" void BitArrayEnumerator_Reset_m2396240084 ();
extern "C" void BitArrayEnumerator_checkVersion_m3010158598 ();
extern "C" void CaseInsensitiveComparer__ctor_m1369638598 ();
extern "C" void CaseInsensitiveComparer__ctor_m2570295078 ();
extern "C" void CaseInsensitiveComparer__cctor_m2724493792 ();
extern "C" void CaseInsensitiveComparer_get_DefaultInvariant_m2201964174 ();
extern "C" void CaseInsensitiveComparer_Compare_m1348555498 ();
extern "C" void CaseInsensitiveHashCodeProvider__ctor_m3591252511 ();
extern "C" void CaseInsensitiveHashCodeProvider__ctor_m256093159 ();
extern "C" void CaseInsensitiveHashCodeProvider__cctor_m2915843407 ();
extern "C" void CaseInsensitiveHashCodeProvider_AreEqual_m2144889497 ();
extern "C" void CaseInsensitiveHashCodeProvider_AreEqual_m1004254695 ();
extern "C" void CaseInsensitiveHashCodeProvider_get_DefaultInvariant_m3219219398 ();
extern "C" void CaseInsensitiveHashCodeProvider_GetHashCode_m2875073 ();
extern "C" void CollectionBase__ctor_m3253555188 ();
extern "C" void CollectionBase_System_Collections_ICollection_CopyTo_m2572720456 ();
extern "C" void CollectionBase_System_Collections_ICollection_get_SyncRoot_m2606609939 ();
extern "C" void CollectionBase_System_Collections_IList_Add_m134587319 ();
extern "C" void CollectionBase_System_Collections_IList_Contains_m675999865 ();
extern "C" void CollectionBase_System_Collections_IList_IndexOf_m3097638141 ();
extern "C" void CollectionBase_System_Collections_IList_Insert_m583839462 ();
extern "C" void CollectionBase_System_Collections_IList_Remove_m3085003941 ();
extern "C" void CollectionBase_System_Collections_IList_get_Item_m1763001231 ();
extern "C" void CollectionBase_System_Collections_IList_set_Item_m1574988689 ();
extern "C" void CollectionBase_get_Count_m13611932 ();
extern "C" void CollectionBase_GetEnumerator_m2711342643 ();
extern "C" void CollectionBase_Clear_m3499885467 ();
extern "C" void CollectionBase_RemoveAt_m860391196 ();
extern "C" void CollectionBase_get_InnerList_m428229921 ();
extern "C" void CollectionBase_get_List_m2159943927 ();
extern "C" void CollectionBase_OnClear_m2583786747 ();
extern "C" void CollectionBase_OnClearComplete_m3700619957 ();
extern "C" void CollectionBase_OnInsert_m1665034841 ();
extern "C" void CollectionBase_OnInsertComplete_m1401033198 ();
extern "C" void CollectionBase_OnRemove_m345705727 ();
extern "C" void CollectionBase_OnRemoveComplete_m3039635368 ();
extern "C" void CollectionBase_OnSet_m2841116004 ();
extern "C" void CollectionBase_OnSetComplete_m3196194211 ();
extern "C" void CollectionBase_OnValidate_m1340864948 ();
extern "C" void Comparer__ctor_m1347258397 ();
extern "C" void Comparer__ctor_m2319474396 ();
extern "C" void Comparer__cctor_m3696268315 ();
extern "C" void Comparer_Compare_m736952830 ();
extern "C" void DictionaryEntry__ctor_m3773202887_AdjustorThunk ();
extern "C" void DictionaryEntry_get_Key_m3364857942_AdjustorThunk ();
extern "C" void DictionaryEntry_get_Value_m3120388264_AdjustorThunk ();
extern "C" void KeyNotFoundException__ctor_m3628075698 ();
extern "C" void KeyNotFoundException__ctor_m2109244851 ();
extern "C" void KeyNotFoundException__ctor_m1802636253 ();
extern "C" void Hashtable__ctor_m1143234103 ();
extern "C" void Hashtable__ctor_m3292572634 ();
extern "C" void Hashtable__ctor_m2612894583 ();
extern "C" void Hashtable__ctor_m1589851152 ();
extern "C" void Hashtable__ctor_m1818784074 ();
extern "C" void Hashtable__ctor_m1202039326 ();
extern "C" void Hashtable__ctor_m132973949 ();
extern "C" void Hashtable__ctor_m347947491 ();
extern "C" void Hashtable__ctor_m1435436894 ();
extern "C" void Hashtable__ctor_m188879612 ();
extern "C" void Hashtable__ctor_m505401515 ();
extern "C" void Hashtable__ctor_m591103269 ();
extern "C" void Hashtable__cctor_m2552068052 ();
extern "C" void Hashtable_System_Collections_IEnumerable_GetEnumerator_m1972518728 ();
extern "C" void Hashtable_set_comparer_m2837928355 ();
extern "C" void Hashtable_set_hcp_m2961569771 ();
extern "C" void Hashtable_get_Count_m78354934 ();
extern "C" void Hashtable_get_SyncRoot_m2040127349 ();
extern "C" void Hashtable_get_Keys_m2064563248 ();
extern "C" void Hashtable_get_Values_m2272493742 ();
extern "C" void Hashtable_get_Item_m235193985 ();
extern "C" void Hashtable_set_Item_m1884260023 ();
extern "C" void Hashtable_CopyTo_m4282595219 ();
extern "C" void Hashtable_Add_m3165682791 ();
extern "C" void Hashtable_Clear_m2559356064 ();
extern "C" void Hashtable_Contains_m1437756016 ();
extern "C" void Hashtable_GetEnumerator_m1856328481 ();
extern "C" void Hashtable_Remove_m3981333743 ();
extern "C" void Hashtable_ContainsKey_m3984503069 ();
extern "C" void Hashtable_Clone_m4285620339 ();
extern "C" void Hashtable_OnDeserialization_m3313245466 ();
extern "C" void Hashtable_Synchronized_m2127876118 ();
extern "C" void Hashtable_GetHash_m2417611000 ();
extern "C" void Hashtable_KeyEquals_m94467689 ();
extern "C" void Hashtable_AdjustThreshold_m4104561679 ();
extern "C" void Hashtable_SetTable_m3625255072 ();
extern "C" void Hashtable_Find_m2068965260 ();
extern "C" void Hashtable_Rehash_m3009677313 ();
extern "C" void Hashtable_PutImpl_m2943660476 ();
extern "C" void Hashtable_CopyToArray_m3105490612 ();
extern "C" void Hashtable_TestPrime_m706506726 ();
extern "C" void Hashtable_CalcPrime_m851781627 ();
extern "C" void Hashtable_ToPrime_m3167575262 ();
extern "C" void Enumerator__ctor_m1432194560 ();
extern "C" void Enumerator__cctor_m4285618846 ();
extern "C" void Enumerator_FailFast_m4030123936 ();
extern "C" void Enumerator_Reset_m2495282333 ();
extern "C" void Enumerator_MoveNext_m2287774590 ();
extern "C" void Enumerator_get_Entry_m1071522276 ();
extern "C" void Enumerator_get_Key_m1593951509 ();
extern "C" void Enumerator_get_Value_m3434206522 ();
extern "C" void Enumerator_get_Current_m225470999 ();
extern "C" void HashKeys__ctor_m3940316379 ();
extern "C" void HashKeys_get_Count_m3449693751 ();
extern "C" void HashKeys_get_SyncRoot_m3523836037 ();
extern "C" void HashKeys_CopyTo_m2920154361 ();
extern "C" void HashKeys_GetEnumerator_m4177351206 ();
extern "C" void HashValues__ctor_m1391954076 ();
extern "C" void HashValues_get_Count_m3804746689 ();
extern "C" void HashValues_get_SyncRoot_m950352266 ();
extern "C" void HashValues_CopyTo_m3240300716 ();
extern "C" void HashValues_GetEnumerator_m2443659244 ();
extern "C" void KeyMarker__ctor_m36625516 ();
extern "C" void KeyMarker__cctor_m3640669474 ();
extern "C" void SyncHashtable__ctor_m961400313 ();
extern "C" void SyncHashtable__ctor_m2131022655 ();
extern "C" void SyncHashtable_System_Collections_IEnumerable_GetEnumerator_m3586725612 ();
extern "C" void SyncHashtable_get_Count_m3568720318 ();
extern "C" void SyncHashtable_get_SyncRoot_m538137672 ();
extern "C" void SyncHashtable_get_Keys_m991828786 ();
extern "C" void SyncHashtable_get_Values_m3195252010 ();
extern "C" void SyncHashtable_get_Item_m4185623069 ();
extern "C" void SyncHashtable_set_Item_m2480293295 ();
extern "C" void SyncHashtable_CopyTo_m1791448066 ();
extern "C" void SyncHashtable_Add_m1878706264 ();
extern "C" void SyncHashtable_Clear_m1362680156 ();
extern "C" void SyncHashtable_Contains_m1954982520 ();
extern "C" void SyncHashtable_GetEnumerator_m573170284 ();
extern "C" void SyncHashtable_Remove_m2169965663 ();
extern "C" void SyncHashtable_ContainsKey_m3862269555 ();
extern "C" void SyncHashtable_Clone_m1436799670 ();
extern "C" void SortedList__ctor_m3601209121 ();
extern "C" void SortedList__ctor_m2023168972 ();
extern "C" void SortedList__ctor_m2742363376 ();
extern "C" void SortedList__ctor_m2926868609 ();
extern "C" void SortedList__cctor_m2457773694 ();
extern "C" void SortedList_System_Collections_IEnumerable_GetEnumerator_m2253009252 ();
extern "C" void SortedList_get_Count_m2004715360 ();
extern "C" void SortedList_get_SyncRoot_m4186691777 ();
extern "C" void SortedList_get_IsFixedSize_m4019735192 ();
extern "C" void SortedList_get_IsReadOnly_m2947941470 ();
extern "C" void SortedList_get_Item_m2537724296 ();
extern "C" void SortedList_set_Item_m808115396 ();
extern "C" void SortedList_get_Capacity_m3176686970 ();
extern "C" void SortedList_set_Capacity_m823537961 ();
extern "C" void SortedList_Add_m2593684945 ();
extern "C" void SortedList_Contains_m1266529256 ();
extern "C" void SortedList_GetEnumerator_m191943740 ();
extern "C" void SortedList_Remove_m3721013730 ();
extern "C" void SortedList_CopyTo_m1731250790 ();
extern "C" void SortedList_Clone_m761330528 ();
extern "C" void SortedList_RemoveAt_m1338265277 ();
extern "C" void SortedList_IndexOfKey_m17048843 ();
extern "C" void SortedList_ContainsKey_m1346463948 ();
extern "C" void SortedList_GetByIndex_m2411263875 ();
extern "C" void SortedList_EnsureCapacity_m2515702407 ();
extern "C" void SortedList_PutImpl_m340229810 ();
extern "C" void SortedList_GetImpl_m4211164780 ();
extern "C" void SortedList_InitTable_m3816943775 ();
extern "C" void SortedList_Find_m3346310779 ();
extern "C" void Enumerator__ctor_m56802941 ();
extern "C" void Enumerator__cctor_m3687757050 ();
extern "C" void Enumerator_Reset_m956450938 ();
extern "C" void Enumerator_MoveNext_m630822851 ();
extern "C" void Enumerator_get_Entry_m4266623009 ();
extern "C" void Enumerator_get_Key_m4004039148 ();
extern "C" void Enumerator_get_Value_m1871689937 ();
extern "C" void Enumerator_get_Current_m2385427688 ();
extern "C" void Enumerator_Clone_m1679634913 ();
extern "C" void Stack__ctor_m4014281920 ();
extern "C" void Stack__ctor_m682860280 ();
extern "C" void Stack__ctor_m2632145311 ();
extern "C" void Stack_Resize_m2616541472 ();
extern "C" void Stack_get_Count_m3521085062 ();
extern "C" void Stack_get_SyncRoot_m332712568 ();
extern "C" void Stack_Clear_m1096186364 ();
extern "C" void Stack_Clone_m3820343942 ();
extern "C" void Stack_CopyTo_m204066610 ();
extern "C" void Stack_GetEnumerator_m53775305 ();
extern "C" void Stack_Peek_m1198535650 ();
extern "C" void Stack_Pop_m4067695422 ();
extern "C" void Stack_Push_m3591675239 ();
extern "C" void Enumerator__ctor_m762187280 ();
extern "C" void Enumerator_Clone_m1465212645 ();
extern "C" void Enumerator_get_Current_m447003484 ();
extern "C" void Enumerator_MoveNext_m552838171 ();
extern "C" void Enumerator_Reset_m2987661950 ();
extern "C" void Console__cctor_m4079772084 ();
extern "C" void Console_SetEncodings_m1077977921 ();
extern "C" void Console_get_Error_m1762483787 ();
extern "C" void Console_Open_m3628545284 ();
extern "C" void Console_OpenStandardError_m3305110800 ();
extern "C" void Console_OpenStandardInput_m4006248948 ();
extern "C" void Console_OpenStandardOutput_m1736171620 ();
extern "C" void Console_SetOut_m1821852987 ();
extern "C" void ContextBoundObject__ctor_m434475057 ();
extern "C" void Convert__cctor_m1126066426 ();
extern "C" void Convert_InternalFromBase64String_m2973573730 ();
extern "C" void Convert_FromBase64String_m3889535293 ();
extern "C" void Convert_ToBase64String_m3960133172 ();
extern "C" void Convert_ToBase64String_m1698956971 ();
extern "C" void Convert_ToBoolean_m181196939 ();
extern "C" void Convert_ToBoolean_m3304463844 ();
extern "C" void Convert_ToBoolean_m2066372582 ();
extern "C" void Convert_ToBoolean_m1092989533 ();
extern "C" void Convert_ToBoolean_m3291433592 ();
extern "C" void Convert_ToBoolean_m3406248544 ();
extern "C" void Convert_ToBoolean_m3849780071 ();
extern "C" void Convert_ToBoolean_m815578138 ();
extern "C" void Convert_ToBoolean_m3560274087 ();
extern "C" void Convert_ToBoolean_m1821365203 ();
extern "C" void Convert_ToBoolean_m3541395848 ();
extern "C" void Convert_ToBoolean_m3686882876 ();
extern "C" void Convert_ToBoolean_m2812258067 ();
extern "C" void Convert_ToBoolean_m3024948542 ();
extern "C" void Convert_ToByte_m1711335136 ();
extern "C" void Convert_ToByte_m1359084456 ();
extern "C" void Convert_ToByte_m4157197134 ();
extern "C" void Convert_ToByte_m3676785831 ();
extern "C" void Convert_ToByte_m866306097 ();
extern "C" void Convert_ToByte_m1522154692 ();
extern "C" void Convert_ToByte_m3415664427 ();
extern "C" void Convert_ToByte_m1998596307 ();
extern "C" void Convert_ToByte_m3660634296 ();
extern "C" void Convert_ToByte_m3362055950 ();
extern "C" void Convert_ToByte_m120001947 ();
extern "C" void Convert_ToByte_m627183151 ();
extern "C" void Convert_ToByte_m1655664624 ();
extern "C" void Convert_ToByte_m2693208695 ();
extern "C" void Convert_ToByte_m301731779 ();
extern "C" void Convert_ToChar_m2909185475 ();
extern "C" void Convert_ToChar_m1363087353 ();
extern "C" void Convert_ToChar_m3767754668 ();
extern "C" void Convert_ToChar_m2197668031 ();
extern "C" void Convert_ToChar_m1651289017 ();
extern "C" void Convert_ToChar_m769872090 ();
extern "C" void Convert_ToChar_m1586478606 ();
extern "C" void Convert_ToChar_m263958929 ();
extern "C" void Convert_ToChar_m2065393292 ();
extern "C" void Convert_ToChar_m1385558794 ();
extern "C" void Convert_ToChar_m3616271797 ();
extern "C" void Convert_ToDateTime_m3346118083 ();
extern "C" void Convert_ToDateTime_m4192135626 ();
extern "C" void Convert_ToDateTime_m4188556608 ();
extern "C" void Convert_ToDateTime_m355355565 ();
extern "C" void Convert_ToDateTime_m3877313006 ();
extern "C" void Convert_ToDateTime_m2904116687 ();
extern "C" void Convert_ToDateTime_m1240677581 ();
extern "C" void Convert_ToDateTime_m2178216354 ();
extern "C" void Convert_ToDateTime_m1691902540 ();
extern "C" void Convert_ToDateTime_m1899241014 ();
extern "C" void Convert_ToDecimal_m1907780385 ();
extern "C" void Convert_ToDecimal_m2813599902 ();
extern "C" void Convert_ToDecimal_m1028642325 ();
extern "C" void Convert_ToDecimal_m3104762561 ();
extern "C" void Convert_ToDecimal_m3602659554 ();
extern "C" void Convert_ToDecimal_m759259367 ();
extern "C" void Convert_ToDecimal_m3435225172 ();
extern "C" void Convert_ToDecimal_m2105281924 ();
extern "C" void Convert_ToDecimal_m4051675827 ();
extern "C" void Convert_ToDecimal_m4173734781 ();
extern "C" void Convert_ToDecimal_m4051859286 ();
extern "C" void Convert_ToDecimal_m1174043729 ();
extern "C" void Convert_ToDecimal_m1566524000 ();
extern "C" void Convert_ToDecimal_m1342054916 ();
extern "C" void Convert_ToDouble_m1759377896 ();
extern "C" void Convert_ToDouble_m676071452 ();
extern "C" void Convert_ToDouble_m359663363 ();
extern "C" void Convert_ToDouble_m206312034 ();
extern "C" void Convert_ToDouble_m1940577304 ();
extern "C" void Convert_ToDouble_m3071019606 ();
extern "C" void Convert_ToDouble_m358816045 ();
extern "C" void Convert_ToDouble_m3582443749 ();
extern "C" void Convert_ToDouble_m3605398720 ();
extern "C" void Convert_ToDouble_m964392636 ();
extern "C" void Convert_ToDouble_m3414375849 ();
extern "C" void Convert_ToDouble_m1338516771 ();
extern "C" void Convert_ToDouble_m1798060040 ();
extern "C" void Convert_ToDouble_m1957959055 ();
extern "C" void Convert_ToInt16_m3614160131 ();
extern "C" void Convert_ToInt16_m2309364880 ();
extern "C" void Convert_ToInt16_m3902136059 ();
extern "C" void Convert_ToInt16_m1630070127 ();
extern "C" void Convert_ToInt16_m3029401211 ();
extern "C" void Convert_ToInt16_m2303614416 ();
extern "C" void Convert_ToInt16_m1702084948 ();
extern "C" void Convert_ToInt16_m4256038495 ();
extern "C" void Convert_ToInt16_m1268881642 ();
extern "C" void Convert_ToInt16_m2801254818 ();
extern "C" void Convert_ToInt16_m3017105195 ();
extern "C" void Convert_ToInt16_m4195031634 ();
extern "C" void Convert_ToInt16_m3052677612 ();
extern "C" void Convert_ToInt16_m4149411228 ();
extern "C" void Convert_ToInt16_m1678654658 ();
extern "C" void Convert_ToInt16_m1908371115 ();
extern "C" void Convert_ToInt32_m1556403369 ();
extern "C" void Convert_ToInt32_m732230018 ();
extern "C" void Convert_ToInt32_m2208179825 ();
extern "C" void Convert_ToInt32_m1333497284 ();
extern "C" void Convert_ToInt32_m4262555422 ();
extern "C" void Convert_ToInt32_m1535806496 ();
extern "C" void Convert_ToInt32_m4078444369 ();
extern "C" void Convert_ToInt32_m1206626159 ();
extern "C" void Convert_ToInt32_m598318614 ();
extern "C" void Convert_ToInt32_m3363061093 ();
extern "C" void Convert_ToInt32_m2420320894 ();
extern "C" void Convert_ToInt32_m575912531 ();
extern "C" void Convert_ToInt32_m349566814 ();
extern "C" void Convert_ToInt32_m1285495894 ();
extern "C" void Convert_ToInt32_m189054182 ();
extern "C" void Convert_ToInt64_m3812963483 ();
extern "C" void Convert_ToInt64_m3641628104 ();
extern "C" void Convert_ToInt64_m2621974401 ();
extern "C" void Convert_ToInt64_m2143779060 ();
extern "C" void Convert_ToInt64_m3660419282 ();
extern "C" void Convert_ToInt64_m2984820389 ();
extern "C" void Convert_ToInt64_m3216407986 ();
extern "C" void Convert_ToInt64_m2741282783 ();
extern "C" void Convert_ToInt64_m1063954313 ();
extern "C" void Convert_ToInt64_m3844943708 ();
extern "C" void Convert_ToInt64_m1328417215 ();
extern "C" void Convert_ToInt64_m2740684590 ();
extern "C" void Convert_ToInt64_m1102749412 ();
extern "C" void Convert_ToInt64_m781390160 ();
extern "C" void Convert_ToInt64_m2202161021 ();
extern "C" void Convert_ToInt64_m2210211785 ();
extern "C" void Convert_ToInt64_m2041309988 ();
extern "C" void Convert_ToSByte_m863025055 ();
extern "C" void Convert_ToSByte_m1485386302 ();
extern "C" void Convert_ToSByte_m1209019644 ();
extern "C" void Convert_ToSByte_m1294608859 ();
extern "C" void Convert_ToSByte_m966005355 ();
extern "C" void Convert_ToSByte_m2780065447 ();
extern "C" void Convert_ToSByte_m3186677260 ();
extern "C" void Convert_ToSByte_m763994626 ();
extern "C" void Convert_ToSByte_m3414187568 ();
extern "C" void Convert_ToSByte_m1378762269 ();
extern "C" void Convert_ToSByte_m1804217813 ();
extern "C" void Convert_ToSByte_m3354070798 ();
extern "C" void Convert_ToSByte_m2415307335 ();
extern "C" void Convert_ToSByte_m1950141290 ();
extern "C" void Convert_ToSingle_m932543265 ();
extern "C" void Convert_ToSingle_m3207606949 ();
extern "C" void Convert_ToSingle_m4290228487 ();
extern "C" void Convert_ToSingle_m188965971 ();
extern "C" void Convert_ToSingle_m2332656787 ();
extern "C" void Convert_ToSingle_m2789125164 ();
extern "C" void Convert_ToSingle_m259335902 ();
extern "C" void Convert_ToSingle_m1511457299 ();
extern "C" void Convert_ToSingle_m913009971 ();
extern "C" void Convert_ToSingle_m861086822 ();
extern "C" void Convert_ToSingle_m3480326864 ();
extern "C" void Convert_ToSingle_m2260413742 ();
extern "C" void Convert_ToSingle_m3787160430 ();
extern "C" void Convert_ToSingle_m2229731220 ();
extern "C" void Convert_ToString_m94911272 ();
extern "C" void Convert_ToString_m1537572958 ();
extern "C" void Convert_ToUInt16_m3506724663 ();
extern "C" void Convert_ToUInt16_m115274073 ();
extern "C" void Convert_ToUInt16_m3490324414 ();
extern "C" void Convert_ToUInt16_m3912085078 ();
extern "C" void Convert_ToUInt16_m207185530 ();
extern "C" void Convert_ToUInt16_m722491632 ();
extern "C" void Convert_ToUInt16_m1222063852 ();
extern "C" void Convert_ToUInt16_m4146807859 ();
extern "C" void Convert_ToUInt16_m1828753276 ();
extern "C" void Convert_ToUInt16_m2948714162 ();
extern "C" void Convert_ToUInt16_m2011096748 ();
extern "C" void Convert_ToUInt16_m418187698 ();
extern "C" void Convert_ToUInt16_m3930851433 ();
extern "C" void Convert_ToUInt16_m996906177 ();
extern "C" void Convert_ToUInt32_m4249819190 ();
extern "C" void Convert_ToUInt32_m823261086 ();
extern "C" void Convert_ToUInt32_m3279699603 ();
extern "C" void Convert_ToUInt32_m837934666 ();
extern "C" void Convert_ToUInt32_m3154368742 ();
extern "C" void Convert_ToUInt32_m3499511866 ();
extern "C" void Convert_ToUInt32_m3902982566 ();
extern "C" void Convert_ToUInt32_m1630022459 ();
extern "C" void Convert_ToUInt32_m2888961780 ();
extern "C" void Convert_ToUInt32_m3875914063 ();
extern "C" void Convert_ToUInt32_m2993243305 ();
extern "C" void Convert_ToUInt32_m2675516930 ();
extern "C" void Convert_ToUInt32_m3219512886 ();
extern "C" void Convert_ToUInt32_m731182432 ();
extern "C" void Convert_ToUInt64_m2793460560 ();
extern "C" void Convert_ToUInt64_m3441470682 ();
extern "C" void Convert_ToUInt64_m651291461 ();
extern "C" void Convert_ToUInt64_m1016581210 ();
extern "C" void Convert_ToUInt64_m4062218541 ();
extern "C" void Convert_ToUInt64_m1587419655 ();
extern "C" void Convert_ToUInt64_m1283785253 ();
extern "C" void Convert_ToUInt64_m3177931015 ();
extern "C" void Convert_ToUInt64_m3453577592 ();
extern "C" void Convert_ToUInt64_m371719104 ();
extern "C" void Convert_ToUInt64_m1691859135 ();
extern "C" void Convert_ToUInt64_m3212089508 ();
extern "C" void Convert_ToUInt64_m1360931664 ();
extern "C" void Convert_ToUInt64_m2801452660 ();
extern "C" void Convert_ToUInt64_m3389700845 ();
extern "C" void Convert_ChangeType_m3958886575 ();
extern "C" void Convert_ToType_m2692801367 ();
extern "C" void CultureAwareComparer__ctor_m2927916088 ();
extern "C" void CultureAwareComparer_Compare_m2414611160 ();
extern "C" void CultureAwareComparer_Equals_m1823500299 ();
extern "C" void CultureAwareComparer_GetHashCode_m4059648712 ();
extern "C" void CurrentSystemTimeZone__ctor_m2856852514 ();
extern "C" void CurrentSystemTimeZone__ctor_m3263608395 ();
extern "C" void CurrentSystemTimeZone_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m2860688338 ();
extern "C" void CurrentSystemTimeZone_GetTimeZoneData_m907897977 ();
extern "C" void CurrentSystemTimeZone_GetDaylightChanges_m3350100744 ();
extern "C" void CurrentSystemTimeZone_GetUtcOffset_m1258737164 ();
extern "C" void CurrentSystemTimeZone_OnDeserialization_m247697366 ();
extern "C" void CurrentSystemTimeZone_GetDaylightTimeFromData_m3659587845 ();
extern "C" void DateTime__ctor_m1162146585_AdjustorThunk ();
extern "C" void DateTime__ctor_m1469143154_AdjustorThunk ();
extern "C" void DateTime__ctor_m112945363_AdjustorThunk ();
extern "C" void DateTime__ctor_m1943919131_AdjustorThunk ();
extern "C" void DateTime__ctor_m3733363217_AdjustorThunk ();
extern "C" void DateTime__ctor_m128199834_AdjustorThunk ();
extern "C" void DateTime__ctor_m3814649173_AdjustorThunk ();
extern "C" void DateTime__cctor_m3687287262 ();
extern "C" void DateTime_System_IConvertible_ToBoolean_m2129434697_AdjustorThunk ();
extern "C" void DateTime_System_IConvertible_ToByte_m349242609_AdjustorThunk ();
extern "C" void DateTime_System_IConvertible_ToChar_m649605060_AdjustorThunk ();
extern "C" void DateTime_System_IConvertible_ToDateTime_m807082907_AdjustorThunk ();
extern "C" void DateTime_System_IConvertible_ToDecimal_m2955402839_AdjustorThunk ();
extern "C" void DateTime_System_IConvertible_ToDouble_m2363360721_AdjustorThunk ();
extern "C" void DateTime_System_IConvertible_ToInt16_m1926912326_AdjustorThunk ();
extern "C" void DateTime_System_IConvertible_ToInt32_m2103877964_AdjustorThunk ();
extern "C" void DateTime_System_IConvertible_ToInt64_m4060354194_AdjustorThunk ();
extern "C" void DateTime_System_IConvertible_ToSByte_m1316244191_AdjustorThunk ();
extern "C" void DateTime_System_IConvertible_ToSingle_m2678801163_AdjustorThunk ();
extern "C" void DateTime_System_IConvertible_ToType_m250355434_AdjustorThunk ();
extern "C" void DateTime_System_IConvertible_ToUInt16_m4275199668_AdjustorThunk ();
extern "C" void DateTime_System_IConvertible_ToUInt32_m3504556377_AdjustorThunk ();
extern "C" void DateTime_System_IConvertible_ToUInt64_m3729346730_AdjustorThunk ();
extern "C" void DateTime_AbsoluteDays_m2581663874 ();
extern "C" void DateTime_FromTicks_m612602014_AdjustorThunk ();
extern "C" void DateTime_get_Month_m3602563062_AdjustorThunk ();
extern "C" void DateTime_get_Day_m3723482842_AdjustorThunk ();
extern "C" void DateTime_get_DayOfWeek_m552950095_AdjustorThunk ();
extern "C" void DateTime_get_Hour_m3279703729_AdjustorThunk ();
extern "C" void DateTime_get_Minute_m2214880909_AdjustorThunk ();
extern "C" void DateTime_get_Second_m359294707_AdjustorThunk ();
extern "C" void DateTime_GetTimeMonotonic_m3035286822 ();
extern "C" void DateTime_GetNow_m1798973790 ();
extern "C" void DateTime_get_Now_m3197730460 ();
extern "C" void DateTime_get_Ticks_m3634951895_AdjustorThunk ();
extern "C" void DateTime_get_Today_m3149731286 ();
extern "C" void DateTime_get_UtcNow_m1166149790 ();
extern "C" void DateTime_get_Year_m2465579539_AdjustorThunk ();
extern "C" void DateTime_get_Kind_m2353497618_AdjustorThunk ();
extern "C" void DateTime_Add_m82617556_AdjustorThunk ();
extern "C" void DateTime_AddTicks_m3326838593_AdjustorThunk ();
extern "C" void DateTime_AddMilliseconds_m2259736413_AdjustorThunk ();
extern "C" void DateTime_AddSeconds_m802575560_AdjustorThunk ();
extern "C" void DateTime_Compare_m3946132637 ();
extern "C" void DateTime_CompareTo_m58681863_AdjustorThunk ();
extern "C" void DateTime_CompareTo_m2695414402_AdjustorThunk ();
extern "C" void DateTime_Equals_m2586536838_AdjustorThunk ();
extern "C" void DateTime_FromBinary_m3052269187 ();
extern "C" void DateTime_SpecifyKind_m565564135 ();
extern "C" void DateTime_DaysInMonth_m3804445593 ();
extern "C" void DateTime_Equals_m2336408426_AdjustorThunk ();
extern "C" void DateTime_CheckDateTimeKind_m906499247_AdjustorThunk ();
extern "C" void DateTime_GetHashCode_m1398597643_AdjustorThunk ();
extern "C" void DateTime_IsLeapYear_m3908824970 ();
extern "C" void DateTime_Parse_m1939827158 ();
extern "C" void DateTime_Parse_m3813128948 ();
extern "C" void DateTime_CoreParse_m337968780 ();
extern "C" void DateTime_YearMonthDayFormats_m249964372 ();
extern "C" void DateTime__ParseNumber_m4213919703 ();
extern "C" void DateTime__ParseEnum_m2925373698 ();
extern "C" void DateTime__ParseString_m4087287987 ();
extern "C" void DateTime__ParseAmPm_m3930102169 ();
extern "C" void DateTime__ParseTimeSeparator_m3385727945 ();
extern "C" void DateTime__ParseDateSeparator_m4159984309 ();
extern "C" void DateTime_IsLetter_m3492201953 ();
extern "C" void DateTime__DoParse_m3124502042 ();
extern "C" void DateTime_ParseExact_m567683756 ();
extern "C" void DateTime_ParseExact_m1292664029 ();
extern "C" void DateTime_CheckStyle_m1194998724 ();
extern "C" void DateTime_ParseExact_m949581326 ();
extern "C" void DateTime_Subtract_m355628981_AdjustorThunk ();
extern "C" void DateTime_ToString_m2872442757_AdjustorThunk ();
extern "C" void DateTime_ToString_m3415865025_AdjustorThunk ();
extern "C" void DateTime_ToString_m3913825546_AdjustorThunk ();
extern "C" void DateTime_ToLocalTime_m2237725792_AdjustorThunk ();
extern "C" void DateTime_ToUniversalTime_m688324665_AdjustorThunk ();
extern "C" void DateTime_op_Addition_m196538926 ();
extern "C" void DateTime_op_Equality_m307014225 ();
extern "C" void DateTime_op_GreaterThan_m3588908669 ();
extern "C" void DateTime_op_GreaterThanOrEqual_m3282389234 ();
extern "C" void DateTime_op_Inequality_m1476518596 ();
extern "C" void DateTime_op_LessThan_m336891988 ();
extern "C" void DateTime_op_LessThanOrEqual_m3737117479 ();
extern "C" void DateTime_op_Subtraction_m2177242756 ();
extern "C" void DateTimeOffset__ctor_m2507223661_AdjustorThunk ();
extern "C" void DateTimeOffset__ctor_m23940287_AdjustorThunk ();
extern "C" void DateTimeOffset__ctor_m1680326190_AdjustorThunk ();
extern "C" void DateTimeOffset__ctor_m947657984_AdjustorThunk ();
extern "C" void DateTimeOffset__cctor_m3359959350 ();
extern "C" void DateTimeOffset_System_IComparable_CompareTo_m1808427407_AdjustorThunk ();
extern "C" void DateTimeOffset_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m2312432265_AdjustorThunk ();
extern "C" void DateTimeOffset_CompareTo_m1080840663_AdjustorThunk ();
extern "C" void DateTimeOffset_Equals_m4119379754_AdjustorThunk ();
extern "C" void DateTimeOffset_Equals_m3384866816_AdjustorThunk ();
extern "C" void DateTimeOffset_GetHashCode_m3032241378_AdjustorThunk ();
extern "C" void DateTimeOffset_ToString_m848849611_AdjustorThunk ();
extern "C" void DateTimeOffset_ToString_m3906072830_AdjustorThunk ();
extern "C" void DateTimeOffset_get_DateTime_m851552317_AdjustorThunk ();
extern "C" void DateTimeOffset_get_Offset_m1551860166_AdjustorThunk ();
extern "C" void DateTimeOffset_get_UtcDateTime_m2232550571_AdjustorThunk ();
extern "C" void DateTimeUtils_CountRepeat_m2243670922 ();
extern "C" void DateTimeUtils_ZeroPad_m1269987479 ();
extern "C" void DateTimeUtils_ParseQuotedString_m3545675658 ();
extern "C" void DateTimeUtils_GetStandardPattern_m3224060921 ();
extern "C" void DateTimeUtils_GetStandardPattern_m2938656100 ();
extern "C" void DateTimeUtils_ToString_m3124505984 ();
extern "C" void DateTimeUtils_ToString_m1521022922 ();
extern "C" void DBNull__ctor_m1217818204 ();
extern "C" void DBNull__ctor_m2976018176 ();
extern "C" void DBNull__cctor_m3995179348 ();
extern "C" void DBNull_System_IConvertible_ToBoolean_m3704708804 ();
extern "C" void DBNull_System_IConvertible_ToByte_m1888019139 ();
extern "C" void DBNull_System_IConvertible_ToChar_m559773690 ();
extern "C" void DBNull_System_IConvertible_ToDateTime_m1427000021 ();
extern "C" void DBNull_System_IConvertible_ToDecimal_m3607776546 ();
extern "C" void DBNull_System_IConvertible_ToDouble_m886967017 ();
extern "C" void DBNull_System_IConvertible_ToInt16_m2287362260 ();
extern "C" void DBNull_System_IConvertible_ToInt32_m1074309452 ();
extern "C" void DBNull_System_IConvertible_ToInt64_m2321239992 ();
extern "C" void DBNull_System_IConvertible_ToSByte_m2073493841 ();
extern "C" void DBNull_System_IConvertible_ToSingle_m368185513 ();
extern "C" void DBNull_System_IConvertible_ToType_m3468123543 ();
extern "C" void DBNull_System_IConvertible_ToUInt16_m298510550 ();
extern "C" void DBNull_System_IConvertible_ToUInt32_m1371873998 ();
extern "C" void DBNull_System_IConvertible_ToUInt64_m161428274 ();
extern "C" void DBNull_ToString_m1261601810 ();
extern "C" void DBNull_ToString_m3538632386 ();
extern "C" void Decimal__ctor_m2606258124_AdjustorThunk ();
extern "C" void Decimal__ctor_m2814078077_AdjustorThunk ();
extern "C" void Decimal__ctor_m2684736918_AdjustorThunk ();
extern "C" void Decimal__ctor_m3672480254_AdjustorThunk ();
extern "C" void Decimal__ctor_m700533170_AdjustorThunk ();
extern "C" void Decimal__ctor_m3564855869_AdjustorThunk ();
extern "C" void Decimal__ctor_m1824806831_AdjustorThunk ();
extern "C" void Decimal__cctor_m3550966988 ();
extern "C" void Decimal_System_IConvertible_ToType_m1469257424_AdjustorThunk ();
extern "C" void Decimal_System_IConvertible_ToBoolean_m3085764627_AdjustorThunk ();
extern "C" void Decimal_System_IConvertible_ToByte_m3781762660_AdjustorThunk ();
extern "C" void Decimal_System_IConvertible_ToChar_m2131880973_AdjustorThunk ();
extern "C" void Decimal_System_IConvertible_ToDateTime_m2162830290_AdjustorThunk ();
extern "C" void Decimal_System_IConvertible_ToDecimal_m3279028900_AdjustorThunk ();
extern "C" void Decimal_System_IConvertible_ToDouble_m2138187674_AdjustorThunk ();
extern "C" void Decimal_System_IConvertible_ToInt16_m2307666217_AdjustorThunk ();
extern "C" void Decimal_System_IConvertible_ToInt32_m4049826490_AdjustorThunk ();
extern "C" void Decimal_System_IConvertible_ToInt64_m4239752044_AdjustorThunk ();
extern "C" void Decimal_System_IConvertible_ToSByte_m3561412798_AdjustorThunk ();
extern "C" void Decimal_System_IConvertible_ToSingle_m222175769_AdjustorThunk ();
extern "C" void Decimal_System_IConvertible_ToUInt16_m11225590_AdjustorThunk ();
extern "C" void Decimal_System_IConvertible_ToUInt32_m4166707801_AdjustorThunk ();
extern "C" void Decimal_System_IConvertible_ToUInt64_m3027983897_AdjustorThunk ();
extern "C" void Decimal_GetBits_m2321028653 ();
extern "C" void Decimal_Add_m3493327949 ();
extern "C" void Decimal_Subtract_m3294185326 ();
extern "C" void Decimal_GetHashCode_m76089080_AdjustorThunk ();
extern "C" void Decimal_u64_m862346687 ();
extern "C" void Decimal_s64_m2192320321 ();
extern "C" void Decimal_Equals_m4165660341 ();
extern "C" void Decimal_Equals_m2099245511_AdjustorThunk ();
extern "C" void Decimal_IsZero_m648824807_AdjustorThunk ();
extern "C" void Decimal_Floor_m1365865268 ();
extern "C" void Decimal_Multiply_m2733971897 ();
extern "C" void Decimal_Divide_m3363200552 ();
extern "C" void Decimal_Compare_m4142387002 ();
extern "C" void Decimal_CompareTo_m2878181521_AdjustorThunk ();
extern "C" void Decimal_CompareTo_m2076266001_AdjustorThunk ();
extern "C" void Decimal_Equals_m422237635_AdjustorThunk ();
extern "C" void Decimal_Parse_m835038489 ();
extern "C" void Decimal_ThrowAtPos_m2666100311 ();
extern "C" void Decimal_ThrowInvalidExp_m3188247161 ();
extern "C" void Decimal_stripStyles_m4154272644 ();
extern "C" void Decimal_Parse_m942228482 ();
extern "C" void Decimal_PerformParse_m1737153701 ();
extern "C" void Decimal_ToString_m401229504_AdjustorThunk ();
extern "C" void Decimal_ToString_m3261532885_AdjustorThunk ();
extern "C" void Decimal_ToString_m522438381_AdjustorThunk ();
extern "C" void Decimal_decimal2UInt64_m3451140894 ();
extern "C" void Decimal_decimal2Int64_m2813801429 ();
extern "C" void Decimal_decimalIncr_m4211330891 ();
extern "C" void Decimal_string2decimal_m1826023876 ();
extern "C" void Decimal_decimalSetExponent_m835136832 ();
extern "C" void Decimal_decimal2double_m1799673680 ();
extern "C" void Decimal_decimalFloorAndTrunc_m102249924 ();
extern "C" void Decimal_decimalMult_m137374035 ();
extern "C" void Decimal_decimalDiv_m3364726341 ();
extern "C" void Decimal_decimalCompare_m1981548760 ();
extern "C" void Decimal_op_Increment_m3460311049 ();
extern "C" void Decimal_op_Subtraction_m2243172173 ();
extern "C" void Decimal_op_Multiply_m1703821494 ();
extern "C" void Decimal_op_Division_m3334649010 ();
extern "C" void Decimal_op_Explicit_m1152324651 ();
extern "C" void Decimal_op_Explicit_m2905558026 ();
extern "C" void Decimal_op_Explicit_m3588017808 ();
extern "C" void Decimal_op_Explicit_m1941964535 ();
extern "C" void Decimal_op_Explicit_m2997451292 ();
extern "C" void Decimal_op_Explicit_m801735887 ();
extern "C" void Decimal_op_Explicit_m2935360665 ();
extern "C" void Decimal_op_Explicit_m3225117811 ();
extern "C" void Decimal_op_Implicit_m1049928174 ();
extern "C" void Decimal_op_Implicit_m1059519657 ();
extern "C" void Decimal_op_Implicit_m3967362805 ();
extern "C" void Decimal_op_Implicit_m2347208944 ();
extern "C" void Decimal_op_Implicit_m4180805396 ();
extern "C" void Decimal_op_Implicit_m74114740 ();
extern "C" void Decimal_op_Implicit_m549347273 ();
extern "C" void Decimal_op_Implicit_m2277896146 ();
extern "C" void Decimal_op_Explicit_m1528918129 ();
extern "C" void Decimal_op_Explicit_m1768772750 ();
extern "C" void Decimal_op_Explicit_m2352650003 ();
extern "C" void Decimal_op_Explicit_m4218697563 ();
extern "C" void Decimal_op_Inequality_m2018481032 ();
extern "C" void Decimal_op_Equality_m85401498 ();
extern "C" void Decimal_op_GreaterThan_m2320203197 ();
extern "C" void Decimal_op_LessThan_m1574523506 ();
extern "C" void Delegate_get_Target_m1823718869 ();
extern "C" void Delegate_CreateDelegate_internal_m1250678387 ();
extern "C" void Delegate_SetMulticastInvoke_m3164144105 ();
extern "C" void Delegate_arg_type_match_m3105837143 ();
extern "C" void Delegate_return_type_match_m2444092117 ();
extern "C" void Delegate_CreateDelegate_m4207012919 ();
extern "C" void Delegate_CreateDelegate_m256783937 ();
extern "C" void Delegate_CreateDelegate_m1478302819 ();
extern "C" void Delegate_Clone_m2907474788 ();
extern "C" void Delegate_Equals_m1837108824 ();
extern "C" void Delegate_GetHashCode_m2081162287 ();
extern "C" void Delegate_GetInvocationList_m2200569124 ();
extern "C" void Delegate_Combine_m1239998138 ();
extern "C" void Delegate_CombineImpl_m1454252388 ();
extern "C" void Delegate_Remove_m1542158 ();
extern "C" void Delegate_RemoveImpl_m3444092335 ();
extern "C" void DebuggableAttribute__ctor_m3553871532 ();
extern "C" void DebuggerBrowsableAttribute__ctor_m1784875164 ();
extern "C" void DebuggerDisplayAttribute__ctor_m685030886 ();
extern "C" void DebuggerDisplayAttribute_set_Name_m2283484547 ();
extern "C" void DebuggerHiddenAttribute__ctor_m749702160 ();
extern "C" void DebuggerStepThroughAttribute__ctor_m1431182322 ();
extern "C" void DebuggerTypeProxyAttribute__ctor_m92548023 ();
extern "C" void StackFrame__ctor_m3278099530 ();
extern "C" void StackFrame__ctor_m2439904383 ();
extern "C" void StackFrame_get_frame_info_m1451964570 ();
extern "C" void StackFrame_GetFileLineNumber_m1458910347 ();
extern "C" void StackFrame_GetFileName_m3260703013 ();
extern "C" void StackFrame_GetSecureFileName_m1434779884 ();
extern "C" void StackFrame_GetILOffset_m2643308193 ();
extern "C" void StackFrame_GetMethod_m3382476531 ();
extern "C" void StackFrame_GetNativeOffset_m516534153 ();
extern "C" void StackFrame_GetInternalMethodName_m358119658 ();
extern "C" void StackFrame_ToString_m3234247435 ();
extern "C" void StackTrace__ctor_m690837410 ();
extern "C" void StackTrace__ctor_m1990137641 ();
extern "C" void StackTrace__ctor_m470329501 ();
extern "C" void StackTrace_init_frames_m554340018 ();
extern "C" void StackTrace_get_trace_m166904215 ();
extern "C" void StackTrace_get_FrameCount_m2559157395 ();
extern "C" void StackTrace_GetFrame_m1674626488 ();
extern "C" void StackTrace_ToString_m604863459 ();
extern "C" void DivideByZeroException__ctor_m2461769612 ();
extern "C" void DivideByZeroException__ctor_m172548445 ();
extern "C" void DllNotFoundException__ctor_m3972231607 ();
extern "C" void DllNotFoundException__ctor_m2177317137 ();
extern "C" void Double_System_IConvertible_ToType_m2118596693_AdjustorThunk ();
extern "C" void Double_System_IConvertible_ToBoolean_m3499772275_AdjustorThunk ();
extern "C" void Double_System_IConvertible_ToByte_m2323666643_AdjustorThunk ();
extern "C" void Double_System_IConvertible_ToChar_m1371902193_AdjustorThunk ();
extern "C" void Double_System_IConvertible_ToDateTime_m1838573054_AdjustorThunk ();
extern "C" void Double_System_IConvertible_ToDecimal_m3448152743_AdjustorThunk ();
extern "C" void Double_System_IConvertible_ToDouble_m2017853998_AdjustorThunk ();
extern "C" void Double_System_IConvertible_ToInt16_m3888266809_AdjustorThunk ();
extern "C" void Double_System_IConvertible_ToInt32_m902693213_AdjustorThunk ();
extern "C" void Double_System_IConvertible_ToInt64_m2173745845_AdjustorThunk ();
extern "C" void Double_System_IConvertible_ToSByte_m2692090898_AdjustorThunk ();
extern "C" void Double_System_IConvertible_ToSingle_m2757226682_AdjustorThunk ();
extern "C" void Double_System_IConvertible_ToUInt16_m3937706903_AdjustorThunk ();
extern "C" void Double_System_IConvertible_ToUInt32_m1948889007_AdjustorThunk ();
extern "C" void Double_System_IConvertible_ToUInt64_m2428224974_AdjustorThunk ();
extern "C" void Double_CompareTo_m4218094276_AdjustorThunk ();
extern "C" void Double_Equals_m1930767410_AdjustorThunk ();
extern "C" void Double_CompareTo_m263060497_AdjustorThunk ();
extern "C" void Double_Equals_m3607726724_AdjustorThunk ();
extern "C" void Double_GetHashCode_m2650551438_AdjustorThunk ();
extern "C" void Double_IsInfinity_m4057497359 ();
extern "C" void Double_IsNaN_m2498995017 ();
extern "C" void Double_IsNegativeInfinity_m2647344334 ();
extern "C" void Double_IsPositiveInfinity_m2293949402 ();
extern "C" void Double_Parse_m1874640025 ();
extern "C" void Double_Parse_m3309721781 ();
extern "C" void Double_Parse_m3093230639 ();
extern "C" void Double_Parse_m3090470462 ();
extern "C" void Double_TryParseStringConstant_m3224623453 ();
extern "C" void Double_ParseImpl_m2682713328 ();
extern "C" void Double_ToString_m3221644217_AdjustorThunk ();
extern "C" void Double_ToString_m1578634236_AdjustorThunk ();
extern "C" void Double_ToString_m1688271985_AdjustorThunk ();
extern "C" void EntryPointNotFoundException__ctor_m387630107 ();
extern "C" void EntryPointNotFoundException__ctor_m2290852730 ();
extern "C" void Enum__ctor_m3337276159 ();
extern "C" void Enum__cctor_m2877062492 ();
extern "C" void Enum_System_IConvertible_ToBoolean_m3975428596 ();
extern "C" void Enum_System_IConvertible_ToByte_m1457733691 ();
extern "C" void Enum_System_IConvertible_ToChar_m3072918045 ();
extern "C" void Enum_System_IConvertible_ToDateTime_m1545541770 ();
extern "C" void Enum_System_IConvertible_ToDecimal_m843595796 ();
extern "C" void Enum_System_IConvertible_ToDouble_m3990924730 ();
extern "C" void Enum_System_IConvertible_ToInt16_m2444520512 ();
extern "C" void Enum_System_IConvertible_ToInt32_m3249873431 ();
extern "C" void Enum_System_IConvertible_ToInt64_m3454953350 ();
extern "C" void Enum_System_IConvertible_ToSByte_m1230613013 ();
extern "C" void Enum_System_IConvertible_ToSingle_m1250279 ();
extern "C" void Enum_System_IConvertible_ToType_m841565390 ();
extern "C" void Enum_System_IConvertible_ToUInt16_m684863254 ();
extern "C" void Enum_System_IConvertible_ToUInt32_m37109982 ();
extern "C" void Enum_System_IConvertible_ToUInt64_m409681183 ();
extern "C" void Enum_GetTypeCode_m3885836212 ();
extern "C" void Enum_get_value_m4243904550 ();
extern "C" void Enum_get_Value_m23588034 ();
extern "C" void Enum_FindPosition_m1790631618 ();
extern "C" void Enum_GetName_m2871384883 ();
extern "C" void Enum_IsDefined_m1555923783 ();
extern "C" void Enum_get_underlying_type_m68214181 ();
extern "C" void Enum_GetUnderlyingType_m4160021701 ();
extern "C" void Enum_FindName_m2628424663 ();
extern "C" void Enum_GetValue_m3881691440 ();
extern "C" void Enum_Parse_m1598758293 ();
extern "C" void Enum_compare_value_to_m2618635368 ();
extern "C" void Enum_CompareTo_m58040714 ();
extern "C" void Enum_ToString_m1753515855 ();
extern "C" void Enum_ToString_m3329141164 ();
extern "C" void Enum_ToString_m997063565 ();
extern "C" void Enum_ToString_m3343977455 ();
extern "C" void Enum_ToObject_m412986634 ();
extern "C" void Enum_ToObject_m1409448066 ();
extern "C" void Enum_ToObject_m1790622089 ();
extern "C" void Enum_ToObject_m3209825685 ();
extern "C" void Enum_ToObject_m1546957196 ();
extern "C" void Enum_ToObject_m2176493389 ();
extern "C" void Enum_ToObject_m363118936 ();
extern "C" void Enum_ToObject_m1273833310 ();
extern "C" void Enum_ToObject_m2284871884 ();
extern "C" void Enum_Equals_m1394678246 ();
extern "C" void Enum_get_hashcode_m919294790 ();
extern "C" void Enum_GetHashCode_m3709446324 ();
extern "C" void Enum_FormatSpecifier_X_m849909662 ();
extern "C" void Enum_FormatFlags_m663299508 ();
extern "C" void Enum_Format_m402722146 ();
extern "C" void Environment_get_SocketSecurityEnabled_m3565195618 ();
extern "C" void Environment_get_NewLine_m921343066 ();
extern "C" void Environment_get_Platform_m3623529198 ();
extern "C" void Environment_GetOSVersionString_m2904473234 ();
extern "C" void Environment_get_OSVersion_m2035335411 ();
extern "C" void Environment_internalGetEnvironmentVariable_m4160102786 ();
extern "C" void Environment_GetEnvironmentVariable_m3846363269 ();
extern "C" void Environment_GetWindowsFolderPath_m1375613171 ();
extern "C" void Environment_GetFolderPath_m3193907391 ();
extern "C" void Environment_ReadXdgUserDir_m2465043690 ();
extern "C" void Environment_InternalGetFolderPath_m4007292074 ();
extern "C" void Environment_get_IsRunningOnWindows_m553665425 ();
extern "C" void Environment_GetMachineConfigPath_m3780024625 ();
extern "C" void Environment_internalGetHome_m3542614548 ();
extern "C" void EventArgs__ctor_m3970052124 ();
extern "C" void EventArgs__cctor_m3463673875 ();
extern "C" void EventHandler__ctor_m2093873464 ();
extern "C" void EventHandler_Invoke_m2399520837 ();
extern "C" void EventHandler_BeginInvoke_m3395495502 ();
extern "C" void EventHandler_EndInvoke_m2472865277 ();
extern "C" void Exception__ctor_m1816951 ();
extern "C" void Exception__ctor_m3415393023 ();
extern "C" void Exception__ctor_m1394352773 ();
extern "C" void Exception__ctor_m947608321 ();
extern "C" void Exception_get_InnerException_m746750210 ();
extern "C" void Exception_get_HResult_m527595245 ();
extern "C" void Exception_set_HResult_m1515670465 ();
extern "C" void Exception_get_ClassName_m2700395653 ();
extern "C" void Exception_get_Message_m4112015361 ();
extern "C" void Exception_get_StackTrace_m3281865502 ();
extern "C" void Exception_ToString_m1002068351 ();
extern "C" void Exception_GetFullNameForStackTrace_m2862456183 ();
extern "C" void Exception_GetType_m3018136727 ();
extern "C" void ExecutionEngineException__ctor_m1110340308 ();
extern "C" void ExecutionEngineException__ctor_m72059654 ();
extern "C" void FieldAccessException__ctor_m2436130539 ();
extern "C" void FieldAccessException__ctor_m2518067006 ();
extern "C" void FieldAccessException__ctor_m1966420921 ();
extern "C" void FlagsAttribute__ctor_m3834565173 ();
extern "C" void FormatException__ctor_m2169431372 ();
extern "C" void FormatException__ctor_m3139021455 ();
extern "C" void FormatException__ctor_m1619316639 ();
extern "C" void GC_SuppressFinalize_m3884757609 ();
extern "C" void Calendar__ctor_m2338449314 ();
extern "C" void Calendar_Clone_m56132940 ();
extern "C" void Calendar_CheckReadOnly_m2788277881 ();
extern "C" void Calendar_get_EraNames_m2687264547 ();
extern "C" void CCFixed_FromDateTime_m3382418001 ();
extern "C" void CCFixed_day_of_week_m4026386843 ();
extern "C" void CCGregorianCalendar_is_leap_year_m4254747021 ();
extern "C" void CCGregorianCalendar_fixed_from_dmy_m3913745442 ();
extern "C" void CCGregorianCalendar_year_from_fixed_m3411809018 ();
extern "C" void CCGregorianCalendar_my_from_fixed_m2023602287 ();
extern "C" void CCGregorianCalendar_dmy_from_fixed_m1125637985 ();
extern "C" void CCGregorianCalendar_month_from_fixed_m1250031562 ();
extern "C" void CCGregorianCalendar_day_from_fixed_m3231376753 ();
extern "C" void CCGregorianCalendar_GetDayOfMonth_m3966148956 ();
extern "C" void CCGregorianCalendar_GetMonth_m3860161554 ();
extern "C" void CCGregorianCalendar_GetYear_m2797871736 ();
extern "C" void CCMath_div_m2559077131 ();
extern "C" void CCMath_mod_m1150743609 ();
extern "C" void CCMath_div_mod_m1713025427 ();
extern "C" void CompareInfo__ctor_m1936272694 ();
extern "C" void CompareInfo__ctor_m213109871 ();
extern "C" void CompareInfo__cctor_m2457604812 ();
extern "C" void CompareInfo_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m703135226 ();
extern "C" void CompareInfo_get_UseManagedCollation_m1122008926 ();
extern "C" void CompareInfo_construct_compareinfo_m3985548769 ();
extern "C" void CompareInfo_free_internal_collator_m3692842750 ();
extern "C" void CompareInfo_internal_compare_m1398079027 ();
extern "C" void CompareInfo_assign_sortkey_m1353932646 ();
extern "C" void CompareInfo_internal_index_m3537664183 ();
extern "C" void CompareInfo_Finalize_m3345386883 ();
extern "C" void CompareInfo_internal_compare_managed_m202538890 ();
extern "C" void CompareInfo_internal_compare_switch_m1789565379 ();
extern "C" void CompareInfo_Compare_m2998687293 ();
extern "C" void CompareInfo_Compare_m433658374 ();
extern "C" void CompareInfo_Compare_m990145062 ();
extern "C" void CompareInfo_Equals_m4096032872 ();
extern "C" void CompareInfo_GetHashCode_m874156047 ();
extern "C" void CompareInfo_GetSortKey_m2288950352 ();
extern "C" void CompareInfo_IndexOf_m1642526294 ();
extern "C" void CompareInfo_internal_index_managed_m847641856 ();
extern "C" void CompareInfo_internal_index_switch_m653835107 ();
extern "C" void CompareInfo_IndexOf_m1727054940 ();
extern "C" void CompareInfo_IsPrefix_m2602982038 ();
extern "C" void CompareInfo_IsSuffix_m2576535659 ();
extern "C" void CompareInfo_LastIndexOf_m729157850 ();
extern "C" void CompareInfo_LastIndexOf_m1490450230 ();
extern "C" void CompareInfo_ToString_m566026374 ();
extern "C" void CompareInfo_get_LCID_m1583067375 ();
extern "C" void CultureInfo__ctor_m3525240161 ();
extern "C" void CultureInfo__ctor_m33467521 ();
extern "C" void CultureInfo__ctor_m924087270 ();
extern "C" void CultureInfo__ctor_m3609353999 ();
extern "C" void CultureInfo__ctor_m950742381 ();
extern "C" void CultureInfo__cctor_m3631226733 ();
extern "C" void CultureInfo_get_InvariantCulture_m1553973733 ();
extern "C" void CultureInfo_get_CurrentCulture_m3186983941 ();
extern "C" void CultureInfo_get_CurrentUICulture_m3611276231 ();
extern "C" void CultureInfo_ConstructCurrentCulture_m2382699215 ();
extern "C" void CultureInfo_ConstructCurrentUICulture_m2245112812 ();
extern "C" void CultureInfo_get_Territory_m2825339350 ();
extern "C" void CultureInfo_get_LCID_m2311161170 ();
extern "C" void CultureInfo_get_Name_m3285309744 ();
extern "C" void CultureInfo_get_Parent_m1061191803 ();
extern "C" void CultureInfo_get_TextInfo_m1378976081 ();
extern "C" void CultureInfo_get_IcuName_m3221843485 ();
extern "C" void CultureInfo_Clone_m2116537561 ();
extern "C" void CultureInfo_Equals_m1263117208 ();
extern "C" void CultureInfo_GetHashCode_m48214238 ();
extern "C" void CultureInfo_ToString_m3153525391 ();
extern "C" void CultureInfo_get_CompareInfo_m3818339347 ();
extern "C" void CultureInfo_get_IsNeutralCulture_m3301447976 ();
extern "C" void CultureInfo_CheckNeutral_m977067172 ();
extern "C" void CultureInfo_get_NumberFormat_m1751954804 ();
extern "C" void CultureInfo_set_NumberFormat_m303999793 ();
extern "C" void CultureInfo_get_DateTimeFormat_m3491120702 ();
extern "C" void CultureInfo_set_DateTimeFormat_m1518630760 ();
extern "C" void CultureInfo_get_IsReadOnly_m2338231366 ();
extern "C" void CultureInfo_GetFormat_m3767138108 ();
extern "C" void CultureInfo_Construct_m3927976456 ();
extern "C" void CultureInfo_ConstructInternalLocaleFromName_m866611573 ();
extern "C" void CultureInfo_ConstructInternalLocaleFromLcid_m1222245966 ();
extern "C" void CultureInfo_ConstructInternalLocaleFromCurrentLocale_m2562214947 ();
extern "C" void CultureInfo_construct_internal_locale_from_lcid_m2093041484 ();
extern "C" void CultureInfo_construct_internal_locale_from_name_m3306341211 ();
extern "C" void CultureInfo_construct_internal_locale_from_current_locale_m1848081026 ();
extern "C" void CultureInfo_construct_datetime_format_m553349517 ();
extern "C" void CultureInfo_construct_number_format_m1440279526 ();
extern "C" void CultureInfo_ConstructInvariant_m2280761812 ();
extern "C" void CultureInfo_CreateTextInfo_m1064081308 ();
extern "C" void CultureInfo_insert_into_shared_tables_m3648892084 ();
extern "C" void CultureInfo_GetCultureInfo_m2002634362 ();
extern "C" void CultureInfo_GetCultureInfo_m1593052334 ();
extern "C" void CultureInfo_CreateCulture_m203779851 ();
extern "C" void DateTimeFormatInfo__ctor_m3648500895 ();
extern "C" void DateTimeFormatInfo__ctor_m760991236 ();
extern "C" void DateTimeFormatInfo__cctor_m2142440524 ();
extern "C" void DateTimeFormatInfo_GetInstance_m4151193095 ();
extern "C" void DateTimeFormatInfo_get_IsReadOnly_m1245526434 ();
extern "C" void DateTimeFormatInfo_ReadOnly_m1624544977 ();
extern "C" void DateTimeFormatInfo_Clone_m221763627 ();
extern "C" void DateTimeFormatInfo_GetFormat_m1198654296 ();
extern "C" void DateTimeFormatInfo_GetAbbreviatedMonthName_m3849932716 ();
extern "C" void DateTimeFormatInfo_GetEraName_m2523233910 ();
extern "C" void DateTimeFormatInfo_GetMonthName_m1976713297 ();
extern "C" void DateTimeFormatInfo_get_RawAbbreviatedDayNames_m2200361064 ();
extern "C" void DateTimeFormatInfo_get_RawAbbreviatedMonthNames_m2837152612 ();
extern "C" void DateTimeFormatInfo_get_RawDayNames_m1788651075 ();
extern "C" void DateTimeFormatInfo_get_RawMonthNames_m3259123252 ();
extern "C" void DateTimeFormatInfo_get_AMDesignator_m1667043795 ();
extern "C" void DateTimeFormatInfo_get_PMDesignator_m1920996550 ();
extern "C" void DateTimeFormatInfo_get_DateSeparator_m3898223580 ();
extern "C" void DateTimeFormatInfo_get_TimeSeparator_m2189288444 ();
extern "C" void DateTimeFormatInfo_get_LongDatePattern_m10673428 ();
extern "C" void DateTimeFormatInfo_get_ShortDatePattern_m800966562 ();
extern "C" void DateTimeFormatInfo_get_ShortTimePattern_m214755676 ();
extern "C" void DateTimeFormatInfo_get_LongTimePattern_m837638707 ();
extern "C" void DateTimeFormatInfo_get_MonthDayPattern_m3900941703 ();
extern "C" void DateTimeFormatInfo_get_YearMonthPattern_m2134005242 ();
extern "C" void DateTimeFormatInfo_get_FullDateTimePattern_m4279931869 ();
extern "C" void DateTimeFormatInfo_get_CurrentInfo_m1909081317 ();
extern "C" void DateTimeFormatInfo_get_InvariantInfo_m3979751787 ();
extern "C" void DateTimeFormatInfo_get_Calendar_m14822801 ();
extern "C" void DateTimeFormatInfo_set_Calendar_m2427543037 ();
extern "C" void DateTimeFormatInfo_get_RFC1123Pattern_m2072104945 ();
extern "C" void DateTimeFormatInfo_get_RoundtripPattern_m343902144 ();
extern "C" void DateTimeFormatInfo_get_SortableDateTimePattern_m2796530772 ();
extern "C" void DateTimeFormatInfo_get_UniversalSortableDateTimePattern_m2797392990 ();
extern "C" void DateTimeFormatInfo_GetAllDateTimePatternsInternal_m3656848191 ();
extern "C" void DateTimeFormatInfo_FillAllDateTimePatterns_m3007362540 ();
extern "C" void DateTimeFormatInfo_GetAllRawDateTimePatterns_m3294676931 ();
extern "C" void DateTimeFormatInfo_GetDayName_m1769119731 ();
extern "C" void DateTimeFormatInfo_GetAbbreviatedDayName_m783154435 ();
extern "C" void DateTimeFormatInfo_FillInvariantPatterns_m308133250 ();
extern "C" void DateTimeFormatInfo_PopulateCombinedList_m85478254 ();
extern "C" void DaylightTime__ctor_m3356372841 ();
extern "C" void DaylightTime_get_Start_m2838577138 ();
extern "C" void DaylightTime_get_End_m99924226 ();
extern "C" void DaylightTime_get_Delta_m2257589543 ();
extern "C" void GregorianCalendar__ctor_m2638258818 ();
extern "C" void GregorianCalendar__ctor_m2095203450 ();
extern "C" void GregorianCalendar_get_Eras_m2448107167 ();
extern "C" void GregorianCalendar_set_CalendarType_m321229003 ();
extern "C" void GregorianCalendar_GetDayOfMonth_m3218095145 ();
extern "C" void GregorianCalendar_GetDayOfWeek_m1831200356 ();
extern "C" void GregorianCalendar_GetEra_m2672846977 ();
extern "C" void GregorianCalendar_GetMonth_m207055093 ();
extern "C" void GregorianCalendar_GetYear_m956119763 ();
extern "C" void NumberFormatInfo__ctor_m3065442163 ();
extern "C" void NumberFormatInfo__ctor_m3394184656 ();
extern "C" void NumberFormatInfo__ctor_m2545500917 ();
extern "C" void NumberFormatInfo__cctor_m2409688165 ();
extern "C" void NumberFormatInfo_get_CurrencyDecimalDigits_m1054586595 ();
extern "C" void NumberFormatInfo_get_CurrencyDecimalSeparator_m1336171097 ();
extern "C" void NumberFormatInfo_get_CurrencyGroupSeparator_m3849499073 ();
extern "C" void NumberFormatInfo_get_RawCurrencyGroupSizes_m2590170303 ();
extern "C" void NumberFormatInfo_get_CurrencyNegativePattern_m683893117 ();
extern "C" void NumberFormatInfo_get_CurrencyPositivePattern_m4188282411 ();
extern "C" void NumberFormatInfo_get_CurrencySymbol_m1696803863 ();
extern "C" void NumberFormatInfo_get_CurrentInfo_m681710701 ();
extern "C" void NumberFormatInfo_get_InvariantInfo_m4171439859 ();
extern "C" void NumberFormatInfo_get_NaNSymbol_m3664499422 ();
extern "C" void NumberFormatInfo_get_NegativeInfinitySymbol_m432321453 ();
extern "C" void NumberFormatInfo_get_NegativeSign_m3505425583 ();
extern "C" void NumberFormatInfo_get_NumberDecimalDigits_m2175143232 ();
extern "C" void NumberFormatInfo_get_NumberDecimalSeparator_m2141040615 ();
extern "C" void NumberFormatInfo_get_NumberGroupSeparator_m2381910380 ();
extern "C" void NumberFormatInfo_get_RawNumberGroupSizes_m1837935180 ();
extern "C" void NumberFormatInfo_get_NumberNegativePattern_m2856862106 ();
extern "C" void NumberFormatInfo_set_NumberNegativePattern_m15717523 ();
extern "C" void NumberFormatInfo_get_PercentDecimalDigits_m3468741610 ();
extern "C" void NumberFormatInfo_get_PercentDecimalSeparator_m3534534709 ();
extern "C" void NumberFormatInfo_get_PercentGroupSeparator_m4188385544 ();
extern "C" void NumberFormatInfo_get_RawPercentGroupSizes_m2784761932 ();
extern "C" void NumberFormatInfo_get_PercentNegativePattern_m1520909170 ();
extern "C" void NumberFormatInfo_get_PercentPositivePattern_m2808305050 ();
extern "C" void NumberFormatInfo_get_PercentSymbol_m2370855822 ();
extern "C" void NumberFormatInfo_get_PerMilleSymbol_m3407208990 ();
extern "C" void NumberFormatInfo_get_PositiveInfinitySymbol_m1704514514 ();
extern "C" void NumberFormatInfo_get_PositiveSign_m3460909578 ();
extern "C" void NumberFormatInfo_GetFormat_m1404969485 ();
extern "C" void NumberFormatInfo_Clone_m2450825682 ();
extern "C" void NumberFormatInfo_GetInstance_m2393140909 ();
extern "C" void RegionInfo__ctor_m1416434243 ();
extern "C" void RegionInfo__ctor_m3049991405 ();
extern "C" void RegionInfo_get_CurrentRegion_m3879384183 ();
extern "C" void RegionInfo_GetByTerritory_m2765791561 ();
extern "C" void RegionInfo_construct_internal_region_from_name_m1964467534 ();
extern "C" void RegionInfo_get_CurrencyEnglishName_m2699486149 ();
extern "C" void RegionInfo_get_CurrencySymbol_m1123963360 ();
extern "C" void RegionInfo_get_DisplayName_m234317390 ();
extern "C" void RegionInfo_get_EnglishName_m1834029122 ();
extern "C" void RegionInfo_get_GeoId_m2009686238 ();
extern "C" void RegionInfo_get_IsMetric_m2010743696 ();
extern "C" void RegionInfo_get_ISOCurrencySymbol_m1527138053 ();
extern "C" void RegionInfo_get_NativeName_m2791477888 ();
extern "C" void RegionInfo_get_CurrencyNativeName_m1078600886 ();
extern "C" void RegionInfo_get_Name_m3529597995 ();
extern "C" void RegionInfo_get_ThreeLetterISORegionName_m3727743814 ();
extern "C" void RegionInfo_get_ThreeLetterWindowsRegionName_m648134458 ();
extern "C" void RegionInfo_get_TwoLetterISORegionName_m4044257697 ();
extern "C" void RegionInfo_Equals_m4016944140 ();
extern "C" void RegionInfo_GetHashCode_m2755242465 ();
extern "C" void RegionInfo_ToString_m1434015883 ();
extern "C" void SortKey__ctor_m3504451567 ();
extern "C" void SortKey__ctor_m148594354 ();
extern "C" void SortKey_Compare_m1772552206 ();
extern "C" void SortKey_get_OriginalString_m1800238925 ();
extern "C" void SortKey_get_KeyData_m2935146546 ();
extern "C" void SortKey_Equals_m273349684 ();
extern "C" void SortKey_GetHashCode_m3484627189 ();
extern "C" void SortKey_ToString_m679734994 ();
extern "C" void TextInfo__ctor_m1202670054 ();
extern "C" void TextInfo__ctor_m3765241533 ();
extern "C" void TextInfo_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m3013407797 ();
extern "C" void TextInfo_get_ListSeparator_m3241381324 ();
extern "C" void TextInfo_get_CultureName_m2324322044 ();
extern "C" void TextInfo_Equals_m1956140142 ();
extern "C" void TextInfo_GetHashCode_m543285532 ();
extern "C" void TextInfo_ToString_m1949805935 ();
extern "C" void TextInfo_ToLower_m941018871 ();
extern "C" void TextInfo_ToUpper_m2328982835 ();
extern "C" void TextInfo_ToLower_m2569245245 ();
extern "C" void TextInfo_Clone_m2167794002 ();
extern "C" void Guid__ctor_m366922919_AdjustorThunk ();
extern "C" void Guid__ctor_m1415939458_AdjustorThunk ();
extern "C" void Guid__ctor_m1840805599_AdjustorThunk ();
extern "C" void Guid__ctor_m4215161261_AdjustorThunk ();
extern "C" void Guid__cctor_m1259807860 ();
extern "C" void Guid_CheckNull_m1432541912 ();
extern "C" void Guid_CheckLength_m404985278 ();
extern "C" void Guid_CheckArray_m979102941 ();
extern "C" void Guid_Compare_m3988396397 ();
extern "C" void Guid_CompareTo_m1283272613_AdjustorThunk ();
extern "C" void Guid_Equals_m996559241_AdjustorThunk ();
extern "C" void Guid_CompareTo_m2483260021_AdjustorThunk ();
extern "C" void Guid_Equals_m3913097454_AdjustorThunk ();
extern "C" void Guid_GetHashCode_m3315775205_AdjustorThunk ();
extern "C" void Guid_ToHex_m2758306647 ();
extern "C" void Guid_NewGuid_m3277502664 ();
extern "C" void Guid_AppendInt_m1197935714 ();
extern "C" void Guid_AppendShort_m2300705645 ();
extern "C" void Guid_AppendByte_m2657930108 ();
extern "C" void Guid_BaseToString_m3503046643_AdjustorThunk ();
extern "C" void Guid_ToString_m2467832444_AdjustorThunk ();
extern "C" void Guid_ToString_m857218039_AdjustorThunk ();
extern "C" void Guid_ToString_m3497849028_AdjustorThunk ();
extern "C" void Guid_op_Equality_m3918449706 ();
extern "C" void GuidParser__ctor_m954092722 ();
extern "C" void GuidParser_Reset_m3895554130 ();
extern "C" void GuidParser_AtEnd_m2681682394 ();
extern "C" void GuidParser_ThrowFormatException_m2810643018 ();
extern "C" void GuidParser_ParseHex_m3369382782 ();
extern "C" void GuidParser_ParseOptChar_m4031216945 ();
extern "C" void GuidParser_ParseChar_m1840855132 ();
extern "C" void GuidParser_ParseGuid1_m103848066 ();
extern "C" void GuidParser_ParseHexPrefix_m2147067076 ();
extern "C" void GuidParser_ParseGuid2_m841714959 ();
extern "C" void GuidParser_Parse_m820144007 ();
extern "C" void IndexOutOfRangeException__ctor_m4192121744 ();
extern "C" void IndexOutOfRangeException__ctor_m2238521308 ();
extern "C" void IndexOutOfRangeException__ctor_m3954211171 ();
extern "C" void Int16_System_IConvertible_ToBoolean_m608568030_AdjustorThunk ();
extern "C" void Int16_System_IConvertible_ToByte_m1672628155_AdjustorThunk ();
extern "C" void Int16_System_IConvertible_ToChar_m2735270145_AdjustorThunk ();
extern "C" void Int16_System_IConvertible_ToDateTime_m3461644636_AdjustorThunk ();
extern "C" void Int16_System_IConvertible_ToDecimal_m2621680868_AdjustorThunk ();
extern "C" void Int16_System_IConvertible_ToDouble_m567044138_AdjustorThunk ();
extern "C" void Int16_System_IConvertible_ToInt16_m1850971393_AdjustorThunk ();
extern "C" void Int16_System_IConvertible_ToInt32_m1937055083_AdjustorThunk ();
extern "C" void Int16_System_IConvertible_ToInt64_m2937777175_AdjustorThunk ();
extern "C" void Int16_System_IConvertible_ToSByte_m2180362671_AdjustorThunk ();
extern "C" void Int16_System_IConvertible_ToSingle_m2608377140_AdjustorThunk ();
extern "C" void Int16_System_IConvertible_ToType_m2143513339_AdjustorThunk ();
extern "C" void Int16_System_IConvertible_ToUInt16_m3718580411_AdjustorThunk ();
extern "C" void Int16_System_IConvertible_ToUInt32_m2354401397_AdjustorThunk ();
extern "C" void Int16_System_IConvertible_ToUInt64_m1213563982_AdjustorThunk ();
extern "C" void Int16_CompareTo_m4027690811_AdjustorThunk ();
extern "C" void Int16_Equals_m1346215590_AdjustorThunk ();
extern "C" void Int16_GetHashCode_m443056876_AdjustorThunk ();
extern "C" void Int16_CompareTo_m2086232926_AdjustorThunk ();
extern "C" void Int16_Equals_m3297601691_AdjustorThunk ();
extern "C" void Int16_Parse_m583677545 ();
extern "C" void Int16_Parse_m1892243601 ();
extern "C" void Int16_Parse_m4281000664 ();
extern "C" void Int16_TryParse_m2772964988 ();
extern "C" void Int16_ToString_m4080367874_AdjustorThunk ();
extern "C" void Int16_ToString_m2532926678_AdjustorThunk ();
extern "C" void Int16_ToString_m1946657537_AdjustorThunk ();
extern "C" void Int16_ToString_m1980211496_AdjustorThunk ();
extern "C" void Int32_System_IConvertible_ToBoolean_m2319908351_AdjustorThunk ();
extern "C" void Int32_System_IConvertible_ToByte_m4272893928_AdjustorThunk ();
extern "C" void Int32_System_IConvertible_ToChar_m2322934717_AdjustorThunk ();
extern "C" void Int32_System_IConvertible_ToDateTime_m715642928_AdjustorThunk ();
extern "C" void Int32_System_IConvertible_ToDecimal_m2713593576_AdjustorThunk ();
extern "C" void Int32_System_IConvertible_ToDouble_m1298176326_AdjustorThunk ();
extern "C" void Int32_System_IConvertible_ToInt16_m2439558523_AdjustorThunk ();
extern "C" void Int32_System_IConvertible_ToInt32_m1807903970_AdjustorThunk ();
extern "C" void Int32_System_IConvertible_ToInt64_m4037035890_AdjustorThunk ();
extern "C" void Int32_System_IConvertible_ToSByte_m4052844363_AdjustorThunk ();
extern "C" void Int32_System_IConvertible_ToSingle_m3592801059_AdjustorThunk ();
extern "C" void Int32_System_IConvertible_ToType_m2475977603_AdjustorThunk ();
extern "C" void Int32_System_IConvertible_ToUInt16_m512640774_AdjustorThunk ();
extern "C" void Int32_System_IConvertible_ToUInt32_m2688754728_AdjustorThunk ();
extern "C" void Int32_System_IConvertible_ToUInt64_m2142522567_AdjustorThunk ();
extern "C" void Int32_CompareTo_m2643288015_AdjustorThunk ();
extern "C" void Int32_Equals_m1484368690_AdjustorThunk ();
extern "C" void Int32_GetHashCode_m339244912_AdjustorThunk ();
extern "C" void Int32_CompareTo_m960498275_AdjustorThunk ();
extern "C" void Int32_Equals_m2128805555_AdjustorThunk ();
extern "C" void Int32_ProcessTrailingWhitespace_m3213104504 ();
extern "C" void Int32_Parse_m1849335896 ();
extern "C" void Int32_Parse_m2525031250 ();
extern "C" void Int32_CheckStyle_m3873463410 ();
extern "C" void Int32_JumpOverWhite_m1044411918 ();
extern "C" void Int32_FindSign_m3693607241 ();
extern "C" void Int32_FindCurrency_m3094038441 ();
extern "C" void Int32_FindExponent_m2503482456 ();
extern "C" void Int32_FindOther_m253001860 ();
extern "C" void Int32_ValidDigit_m1948424466 ();
extern "C" void Int32_GetFormatException_m3406637715 ();
extern "C" void Int32_Parse_m3592018864 ();
extern "C" void Int32_Parse_m501041078 ();
extern "C" void Int32_Parse_m1540871473 ();
extern "C" void Int32_TryParse_m86332859 ();
extern "C" void Int32_TryParse_m1513256800 ();
extern "C" void Int32_ToString_m942907109_AdjustorThunk ();
extern "C" void Int32_ToString_m1055050781_AdjustorThunk ();
extern "C" void Int32_ToString_m678894197_AdjustorThunk ();
extern "C" void Int32_ToString_m3162039764_AdjustorThunk ();
extern "C" void Int64_System_IConvertible_ToBoolean_m4098580936_AdjustorThunk ();
extern "C" void Int64_System_IConvertible_ToByte_m1049109362_AdjustorThunk ();
extern "C" void Int64_System_IConvertible_ToChar_m2468419666_AdjustorThunk ();
extern "C" void Int64_System_IConvertible_ToDateTime_m2709613486_AdjustorThunk ();
extern "C" void Int64_System_IConvertible_ToDecimal_m736076175_AdjustorThunk ();
extern "C" void Int64_System_IConvertible_ToDouble_m35851038_AdjustorThunk ();
extern "C" void Int64_System_IConvertible_ToInt16_m2310188060_AdjustorThunk ();
extern "C" void Int64_System_IConvertible_ToInt32_m149402608_AdjustorThunk ();
extern "C" void Int64_System_IConvertible_ToInt64_m1119652710_AdjustorThunk ();
extern "C" void Int64_System_IConvertible_ToSByte_m4250139882_AdjustorThunk ();
extern "C" void Int64_System_IConvertible_ToSingle_m3691596191_AdjustorThunk ();
extern "C" void Int64_System_IConvertible_ToType_m480516046_AdjustorThunk ();
extern "C" void Int64_System_IConvertible_ToUInt16_m1774985091_AdjustorThunk ();
extern "C" void Int64_System_IConvertible_ToUInt32_m490642399_AdjustorThunk ();
extern "C" void Int64_System_IConvertible_ToUInt64_m476448274_AdjustorThunk ();
extern "C" void Int64_CompareTo_m4140694992_AdjustorThunk ();
extern "C" void Int64_Equals_m3567984782_AdjustorThunk ();
extern "C" void Int64_GetHashCode_m2212758480_AdjustorThunk ();
extern "C" void Int64_CompareTo_m2587245040_AdjustorThunk ();
extern "C" void Int64_Equals_m1853265956_AdjustorThunk ();
extern "C" void Int64_Parse_m115698243 ();
extern "C" void Int64_Parse_m2921227530 ();
extern "C" void Int64_Parse_m3896306439 ();
extern "C" void Int64_Parse_m3857086958 ();
extern "C" void Int64_Parse_m4285359207 ();
extern "C" void Int64_TryParse_m3030841212 ();
extern "C" void Int64_TryParse_m1386765942 ();
extern "C" void Int64_ToString_m1285539637_AdjustorThunk ();
extern "C" void Int64_ToString_m2384113553_AdjustorThunk ();
extern "C" void Int64_ToString_m2836968315_AdjustorThunk ();
extern "C" void Int64_ToString_m1263666479_AdjustorThunk ();
extern "C" void IntPtr__ctor_m1874125153_AdjustorThunk ();
extern "C" void IntPtr__ctor_m1846124433_AdjustorThunk ();
extern "C" void IntPtr__ctor_m3020263568_AdjustorThunk ();
extern "C" void IntPtr__ctor_m3650517410_AdjustorThunk ();
extern "C" void IntPtr_get_Size_m3603629221 ();
extern "C" void IntPtr_Equals_m3250342679_AdjustorThunk ();
extern "C" void IntPtr_GetHashCode_m2382536356_AdjustorThunk ();
extern "C" void IntPtr_ToInt64_m3776154157_AdjustorThunk ();
extern "C" void IntPtr_ToPointer_m565950407_AdjustorThunk ();
extern "C" void IntPtr_ToString_m2819219677_AdjustorThunk ();
extern "C" void IntPtr_ToString_m1070034645_AdjustorThunk ();
extern "C" void IntPtr_op_Equality_m3978210203 ();
extern "C" void IntPtr_op_Inequality_m1374147269 ();
extern "C" void IntPtr_op_Explicit_m3440174878 ();
extern "C" void IntPtr_op_Explicit_m1287079606 ();
extern "C" void IntPtr_op_Explicit_m913523000 ();
extern "C" void IntPtr_op_Explicit_m2003607512 ();
extern "C" void IntPtr_op_Explicit_m3354255807 ();
extern "C" void InvalidCastException__ctor_m2302931503 ();
extern "C" void InvalidCastException__ctor_m1929418500 ();
extern "C" void InvalidCastException__ctor_m395183990 ();
extern "C" void InvalidOperationException__ctor_m3074049270 ();
extern "C" void InvalidOperationException__ctor_m1754939824 ();
extern "C" void InvalidOperationException__ctor_m1249789426 ();
extern "C" void InvalidOperationException__ctor_m1909242104 ();
extern "C" void BinaryReader__ctor_m3891178347 ();
extern "C" void BinaryReader__ctor_m2947142779 ();
extern "C" void BinaryReader_System_IDisposable_Dispose_m3066875955 ();
extern "C" void BinaryReader_get_BaseStream_m436692523 ();
extern "C" void BinaryReader_Close_m656404955 ();
extern "C" void BinaryReader_Dispose_m1579899878 ();
extern "C" void BinaryReader_FillBuffer_m4230478491 ();
extern "C" void BinaryReader_Read_m2359026958 ();
extern "C" void BinaryReader_Read_m2792699949 ();
extern "C" void BinaryReader_Read_m3041790912 ();
extern "C" void BinaryReader_ReadCharBytes_m4274906988 ();
extern "C" void BinaryReader_Read7BitEncodedInt_m3619737175 ();
extern "C" void BinaryReader_ReadBoolean_m3554613854 ();
extern "C" void BinaryReader_ReadByte_m2801931525 ();
extern "C" void BinaryReader_ReadBytes_m1713992062 ();
extern "C" void BinaryReader_ReadChar_m914228394 ();
extern "C" void BinaryReader_ReadDecimal_m768237421 ();
extern "C" void BinaryReader_ReadDouble_m1160704559 ();
extern "C" void BinaryReader_ReadInt16_m1101465428 ();
extern "C" void BinaryReader_ReadInt32_m995919288 ();
extern "C" void BinaryReader_ReadInt64_m1982603364 ();
extern "C" void BinaryReader_ReadSByte_m3116176050 ();
extern "C" void BinaryReader_ReadString_m3318513216 ();
extern "C" void BinaryReader_ReadSingle_m2392231938 ();
extern "C" void BinaryReader_ReadUInt16_m3883529060 ();
extern "C" void BinaryReader_ReadUInt32_m4084076630 ();
extern "C" void BinaryReader_ReadUInt64_m181663393 ();
extern "C" void BinaryReader_CheckBuffer_m2312246899 ();
extern "C" void Directory_CreateDirectory_m1758760249 ();
extern "C" void Directory_CreateDirectoriesInternal_m3935921796 ();
extern "C" void Directory_Exists_m3276502796 ();
extern "C" void Directory_GetCurrentDirectory_m331201736 ();
extern "C" void Directory_GetFiles_m235723336 ();
extern "C" void Directory_GetFileSystemEntries_m3572746264 ();
extern "C" void DirectoryInfo__ctor_m1609466484 ();
extern "C" void DirectoryInfo__ctor_m2105118223 ();
extern "C" void DirectoryInfo__ctor_m474244860 ();
extern "C" void DirectoryInfo_Initialize_m428229676 ();
extern "C" void DirectoryInfo_get_Exists_m1872834505 ();
extern "C" void DirectoryInfo_get_Parent_m3132120194 ();
extern "C" void DirectoryInfo_Create_m1869279555 ();
extern "C" void DirectoryInfo_ToString_m443528604 ();
extern "C" void DirectoryNotFoundException__ctor_m1947819234 ();
extern "C" void DirectoryNotFoundException__ctor_m1202819030 ();
extern "C" void DirectoryNotFoundException__ctor_m2906679049 ();
extern "C" void EndOfStreamException__ctor_m2347655014 ();
extern "C" void EndOfStreamException__ctor_m178835592 ();
extern "C" void File_Delete_m2485719844 ();
extern "C" void File_Exists_m2248590861 ();
extern "C" void File_Open_m2870785110 ();
extern "C" void File_OpenRead_m2214564889 ();
extern "C" void File_OpenText_m276421639 ();
extern "C" void FileLoadException__ctor_m521743258 ();
extern "C" void FileLoadException__ctor_m3079274755 ();
extern "C" void FileLoadException_get_Message_m98332663 ();
extern "C" void FileLoadException_ToString_m3184166710 ();
extern "C" void FileNotFoundException__ctor_m1923109332 ();
extern "C" void FileNotFoundException__ctor_m3508239193 ();
extern "C" void FileNotFoundException__ctor_m2268276037 ();
extern "C" void FileNotFoundException_get_Message_m2819723224 ();
extern "C" void FileNotFoundException_ToString_m260718854 ();
extern "C" void FileStream__ctor_m3918629695 ();
extern "C" void FileStream__ctor_m2298191702 ();
extern "C" void FileStream__ctor_m1378073862 ();
extern "C" void FileStream__ctor_m1627439574 ();
extern "C" void FileStream__ctor_m199725632 ();
extern "C" void FileStream_get_CanRead_m2205092554 ();
extern "C" void FileStream_get_CanWrite_m3403378411 ();
extern "C" void FileStream_get_CanSeek_m1969291698 ();
extern "C" void FileStream_get_Length_m3023624910 ();
extern "C" void FileStream_get_Position_m1789780299 ();
extern "C" void FileStream_set_Position_m1342652146 ();
extern "C" void FileStream_ReadByte_m1668565028 ();
extern "C" void FileStream_WriteByte_m24018895 ();
extern "C" void FileStream_Read_m2991800180 ();
extern "C" void FileStream_ReadInternal_m4238080744 ();
extern "C" void FileStream_BeginRead_m2129228277 ();
extern "C" void FileStream_EndRead_m2497897596 ();
extern "C" void FileStream_Write_m1947011602 ();
extern "C" void FileStream_WriteInternal_m37363449 ();
extern "C" void FileStream_BeginWrite_m3846105556 ();
extern "C" void FileStream_EndWrite_m4253206090 ();
extern "C" void FileStream_Seek_m3619933131 ();
extern "C" void FileStream_SetLength_m3541353046 ();
extern "C" void FileStream_Flush_m888958391 ();
extern "C" void FileStream_Finalize_m274693354 ();
extern "C" void FileStream_Dispose_m3584873965 ();
extern "C" void FileStream_ReadSegment_m928397223 ();
extern "C" void FileStream_WriteSegment_m527164273 ();
extern "C" void FileStream_FlushBuffer_m533766994 ();
extern "C" void FileStream_FlushBuffer_m2876878919 ();
extern "C" void FileStream_FlushBufferIfDirty_m676211487 ();
extern "C" void FileStream_RefillBuffer_m537701314 ();
extern "C" void FileStream_ReadData_m406536788 ();
extern "C" void FileStream_InitBuffer_m2348525094 ();
extern "C" void FileStream_GetSecureFileName_m3144911672 ();
extern "C" void FileStream_GetSecureFileName_m2765795265 ();
extern "C" void ReadDelegate__ctor_m1101817851 ();
extern "C" void ReadDelegate_Invoke_m2426149229 ();
extern "C" void ReadDelegate_BeginInvoke_m923466772 ();
extern "C" void ReadDelegate_EndInvoke_m1073758779 ();
extern "C" void WriteDelegate__ctor_m944271225 ();
extern "C" void WriteDelegate_Invoke_m1513952088 ();
extern "C" void WriteDelegate_BeginInvoke_m3915988971 ();
extern "C" void WriteDelegate_EndInvoke_m1108179134 ();
extern "C" void FileStreamAsyncResult__ctor_m3777138729 ();
extern "C" void FileStreamAsyncResult_CBWrapper_m3203659335 ();
extern "C" void FileStreamAsyncResult_get_AsyncState_m2828107890 ();
extern "C" void FileStreamAsyncResult_get_AsyncWaitHandle_m634220271 ();
extern "C" void FileStreamAsyncResult_get_IsCompleted_m283701089 ();
extern "C" void FileSystemInfo__ctor_m3361274585 ();
extern "C" void FileSystemInfo__ctor_m3175765434 ();
extern "C" void FileSystemInfo_get_FullName_m1008202315 ();
extern "C" void FileSystemInfo_Refresh_m2155846864 ();
extern "C" void FileSystemInfo_InternalRefresh_m2899045144 ();
extern "C" void FileSystemInfo_CheckPath_m601048000 ();
extern "C" void IOException__ctor_m2747881001 ();
extern "C" void IOException__ctor_m239973922 ();
extern "C" void IOException__ctor_m804076173 ();
extern "C" void IOException__ctor_m2871752017 ();
extern "C" void IOException__ctor_m324016635 ();
extern "C" void IsolatedStorageException__ctor_m2374576592 ();
extern "C" void IsolatedStorageException__ctor_m2598607174 ();
extern "C" void IsolatedStorageException__ctor_m1969844842 ();
extern "C" void MemoryStream__ctor_m152170362 ();
extern "C" void MemoryStream__ctor_m3154874056 ();
extern "C" void MemoryStream__ctor_m3060464295 ();
extern "C" void MemoryStream_InternalConstructor_m2014775173 ();
extern "C" void MemoryStream_CheckIfClosedThrowDisposed_m1274599527 ();
extern "C" void MemoryStream_get_CanRead_m4260422867 ();
extern "C" void MemoryStream_get_CanSeek_m3031870200 ();
extern "C" void MemoryStream_get_CanWrite_m1144494425 ();
extern "C" void MemoryStream_set_Capacity_m609551339 ();
extern "C" void MemoryStream_get_Length_m2968868229 ();
extern "C" void MemoryStream_get_Position_m3673270320 ();
extern "C" void MemoryStream_set_Position_m1362836606 ();
extern "C" void MemoryStream_Dispose_m2676754718 ();
extern "C" void MemoryStream_Flush_m2402768916 ();
extern "C" void MemoryStream_Read_m1905632689 ();
extern "C" void MemoryStream_ReadByte_m1203405088 ();
extern "C" void MemoryStream_Seek_m413807161 ();
extern "C" void MemoryStream_CalculateNewCapacity_m4130758411 ();
extern "C" void MemoryStream_Expand_m2060252436 ();
extern "C" void MemoryStream_SetLength_m797959567 ();
extern "C" void MemoryStream_ToArray_m3844442891 ();
extern "C" void MemoryStream_Write_m58461126 ();
extern "C" void MemoryStream_WriteByte_m957317921 ();
extern "C" void MonoIO__cctor_m2224974877 ();
extern "C" void MonoIO_GetException_m3894828512 ();
extern "C" void MonoIO_GetException_m3130736866 ();
extern "C" void MonoIO_CreateDirectory_m1218943223 ();
extern "C" void MonoIO_GetFileSystemEntries_m3041356216 ();
extern "C" void MonoIO_GetCurrentDirectory_m4279991629 ();
extern "C" void MonoIO_DeleteFile_m4294701721 ();
extern "C" void MonoIO_GetFileAttributes_m3850897511 ();
extern "C" void MonoIO_GetFileType_m1683047928 ();
extern "C" void MonoIO_ExistsFile_m545552615 ();
extern "C" void MonoIO_ExistsDirectory_m1423241567 ();
extern "C" void MonoIO_GetFileStat_m3378506111 ();
extern "C" void MonoIO_Open_m4023759764 ();
extern "C" void MonoIO_Close_m154153664 ();
extern "C" void MonoIO_Read_m1573024126 ();
extern "C" void MonoIO_Write_m4073260495 ();
extern "C" void MonoIO_Seek_m2929164469 ();
extern "C" void MonoIO_GetLength_m931400829 ();
extern "C" void MonoIO_SetLength_m3496987753 ();
extern "C" void MonoIO_get_ConsoleOutput_m2839318334 ();
extern "C" void MonoIO_get_ConsoleInput_m270320281 ();
extern "C" void MonoIO_get_ConsoleError_m459703959 ();
extern "C" void MonoIO_get_VolumeSeparatorChar_m1461410365 ();
extern "C" void MonoIO_get_DirectorySeparatorChar_m474903005 ();
extern "C" void MonoIO_get_AltDirectorySeparatorChar_m3530465749 ();
extern "C" void MonoIO_get_PathSeparator_m3158637569 ();
extern "C" void MonoIO_RemapPath_m4185117495 ();
extern "C" void NullStream__ctor_m1694643915 ();
extern "C" void NullStream_get_CanRead_m244471328 ();
extern "C" void NullStream_get_CanSeek_m3696771727 ();
extern "C" void NullStream_get_CanWrite_m2701633067 ();
extern "C" void NullStream_get_Length_m3809531554 ();
extern "C" void NullStream_get_Position_m1314498788 ();
extern "C" void NullStream_set_Position_m1303016132 ();
extern "C" void NullStream_Flush_m608790423 ();
extern "C" void NullStream_Read_m869508193 ();
extern "C" void NullStream_ReadByte_m909255314 ();
extern "C" void NullStream_Seek_m1958745155 ();
extern "C" void NullStream_SetLength_m3931240662 ();
extern "C" void NullStream_Write_m1347951148 ();
extern "C" void NullStream_WriteByte_m540204641 ();
extern "C" void Path__cctor_m1964914989 ();
extern "C" void Path_Combine_m668557981 ();
extern "C" void Path_CleanPath_m928099398 ();
extern "C" void Path_GetDirectoryName_m3928521125 ();
extern "C" void Path_GetFileName_m1682010817 ();
extern "C" void Path_GetFullPath_m452249650 ();
extern "C" void Path_WindowsDriveAdjustment_m2051393215 ();
extern "C" void Path_InsecureGetFullPath_m2251214602 ();
extern "C" void Path_IsDsc_m4246137163 ();
extern "C" void Path_GetPathRoot_m3398115072 ();
extern "C" void Path_IsPathRooted_m955005557 ();
extern "C" void Path_GetInvalidPathChars_m4276546429 ();
extern "C" void Path_GetServerAndShare_m1454975977 ();
extern "C" void Path_SameRoot_m2647504235 ();
extern "C" void Path_CanonicalizePath_m2319072634 ();
extern "C" void PathTooLongException__ctor_m1890132073 ();
extern "C" void PathTooLongException__ctor_m4167185363 ();
extern "C" void PathTooLongException__ctor_m232413694 ();
extern "C" void SearchPattern__cctor_m2509654630 ();
extern "C" void Stream__ctor_m1504197473 ();
extern "C" void Stream__cctor_m381764956 ();
extern "C" void Stream_Dispose_m2041138381 ();
extern "C" void Stream_Dispose_m1784135134 ();
extern "C" void Stream_Close_m3852604827 ();
extern "C" void Stream_ReadByte_m3485022462 ();
extern "C" void Stream_WriteByte_m4148766345 ();
extern "C" void Stream_BeginRead_m21201343 ();
extern "C" void Stream_BeginWrite_m1364911803 ();
extern "C" void Stream_EndRead_m347362531 ();
extern "C" void Stream_EndWrite_m1831026058 ();
extern "C" void StreamAsyncResult__ctor_m1151342516 ();
extern "C" void StreamAsyncResult_SetComplete_m1586080444 ();
extern "C" void StreamAsyncResult_SetComplete_m1418769851 ();
extern "C" void StreamAsyncResult_get_AsyncState_m586245974 ();
extern "C" void StreamAsyncResult_get_AsyncWaitHandle_m4196239148 ();
extern "C" void StreamAsyncResult_get_IsCompleted_m3518571650 ();
extern "C" void StreamAsyncResult_get_Exception_m1467024748 ();
extern "C" void StreamAsyncResult_get_NBytes_m1123713136 ();
extern "C" void StreamAsyncResult_get_Done_m2868486077 ();
extern "C" void StreamAsyncResult_set_Done_m4141014649 ();
extern "C" void StreamReader__ctor_m306838996 ();
extern "C" void StreamReader__ctor_m1430778449 ();
extern "C" void StreamReader__ctor_m772411305 ();
extern "C" void StreamReader__ctor_m574213171 ();
extern "C" void StreamReader__ctor_m476460896 ();
extern "C" void StreamReader__cctor_m2999995226 ();
extern "C" void StreamReader_Initialize_m3472901841 ();
extern "C" void StreamReader_Dispose_m1707073584 ();
extern "C" void StreamReader_DoChecks_m382520478 ();
extern "C" void StreamReader_ReadBuffer_m2597661069 ();
extern "C" void StreamReader_Peek_m840963052 ();
extern "C" void StreamReader_Read_m4201100268 ();
extern "C" void StreamReader_Read_m984462095 ();
extern "C" void StreamReader_FindNextEOL_m1787012226 ();
extern "C" void StreamReader_ReadLine_m1306910928 ();
extern "C" void StreamReader_ReadToEnd_m2702940513 ();
extern "C" void NullStreamReader__ctor_m2177862304 ();
extern "C" void NullStreamReader_Peek_m962605665 ();
extern "C" void NullStreamReader_Read_m3147588732 ();
extern "C" void NullStreamReader_Read_m559467848 ();
extern "C" void NullStreamReader_ReadLine_m1824535686 ();
extern "C" void NullStreamReader_ReadToEnd_m699976792 ();
extern "C" void StreamWriter__ctor_m2953689202 ();
extern "C" void StreamWriter__ctor_m2151664980 ();
extern "C" void StreamWriter__cctor_m3972042264 ();
extern "C" void StreamWriter_Initialize_m136491558 ();
extern "C" void StreamWriter_set_AutoFlush_m4040646994 ();
extern "C" void StreamWriter_Dispose_m2868164241 ();
extern "C" void StreamWriter_Flush_m2894376598 ();
extern "C" void StreamWriter_FlushBytes_m3815561943 ();
extern "C" void StreamWriter_Decode_m1893779174 ();
extern "C" void StreamWriter_Write_m754966457 ();
extern "C" void StreamWriter_LowLevelWrite_m1464925781 ();
extern "C" void StreamWriter_LowLevelWrite_m1585693714 ();
extern "C" void StreamWriter_Write_m411622688 ();
extern "C" void StreamWriter_Write_m3672466093 ();
extern "C" void StreamWriter_Write_m1157245398 ();
extern "C" void StreamWriter_Close_m2543594019 ();
extern "C" void StreamWriter_Finalize_m2960509706 ();
extern "C" void StringReader__ctor_m293197912 ();
extern "C" void StringReader_Dispose_m3582706645 ();
extern "C" void StringReader_Peek_m517508988 ();
extern "C" void StringReader_Read_m219013114 ();
extern "C" void StringReader_Read_m1450996487 ();
extern "C" void StringReader_ReadLine_m3378185754 ();
extern "C" void StringReader_ReadToEnd_m3293493509 ();
extern "C" void StringReader_CheckObjectDisposedException_m459857537 ();
extern "C" void SynchronizedReader__ctor_m97537115 ();
extern "C" void SynchronizedReader_Peek_m2068499653 ();
extern "C" void SynchronizedReader_ReadLine_m603631510 ();
extern "C" void SynchronizedReader_ReadToEnd_m1477645998 ();
extern "C" void SynchronizedReader_Read_m1699992380 ();
extern "C" void SynchronizedReader_Read_m1276095379 ();
extern "C" void SynchronizedWriter__ctor_m3291303868 ();
extern "C" void SynchronizedWriter_Close_m976773255 ();
extern "C" void SynchronizedWriter_Flush_m2790170340 ();
extern "C" void SynchronizedWriter_Write_m721687999 ();
extern "C" void SynchronizedWriter_Write_m4139598715 ();
extern "C" void SynchronizedWriter_Write_m2563334752 ();
extern "C" void SynchronizedWriter_Write_m880103225 ();
extern "C" void SynchronizedWriter_WriteLine_m189940250 ();
extern "C" void SynchronizedWriter_WriteLine_m2909572594 ();
extern "C" void TextReader__ctor_m1823118017 ();
extern "C" void TextReader__cctor_m1403833620 ();
extern "C" void TextReader_Dispose_m2213368838 ();
extern "C" void TextReader_Dispose_m2560631502 ();
extern "C" void TextReader_Peek_m2365689290 ();
extern "C" void TextReader_Read_m13623654 ();
extern "C" void TextReader_Read_m947437872 ();
extern "C" void TextReader_ReadLine_m1272697447 ();
extern "C" void TextReader_ReadToEnd_m2199643530 ();
extern "C" void TextReader_Synchronized_m3878481166 ();
extern "C" void NullTextReader__ctor_m2057671012 ();
extern "C" void NullTextReader_ReadLine_m2049156248 ();
extern "C" void TextWriter__ctor_m1254332623 ();
extern "C" void TextWriter__cctor_m2254058948 ();
extern "C" void TextWriter_Close_m1696769202 ();
extern "C" void TextWriter_Dispose_m1209375003 ();
extern "C" void TextWriter_Dispose_m3262285662 ();
extern "C" void TextWriter_Flush_m3921915905 ();
extern "C" void TextWriter_Synchronized_m921099061 ();
extern "C" void TextWriter_Write_m4283680745 ();
extern "C" void TextWriter_Write_m1908988917 ();
extern "C" void TextWriter_Write_m1675905377 ();
extern "C" void TextWriter_Write_m2294181007 ();
extern "C" void TextWriter_WriteLine_m1222244591 ();
extern "C" void TextWriter_WriteLine_m1258241687 ();
extern "C" void NullTextWriter__ctor_m4250544285 ();
extern "C" void NullTextWriter_Write_m2871250657 ();
extern "C" void NullTextWriter_Write_m1953194581 ();
extern "C" void NullTextWriter_Write_m3278725927 ();
extern "C" void UnexceptionalStreamReader__ctor_m3446914697 ();
extern "C" void UnexceptionalStreamReader__cctor_m364230527 ();
extern "C" void UnexceptionalStreamReader_Peek_m2077668078 ();
extern "C" void UnexceptionalStreamReader_Read_m3056287453 ();
extern "C" void UnexceptionalStreamReader_Read_m3929647529 ();
extern "C" void UnexceptionalStreamReader_CheckEOL_m1688596573 ();
extern "C" void UnexceptionalStreamReader_ReadLine_m3183025907 ();
extern "C" void UnexceptionalStreamReader_ReadToEnd_m2853978983 ();
extern "C" void UnexceptionalStreamWriter__ctor_m2260891421 ();
extern "C" void UnexceptionalStreamWriter_Flush_m2386651684 ();
extern "C" void UnexceptionalStreamWriter_Write_m1684018641 ();
extern "C" void UnexceptionalStreamWriter_Write_m2192175500 ();
extern "C" void UnexceptionalStreamWriter_Write_m3307790284 ();
extern "C" void UnexceptionalStreamWriter_Write_m2296388455 ();
extern "C" void UnmanagedMemoryStream_get_CanRead_m3880114840 ();
extern "C" void UnmanagedMemoryStream_get_CanSeek_m611005855 ();
extern "C" void UnmanagedMemoryStream_get_CanWrite_m1101817676 ();
extern "C" void UnmanagedMemoryStream_get_Length_m1106236018 ();
extern "C" void UnmanagedMemoryStream_get_Position_m3512336964 ();
extern "C" void UnmanagedMemoryStream_set_Position_m2743156219 ();
extern "C" void UnmanagedMemoryStream_Read_m3618811389 ();
extern "C" void UnmanagedMemoryStream_ReadByte_m1842835906 ();
extern "C" void UnmanagedMemoryStream_Seek_m3376649204 ();
extern "C" void UnmanagedMemoryStream_SetLength_m1197559161 ();
extern "C" void UnmanagedMemoryStream_Flush_m2850088448 ();
extern "C" void UnmanagedMemoryStream_Dispose_m296305050 ();
extern "C" void UnmanagedMemoryStream_Write_m2848563498 ();
extern "C" void UnmanagedMemoryStream_WriteByte_m2073206089 ();
extern "C" void LocalDataStoreSlot__ctor_m2109704463 ();
extern "C" void LocalDataStoreSlot__cctor_m1713086281 ();
extern "C" void LocalDataStoreSlot_Finalize_m3310459962 ();
extern "C" void MarshalByRefObject__ctor_m3580128209 ();
extern "C" void MarshalByRefObject_get_ObjectIdentity_m1652221559 ();
extern "C" void Math_Abs_m409923275 ();
extern "C" void Math_Abs_m449664873 ();
extern "C" void Math_Abs_m1960589991 ();
extern "C" void Math_Floor_m685695611 ();
extern "C" void Math_Max_m3051295998 ();
extern "C" void Math_Min_m1276480861 ();
extern "C" void Math_Round_m3365377037 ();
extern "C" void Math_Round_m2262982208 ();
extern "C" void Math_Pow_m1935598555 ();
extern "C" void Math_Sqrt_m121065893 ();
extern "C" void MemberAccessException__ctor_m1789979295 ();
extern "C" void MemberAccessException__ctor_m4010303872 ();
extern "C" void MemberAccessException__ctor_m3521037496 ();
extern "C" void MethodAccessException__ctor_m1637646650 ();
extern "C" void MethodAccessException__ctor_m3103093336 ();
extern "C" void MissingFieldException__ctor_m188650395 ();
extern "C" void MissingFieldException__ctor_m1971298103 ();
extern "C" void MissingFieldException__ctor_m3080962593 ();
extern "C" void MissingFieldException_get_Message_m1093724828 ();
extern "C" void MissingMemberException__ctor_m766432918 ();
extern "C" void MissingMemberException__ctor_m2292368174 ();
extern "C" void MissingMemberException__ctor_m2506390741 ();
extern "C" void MissingMemberException__ctor_m176814157 ();
extern "C" void MissingMemberException_get_Message_m1601224464 ();
extern "C" void MissingMethodException__ctor_m2581308231 ();
extern "C" void MissingMethodException__ctor_m1031234694 ();
extern "C" void MissingMethodException__ctor_m16286169 ();
extern "C" void MissingMethodException__ctor_m2949116767 ();
extern "C" void MissingMethodException_get_Message_m842428339 ();
extern "C" void MonoAsyncCall__ctor_m3504457514 ();
extern "C" void MonoCustomAttrs__cctor_m13843532 ();
extern "C" void MonoCustomAttrs_IsUserCattrProvider_m2069719286 ();
extern "C" void MonoCustomAttrs_GetCustomAttributesInternal_m3884961971 ();
extern "C" void MonoCustomAttrs_GetPseudoCustomAttributes_m3610733686 ();
extern "C" void MonoCustomAttrs_GetCustomAttributesBase_m2436230232 ();
extern "C" void MonoCustomAttrs_GetCustomAttribute_m4275597331 ();
extern "C" void MonoCustomAttrs_GetCustomAttributes_m3735333428 ();
extern "C" void MonoCustomAttrs_GetCustomAttributes_m3649344832 ();
extern "C" void MonoCustomAttrs_GetCustomAttributesDataInternal_m1105915382 ();
extern "C" void MonoCustomAttrs_GetCustomAttributesData_m4191467674 ();
extern "C" void MonoCustomAttrs_IsDefined_m3302438960 ();
extern "C" void MonoCustomAttrs_IsDefinedInternal_m3560397479 ();
extern "C" void MonoCustomAttrs_GetBasePropertyDefinition_m87202095 ();
extern "C" void MonoCustomAttrs_GetBase_m204129966 ();
extern "C" void MonoCustomAttrs_RetrieveAttributeUsage_m881586189 ();
extern "C" void AttributeInfo__ctor_m914143849 ();
extern "C" void AttributeInfo_get_Usage_m21368055 ();
extern "C" void AttributeInfo_get_InheritanceLevel_m4227771319 ();
extern "C" void MonoDocumentationNoteAttribute__ctor_m3324629758 ();
extern "C" void MonoEnumInfo__ctor_m1643764635_AdjustorThunk ();
extern "C" void MonoEnumInfo__cctor_m2409779423 ();
extern "C" void MonoEnumInfo_get_enum_info_m2018182279 ();
extern "C" void MonoEnumInfo_get_Cache_m2364646175 ();
extern "C" void MonoEnumInfo_GetInfo_m483821611 ();
extern "C" void IntComparer__ctor_m498925939 ();
extern "C" void IntComparer_Compare_m2390570492 ();
extern "C" void IntComparer_Compare_m2288675866 ();
extern "C" void LongComparer__ctor_m1782533220 ();
extern "C" void LongComparer_Compare_m3581620395 ();
extern "C" void LongComparer_Compare_m3608989338 ();
extern "C" void SByteComparer__ctor_m1544949693 ();
extern "C" void SByteComparer_Compare_m1249332637 ();
extern "C" void SByteComparer_Compare_m1565703925 ();
extern "C" void ShortComparer__ctor_m809988133 ();
extern "C" void ShortComparer_Compare_m3794535082 ();
extern "C" void ShortComparer_Compare_m2290445900 ();
extern "C" void MonoTODOAttribute__ctor_m1347054473 ();
extern "C" void MonoTODOAttribute__ctor_m2689597370 ();
extern "C" void MonoTouchAOTHelper__cctor_m4150697742 ();
extern "C" void MonoType_get_attributes_m1308912579 ();
extern "C" void MonoType_GetDefaultConstructor_m844404926 ();
extern "C" void MonoType_GetAttributeFlagsImpl_m3149937489 ();
extern "C" void MonoType_GetConstructorImpl_m3454079076 ();
extern "C" void MonoType_GetConstructors_internal_m342936590 ();
extern "C" void MonoType_GetConstructors_m1583588365 ();
extern "C" void MonoType_InternalGetEvent_m4167640615 ();
extern "C" void MonoType_GetEvent_m1659059560 ();
extern "C" void MonoType_GetField_m3686129833 ();
extern "C" void MonoType_GetFields_internal_m2059525432 ();
extern "C" void MonoType_GetFields_m4170770315 ();
extern "C" void MonoType_GetInterfaces_m534239473 ();
extern "C" void MonoType_GetMethodsByName_m3677183547 ();
extern "C" void MonoType_GetMethods_m3707161063 ();
extern "C" void MonoType_GetMethodImpl_m886303632 ();
extern "C" void MonoType_GetPropertiesByName_m225472389 ();
extern "C" void MonoType_GetPropertyImpl_m1936836523 ();
extern "C" void MonoType_HasElementTypeImpl_m3143972582 ();
extern "C" void MonoType_IsArrayImpl_m3271761795 ();
extern "C" void MonoType_IsByRefImpl_m2577121498 ();
extern "C" void MonoType_IsPointerImpl_m1487282369 ();
extern "C" void MonoType_IsPrimitiveImpl_m2286522716 ();
extern "C" void MonoType_IsSubclassOf_m3938220594 ();
extern "C" void MonoType_InvokeMember_m2094661425 ();
extern "C" void MonoType_GetElementType_m1981081120 ();
extern "C" void MonoType_get_UnderlyingSystemType_m2032206497 ();
extern "C" void MonoType_get_Assembly_m819582969 ();
extern "C" void MonoType_get_AssemblyQualifiedName_m208321748 ();
extern "C" void MonoType_getFullName_m132163685 ();
extern "C" void MonoType_get_BaseType_m3678567033 ();
extern "C" void MonoType_get_FullName_m3296291579 ();
extern "C" void MonoType_IsDefined_m2186296028 ();
extern "C" void MonoType_GetCustomAttributes_m3798502322 ();
extern "C" void MonoType_GetCustomAttributes_m3840448869 ();
extern "C" void MonoType_get_MemberType_m378348367 ();
extern "C" void MonoType_get_Name_m381307003 ();
extern "C" void MonoType_get_Namespace_m3829586408 ();
extern "C" void MonoType_get_Module_m2928378227 ();
extern "C" void MonoType_get_DeclaringType_m2734903002 ();
extern "C" void MonoType_get_ReflectedType_m3946481517 ();
extern "C" void MonoType_get_TypeHandle_m397824088 ();
extern "C" void MonoType_ToString_m1100936579 ();
extern "C" void MonoType_GetGenericArguments_m1335083072 ();
extern "C" void MonoType_get_ContainsGenericParameters_m6578403 ();
extern "C" void MonoType_get_IsGenericParameter_m1967179938 ();
extern "C" void MonoType_GetGenericTypeDefinition_m1435681011 ();
extern "C" void MonoType_CheckMethodSecurity_m1030836417 ();
extern "C" void MonoType_ReorderParamArrayArguments_m2524972069 ();
extern "C" void MonoTypeInfo__ctor_m1547907977 ();
extern "C" void MulticastDelegate_Equals_m64371161 ();
extern "C" void MulticastDelegate_GetHashCode_m2150985785 ();
extern "C" void MulticastDelegate_GetInvocationList_m1417804319 ();
extern "C" void MulticastDelegate_CombineImpl_m952498781 ();
extern "C" void MulticastDelegate_BaseEquals_m603046058 ();
extern "C" void MulticastDelegate_KPM_m2529894516 ();
extern "C" void MulticastDelegate_RemoveImpl_m2938661263 ();
extern "C" void MulticastNotSupportedException__ctor_m2664693452 ();
extern "C" void MulticastNotSupportedException__ctor_m747815933 ();
extern "C" void MulticastNotSupportedException__ctor_m199605124 ();
extern "C" void NonSerializedAttribute__ctor_m100300183 ();
extern "C" void NotImplementedException__ctor_m2570805563 ();
extern "C" void NotImplementedException__ctor_m595125831 ();
extern "C" void NotImplementedException__ctor_m2106393729 ();
extern "C" void NotSupportedException__ctor_m2647670222 ();
extern "C" void NotSupportedException__ctor_m4039520923 ();
extern "C" void NotSupportedException__ctor_m1240477304 ();
extern "C" void NullReferenceException__ctor_m2749761743 ();
extern "C" void NullReferenceException__ctor_m1443281920 ();
extern "C" void NullReferenceException__ctor_m172726003 ();
extern "C" void NumberFormatter__ctor_m882423236 ();
extern "C" void NumberFormatter__cctor_m3127417417 ();
extern "C" void NumberFormatter_GetFormatterTables_m2527842287 ();
extern "C" void NumberFormatter_GetTenPowerOf_m580279802 ();
extern "C" void NumberFormatter_InitDecHexDigits_m1515890855 ();
extern "C" void NumberFormatter_InitDecHexDigits_m4153623248 ();
extern "C" void NumberFormatter_InitDecHexDigits_m523693063 ();
extern "C" void NumberFormatter_FastToDecHex_m11094303 ();
extern "C" void NumberFormatter_ToDecHex_m1152169443 ();
extern "C" void NumberFormatter_FastDecHexLen_m2867570029 ();
extern "C" void NumberFormatter_DecHexLen_m1580324117 ();
extern "C" void NumberFormatter_DecHexLen_m3752403606 ();
extern "C" void NumberFormatter_ScaleOrder_m2995984485 ();
extern "C" void NumberFormatter_InitialFloatingPrecision_m180952382 ();
extern "C" void NumberFormatter_ParsePrecision_m81415732 ();
extern "C" void NumberFormatter_Init_m3482988837 ();
extern "C" void NumberFormatter_InitHex_m1660820330 ();
extern "C" void NumberFormatter_Init_m3611118981 ();
extern "C" void NumberFormatter_Init_m743203113 ();
extern "C" void NumberFormatter_Init_m2452070105 ();
extern "C" void NumberFormatter_Init_m1055011748 ();
extern "C" void NumberFormatter_Init_m4226945301 ();
extern "C" void NumberFormatter_Init_m2055243600 ();
extern "C" void NumberFormatter_ResetCharBuf_m3226660505 ();
extern "C" void NumberFormatter_Resize_m1832105174 ();
extern "C" void NumberFormatter_Append_m2953789120 ();
extern "C" void NumberFormatter_Append_m1387470406 ();
extern "C" void NumberFormatter_Append_m3558378514 ();
extern "C" void NumberFormatter_GetNumberFormatInstance_m3068474261 ();
extern "C" void NumberFormatter_set_CurrentCulture_m4133449828 ();
extern "C" void NumberFormatter_get_IntegerDigits_m47711755 ();
extern "C" void NumberFormatter_get_DecimalDigits_m2386777519 ();
extern "C" void NumberFormatter_get_IsFloatingSource_m228797787 ();
extern "C" void NumberFormatter_get_IsZero_m2438227288 ();
extern "C" void NumberFormatter_get_IsZeroInteger_m4115179637 ();
extern "C" void NumberFormatter_RoundPos_m679530010 ();
extern "C" void NumberFormatter_RoundDecimal_m639068567 ();
extern "C" void NumberFormatter_RoundBits_m3611922302 ();
extern "C" void NumberFormatter_RemoveTrailingZeros_m2551313845 ();
extern "C" void NumberFormatter_AddOneToDecHex_m3751539435 ();
extern "C" void NumberFormatter_AddOneToDecHex_m3021247525 ();
extern "C" void NumberFormatter_CountTrailingZeros_m35386702 ();
extern "C" void NumberFormatter_CountTrailingZeros_m2729623164 ();
extern "C" void NumberFormatter_GetInstance_m2657568193 ();
extern "C" void NumberFormatter_Release_m3231894907 ();
extern "C" void NumberFormatter_SetThreadCurrentCulture_m1936928120 ();
extern "C" void NumberFormatter_NumberToString_m1101975758 ();
extern "C" void NumberFormatter_NumberToString_m3052084606 ();
extern "C" void NumberFormatter_NumberToString_m2171216354 ();
extern "C" void NumberFormatter_NumberToString_m2879872021 ();
extern "C" void NumberFormatter_NumberToString_m1903630728 ();
extern "C" void NumberFormatter_NumberToString_m2443105739 ();
extern "C" void NumberFormatter_NumberToString_m3446587791 ();
extern "C" void NumberFormatter_NumberToString_m3799590785 ();
extern "C" void NumberFormatter_NumberToString_m1231813599 ();
extern "C" void NumberFormatter_NumberToString_m1232625936 ();
extern "C" void NumberFormatter_NumberToString_m1326808136 ();
extern "C" void NumberFormatter_NumberToString_m2193112985 ();
extern "C" void NumberFormatter_NumberToString_m3758122717 ();
extern "C" void NumberFormatter_NumberToString_m2131945648 ();
extern "C" void NumberFormatter_NumberToString_m1983366083 ();
extern "C" void NumberFormatter_NumberToString_m3542247186 ();
extern "C" void NumberFormatter_NumberToString_m2872185530 ();
extern "C" void NumberFormatter_FastIntegerToString_m3758483719 ();
extern "C" void NumberFormatter_IntegerToString_m792550392 ();
extern "C" void NumberFormatter_NumberToString_m2691816022 ();
extern "C" void NumberFormatter_FormatCurrency_m329550722 ();
extern "C" void NumberFormatter_FormatDecimal_m1064728487 ();
extern "C" void NumberFormatter_FormatHexadecimal_m2081806955 ();
extern "C" void NumberFormatter_FormatFixedPoint_m1540112410 ();
extern "C" void NumberFormatter_FormatRoundtrip_m1541160154 ();
extern "C" void NumberFormatter_FormatRoundtrip_m2836917638 ();
extern "C" void NumberFormatter_FormatGeneral_m3897558037 ();
extern "C" void NumberFormatter_FormatNumber_m210532640 ();
extern "C" void NumberFormatter_FormatPercent_m948264060 ();
extern "C" void NumberFormatter_FormatExponential_m3535129782 ();
extern "C" void NumberFormatter_FormatExponential_m4223907376 ();
extern "C" void NumberFormatter_FormatCustom_m2307374058 ();
extern "C" void NumberFormatter_ZeroTrimEnd_m3695493236 ();
extern "C" void NumberFormatter_IsZeroOnly_m301052596 ();
extern "C" void NumberFormatter_AppendNonNegativeNumber_m3842278575 ();
extern "C" void NumberFormatter_AppendIntegerString_m3975100308 ();
extern "C" void NumberFormatter_AppendIntegerString_m1574486672 ();
extern "C" void NumberFormatter_AppendDecimalString_m3929917867 ();
extern "C" void NumberFormatter_AppendDecimalString_m2615915907 ();
extern "C" void NumberFormatter_AppendIntegerStringWithGroupSeparator_m1886348349 ();
extern "C" void NumberFormatter_AppendExponent_m2078024189 ();
extern "C" void NumberFormatter_AppendOneDigit_m3331299856 ();
extern "C" void NumberFormatter_FastAppendDigits_m2298647756 ();
extern "C" void NumberFormatter_AppendDigits_m4058032180 ();
extern "C" void NumberFormatter_AppendDigits_m3796019938 ();
extern "C" void NumberFormatter_Multiply10_m3376371810 ();
extern "C" void NumberFormatter_Divide10_m2887365473 ();
extern "C" void NumberFormatter_GetClone_m3323601924 ();
extern "C" void CustomInfo__ctor_m1296891841 ();
extern "C" void CustomInfo_GetActiveSection_m861289010 ();
extern "C" void CustomInfo_Parse_m4259014900 ();
extern "C" void CustomInfo_Format_m1684327327 ();
extern "C" void Object__ctor_m3446193457 ();
extern "C" void Object_Equals_m3544058702 ();
extern "C" void Object_Equals_m899471607 ();
extern "C" void Object_Finalize_m1722074356 ();
extern "C" void Object_GetHashCode_m1725373185 ();
extern "C" void Object_GetType_m4087638284 ();
extern "C" void Object_MemberwiseClone_m1134924534 ();
extern "C" void Object_ToString_m1080586622 ();
extern "C" void Object_ReferenceEquals_m1923645570 ();
extern "C" void Object_InternalGetHashCode_m1442750202 ();
extern "C" void ObjectDisposedException__ctor_m781126761 ();
extern "C" void ObjectDisposedException__ctor_m4001580019 ();
extern "C" void ObjectDisposedException__ctor_m2784909353 ();
extern "C" void ObjectDisposedException_get_Message_m3136208161 ();
extern "C" void ObsoleteAttribute__ctor_m1879591032 ();
extern "C" void ObsoleteAttribute__ctor_m3274828507 ();
extern "C" void ObsoleteAttribute__ctor_m1055576761 ();
extern "C" void OperatingSystem__ctor_m983404079 ();
extern "C" void OperatingSystem_get_Platform_m3729450976 ();
extern "C" void OperatingSystem_Clone_m2635910214 ();
extern "C" void OperatingSystem_ToString_m97079520 ();
extern "C" void OrdinalComparer__ctor_m1284925701 ();
extern "C" void OrdinalComparer_Compare_m3031790537 ();
extern "C" void OrdinalComparer_Equals_m3606852489 ();
extern "C" void OrdinalComparer_GetHashCode_m4105287100 ();
extern "C" void OutOfMemoryException__ctor_m3233772617 ();
extern "C" void OutOfMemoryException__ctor_m2276193125 ();
extern "C" void OutOfMemoryException__ctor_m441591316 ();
extern "C" void OverflowException__ctor_m3668245243 ();
extern "C" void OverflowException__ctor_m1384916612 ();
extern "C" void OverflowException__ctor_m3735634925 ();
extern "C" void ParamArrayAttribute__ctor_m1696407647 ();
extern "C" void PlatformNotSupportedException__ctor_m3311290888 ();
extern "C" void PlatformNotSupportedException__ctor_m1475964536 ();
extern "C" void RankException__ctor_m2090556091 ();
extern "C" void RankException__ctor_m774219180 ();
extern "C" void RankException__ctor_m2523038546 ();
extern "C" void AmbiguousMatchException__ctor_m2375151799 ();
extern "C" void AmbiguousMatchException__ctor_m3181965201 ();
extern "C" void AmbiguousMatchException__ctor_m1115300120 ();
extern "C" void Assembly__ctor_m2503447432 ();
extern "C" void Assembly_get_code_base_m2040813 ();
extern "C" void Assembly_get_fullname_m1845803503 ();
extern "C" void Assembly_get_location_m329465134 ();
extern "C" void Assembly_GetCodeBase_m1589103529 ();
extern "C" void Assembly_get_FullName_m4007032079 ();
extern "C" void Assembly_get_Location_m2735854603 ();
extern "C" void Assembly_IsDefined_m3029835645 ();
extern "C" void Assembly_GetCustomAttributes_m77163733 ();
extern "C" void Assembly_GetManifestResourceInternal_m2271067159 ();
extern "C" void Assembly_GetTypes_m3955978312 ();
extern "C" void Assembly_GetTypes_m1975427444 ();
extern "C" void Assembly_GetType_m2280899582 ();
extern "C" void Assembly_GetType_m2764996290 ();
extern "C" void Assembly_InternalGetType_m4001481222 ();
extern "C" void Assembly_GetType_m3123598361 ();
extern "C" void Assembly_FillName_m829560512 ();
extern "C" void Assembly_GetName_m3194382543 ();
extern "C" void Assembly_GetName_m3130637144 ();
extern "C" void Assembly_UnprotectedGetName_m1763648659 ();
extern "C" void Assembly_ToString_m2040911384 ();
extern "C" void Assembly_Load_m1225372324 ();
extern "C" void Assembly_GetExecutingAssembly_m1654616147 ();
extern "C" void ResolveEventHolder__ctor_m341688792 ();
extern "C" void AssemblyCompanyAttribute__ctor_m1315167307 ();
extern "C" void AssemblyCopyrightAttribute__ctor_m4282510565 ();
extern "C" void AssemblyDefaultAliasAttribute__ctor_m393492554 ();
extern "C" void AssemblyDelaySignAttribute__ctor_m3802049740 ();
extern "C" void AssemblyDescriptionAttribute__ctor_m3502566481 ();
extern "C" void AssemblyFileVersionAttribute__ctor_m4281768118 ();
extern "C" void AssemblyInformationalVersionAttribute__ctor_m1415206487 ();
extern "C" void AssemblyKeyFileAttribute__ctor_m1870834162 ();
extern "C" void AssemblyName__ctor_m144750582 ();
extern "C" void AssemblyName__ctor_m1750531701 ();
extern "C" void AssemblyName_get_Name_m770412142 ();
extern "C" void AssemblyName_get_Flags_m1242999343 ();
extern "C" void AssemblyName_get_FullName_m2833406749 ();
extern "C" void AssemblyName_get_Version_m2538954018 ();
extern "C" void AssemblyName_set_Version_m1461954998 ();
extern "C" void AssemblyName_ToString_m2513744769 ();
extern "C" void AssemblyName_get_IsPublicKeyValid_m1649981173 ();
extern "C" void AssemblyName_InternalGetPublicKeyToken_m1069083009 ();
extern "C" void AssemblyName_ComputePublicKeyToken_m1615069588 ();
extern "C" void AssemblyName_SetPublicKey_m565542145 ();
extern "C" void AssemblyName_SetPublicKeyToken_m1864317684 ();
extern "C" void AssemblyName_Clone_m3092312624 ();
extern "C" void AssemblyName_OnDeserialization_m772831047 ();
extern "C" void AssemblyProductAttribute__ctor_m2217606340 ();
extern "C" void AssemblyTitleAttribute__ctor_m4091976430 ();
extern "C" void Binder__ctor_m2031026775 ();
extern "C" void Binder__cctor_m2352550279 ();
extern "C" void Binder_get_DefaultBinder_m2045865785 ();
extern "C" void Binder_ConvertArgs_m3763414446 ();
extern "C" void Binder_GetDerivedLevel_m1948575344 ();
extern "C" void Binder_FindMostDerivedMatch_m3497124822 ();
extern "C" void Default__ctor_m4273947660 ();
extern "C" void Default_BindToMethod_m2514609917 ();
extern "C" void Default_ReorderParameters_m4230856473 ();
extern "C" void Default_IsArrayAssignable_m1201555924 ();
extern "C" void Default_ChangeType_m2319690257 ();
extern "C" void Default_ReorderArgumentArray_m266338901 ();
extern "C" void Default_check_type_m377139779 ();
extern "C" void Default_check_arguments_m2892567715 ();
extern "C" void Default_SelectMethod_m1741803856 ();
extern "C" void Default_SelectMethod_m4094796858 ();
extern "C" void Default_GetBetterMethod_m372074302 ();
extern "C" void Default_CompareCloserType_m4137505321 ();
extern "C" void Default_SelectProperty_m3861348674 ();
extern "C" void Default_check_arguments_with_score_m680913026 ();
extern "C" void Default_check_type_with_score_m773560188 ();
extern "C" void ConstructorInfo__ctor_m1808498097 ();
extern "C" void ConstructorInfo__cctor_m2772849124 ();
extern "C" void ConstructorInfo_get_MemberType_m4000823216 ();
extern "C" void ConstructorInfo_Invoke_m4178483106 ();
extern "C" void CustomAttributeData__ctor_m1227935591 ();
extern "C" void CustomAttributeData_get_Constructor_m3981983546 ();
extern "C" void CustomAttributeData_get_ConstructorArguments_m3052531565 ();
extern "C" void CustomAttributeData_get_NamedArguments_m1329570588 ();
extern "C" void CustomAttributeData_GetCustomAttributes_m1773804050 ();
extern "C" void CustomAttributeData_GetCustomAttributes_m3510215830 ();
extern "C" void CustomAttributeData_GetCustomAttributes_m4140466514 ();
extern "C" void CustomAttributeData_GetCustomAttributes_m17259639 ();
extern "C" void CustomAttributeData_ToString_m3967580330 ();
extern "C" void CustomAttributeData_Equals_m3157954665 ();
extern "C" void CustomAttributeData_GetHashCode_m2489456201 ();
extern "C" void CustomAttributeNamedArgument_ToString_m1920457871_AdjustorThunk ();
extern "C" void CustomAttributeNamedArgument_Equals_m2602747387_AdjustorThunk ();
extern "C" void CustomAttributeNamedArgument_GetHashCode_m3609262192_AdjustorThunk ();
extern "C" void CustomAttributeTypedArgument_ToString_m3280070021_AdjustorThunk ();
extern "C" void CustomAttributeTypedArgument_Equals_m1480105269_AdjustorThunk ();
extern "C" void CustomAttributeTypedArgument_GetHashCode_m1148730416_AdjustorThunk ();
extern "C" void DefaultMemberAttribute__ctor_m3129895323 ();
extern "C" void DefaultMemberAttribute_get_MemberName_m1752551129 ();
extern "C" void AssemblyBuilder_get_Location_m2901826204 ();
extern "C" void AssemblyBuilder_GetTypes_m2732780010 ();
extern "C" void AssemblyBuilder_get_IsCompilerContext_m2403536670 ();
extern "C" void AssemblyBuilder_not_supported_m738697247 ();
extern "C" void AssemblyBuilder_UnprotectedGetName_m1473090115 ();
extern "C" void ConstructorBuilder__ctor_m3380435570 ();
extern "C" void ConstructorBuilder_get_CallingConvention_m416770567 ();
extern "C" void ConstructorBuilder_get_TypeBuilder_m472195973 ();
extern "C" void ConstructorBuilder_GetParameters_m2431579090 ();
extern "C" void ConstructorBuilder_GetParametersInternal_m472900066 ();
extern "C" void ConstructorBuilder_GetParameterCount_m1948820966 ();
extern "C" void ConstructorBuilder_Invoke_m279985127 ();
extern "C" void ConstructorBuilder_Invoke_m2799833077 ();
extern "C" void ConstructorBuilder_get_MethodHandle_m1748976910 ();
extern "C" void ConstructorBuilder_get_Attributes_m1398665057 ();
extern "C" void ConstructorBuilder_get_ReflectedType_m964892251 ();
extern "C" void ConstructorBuilder_get_DeclaringType_m2546517360 ();
extern "C" void ConstructorBuilder_get_Name_m3145274236 ();
extern "C" void ConstructorBuilder_IsDefined_m4089054386 ();
extern "C" void ConstructorBuilder_GetCustomAttributes_m1738281631 ();
extern "C" void ConstructorBuilder_GetCustomAttributes_m2389683246 ();
extern "C" void ConstructorBuilder_GetILGenerator_m2071550933 ();
extern "C" void ConstructorBuilder_GetILGenerator_m3080353190 ();
extern "C" void ConstructorBuilder_GetToken_m3335315133 ();
extern "C" void ConstructorBuilder_get_Module_m539373161 ();
extern "C" void ConstructorBuilder_ToString_m620390697 ();
extern "C" void ConstructorBuilder_fixup_m643618020 ();
extern "C" void ConstructorBuilder_get_next_table_index_m1541009121 ();
extern "C" void ConstructorBuilder_get_IsCompilerContext_m1774478654 ();
extern "C" void ConstructorBuilder_not_supported_m3101107144 ();
extern "C" void ConstructorBuilder_not_created_m343206691 ();
extern "C" void EnumBuilder_get_Assembly_m565086802 ();
extern "C" void EnumBuilder_get_AssemblyQualifiedName_m2614673987 ();
extern "C" void EnumBuilder_get_BaseType_m3289929135 ();
extern "C" void EnumBuilder_get_DeclaringType_m2594970671 ();
extern "C" void EnumBuilder_get_FullName_m4113314092 ();
extern "C" void EnumBuilder_get_Module_m997829433 ();
extern "C" void EnumBuilder_get_Name_m1181153314 ();
extern "C" void EnumBuilder_get_Namespace_m4211380120 ();
extern "C" void EnumBuilder_get_ReflectedType_m2656989504 ();
extern "C" void EnumBuilder_get_TypeHandle_m407547082 ();
extern "C" void EnumBuilder_get_UnderlyingSystemType_m3966542618 ();
extern "C" void EnumBuilder_GetAttributeFlagsImpl_m1076718463 ();
extern "C" void EnumBuilder_GetConstructorImpl_m561979540 ();
extern "C" void EnumBuilder_GetConstructors_m3085338377 ();
extern "C" void EnumBuilder_GetCustomAttributes_m274406178 ();
extern "C" void EnumBuilder_GetCustomAttributes_m1994953809 ();
extern "C" void EnumBuilder_GetElementType_m2227593022 ();
extern "C" void EnumBuilder_GetEvent_m694296250 ();
extern "C" void EnumBuilder_GetField_m1672069908 ();
extern "C" void EnumBuilder_GetFields_m1987286750 ();
extern "C" void EnumBuilder_GetInterfaces_m1858647194 ();
extern "C" void EnumBuilder_GetMethodImpl_m3445072901 ();
extern "C" void EnumBuilder_GetMethods_m977878939 ();
extern "C" void EnumBuilder_GetPropertyImpl_m2233824998 ();
extern "C" void EnumBuilder_HasElementTypeImpl_m2534796953 ();
extern "C" void EnumBuilder_InvokeMember_m2984316485 ();
extern "C" void EnumBuilder_IsArrayImpl_m2802054820 ();
extern "C" void EnumBuilder_IsByRefImpl_m1401428246 ();
extern "C" void EnumBuilder_IsPointerImpl_m118503420 ();
extern "C" void EnumBuilder_IsPrimitiveImpl_m3496261765 ();
extern "C" void EnumBuilder_IsValueTypeImpl_m1769410956 ();
extern "C" void EnumBuilder_IsDefined_m826867273 ();
extern "C" void EnumBuilder_CreateNotSupportedException_m861705916 ();
extern "C" void FieldBuilder_get_Attributes_m219928032 ();
extern "C" void FieldBuilder_get_DeclaringType_m1073567308 ();
extern "C" void FieldBuilder_get_FieldHandle_m1930105241 ();
extern "C" void FieldBuilder_get_FieldType_m2522660795 ();
extern "C" void FieldBuilder_get_Name_m3394280395 ();
extern "C" void FieldBuilder_get_ReflectedType_m3513560358 ();
extern "C" void FieldBuilder_GetCustomAttributes_m608818869 ();
extern "C" void FieldBuilder_GetCustomAttributes_m3255372514 ();
extern "C" void FieldBuilder_GetValue_m846706224 ();
extern "C" void FieldBuilder_IsDefined_m2962848206 ();
extern "C" void FieldBuilder_GetFieldOffset_m1720899837 ();
extern "C" void FieldBuilder_SetValue_m3566470566 ();
extern "C" void FieldBuilder_get_UMarshal_m2493034584 ();
extern "C" void FieldBuilder_CreateNotSupportedException_m1374657726 ();
extern "C" void FieldBuilder_get_Module_m1803345809 ();
extern "C" void GenericTypeParameterBuilder_IsSubclassOf_m2493609931 ();
extern "C" void GenericTypeParameterBuilder_GetAttributeFlagsImpl_m3990635610 ();
extern "C" void GenericTypeParameterBuilder_GetConstructorImpl_m753999115 ();
extern "C" void GenericTypeParameterBuilder_GetConstructors_m2634453164 ();
extern "C" void GenericTypeParameterBuilder_GetEvent_m1329508473 ();
extern "C" void GenericTypeParameterBuilder_GetField_m1889715417 ();
extern "C" void GenericTypeParameterBuilder_GetFields_m2744857607 ();
extern "C" void GenericTypeParameterBuilder_GetInterfaces_m3462466532 ();
extern "C" void GenericTypeParameterBuilder_GetMethods_m4219420128 ();
extern "C" void GenericTypeParameterBuilder_GetMethodImpl_m2980884362 ();
extern "C" void GenericTypeParameterBuilder_GetPropertyImpl_m3626565850 ();
extern "C" void GenericTypeParameterBuilder_HasElementTypeImpl_m150032775 ();
extern "C" void GenericTypeParameterBuilder_IsAssignableFrom_m1102625234 ();
extern "C" void GenericTypeParameterBuilder_IsInstanceOfType_m939262635 ();
extern "C" void GenericTypeParameterBuilder_IsArrayImpl_m4154256820 ();
extern "C" void GenericTypeParameterBuilder_IsByRefImpl_m3287374520 ();
extern "C" void GenericTypeParameterBuilder_IsPointerImpl_m1987073912 ();
extern "C" void GenericTypeParameterBuilder_IsPrimitiveImpl_m3285618665 ();
extern "C" void GenericTypeParameterBuilder_IsValueTypeImpl_m289531264 ();
extern "C" void GenericTypeParameterBuilder_InvokeMember_m1147727746 ();
extern "C" void GenericTypeParameterBuilder_GetElementType_m460425678 ();
extern "C" void GenericTypeParameterBuilder_get_UnderlyingSystemType_m1633586012 ();
extern "C" void GenericTypeParameterBuilder_get_Assembly_m4223435044 ();
extern "C" void GenericTypeParameterBuilder_get_AssemblyQualifiedName_m3052126742 ();
extern "C" void GenericTypeParameterBuilder_get_BaseType_m943684461 ();
extern "C" void GenericTypeParameterBuilder_get_FullName_m2984553906 ();
extern "C" void GenericTypeParameterBuilder_IsDefined_m3409969362 ();
extern "C" void GenericTypeParameterBuilder_GetCustomAttributes_m948809026 ();
extern "C" void GenericTypeParameterBuilder_GetCustomAttributes_m4219671456 ();
extern "C" void GenericTypeParameterBuilder_get_Name_m4223682894 ();
extern "C" void GenericTypeParameterBuilder_get_Namespace_m10098664 ();
extern "C" void GenericTypeParameterBuilder_get_Module_m2309239027 ();
extern "C" void GenericTypeParameterBuilder_get_DeclaringType_m481244658 ();
extern "C" void GenericTypeParameterBuilder_get_ReflectedType_m2468647034 ();
extern "C" void GenericTypeParameterBuilder_get_TypeHandle_m3153660848 ();
extern "C" void GenericTypeParameterBuilder_GetGenericArguments_m4175544120 ();
extern "C" void GenericTypeParameterBuilder_GetGenericTypeDefinition_m3873940944 ();
extern "C" void GenericTypeParameterBuilder_get_ContainsGenericParameters_m326624778 ();
extern "C" void GenericTypeParameterBuilder_get_IsGenericParameter_m302075495 ();
extern "C" void GenericTypeParameterBuilder_get_IsGenericType_m2448345782 ();
extern "C" void GenericTypeParameterBuilder_get_IsGenericTypeDefinition_m2522764535 ();
extern "C" void GenericTypeParameterBuilder_not_supported_m3311309618 ();
extern "C" void GenericTypeParameterBuilder_ToString_m205292240 ();
extern "C" void GenericTypeParameterBuilder_Equals_m3732267147 ();
extern "C" void GenericTypeParameterBuilder_GetHashCode_m3376400717 ();
extern "C" void GenericTypeParameterBuilder_MakeGenericType_m3812059673 ();
extern "C" void ILGenerator__ctor_m4276427634 ();
extern "C" void ILGenerator__cctor_m2225434446 ();
extern "C" void ILGenerator_add_token_fixup_m524277590 ();
extern "C" void ILGenerator_make_room_m2145189716 ();
extern "C" void ILGenerator_emit_int_m3878412281 ();
extern "C" void ILGenerator_ll_emit_m2330719544 ();
extern "C" void ILGenerator_Emit_m2237324484 ();
extern "C" void ILGenerator_Emit_m1154166167 ();
extern "C" void ILGenerator_label_fixup_m1333664849 ();
extern "C" void ILGenerator_Mono_GetCurrentOffset_m66508256 ();
extern "C" void MethodBuilder_get_ContainsGenericParameters_m2360427483 ();
extern "C" void MethodBuilder_get_MethodHandle_m3410394182 ();
extern "C" void MethodBuilder_get_ReturnType_m133520348 ();
extern "C" void MethodBuilder_get_ReflectedType_m3218931816 ();
extern "C" void MethodBuilder_get_DeclaringType_m3992555149 ();
extern "C" void MethodBuilder_get_Name_m2164650894 ();
extern "C" void MethodBuilder_get_Attributes_m557318860 ();
extern "C" void MethodBuilder_get_CallingConvention_m452128205 ();
extern "C" void MethodBuilder_GetBaseDefinition_m1236478928 ();
extern "C" void MethodBuilder_GetParameters_m2744380191 ();
extern "C" void MethodBuilder_GetParameterCount_m3686046676 ();
extern "C" void MethodBuilder_Invoke_m2609170815 ();
extern "C" void MethodBuilder_IsDefined_m277230573 ();
extern "C" void MethodBuilder_GetCustomAttributes_m4189138581 ();
extern "C" void MethodBuilder_GetCustomAttributes_m2985734895 ();
extern "C" void MethodBuilder_check_override_m4022641967 ();
extern "C" void MethodBuilder_fixup_m2055556419 ();
extern "C" void MethodBuilder_ToString_m1434929445 ();
extern "C" void MethodBuilder_Equals_m303455341 ();
extern "C" void MethodBuilder_GetHashCode_m4200125178 ();
extern "C" void MethodBuilder_get_next_table_index_m397105556 ();
extern "C" void MethodBuilder_NotSupported_m3832827859 ();
extern "C" void MethodBuilder_MakeGenericMethod_m1039446169 ();
extern "C" void MethodBuilder_get_IsGenericMethodDefinition_m1968682553 ();
extern "C" void MethodBuilder_get_IsGenericMethod_m2084698038 ();
extern "C" void MethodBuilder_GetGenericArguments_m3487349379 ();
extern "C" void MethodBuilder_get_Module_m3640104193 ();
extern "C" void MethodToken__ctor_m2157754371_AdjustorThunk ();
extern "C" void MethodToken__cctor_m963256158 ();
extern "C" void MethodToken_Equals_m848415105_AdjustorThunk ();
extern "C" void MethodToken_GetHashCode_m715310535_AdjustorThunk ();
extern "C" void MethodToken_get_Token_m1103007335_AdjustorThunk ();
extern "C" void ModuleBuilder__cctor_m4239403761 ();
extern "C" void ModuleBuilder_get_next_table_index_m3200252350 ();
extern "C" void ModuleBuilder_GetTypes_m1324970686 ();
extern "C" void ModuleBuilder_getToken_m3901973344 ();
extern "C" void ModuleBuilder_GetToken_m848611450 ();
extern "C" void ModuleBuilder_RegisterToken_m619657331 ();
extern "C" void ModuleBuilder_GetTokenGenerator_m1794001162 ();
extern "C" void ModuleBuilderTokenGenerator__ctor_m18313188 ();
extern "C" void ModuleBuilderTokenGenerator_GetToken_m1488753242 ();
extern "C" void OpCode__ctor_m1726332432_AdjustorThunk ();
extern "C" void OpCode_GetHashCode_m2246397448_AdjustorThunk ();
extern "C" void OpCode_Equals_m2955207306_AdjustorThunk ();
extern "C" void OpCode_ToString_m3107149020_AdjustorThunk ();
extern "C" void OpCode_get_Name_m3025990655_AdjustorThunk ();
extern "C" void OpCode_get_Size_m931123431_AdjustorThunk ();
extern "C" void OpCode_get_StackBehaviourPop_m2005666979_AdjustorThunk ();
extern "C" void OpCode_get_StackBehaviourPush_m3170452393_AdjustorThunk ();
extern "C" void OpCodeNames__cctor_m1400736817 ();
extern "C" void OpCodes__cctor_m4217172955 ();
extern "C" void ParameterBuilder_get_Attributes_m81931643 ();
extern "C" void ParameterBuilder_get_Name_m3516894523 ();
extern "C" void ParameterBuilder_get_Position_m395919688 ();
extern "C" void PropertyBuilder_get_Attributes_m2279594537 ();
extern "C" void PropertyBuilder_get_CanRead_m2409695466 ();
extern "C" void PropertyBuilder_get_CanWrite_m2802360124 ();
extern "C" void PropertyBuilder_get_DeclaringType_m1225045750 ();
extern "C" void PropertyBuilder_get_Name_m2642909525 ();
extern "C" void PropertyBuilder_get_PropertyType_m2926243180 ();
extern "C" void PropertyBuilder_get_ReflectedType_m3401682742 ();
extern "C" void PropertyBuilder_GetAccessors_m1237269319 ();
extern "C" void PropertyBuilder_GetCustomAttributes_m837390663 ();
extern "C" void PropertyBuilder_GetCustomAttributes_m387135554 ();
extern "C" void PropertyBuilder_GetGetMethod_m2399017824 ();
extern "C" void PropertyBuilder_GetIndexParameters_m2252963430 ();
extern "C" void PropertyBuilder_GetSetMethod_m3147416675 ();
extern "C" void PropertyBuilder_GetValue_m1434073069 ();
extern "C" void PropertyBuilder_GetValue_m3294302440 ();
extern "C" void PropertyBuilder_IsDefined_m3135037989 ();
extern "C" void PropertyBuilder_SetValue_m2153976900 ();
extern "C" void PropertyBuilder_SetValue_m1276310233 ();
extern "C" void PropertyBuilder_get_Module_m1043329146 ();
extern "C" void PropertyBuilder_not_supported_m1711805992 ();
extern "C" void TypeBuilder_GetAttributeFlagsImpl_m2598685731 ();
extern "C" void TypeBuilder_setup_internal_class_m128930060 ();
extern "C" void TypeBuilder_create_generic_class_m261174092 ();
extern "C" void TypeBuilder_get_Assembly_m223781556 ();
extern "C" void TypeBuilder_get_AssemblyQualifiedName_m4188959294 ();
extern "C" void TypeBuilder_get_BaseType_m2270822725 ();
extern "C" void TypeBuilder_get_DeclaringType_m2338804447 ();
extern "C" void TypeBuilder_get_UnderlyingSystemType_m65590438 ();
extern "C" void TypeBuilder_get_FullName_m3642975229 ();
extern "C" void TypeBuilder_get_Module_m1616194710 ();
extern "C" void TypeBuilder_get_Name_m982983587 ();
extern "C" void TypeBuilder_get_Namespace_m208582417 ();
extern "C" void TypeBuilder_get_ReflectedType_m438795595 ();
extern "C" void TypeBuilder_GetConstructorImpl_m3989283741 ();
extern "C" void TypeBuilder_IsDefined_m3003472617 ();
extern "C" void TypeBuilder_GetCustomAttributes_m3294702302 ();
extern "C" void TypeBuilder_GetCustomAttributes_m1756217549 ();
extern "C" void TypeBuilder_DefineConstructor_m1162809555 ();
extern "C" void TypeBuilder_DefineConstructor_m2260052946 ();
extern "C" void TypeBuilder_DefineDefaultConstructor_m3505124928 ();
extern "C" void TypeBuilder_create_runtime_class_m857078028 ();
extern "C" void TypeBuilder_is_nested_in_m2924220661 ();
extern "C" void TypeBuilder_has_ctor_method_m877392841 ();
extern "C" void TypeBuilder_CreateType_m3690229376 ();
extern "C" void TypeBuilder_GetConstructors_m55221742 ();
extern "C" void TypeBuilder_GetConstructorsInternal_m2910998681 ();
extern "C" void TypeBuilder_GetElementType_m420773727 ();
extern "C" void TypeBuilder_GetEvent_m696517673 ();
extern "C" void TypeBuilder_GetField_m394689338 ();
extern "C" void TypeBuilder_GetFields_m155238781 ();
extern "C" void TypeBuilder_GetInterfaces_m1636926525 ();
extern "C" void TypeBuilder_GetMethodsByName_m1570553827 ();
extern "C" void TypeBuilder_GetMethods_m2466610851 ();
extern "C" void TypeBuilder_GetMethodImpl_m2234341452 ();
extern "C" void TypeBuilder_GetPropertyImpl_m955190697 ();
extern "C" void TypeBuilder_HasElementTypeImpl_m3213799375 ();
extern "C" void TypeBuilder_InvokeMember_m1316938506 ();
extern "C" void TypeBuilder_IsArrayImpl_m244275415 ();
extern "C" void TypeBuilder_IsByRefImpl_m3935315218 ();
extern "C" void TypeBuilder_IsPointerImpl_m1316609598 ();
extern "C" void TypeBuilder_IsPrimitiveImpl_m2172525888 ();
extern "C" void TypeBuilder_IsValueTypeImpl_m683567880 ();
extern "C" void TypeBuilder_MakeGenericType_m2575213963 ();
extern "C" void TypeBuilder_get_TypeHandle_m3307184692 ();
extern "C" void TypeBuilder_SetParent_m3238504574 ();
extern "C" void TypeBuilder_get_next_table_index_m2276959831 ();
extern "C" void TypeBuilder_get_IsCompilerContext_m3599164304 ();
extern "C" void TypeBuilder_get_is_created_m4289290384 ();
extern "C" void TypeBuilder_not_supported_m3120789591 ();
extern "C" void TypeBuilder_check_not_created_m3504919281 ();
extern "C" void TypeBuilder_check_created_m102704719 ();
extern "C" void TypeBuilder_ToString_m3726280791 ();
extern "C" void TypeBuilder_IsAssignableFrom_m1068163903 ();
extern "C" void TypeBuilder_IsSubclassOf_m2364973326 ();
extern "C" void TypeBuilder_IsAssignableTo_m326333311 ();
extern "C" void TypeBuilder_GetGenericArguments_m4118364689 ();
extern "C" void TypeBuilder_GetGenericTypeDefinition_m2118699508 ();
extern "C" void TypeBuilder_get_ContainsGenericParameters_m204346989 ();
extern "C" void TypeBuilder_get_IsGenericParameter_m2020796253 ();
extern "C" void TypeBuilder_get_IsGenericTypeDefinition_m4127126086 ();
extern "C" void TypeBuilder_get_IsGenericType_m2610618835 ();
extern "C" void UnmanagedMarshal_ToMarshalAsAttribute_m1690298113 ();
extern "C" void EventInfo__ctor_m910283199 ();
extern "C" void EventInfo_get_EventHandlerType_m3748158604 ();
extern "C" void EventInfo_get_MemberType_m3944376584 ();
extern "C" void AddEventAdapter__ctor_m1612816262 ();
extern "C" void AddEventAdapter_Invoke_m2396517987 ();
extern "C" void AddEventAdapter_BeginInvoke_m206482786 ();
extern "C" void AddEventAdapter_EndInvoke_m3041319155 ();
extern "C" void FieldInfo__ctor_m499818762 ();
extern "C" void FieldInfo_get_MemberType_m582595557 ();
extern "C" void FieldInfo_get_IsLiteral_m2892539950 ();
extern "C" void FieldInfo_get_IsStatic_m2429808216 ();
extern "C" void FieldInfo_get_IsNotSerialized_m803345153 ();
extern "C" void FieldInfo_SetValue_m1839240114 ();
extern "C" void FieldInfo_GetFieldOffset_m1670647838 ();
extern "C" void FieldInfo_GetUnmanagedMarshal_m1651794009 ();
extern "C" void FieldInfo_get_UMarshal_m1787194349 ();
extern "C" void FieldInfo_GetPseudoCustomAttributes_m1553561892 ();
extern "C" void MemberFilter__ctor_m2871268628 ();
extern "C" void MemberFilter_Invoke_m4055074395 ();
extern "C" void MemberFilter_BeginInvoke_m2633951521 ();
extern "C" void MemberFilter_EndInvoke_m313368938 ();
extern "C" void MemberInfo__ctor_m738365750 ();
extern "C" void MemberInfo_get_Module_m1214320798 ();
extern "C" void MemberInfoSerializationHolder__ctor_m1793192197 ();
extern "C" void MemberInfoSerializationHolder_Serialize_m1394200861 ();
extern "C" void MemberInfoSerializationHolder_Serialize_m3986357309 ();
extern "C" void MemberInfoSerializationHolder_GetRealObject_m2142596729 ();
extern "C" void MethodBase__ctor_m2227469883 ();
extern "C" void MethodBase_GetParameterCount_m1185680379 ();
extern "C" void MethodBase_Invoke_m3351713316 ();
extern "C" void MethodBase_get_CallingConvention_m2543146775 ();
extern "C" void MethodBase_get_IsPublic_m3024571211 ();
extern "C" void MethodBase_get_IsStatic_m508456077 ();
extern "C" void MethodBase_get_IsVirtual_m487307940 ();
extern "C" void MethodBase_get_IsAbstract_m2773230650 ();
extern "C" void MethodBase_get_next_table_index_m3076840473 ();
extern "C" void MethodBase_GetGenericArguments_m3457019161 ();
extern "C" void MethodBase_get_ContainsGenericParameters_m2180266399 ();
extern "C" void MethodBase_get_IsGenericMethodDefinition_m2490092960 ();
extern "C" void MethodBase_get_IsGenericMethod_m3878297800 ();
extern "C" void MethodInfo__ctor_m789055071 ();
extern "C" void MethodInfo_get_MemberType_m4003411807 ();
extern "C" void MethodInfo_get_ReturnType_m945454792 ();
extern "C" void MethodInfo_MakeGenericMethod_m1848918275 ();
extern "C" void MethodInfo_GetGenericArguments_m254251600 ();
extern "C" void MethodInfo_get_IsGenericMethod_m667970124 ();
extern "C" void MethodInfo_get_IsGenericMethodDefinition_m4275495935 ();
extern "C" void MethodInfo_get_ContainsGenericParameters_m3315267095 ();
extern "C" void Missing__ctor_m4139730968 ();
extern "C" void Missing__cctor_m11351030 ();
extern "C" void Module__ctor_m1002678804 ();
extern "C" void Module__cctor_m125837104 ();
extern "C" void Module_get_Assembly_m765085037 ();
extern "C" void Module_GetCustomAttributes_m3382118587 ();
extern "C" void Module_InternalGetTypes_m3630318529 ();
extern "C" void Module_GetTypes_m2740322649 ();
extern "C" void Module_IsDefined_m1481658834 ();
extern "C" void Module_ToString_m4038533380 ();
extern "C" void Module_filter_by_type_name_m848320559 ();
extern "C" void Module_filter_by_type_name_ignore_case_m1757284250 ();
extern "C" void MonoCMethod__ctor_m2895813043 ();
extern "C" void MonoCMethod_GetParameters_m790514693 ();
extern "C" void MonoCMethod_InternalInvoke_m4055125447 ();
extern "C" void MonoCMethod_Invoke_m3006660650 ();
extern "C" void MonoCMethod_Invoke_m362486451 ();
extern "C" void MonoCMethod_get_MethodHandle_m3185544190 ();
extern "C" void MonoCMethod_get_Attributes_m3566437198 ();
extern "C" void MonoCMethod_get_CallingConvention_m3969797515 ();
extern "C" void MonoCMethod_get_ReflectedType_m4171658207 ();
extern "C" void MonoCMethod_get_DeclaringType_m671002914 ();
extern "C" void MonoCMethod_get_Name_m1727551819 ();
extern "C" void MonoCMethod_IsDefined_m2209325791 ();
extern "C" void MonoCMethod_GetCustomAttributes_m3478280530 ();
extern "C" void MonoCMethod_GetCustomAttributes_m1821120655 ();
extern "C" void MonoCMethod_ToString_m2243555642 ();
extern "C" void MonoEvent__ctor_m1226517236 ();
extern "C" void MonoEvent_get_Attributes_m2933370075 ();
extern "C" void MonoEvent_GetAddMethod_m4239159506 ();
extern "C" void MonoEvent_get_DeclaringType_m2267637479 ();
extern "C" void MonoEvent_get_ReflectedType_m1588732160 ();
extern "C" void MonoEvent_get_Name_m3169484882 ();
extern "C" void MonoEvent_ToString_m193961336 ();
extern "C" void MonoEvent_IsDefined_m2332101501 ();
extern "C" void MonoEvent_GetCustomAttributes_m3888560627 ();
extern "C" void MonoEvent_GetCustomAttributes_m1886948559 ();
extern "C" void MonoEventInfo_get_event_info_m99066771 ();
extern "C" void MonoEventInfo_GetEventInfo_m3621453625 ();
extern "C" void MonoField__ctor_m4070616665 ();
extern "C" void MonoField_get_Attributes_m1135577176 ();
extern "C" void MonoField_get_FieldHandle_m3720439131 ();
extern "C" void MonoField_get_FieldType_m2853174462 ();
extern "C" void MonoField_GetParentType_m4103890175 ();
extern "C" void MonoField_get_ReflectedType_m3972152971 ();
extern "C" void MonoField_get_DeclaringType_m1121037054 ();
extern "C" void MonoField_get_Name_m596017830 ();
extern "C" void MonoField_IsDefined_m3211024192 ();
extern "C" void MonoField_GetCustomAttributes_m947388369 ();
extern "C" void MonoField_GetCustomAttributes_m93951468 ();
extern "C" void MonoField_GetFieldOffset_m1899891901 ();
extern "C" void MonoField_GetValueInternal_m4270465109 ();
extern "C" void MonoField_GetValue_m3005604917 ();
extern "C" void MonoField_ToString_m1770889927 ();
extern "C" void MonoField_SetValueInternal_m990605174 ();
extern "C" void MonoField_SetValue_m326982726 ();
extern "C" void MonoField_CheckGeneric_m1090723102 ();
extern "C" void MonoGenericCMethod__ctor_m2398772755 ();
extern "C" void MonoGenericCMethod_get_ReflectedType_m1913009136 ();
extern "C" void MonoGenericMethod__ctor_m1702695577 ();
extern "C" void MonoGenericMethod_get_ReflectedType_m3851317345 ();
extern "C" void MonoMethod__ctor_m1390794006 ();
extern "C" void MonoMethod_get_name_m949262704 ();
extern "C" void MonoMethod_get_base_definition_m51581349 ();
extern "C" void MonoMethod_GetBaseDefinition_m249649666 ();
extern "C" void MonoMethod_get_ReturnType_m2613085097 ();
extern "C" void MonoMethod_GetParameters_m740194689 ();
extern "C" void MonoMethod_InternalInvoke_m542378430 ();
extern "C" void MonoMethod_Invoke_m465456020 ();
extern "C" void MonoMethod_get_MethodHandle_m2035895019 ();
extern "C" void MonoMethod_get_Attributes_m402091681 ();
extern "C" void MonoMethod_get_CallingConvention_m3515242436 ();
extern "C" void MonoMethod_get_ReflectedType_m777453553 ();
extern "C" void MonoMethod_get_DeclaringType_m3952561631 ();
extern "C" void MonoMethod_get_Name_m1963247321 ();
extern "C" void MonoMethod_IsDefined_m2816321146 ();
extern "C" void MonoMethod_GetCustomAttributes_m809032985 ();
extern "C" void MonoMethod_GetCustomAttributes_m951681242 ();
extern "C" void MonoMethod_GetDllImportAttribute_m1781219005 ();
extern "C" void MonoMethod_GetPseudoCustomAttributes_m3930606169 ();
extern "C" void MonoMethod_ShouldPrintFullName_m3366167260 ();
extern "C" void MonoMethod_ToString_m497053873 ();
extern "C" void MonoMethod_MakeGenericMethod_m786043505 ();
extern "C" void MonoMethod_MakeGenericMethod_impl_m2299472065 ();
extern "C" void MonoMethod_GetGenericArguments_m2628852535 ();
extern "C" void MonoMethod_get_IsGenericMethodDefinition_m3164616112 ();
extern "C" void MonoMethod_get_IsGenericMethod_m151038164 ();
extern "C" void MonoMethod_get_ContainsGenericParameters_m966846008 ();
extern "C" void MonoMethodInfo_get_method_info_m3673440566 ();
extern "C" void MonoMethodInfo_GetMethodInfo_m1772785548 ();
extern "C" void MonoMethodInfo_GetDeclaringType_m3185305103 ();
extern "C" void MonoMethodInfo_GetReturnType_m4027736285 ();
extern "C" void MonoMethodInfo_GetAttributes_m2985493472 ();
extern "C" void MonoMethodInfo_GetCallingConvention_m173365556 ();
extern "C" void MonoMethodInfo_get_parameter_info_m3111561253 ();
extern "C" void MonoMethodInfo_GetParametersInfo_m2428136279 ();
extern "C" void MonoProperty__ctor_m1812699322 ();
extern "C" void MonoProperty_CachePropertyInfo_m692654432 ();
extern "C" void MonoProperty_get_Attributes_m3368325028 ();
extern "C" void MonoProperty_get_CanRead_m1741287715 ();
extern "C" void MonoProperty_get_CanWrite_m2077594845 ();
extern "C" void MonoProperty_get_PropertyType_m694198643 ();
extern "C" void MonoProperty_get_ReflectedType_m2866745655 ();
extern "C" void MonoProperty_get_DeclaringType_m67029977 ();
extern "C" void MonoProperty_get_Name_m3968699540 ();
extern "C" void MonoProperty_GetAccessors_m2775370848 ();
extern "C" void MonoProperty_GetGetMethod_m4275470950 ();
extern "C" void MonoProperty_GetIndexParameters_m3544968989 ();
extern "C" void MonoProperty_GetSetMethod_m579251830 ();
extern "C" void MonoProperty_IsDefined_m3261039459 ();
extern "C" void MonoProperty_GetCustomAttributes_m563003902 ();
extern "C" void MonoProperty_GetCustomAttributes_m3357860972 ();
extern "C" void MonoProperty_CreateGetterDelegate_m3913706848 ();
extern "C" void MonoProperty_GetValue_m3818167631 ();
extern "C" void MonoProperty_GetValue_m264044638 ();
extern "C" void MonoProperty_SetValue_m236087792 ();
extern "C" void MonoProperty_ToString_m618904038 ();
extern "C" void MonoProperty_GetOptionalCustomModifiers_m3230855865 ();
extern "C" void MonoProperty_GetRequiredCustomModifiers_m2199146939 ();
extern "C" void MonoProperty_GetObjectData_m812207635 ();
extern "C" void GetterAdapter__ctor_m3057432183 ();
extern "C" void GetterAdapter_Invoke_m4219243567 ();
extern "C" void GetterAdapter_BeginInvoke_m1327405488 ();
extern "C" void GetterAdapter_EndInvoke_m1717440936 ();
extern "C" void MonoPropertyInfo_get_property_info_m2439647802 ();
extern "C" void MonoPropertyInfo_GetTypeModifiers_m366762850 ();
extern "C" void ParameterInfo__ctor_m73859067 ();
extern "C" void ParameterInfo__ctor_m3408505972 ();
extern "C" void ParameterInfo__ctor_m2243459569 ();
extern "C" void ParameterInfo_ToString_m4003100681 ();
extern "C" void ParameterInfo_get_ParameterType_m4183362478 ();
extern "C" void ParameterInfo_get_Attributes_m2978486817 ();
extern "C" void ParameterInfo_get_IsIn_m1843450765 ();
extern "C" void ParameterInfo_get_IsOptional_m1616592526 ();
extern "C" void ParameterInfo_get_IsOut_m4088738266 ();
extern "C" void ParameterInfo_get_IsRetval_m1400777994 ();
extern "C" void ParameterInfo_get_Member_m3384122622 ();
extern "C" void ParameterInfo_get_Name_m132787823 ();
extern "C" void ParameterInfo_get_Position_m4138316983 ();
extern "C" void ParameterInfo_GetCustomAttributes_m2182009624 ();
extern "C" void ParameterInfo_IsDefined_m871444883 ();
extern "C" void ParameterInfo_GetPseudoCustomAttributes_m3146711034 ();
extern "C" void Pointer__ctor_m48965945 ();
extern "C" void Pointer_Box_m1999707857 ();
extern "C" void PropertyInfo__ctor_m1356365561 ();
extern "C" void PropertyInfo_get_MemberType_m199392709 ();
extern "C" void PropertyInfo_GetValue_m1153076048 ();
extern "C" void PropertyInfo_SetValue_m3460960658 ();
extern "C" void PropertyInfo_GetOptionalCustomModifiers_m3574501446 ();
extern "C" void PropertyInfo_GetRequiredCustomModifiers_m2250392716 ();
extern "C" void StrongNameKeyPair__ctor_m1018690296 ();
extern "C" void StrongNameKeyPair_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m64041457 ();
extern "C" void TargetException__ctor_m473994234 ();
extern "C" void TargetException__ctor_m1461726186 ();
extern "C" void TargetException__ctor_m424977850 ();
extern "C" void TargetInvocationException__ctor_m3597313075 ();
extern "C" void TargetInvocationException__ctor_m3012971882 ();
extern "C" void TargetParameterCountException__ctor_m3690859315 ();
extern "C" void TargetParameterCountException__ctor_m1313217406 ();
extern "C" void TargetParameterCountException__ctor_m3120156734 ();
extern "C" void TypeFilter__ctor_m1878643257 ();
extern "C" void TypeFilter_Invoke_m3030351165 ();
extern "C" void TypeFilter_BeginInvoke_m3323671216 ();
extern "C" void TypeFilter_EndInvoke_m4094864109 ();
extern "C" void ResolveEventArgs__ctor_m93077578 ();
extern "C" void ResolveEventHandler__ctor_m3187065900 ();
extern "C" void ResolveEventHandler_Invoke_m1841633108 ();
extern "C" void ResolveEventHandler_BeginInvoke_m427632625 ();
extern "C" void ResolveEventHandler_EndInvoke_m4137252560 ();
extern "C" void NeutralResourcesLanguageAttribute__ctor_m1815010898 ();
extern "C" void ResourceManager__ctor_m3572514865 ();
extern "C" void ResourceManager__cctor_m3184761503 ();
extern "C" void ResourceReader__ctor_m3266399222 ();
extern "C" void ResourceReader__ctor_m1277725863 ();
extern "C" void ResourceReader_System_Collections_IEnumerable_GetEnumerator_m3288141321 ();
extern "C" void ResourceReader_System_IDisposable_Dispose_m1291672711 ();
extern "C" void ResourceReader_ReadHeaders_m3430125333 ();
extern "C" void ResourceReader_CreateResourceInfo_m3585089141 ();
extern "C" void ResourceReader_Read7BitEncodedInt_m1423726517 ();
extern "C" void ResourceReader_ReadValueVer2_m1431409153 ();
extern "C" void ResourceReader_ReadValueVer1_m2263386586 ();
extern "C" void ResourceReader_ReadNonPredefinedValue_m2833350557 ();
extern "C" void ResourceReader_LoadResourceValues_m423366887 ();
extern "C" void ResourceReader_Close_m1439213757 ();
extern "C" void ResourceReader_GetEnumerator_m1325379635 ();
extern "C" void ResourceReader_Dispose_m1468634256 ();
extern "C" void ResourceCacheItem__ctor_m909295167_AdjustorThunk ();
extern "C" void ResourceEnumerator__ctor_m1885558666 ();
extern "C" void ResourceEnumerator_get_Entry_m1054530742 ();
extern "C" void ResourceEnumerator_get_Key_m390468842 ();
extern "C" void ResourceEnumerator_get_Value_m1319048932 ();
extern "C" void ResourceEnumerator_get_Current_m284725302 ();
extern "C" void ResourceEnumerator_MoveNext_m1494841905 ();
extern "C" void ResourceEnumerator_Reset_m2674885615 ();
extern "C" void ResourceEnumerator_FillCache_m3498559180 ();
extern "C" void ResourceInfo__ctor_m297963896_AdjustorThunk ();
extern "C" void ResourceSet__ctor_m1489955567 ();
extern "C" void ResourceSet__ctor_m212014079 ();
extern "C" void ResourceSet__ctor_m1948322296 ();
extern "C" void ResourceSet__ctor_m909158515 ();
extern "C" void ResourceSet_System_Collections_IEnumerable_GetEnumerator_m3678908000 ();
extern "C" void ResourceSet_Dispose_m404370153 ();
extern "C" void ResourceSet_Dispose_m527036616 ();
extern "C" void ResourceSet_GetEnumerator_m2033896022 ();
extern "C" void ResourceSet_GetObjectInternal_m1910120986 ();
extern "C" void ResourceSet_GetObject_m1742653053 ();
extern "C" void ResourceSet_GetObject_m1921946804 ();
extern "C" void ResourceSet_ReadResources_m928108619 ();
extern "C" void RuntimeResourceSet__ctor_m3660273965 ();
extern "C" void RuntimeResourceSet__ctor_m4023448617 ();
extern "C" void RuntimeResourceSet__ctor_m2855192960 ();
extern "C" void RuntimeResourceSet_GetObject_m1369852933 ();
extern "C" void RuntimeResourceSet_GetObject_m373416841 ();
extern "C" void RuntimeResourceSet_CloneDisposableObjectIfPossible_m3448549430 ();
extern "C" void SatelliteContractVersionAttribute__ctor_m4270552604 ();
extern "C" void CompilationRelaxationsAttribute__ctor_m1638540989 ();
extern "C" void CompilerGeneratedAttribute__ctor_m903969540 ();
extern "C" void DecimalConstantAttribute__ctor_m579883733 ();
extern "C" void DefaultDependencyAttribute__ctor_m552191618 ();
extern "C" void FixedBufferAttribute__ctor_m2305776628 ();
extern "C" void FixedBufferAttribute_get_ElementType_m784922463 ();
extern "C" void FixedBufferAttribute_get_Length_m1055835580 ();
extern "C" void InternalsVisibleToAttribute__ctor_m2209294008 ();
extern "C" void RuntimeCompatibilityAttribute__ctor_m853651104 ();
extern "C" void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m4154715430 ();
extern "C" void RuntimeHelpers_InitializeArray_m751293544 ();
extern "C" void RuntimeHelpers_InitializeArray_m22859371 ();
extern "C" void RuntimeHelpers_get_OffsetToStringData_m2182702420 ();
extern "C" void StringFreezingAttribute__ctor_m1275687280 ();
extern "C" void CriticalFinalizerObject__ctor_m1083616025 ();
extern "C" void CriticalFinalizerObject_Finalize_m82872096 ();
extern "C" void ReliabilityContractAttribute__ctor_m2586077688 ();
extern "C" void ClassInterfaceAttribute__ctor_m2759037627 ();
extern "C" void ComDefaultInterfaceAttribute__ctor_m1937741735 ();
extern "C" void COMException__ctor_m314442114 ();
extern "C" void COMException__ctor_m2380647294 ();
extern "C" void COMException_ToString_m592648057 ();
extern "C" void ComImportAttribute__ctor_m2388436170 ();
extern "C" void ComVisibleAttribute__ctor_m29339631 ();
extern "C" void DispIdAttribute__ctor_m2341845790 ();
extern "C" void DllImportAttribute__ctor_m3723685963 ();
extern "C" void DllImportAttribute_get_Value_m4024763376 ();
extern "C" void ExternalException__ctor_m2380751802 ();
extern "C" void ExternalException__ctor_m1530163908 ();
extern "C" void FieldOffsetAttribute__ctor_m2729649564 ();
extern "C" void GCHandle__ctor_m2029694511_AdjustorThunk ();
extern "C" void GCHandle_get_IsAllocated_m1141928539_AdjustorThunk ();
extern "C" void GCHandle_get_Target_m342603595_AdjustorThunk ();
extern "C" void GCHandle_Alloc_m3489159186 ();
extern "C" void GCHandle_Free_m921347623_AdjustorThunk ();
extern "C" void GCHandle_GetTarget_m3327406846 ();
extern "C" void GCHandle_GetTargetHandle_m4231843731 ();
extern "C" void GCHandle_FreeHandle_m106505230 ();
extern "C" void GCHandle_Equals_m3225201818_AdjustorThunk ();
extern "C" void GCHandle_GetHashCode_m2365118290_AdjustorThunk ();
extern "C" void GuidAttribute__ctor_m612200210 ();
extern "C" void InAttribute__ctor_m228923551 ();
extern "C" void InterfaceTypeAttribute__ctor_m342415882 ();
extern "C" void Marshal__cctor_m2969155085 ();
extern "C" void Marshal_copy_from_unmanaged_m1563762894 ();
extern "C" void Marshal_Copy_m3898306904 ();
extern "C" void Marshal_Copy_m3204855539 ();
extern "C" void Marshal_ReadByte_m943398470 ();
extern "C" void Marshal_WriteByte_m199556577 ();
extern "C" void MarshalAsAttribute__ctor_m1960246711 ();
extern "C" void MarshalDirectiveException__ctor_m961324852 ();
extern "C" void MarshalDirectiveException__ctor_m4187866271 ();
extern "C" void OptionalAttribute__ctor_m2155356768 ();
extern "C" void OutAttribute__ctor_m1442270007 ();
extern "C" void PreserveSigAttribute__ctor_m2960960257 ();
extern "C" void SafeHandle__ctor_m20409610 ();
extern "C" void SafeHandle_Close_m106241507 ();
extern "C" void SafeHandle_DangerousAddRef_m3068760737 ();
extern "C" void SafeHandle_DangerousGetHandle_m987171088 ();
extern "C" void SafeHandle_DangerousRelease_m2343451392 ();
extern "C" void SafeHandle_Dispose_m3045814524 ();
extern "C" void SafeHandle_Dispose_m1778975602 ();
extern "C" void SafeHandle_SetHandle_m105306455 ();
extern "C" void SafeHandle_Finalize_m2145306507 ();
extern "C" void TypeLibImportClassAttribute__ctor_m1502349925 ();
extern "C" void TypeLibVersionAttribute__ctor_m3431410536 ();
extern "C" void UnmanagedFunctionPointerAttribute__ctor_m323663599 ();
extern "C" void ActivatedClientTypeEntry__ctor_m1819695246 ();
extern "C" void ActivatedClientTypeEntry_get_ApplicationUrl_m3873673798 ();
extern "C" void ActivatedClientTypeEntry_get_ContextAttributes_m3033756077 ();
extern "C" void ActivatedClientTypeEntry_get_ObjectType_m1878753871 ();
extern "C" void ActivatedClientTypeEntry_ToString_m2855942163 ();
extern "C" void ActivatedServiceTypeEntry__ctor_m3343564895 ();
extern "C" void ActivatedServiceTypeEntry_get_ObjectType_m1241835709 ();
extern "C" void ActivatedServiceTypeEntry_ToString_m4113167625 ();
extern "C" void ActivationServices_get_ConstructionActivator_m2979483328 ();
extern "C" void ActivationServices_CreateProxyFromAttributes_m1090915031 ();
extern "C" void ActivationServices_CreateConstructionCall_m2208125204 ();
extern "C" void ActivationServices_AllocateUninitializedClassInstance_m2119168546 ();
extern "C" void ActivationServices_EnableProxyActivation_m3920844405 ();
extern "C" void AppDomainLevelActivator__ctor_m255127195 ();
extern "C" void ConstructionLevelActivator__ctor_m2437471156 ();
extern "C" void ContextLevelActivator__ctor_m3317000986 ();
extern "C" void UrlAttribute_get_UrlValue_m2650265094 ();
extern "C" void UrlAttribute_Equals_m4185440562 ();
extern "C" void UrlAttribute_GetHashCode_m324081125 ();
extern "C" void UrlAttribute_GetPropertiesForNewContext_m3068588212 ();
extern "C" void UrlAttribute_IsContextOK_m4070904995 ();
extern "C" void ChannelData__ctor_m3849569811 ();
extern "C" void ChannelData_get_ServerProviders_m852758128 ();
extern "C" void ChannelData_get_ClientProviders_m1218169940 ();
extern "C" void ChannelData_get_CustomProperties_m3398830784 ();
extern "C" void ChannelData_CopyFrom_m2178894067 ();
extern "C" void ChannelInfo__ctor_m3311074949 ();
extern "C" void ChannelInfo_get_ChannelData_m1785884967 ();
extern "C" void ChannelServices__cctor_m3336674656 ();
extern "C" void ChannelServices_CreateClientChannelSinkChain_m1344844420 ();
extern "C" void ChannelServices_CreateClientChannelSinkChain_m3719553906 ();
extern "C" void ChannelServices_RegisterChannel_m1635994151 ();
extern "C" void ChannelServices_RegisterChannel_m394917815 ();
extern "C" void ChannelServices_RegisterChannelConfig_m4010407106 ();
extern "C" void ChannelServices_CreateProvider_m128710911 ();
extern "C" void ChannelServices_GetCurrentChannelInfo_m4213029739 ();
extern "C" void CrossAppDomainChannel__ctor_m4072987578 ();
extern "C" void CrossAppDomainChannel__cctor_m2249114501 ();
extern "C" void CrossAppDomainChannel_RegisterCrossAppDomainChannel_m4263628836 ();
extern "C" void CrossAppDomainChannel_get_ChannelName_m1336765459 ();
extern "C" void CrossAppDomainChannel_get_ChannelPriority_m2991035339 ();
extern "C" void CrossAppDomainChannel_get_ChannelData_m1576303390 ();
extern "C" void CrossAppDomainChannel_StartListening_m2105934298 ();
extern "C" void CrossAppDomainChannel_CreateMessageSink_m884114675 ();
extern "C" void CrossAppDomainData__ctor_m2872468782 ();
extern "C" void CrossAppDomainData_get_DomainID_m2201551882 ();
extern "C" void CrossAppDomainData_get_ProcessID_m2703215934 ();
extern "C" void CrossAppDomainSink__ctor_m3633578688 ();
extern "C" void CrossAppDomainSink__cctor_m4258184287 ();
extern "C" void CrossAppDomainSink_GetSink_m1074615250 ();
extern "C" void CrossAppDomainSink_get_TargetDomainId_m3724016769 ();
extern "C" void SinkProviderData__ctor_m441492975 ();
extern "C" void SinkProviderData_get_Children_m3216313090 ();
extern "C" void SinkProviderData_get_Properties_m1995188978 ();
extern "C" void ClientActivatedIdentity_GetServerObject_m709948078 ();
extern "C" void ClientIdentity__ctor_m4235777231 ();
extern "C" void ClientIdentity_get_ClientProxy_m630894956 ();
extern "C" void ClientIdentity_set_ClientProxy_m3756692934 ();
extern "C" void ClientIdentity_CreateObjRef_m606049357 ();
extern "C" void ClientIdentity_get_TargetUri_m32374781 ();
extern "C" void ConfigHandler__ctor_m3270116852 ();
extern "C" void ConfigHandler_ValidatePath_m3545750813 ();
extern "C" void ConfigHandler_CheckPath_m2304131255 ();
extern "C" void ConfigHandler_OnStartParsing_m430780344 ();
extern "C" void ConfigHandler_OnProcessingInstruction_m3787845528 ();
extern "C" void ConfigHandler_OnIgnorableWhitespace_m189830949 ();
extern "C" void ConfigHandler_OnStartElement_m112480001 ();
extern "C" void ConfigHandler_ParseElement_m3001301197 ();
extern "C" void ConfigHandler_OnEndElement_m1813247236 ();
extern "C" void ConfigHandler_ReadCustomProviderData_m2457933861 ();
extern "C" void ConfigHandler_ReadLifetine_m2261548993 ();
extern "C" void ConfigHandler_ParseTime_m2994502506 ();
extern "C" void ConfigHandler_ReadChannel_m1878857123 ();
extern "C" void ConfigHandler_ReadProvider_m2393547467 ();
extern "C" void ConfigHandler_ReadClientActivated_m2054330218 ();
extern "C" void ConfigHandler_ReadServiceActivated_m2908609747 ();
extern "C" void ConfigHandler_ReadClientWellKnown_m4188255355 ();
extern "C" void ConfigHandler_ReadServiceWellKnown_m3819404742 ();
extern "C" void ConfigHandler_ReadInteropXml_m1112646852 ();
extern "C" void ConfigHandler_ReadPreload_m2358582171 ();
extern "C" void ConfigHandler_GetNotNull_m2724870242 ();
extern "C" void ConfigHandler_ExtractAssembly_m3414662013 ();
extern "C" void ConfigHandler_OnChars_m2042791030 ();
extern "C" void ConfigHandler_OnEndParsing_m1466408695 ();
extern "C" void Context__ctor_m2358096070 ();
extern "C" void Context__cctor_m154458549 ();
extern "C" void Context_Finalize_m4146766630 ();
extern "C" void Context_get_DefaultContext_m3129763904 ();
extern "C" void Context_get_ContextID_m872742819 ();
extern "C" void Context_get_ContextProperties_m3483560853 ();
extern "C" void Context_get_IsDefaultContext_m1695961388 ();
extern "C" void Context_get_NeedsContextSink_m4170912896 ();
extern "C" void Context_RegisterDynamicProperty_m1394278564 ();
extern "C" void Context_UnregisterDynamicProperty_m3767934252 ();
extern "C" void Context_GetDynamicPropertyCollection_m4222645020 ();
extern "C" void Context_NotifyGlobalDynamicSinks_m3154063652 ();
extern "C" void Context_get_HasGlobalDynamicSinks_m2219829718 ();
extern "C" void Context_NotifyDynamicSinks_m1606948064 ();
extern "C" void Context_get_HasDynamicSinks_m166659476 ();
extern "C" void Context_get_HasExitSinks_m3583696172 ();
extern "C" void Context_GetProperty_m1437983912 ();
extern "C" void Context_SetProperty_m2564048121 ();
extern "C" void Context_Freeze_m2981637364 ();
extern "C" void Context_ToString_m3508388157 ();
extern "C" void Context_GetServerContextSinkChain_m41058259 ();
extern "C" void Context_GetClientContextSinkChain_m4126474018 ();
extern "C" void Context_CreateServerObjectSinkChain_m55752487 ();
extern "C" void Context_CreateEnvoySink_m3796096298 ();
extern "C" void Context_SwitchToContext_m683333002 ();
extern "C" void Context_CreateNewContext_m3406392012 ();
extern "C" void Context_DoCallBack_m1276336576 ();
extern "C" void Context_AllocateDataSlot_m2865612260 ();
extern "C" void Context_AllocateNamedDataSlot_m1602448103 ();
extern "C" void Context_FreeNamedDataSlot_m1826329033 ();
extern "C" void Context_GetData_m1620126795 ();
extern "C" void Context_GetNamedDataSlot_m985647519 ();
extern "C" void Context_SetData_m4157435612 ();
extern "C" void ContextAttribute__ctor_m3943029900 ();
extern "C" void ContextAttribute_get_Name_m2590719021 ();
extern "C" void ContextAttribute_Equals_m281624901 ();
extern "C" void ContextAttribute_Freeze_m3525118109 ();
extern "C" void ContextAttribute_GetHashCode_m1066514836 ();
extern "C" void ContextAttribute_GetPropertiesForNewContext_m3862978192 ();
extern "C" void ContextAttribute_IsContextOK_m4085172616 ();
extern "C" void ContextAttribute_IsNewContextOK_m2757508237 ();
extern "C" void ContextCallbackObject__ctor_m1057769426 ();
extern "C" void ContextCallbackObject_DoCallBack_m2365561453 ();
extern "C" void CrossContextChannel__ctor_m1492884516 ();
extern "C" void CrossContextDelegate__ctor_m2049533611 ();
extern "C" void CrossContextDelegate_Invoke_m3821704953 ();
extern "C" void CrossContextDelegate_BeginInvoke_m3957885468 ();
extern "C" void CrossContextDelegate_EndInvoke_m150737163 ();
extern "C" void DynamicPropertyCollection__ctor_m4108681380 ();
extern "C" void DynamicPropertyCollection_get_HasProperties_m1156612739 ();
extern "C" void DynamicPropertyCollection_RegisterDynamicProperty_m117639120 ();
extern "C" void DynamicPropertyCollection_UnregisterDynamicProperty_m3421206888 ();
extern "C" void DynamicPropertyCollection_NotifyMessage_m3140567461 ();
extern "C" void DynamicPropertyCollection_FindProperty_m1056171965 ();
extern "C" void DynamicPropertyReg__ctor_m1113221123 ();
extern "C" void SynchronizationAttribute__ctor_m548131118 ();
extern "C" void SynchronizationAttribute__ctor_m1680658202 ();
extern "C" void SynchronizationAttribute_set_Locked_m3609947079 ();
extern "C" void SynchronizationAttribute_ReleaseLock_m141792516 ();
extern "C" void SynchronizationAttribute_GetPropertiesForNewContext_m1138224242 ();
extern "C" void SynchronizationAttribute_GetClientContextSink_m1220088372 ();
extern "C" void SynchronizationAttribute_GetServerContextSink_m4260393252 ();
extern "C" void SynchronizationAttribute_IsContextOK_m3271333896 ();
extern "C" void SynchronizationAttribute_ExitContext_m3277325561 ();
extern "C" void SynchronizationAttribute_EnterContext_m970367295 ();
extern "C" void SynchronizedClientContextSink__ctor_m3191072250 ();
extern "C" void SynchronizedServerContextSink__ctor_m3863347475 ();
extern "C" void EnvoyInfo__ctor_m3541351662 ();
extern "C" void EnvoyInfo_get_EnvoySinks_m1163783610 ();
extern "C" void FormatterData__ctor_m1072025512 ();
extern "C" void Identity__ctor_m2977509058 ();
extern "C" void Identity_get_ChannelSink_m2802483952 ();
extern "C" void Identity_set_ChannelSink_m3209362843 ();
extern "C" void Identity_get_ObjectUri_m1367937621 ();
extern "C" void Identity_get_Disposed_m3822893581 ();
extern "C" void Identity_set_Disposed_m1142682364 ();
extern "C" void Identity_get_ClientDynamicProperties_m224584693 ();
extern "C" void Identity_get_ServerDynamicProperties_m4177201799 ();
extern "C" void InternalRemotingServices__cctor_m3528201971 ();
extern "C" void InternalRemotingServices_GetCachedSoapAttribute_m676971033 ();
extern "C" void LeaseManager__ctor_m2498920970 ();
extern "C" void LeaseManager_SetPollTime_m2675030242 ();
extern "C" void LeaseSink__ctor_m1931990775 ();
extern "C" void LifetimeServices__cctor_m1930292675 ();
extern "C" void LifetimeServices_set_LeaseManagerPollTime_m3041349301 ();
extern "C" void LifetimeServices_set_LeaseTime_m4027116677 ();
extern "C" void LifetimeServices_set_RenewOnCallTime_m1807637854 ();
extern "C" void LifetimeServices_set_SponsorshipTimeout_m4057621160 ();
extern "C" void ArgInfo__ctor_m4159974758 ();
extern "C" void ArgInfo_GetInOutArgs_m3181936204 ();
extern "C" void AsyncResult__ctor_m1801982601 ();
extern "C" void AsyncResult_get_AsyncState_m3143114484 ();
extern "C" void AsyncResult_get_AsyncWaitHandle_m529416838 ();
extern "C" void AsyncResult_get_CompletedSynchronously_m1821815858 ();
extern "C" void AsyncResult_get_IsCompleted_m3014818534 ();
extern "C" void AsyncResult_get_EndInvokeCalled_m2256230767 ();
extern "C" void AsyncResult_set_EndInvokeCalled_m1141779661 ();
extern "C" void AsyncResult_get_AsyncDelegate_m3035985913 ();
extern "C" void AsyncResult_get_NextSink_m3138582479 ();
extern "C" void AsyncResult_AsyncProcessMessage_m652707586 ();
extern "C" void AsyncResult_GetReplyMessage_m1358645350 ();
extern "C" void AsyncResult_SetMessageCtrl_m2108631088 ();
extern "C" void AsyncResult_SetCompletedSynchronously_m847983035 ();
extern "C" void AsyncResult_EndInvoke_m172785111 ();
extern "C" void AsyncResult_SyncProcessMessage_m3292051227 ();
extern "C" void AsyncResult_get_CallMessage_m220967056 ();
extern "C" void AsyncResult_set_CallMessage_m3225294145 ();
extern "C" void CallContextRemotingData__ctor_m1902173160 ();
extern "C" void CallContextRemotingData_Clone_m735009638 ();
extern "C" void ClientContextTerminatorSink__ctor_m813223473 ();
extern "C" void ConstructionCall__ctor_m1095431613 ();
extern "C" void ConstructionCall__ctor_m2101270576 ();
extern "C" void ConstructionCall_InitDictionary_m2564472215 ();
extern "C" void ConstructionCall_set_IsContextOk_m3644001000 ();
extern "C" void ConstructionCall_get_ActivationType_m2655866177 ();
extern "C" void ConstructionCall_get_ActivationTypeName_m2245962217 ();
extern "C" void ConstructionCall_get_Activator_m2794670563 ();
extern "C" void ConstructionCall_set_Activator_m894344884 ();
extern "C" void ConstructionCall_get_CallSiteActivationAttributes_m2423405968 ();
extern "C" void ConstructionCall_SetActivationAttributes_m3955865312 ();
extern "C" void ConstructionCall_get_ContextProperties_m3161236960 ();
extern "C" void ConstructionCall_InitMethodProperty_m2720989452 ();
extern "C" void ConstructionCall_get_Properties_m3202231363 ();
extern "C" void ConstructionCallDictionary__ctor_m1181026802 ();
extern "C" void ConstructionCallDictionary__cctor_m96353376 ();
extern "C" void ConstructionCallDictionary_GetMethodProperty_m2304169240 ();
extern "C" void ConstructionCallDictionary_SetMethodProperty_m552722302 ();
extern "C" void EnvoyTerminatorSink__ctor_m859706469 ();
extern "C" void EnvoyTerminatorSink__cctor_m2706474606 ();
extern "C" void Header__ctor_m3826209702 ();
extern "C" void Header__ctor_m1479211447 ();
extern "C" void Header__ctor_m243167164 ();
extern "C" void HeaderHandler__ctor_m1135507152 ();
extern "C" void HeaderHandler_Invoke_m3271043078 ();
extern "C" void HeaderHandler_BeginInvoke_m1603080229 ();
extern "C" void HeaderHandler_EndInvoke_m1281900991 ();
extern "C" void LogicalCallContext__ctor_m2817560434 ();
extern "C" void LogicalCallContext__ctor_m1410930345 ();
extern "C" void LogicalCallContext_SetData_m3089875600 ();
extern "C" void LogicalCallContext_Clone_m3866404782 ();
extern "C" void MethodCall__ctor_m884744044 ();
extern "C" void MethodCall__ctor_m1742009723 ();
extern "C" void MethodCall__ctor_m3497155135 ();
extern "C" void MethodCall_System_Runtime_Remoting_Messaging_IInternalMessage_set_Uri_m3838354409 ();
extern "C" void MethodCall_InitMethodProperty_m1459153458 ();
extern "C" void MethodCall_get_Args_m2716733821 ();
extern "C" void MethodCall_get_LogicalCallContext_m4057972022 ();
extern "C" void MethodCall_get_MethodBase_m154116763 ();
extern "C" void MethodCall_get_MethodName_m3514373525 ();
extern "C" void MethodCall_get_MethodSignature_m231438085 ();
extern "C" void MethodCall_get_Properties_m1466387397 ();
extern "C" void MethodCall_InitDictionary_m3585539223 ();
extern "C" void MethodCall_get_TypeName_m2708109644 ();
extern "C" void MethodCall_get_Uri_m3826928422 ();
extern "C" void MethodCall_set_Uri_m193158428 ();
extern "C" void MethodCall_Init_m4035917907 ();
extern "C" void MethodCall_ResolveMethod_m652866482 ();
extern "C" void MethodCall_CastTo_m3102252139 ();
extern "C" void MethodCall_GetTypeNameFromAssemblyQualifiedName_m1514492318 ();
extern "C" void MethodCall_get_GenericArguments_m2561245121 ();
extern "C" void MethodCallDictionary__ctor_m1706706460 ();
extern "C" void MethodCallDictionary__cctor_m14416868 ();
extern "C" void MethodDictionary__ctor_m2350470509 ();
extern "C" void MethodDictionary_System_Collections_IEnumerable_GetEnumerator_m2001247713 ();
extern "C" void MethodDictionary_set_MethodKeys_m2384280808 ();
extern "C" void MethodDictionary_AllocInternalProperties_m3279357487 ();
extern "C" void MethodDictionary_GetInternalProperties_m2405237846 ();
extern "C" void MethodDictionary_IsOverridenKey_m3505085623 ();
extern "C" void MethodDictionary_get_Item_m1955409235 ();
extern "C" void MethodDictionary_set_Item_m94361284 ();
extern "C" void MethodDictionary_GetMethodProperty_m4241101875 ();
extern "C" void MethodDictionary_SetMethodProperty_m148196079 ();
extern "C" void MethodDictionary_get_Values_m3928398530 ();
extern "C" void MethodDictionary_Add_m1681874019 ();
extern "C" void MethodDictionary_Remove_m2994596185 ();
extern "C" void MethodDictionary_get_Count_m705114034 ();
extern "C" void MethodDictionary_get_SyncRoot_m725319460 ();
extern "C" void MethodDictionary_CopyTo_m1829913959 ();
extern "C" void MethodDictionary_GetEnumerator_m3231228815 ();
extern "C" void DictionaryEnumerator__ctor_m3172994930 ();
extern "C" void DictionaryEnumerator_get_Current_m25673846 ();
extern "C" void DictionaryEnumerator_MoveNext_m2795532047 ();
extern "C" void DictionaryEnumerator_Reset_m739841666 ();
extern "C" void DictionaryEnumerator_get_Entry_m1836786350 ();
extern "C" void DictionaryEnumerator_get_Key_m1817568991 ();
extern "C" void DictionaryEnumerator_get_Value_m611140825 ();
extern "C" void MethodReturnDictionary__ctor_m1050817137 ();
extern "C" void MethodReturnDictionary__cctor_m2264198370 ();
extern "C" void MonoMethodMessage_get_Args_m2551945928 ();
extern "C" void MonoMethodMessage_get_LogicalCallContext_m1259579640 ();
extern "C" void MonoMethodMessage_get_MethodBase_m2960378457 ();
extern "C" void MonoMethodMessage_get_MethodName_m3948558933 ();
extern "C" void MonoMethodMessage_get_MethodSignature_m3074315531 ();
extern "C" void MonoMethodMessage_get_TypeName_m2135069458 ();
extern "C" void MonoMethodMessage_get_Uri_m148660560 ();
extern "C" void MonoMethodMessage_set_Uri_m4050398394 ();
extern "C" void MonoMethodMessage_get_Exception_m3516160438 ();
extern "C" void MonoMethodMessage_get_OutArgCount_m3426781770 ();
extern "C" void MonoMethodMessage_get_OutArgs_m3941705659 ();
extern "C" void MonoMethodMessage_get_ReturnValue_m68851356 ();
extern "C" void ObjRefSurrogate__ctor_m3417937042 ();
extern "C" void ObjRefSurrogate_SetObjectData_m4212308433 ();
extern "C" void RemotingSurrogate__ctor_m4190060429 ();
extern "C" void RemotingSurrogate_SetObjectData_m748538920 ();
extern "C" void RemotingSurrogateSelector__ctor_m3096157960 ();
extern "C" void RemotingSurrogateSelector__cctor_m1496344619 ();
extern "C" void RemotingSurrogateSelector_GetSurrogate_m245554266 ();
extern "C" void ReturnMessage__ctor_m87180958 ();
extern "C" void ReturnMessage__ctor_m1524202706 ();
extern "C" void ReturnMessage_System_Runtime_Remoting_Messaging_IInternalMessage_set_Uri_m1763195932 ();
extern "C" void ReturnMessage_get_Args_m1370072984 ();
extern "C" void ReturnMessage_get_LogicalCallContext_m2771752071 ();
extern "C" void ReturnMessage_get_MethodBase_m234441048 ();
extern "C" void ReturnMessage_get_MethodName_m4111095637 ();
extern "C" void ReturnMessage_get_MethodSignature_m3528607433 ();
extern "C" void ReturnMessage_get_Properties_m2680922070 ();
extern "C" void ReturnMessage_get_TypeName_m260498371 ();
extern "C" void ReturnMessage_get_Uri_m553618093 ();
extern "C" void ReturnMessage_set_Uri_m3542991743 ();
extern "C" void ReturnMessage_get_Exception_m2983507843 ();
extern "C" void ReturnMessage_get_OutArgs_m3497822294 ();
extern "C" void ReturnMessage_get_ReturnValue_m3326584974 ();
extern "C" void ServerContextTerminatorSink__ctor_m3664219034 ();
extern "C" void ServerObjectTerminatorSink__ctor_m3525098792 ();
extern "C" void StackBuilderSink__ctor_m64101652 ();
extern "C" void SoapAttribute__ctor_m518099028 ();
extern "C" void SoapAttribute_get_UseAttribute_m1707629656 ();
extern "C" void SoapAttribute_get_XmlNamespace_m1834257290 ();
extern "C" void SoapAttribute_SetReflectionObject_m4126214974 ();
extern "C" void SoapFieldAttribute__ctor_m3774082701 ();
extern "C" void SoapFieldAttribute_get_XmlElementName_m2058178999 ();
extern "C" void SoapFieldAttribute_IsInteropXmlElement_m3647752714 ();
extern "C" void SoapFieldAttribute_SetReflectionObject_m1610909618 ();
extern "C" void SoapMethodAttribute__ctor_m2287295344 ();
extern "C" void SoapMethodAttribute_get_UseAttribute_m2463822588 ();
extern "C" void SoapMethodAttribute_get_XmlNamespace_m1545598731 ();
extern "C" void SoapMethodAttribute_SetReflectionObject_m801625592 ();
extern "C" void SoapParameterAttribute__ctor_m3529185311 ();
extern "C" void SoapTypeAttribute__ctor_m1846542587 ();
extern "C" void SoapTypeAttribute_get_UseAttribute_m2333117870 ();
extern "C" void SoapTypeAttribute_get_XmlElementName_m3022579292 ();
extern "C" void SoapTypeAttribute_get_XmlNamespace_m1625614089 ();
extern "C" void SoapTypeAttribute_get_XmlTypeName_m90744578 ();
extern "C" void SoapTypeAttribute_get_XmlTypeNamespace_m4178519243 ();
extern "C" void SoapTypeAttribute_get_IsInteropXmlElement_m2959121094 ();
extern "C" void SoapTypeAttribute_get_IsInteropXmlType_m3762924927 ();
extern "C" void SoapTypeAttribute_SetReflectionObject_m3882901426 ();
extern "C" void ObjRef__ctor_m2786642265 ();
extern "C" void ObjRef__ctor_m3254298855 ();
extern "C" void ObjRef__cctor_m871093012 ();
extern "C" void ObjRef_get_IsReferenceToWellKnow_m2224633975 ();
extern "C" void ObjRef_get_ChannelInfo_m3336748132 ();
extern "C" void ObjRef_get_EnvoyInfo_m2326759189 ();
extern "C" void ObjRef_set_EnvoyInfo_m1158264358 ();
extern "C" void ObjRef_get_TypeInfo_m2948790669 ();
extern "C" void ObjRef_set_TypeInfo_m2537720298 ();
extern "C" void ObjRef_get_URI_m3387765577 ();
extern "C" void ObjRef_set_URI_m3186050253 ();
extern "C" void ObjRef_GetRealObject_m4233575813 ();
extern "C" void ObjRef_UpdateChannelInfo_m1580754822 ();
extern "C" void ObjRef_get_ServerType_m2919537993 ();
extern "C" void ProviderData__ctor_m1369545012 ();
extern "C" void ProviderData_CopyFrom_m3371583065 ();
extern "C" void ProxyAttribute_CreateInstance_m2180868281 ();
extern "C" void ProxyAttribute_CreateProxy_m3870939658 ();
extern "C" void ProxyAttribute_GetPropertiesForNewContext_m3937741509 ();
extern "C" void ProxyAttribute_IsContextOK_m4171219307 ();
extern "C" void RealProxy__ctor_m3226966492 ();
extern "C" void RealProxy__ctor_m2536418102 ();
extern "C" void RealProxy__ctor_m58325570 ();
extern "C" void RealProxy_InternalGetProxyType_m3275084161 ();
extern "C" void RealProxy_GetProxiedType_m162235532 ();
extern "C" void RealProxy_get_ObjectIdentity_m3187017140 ();
extern "C" void RealProxy_InternalGetTransparentProxy_m1418543192 ();
extern "C" void RealProxy_GetTransparentProxy_m2302031412 ();
extern "C" void RealProxy_SetTargetDomain_m290727647 ();
extern "C" void RemotingProxy__ctor_m34659926 ();
extern "C" void RemotingProxy__ctor_m3434603057 ();
extern "C" void RemotingProxy__cctor_m2837767763 ();
extern "C" void RemotingProxy_get_TypeName_m2112259464 ();
extern "C" void RemotingProxy_Finalize_m525159468 ();
extern "C" void RemotingConfiguration__cctor_m1279995388 ();
extern "C" void RemotingConfiguration_get_ApplicationName_m2327332192 ();
extern "C" void RemotingConfiguration_set_ApplicationName_m3067372830 ();
extern "C" void RemotingConfiguration_get_ProcessId_m809639351 ();
extern "C" void RemotingConfiguration_LoadDefaultDelayedChannels_m4234959241 ();
extern "C" void RemotingConfiguration_IsRemotelyActivatedClientType_m3273590860 ();
extern "C" void RemotingConfiguration_RegisterActivatedClientType_m199437308 ();
extern "C" void RemotingConfiguration_RegisterActivatedServiceType_m787552533 ();
extern "C" void RemotingConfiguration_RegisterWellKnownClientType_m1023771625 ();
extern "C" void RemotingConfiguration_RegisterWellKnownServiceType_m3537794122 ();
extern "C" void RemotingConfiguration_RegisterChannelTemplate_m542601404 ();
extern "C" void RemotingConfiguration_RegisterClientProviderTemplate_m945610249 ();
extern "C" void RemotingConfiguration_RegisterServerProviderTemplate_m304276082 ();
extern "C" void RemotingConfiguration_RegisterChannels_m1399160752 ();
extern "C" void RemotingConfiguration_RegisterTypes_m931646882 ();
extern "C" void RemotingConfiguration_SetCustomErrorsMode_m4130863040 ();
extern "C" void RemotingException__ctor_m293416780 ();
extern "C" void RemotingException__ctor_m1492161056 ();
extern "C" void RemotingException__ctor_m1971504521 ();
extern "C" void RemotingException__ctor_m3202151879 ();
extern "C" void RemotingServices__cctor_m1277049829 ();
extern "C" void RemotingServices_GetVirtualMethod_m2518771061 ();
extern "C" void RemotingServices_IsTransparentProxy_m744747325 ();
extern "C" void RemotingServices_GetServerTypeForUri_m550359732 ();
extern "C" void RemotingServices_Unmarshal_m1289686908 ();
extern "C" void RemotingServices_Unmarshal_m778773009 ();
extern "C" void RemotingServices_GetRealProxy_m256026011 ();
extern "C" void RemotingServices_GetMethodBaseFromMethodMessage_m2395724458 ();
extern "C" void RemotingServices_GetMethodBaseFromName_m2424336723 ();
extern "C" void RemotingServices_FindInterfaceMethod_m1015202004 ();
extern "C" void RemotingServices_CreateClientProxy_m107493679 ();
extern "C" void RemotingServices_CreateClientProxy_m400991789 ();
extern "C" void RemotingServices_CreateClientProxyForContextBound_m203650324 ();
extern "C" void RemotingServices_GetIdentityForUri_m3222906492 ();
extern "C" void RemotingServices_RemoveAppNameFromUri_m618880628 ();
extern "C" void RemotingServices_GetOrCreateClientIdentity_m2784245793 ();
extern "C" void RemotingServices_GetClientChannelSinkChain_m457476899 ();
extern "C" void RemotingServices_CreateWellKnownServerIdentity_m3649196724 ();
extern "C" void RemotingServices_RegisterServerIdentity_m834217415 ();
extern "C" void RemotingServices_GetProxyForRemoteObject_m3819863274 ();
extern "C" void RemotingServices_GetRemoteObject_m1750813135 ();
extern "C" void RemotingServices_RegisterInternalChannels_m275911897 ();
extern "C" void RemotingServices_DisposeIdentity_m323595472 ();
extern "C" void RemotingServices_GetNormalizedUri_m255324727 ();
extern "C" void ServerIdentity__ctor_m1102360821 ();
extern "C" void ServerIdentity_get_ObjectType_m512804537 ();
extern "C" void ServerIdentity_CreateObjRef_m2011463462 ();
extern "C" void TrackingServices__cctor_m2429798832 ();
extern "C" void TrackingServices_NotifyUnmarshaledObject_m1471860047 ();
extern "C" void SingleCallIdentity__ctor_m1736848410 ();
extern "C" void SingletonIdentity__ctor_m1865259725 ();
extern "C" void SoapServices__cctor_m2457007482 ();
extern "C" void SoapServices_get_XmlNsForClrTypeWithAssembly_m2612822279 ();
extern "C" void SoapServices_get_XmlNsForClrTypeWithNs_m3163533917 ();
extern "C" void SoapServices_get_XmlNsForClrTypeWithNsAndAssembly_m2640095658 ();
extern "C" void SoapServices_CodeXmlNamespaceForClrTypeNamespace_m4133277412 ();
extern "C" void SoapServices_GetNameKey_m1092011777 ();
extern "C" void SoapServices_GetAssemblyName_m2829699794 ();
extern "C" void SoapServices_GetXmlElementForInteropType_m1640193632 ();
extern "C" void SoapServices_GetXmlNamespaceForMethodCall_m1056000020 ();
extern "C" void SoapServices_GetXmlNamespaceForMethodResponse_m2553670593 ();
extern "C" void SoapServices_GetXmlTypeForInteropType_m1005719194 ();
extern "C" void SoapServices_PreLoad_m6189216 ();
extern "C" void SoapServices_PreLoad_m2104060828 ();
extern "C" void SoapServices_RegisterInteropXmlElement_m908353777 ();
extern "C" void SoapServices_RegisterInteropXmlType_m2453065344 ();
extern "C" void SoapServices_EncodeNs_m2632503700 ();
extern "C" void TypeInfo__ctor_m2715261247 ();
extern "C" void TypeEntry__ctor_m3575940353 ();
extern "C" void TypeEntry_get_AssemblyName_m3054311022 ();
extern "C" void TypeEntry_set_AssemblyName_m499963479 ();
extern "C" void TypeEntry_get_TypeName_m1232453208 ();
extern "C" void TypeEntry_set_TypeName_m1952212701 ();
extern "C" void TypeInfo__ctor_m2364486315 ();
extern "C" void TypeInfo_get_TypeName_m2863368978 ();
extern "C" void WellKnownClientTypeEntry__ctor_m1668350696 ();
extern "C" void WellKnownClientTypeEntry_get_ApplicationUrl_m21167485 ();
extern "C" void WellKnownClientTypeEntry_get_ObjectType_m4039108672 ();
extern "C" void WellKnownClientTypeEntry_get_ObjectUrl_m544692699 ();
extern "C" void WellKnownClientTypeEntry_ToString_m3394858157 ();
extern "C" void WellKnownServiceTypeEntry__ctor_m2293108517 ();
extern "C" void WellKnownServiceTypeEntry_get_Mode_m2923959592 ();
extern "C" void WellKnownServiceTypeEntry_get_ObjectType_m1746130983 ();
extern "C" void WellKnownServiceTypeEntry_get_ObjectUri_m3935359109 ();
extern "C" void WellKnownServiceTypeEntry_ToString_m1431742318 ();
extern "C" void ArrayFixupRecord__ctor_m3311888015 ();
extern "C" void ArrayFixupRecord_FixupImpl_m1513658972 ();
extern "C" void BaseFixupRecord__ctor_m3835164596 ();
extern "C" void BaseFixupRecord_DoFixup_m1851697309 ();
extern "C" void DelayedFixupRecord__ctor_m2181884632 ();
extern "C" void DelayedFixupRecord_FixupImpl_m401936948 ();
extern "C" void FixupRecord__ctor_m3656271538 ();
extern "C" void FixupRecord_FixupImpl_m797154939 ();
extern "C" void FormatterConverter__ctor_m3430055030 ();
extern "C" void FormatterConverter_Convert_m3961524211 ();
extern "C" void FormatterConverter_ToBoolean_m1798860026 ();
extern "C" void FormatterConverter_ToInt16_m1177479242 ();
extern "C" void FormatterConverter_ToInt32_m456787332 ();
extern "C" void FormatterConverter_ToInt64_m4130265265 ();
extern "C" void FormatterConverter_ToString_m3532187504 ();
extern "C" void BinaryCommon__cctor_m936375461 ();
extern "C" void BinaryCommon_IsPrimitive_m2659553809 ();
extern "C" void BinaryCommon_GetTypeFromCode_m1100058929 ();
extern "C" void BinaryCommon_SwapBytes_m1969176942 ();
extern "C" void BinaryFormatter__ctor_m3409016867 ();
extern "C" void BinaryFormatter__ctor_m4243617903 ();
extern "C" void BinaryFormatter_get_DefaultSurrogateSelector_m1118519797 ();
extern "C" void BinaryFormatter_set_AssemblyFormat_m2806325422 ();
extern "C" void BinaryFormatter_get_Binder_m2353889501 ();
extern "C" void BinaryFormatter_get_Context_m1683691927 ();
extern "C" void BinaryFormatter_get_SurrogateSelector_m2311519614 ();
extern "C" void BinaryFormatter_get_FilterLevel_m686970985 ();
extern "C" void BinaryFormatter_Deserialize_m2376450417 ();
extern "C" void BinaryFormatter_NoCheckDeserialize_m1340843907 ();
extern "C" void BinaryFormatter_ReadBinaryHeader_m683624077 ();
extern "C" void MessageFormatter_ReadMethodCall_m1820326579 ();
extern "C" void MessageFormatter_ReadMethodResponse_m2419236021 ();
extern "C" void ObjectReader__ctor_m653516426 ();
extern "C" void ObjectReader_ReadObjectGraph_m3185940113 ();
extern "C" void ObjectReader_ReadObjectGraph_m2086328326 ();
extern "C" void ObjectReader_ReadNextObject_m1157177315 ();
extern "C" void ObjectReader_ReadNextObject_m3256368101 ();
extern "C" void ObjectReader_get_CurrentObject_m821465781 ();
extern "C" void ObjectReader_ReadObject_m3369997476 ();
extern "C" void ObjectReader_ReadAssembly_m4242168269 ();
extern "C" void ObjectReader_ReadObjectInstance_m1968249236 ();
extern "C" void ObjectReader_ReadRefTypeObjectInstance_m2451797494 ();
extern "C" void ObjectReader_ReadObjectContent_m3969357595 ();
extern "C" void ObjectReader_RegisterObject_m1843472302 ();
extern "C" void ObjectReader_ReadStringIntance_m252569550 ();
extern "C" void ObjectReader_ReadGenericArray_m1531022411 ();
extern "C" void ObjectReader_ReadBoxedPrimitiveTypeValue_m3484375627 ();
extern "C" void ObjectReader_ReadArrayOfPrimitiveType_m3868850930 ();
extern "C" void ObjectReader_BlockRead_m1857167249 ();
extern "C" void ObjectReader_ReadArrayOfObject_m596245004 ();
extern "C" void ObjectReader_ReadArrayOfString_m839118322 ();
extern "C" void ObjectReader_ReadSimpleArray_m1425418341 ();
extern "C" void ObjectReader_ReadTypeMetadata_m339584867 ();
extern "C" void ObjectReader_ReadValue_m4196722483 ();
extern "C" void ObjectReader_SetObjectValue_m1775786951 ();
extern "C" void ObjectReader_RecordFixup_m1730193250 ();
extern "C" void ObjectReader_GetDeserializationType_m1430761280 ();
extern "C" void ObjectReader_ReadType_m2493401618 ();
extern "C" void ObjectReader_ReadPrimitiveTypeValue_m3966667252 ();
extern "C" void ArrayNullFiller__ctor_m636923149 ();
extern "C" void TypeMetadata__ctor_m327828241 ();
extern "C" void FormatterServices_GetUninitializedObject_m666738276 ();
extern "C" void FormatterServices_GetSafeUninitializedObject_m1769512035 ();
extern "C" void MultiArrayFixupRecord__ctor_m2343861941 ();
extern "C" void MultiArrayFixupRecord_FixupImpl_m772772236 ();
extern "C" void ObjectManager__ctor_m2047177295 ();
extern "C" void ObjectManager_DoFixups_m4157975760 ();
extern "C" void ObjectManager_GetObjectRecord_m4081035814 ();
extern "C" void ObjectManager_GetObject_m3854971286 ();
extern "C" void ObjectManager_RaiseDeserializationEvent_m372858428 ();
extern "C" void ObjectManager_RaiseOnDeserializingEvent_m3159578434 ();
extern "C" void ObjectManager_RaiseOnDeserializedEvent_m1878246435 ();
extern "C" void ObjectManager_AddFixup_m553412757 ();
extern "C" void ObjectManager_RecordArrayElementFixup_m1940367603 ();
extern "C" void ObjectManager_RecordArrayElementFixup_m665635846 ();
extern "C" void ObjectManager_RecordDelayedFixup_m976599896 ();
extern "C" void ObjectManager_RecordFixup_m2480262735 ();
extern "C" void ObjectManager_RegisterObjectInternal_m1911644234 ();
extern "C" void ObjectManager_RegisterObject_m344094489 ();
extern "C" void ObjectRecord__ctor_m1406678827 ();
extern "C" void ObjectRecord_SetMemberValue_m4070346018 ();
extern "C" void ObjectRecord_SetArrayValue_m1003342309 ();
extern "C" void ObjectRecord_SetMemberValue_m1871413147 ();
extern "C" void ObjectRecord_get_IsInstanceReady_m1438161326 ();
extern "C" void ObjectRecord_get_IsUnsolvedObjectReference_m467999924 ();
extern "C" void ObjectRecord_get_IsRegistered_m823496992 ();
extern "C" void ObjectRecord_DoFixups_m3951361198 ();
extern "C" void ObjectRecord_RemoveFixup_m2600767340 ();
extern "C" void ObjectRecord_UnchainFixup_m1537704317 ();
extern "C" void ObjectRecord_ChainFixup_m1047594955 ();
extern "C" void ObjectRecord_LoadData_m4129923566 ();
extern "C" void ObjectRecord_get_HasPendingFixups_m3517138698 ();
extern "C" void SerializationBinder__ctor_m807311882 ();
extern "C" void SerializationCallbacks__ctor_m3030529948 ();
extern "C" void SerializationCallbacks__cctor_m3792349727 ();
extern "C" void SerializationCallbacks_get_HasDeserializedCallbacks_m2204430898 ();
extern "C" void SerializationCallbacks_GetMethodsByAttribute_m1467320038 ();
extern "C" void SerializationCallbacks_Invoke_m1529209864 ();
extern "C" void SerializationCallbacks_RaiseOnDeserializing_m1899694156 ();
extern "C" void SerializationCallbacks_RaiseOnDeserialized_m1392288165 ();
extern "C" void SerializationCallbacks_GetSerializationCallbacks_m188332064 ();
extern "C" void CallbackHandler__ctor_m3260575867 ();
extern "C" void CallbackHandler_Invoke_m3213427438 ();
extern "C" void CallbackHandler_BeginInvoke_m3425019071 ();
extern "C" void CallbackHandler_EndInvoke_m3352431872 ();
extern "C" void SerializationEntry__ctor_m69151158_AdjustorThunk ();
extern "C" void SerializationEntry_get_Name_m122287639_AdjustorThunk ();
extern "C" void SerializationEntry_get_Value_m3922915124_AdjustorThunk ();
extern "C" void SerializationException__ctor_m2606810985 ();
extern "C" void SerializationException__ctor_m706051698 ();
extern "C" void SerializationException__ctor_m244914060 ();
extern "C" void SerializationInfo__ctor_m283007395 ();
extern "C" void SerializationInfo_AddValue_m355239916 ();
extern "C" void SerializationInfo_GetValue_m1114212844 ();
extern "C" void SerializationInfo_SetType_m3163138465 ();
extern "C" void SerializationInfo_GetEnumerator_m461080135 ();
extern "C" void SerializationInfo_AddValue_m2159499637 ();
extern "C" void SerializationInfo_GetBoolean_m500417771 ();
extern "C" void SerializationInfo_GetInt16_m3836928717 ();
extern "C" void SerializationInfo_GetInt32_m27214234 ();
extern "C" void SerializationInfo_GetInt64_m4002248073 ();
extern "C" void SerializationInfo_GetString_m2598891044 ();
extern "C" void SerializationInfoEnumerator__ctor_m490872531 ();
extern "C" void SerializationInfoEnumerator_System_Collections_IEnumerator_get_Current_m2134644067 ();
extern "C" void SerializationInfoEnumerator_get_Current_m2556573355 ();
extern "C" void SerializationInfoEnumerator_get_Name_m2267113070 ();
extern "C" void SerializationInfoEnumerator_get_Value_m3733105889 ();
extern "C" void SerializationInfoEnumerator_MoveNext_m3795119086 ();
extern "C" void SerializationInfoEnumerator_Reset_m3017682611 ();
extern "C" void StreamingContext__ctor_m3383325272_AdjustorThunk ();
extern "C" void StreamingContext__ctor_m3932145525_AdjustorThunk ();
extern "C" void StreamingContext_get_State_m135078841_AdjustorThunk ();
extern "C" void StreamingContext_Equals_m1148347797_AdjustorThunk ();
extern "C" void StreamingContext_GetHashCode_m685689445_AdjustorThunk ();
extern "C" void RuntimeFieldHandle__ctor_m4109695609_AdjustorThunk ();
extern "C" void RuntimeFieldHandle_get_Value_m1454285835_AdjustorThunk ();
extern "C" void RuntimeFieldHandle_Equals_m1824119433_AdjustorThunk ();
extern "C" void RuntimeFieldHandle_GetHashCode_m861234645_AdjustorThunk ();
extern "C" void RuntimeMethodHandle__ctor_m3876746557_AdjustorThunk ();
extern "C" void RuntimeMethodHandle__ctor_m783095629_AdjustorThunk ();
extern "C" void RuntimeMethodHandle_get_Value_m819780389_AdjustorThunk ();
extern "C" void RuntimeMethodHandle_Equals_m2047619131_AdjustorThunk ();
extern "C" void RuntimeMethodHandle_GetHashCode_m2344053433_AdjustorThunk ();
extern "C" void RuntimeTypeHandle__ctor_m2592940784_AdjustorThunk ();
extern "C" void RuntimeTypeHandle_get_Value_m341583565_AdjustorThunk ();
extern "C" void RuntimeTypeHandle_Equals_m3288488752_AdjustorThunk ();
extern "C" void RuntimeTypeHandle_GetHashCode_m3601639994_AdjustorThunk ();
extern "C" void SByte_System_IConvertible_ToBoolean_m3232667366_AdjustorThunk ();
extern "C" void SByte_System_IConvertible_ToByte_m2425403364_AdjustorThunk ();
extern "C" void SByte_System_IConvertible_ToChar_m3723688810_AdjustorThunk ();
extern "C" void SByte_System_IConvertible_ToDateTime_m2748674448_AdjustorThunk ();
extern "C" void SByte_System_IConvertible_ToDecimal_m228135877_AdjustorThunk ();
extern "C" void SByte_System_IConvertible_ToDouble_m4085354590_AdjustorThunk ();
extern "C" void SByte_System_IConvertible_ToInt16_m4117717194_AdjustorThunk ();
extern "C" void SByte_System_IConvertible_ToInt32_m1080684567_AdjustorThunk ();
extern "C" void SByte_System_IConvertible_ToInt64_m338116548_AdjustorThunk ();
extern "C" void SByte_System_IConvertible_ToSByte_m2998821402_AdjustorThunk ();
extern "C" void SByte_System_IConvertible_ToSingle_m218660642_AdjustorThunk ();
extern "C" void SByte_System_IConvertible_ToType_m2818990883_AdjustorThunk ();
extern "C" void SByte_System_IConvertible_ToUInt16_m2876433645_AdjustorThunk ();
extern "C" void SByte_System_IConvertible_ToUInt32_m98339127_AdjustorThunk ();
extern "C" void SByte_System_IConvertible_ToUInt64_m2171951220_AdjustorThunk ();
extern "C" void SByte_CompareTo_m1663211154_AdjustorThunk ();
extern "C" void SByte_Equals_m3029699812_AdjustorThunk ();
extern "C" void SByte_GetHashCode_m3416233913_AdjustorThunk ();
extern "C" void SByte_CompareTo_m1820876469_AdjustorThunk ();
extern "C" void SByte_Equals_m1983290383_AdjustorThunk ();
extern "C" void SByte_Parse_m3989199642 ();
extern "C" void SByte_Parse_m3790253308 ();
extern "C" void SByte_Parse_m1842460049 ();
extern "C" void SByte_TryParse_m3168549138 ();
extern "C" void SByte_ToString_m2540408260_AdjustorThunk ();
extern "C" void SByte_ToString_m995206019_AdjustorThunk ();
extern "C" void SByte_ToString_m1817830399_AdjustorThunk ();
extern "C" void SByte_ToString_m3691134368_AdjustorThunk ();
extern "C" void AllowPartiallyTrustedCallersAttribute__ctor_m1393518081 ();
extern "C" void CodeAccessPermission__ctor_m3911337023 ();
extern "C" void CodeAccessPermission_Equals_m593433957 ();
extern "C" void CodeAccessPermission_GetHashCode_m2903672344 ();
extern "C" void CodeAccessPermission_ToString_m2062487043 ();
extern "C" void CodeAccessPermission_Element_m217284982 ();
extern "C" void CodeAccessPermission_ThrowInvalidPermission_m1868625873 ();
extern "C" void AsymmetricAlgorithm__ctor_m291671647 ();
extern "C" void AsymmetricAlgorithm_System_IDisposable_Dispose_m88189229 ();
extern "C" void AsymmetricAlgorithm_get_KeySize_m1902598726 ();
extern "C" void AsymmetricAlgorithm_set_KeySize_m514272367 ();
extern "C" void AsymmetricAlgorithm_Clear_m1058357189 ();
extern "C" void AsymmetricAlgorithm_GetNamedParam_m840831014 ();
extern "C" void AsymmetricKeyExchangeFormatter__ctor_m4040727494 ();
extern "C" void AsymmetricSignatureDeformatter__ctor_m3492573234 ();
extern "C" void AsymmetricSignatureFormatter__ctor_m2850916843 ();
extern "C" void Base64Constants__cctor_m2233542957 ();
extern "C" void CryptoConfig__cctor_m2675611865 ();
extern "C" void CryptoConfig_Initialize_m3418844644 ();
extern "C" void CryptoConfig_CreateFromName_m804979997 ();
extern "C" void CryptoConfig_CreateFromName_m1179263245 ();
extern "C" void CryptoConfig_MapNameToOID_m3314400358 ();
extern "C" void CryptoConfig_EncodeOID_m1263679715 ();
extern "C" void CryptoConfig_EncodeLongNumber_m3991462207 ();
extern "C" void CryptographicException__ctor_m1849887889 ();
extern "C" void CryptographicException__ctor_m2064601648 ();
extern "C" void CryptographicException__ctor_m3038635483 ();
extern "C" void CryptographicException__ctor_m227323875 ();
extern "C" void CryptographicException__ctor_m4220601442 ();
extern "C" void CryptographicUnexpectedOperationException__ctor_m172375967 ();
extern "C" void CryptographicUnexpectedOperationException__ctor_m2989532558 ();
extern "C" void CryptographicUnexpectedOperationException__ctor_m2348287976 ();
extern "C" void CspParameters__ctor_m2651133894 ();
extern "C" void CspParameters__ctor_m2808884695 ();
extern "C" void CspParameters__ctor_m2150729816 ();
extern "C" void CspParameters__ctor_m871400341 ();
extern "C" void CspParameters_get_Flags_m4242149968 ();
extern "C" void CspParameters_set_Flags_m1836067686 ();
extern "C" void DES__ctor_m2071078587 ();
extern "C" void DES__cctor_m3263083662 ();
extern "C" void DES_Create_m50114166 ();
extern "C" void DES_Create_m988832518 ();
extern "C" void DES_IsWeakKey_m3741566582 ();
extern "C" void DES_IsSemiWeakKey_m540835628 ();
extern "C" void DES_get_Key_m1041051377 ();
extern "C" void DES_set_Key_m3627031068 ();
extern "C" void DESCryptoServiceProvider__ctor_m3142145469 ();
extern "C" void DESCryptoServiceProvider_CreateDecryptor_m3671349511 ();
extern "C" void DESCryptoServiceProvider_CreateEncryptor_m2932690635 ();
extern "C" void DESCryptoServiceProvider_GenerateIV_m3248784923 ();
extern "C" void DESCryptoServiceProvider_GenerateKey_m2289968366 ();
extern "C" void DESTransform__ctor_m1440260789 ();
extern "C" void DESTransform__cctor_m4175857897 ();
extern "C" void DESTransform_CipherFunct_m1470689048 ();
extern "C" void DESTransform_Permutation_m1379612147 ();
extern "C" void DESTransform_BSwap_m1755799969 ();
extern "C" void DESTransform_SetKey_m213337146 ();
extern "C" void DESTransform_ProcessBlock_m273492051 ();
extern "C" void DESTransform_ECB_m2502013171 ();
extern "C" void DESTransform_GetStrongKey_m2146352829 ();
extern "C" void DSA__ctor_m3689799073 ();
extern "C" void DSA_Create_m2752565669 ();
extern "C" void DSA_Create_m561737319 ();
extern "C" void DSA_ZeroizePrivateKey_m1359079785 ();
extern "C" void DSA_FromXmlString_m3131159162 ();
extern "C" void DSA_ToXmlString_m721158241 ();
extern "C" void DSACryptoServiceProvider__ctor_m3196392230 ();
extern "C" void DSACryptoServiceProvider__ctor_m1140958945 ();
extern "C" void DSACryptoServiceProvider__ctor_m1444452395 ();
extern "C" void DSACryptoServiceProvider__cctor_m3923475969 ();
extern "C" void DSACryptoServiceProvider_Finalize_m2646587258 ();
extern "C" void DSACryptoServiceProvider_get_KeySize_m51612726 ();
extern "C" void DSACryptoServiceProvider_get_PublicOnly_m3652955571 ();
extern "C" void DSACryptoServiceProvider_ExportParameters_m330548664 ();
extern "C" void DSACryptoServiceProvider_ImportParameters_m2126638017 ();
extern "C" void DSACryptoServiceProvider_CreateSignature_m3316725619 ();
extern "C" void DSACryptoServiceProvider_VerifySignature_m3390963116 ();
extern "C" void DSACryptoServiceProvider_Dispose_m3580099521 ();
extern "C" void DSACryptoServiceProvider_OnKeyGenerated_m2360698721 ();
extern "C" void DSASignatureDeformatter__ctor_m626251588 ();
extern "C" void DSASignatureDeformatter__ctor_m4204963265 ();
extern "C" void DSASignatureDeformatter_SetHashAlgorithm_m1774837202 ();
extern "C" void DSASignatureDeformatter_SetKey_m700940086 ();
extern "C" void DSASignatureDeformatter_VerifySignature_m2492922105 ();
extern "C" void DSASignatureDescription__ctor_m3857331962 ();
extern "C" void DSASignatureFormatter__ctor_m2808259383 ();
extern "C" void DSASignatureFormatter_CreateSignature_m1203887686 ();
extern "C" void DSASignatureFormatter_SetHashAlgorithm_m1735504798 ();
extern "C" void DSASignatureFormatter_SetKey_m1731353802 ();
extern "C" void HashAlgorithm__ctor_m1707064752 ();
extern "C" void HashAlgorithm_System_IDisposable_Dispose_m592015170 ();
extern "C" void HashAlgorithm_get_CanReuseTransform_m3839548304 ();
extern "C" void HashAlgorithm_ComputeHash_m3223123939 ();
extern "C" void HashAlgorithm_ComputeHash_m1624199078 ();
extern "C" void HashAlgorithm_Create_m389128258 ();
extern "C" void HashAlgorithm_get_Hash_m3278937893 ();
extern "C" void HashAlgorithm_get_HashSize_m257897831 ();
extern "C" void HashAlgorithm_Dispose_m2612993633 ();
extern "C" void HashAlgorithm_TransformBlock_m772172284 ();
extern "C" void HashAlgorithm_TransformFinalBlock_m1323739666 ();
extern "C" void HMAC__ctor_m552637390 ();
extern "C" void HMAC_get_BlockSizeValue_m1423446315 ();
extern "C" void HMAC_set_BlockSizeValue_m1637616519 ();
extern "C" void HMAC_set_HashName_m3407898326 ();
extern "C" void HMAC_get_Key_m2998777640 ();
extern "C" void HMAC_set_Key_m2022302723 ();
extern "C" void HMAC_get_Block_m2704784173 ();
extern "C" void HMAC_KeySetup_m2898857670 ();
extern "C" void HMAC_Dispose_m1734594900 ();
extern "C" void HMAC_HashCore_m1588024292 ();
extern "C" void HMAC_HashFinal_m3818613210 ();
extern "C" void HMAC_Initialize_m1955285998 ();
extern "C" void HMAC_Create_m2869975659 ();
extern "C" void HMAC_Create_m822913551 ();
extern "C" void HMACMD5__ctor_m1560771008 ();
extern "C" void HMACMD5__ctor_m349827019 ();
extern "C" void HMACRIPEMD160__ctor_m3551533459 ();
extern "C" void HMACRIPEMD160__ctor_m924157401 ();
extern "C" void HMACSHA1__ctor_m896219247 ();
extern "C" void HMACSHA1__ctor_m2286467978 ();
extern "C" void HMACSHA256__ctor_m2142671648 ();
extern "C" void HMACSHA256__ctor_m1030208081 ();
extern "C" void HMACSHA384__ctor_m1367037561 ();
extern "C" void HMACSHA384__ctor_m1694133122 ();
extern "C" void HMACSHA384__cctor_m3488584827 ();
extern "C" void HMACSHA384_set_ProduceLegacyHmacValues_m1479681577 ();
extern "C" void HMACSHA512__ctor_m149122110 ();
extern "C" void HMACSHA512__ctor_m562212266 ();
extern "C" void HMACSHA512__cctor_m3792482634 ();
extern "C" void HMACSHA512_set_ProduceLegacyHmacValues_m237511312 ();
extern "C" void KeyedHashAlgorithm__ctor_m4208701600 ();
extern "C" void KeyedHashAlgorithm_Finalize_m1701091456 ();
extern "C" void KeyedHashAlgorithm_get_Key_m3725253812 ();
extern "C" void KeyedHashAlgorithm_set_Key_m2903603585 ();
extern "C" void KeyedHashAlgorithm_Dispose_m1778000470 ();
extern "C" void KeyedHashAlgorithm_ZeroizeKey_m1138297311 ();
extern "C" void KeySizes__ctor_m416378169 ();
extern "C" void KeySizes_get_MaxSize_m192874535 ();
extern "C" void KeySizes_get_MinSize_m1626877941 ();
extern "C" void KeySizes_get_SkipSize_m2105865932 ();
extern "C" void KeySizes_IsLegal_m989369878 ();
extern "C" void KeySizes_IsLegalKeySize_m1950532129 ();
extern "C" void MACTripleDES__ctor_m2400801372 ();
extern "C" void MACTripleDES_Setup_m2341973299 ();
extern "C" void MACTripleDES_Finalize_m2622871706 ();
extern "C" void MACTripleDES_Dispose_m247372160 ();
extern "C" void MACTripleDES_Initialize_m385376470 ();
extern "C" void MACTripleDES_HashCore_m2727696638 ();
extern "C" void MACTripleDES_HashFinal_m1042999865 ();
extern "C" void MD5__ctor_m596613171 ();
extern "C" void MD5_Create_m4125528865 ();
extern "C" void MD5_Create_m205545373 ();
extern "C" void MD5CryptoServiceProvider__ctor_m753386372 ();
extern "C" void MD5CryptoServiceProvider__cctor_m270258082 ();
extern "C" void MD5CryptoServiceProvider_Finalize_m955267910 ();
extern "C" void MD5CryptoServiceProvider_Dispose_m1620185781 ();
extern "C" void MD5CryptoServiceProvider_HashCore_m2121415868 ();
extern "C" void MD5CryptoServiceProvider_HashFinal_m2818288565 ();
extern "C" void MD5CryptoServiceProvider_Initialize_m3474556849 ();
extern "C" void MD5CryptoServiceProvider_ProcessBlock_m182282920 ();
extern "C" void MD5CryptoServiceProvider_ProcessFinalBlock_m799316274 ();
extern "C" void MD5CryptoServiceProvider_AddLength_m462814318 ();
extern "C" void RandomNumberGenerator__ctor_m3840730272 ();
extern "C" void RandomNumberGenerator_Create_m4206972973 ();
extern "C" void RandomNumberGenerator_Create_m2425005426 ();
extern "C" void RC2__ctor_m3631573358 ();
extern "C" void RC2_Create_m1850727163 ();
extern "C" void RC2_Create_m246291559 ();
extern "C" void RC2_get_EffectiveKeySize_m4238040213 ();
extern "C" void RC2_get_KeySize_m2670902482 ();
extern "C" void RC2_set_KeySize_m2041602034 ();
extern "C" void RC2CryptoServiceProvider__ctor_m253482583 ();
extern "C" void RC2CryptoServiceProvider_get_EffectiveKeySize_m1930909438 ();
extern "C" void RC2CryptoServiceProvider_CreateDecryptor_m1625321546 ();
extern "C" void RC2CryptoServiceProvider_CreateEncryptor_m4236515671 ();
extern "C" void RC2CryptoServiceProvider_GenerateIV_m1845440477 ();
extern "C" void RC2CryptoServiceProvider_GenerateKey_m353453894 ();
extern "C" void RC2Transform__ctor_m844199803 ();
extern "C" void RC2Transform__cctor_m2771795387 ();
extern "C" void RC2Transform_ECB_m846182354 ();
extern "C" void Rijndael__ctor_m2910605669 ();
extern "C" void Rijndael_Create_m2744073199 ();
extern "C" void Rijndael_Create_m3396896623 ();
extern "C" void RijndaelManaged__ctor_m3266499975 ();
extern "C" void RijndaelManaged_GenerateIV_m1915080978 ();
extern "C" void RijndaelManaged_GenerateKey_m1787764991 ();
extern "C" void RijndaelManaged_CreateDecryptor_m3673627565 ();
extern "C" void RijndaelManaged_CreateEncryptor_m3840576876 ();
extern "C" void RijndaelManagedTransform__ctor_m1483700773 ();
extern "C" void RijndaelManagedTransform_System_IDisposable_Dispose_m4058554074 ();
extern "C" void RijndaelManagedTransform_get_CanReuseTransform_m3197918900 ();
extern "C" void RijndaelManagedTransform_TransformBlock_m2185477921 ();
extern "C" void RijndaelManagedTransform_TransformFinalBlock_m3059836191 ();
extern "C" void RijndaelTransform__ctor_m4128219783 ();
extern "C" void RijndaelTransform__cctor_m4016021323 ();
extern "C" void RijndaelTransform_Clear_m3250384753 ();
extern "C" void RijndaelTransform_ECB_m1050161573 ();
extern "C" void RijndaelTransform_SubByte_m1285705810 ();
extern "C" void RijndaelTransform_Encrypt128_m3469715421 ();
extern "C" void RijndaelTransform_Encrypt192_m2684119719 ();
extern "C" void RijndaelTransform_Encrypt256_m2620886660 ();
extern "C" void RijndaelTransform_Decrypt128_m2721620345 ();
extern "C" void RijndaelTransform_Decrypt192_m2892346114 ();
extern "C" void RijndaelTransform_Decrypt256_m3633649152 ();
extern "C" void RIPEMD160__ctor_m3973466306 ();
extern "C" void RIPEMD160Managed__ctor_m1524938579 ();
extern "C" void RIPEMD160Managed_Initialize_m3387271859 ();
extern "C" void RIPEMD160Managed_HashCore_m997955926 ();
extern "C" void RIPEMD160Managed_HashFinal_m2196059574 ();
extern "C" void RIPEMD160Managed_Finalize_m3695408240 ();
extern "C" void RIPEMD160Managed_ProcessBlock_m720135161 ();
extern "C" void RIPEMD160Managed_Compress_m3442516984 ();
extern "C" void RIPEMD160Managed_CompressFinal_m2548849686 ();
extern "C" void RIPEMD160Managed_ROL_m663937787 ();
extern "C" void RIPEMD160Managed_F_m27732081 ();
extern "C" void RIPEMD160Managed_G_m2542142523 ();
extern "C" void RIPEMD160Managed_H_m1727654113 ();
extern "C" void RIPEMD160Managed_I_m2826723322 ();
extern "C" void RIPEMD160Managed_J_m3895786971 ();
extern "C" void RIPEMD160Managed_FF_m1909724065 ();
extern "C" void RIPEMD160Managed_GG_m3306331971 ();
extern "C" void RIPEMD160Managed_HH_m945423221 ();
extern "C" void RIPEMD160Managed_II_m1874445900 ();
extern "C" void RIPEMD160Managed_JJ_m1236397107 ();
extern "C" void RIPEMD160Managed_FFF_m3028505343 ();
extern "C" void RIPEMD160Managed_GGG_m3380835391 ();
extern "C" void RIPEMD160Managed_HHH_m1160663704 ();
extern "C" void RIPEMD160Managed_III_m3527519578 ();
extern "C" void RIPEMD160Managed_JJJ_m442232833 ();
extern "C" void RNGCryptoServiceProvider__ctor_m943297446 ();
extern "C" void RNGCryptoServiceProvider__cctor_m966452489 ();
extern "C" void RNGCryptoServiceProvider_Check_m437531604 ();
extern "C" void RNGCryptoServiceProvider_RngOpen_m1081408587 ();
extern "C" void RNGCryptoServiceProvider_RngInitialize_m3078861152 ();
extern "C" void RNGCryptoServiceProvider_RngGetBytes_m3536057600 ();
extern "C" void RNGCryptoServiceProvider_RngClose_m2288642125 ();
extern "C" void RNGCryptoServiceProvider_GetBytes_m1534062158 ();
extern "C" void RNGCryptoServiceProvider_GetNonZeroBytes_m4259469323 ();
extern "C" void RNGCryptoServiceProvider_Finalize_m2708995255 ();
extern "C" void RSA__ctor_m3625671390 ();
extern "C" void RSA_Create_m3650625912 ();
extern "C" void RSA_Create_m3773847354 ();
extern "C" void RSA_ZeroizePrivateKey_m1340799204 ();
extern "C" void RSA_FromXmlString_m3696452870 ();
extern "C" void RSA_ToXmlString_m4181509890 ();
extern "C" void RSACryptoServiceProvider__ctor_m367282860 ();
extern "C" void RSACryptoServiceProvider__ctor_m1534247216 ();
extern "C" void RSACryptoServiceProvider__ctor_m1453217386 ();
extern "C" void RSACryptoServiceProvider__cctor_m3138589205 ();
extern "C" void RSACryptoServiceProvider_Common_m1337984034 ();
extern "C" void RSACryptoServiceProvider_Finalize_m3061148868 ();
extern "C" void RSACryptoServiceProvider_get_KeySize_m2842481204 ();
extern "C" void RSACryptoServiceProvider_get_PublicOnly_m3250695170 ();
extern "C" void RSACryptoServiceProvider_DecryptValue_m4214132066 ();
extern "C" void RSACryptoServiceProvider_EncryptValue_m1796613593 ();
extern "C" void RSACryptoServiceProvider_ExportParameters_m4155150051 ();
extern "C" void RSACryptoServiceProvider_ImportParameters_m1846951175 ();
extern "C" void RSACryptoServiceProvider_Dispose_m1440214926 ();
extern "C" void RSACryptoServiceProvider_OnKeyGenerated_m3780506407 ();
extern "C" void RSAPKCS1KeyExchangeFormatter__ctor_m371779760 ();
extern "C" void RSAPKCS1KeyExchangeFormatter_CreateKeyExchange_m1481364295 ();
extern "C" void RSAPKCS1KeyExchangeFormatter_SetRSAKey_m2334878093 ();
extern "C" void RSAPKCS1SHA1SignatureDescription__ctor_m1453323230 ();
extern "C" void RSAPKCS1SignatureDeformatter__ctor_m922759933 ();
extern "C" void RSAPKCS1SignatureDeformatter__ctor_m94662501 ();
extern "C" void RSAPKCS1SignatureDeformatter_SetHashAlgorithm_m2028328074 ();
extern "C" void RSAPKCS1SignatureDeformatter_SetKey_m1241529138 ();
extern "C" void RSAPKCS1SignatureDeformatter_VerifySignature_m3971152561 ();
extern "C" void RSAPKCS1SignatureFormatter__ctor_m3354498740 ();
extern "C" void RSAPKCS1SignatureFormatter_CreateSignature_m4150834117 ();
extern "C" void RSAPKCS1SignatureFormatter_SetHashAlgorithm_m1125255771 ();
extern "C" void RSAPKCS1SignatureFormatter_SetKey_m3922233774 ();
extern "C" void SHA1__ctor_m4034446490 ();
extern "C" void SHA1_Create_m51473861 ();
extern "C" void SHA1_Create_m3635804177 ();
extern "C" void SHA1CryptoServiceProvider__ctor_m2333822830 ();
extern "C" void SHA1CryptoServiceProvider_Finalize_m206632601 ();
extern "C" void SHA1CryptoServiceProvider_Dispose_m455058617 ();
extern "C" void SHA1CryptoServiceProvider_HashCore_m1649570722 ();
extern "C" void SHA1CryptoServiceProvider_HashFinal_m247104950 ();
extern "C" void SHA1CryptoServiceProvider_Initialize_m3672850906 ();
extern "C" void SHA1Internal__ctor_m2325882877 ();
extern "C" void SHA1Internal_HashCore_m3378282166 ();
extern "C" void SHA1Internal_HashFinal_m1656072795 ();
extern "C" void SHA1Internal_Initialize_m3917808401 ();
extern "C" void SHA1Internal_ProcessBlock_m1171620405 ();
extern "C" void SHA1Internal_InitialiseBuff_m4266595327 ();
extern "C" void SHA1Internal_FillBuff_m3772419989 ();
extern "C" void SHA1Internal_ProcessFinalBlock_m2445418988 ();
extern "C" void SHA1Internal_AddLength_m2664273459 ();
extern "C" void SHA1Managed__ctor_m1463248351 ();
extern "C" void SHA1Managed_HashCore_m791876697 ();
extern "C" void SHA1Managed_HashFinal_m2802009378 ();
extern "C" void SHA1Managed_Initialize_m927269960 ();
extern "C" void SHA256__ctor_m1589541066 ();
extern "C" void SHA256_Create_m1972474237 ();
extern "C" void SHA256_Create_m3761074742 ();
extern "C" void SHA256Managed__ctor_m1100320217 ();
extern "C" void SHA256Managed_HashCore_m2504427447 ();
extern "C" void SHA256Managed_HashFinal_m2812455299 ();
extern "C" void SHA256Managed_Initialize_m1098343159 ();
extern "C" void SHA256Managed_ProcessBlock_m1968446734 ();
extern "C" void SHA256Managed_ProcessFinalBlock_m1034177166 ();
extern "C" void SHA256Managed_AddLength_m1753316025 ();
extern "C" void SHA384__ctor_m42777444 ();
extern "C" void SHA384_Create_m2545117731 ();
extern "C" void SHA384_Create_m186640052 ();
extern "C" void SHA384Managed__ctor_m2760360676 ();
extern "C" void SHA384Managed_Initialize_m2368260672 ();
extern "C" void SHA384Managed_Initialize_m1472779757 ();
extern "C" void SHA384Managed_HashCore_m2058781025 ();
extern "C" void SHA384Managed_HashFinal_m1091129534 ();
extern "C" void SHA384Managed_update_m832600628 ();
extern "C" void SHA384Managed_processWord_m3910418284 ();
extern "C" void SHA384Managed_unpackWord_m3305815558 ();
extern "C" void SHA384Managed_adjustByteCounts_m1577367448 ();
extern "C" void SHA384Managed_processLength_m1998016938 ();
extern "C" void SHA384Managed_processBlock_m1044762650 ();
extern "C" void SHA512__ctor_m849092553 ();
extern "C" void SHA512_Create_m3996179329 ();
extern "C" void SHA512_Create_m2046618513 ();
extern "C" void SHA512Managed__ctor_m1907512623 ();
extern "C" void SHA512Managed_Initialize_m1341762678 ();
extern "C" void SHA512Managed_Initialize_m3709419690 ();
extern "C" void SHA512Managed_HashCore_m3736500170 ();
extern "C" void SHA512Managed_HashFinal_m4222140059 ();
extern "C" void SHA512Managed_update_m1980741561 ();
extern "C" void SHA512Managed_processWord_m3086419716 ();
extern "C" void SHA512Managed_unpackWord_m186287524 ();
extern "C" void SHA512Managed_adjustByteCounts_m3584504717 ();
extern "C" void SHA512Managed_processLength_m560611531 ();
extern "C" void SHA512Managed_processBlock_m1321711005 ();
extern "C" void SHA512Managed_rotateRight_m3684641738 ();
extern "C" void SHA512Managed_Ch_m3989645947 ();
extern "C" void SHA512Managed_Maj_m2919184232 ();
extern "C" void SHA512Managed_Sum0_m2877570381 ();
extern "C" void SHA512Managed_Sum1_m2188034114 ();
extern "C" void SHA512Managed_Sigma0_m1705360206 ();
extern "C" void SHA512Managed_Sigma1_m2986763090 ();
extern "C" void SHAConstants__cctor_m123851408 ();
extern "C" void SignatureDescription__ctor_m1612965412 ();
extern "C" void SignatureDescription_set_DeformatterAlgorithm_m1821656658 ();
extern "C" void SignatureDescription_set_DigestAlgorithm_m2466896255 ();
extern "C" void SignatureDescription_set_FormatterAlgorithm_m1935147294 ();
extern "C" void SignatureDescription_set_KeyAlgorithm_m377164898 ();
extern "C" void SymmetricAlgorithm__ctor_m1122125023 ();
extern "C" void SymmetricAlgorithm_System_IDisposable_Dispose_m2796869492 ();
extern "C" void SymmetricAlgorithm_Finalize_m1190243561 ();
extern "C" void SymmetricAlgorithm_Clear_m3481857600 ();
extern "C" void SymmetricAlgorithm_Dispose_m3325643277 ();
extern "C" void SymmetricAlgorithm_get_BlockSize_m1273428540 ();
extern "C" void SymmetricAlgorithm_set_BlockSize_m3183805403 ();
extern "C" void SymmetricAlgorithm_get_FeedbackSize_m3223966159 ();
extern "C" void SymmetricAlgorithm_get_IV_m72293949 ();
extern "C" void SymmetricAlgorithm_set_IV_m2177422850 ();
extern "C" void SymmetricAlgorithm_get_Key_m2698285671 ();
extern "C" void SymmetricAlgorithm_set_Key_m932106934 ();
extern "C" void SymmetricAlgorithm_get_KeySize_m210518352 ();
extern "C" void SymmetricAlgorithm_set_KeySize_m2404708908 ();
extern "C" void SymmetricAlgorithm_get_LegalKeySizes_m2001160061 ();
extern "C" void SymmetricAlgorithm_get_Mode_m358157376 ();
extern "C" void SymmetricAlgorithm_set_Mode_m3137309540 ();
extern "C" void SymmetricAlgorithm_get_Padding_m2219535380 ();
extern "C" void SymmetricAlgorithm_set_Padding_m4286987756 ();
extern "C" void SymmetricAlgorithm_CreateDecryptor_m1065208203 ();
extern "C" void SymmetricAlgorithm_CreateEncryptor_m2069911176 ();
extern "C" void SymmetricAlgorithm_Create_m1465901834 ();
extern "C" void ToBase64Transform_System_IDisposable_Dispose_m745662615 ();
extern "C" void ToBase64Transform_Finalize_m3609906449 ();
extern "C" void ToBase64Transform_get_CanReuseTransform_m3440077908 ();
extern "C" void ToBase64Transform_get_InputBlockSize_m2587476540 ();
extern "C" void ToBase64Transform_get_OutputBlockSize_m3091865724 ();
extern "C" void ToBase64Transform_Dispose_m2067590776 ();
extern "C" void ToBase64Transform_TransformBlock_m1687230426 ();
extern "C" void ToBase64Transform_InternalTransformBlock_m1011363023 ();
extern "C" void ToBase64Transform_TransformFinalBlock_m1716618444 ();
extern "C" void ToBase64Transform_InternalTransformFinalBlock_m2826977121 ();
extern "C" void TripleDES__ctor_m3764042420 ();
extern "C" void TripleDES_get_Key_m4270652781 ();
extern "C" void TripleDES_set_Key_m785520234 ();
extern "C" void TripleDES_IsWeakKey_m321623027 ();
extern "C" void TripleDES_Create_m810966163 ();
extern "C" void TripleDES_Create_m3990140880 ();
extern "C" void TripleDESCryptoServiceProvider__ctor_m1548678773 ();
extern "C" void TripleDESCryptoServiceProvider_GenerateIV_m4042888525 ();
extern "C" void TripleDESCryptoServiceProvider_GenerateKey_m3908710279 ();
extern "C" void TripleDESCryptoServiceProvider_CreateDecryptor_m564196624 ();
extern "C" void TripleDESCryptoServiceProvider_CreateEncryptor_m4119148710 ();
extern "C" void TripleDESTransform__ctor_m1399930954 ();
extern "C" void TripleDESTransform_ECB_m3064597145 ();
extern "C" void TripleDESTransform_GetStrongKey_m2843858850 ();
extern "C" void X509Certificate__ctor_m2598704077 ();
extern "C" void X509Certificate__ctor_m772299980 ();
extern "C" void X509Certificate__ctor_m676817484 ();
extern "C" void X509Certificate__ctor_m3195389856 ();
extern "C" void X509Certificate_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m2916969062 ();
extern "C" void X509Certificate_tostr_m2358917317 ();
extern "C" void X509Certificate_Equals_m1506120451 ();
extern "C" void X509Certificate_GetCertHash_m386807738 ();
extern "C" void X509Certificate_GetCertHashString_m1746844672 ();
extern "C" void X509Certificate_GetEffectiveDateString_m280558803 ();
extern "C" void X509Certificate_GetExpirationDateString_m2049787079 ();
extern "C" void X509Certificate_GetHashCode_m3341056423 ();
extern "C" void X509Certificate_GetIssuerName_m1334954263 ();
extern "C" void X509Certificate_GetName_m865911396 ();
extern "C" void X509Certificate_GetPublicKey_m2681245673 ();
extern "C" void X509Certificate_GetRawCertData_m3615746927 ();
extern "C" void X509Certificate_ToString_m3600669461 ();
extern "C" void X509Certificate_ToString_m3384700977 ();
extern "C" void X509Certificate_get_Issuer_m4136295997 ();
extern "C" void X509Certificate_get_Subject_m52101678 ();
extern "C" void X509Certificate_Equals_m2980846026 ();
extern "C" void X509Certificate_Import_m1670206153 ();
extern "C" void X509Certificate_Reset_m2166992089 ();
extern "C" void SecurityPermission__ctor_m1598538662 ();
extern "C" void SecurityPermission_set_Flags_m3835982641 ();
extern "C" void SecurityPermission_IsUnrestricted_m3853515820 ();
extern "C" void SecurityPermission_IsSubsetOf_m2984130754 ();
extern "C" void SecurityPermission_ToXml_m66315343 ();
extern "C" void SecurityPermission_IsEmpty_m3170835685 ();
extern "C" void SecurityPermission_Cast_m1737603092 ();
extern "C" void SecurityPermissionAttribute_set_SkipVerification_m3395114830 ();
extern "C" void StrongNamePublicKeyBlob_Equals_m3786157380 ();
extern "C" void StrongNamePublicKeyBlob_GetHashCode_m707552328 ();
extern "C" void StrongNamePublicKeyBlob_ToString_m3447543794 ();
extern "C" void PermissionSet__ctor_m2510521410 ();
extern "C" void PermissionSet__ctor_m503708646 ();
extern "C" void PermissionSet_set_DeclarativeSecurity_m84390744 ();
extern "C" void PermissionSet_CreateFromBinaryFormat_m2890811445 ();
extern "C" void ApplicationTrust__ctor_m2779199393 ();
extern "C" void Evidence__ctor_m4143335037 ();
extern "C" void Evidence_get_Count_m1102568452 ();
extern "C" void Evidence_get_SyncRoot_m3796355461 ();
extern "C" void Evidence_get_HostEvidenceList_m241892834 ();
extern "C" void Evidence_get_AssemblyEvidenceList_m69417573 ();
extern "C" void Evidence_CopyTo_m3720445569 ();
extern "C" void Evidence_Equals_m846405073 ();
extern "C" void Evidence_GetEnumerator_m2624028338 ();
extern "C" void Evidence_GetHashCode_m1726229036 ();
extern "C" void EvidenceEnumerator__ctor_m4246181871 ();
extern "C" void EvidenceEnumerator_MoveNext_m595197927 ();
extern "C" void EvidenceEnumerator_Reset_m1353628469 ();
extern "C" void EvidenceEnumerator_get_Current_m2741679296 ();
extern "C" void Hash__ctor_m897128064 ();
extern "C" void Hash__ctor_m3945204650 ();
extern "C" void Hash_ToString_m2479841340 ();
extern "C" void Hash_GetData_m2427324099 ();
extern "C" void StrongName_get_Name_m3251809947 ();
extern "C" void StrongName_get_PublicKey_m1623125475 ();
extern "C" void StrongName_get_Version_m3532428238 ();
extern "C" void StrongName_Equals_m3137197790 ();
extern "C" void StrongName_GetHashCode_m3018610381 ();
extern "C" void StrongName_ToString_m2399807839 ();
extern "C" void WindowsIdentity__ctor_m3795789436 ();
extern "C" void WindowsIdentity__cctor_m163644234 ();
extern "C" void WindowsIdentity_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m2645016059 ();
extern "C" void WindowsIdentity_Dispose_m953714069 ();
extern "C" void WindowsIdentity_GetCurrentToken_m2622243291 ();
extern "C" void WindowsIdentity_GetTokenName_m3719110404 ();
extern "C" void SecurityContext__ctor_m1210181915 ();
extern "C" void SecurityContext__ctor_m3972088427 ();
extern "C" void SecurityContext_Capture_m2535726366 ();
extern "C" void SecurityContext_get_FlowSuppressed_m9276115 ();
extern "C" void SecurityContext_get_CompressedStack_m3409548514 ();
extern "C" void SecurityCriticalAttribute__ctor_m248669408 ();
extern "C" void SecurityElement__ctor_m1254201806 ();
extern "C" void SecurityElement__ctor_m3829440501 ();
extern "C" void SecurityElement__cctor_m3840129476 ();
extern "C" void SecurityElement_get_Children_m3081891010 ();
extern "C" void SecurityElement_get_Tag_m956161580 ();
extern "C" void SecurityElement_set_Text_m3231999538 ();
extern "C" void SecurityElement_AddAttribute_m779359869 ();
extern "C" void SecurityElement_AddChild_m1712177507 ();
extern "C" void SecurityElement_Escape_m3400820581 ();
extern "C" void SecurityElement_Unescape_m3674705609 ();
extern "C" void SecurityElement_IsValidAttributeName_m3209599636 ();
extern "C" void SecurityElement_IsValidAttributeValue_m1091331824 ();
extern "C" void SecurityElement_IsValidTag_m1338833153 ();
extern "C" void SecurityElement_IsValidText_m2963081070 ();
extern "C" void SecurityElement_SearchForChildByTag_m3962923931 ();
extern "C" void SecurityElement_ToString_m1897021376 ();
extern "C" void SecurityElement_ToXml_m1158908822 ();
extern "C" void SecurityElement_GetAttribute_m3701624767 ();
extern "C" void SecurityAttribute__ctor_m2817753236 ();
extern "C" void SecurityAttribute_get_Name_m737229083 ();
extern "C" void SecurityAttribute_get_Value_m3667996049 ();
extern "C" void SecurityException__ctor_m2999055479 ();
extern "C" void SecurityException__ctor_m1157428826 ();
extern "C" void SecurityException__ctor_m2303508220 ();
extern "C" void SecurityException_get_Demanded_m584995367 ();
extern "C" void SecurityException_get_FirstPermissionThatFailed_m3516293922 ();
extern "C" void SecurityException_get_PermissionState_m394476706 ();
extern "C" void SecurityException_get_PermissionType_m300429407 ();
extern "C" void SecurityException_get_GrantedSet_m4020109840 ();
extern "C" void SecurityException_get_RefusedSet_m341501771 ();
extern "C" void SecurityException_ToString_m1678274603 ();
extern "C" void SecurityFrame__ctor_m287375657_AdjustorThunk ();
extern "C" void SecurityFrame__GetSecurityStack_m2417479436 ();
extern "C" void SecurityFrame_InitFromRuntimeFrame_m629713065_AdjustorThunk ();
extern "C" void SecurityFrame_get_Assembly_m1882709798_AdjustorThunk ();
extern "C" void SecurityFrame_get_Domain_m4128432817_AdjustorThunk ();
extern "C" void SecurityFrame_ToString_m3398776129_AdjustorThunk ();
extern "C" void SecurityFrame_GetStack_m3665290341 ();
extern "C" void SecurityManager__cctor_m1484737609 ();
extern "C" void SecurityManager_get_SecurityEnabled_m684733761 ();
extern "C" void SecurityManager_Decode_m1399211754 ();
extern "C" void SecurityManager_Decode_m948428671 ();
extern "C" void SecuritySafeCriticalAttribute__ctor_m1135903908 ();
extern "C" void SuppressUnmanagedCodeSecurityAttribute__ctor_m1831174959 ();
extern "C" void UnverifiableCodeAttribute__ctor_m3439683954 ();
extern "C" void SerializableAttribute__ctor_m3301598451 ();
extern "C" void Single_System_IConvertible_ToBoolean_m4195264544_AdjustorThunk ();
extern "C" void Single_System_IConvertible_ToByte_m2163323549_AdjustorThunk ();
extern "C" void Single_System_IConvertible_ToChar_m3157121886_AdjustorThunk ();
extern "C" void Single_System_IConvertible_ToDateTime_m1798462033_AdjustorThunk ();
extern "C" void Single_System_IConvertible_ToDecimal_m2823837807_AdjustorThunk ();
extern "C" void Single_System_IConvertible_ToDouble_m2992811009_AdjustorThunk ();
extern "C" void Single_System_IConvertible_ToInt16_m356698018_AdjustorThunk ();
extern "C" void Single_System_IConvertible_ToInt32_m922147041_AdjustorThunk ();
extern "C" void Single_System_IConvertible_ToInt64_m875097587_AdjustorThunk ();
extern "C" void Single_System_IConvertible_ToSByte_m172131650_AdjustorThunk ();
extern "C" void Single_System_IConvertible_ToSingle_m2195342775_AdjustorThunk ();
extern "C" void Single_System_IConvertible_ToType_m3191890887_AdjustorThunk ();
extern "C" void Single_System_IConvertible_ToUInt16_m3732101497_AdjustorThunk ();
extern "C" void Single_System_IConvertible_ToUInt32_m1724267434_AdjustorThunk ();
extern "C" void Single_System_IConvertible_ToUInt64_m2303184156_AdjustorThunk ();
extern "C" void Single_CompareTo_m67370061_AdjustorThunk ();
extern "C" void Single_Equals_m2142477588_AdjustorThunk ();
extern "C" void Single_CompareTo_m656830570_AdjustorThunk ();
extern "C" void Single_Equals_m3179646383_AdjustorThunk ();
extern "C" void Single_GetHashCode_m3263933369_AdjustorThunk ();
extern "C" void Single_IsInfinity_m1761771528 ();
extern "C" void Single_IsNaN_m1192835548 ();
extern "C" void Single_IsNegativeInfinity_m1017463537 ();
extern "C" void Single_IsPositiveInfinity_m1234252695 ();
extern "C" void Single_Parse_m579890978 ();
extern "C" void Single_ToString_m1434489600_AdjustorThunk ();
extern "C" void Single_ToString_m80967036_AdjustorThunk ();
extern "C" void Single_ToString_m929050888_AdjustorThunk ();
extern "C" void StackOverflowException__ctor_m4052237075 ();
extern "C" void StackOverflowException__ctor_m4043369449 ();
extern "C" void StackOverflowException__ctor_m187608528 ();
extern "C" void String__ctor_m1254661065 ();
extern "C" void String__ctor_m3140314943 ();
extern "C" void String__ctor_m906858938 ();
extern "C" void String__ctor_m3325256682 ();
extern "C" void String__cctor_m2122519985 ();
extern "C" void String_System_IConvertible_ToBoolean_m1378750767 ();
extern "C" void String_System_IConvertible_ToByte_m3927732158 ();
extern "C" void String_System_IConvertible_ToChar_m854595448 ();
extern "C" void String_System_IConvertible_ToDateTime_m1467551385 ();
extern "C" void String_System_IConvertible_ToDecimal_m8092706 ();
extern "C" void String_System_IConvertible_ToDouble_m1038698567 ();
extern "C" void String_System_IConvertible_ToInt16_m291669052 ();
extern "C" void String_System_IConvertible_ToInt32_m2512576351 ();
extern "C" void String_System_IConvertible_ToInt64_m54193686 ();
extern "C" void String_System_IConvertible_ToSByte_m1346562548 ();
extern "C" void String_System_IConvertible_ToSingle_m3859507638 ();
extern "C" void String_System_IConvertible_ToType_m804510055 ();
extern "C" void String_System_IConvertible_ToUInt16_m690306049 ();
extern "C" void String_System_IConvertible_ToUInt32_m655826677 ();
extern "C" void String_System_IConvertible_ToUInt64_m1787836301 ();
extern "C" void String_System_Collections_Generic_IEnumerableU3CcharU3E_GetEnumerator_m1131017081 ();
extern "C" void String_System_Collections_IEnumerable_GetEnumerator_m290724930 ();
extern "C" void String_Equals_m3510555014 ();
extern "C" void String_Equals_m1448975066 ();
extern "C" void String_Equals_m672522368 ();
extern "C" void String_get_Chars_m2227998457 ();
extern "C" void String_Clone_m3315204899 ();
extern "C" void String_CopyTo_m4208391136 ();
extern "C" void String_ToCharArray_m3872542010 ();
extern "C" void String_ToCharArray_m2678128660 ();
extern "C" void String_Split_m4016464379 ();
extern "C" void String_Split_m1309689757 ();
extern "C" void String_Split_m2753730040 ();
extern "C" void String_Split_m2680706933 ();
extern "C" void String_Split_m993848168 ();
extern "C" void String_Substring_m4139251900 ();
extern "C" void String_Substring_m2404821311 ();
extern "C" void String_SubstringUnchecked_m3803949323 ();
extern "C" void String_Trim_m2749071318 ();
extern "C" void String_Trim_m361890761 ();
extern "C" void String_TrimStart_m4243963028 ();
extern "C" void String_TrimEnd_m498525724 ();
extern "C" void String_FindNotWhiteSpace_m842093031 ();
extern "C" void String_FindNotInTable_m1859306728 ();
extern "C" void String_Compare_m492058313 ();
extern "C" void String_Compare_m3204107571 ();
extern "C" void String_Compare_m1081297498 ();
extern "C" void String_Compare_m950386926 ();
extern "C" void String_CompareTo_m3788835867 ();
extern "C" void String_CompareTo_m1140222528 ();
extern "C" void String_CompareOrdinal_m344226304 ();
extern "C" void String_CompareOrdinalUnchecked_m2969867142 ();
extern "C" void String_CompareOrdinalCaseInsensitiveUnchecked_m3603703299 ();
extern "C" void String_EndsWith_m2915171759 ();
extern "C" void String_IndexOfAny_m2419327538 ();
extern "C" void String_IndexOfAny_m2024923686 ();
extern "C" void String_IndexOfAny_m1269729619 ();
extern "C" void String_IndexOfAnyUnchecked_m3584206985 ();
extern "C" void String_IndexOf_m3925697364 ();
extern "C" void String_IndexOf_m2477875696 ();
extern "C" void String_IndexOfOrdinal_m1909812369 ();
extern "C" void String_IndexOfOrdinalUnchecked_m2854806724 ();
extern "C" void String_IndexOfOrdinalIgnoreCaseUnchecked_m170710479 ();
extern "C" void String_IndexOf_m2153404505 ();
extern "C" void String_IndexOf_m3796136682 ();
extern "C" void String_IndexOf_m577986163 ();
extern "C" void String_IndexOfUnchecked_m1397064270 ();
extern "C" void String_IndexOf_m374129865 ();
extern "C" void String_IndexOf_m1325451196 ();
extern "C" void String_IndexOf_m1042034519 ();
extern "C" void String_LastIndexOfAny_m2182679387 ();
extern "C" void String_LastIndexOfAnyUnchecked_m4157161479 ();
extern "C" void String_LastIndexOf_m4176159581 ();
extern "C" void String_LastIndexOf_m1340797273 ();
extern "C" void String_LastIndexOf_m4050967138 ();
extern "C" void String_LastIndexOfUnchecked_m103383808 ();
extern "C" void String_LastIndexOf_m2642828268 ();
extern "C" void String_LastIndexOf_m2381323542 ();
extern "C" void String_IsNullOrEmpty_m3560539680 ();
extern "C" void String_PadRight_m1765948888 ();
extern "C" void String_StartsWith_m2938647967 ();
extern "C" void String_Replace_m2363393026 ();
extern "C" void String_Replace_m3637142237 ();
extern "C" void String_ReplaceUnchecked_m1744670392 ();
extern "C" void String_ReplaceFallback_m457580136 ();
extern "C" void String_Remove_m108136435 ();
extern "C" void String_ToLower_m772084117 ();
extern "C" void String_ToLower_m1166643094 ();
extern "C" void String_ToLowerInvariant_m3401715076 ();
extern "C" void String_ToUpperInvariant_m2873657664 ();
extern "C" void String_ToString_m4194906941 ();
extern "C" void String_ToString_m4195444978 ();
extern "C" void String_Format_m1366448113 ();
extern "C" void String_Format_m2216003994 ();
extern "C" void String_Format_m2132242274 ();
extern "C" void String_Format_m2496254338 ();
extern "C" void String_Format_m3845522687 ();
extern "C" void String_FormatHelper_m2208728312 ();
extern "C" void String_Concat_m2669786253 ();
extern "C" void String_Concat_m3876450496 ();
extern "C" void String_Concat_m47853911 ();
extern "C" void String_Concat_m4209604784 ();
extern "C" void String_Concat_m1894493014 ();
extern "C" void String_Concat_m2733933624 ();
extern "C" void String_Concat_m2040094846 ();
extern "C" void String_ConcatInternal_m2804258664 ();
extern "C" void String_Insert_m2432934541 ();
extern "C" void String_Join_m2919922570 ();
extern "C" void String_Join_m2171177581 ();
extern "C" void String_JoinUnchecked_m2289791537 ();
extern "C" void String_get_Length_m3102021951 ();
extern "C" void String_ParseFormatSpecifier_m3912043387 ();
extern "C" void String_ParseDecimal_m2788274796 ();
extern "C" void String_InternalSetChar_m3256793723 ();
extern "C" void String_InternalSetLength_m1600004936 ();
extern "C" void String_GetHashCode_m3205775290 ();
extern "C" void String_GetCaseInsensitiveHashCode_m2313840820 ();
extern "C" void String_CreateString_m1272503540 ();
extern "C" void String_CreateString_m648189005 ();
extern "C" void String_CreateString_m214616560 ();
extern "C" void String_CreateString_m1720740393 ();
extern "C" void String_CreateString_m3924920656 ();
extern "C" void String_CreateString_m3287943429 ();
extern "C" void String_CreateString_m3897475374 ();
extern "C" void String_CreateString_m2877097702 ();
extern "C" void String_memcpy4_m726498586 ();
extern "C" void String_memcpy2_m396091641 ();
extern "C" void String_memcpy1_m363580472 ();
extern "C" void String_memcpy_m1517557384 ();
extern "C" void String_CharCopy_m3297852267 ();
extern "C" void String_CharCopyReverse_m448059676 ();
extern "C" void String_CharCopy_m668588423 ();
extern "C" void String_CharCopy_m1770647809 ();
extern "C" void String_CharCopyReverse_m3178202005 ();
extern "C" void String_InternalSplit_m2194779229 ();
extern "C" void String_InternalAllocateStr_m146585826 ();
extern "C" void String_op_Equality_m532451312 ();
extern "C" void String_op_Inequality_m1315478967 ();
extern "C" void StringComparer__ctor_m3127041636 ();
extern "C" void StringComparer__cctor_m3312141915 ();
extern "C" void StringComparer_get_InvariantCultureIgnoreCase_m2981715268 ();
extern "C" void StringComparer_Compare_m3002408790 ();
extern "C" void StringComparer_Equals_m3422250467 ();
extern "C" void StringComparer_GetHashCode_m4117579396 ();
extern "C" void SystemException__ctor_m3831273481 ();
extern "C" void SystemException__ctor_m2834575246 ();
extern "C" void SystemException__ctor_m2071093010 ();
extern "C" void SystemException__ctor_m3421368584 ();
extern "C" void ASCIIEncoding__ctor_m1796837888 ();
extern "C" void ASCIIEncoding_GetByteCount_m1894082081 ();
extern "C" void ASCIIEncoding_GetByteCount_m231792613 ();
extern "C" void ASCIIEncoding_GetBytes_m2888701808 ();
extern "C" void ASCIIEncoding_GetBytes_m641040454 ();
extern "C" void ASCIIEncoding_GetBytes_m3674966356 ();
extern "C" void ASCIIEncoding_GetBytes_m864289360 ();
extern "C" void ASCIIEncoding_GetCharCount_m2530213940 ();
extern "C" void ASCIIEncoding_GetChars_m293253735 ();
extern "C" void ASCIIEncoding_GetChars_m3032899947 ();
extern "C" void ASCIIEncoding_GetMaxByteCount_m2654379646 ();
extern "C" void ASCIIEncoding_GetMaxCharCount_m1439292005 ();
extern "C" void ASCIIEncoding_GetString_m3084825367 ();
extern "C" void ASCIIEncoding_GetBytes_m687126587 ();
extern "C" void ASCIIEncoding_GetByteCount_m1478581917 ();
extern "C" void ASCIIEncoding_GetDecoder_m763127219 ();
extern "C" void Decoder__ctor_m1987520440 ();
extern "C" void Decoder_set_Fallback_m2700469140 ();
extern "C" void Decoder_get_FallbackBuffer_m2240311834 ();
extern "C" void DecoderExceptionFallback__ctor_m67358718 ();
extern "C" void DecoderExceptionFallback_CreateFallbackBuffer_m2543099475 ();
extern "C" void DecoderExceptionFallback_Equals_m1068623654 ();
extern "C" void DecoderExceptionFallback_GetHashCode_m2030355712 ();
extern "C" void DecoderExceptionFallbackBuffer__ctor_m2542196856 ();
extern "C" void DecoderExceptionFallbackBuffer_get_Remaining_m1536658658 ();
extern "C" void DecoderExceptionFallbackBuffer_Fallback_m633606720 ();
extern "C" void DecoderExceptionFallbackBuffer_GetNextChar_m2071376374 ();
extern "C" void DecoderFallback__ctor_m4073266908 ();
extern "C" void DecoderFallback__cctor_m3632383896 ();
extern "C" void DecoderFallback_get_ExceptionFallback_m1100223985 ();
extern "C" void DecoderFallback_get_ReplacementFallback_m1288894870 ();
extern "C" void DecoderFallback_get_StandardSafeFallback_m2441865702 ();
extern "C" void DecoderFallbackBuffer__ctor_m215246348 ();
extern "C" void DecoderFallbackBuffer_Reset_m2389418149 ();
extern "C" void DecoderFallbackException__ctor_m4249827266 ();
extern "C" void DecoderFallbackException__ctor_m3764406113 ();
extern "C" void DecoderFallbackException__ctor_m3290567523 ();
extern "C" void DecoderReplacementFallback__ctor_m3512596817 ();
extern "C" void DecoderReplacementFallback__ctor_m3316979574 ();
extern "C" void DecoderReplacementFallback_get_DefaultString_m3362905477 ();
extern "C" void DecoderReplacementFallback_CreateFallbackBuffer_m1460208184 ();
extern "C" void DecoderReplacementFallback_Equals_m2141877731 ();
extern "C" void DecoderReplacementFallback_GetHashCode_m2724846204 ();
extern "C" void DecoderReplacementFallbackBuffer__ctor_m1582989656 ();
extern "C" void DecoderReplacementFallbackBuffer_get_Remaining_m3730867186 ();
extern "C" void DecoderReplacementFallbackBuffer_Fallback_m387963607 ();
extern "C" void DecoderReplacementFallbackBuffer_GetNextChar_m3169801814 ();
extern "C" void DecoderReplacementFallbackBuffer_Reset_m3676412794 ();
extern "C" void EncoderExceptionFallback__ctor_m720310507 ();
extern "C" void EncoderExceptionFallback_CreateFallbackBuffer_m1565914304 ();
extern "C" void EncoderExceptionFallback_Equals_m3767715126 ();
extern "C" void EncoderExceptionFallback_GetHashCode_m2099422526 ();
extern "C" void EncoderExceptionFallbackBuffer__ctor_m3920263034 ();
extern "C" void EncoderExceptionFallbackBuffer_get_Remaining_m1878305272 ();
extern "C" void EncoderExceptionFallbackBuffer_Fallback_m4284418952 ();
extern "C" void EncoderExceptionFallbackBuffer_Fallback_m2850919124 ();
extern "C" void EncoderExceptionFallbackBuffer_GetNextChar_m2132786544 ();
extern "C" void EncoderFallback__ctor_m3426198995 ();
extern "C" void EncoderFallback__cctor_m284215335 ();
extern "C" void EncoderFallback_get_ExceptionFallback_m1464972749 ();
extern "C" void EncoderFallback_get_ReplacementFallback_m3827922266 ();
extern "C" void EncoderFallback_get_StandardSafeFallback_m1851577631 ();
extern "C" void EncoderFallbackBuffer__ctor_m3569152056 ();
extern "C" void EncoderFallbackException__ctor_m2123956912 ();
extern "C" void EncoderFallbackException__ctor_m781728286 ();
extern "C" void EncoderFallbackException__ctor_m2977126351 ();
extern "C" void EncoderFallbackException__ctor_m2113416521 ();
extern "C" void EncoderReplacementFallback__ctor_m2633060057 ();
extern "C" void EncoderReplacementFallback__ctor_m1548856421 ();
extern "C" void EncoderReplacementFallback_get_DefaultString_m1152386198 ();
extern "C" void EncoderReplacementFallback_CreateFallbackBuffer_m2288764815 ();
extern "C" void EncoderReplacementFallback_Equals_m1754666140 ();
extern "C" void EncoderReplacementFallback_GetHashCode_m2810870492 ();
extern "C" void EncoderReplacementFallbackBuffer__ctor_m3187926460 ();
extern "C" void EncoderReplacementFallbackBuffer_get_Remaining_m596312523 ();
extern "C" void EncoderReplacementFallbackBuffer_Fallback_m4112181947 ();
extern "C" void EncoderReplacementFallbackBuffer_Fallback_m1000484618 ();
extern "C" void EncoderReplacementFallbackBuffer_Fallback_m4121272877 ();
extern "C" void EncoderReplacementFallbackBuffer_GetNextChar_m865947909 ();
extern "C" void Encoding__ctor_m455606385 ();
extern "C" void Encoding__ctor_m2364596850 ();
extern "C" void Encoding__cctor_m3823259146 ();
extern "C" void Encoding___m1546889080 ();
extern "C" void Encoding_get_IsReadOnly_m799279676 ();
extern "C" void Encoding_get_DecoderFallback_m968544798 ();
extern "C" void Encoding_set_DecoderFallback_m3013932350 ();
extern "C" void Encoding_get_EncoderFallback_m789384937 ();
extern "C" void Encoding_SetFallbackInternal_m994007432 ();
extern "C" void Encoding_Equals_m452169602 ();
extern "C" void Encoding_GetByteCount_m4227186505 ();
extern "C" void Encoding_GetByteCount_m3010343368 ();
extern "C" void Encoding_GetBytes_m3488625454 ();
extern "C" void Encoding_GetBytes_m2873502639 ();
extern "C" void Encoding_GetBytes_m1593548296 ();
extern "C" void Encoding_GetBytes_m1025265848 ();
extern "C" void Encoding_GetChars_m435610014 ();
extern "C" void Encoding_GetDecoder_m53929652 ();
extern "C" void Encoding_InvokeI18N_m3158856541 ();
extern "C" void Encoding_GetEncoding_m773485090 ();
extern "C" void Encoding_Clone_m1538624630 ();
extern "C" void Encoding_GetEncoding_m535151945 ();
extern "C" void Encoding_GetHashCode_m3895090026 ();
extern "C" void Encoding_GetPreamble_m2460099611 ();
extern "C" void Encoding_GetString_m2468918681 ();
extern "C" void Encoding_GetString_m3245103957 ();
extern "C" void Encoding_get_ASCII_m2386167277 ();
extern "C" void Encoding_get_BigEndianUnicode_m747476343 ();
extern "C" void Encoding_InternalCodePage_m3807295418 ();
extern "C" void Encoding_get_Default_m1892828879 ();
extern "C" void Encoding_get_ISOLatin1_m2102831549 ();
extern "C" void Encoding_get_UTF7_m3894984704 ();
extern "C" void Encoding_get_UTF8_m1678229949 ();
extern "C" void Encoding_get_UTF8Unmarked_m4114693721 ();
extern "C" void Encoding_get_UTF8UnmarkedUnsafe_m4093082537 ();
extern "C" void Encoding_get_Unicode_m3505795070 ();
extern "C" void Encoding_get_UTF32_m3017339306 ();
extern "C" void Encoding_get_BigEndianUTF32_m334537726 ();
extern "C" void Encoding_GetByteCount_m2900855870 ();
extern "C" void Encoding_GetBytes_m3914031873 ();
extern "C" void ForwardingDecoder__ctor_m663073553 ();
extern "C" void ForwardingDecoder_GetChars_m1871710230 ();
extern "C" void Latin1Encoding__ctor_m249219529 ();
extern "C" void Latin1Encoding_GetByteCount_m3568441451 ();
extern "C" void Latin1Encoding_GetByteCount_m794715241 ();
extern "C" void Latin1Encoding_GetBytes_m3181550128 ();
extern "C" void Latin1Encoding_GetBytes_m2652421294 ();
extern "C" void Latin1Encoding_GetBytes_m3220298131 ();
extern "C" void Latin1Encoding_GetBytes_m459498377 ();
extern "C" void Latin1Encoding_GetCharCount_m1673711890 ();
extern "C" void Latin1Encoding_GetChars_m1671534508 ();
extern "C" void Latin1Encoding_GetMaxByteCount_m3041479951 ();
extern "C" void Latin1Encoding_GetMaxCharCount_m1595185718 ();
extern "C" void Latin1Encoding_GetString_m58970960 ();
extern "C" void Latin1Encoding_GetString_m4212017743 ();
extern "C" void StringBuilder__ctor_m3733079872 ();
extern "C" void StringBuilder__ctor_m2868659477 ();
extern "C" void StringBuilder__ctor_m3704793828 ();
extern "C" void StringBuilder__ctor_m2602435243 ();
extern "C" void StringBuilder__ctor_m473439139 ();
extern "C" void StringBuilder__ctor_m750369356 ();
extern "C" void StringBuilder__ctor_m520041810 ();
extern "C" void StringBuilder_get_Capacity_m4150733028 ();
extern "C" void StringBuilder_set_Capacity_m1854997739 ();
extern "C" void StringBuilder_get_Length_m567963887 ();
extern "C" void StringBuilder_set_Length_m3370055684 ();
extern "C" void StringBuilder_get_Chars_m819910185 ();
extern "C" void StringBuilder_set_Chars_m3142390845 ();
extern "C" void StringBuilder_ToString_m2645140727 ();
extern "C" void StringBuilder_ToString_m3822198548 ();
extern "C" void StringBuilder_Remove_m1534981456 ();
extern "C" void StringBuilder_Replace_m880021320 ();
extern "C" void StringBuilder_Replace_m3626715581 ();
extern "C" void StringBuilder_Append_m4203923530 ();
extern "C" void StringBuilder_Append_m1994745533 ();
extern "C" void StringBuilder_Append_m426372261 ();
extern "C" void StringBuilder_Append_m234831009 ();
extern "C" void StringBuilder_Append_m2407468139 ();
extern "C" void StringBuilder_Append_m3281326376 ();
extern "C" void StringBuilder_Append_m4271635713 ();
extern "C" void StringBuilder_Append_m4169899056 ();
extern "C" void StringBuilder_AppendFormat_m2847832545 ();
extern "C" void StringBuilder_AppendFormat_m2246691282 ();
extern "C" void StringBuilder_AppendFormat_m2570135551 ();
extern "C" void StringBuilder_AppendFormat_m2147218040 ();
extern "C" void StringBuilder_AppendFormat_m2206827616 ();
extern "C" void StringBuilder_Insert_m2512051818 ();
extern "C" void StringBuilder_Insert_m4102175982 ();
extern "C" void StringBuilder_Insert_m345379931 ();
extern "C" void StringBuilder_InternalEnsureCapacity_m2076946123 ();
extern "C" void UnicodeEncoding__ctor_m1220127606 ();
extern "C" void UnicodeEncoding__ctor_m199533177 ();
extern "C" void UnicodeEncoding__ctor_m239671188 ();
extern "C" void UnicodeEncoding_GetByteCount_m1953964001 ();
extern "C" void UnicodeEncoding_GetByteCount_m3174918053 ();
extern "C" void UnicodeEncoding_GetByteCount_m3150074527 ();
extern "C" void UnicodeEncoding_GetBytes_m572689233 ();
extern "C" void UnicodeEncoding_GetBytes_m535551194 ();
extern "C" void UnicodeEncoding_GetBytes_m3268668497 ();
extern "C" void UnicodeEncoding_GetBytesInternal_m2544128545 ();
extern "C" void UnicodeEncoding_GetCharCount_m3065712957 ();
extern "C" void UnicodeEncoding_GetChars_m3267083662 ();
extern "C" void UnicodeEncoding_GetString_m1015926560 ();
extern "C" void UnicodeEncoding_GetCharsInternal_m3032390807 ();
extern "C" void UnicodeEncoding_GetMaxByteCount_m4079936646 ();
extern "C" void UnicodeEncoding_GetMaxCharCount_m687012648 ();
extern "C" void UnicodeEncoding_GetDecoder_m763929548 ();
extern "C" void UnicodeEncoding_GetPreamble_m3175003997 ();
extern "C" void UnicodeEncoding_Equals_m3524788131 ();
extern "C" void UnicodeEncoding_GetHashCode_m1369621258 ();
extern "C" void UnicodeEncoding_CopyChars_m768483352 ();
extern "C" void UnicodeDecoder__ctor_m3834315701 ();
extern "C" void UnicodeDecoder_GetChars_m4285425991 ();
extern "C" void UTF32Encoding__ctor_m470477956 ();
extern "C" void UTF32Encoding__ctor_m1739533329 ();
extern "C" void UTF32Encoding__ctor_m970607301 ();
extern "C" void UTF32Encoding_GetByteCount_m1402347958 ();
extern "C" void UTF32Encoding_GetBytes_m4274563278 ();
extern "C" void UTF32Encoding_GetCharCount_m800650398 ();
extern "C" void UTF32Encoding_GetChars_m4077621955 ();
extern "C" void UTF32Encoding_GetMaxByteCount_m1299013354 ();
extern "C" void UTF32Encoding_GetMaxCharCount_m1345454425 ();
extern "C" void UTF32Encoding_GetDecoder_m2722361452 ();
extern "C" void UTF32Encoding_GetPreamble_m1463944593 ();
extern "C" void UTF32Encoding_Equals_m496542124 ();
extern "C" void UTF32Encoding_GetHashCode_m1430962791 ();
extern "C" void UTF32Encoding_GetByteCount_m2178484365 ();
extern "C" void UTF32Encoding_GetByteCount_m1567370710 ();
extern "C" void UTF32Encoding_GetBytes_m3528139859 ();
extern "C" void UTF32Encoding_GetBytes_m275935333 ();
extern "C" void UTF32Encoding_GetString_m3750925481 ();
extern "C" void UTF32Decoder__ctor_m1099217577 ();
extern "C" void UTF32Decoder_GetChars_m2709987282 ();
extern "C" void UTF7Encoding__ctor_m955668561 ();
extern "C" void UTF7Encoding__ctor_m2115977681 ();
extern "C" void UTF7Encoding__cctor_m2972476810 ();
extern "C" void UTF7Encoding_GetHashCode_m3728423310 ();
extern "C" void UTF7Encoding_Equals_m3440788742 ();
extern "C" void UTF7Encoding_InternalGetByteCount_m2627284808 ();
extern "C" void UTF7Encoding_GetByteCount_m2460219089 ();
extern "C" void UTF7Encoding_InternalGetBytes_m4200488742 ();
extern "C" void UTF7Encoding_GetBytes_m2085816167 ();
extern "C" void UTF7Encoding_InternalGetCharCount_m973241758 ();
extern "C" void UTF7Encoding_GetCharCount_m2309839641 ();
extern "C" void UTF7Encoding_InternalGetChars_m3577722124 ();
extern "C" void UTF7Encoding_GetChars_m2378901927 ();
extern "C" void UTF7Encoding_GetMaxByteCount_m1237040058 ();
extern "C" void UTF7Encoding_GetMaxCharCount_m4023184980 ();
extern "C" void UTF7Encoding_GetDecoder_m2480715294 ();
extern "C" void UTF7Encoding_GetByteCount_m4153636640 ();
extern "C" void UTF7Encoding_GetByteCount_m260339288 ();
extern "C" void UTF7Encoding_GetBytes_m3276108052 ();
extern "C" void UTF7Encoding_GetBytes_m565634380 ();
extern "C" void UTF7Encoding_GetString_m753427559 ();
extern "C" void UTF7Decoder__ctor_m219610152 ();
extern "C" void UTF7Decoder_GetChars_m728310123 ();
extern "C" void UTF8Encoding__ctor_m1946666566 ();
extern "C" void UTF8Encoding__ctor_m4021640945 ();
extern "C" void UTF8Encoding__ctor_m913665375 ();
extern "C" void UTF8Encoding_InternalGetByteCount_m625368048 ();
extern "C" void UTF8Encoding_InternalGetByteCount_m921360822 ();
extern "C" void UTF8Encoding_GetByteCount_m584436224 ();
extern "C" void UTF8Encoding_GetByteCount_m2144817935 ();
extern "C" void UTF8Encoding_InternalGetBytes_m4284173573 ();
extern "C" void UTF8Encoding_InternalGetBytes_m324560689 ();
extern "C" void UTF8Encoding_GetBytes_m1324440997 ();
extern "C" void UTF8Encoding_GetBytes_m1555704259 ();
extern "C" void UTF8Encoding_GetBytes_m1154207544 ();
extern "C" void UTF8Encoding_InternalGetCharCount_m2193460697 ();
extern "C" void UTF8Encoding_InternalGetCharCount_m2948469514 ();
extern "C" void UTF8Encoding_Fallback_m4168354327 ();
extern "C" void UTF8Encoding_Fallback_m731144313 ();
extern "C" void UTF8Encoding_GetCharCount_m1768270997 ();
extern "C" void UTF8Encoding_InternalGetChars_m3696140585 ();
extern "C" void UTF8Encoding_InternalGetChars_m2862833516 ();
extern "C" void UTF8Encoding_GetChars_m2967239546 ();
extern "C" void UTF8Encoding_GetMaxByteCount_m1472884927 ();
extern "C" void UTF8Encoding_GetMaxCharCount_m654358457 ();
extern "C" void UTF8Encoding_GetDecoder_m2371355084 ();
extern "C" void UTF8Encoding_GetPreamble_m88423642 ();
extern "C" void UTF8Encoding_Equals_m3263540439 ();
extern "C" void UTF8Encoding_GetHashCode_m371853634 ();
extern "C" void UTF8Encoding_GetByteCount_m3476056399 ();
extern "C" void UTF8Encoding_GetString_m774016146 ();
extern "C" void UTF8Decoder__ctor_m920506496 ();
extern "C" void UTF8Decoder_GetChars_m4196807859 ();
extern "C" void CompressedStack__ctor_m1057186699 ();
extern "C" void CompressedStack__ctor_m112312717 ();
extern "C" void CompressedStack_CreateCopy_m233976566 ();
extern "C" void CompressedStack_Capture_m1021096277 ();
extern "C" void CompressedStack_IsEmpty_m1519192734 ();
extern "C" void EventWaitHandle__ctor_m2331070755 ();
extern "C" void EventWaitHandle_IsManualReset_m1306907451 ();
extern "C" void EventWaitHandle_Reset_m2787136410 ();
extern "C" void EventWaitHandle_Set_m1536670572 ();
extern "C" void ExecutionContext__ctor_m1553713310 ();
extern "C" void ExecutionContext__ctor_m1482236790 ();
extern "C" void ExecutionContext__ctor_m184114300 ();
extern "C" void ExecutionContext_Capture_m3220815208 ();
extern "C" void ExecutionContext_get_SecurityContext_m390895419 ();
extern "C" void ExecutionContext_set_SecurityContext_m2351306740 ();
extern "C" void ExecutionContext_get_FlowSuppressed_m2397910917 ();
extern "C" void ExecutionContext_IsFlowSuppressed_m300887835 ();
extern "C" void Interlocked_CompareExchange_m2168439704 ();
extern "C" void ManualResetEvent__ctor_m2560393312 ();
extern "C" void Monitor_Enter_m309883778 ();
extern "C" void Monitor_Exit_m1372154428 ();
extern "C" void Monitor_Monitor_pulse_m1802009538 ();
extern "C" void Monitor_Monitor_test_synchronised_m2218470822 ();
extern "C" void Monitor_Pulse_m2424390085 ();
extern "C" void Monitor_Monitor_wait_m1777647501 ();
extern "C" void Monitor_Wait_m864905364 ();
extern "C" void Mutex__ctor_m2870296590 ();
extern "C" void Mutex_CreateMutex_internal_m663697220 ();
extern "C" void Mutex_ReleaseMutex_internal_m999149428 ();
extern "C" void Mutex_ReleaseMutex_m1972465564 ();
extern "C" void NativeEventCalls_CreateEvent_internal_m4252918095 ();
extern "C" void NativeEventCalls_SetEvent_internal_m1514150800 ();
extern "C" void NativeEventCalls_ResetEvent_internal_m3468928099 ();
extern "C" void NativeEventCalls_CloseEvent_internal_m303970804 ();
extern "C" void SendOrPostCallback__ctor_m416373199 ();
extern "C" void SendOrPostCallback_Invoke_m2866084970 ();
extern "C" void SendOrPostCallback_BeginInvoke_m1762431920 ();
extern "C" void SendOrPostCallback_EndInvoke_m1650533905 ();
extern "C" void SynchronizationContext__ctor_m4018090004 ();
extern "C" void SynchronizationContext_get_Current_m925115792 ();
extern "C" void SynchronizationContext_SetSynchronizationContext_m3224732528 ();
extern "C" void SynchronizationLockException__ctor_m4232452005 ();
extern "C" void SynchronizationLockException__ctor_m3095735310 ();
extern "C" void SynchronizationLockException__ctor_m1644078897 ();
extern "C" void Thread__ctor_m1218933919 ();
extern "C" void Thread__cctor_m2410562571 ();
extern "C" void Thread_get_CurrentContext_m2699842704 ();
extern "C" void Thread_CurrentThread_internal_m891913787 ();
extern "C" void Thread_get_CurrentThread_m3312024903 ();
extern "C" void Thread_FreeLocalSlotValues_m995133886 ();
extern "C" void Thread_GetDomainID_m3556191158 ();
extern "C" void Thread_Thread_internal_m3936584328 ();
extern "C" void Thread_Thread_init_m440407393 ();
extern "C" void Thread_GetCachedCurrentCulture_m2839467004 ();
extern "C" void Thread_GetSerializedCurrentCulture_m4005324856 ();
extern "C" void Thread_SetCachedCurrentCulture_m1281481126 ();
extern "C" void Thread_GetCachedCurrentUICulture_m85626342 ();
extern "C" void Thread_GetSerializedCurrentUICulture_m2578392406 ();
extern "C" void Thread_SetCachedCurrentUICulture_m1065553103 ();
extern "C" void Thread_get_CurrentCulture_m640162036 ();
extern "C" void Thread_get_CurrentUICulture_m3790386189 ();
extern "C" void Thread_set_IsBackground_m2646175327 ();
extern "C" void Thread_SetName_internal_m98894528 ();
extern "C" void Thread_set_Name_m3218111234 ();
extern "C" void Thread_Start_m1856932251 ();
extern "C" void Thread_Thread_free_internal_m640920438 ();
extern "C" void Thread_Finalize_m773434565 ();
extern "C" void Thread_SetState_m3834474597 ();
extern "C" void Thread_ClrState_m396238162 ();
extern "C" void Thread_GetNewManagedId_m3868873597 ();
extern "C" void Thread_GetNewManagedId_internal_m3419403436 ();
extern "C" void Thread_get_ExecutionContext_m2718260421 ();
extern "C" void Thread_get_ManagedThreadId_m2540698636 ();
extern "C" void Thread_GetHashCode_m1763772112 ();
extern "C" void Thread_GetCompressedStack_m2176559759 ();
extern "C" void ThreadAbortException__ctor_m1292743711 ();
extern "C" void ThreadAbortException__ctor_m3273094655 ();
extern "C" void ThreadInterruptedException__ctor_m765906674 ();
extern "C" void ThreadInterruptedException__ctor_m264383818 ();
extern "C" void ThreadPool_QueueUserWorkItem_m3667709369 ();
extern "C" void ThreadStart__ctor_m176658305 ();
extern "C" void ThreadStart_Invoke_m896459897 ();
extern "C" void ThreadStart_BeginInvoke_m1657674684 ();
extern "C" void ThreadStart_EndInvoke_m2226254000 ();
extern "C" void ThreadStateException__ctor_m4239605846 ();
extern "C" void ThreadStateException__ctor_m1274041360 ();
extern "C" void Timer__cctor_m3385754584 ();
extern "C" void Timer_Change_m876470576 ();
extern "C" void Timer_Dispose_m2687405967 ();
extern "C" void Timer_Change_m2141970086 ();
extern "C" void Scheduler__ctor_m3901549269 ();
extern "C" void Scheduler__cctor_m2851743699 ();
extern "C" void Scheduler_get_Instance_m3397409287 ();
extern "C" void Scheduler_Remove_m4169009551 ();
extern "C" void Scheduler_Change_m2902544748 ();
extern "C" void Scheduler_Add_m3132001641 ();
extern "C" void Scheduler_InternalRemove_m2025230331 ();
extern "C" void Scheduler_SchedulerThread_m44863378 ();
extern "C" void Scheduler_ShrinkIfNeeded_m122939492 ();
extern "C" void TimerComparer__ctor_m3265754441 ();
extern "C" void TimerComparer_Compare_m3785057619 ();
extern "C" void TimerCallback__ctor_m54121087 ();
extern "C" void TimerCallback_Invoke_m3208045712 ();
extern "C" void TimerCallback_BeginInvoke_m1465134106 ();
extern "C" void TimerCallback_EndInvoke_m1103917671 ();
extern "C" void WaitCallback__ctor_m559565817 ();
extern "C" void WaitCallback_Invoke_m46906208 ();
extern "C" void WaitCallback_BeginInvoke_m1413555015 ();
extern "C" void WaitCallback_EndInvoke_m3269205806 ();
extern "C" void WaitHandle__ctor_m3660572202 ();
extern "C" void WaitHandle__cctor_m1541354027 ();
extern "C" void WaitHandle_System_IDisposable_Dispose_m1844882303 ();
extern "C" void WaitHandle_get_Handle_m2944703788 ();
extern "C" void WaitHandle_set_Handle_m477214614 ();
extern "C" void WaitHandle_WaitOne_internal_m2537485944 ();
extern "C" void WaitHandle_Dispose_m3946988443 ();
extern "C" void WaitHandle_WaitOne_m712082545 ();
extern "C" void WaitHandle_WaitOne_m3888614806 ();
extern "C" void WaitHandle_CheckDisposed_m2071914923 ();
extern "C" void WaitHandle_Finalize_m1085828110 ();
extern "C" void ThreadStaticAttribute__ctor_m778228724 ();
extern "C" void TimeSpan__ctor_m1003715641_AdjustorThunk ();
extern "C" void TimeSpan__ctor_m959658474_AdjustorThunk ();
extern "C" void TimeSpan__ctor_m2308561733_AdjustorThunk ();
extern "C" void TimeSpan__cctor_m3835473733 ();
extern "C" void TimeSpan_CalculateTicks_m3509558447 ();
extern "C" void TimeSpan_get_Days_m2704054316_AdjustorThunk ();
extern "C" void TimeSpan_get_Hours_m781259545_AdjustorThunk ();
extern "C" void TimeSpan_get_Milliseconds_m2373414053_AdjustorThunk ();
extern "C" void TimeSpan_get_Minutes_m2209932375_AdjustorThunk ();
extern "C" void TimeSpan_get_Seconds_m4112581433_AdjustorThunk ();
extern "C" void TimeSpan_get_Ticks_m3409200361_AdjustorThunk ();
extern "C" void TimeSpan_get_TotalDays_m3448510689_AdjustorThunk ();
extern "C" void TimeSpan_get_TotalHours_m3010569118_AdjustorThunk ();
extern "C" void TimeSpan_get_TotalMilliseconds_m1329041946_AdjustorThunk ();
extern "C" void TimeSpan_get_TotalMinutes_m3004298888_AdjustorThunk ();
extern "C" void TimeSpan_get_TotalSeconds_m1045324781_AdjustorThunk ();
extern "C" void TimeSpan_Add_m1021118328_AdjustorThunk ();
extern "C" void TimeSpan_Compare_m1407526137 ();
extern "C" void TimeSpan_CompareTo_m3267940589_AdjustorThunk ();
extern "C" void TimeSpan_CompareTo_m3198449526_AdjustorThunk ();
extern "C" void TimeSpan_Equals_m1555084785_AdjustorThunk ();
extern "C" void TimeSpan_Duration_m234222681_AdjustorThunk ();
extern "C" void TimeSpan_Equals_m2659880825_AdjustorThunk ();
extern "C" void TimeSpan_FromDays_m3939268644 ();
extern "C" void TimeSpan_FromHours_m3350197084 ();
extern "C" void TimeSpan_FromMinutes_m3157462784 ();
extern "C" void TimeSpan_FromSeconds_m3012122711 ();
extern "C" void TimeSpan_FromMilliseconds_m229107921 ();
extern "C" void TimeSpan_From_m144412854 ();
extern "C" void TimeSpan_GetHashCode_m3640128864_AdjustorThunk ();
extern "C" void TimeSpan_Negate_m1765346103_AdjustorThunk ();
extern "C" void TimeSpan_Subtract_m454324943_AdjustorThunk ();
extern "C" void TimeSpan_ToString_m472795270_AdjustorThunk ();
extern "C" void TimeSpan_op_Addition_m3775597963 ();
extern "C" void TimeSpan_op_Equality_m463025364 ();
extern "C" void TimeSpan_op_GreaterThan_m687125154 ();
extern "C" void TimeSpan_op_GreaterThanOrEqual_m691354355 ();
extern "C" void TimeSpan_op_Inequality_m2743874126 ();
extern "C" void TimeSpan_op_LessThan_m2900781947 ();
extern "C" void TimeSpan_op_LessThanOrEqual_m3930938175 ();
extern "C" void TimeSpan_op_Subtraction_m2472412875 ();
extern "C" void TimeZone__ctor_m3782519645 ();
extern "C" void TimeZone__cctor_m744168230 ();
extern "C" void TimeZone_get_CurrentTimeZone_m3918037878 ();
extern "C" void TimeZone_IsDaylightSavingTime_m914872925 ();
extern "C" void TimeZone_IsDaylightSavingTime_m1594110884 ();
extern "C" void TimeZone_ToLocalTime_m1094100684 ();
extern "C" void TimeZone_ToUniversalTime_m376605559 ();
extern "C" void TimeZone_GetLocalTimeDiff_m1621322960 ();
extern "C" void TimeZone_GetLocalTimeDiff_m3151638654 ();
extern "C" void Type__ctor_m930283407 ();
extern "C" void Type__cctor_m3931795912 ();
extern "C" void Type_FilterName_impl_m191564066 ();
extern "C" void Type_FilterNameIgnoreCase_impl_m2106047663 ();
extern "C" void Type_FilterAttribute_impl_m1030794727 ();
extern "C" void Type_get_Attributes_m364001767 ();
extern "C" void Type_get_DeclaringType_m752513822 ();
extern "C" void Type_get_HasElementType_m1699071148 ();
extern "C" void Type_get_IsAbstract_m3394539562 ();
extern "C" void Type_get_IsArray_m745419188 ();
extern "C" void Type_get_IsByRef_m184143203 ();
extern "C" void Type_get_IsClass_m1661969002 ();
extern "C" void Type_get_IsContextful_m2705557627 ();
extern "C" void Type_get_IsEnum_m1948977141 ();
extern "C" void Type_get_IsExplicitLayout_m599632686 ();
extern "C" void Type_get_IsInterface_m788827182 ();
extern "C" void Type_get_IsMarshalByRef_m1952159066 ();
extern "C" void Type_get_IsPointer_m513263405 ();
extern "C" void Type_get_IsPrimitive_m3385012571 ();
extern "C" void Type_get_IsSealed_m1431910581 ();
extern "C" void Type_get_IsSerializable_m3603849484 ();
extern "C" void Type_get_IsValueType_m2191848361 ();
extern "C" void Type_get_MemberType_m1088770117 ();
extern "C" void Type_get_ReflectedType_m3500185842 ();
extern "C" void Type_get_TypeHandle_m3673387995 ();
extern "C" void Type_Equals_m843526326 ();
extern "C" void Type_Equals_m911247780 ();
extern "C" void Type_EqualsInternal_m2297504039 ();
extern "C" void Type_internal_from_handle_m2084068818 ();
extern "C" void Type_internal_from_name_m1153252880 ();
extern "C" void Type_GetType_m931458164 ();
extern "C" void Type_GetType_m418477468 ();
extern "C" void Type_GetTypeCodeInternal_m197246513 ();
extern "C" void Type_GetTypeCode_m2853463675 ();
extern "C" void Type_GetTypeFromHandle_m3171413529 ();
extern "C" void Type_type_is_subtype_of_m473101958 ();
extern "C" void Type_type_is_assignable_from_m1933252983 ();
extern "C" void Type_IsSubclassOf_m61888076 ();
extern "C" void Type_IsAssignableFrom_m3874020498 ();
extern "C" void Type_IsInstanceOfType_m2795469003 ();
extern "C" void Type_GetField_m1925245366 ();
extern "C" void Type_GetHashCode_m3495876822 ();
extern "C" void Type_GetMethod_m2744573080 ();
extern "C" void Type_GetMethod_m1772015249 ();
extern "C" void Type_GetMethod_m3484360104 ();
extern "C" void Type_GetMethod_m3039077285 ();
extern "C" void Type_GetProperty_m3255922200 ();
extern "C" void Type_GetProperty_m1532833315 ();
extern "C" void Type_GetProperty_m567419265 ();
extern "C" void Type_GetProperty_m1936989742 ();
extern "C" void Type_GetProperty_m3035335666 ();
extern "C" void Type_IsArrayImpl_m330107357 ();
extern "C" void Type_IsValueTypeImpl_m238784021 ();
extern "C" void Type_IsContextfulImpl_m4293596303 ();
extern "C" void Type_IsMarshalByRefImpl_m2853733014 ();
extern "C" void Type_GetConstructor_m272770726 ();
extern "C" void Type_GetConstructor_m2099858645 ();
extern "C" void Type_GetConstructor_m3078335309 ();
extern "C" void Type_ToString_m2928545488 ();
extern "C" void Type_get_IsSystemType_m660774764 ();
extern "C" void Type_GetGenericArguments_m2511307491 ();
extern "C" void Type_get_ContainsGenericParameters_m13871687 ();
extern "C" void Type_get_IsGenericTypeDefinition_m724009791 ();
extern "C" void Type_GetGenericTypeDefinition_impl_m1657286486 ();
extern "C" void Type_GetGenericTypeDefinition_m239171289 ();
extern "C" void Type_get_IsGenericType_m2317020012 ();
extern "C" void Type_MakeGenericType_m1957991317 ();
extern "C" void Type_MakeGenericType_m4233399084 ();
extern "C" void Type_get_IsGenericParameter_m1326572191 ();
extern "C" void Type_get_IsNested_m2978419709 ();
extern "C" void Type_GetPseudoCustomAttributes_m2605516109 ();
extern "C" void TypedReference_Equals_m3326408017_AdjustorThunk ();
extern "C" void TypedReference_GetHashCode_m4094619062_AdjustorThunk ();
extern "C" void TypeInitializationException__ctor_m1757867175 ();
extern "C" void TypeLoadException__ctor_m2399716549 ();
extern "C" void TypeLoadException__ctor_m2719585591 ();
extern "C" void TypeLoadException__ctor_m243829707 ();
extern "C" void TypeLoadException__ctor_m3378867843 ();
extern "C" void TypeLoadException_get_Message_m3625839464 ();
extern "C" void UInt16_System_IConvertible_ToBoolean_m1772144888_AdjustorThunk ();
extern "C" void UInt16_System_IConvertible_ToByte_m2672866721_AdjustorThunk ();
extern "C" void UInt16_System_IConvertible_ToChar_m872046169_AdjustorThunk ();
extern "C" void UInt16_System_IConvertible_ToDateTime_m2415933095_AdjustorThunk ();
extern "C" void UInt16_System_IConvertible_ToDecimal_m3032145820_AdjustorThunk ();
extern "C" void UInt16_System_IConvertible_ToDouble_m1831865283_AdjustorThunk ();
extern "C" void UInt16_System_IConvertible_ToInt16_m2949227146_AdjustorThunk ();
extern "C" void UInt16_System_IConvertible_ToInt32_m3293594369_AdjustorThunk ();
extern "C" void UInt16_System_IConvertible_ToInt64_m552616453_AdjustorThunk ();
extern "C" void UInt16_System_IConvertible_ToSByte_m3483965902_AdjustorThunk ();
extern "C" void UInt16_System_IConvertible_ToSingle_m167759059_AdjustorThunk ();
extern "C" void UInt16_System_IConvertible_ToType_m379025759_AdjustorThunk ();
extern "C" void UInt16_System_IConvertible_ToUInt16_m1894970504_AdjustorThunk ();
extern "C" void UInt16_System_IConvertible_ToUInt32_m2992238646_AdjustorThunk ();
extern "C" void UInt16_System_IConvertible_ToUInt64_m1362953793_AdjustorThunk ();
extern "C" void UInt16_CompareTo_m4006209376_AdjustorThunk ();
extern "C" void UInt16_Equals_m2071734182_AdjustorThunk ();
extern "C" void UInt16_GetHashCode_m1814971889_AdjustorThunk ();
extern "C" void UInt16_CompareTo_m4172686937_AdjustorThunk ();
extern "C" void UInt16_Equals_m3634007042_AdjustorThunk ();
extern "C" void UInt16_Parse_m1722685482 ();
extern "C" void UInt16_Parse_m1446859916 ();
extern "C" void UInt16_TryParse_m78750700 ();
extern "C" void UInt16_TryParse_m209638213 ();
extern "C" void UInt16_ToString_m1179315216_AdjustorThunk ();
extern "C" void UInt16_ToString_m1842294654_AdjustorThunk ();
extern "C" void UInt16_ToString_m45510773_AdjustorThunk ();
extern "C" void UInt16_ToString_m3156601356_AdjustorThunk ();
extern "C" void UInt32_System_IConvertible_ToBoolean_m670198194_AdjustorThunk ();
extern "C" void UInt32_System_IConvertible_ToByte_m3042305990_AdjustorThunk ();
extern "C" void UInt32_System_IConvertible_ToChar_m28659192_AdjustorThunk ();
extern "C" void UInt32_System_IConvertible_ToDateTime_m2962576325_AdjustorThunk ();
extern "C" void UInt32_System_IConvertible_ToDecimal_m2984733657_AdjustorThunk ();
extern "C" void UInt32_System_IConvertible_ToDouble_m2650942160_AdjustorThunk ();
extern "C" void UInt32_System_IConvertible_ToInt16_m2400636590_AdjustorThunk ();
extern "C" void UInt32_System_IConvertible_ToInt32_m1930688566_AdjustorThunk ();
extern "C" void UInt32_System_IConvertible_ToInt64_m3440018551_AdjustorThunk ();
extern "C" void UInt32_System_IConvertible_ToSByte_m3762512969_AdjustorThunk ();
extern "C" void UInt32_System_IConvertible_ToSingle_m3653506289_AdjustorThunk ();
extern "C" void UInt32_System_IConvertible_ToType_m3214923870_AdjustorThunk ();
extern "C" void UInt32_System_IConvertible_ToUInt16_m3301943048_AdjustorThunk ();
extern "C" void UInt32_System_IConvertible_ToUInt32_m1272215724_AdjustorThunk ();
extern "C" void UInt32_System_IConvertible_ToUInt64_m3268886020_AdjustorThunk ();
extern "C" void UInt32_CompareTo_m3050130732_AdjustorThunk ();
extern "C" void UInt32_Equals_m4051847291_AdjustorThunk ();
extern "C" void UInt32_GetHashCode_m329710466_AdjustorThunk ();
extern "C" void UInt32_CompareTo_m4043248230_AdjustorThunk ();
extern "C" void UInt32_Equals_m3622421884_AdjustorThunk ();
extern "C" void UInt32_Parse_m2924183215 ();
extern "C" void UInt32_Parse_m2694396900 ();
extern "C" void UInt32_Parse_m51087447 ();
extern "C" void UInt32_Parse_m1290187410 ();
extern "C" void UInt32_TryParse_m3247026924 ();
extern "C" void UInt32_TryParse_m1105936683 ();
extern "C" void UInt32_ToString_m1877455711_AdjustorThunk ();
extern "C" void UInt32_ToString_m2846686701_AdjustorThunk ();
extern "C" void UInt32_ToString_m2637752551_AdjustorThunk ();
extern "C" void UInt32_ToString_m3640631563_AdjustorThunk ();
extern "C" void UInt64_System_IConvertible_ToBoolean_m1215656230_AdjustorThunk ();
extern "C" void UInt64_System_IConvertible_ToByte_m510547492_AdjustorThunk ();
extern "C" void UInt64_System_IConvertible_ToChar_m1705077510_AdjustorThunk ();
extern "C" void UInt64_System_IConvertible_ToDateTime_m1487984179_AdjustorThunk ();
extern "C" void UInt64_System_IConvertible_ToDecimal_m4073095233_AdjustorThunk ();
extern "C" void UInt64_System_IConvertible_ToDouble_m3893284830_AdjustorThunk ();
extern "C" void UInt64_System_IConvertible_ToInt16_m1822107030_AdjustorThunk ();
extern "C" void UInt64_System_IConvertible_ToInt32_m3311852403_AdjustorThunk ();
extern "C" void UInt64_System_IConvertible_ToInt64_m2160517190_AdjustorThunk ();
extern "C" void UInt64_System_IConvertible_ToSByte_m83726035_AdjustorThunk ();
extern "C" void UInt64_System_IConvertible_ToSingle_m113327491_AdjustorThunk ();
extern "C" void UInt64_System_IConvertible_ToType_m1456734333_AdjustorThunk ();
extern "C" void UInt64_System_IConvertible_ToUInt16_m298580982_AdjustorThunk ();
extern "C" void UInt64_System_IConvertible_ToUInt32_m3366122957_AdjustorThunk ();
extern "C" void UInt64_System_IConvertible_ToUInt64_m769495286_AdjustorThunk ();
extern "C" void UInt64_CompareTo_m1716157644_AdjustorThunk ();
extern "C" void UInt64_Equals_m1119348279_AdjustorThunk ();
extern "C" void UInt64_GetHashCode_m808575860_AdjustorThunk ();
extern "C" void UInt64_CompareTo_m4190558311_AdjustorThunk ();
extern "C" void UInt64_Equals_m2428524726_AdjustorThunk ();
extern "C" void UInt64_Parse_m1110924678 ();
extern "C" void UInt64_Parse_m722310325 ();
extern "C" void UInt64_Parse_m1872577878 ();
extern "C" void UInt64_TryParse_m1628442159 ();
extern "C" void UInt64_ToString_m2334142373_AdjustorThunk ();
extern "C" void UInt64_ToString_m479609776_AdjustorThunk ();
extern "C" void UInt64_ToString_m623444807_AdjustorThunk ();
extern "C" void UInt64_ToString_m2651165391_AdjustorThunk ();
extern "C" void UIntPtr__ctor_m2323485797_AdjustorThunk ();
extern "C" void UIntPtr__cctor_m807304978 ();
extern "C" void UIntPtr_Equals_m1643140424_AdjustorThunk ();
extern "C" void UIntPtr_GetHashCode_m1760467123_AdjustorThunk ();
extern "C" void UIntPtr_ToString_m3571085837_AdjustorThunk ();
extern "C" void UnauthorizedAccessException__ctor_m1702503491 ();
extern "C" void UnauthorizedAccessException__ctor_m3177808355 ();
extern "C" void UnauthorizedAccessException__ctor_m3346029613 ();
extern "C" void UnhandledExceptionEventArgs__ctor_m3873837110 ();
extern "C" void UnhandledExceptionEventArgs_get_ExceptionObject_m2012398684 ();
extern "C" void UnhandledExceptionEventArgs_get_IsTerminating_m3281711421 ();
extern "C" void UnhandledExceptionEventHandler__ctor_m4070891721 ();
extern "C" void UnhandledExceptionEventHandler_Invoke_m651596339 ();
extern "C" void UnhandledExceptionEventHandler_BeginInvoke_m1490584764 ();
extern "C" void UnhandledExceptionEventHandler_EndInvoke_m1267772348 ();
extern "C" void ValueType__ctor_m3630339402 ();
extern "C" void ValueType_InternalEquals_m3908153565 ();
extern "C" void ValueType_DefaultEquals_m3990640312 ();
extern "C" void ValueType_Equals_m2470668449 ();
extern "C" void ValueType_InternalGetHashCode_m2863233978 ();
extern "C" void ValueType_GetHashCode_m321390148 ();
extern "C" void ValueType_ToString_m1188808705 ();
extern "C" void Version__ctor_m174414257 ();
extern "C" void Version__ctor_m2903744032 ();
extern "C" void Version__ctor_m3414114207 ();
extern "C" void Version__ctor_m997668137 ();
extern "C" void Version__ctor_m54823598 ();
extern "C" void Version_CheckedSet_m1756601458 ();
extern "C" void Version_get_Build_m3504770588 ();
extern "C" void Version_get_Major_m441588429 ();
extern "C" void Version_get_Minor_m3891662079 ();
extern "C" void Version_get_Revision_m2221074109 ();
extern "C" void Version_Clone_m2238283684 ();
extern "C" void Version_CompareTo_m1839308617 ();
extern "C" void Version_Equals_m572287887 ();
extern "C" void Version_CompareTo_m3067004787 ();
extern "C" void Version_Equals_m2191984290 ();
extern "C" void Version_GetHashCode_m3793907397 ();
extern "C" void Version_ToString_m146400385 ();
extern "C" void Version_CreateFromString_m1195539406 ();
extern "C" void Version_op_Equality_m2886948052 ();
extern "C" void Version_op_Inequality_m1171013002 ();
extern "C" void WeakReference__ctor_m3191982620 ();
extern "C" void WeakReference__ctor_m1159415219 ();
extern "C" void WeakReference__ctor_m1420801414 ();
extern "C" void WeakReference__ctor_m3560408400 ();
extern "C" void WeakReference_AllocateHandle_m3568204303 ();
extern "C" void WeakReference_get_IsAlive_m2821973386 ();
extern "C" void WeakReference_get_Target_m2733015050 ();
extern "C" void WeakReference_Finalize_m1590068820 ();
extern "C" void Locale_GetText_m2626776732 ();
extern "C" void Locale_GetText_m3635108345 ();
extern "C" void HybridDictionary__ctor_m3166222034 ();
extern "C" void HybridDictionary__ctor_m2525772109 ();
extern "C" void HybridDictionary_System_Collections_IEnumerable_GetEnumerator_m1770827537 ();
extern "C" void HybridDictionary_get_inner_m72932363 ();
extern "C" void HybridDictionary_get_Count_m4009529679 ();
extern "C" void HybridDictionary_get_Item_m3000032940 ();
extern "C" void HybridDictionary_set_Item_m3169884881 ();
extern "C" void HybridDictionary_get_SyncRoot_m94558822 ();
extern "C" void HybridDictionary_Add_m3500347722 ();
extern "C" void HybridDictionary_CopyTo_m3885249862 ();
extern "C" void HybridDictionary_GetEnumerator_m174533349 ();
extern "C" void HybridDictionary_Remove_m852034280 ();
extern "C" void HybridDictionary_Switch_m1535303149 ();
extern "C" void ListDictionary__ctor_m2698263728 ();
extern "C" void ListDictionary__ctor_m2304480032 ();
extern "C" void ListDictionary_System_Collections_IEnumerable_GetEnumerator_m425290031 ();
extern "C" void ListDictionary_FindEntry_m1451975503 ();
extern "C" void ListDictionary_FindEntry_m2656042279 ();
extern "C" void ListDictionary_AddImpl_m998935006 ();
extern "C" void ListDictionary_get_Count_m1430205299 ();
extern "C" void ListDictionary_get_SyncRoot_m3621968267 ();
extern "C" void ListDictionary_CopyTo_m728934192 ();
extern "C" void ListDictionary_get_Item_m3430875462 ();
extern "C" void ListDictionary_set_Item_m3558077738 ();
extern "C" void ListDictionary_Add_m3900846117 ();
extern "C" void ListDictionary_Clear_m1929591539 ();
extern "C" void ListDictionary_GetEnumerator_m1036563447 ();
extern "C" void ListDictionary_Remove_m3582970803 ();
extern "C" void DictionaryNode__ctor_m2812764942 ();
extern "C" void DictionaryNodeEnumerator__ctor_m3105575145 ();
extern "C" void DictionaryNodeEnumerator_FailFast_m962276816 ();
extern "C" void DictionaryNodeEnumerator_MoveNext_m2252273661 ();
extern "C" void DictionaryNodeEnumerator_Reset_m1713695554 ();
extern "C" void DictionaryNodeEnumerator_get_Current_m2052208030 ();
extern "C" void DictionaryNodeEnumerator_get_DictionaryNode_m2904091029 ();
extern "C" void DictionaryNodeEnumerator_get_Entry_m3422544269 ();
extern "C" void DictionaryNodeEnumerator_get_Key_m1136331636 ();
extern "C" void DictionaryNodeEnumerator_get_Value_m1963801786 ();
extern "C" void NameObjectCollectionBase__ctor_m1140799417 ();
extern "C" void NameObjectCollectionBase__ctor_m1872199601 ();
extern "C" void NameObjectCollectionBase_System_Collections_ICollection_get_SyncRoot_m4265226444 ();
extern "C" void NameObjectCollectionBase_System_Collections_ICollection_CopyTo_m3856561326 ();
extern "C" void NameObjectCollectionBase_Init_m1954961079 ();
extern "C" void NameObjectCollectionBase_get_Keys_m2738950250 ();
extern "C" void NameObjectCollectionBase_GetEnumerator_m934528883 ();
extern "C" void NameObjectCollectionBase_get_Count_m1728031609 ();
extern "C" void NameObjectCollectionBase_OnDeserialization_m2828687802 ();
extern "C" void NameObjectCollectionBase_get_IsReadOnly_m2819799010 ();
extern "C" void NameObjectCollectionBase_BaseAdd_m2255957085 ();
extern "C" void NameObjectCollectionBase_BaseGet_m3933727077 ();
extern "C" void NameObjectCollectionBase_BaseGet_m3607011638 ();
extern "C" void NameObjectCollectionBase_BaseGetKey_m4113912735 ();
extern "C" void NameObjectCollectionBase_FindFirstMatchedItem_m2942906622 ();
extern "C" void _Item__ctor_m2570877 ();
extern "C" void _KeysEnumerator__ctor_m1715162644 ();
extern "C" void _KeysEnumerator_get_Current_m427090870 ();
extern "C" void _KeysEnumerator_MoveNext_m1292840571 ();
extern "C" void _KeysEnumerator_Reset_m2886153187 ();
extern "C" void KeysCollection__ctor_m787396037 ();
extern "C" void KeysCollection_System_Collections_ICollection_CopyTo_m4003097592 ();
extern "C" void KeysCollection_System_Collections_ICollection_get_SyncRoot_m347632918 ();
extern "C" void KeysCollection_get_Count_m705263402 ();
extern "C" void KeysCollection_GetEnumerator_m207991736 ();
extern "C" void NameValueCollection__ctor_m2376174040 ();
extern "C" void NameValueCollection__ctor_m3698979679 ();
extern "C" void NameValueCollection_Add_m872765989 ();
extern "C" void NameValueCollection_Get_m136787640 ();
extern "C" void NameValueCollection_AsSingleString_m1841990898 ();
extern "C" void NameValueCollection_GetKey_m2727023721 ();
extern "C" void NameValueCollection_InvalidateCachedArrays_m2315504090 ();
extern "C" void TypeConverterAttribute__ctor_m2343876285 ();
extern "C" void TypeConverterAttribute__ctor_m2796508157 ();
extern "C" void TypeConverterAttribute__cctor_m3401448577 ();
extern "C" void TypeConverterAttribute_Equals_m1475102403 ();
extern "C" void TypeConverterAttribute_GetHashCode_m2802665213 ();
extern "C" void TypeConverterAttribute_get_ConverterTypeName_m2003125732 ();
extern "C" void DefaultUriParser__ctor_m3211200812 ();
extern "C" void DefaultUriParser__ctor_m3199576676 ();
extern "C" void MonoTODOAttribute__ctor_m423555603 ();
extern "C" void MonoTODOAttribute__ctor_m2017426088 ();
extern "C" void DefaultCertificatePolicy__ctor_m4284737171 ();
extern "C" void DefaultCertificatePolicy_CheckValidationResult_m2232677212 ();
extern "C" void FileWebRequest__ctor_m875314294 ();
extern "C" void FileWebRequest__ctor_m2545559098 ();
extern "C" void FileWebRequestCreator__ctor_m2001750601 ();
extern "C" void FileWebRequestCreator_Create_m2420640644 ();
extern "C" void FtpRequestCreator__ctor_m3936842167 ();
extern "C" void FtpRequestCreator_Create_m142425646 ();
extern "C" void FtpWebRequest__ctor_m2851367638 ();
extern "C" void FtpWebRequest__cctor_m2161426154 ();
extern "C" void FtpWebRequest_U3CcallbackU3Em__B_m1047401003 ();
extern "C" void GlobalProxySelection_get_Select_m1877492349 ();
extern "C" void HttpRequestCreator__ctor_m1871791882 ();
extern "C" void HttpRequestCreator_Create_m2191635693 ();
extern "C" void HttpVersion__cctor_m3376169429 ();
extern "C" void HttpWebRequest__ctor_m2736359513 ();
extern "C" void HttpWebRequest__ctor_m3658280394 ();
extern "C" void HttpWebRequest__cctor_m1749648113 ();
extern "C" void HttpWebRequest_get_Address_m3019577225 ();
extern "C" void HttpWebRequest_get_ServicePoint_m899481075 ();
extern "C" void HttpWebRequest_GetServicePoint_m3427236345 ();
extern "C" void IPAddress__ctor_m2998688361 ();
extern "C" void IPAddress__ctor_m3961987861 ();
extern "C" void IPAddress__cctor_m4181497143 ();
extern "C" void IPAddress_SwapShort_m1858624640 ();
extern "C" void IPAddress_HostToNetworkOrder_m962297178 ();
extern "C" void IPAddress_NetworkToHostOrder_m4114126828 ();
extern "C" void IPAddress_Parse_m253184166 ();
extern "C" void IPAddress_TryParse_m1179585744 ();
extern "C" void IPAddress_ParseIPV4_m180622572 ();
extern "C" void IPAddress_ParseIPV6_m3727203205 ();
extern "C" void IPAddress_get_InternalIPv4Address_m4258149000 ();
extern "C" void IPAddress_get_ScopeId_m2903494700 ();
extern "C" void IPAddress_get_AddressFamily_m1298645662 ();
extern "C" void IPAddress_IsLoopback_m3034121712 ();
extern "C" void IPAddress_ToString_m1512291888 ();
extern "C" void IPAddress_ToString_m1991813227 ();
extern "C" void IPAddress_Equals_m3877822454 ();
extern "C" void IPAddress_GetHashCode_m3138408096 ();
extern "C" void IPAddress_Hash_m739624159 ();
extern "C" void IPv6Address__ctor_m4062223255 ();
extern "C" void IPv6Address__ctor_m951761819 ();
extern "C" void IPv6Address__ctor_m1666403 ();
extern "C" void IPv6Address__cctor_m1600435160 ();
extern "C" void IPv6Address_Parse_m121713681 ();
extern "C" void IPv6Address_Fill_m3054533284 ();
extern "C" void IPv6Address_TryParse_m3087512846 ();
extern "C" void IPv6Address_TryParse_m1452498452 ();
extern "C" void IPv6Address_get_Address_m2230496963 ();
extern "C" void IPv6Address_get_ScopeId_m1821907027 ();
extern "C" void IPv6Address_set_ScopeId_m1505919733 ();
extern "C" void IPv6Address_IsLoopback_m124461899 ();
extern "C" void IPv6Address_SwapUShort_m3847599182 ();
extern "C" void IPv6Address_AsIPv4Int_m404489331 ();
extern "C" void IPv6Address_IsIPv4Compatible_m1526179885 ();
extern "C" void IPv6Address_IsIPv4Mapped_m571396438 ();
extern "C" void IPv6Address_ToString_m831693711 ();
extern "C" void IPv6Address_ToString_m3233213616 ();
extern "C" void IPv6Address_Equals_m1759133691 ();
extern "C" void IPv6Address_GetHashCode_m2357397747 ();
extern "C" void IPv6Address_Hash_m1631808157 ();
extern "C" void RemoteCertificateValidationCallback__ctor_m3841569000 ();
extern "C" void RemoteCertificateValidationCallback_Invoke_m2353857608 ();
extern "C" void RemoteCertificateValidationCallback_BeginInvoke_m368301197 ();
extern "C" void RemoteCertificateValidationCallback_EndInvoke_m4102898920 ();
extern "C" void ServicePoint__ctor_m541371194 ();
extern "C" void ServicePoint_get_Address_m3846712954 ();
extern "C" void ServicePoint_get_CurrentConnections_m1105853250 ();
extern "C" void ServicePoint_get_IdleSince_m2729517043 ();
extern "C" void ServicePoint_set_IdleSince_m2026649210 ();
extern "C" void ServicePoint_set_Expect100Continue_m4032784863 ();
extern "C" void ServicePoint_set_UseNagleAlgorithm_m519465156 ();
extern "C" void ServicePoint_set_SendContinue_m3746141504 ();
extern "C" void ServicePoint_set_UsesProxy_m3389395357 ();
extern "C" void ServicePoint_set_UseConnect_m3820634640 ();
extern "C" void ServicePoint_get_AvailableForRecycling_m4187650904 ();
extern "C" void ServicePointManager__cctor_m2362068564 ();
extern "C" void ServicePointManager_get_CertificatePolicy_m1315874930 ();
extern "C" void ServicePointManager_get_CheckCertificateRevocationList_m1108407563 ();
extern "C" void ServicePointManager_get_SecurityProtocol_m3491844700 ();
extern "C" void ServicePointManager_get_ServerCertificateValidationCallback_m1866385585 ();
extern "C" void ServicePointManager_FindServicePoint_m2815391183 ();
extern "C" void ServicePointManager_RecycleServicePoints_m3938343390 ();
extern "C" void SPKey__ctor_m3457578310 ();
extern "C" void SPKey_GetHashCode_m2652803391 ();
extern "C" void SPKey_Equals_m228165283 ();
extern "C" void WebHeaderCollection__ctor_m3543537335 ();
extern "C" void WebHeaderCollection__ctor_m1894830221 ();
extern "C" void WebHeaderCollection__ctor_m4254771387 ();
extern "C" void WebHeaderCollection__cctor_m370270238 ();
extern "C" void WebHeaderCollection_Add_m2149172933 ();
extern "C" void WebHeaderCollection_AddWithoutValidate_m1614536327 ();
extern "C" void WebHeaderCollection_IsRestricted_m3157354787 ();
extern "C" void WebHeaderCollection_OnDeserialization_m921538671 ();
extern "C" void WebHeaderCollection_ToString_m789018140 ();
extern "C" void WebHeaderCollection_get_Count_m1031696890 ();
extern "C" void WebHeaderCollection_get_Keys_m1617211394 ();
extern "C" void WebHeaderCollection_Get_m1807072431 ();
extern "C" void WebHeaderCollection_GetKey_m56874696 ();
extern "C" void WebHeaderCollection_GetEnumerator_m152674609 ();
extern "C" void WebHeaderCollection_IsHeaderValue_m4234904120 ();
extern "C" void WebHeaderCollection_IsHeaderName_m694174817 ();
extern "C" void WebProxy__ctor_m3438920520 ();
extern "C" void WebProxy__ctor_m151208726 ();
extern "C" void WebProxy__ctor_m2644648429 ();
extern "C" void WebProxy_GetProxy_m3358069819 ();
extern "C" void WebProxy_IsBypassed_m1890758997 ();
extern "C" void WebProxy_CheckBypassList_m1500623066 ();
extern "C" void WebRequest__ctor_m2656955935 ();
extern "C" void WebRequest__ctor_m3488295241 ();
extern "C" void WebRequest__cctor_m1135262946 ();
extern "C" void WebRequest_AddDynamicPrefix_m3639969267 ();
extern "C" void WebRequest_get_DefaultWebProxy_m3315946553 ();
extern "C" void WebRequest_GetDefaultWebProxy_m291030002 ();
extern "C" void WebRequest_AddPrefix_m680934778 ();
extern "C" void AsnEncodedData__ctor_m1686559169 ();
extern "C" void AsnEncodedData__ctor_m1066383994 ();
extern "C" void AsnEncodedData__ctor_m3367282397 ();
extern "C" void AsnEncodedData_get_Oid_m3751842987 ();
extern "C" void AsnEncodedData_set_Oid_m516923593 ();
extern "C" void AsnEncodedData_get_RawData_m15804319 ();
extern "C" void AsnEncodedData_set_RawData_m1637570500 ();
extern "C" void AsnEncodedData_CopyFrom_m2098061996 ();
extern "C" void AsnEncodedData_ToString_m103952896 ();
extern "C" void AsnEncodedData_Default_m3235139048 ();
extern "C" void AsnEncodedData_BasicConstraintsExtension_m4146259797 ();
extern "C" void AsnEncodedData_EnhancedKeyUsageExtension_m3786455932 ();
extern "C" void AsnEncodedData_KeyUsageExtension_m1588063621 ();
extern "C" void AsnEncodedData_SubjectKeyIdentifierExtension_m1004541233 ();
extern "C" void AsnEncodedData_SubjectAltName_m3675814266 ();
extern "C" void AsnEncodedData_NetscapeCertType_m3168148730 ();
extern "C" void Oid__ctor_m1870915178 ();
extern "C" void Oid__ctor_m4119000323 ();
extern "C" void Oid__ctor_m2873505868 ();
extern "C" void Oid__ctor_m2474010395 ();
extern "C" void Oid_get_FriendlyName_m1688331560 ();
extern "C" void Oid_get_Value_m1894942781 ();
extern "C" void Oid_GetName_m880017793 ();
extern "C" void OidCollection__ctor_m2607409884 ();
extern "C" void OidCollection_System_Collections_ICollection_CopyTo_m75121772 ();
extern "C" void OidCollection_System_Collections_IEnumerable_GetEnumerator_m3574465405 ();
extern "C" void OidCollection_get_Count_m2266807653 ();
extern "C" void OidCollection_get_Item_m37537949 ();
extern "C" void OidCollection_get_SyncRoot_m1484651087 ();
extern "C" void OidCollection_Add_m1191707284 ();
extern "C" void OidEnumerator__ctor_m2522427274 ();
extern "C" void OidEnumerator_System_Collections_IEnumerator_get_Current_m2376888734 ();
extern "C" void OidEnumerator_MoveNext_m10888191 ();
extern "C" void OidEnumerator_Reset_m830666839 ();
extern "C" void PublicKey__ctor_m1750800224 ();
extern "C" void PublicKey_get_EncodedKeyValue_m736788861 ();
extern "C" void PublicKey_get_EncodedParameters_m2990914164 ();
extern "C" void PublicKey_get_Key_m1548320275 ();
extern "C" void PublicKey_get_Oid_m3517176937 ();
extern "C" void PublicKey_GetUnsignedBigInteger_m1007476432 ();
extern "C" void PublicKey_DecodeDSA_m165869145 ();
extern "C" void PublicKey_DecodeRSA_m1730960699 ();
extern "C" void X500DistinguishedName__ctor_m1201939721 ();
extern "C" void X500DistinguishedName_Decode_m11295179 ();
extern "C" void X500DistinguishedName_GetSeparator_m1302505900 ();
extern "C" void X500DistinguishedName_DecodeRawData_m4171517381 ();
extern "C" void X500DistinguishedName_Canonize_m543730904 ();
extern "C" void X500DistinguishedName_AreEqual_m1796807539 ();
extern "C" void X509BasicConstraintsExtension__ctor_m3135938564 ();
extern "C" void X509BasicConstraintsExtension__ctor_m2548391895 ();
extern "C" void X509BasicConstraintsExtension__ctor_m4095953927 ();
extern "C" void X509BasicConstraintsExtension_get_CertificateAuthority_m3674544514 ();
extern "C" void X509BasicConstraintsExtension_get_HasPathLengthConstraint_m414713914 ();
extern "C" void X509BasicConstraintsExtension_get_PathLengthConstraint_m3246637991 ();
extern "C" void X509BasicConstraintsExtension_CopyFrom_m340687556 ();
extern "C" void X509BasicConstraintsExtension_Decode_m3249055662 ();
extern "C" void X509BasicConstraintsExtension_Encode_m3998660200 ();
extern "C" void X509BasicConstraintsExtension_ToString_m2890073721 ();
extern "C" void X509Certificate2__ctor_m308198123 ();
extern "C" void X509Certificate2__cctor_m484391723 ();
extern "C" void X509Certificate2_get_Extensions_m1023292323 ();
extern "C" void X509Certificate2_get_IssuerName_m1460999157 ();
extern "C" void X509Certificate2_get_NotAfter_m2823396055 ();
extern "C" void X509Certificate2_get_NotBefore_m3393295511 ();
extern "C" void X509Certificate2_get_PrivateKey_m2166338226 ();
extern "C" void X509Certificate2_get_PublicKey_m3138038966 ();
extern "C" void X509Certificate2_get_SerialNumber_m1302737622 ();
extern "C" void X509Certificate2_get_SignatureAlgorithm_m3770353299 ();
extern "C" void X509Certificate2_get_SubjectName_m1275144177 ();
extern "C" void X509Certificate2_get_Thumbprint_m4049803910 ();
extern "C" void X509Certificate2_get_Version_m3027027448 ();
extern "C" void X509Certificate2_GetNameInfo_m3374661201 ();
extern "C" void X509Certificate2_Find_m657946865 ();
extern "C" void X509Certificate2_GetValueAsString_m1993705590 ();
extern "C" void X509Certificate2_ImportPkcs12_m3788361047 ();
extern "C" void X509Certificate2_Import_m2733344782 ();
extern "C" void X509Certificate2_Reset_m789147374 ();
extern "C" void X509Certificate2_ToString_m345725120 ();
extern "C" void X509Certificate2_ToString_m807561955 ();
extern "C" void X509Certificate2_AppendBuffer_m1828255695 ();
extern "C" void X509Certificate2_Verify_m1956073113 ();
extern "C" void X509Certificate2_get_MonoCertificate_m3193125116 ();
extern "C" void X509Certificate2Collection__ctor_m3907597786 ();
extern "C" void X509Certificate2Collection__ctor_m2544859497 ();
extern "C" void X509Certificate2Collection_get_Item_m1966030007 ();
extern "C" void X509Certificate2Collection_Add_m1999011476 ();
extern "C" void X509Certificate2Collection_AddRange_m3259897552 ();
extern "C" void X509Certificate2Collection_Contains_m2676920466 ();
extern "C" void X509Certificate2Collection_Find_m1361050413 ();
extern "C" void X509Certificate2Collection_GetEnumerator_m108001564 ();
extern "C" void X509Certificate2Enumerator__ctor_m1875203866 ();
extern "C" void X509Certificate2Enumerator_System_Collections_IEnumerator_get_Current_m2303160842 ();
extern "C" void X509Certificate2Enumerator_System_Collections_IEnumerator_MoveNext_m327021471 ();
extern "C" void X509Certificate2Enumerator_System_Collections_IEnumerator_Reset_m2982496364 ();
extern "C" void X509Certificate2Enumerator_get_Current_m2011474582 ();
extern "C" void X509Certificate2Enumerator_MoveNext_m4110494477 ();
extern "C" void X509Certificate2Enumerator_Reset_m2524280618 ();
extern "C" void X509CertificateCollection__ctor_m1036914496 ();
extern "C" void X509CertificateCollection__ctor_m2100269461 ();
extern "C" void X509CertificateCollection_get_Item_m1784093064 ();
extern "C" void X509CertificateCollection_AddRange_m1575676587 ();
extern "C" void X509CertificateCollection_GetEnumerator_m2744545608 ();
extern "C" void X509CertificateCollection_GetHashCode_m2550441373 ();
extern "C" void X509CertificateEnumerator__ctor_m444840300 ();
extern "C" void X509CertificateEnumerator_System_Collections_IEnumerator_get_Current_m490380195 ();
extern "C" void X509CertificateEnumerator_System_Collections_IEnumerator_MoveNext_m2751375897 ();
extern "C" void X509CertificateEnumerator_System_Collections_IEnumerator_Reset_m1237476061 ();
extern "C" void X509CertificateEnumerator_get_Current_m4152720733 ();
extern "C" void X509CertificateEnumerator_MoveNext_m783626541 ();
extern "C" void X509CertificateEnumerator_Reset_m3529991512 ();
extern "C" void X509Chain__ctor_m2295765266 ();
extern "C" void X509Chain__ctor_m1673580690 ();
extern "C" void X509Chain__cctor_m2749740026 ();
extern "C" void X509Chain_get_ChainPolicy_m1264065711 ();
extern "C" void X509Chain_Build_m3453334197 ();
extern "C" void X509Chain_Reset_m3331458161 ();
extern "C" void X509Chain_get_Roots_m744023777 ();
extern "C" void X509Chain_get_CertificateAuthorities_m657405698 ();
extern "C" void X509Chain_get_CertificateCollection_m3748443862 ();
extern "C" void X509Chain_BuildChainFrom_m487452279 ();
extern "C" void X509Chain_SelectBestFromCollection_m4267993714 ();
extern "C" void X509Chain_FindParent_m3735542056 ();
extern "C" void X509Chain_IsChainComplete_m1242041874 ();
extern "C" void X509Chain_IsSelfIssued_m1485661908 ();
extern "C" void X509Chain_ValidateChain_m3813329889 ();
extern "C" void X509Chain_Process_m1041844227 ();
extern "C" void X509Chain_PrepareForNextCertificate_m301529593 ();
extern "C" void X509Chain_WrapUp_m1930673592 ();
extern "C" void X509Chain_ProcessCertificateExtensions_m3038036694 ();
extern "C" void X509Chain_IsSignedWith_m3624544465 ();
extern "C" void X509Chain_GetSubjectKeyIdentifier_m2959801487 ();
extern "C" void X509Chain_GetAuthorityKeyIdentifier_m3631641129 ();
extern "C" void X509Chain_GetAuthorityKeyIdentifier_m3113434308 ();
extern "C" void X509Chain_GetAuthorityKeyIdentifier_m585911764 ();
extern "C" void X509Chain_CheckRevocationOnChain_m1902396785 ();
extern "C" void X509Chain_CheckRevocation_m2319573292 ();
extern "C" void X509Chain_CheckRevocation_m48989749 ();
extern "C" void X509Chain_FindCrl_m3014024698 ();
extern "C" void X509Chain_ProcessCrlExtensions_m2373206719 ();
extern "C" void X509Chain_ProcessCrlEntryExtensions_m2033824136 ();
extern "C" void X509ChainElement__ctor_m1568656711 ();
extern "C" void X509ChainElement_get_Certificate_m3417167996 ();
extern "C" void X509ChainElement_get_ChainElementStatus_m192323064 ();
extern "C" void X509ChainElement_get_StatusFlags_m1096301318 ();
extern "C" void X509ChainElement_set_StatusFlags_m1290770496 ();
extern "C" void X509ChainElement_Count_m2358318203 ();
extern "C" void X509ChainElement_Set_m2649997160 ();
extern "C" void X509ChainElement_UncompressFlags_m4045409131 ();
extern "C" void X509ChainElementCollection__ctor_m2015381107 ();
extern "C" void X509ChainElementCollection_System_Collections_ICollection_CopyTo_m631034808 ();
extern "C" void X509ChainElementCollection_System_Collections_IEnumerable_GetEnumerator_m2401724130 ();
extern "C" void X509ChainElementCollection_get_Count_m3868431546 ();
extern "C" void X509ChainElementCollection_get_Item_m3912577230 ();
extern "C" void X509ChainElementCollection_get_SyncRoot_m2383237448 ();
extern "C" void X509ChainElementCollection_GetEnumerator_m4097956872 ();
extern "C" void X509ChainElementCollection_Add_m847233729 ();
extern "C" void X509ChainElementCollection_Clear_m3786857277 ();
extern "C" void X509ChainElementCollection_Contains_m2056386153 ();
extern "C" void X509ChainElementEnumerator__ctor_m1560965757 ();
extern "C" void X509ChainElementEnumerator_System_Collections_IEnumerator_get_Current_m2638688153 ();
extern "C" void X509ChainElementEnumerator_get_Current_m3357365272 ();
extern "C" void X509ChainElementEnumerator_MoveNext_m1362717986 ();
extern "C" void X509ChainElementEnumerator_Reset_m922121085 ();
extern "C" void X509ChainPolicy__ctor_m2726805601 ();
extern "C" void X509ChainPolicy_get_ExtraStore_m2990822534 ();
extern "C" void X509ChainPolicy_get_RevocationFlag_m3281186121 ();
extern "C" void X509ChainPolicy_get_RevocationMode_m210982597 ();
extern "C" void X509ChainPolicy_get_VerificationFlags_m1593716000 ();
extern "C" void X509ChainPolicy_get_VerificationTime_m1721451404 ();
extern "C" void X509ChainPolicy_Reset_m2849766180 ();
extern "C" void X509ChainStatus__ctor_m3489936283_AdjustorThunk ();
extern "C" void X509ChainStatus_get_Status_m335128257_AdjustorThunk ();
extern "C" void X509ChainStatus_set_Status_m4280193025_AdjustorThunk ();
extern "C" void X509ChainStatus_set_StatusInformation_m4272839486_AdjustorThunk ();
extern "C" void X509ChainStatus_GetInformation_m1839183247 ();
extern "C" void X509EnhancedKeyUsageExtension__ctor_m730911859 ();
extern "C" void X509EnhancedKeyUsageExtension_CopyFrom_m979756146 ();
extern "C" void X509EnhancedKeyUsageExtension_Decode_m2986058248 ();
extern "C" void X509EnhancedKeyUsageExtension_ToString_m3261438540 ();
extern "C" void X509Extension__ctor_m2799141655 ();
extern "C" void X509Extension__ctor_m406737987 ();
extern "C" void X509Extension_get_Critical_m3055848086 ();
extern "C" void X509Extension_set_Critical_m2709766182 ();
extern "C" void X509Extension_CopyFrom_m1301243670 ();
extern "C" void X509Extension_FormatUnkownData_m3784592395 ();
extern "C" void X509ExtensionCollection__ctor_m3961408697 ();
extern "C" void X509ExtensionCollection_System_Collections_ICollection_CopyTo_m161674631 ();
extern "C" void X509ExtensionCollection_System_Collections_IEnumerable_GetEnumerator_m1063171060 ();
extern "C" void X509ExtensionCollection_get_Count_m224995060 ();
extern "C" void X509ExtensionCollection_get_SyncRoot_m4031705892 ();
extern "C" void X509ExtensionCollection_get_Item_m1068911087 ();
extern "C" void X509ExtensionCollection_GetEnumerator_m3085262559 ();
extern "C" void X509ExtensionEnumerator__ctor_m984586973 ();
extern "C" void X509ExtensionEnumerator_System_Collections_IEnumerator_get_Current_m4104731131 ();
extern "C" void X509ExtensionEnumerator_get_Current_m2694119415 ();
extern "C" void X509ExtensionEnumerator_MoveNext_m3210325396 ();
extern "C" void X509ExtensionEnumerator_Reset_m801981063 ();
extern "C" void X509KeyUsageExtension__ctor_m107365671 ();
extern "C" void X509KeyUsageExtension__ctor_m1195762189 ();
extern "C" void X509KeyUsageExtension__ctor_m1265507522 ();
extern "C" void X509KeyUsageExtension_get_KeyUsages_m1532518629 ();
extern "C" void X509KeyUsageExtension_CopyFrom_m554319546 ();
extern "C" void X509KeyUsageExtension_GetValidFlags_m3424324572 ();
extern "C" void X509KeyUsageExtension_Decode_m2836274340 ();
extern "C" void X509KeyUsageExtension_Encode_m1072002440 ();
extern "C" void X509KeyUsageExtension_ToString_m1381174620 ();
extern "C" void X509Store__ctor_m3994237834 ();
extern "C" void X509Store_get_Certificates_m1702400367 ();
extern "C" void X509Store_get_Factory_m579812897 ();
extern "C" void X509Store_get_Store_m3626430760 ();
extern "C" void X509Store_Close_m4270546084 ();
extern "C" void X509Store_Open_m53237587 ();
extern "C" void X509SubjectKeyIdentifierExtension__ctor_m2623171705 ();
extern "C" void X509SubjectKeyIdentifierExtension__ctor_m3657184001 ();
extern "C" void X509SubjectKeyIdentifierExtension__ctor_m2884178020 ();
extern "C" void X509SubjectKeyIdentifierExtension__ctor_m2686017024 ();
extern "C" void X509SubjectKeyIdentifierExtension__ctor_m2795298815 ();
extern "C" void X509SubjectKeyIdentifierExtension__ctor_m1645984076 ();
extern "C" void X509SubjectKeyIdentifierExtension_get_SubjectKeyIdentifier_m4232312249 ();
extern "C" void X509SubjectKeyIdentifierExtension_CopyFrom_m262411069 ();
extern "C" void X509SubjectKeyIdentifierExtension_FromHexChar_m3542757554 ();
extern "C" void X509SubjectKeyIdentifierExtension_FromHexChars_m882804205 ();
extern "C" void X509SubjectKeyIdentifierExtension_FromHex_m3217166338 ();
extern "C" void X509SubjectKeyIdentifierExtension_Decode_m269290275 ();
extern "C" void X509SubjectKeyIdentifierExtension_Encode_m1665948625 ();
extern "C" void X509SubjectKeyIdentifierExtension_ToString_m3993080498 ();
extern "C" void BaseMachine__ctor_m2808867664 ();
extern "C" void BaseMachine_Scan_m2557486571 ();
extern "C" void Capture__ctor_m1869455468 ();
extern "C" void Capture__ctor_m1121012478 ();
extern "C" void Capture_get_Index_m3907808290 ();
extern "C" void Capture_get_Length_m3971179031 ();
extern "C" void Capture_get_Value_m4071717308 ();
extern "C" void Capture_ToString_m3697979501 ();
extern "C" void Capture_get_Text_m74686649 ();
extern "C" void CaptureCollection__ctor_m1463284443 ();
extern "C" void CaptureCollection_get_Count_m86765405 ();
extern "C" void CaptureCollection_SetValue_m1524796401 ();
extern "C" void CaptureCollection_get_SyncRoot_m888355354 ();
extern "C" void CaptureCollection_CopyTo_m3413021498 ();
extern "C" void CaptureCollection_GetEnumerator_m812510493 ();
extern "C" void CategoryUtils_CategoryFromName_m3811753124 ();
extern "C" void CategoryUtils_IsCategory_m4155413800 ();
extern "C" void CategoryUtils_IsCategory_m2163137776 ();
extern "C" void FactoryCache__ctor_m3864976125 ();
extern "C" void FactoryCache_Add_m2489922677 ();
extern "C" void FactoryCache_Cleanup_m2082071718 ();
extern "C" void FactoryCache_Lookup_m1054485078 ();
extern "C" void Key__ctor_m2851356833 ();
extern "C" void Key_GetHashCode_m1728174491 ();
extern "C" void Key_Equals_m700341561 ();
extern "C" void Key_ToString_m2395030159 ();
extern "C" void Group__ctor_m657414250 ();
extern "C" void Group__ctor_m2985966418 ();
extern "C" void Group__ctor_m1050548384 ();
extern "C" void Group__cctor_m3420511086 ();
extern "C" void Group_get_Captures_m1103670571 ();
extern "C" void Group_get_Success_m55565450 ();
extern "C" void GroupCollection__ctor_m2650234459 ();
extern "C" void GroupCollection_get_Count_m3082209498 ();
extern "C" void GroupCollection_get_Item_m3590181931 ();
extern "C" void GroupCollection_SetValue_m4030022841 ();
extern "C" void GroupCollection_get_SyncRoot_m597033413 ();
extern "C" void GroupCollection_CopyTo_m2153963735 ();
extern "C" void GroupCollection_GetEnumerator_m2139291211 ();
extern "C" void Interpreter__ctor_m4034007428 ();
extern "C" void Interpreter_ReadProgramCount_m2060549378 ();
extern "C" void Interpreter_Scan_m3093498884 ();
extern "C" void Interpreter_Reset_m196471111 ();
extern "C" void Interpreter_Eval_m3147225683 ();
extern "C" void Interpreter_EvalChar_m2388057289 ();
extern "C" void Interpreter_TryMatch_m2950454473 ();
extern "C" void Interpreter_IsPosition_m4237205282 ();
extern "C" void Interpreter_IsWordChar_m1873249047 ();
extern "C" void Interpreter_GetString_m3892511047 ();
extern "C" void Interpreter_Open_m3558015365 ();
extern "C" void Interpreter_Close_m1079184275 ();
extern "C" void Interpreter_Balance_m2378028983 ();
extern "C" void Interpreter_Checkpoint_m2292620714 ();
extern "C" void Interpreter_Backtrack_m358817158 ();
extern "C" void Interpreter_ResetGroups_m929204024 ();
extern "C" void Interpreter_GetLastDefined_m45218908 ();
extern "C" void Interpreter_CreateMark_m2549972701 ();
extern "C" void Interpreter_GetGroupInfo_m1313779362 ();
extern "C" void Interpreter_PopulateGroup_m1203148445 ();
extern "C" void Interpreter_GenerateMatch_m4133769251 ();
extern "C" void IntStack_Pop_m4044529908_AdjustorThunk ();
extern "C" void IntStack_Push_m3404206713_AdjustorThunk ();
extern "C" void IntStack_get_Count_m2431504187_AdjustorThunk ();
extern "C" void IntStack_set_Count_m688359232_AdjustorThunk ();
extern "C" void RepeatContext__ctor_m4019822713 ();
extern "C" void RepeatContext_get_Count_m4176031860 ();
extern "C" void RepeatContext_set_Count_m3205893559 ();
extern "C" void RepeatContext_get_Start_m2009793756 ();
extern "C" void RepeatContext_set_Start_m250610698 ();
extern "C" void RepeatContext_get_IsMinimum_m2089709823 ();
extern "C" void RepeatContext_get_IsMaximum_m4195905978 ();
extern "C" void RepeatContext_get_IsLazy_m662975735 ();
extern "C" void RepeatContext_get_Expression_m513395332 ();
extern "C" void RepeatContext_get_Previous_m2451857091 ();
extern "C" void InterpreterFactory__ctor_m926229535 ();
extern "C" void InterpreterFactory_NewInstance_m1772001723 ();
extern "C" void InterpreterFactory_get_GroupCount_m3704859672 ();
extern "C" void InterpreterFactory_get_Gap_m4180186475 ();
extern "C" void InterpreterFactory_set_Gap_m4223065257 ();
extern "C" void InterpreterFactory_get_Mapping_m2580782401 ();
extern "C" void InterpreterFactory_set_Mapping_m4265804398 ();
extern "C" void InterpreterFactory_get_NamesMapping_m3153572372 ();
extern "C" void InterpreterFactory_set_NamesMapping_m1645343155 ();
extern "C" void Interval__ctor_m3102752716_AdjustorThunk ();
extern "C" void Interval_get_Empty_m1612289276 ();
extern "C" void Interval_get_IsDiscontiguous_m681417648_AdjustorThunk ();
extern "C" void Interval_get_IsSingleton_m895795660_AdjustorThunk ();
extern "C" void Interval_get_IsEmpty_m2966462965_AdjustorThunk ();
extern "C" void Interval_get_Size_m3182553814_AdjustorThunk ();
extern "C" void Interval_IsDisjoint_m1320129171_AdjustorThunk ();
extern "C" void Interval_IsAdjacent_m1087758723_AdjustorThunk ();
extern "C" void Interval_Contains_m386969978_AdjustorThunk ();
extern "C" void Interval_Contains_m3687697697_AdjustorThunk ();
extern "C" void Interval_Intersects_m1543757554_AdjustorThunk ();
extern "C" void Interval_Merge_m3917739616_AdjustorThunk ();
extern "C" void Interval_CompareTo_m745948360_AdjustorThunk ();
extern "C" void IntervalCollection__ctor_m918788359 ();
extern "C" void IntervalCollection_get_Item_m783730233 ();
extern "C" void IntervalCollection_Add_m3169764839 ();
extern "C" void IntervalCollection_Normalize_m1295588297 ();
extern "C" void IntervalCollection_GetMetaCollection_m1267446608 ();
extern "C" void IntervalCollection_Optimize_m2126698842 ();
extern "C" void IntervalCollection_get_Count_m632632423 ();
extern "C" void IntervalCollection_get_SyncRoot_m2894804251 ();
extern "C" void IntervalCollection_CopyTo_m2496767053 ();
extern "C" void IntervalCollection_GetEnumerator_m1596418039 ();
extern "C" void CostDelegate__ctor_m578116942 ();
extern "C" void CostDelegate_Invoke_m2940437879 ();
extern "C" void CostDelegate_BeginInvoke_m3941820692 ();
extern "C" void CostDelegate_EndInvoke_m3443741528 ();
extern "C" void Enumerator__ctor_m174366350 ();
extern "C" void Enumerator_get_Current_m4102274120 ();
extern "C" void Enumerator_MoveNext_m1689981771 ();
extern "C" void Enumerator_Reset_m2833936142 ();
extern "C" void LinkRef__ctor_m3104548595 ();
extern "C" void LinkStack__ctor_m281815638 ();
extern "C" void LinkStack_Push_m1448127355 ();
extern "C" void LinkStack_Pop_m3803592339 ();
extern "C" void Mark_get_IsDefined_m3609987897_AdjustorThunk ();
extern "C" void Mark_get_Index_m873878044_AdjustorThunk ();
extern "C" void Mark_get_Length_m1016015124_AdjustorThunk ();
extern "C" void Match__ctor_m238110389 ();
extern "C" void Match__ctor_m2112467114 ();
extern "C" void Match__ctor_m2842759013 ();
extern "C" void Match__cctor_m1524008222 ();
extern "C" void Match_get_Empty_m1922684559 ();
extern "C" void Match_get_Groups_m376908515 ();
extern "C" void Match_NextMatch_m1246497778 ();
extern "C" void Match_get_Regex_m2048620843 ();
extern "C" void MatchCollection__ctor_m2881869702 ();
extern "C" void MatchCollection_get_Count_m2375168608 ();
extern "C" void MatchCollection_get_Item_m265458295 ();
extern "C" void MatchCollection_get_SyncRoot_m3959276890 ();
extern "C" void MatchCollection_CopyTo_m2972287649 ();
extern "C" void MatchCollection_GetEnumerator_m1809159483 ();
extern "C" void MatchCollection_TryToGet_m3109984710 ();
extern "C" void MatchCollection_get_FullList_m4240090779 ();
extern "C" void Enumerator__ctor_m157141097 ();
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m3082080361 ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m2240246121 ();
extern "C" void Enumerator_System_Collections_IEnumerator_MoveNext_m1904606682 ();
extern "C" void MRUList__ctor_m2174943652 ();
extern "C" void MRUList_Use_m2846104550 ();
extern "C" void MRUList_Evict_m1811132593 ();
extern "C" void Node__ctor_m2510706571 ();
extern "C" void PatternCompiler__ctor_m3722632908 ();
extern "C" void PatternCompiler_EncodeOp_m1577698176 ();
extern "C" void PatternCompiler_GetMachineFactory_m505229676 ();
extern "C" void PatternCompiler_EmitFalse_m3671235209 ();
extern "C" void PatternCompiler_EmitTrue_m3052543991 ();
extern "C" void PatternCompiler_EmitCount_m3024087504 ();
extern "C" void PatternCompiler_EmitCharacter_m4144999552 ();
extern "C" void PatternCompiler_EmitCategory_m1974828108 ();
extern "C" void PatternCompiler_EmitNotCategory_m3788048959 ();
extern "C" void PatternCompiler_EmitRange_m2418012862 ();
extern "C" void PatternCompiler_EmitSet_m2888416005 ();
extern "C" void PatternCompiler_EmitString_m2907993839 ();
extern "C" void PatternCompiler_EmitPosition_m3185465351 ();
extern "C" void PatternCompiler_EmitOpen_m811998928 ();
extern "C" void PatternCompiler_EmitClose_m3128219862 ();
extern "C" void PatternCompiler_EmitBalanceStart_m1924342539 ();
extern "C" void PatternCompiler_EmitBalance_m1652518181 ();
extern "C" void PatternCompiler_EmitReference_m2216765694 ();
extern "C" void PatternCompiler_EmitIfDefined_m1468354804 ();
extern "C" void PatternCompiler_EmitSub_m1049866462 ();
extern "C" void PatternCompiler_EmitTest_m1359345437 ();
extern "C" void PatternCompiler_EmitBranch_m3705995786 ();
extern "C" void PatternCompiler_EmitJump_m1958236741 ();
extern "C" void PatternCompiler_EmitRepeat_m3353380693 ();
extern "C" void PatternCompiler_EmitUntil_m181962929 ();
extern "C" void PatternCompiler_EmitFastRepeat_m2138837181 ();
extern "C" void PatternCompiler_EmitIn_m1522679842 ();
extern "C" void PatternCompiler_EmitAnchor_m3028352968 ();
extern "C" void PatternCompiler_EmitInfo_m1450139948 ();
extern "C" void PatternCompiler_NewLink_m3979462017 ();
extern "C" void PatternCompiler_ResolveLink_m164156451 ();
extern "C" void PatternCompiler_EmitBranchEnd_m2911363091 ();
extern "C" void PatternCompiler_EmitAlternationEnd_m242176802 ();
extern "C" void PatternCompiler_MakeFlags_m319497320 ();
extern "C" void PatternCompiler_Emit_m1106278210 ();
extern "C" void PatternCompiler_Emit_m2601956873 ();
extern "C" void PatternCompiler_Emit_m593203751 ();
extern "C" void PatternCompiler_get_CurrentAddress_m3694601918 ();
extern "C" void PatternCompiler_BeginLink_m3852665130 ();
extern "C" void PatternCompiler_EmitLink_m1763248820 ();
extern "C" void PatternLinkStack__ctor_m4063965746 ();
extern "C" void PatternLinkStack_set_BaseAddress_m3909468559 ();
extern "C" void PatternLinkStack_get_OffsetAddress_m3459546459 ();
extern "C" void PatternLinkStack_set_OffsetAddress_m3407729897 ();
extern "C" void PatternLinkStack_GetOffset_m787873589 ();
extern "C" void PatternLinkStack_GetCurrent_m3175483000 ();
extern "C" void PatternLinkStack_SetCurrent_m2772557551 ();
extern "C" void QuickSearch__ctor_m2205088277 ();
extern "C" void QuickSearch__cctor_m210554867 ();
extern "C" void QuickSearch_get_Length_m877323884 ();
extern "C" void QuickSearch_Search_m872205891 ();
extern "C" void QuickSearch_SetupShiftTable_m971330239 ();
extern "C" void QuickSearch_GetShiftDistance_m2244908312 ();
extern "C" void QuickSearch_GetChar_m2194097073 ();
extern "C" void Regex__ctor_m2953022631 ();
extern "C" void Regex__ctor_m1281000962 ();
extern "C" void Regex__ctor_m371066603 ();
extern "C" void Regex__ctor_m2397604299 ();
extern "C" void Regex__cctor_m3162765483 ();
extern "C" void Regex_validate_options_m3793258073 ();
extern "C" void Regex_Init_m3238975230 ();
extern "C" void Regex_InitNewRegex_m4214253127 ();
extern "C" void Regex_CreateMachineFactory_m37122244 ();
extern "C" void Regex_get_RightToLeft_m596212595 ();
extern "C" void Regex_GetGroupIndex_m3620870088 ();
extern "C" void Regex_default_startat_m2015953969 ();
extern "C" void Regex_IsMatch_m1298252676 ();
extern "C" void Regex_IsMatch_m1844270105 ();
extern "C" void Regex_Match_m1753254329 ();
extern "C" void Regex_Matches_m3884828707 ();
extern "C" void Regex_Matches_m1303075336 ();
extern "C" void Regex_ToString_m30946562 ();
extern "C" void Regex_get_Gap_m993289954 ();
extern "C" void Regex_CreateMachine_m3816343126 ();
extern "C" void Regex_GetGroupNamesArray_m1470390606 ();
extern "C" void Regex_get_GroupNumbers_m4144353649 ();
extern "C" void Alternation__ctor_m3937462547 ();
extern "C" void Alternation_get_Alternatives_m2599806281 ();
extern "C" void Alternation_AddAlternative_m2534753035 ();
extern "C" void Alternation_Compile_m3814532839 ();
extern "C" void Alternation_GetWidth_m3813024235 ();
extern "C" void AnchorInfo__ctor_m1329460025 ();
extern "C" void AnchorInfo__ctor_m1145966173 ();
extern "C" void AnchorInfo__ctor_m2587556851 ();
extern "C" void AnchorInfo_get_Offset_m2032118130 ();
extern "C" void AnchorInfo_get_Width_m3275617445 ();
extern "C" void AnchorInfo_get_Length_m3963962564 ();
extern "C" void AnchorInfo_get_IsUnknownWidth_m3823293466 ();
extern "C" void AnchorInfo_get_IsComplete_m473968426 ();
extern "C" void AnchorInfo_get_Substring_m3135972819 ();
extern "C" void AnchorInfo_get_IgnoreCase_m3064680505 ();
extern "C" void AnchorInfo_get_Position_m1494480134 ();
extern "C" void AnchorInfo_get_IsSubstring_m1604771791 ();
extern "C" void AnchorInfo_get_IsPosition_m3903535620 ();
extern "C" void AnchorInfo_GetInterval_m3214481456 ();
extern "C" void Assertion__ctor_m986873442 ();
extern "C" void Assertion_get_TrueExpression_m2657609650 ();
extern "C" void Assertion_set_TrueExpression_m591545232 ();
extern "C" void Assertion_get_FalseExpression_m3458447370 ();
extern "C" void Assertion_set_FalseExpression_m3796428453 ();
extern "C" void Assertion_GetWidth_m3363495984 ();
extern "C" void BackslashNumber__ctor_m119971167 ();
extern "C" void BackslashNumber_ResolveReference_m546725419 ();
extern "C" void BackslashNumber_Compile_m3847188744 ();
extern "C" void BalancingGroup__ctor_m3920400059 ();
extern "C" void BalancingGroup_set_Balance_m1168008001 ();
extern "C" void BalancingGroup_Compile_m2333330309 ();
extern "C" void CaptureAssertion__ctor_m203256103 ();
extern "C" void CaptureAssertion_set_CapturingGroup_m2339232823 ();
extern "C" void CaptureAssertion_Compile_m991063116 ();
extern "C" void CaptureAssertion_IsComplex_m762484280 ();
extern "C" void CaptureAssertion_get_Alternate_m1528927867 ();
extern "C" void CapturingGroup__ctor_m349869863 ();
extern "C" void CapturingGroup_get_Index_m1859381638 ();
extern "C" void CapturingGroup_set_Index_m1160728315 ();
extern "C" void CapturingGroup_get_Name_m3221585584 ();
extern "C" void CapturingGroup_set_Name_m402876030 ();
extern "C" void CapturingGroup_get_IsNamed_m3262980019 ();
extern "C" void CapturingGroup_Compile_m1038721027 ();
extern "C" void CapturingGroup_IsComplex_m800801524 ();
extern "C" void CapturingGroup_CompareTo_m1280739012 ();
extern "C" void CharacterClass__ctor_m3535361022 ();
extern "C" void CharacterClass__ctor_m3753319170 ();
extern "C" void CharacterClass__cctor_m3458400496 ();
extern "C" void CharacterClass_AddCategory_m2429202189 ();
extern "C" void CharacterClass_AddCharacter_m1723760262 ();
extern "C" void CharacterClass_AddRange_m2748186401 ();
extern "C" void CharacterClass_Compile_m2702964455 ();
extern "C" void CharacterClass_GetWidth_m3398986822 ();
extern "C" void CharacterClass_IsComplex_m3017285671 ();
extern "C" void CharacterClass_GetIntervalCost_m2636988507 ();
extern "C" void CompositeExpression__ctor_m2373332310 ();
extern "C" void CompositeExpression_get_Expressions_m423326886 ();
extern "C" void CompositeExpression_GetWidth_m2603012898 ();
extern "C" void CompositeExpression_IsComplex_m4270689462 ();
extern "C" void Expression__ctor_m1698997405 ();
extern "C" void Expression_GetFixedWidth_m1913178642 ();
extern "C" void Expression_GetAnchorInfo_m1368236587 ();
extern "C" void ExpressionAssertion__ctor_m958345060 ();
extern "C" void ExpressionAssertion_set_Reverse_m968216331 ();
extern "C" void ExpressionAssertion_set_Negate_m2078812428 ();
extern "C" void ExpressionAssertion_get_TestExpression_m3948843979 ();
extern "C" void ExpressionAssertion_set_TestExpression_m156845531 ();
extern "C" void ExpressionAssertion_Compile_m2275114285 ();
extern "C" void ExpressionAssertion_IsComplex_m3586836482 ();
extern "C" void ExpressionCollection__ctor_m2296781218 ();
extern "C" void ExpressionCollection_Add_m44362897 ();
extern "C" void ExpressionCollection_get_Item_m1218952264 ();
extern "C" void ExpressionCollection_set_Item_m514568351 ();
extern "C" void ExpressionCollection_OnValidate_m3600058657 ();
extern "C" void Group__ctor_m21310139 ();
extern "C" void Group_AppendExpression_m4182799354 ();
extern "C" void Group_Compile_m3459444973 ();
extern "C" void Group_GetWidth_m1951523732 ();
extern "C" void Group_GetAnchorInfo_m3067905628 ();
extern "C" void Literal__ctor_m3944583913 ();
extern "C" void Literal_CompileLiteral_m2295396345 ();
extern "C" void Literal_Compile_m3069156456 ();
extern "C" void Literal_GetWidth_m2642201717 ();
extern "C" void Literal_GetAnchorInfo_m2455849048 ();
extern "C" void Literal_IsComplex_m1836923678 ();
extern "C" void NonBacktrackingGroup__ctor_m100607149 ();
extern "C" void NonBacktrackingGroup_Compile_m50089232 ();
extern "C" void NonBacktrackingGroup_IsComplex_m1617853001 ();
extern "C" void Parser__ctor_m994026618 ();
extern "C" void Parser_ParseDecimal_m3607451297 ();
extern "C" void Parser_ParseOctal_m707484128 ();
extern "C" void Parser_ParseHex_m107542399 ();
extern "C" void Parser_ParseNumber_m4036076299 ();
extern "C" void Parser_ParseName_m795481024 ();
extern "C" void Parser_ParseRegularExpression_m94923152 ();
extern "C" void Parser_GetMapping_m1042730142 ();
extern "C" void Parser_ParseGroup_m1087700183 ();
extern "C" void Parser_ParseGroupingConstruct_m595122439 ();
extern "C" void Parser_ParseAssertionType_m534871963 ();
extern "C" void Parser_ParseOptions_m405080860 ();
extern "C" void Parser_ParseCharacterClass_m2750765194 ();
extern "C" void Parser_ParseRepetitionBounds_m3016884405 ();
extern "C" void Parser_ParseUnicodeCategory_m365050900 ();
extern "C" void Parser_ParseSpecial_m2277867298 ();
extern "C" void Parser_ParseEscape_m118911671 ();
extern "C" void Parser_ParseName_m4099893475 ();
extern "C" void Parser_IsNameChar_m3039549884 ();
extern "C" void Parser_ParseNumber_m2192324642 ();
extern "C" void Parser_ParseDigit_m826611528 ();
extern "C" void Parser_ConsumeWhitespace_m557069978 ();
extern "C" void Parser_ResolveReferences_m1171255232 ();
extern "C" void Parser_HandleExplicitNumericGroups_m2400775214 ();
extern "C" void Parser_IsIgnoreCase_m1568471712 ();
extern "C" void Parser_IsMultiline_m2672509604 ();
extern "C" void Parser_IsExplicitCapture_m2908769234 ();
extern "C" void Parser_IsSingleline_m4271082409 ();
extern "C" void Parser_IsIgnorePatternWhitespace_m213713411 ();
extern "C" void Parser_IsECMAScript_m3290963469 ();
extern "C" void Parser_NewParseException_m2148015940 ();
extern "C" void PositionAssertion__ctor_m2606603788 ();
extern "C" void PositionAssertion_Compile_m2554548798 ();
extern "C" void PositionAssertion_GetWidth_m1818973288 ();
extern "C" void PositionAssertion_IsComplex_m3193019255 ();
extern "C" void PositionAssertion_GetAnchorInfo_m1351470229 ();
extern "C" void Reference__ctor_m432585372 ();
extern "C" void Reference_get_CapturingGroup_m3385097773 ();
extern "C" void Reference_set_CapturingGroup_m4031127075 ();
extern "C" void Reference_get_IgnoreCase_m2455052324 ();
extern "C" void Reference_Compile_m4270342402 ();
extern "C" void Reference_GetWidth_m2118856808 ();
extern "C" void Reference_IsComplex_m2224049982 ();
extern "C" void RegularExpression__ctor_m190838148 ();
extern "C" void RegularExpression_set_GroupCount_m907963075 ();
extern "C" void RegularExpression_Compile_m1271264672 ();
extern "C" void Repetition__ctor_m2150805284 ();
extern "C" void Repetition_get_Expression_m2931223493 ();
extern "C" void Repetition_set_Expression_m2410579300 ();
extern "C" void Repetition_get_Minimum_m1505192843 ();
extern "C" void Repetition_Compile_m3606158506 ();
extern "C" void Repetition_GetWidth_m2532613934 ();
extern "C" void Repetition_GetAnchorInfo_m1189699050 ();
extern "C" void Uri__ctor_m2889882320 ();
extern "C" void Uri__ctor_m3306082530 ();
extern "C" void Uri__ctor_m989934718 ();
extern "C" void Uri__ctor_m1783104167 ();
extern "C" void Uri__ctor_m3032927579 ();
extern "C" void Uri__cctor_m1161855378 ();
extern "C" void Uri_Merge_m1266107362 ();
extern "C" void Uri_get_AbsoluteUri_m1866097448 ();
extern "C" void Uri_get_Authority_m2389499900 ();
extern "C" void Uri_get_Host_m820743893 ();
extern "C" void Uri_get_IsFile_m2531602476 ();
extern "C" void Uri_get_IsLoopback_m262667925 ();
extern "C" void Uri_get_IsUnc_m3063098029 ();
extern "C" void Uri_get_Scheme_m3427514748 ();
extern "C" void Uri_get_IsAbsoluteUri_m1364944176 ();
extern "C" void Uri_get_OriginalString_m3974371109 ();
extern "C" void Uri_CheckHostName_m3929918188 ();
extern "C" void Uri_IsIPv4Address_m3644737395 ();
extern "C" void Uri_IsDomainAddress_m3525740579 ();
extern "C" void Uri_CheckSchemeName_m1082429697 ();
extern "C" void Uri_IsAlpha_m319673644 ();
extern "C" void Uri_Equals_m2964182858 ();
extern "C" void Uri_InternalEquals_m3661401220 ();
extern "C" void Uri_GetHashCode_m1009930693 ();
extern "C" void Uri_GetLeftPart_m3893306517 ();
extern "C" void Uri_FromHex_m686355279 ();
extern "C" void Uri_HexEscape_m2931306801 ();
extern "C" void Uri_IsHexDigit_m3446062221 ();
extern "C" void Uri_IsHexEncoding_m2663971616 ();
extern "C" void Uri_AppendQueryAndFragment_m3441955563 ();
extern "C" void Uri_ToString_m1672165748 ();
extern "C" void Uri_EscapeString_m4163751340 ();
extern "C" void Uri_EscapeString_m2527607796 ();
extern "C" void Uri_ParseUri_m4141183113 ();
extern "C" void Uri_Unescape_m2753918802 ();
extern "C" void Uri_Unescape_m3041304630 ();
extern "C" void Uri_ParseAsWindowsUNC_m4264642627 ();
extern "C" void Uri_ParseAsWindowsAbsoluteFilePath_m3582223925 ();
extern "C" void Uri_ParseAsUnixAbsoluteFilePath_m1263181827 ();
extern "C" void Uri_Parse_m2500470841 ();
extern "C" void Uri_ParseNoExceptions_m3436493051 ();
extern "C" void Uri_CompactEscaped_m2276642188 ();
extern "C" void Uri_Reduce_m433901093 ();
extern "C" void Uri_HexUnescapeMultiByte_m2288286187 ();
extern "C" void Uri_GetSchemeDelimiter_m2083514957 ();
extern "C" void Uri_GetDefaultPort_m1829954335 ();
extern "C" void Uri_GetOpaqueWiseSchemeDelimiter_m917397071 ();
extern "C" void Uri_IsPredefinedScheme_m1895878122 ();
extern "C" void Uri_get_Parser_m4141899287 ();
extern "C" void Uri_EnsureAbsoluteUri_m2092394955 ();
extern "C" void Uri_op_Equality_m1239543256 ();
extern "C" void UriScheme__ctor_m3911154691_AdjustorThunk ();
extern "C" void UriFormatException__ctor_m2640744054 ();
extern "C" void UriFormatException__ctor_m1390276363 ();
extern "C" void UriFormatException__ctor_m4068537724 ();
extern "C" void UriParser__ctor_m457649601 ();
extern "C" void UriParser__cctor_m1276965653 ();
extern "C" void UriParser_InitializeAndValidate_m3236017038 ();
extern "C" void UriParser_OnRegister_m3456593883 ();
extern "C" void UriParser_set_SchemeName_m895622768 ();
extern "C" void UriParser_get_DefaultPort_m838775600 ();
extern "C" void UriParser_set_DefaultPort_m2606622307 ();
extern "C" void UriParser_CreateDefaults_m433879834 ();
extern "C" void UriParser_InternalRegister_m2443930864 ();
extern "C" void UriParser_GetParser_m2758076409 ();
extern "C" void Locale_GetText_m2131320010 ();
extern "C" void BigInteger__ctor_m2308028736 ();
extern "C" void BigInteger__ctor_m3171712754 ();
extern "C" void BigInteger__ctor_m615970763 ();
extern "C" void BigInteger__ctor_m362145393 ();
extern "C" void BigInteger__ctor_m243072073 ();
extern "C" void BigInteger__cctor_m2442185530 ();
extern "C" void BigInteger_get_Rng_m2491015065 ();
extern "C" void BigInteger_GenerateRandom_m2808881327 ();
extern "C" void BigInteger_GenerateRandom_m1605528331 ();
extern "C" void BigInteger_BitCount_m1686537047 ();
extern "C" void BigInteger_TestBit_m316041159 ();
extern "C" void BigInteger_SetBit_m1692600095 ();
extern "C" void BigInteger_SetBit_m1046987020 ();
extern "C" void BigInteger_LowestSetBit_m2473842285 ();
extern "C" void BigInteger_GetBytes_m2037018530 ();
extern "C" void BigInteger_ToString_m1741067599 ();
extern "C" void BigInteger_ToString_m2080477611 ();
extern "C" void BigInteger_Normalize_m769597197 ();
extern "C" void BigInteger_Clear_m3473385376 ();
extern "C" void BigInteger_GetHashCode_m2482243794 ();
extern "C" void BigInteger_ToString_m2162538040 ();
extern "C" void BigInteger_Equals_m3286409704 ();
extern "C" void BigInteger_ModInverse_m1816311639 ();
extern "C" void BigInteger_ModPow_m2564492426 ();
extern "C" void BigInteger_GeneratePseudoPrime_m3506510940 ();
extern "C" void BigInteger_Incr2_m1100972076 ();
extern "C" void BigInteger_op_Implicit_m2994795725 ();
extern "C" void BigInteger_op_Implicit_m11032657 ();
extern "C" void BigInteger_op_Addition_m2099490839 ();
extern "C" void BigInteger_op_Subtraction_m51355653 ();
extern "C" void BigInteger_op_Modulus_m1177843781 ();
extern "C" void BigInteger_op_Modulus_m2613660825 ();
extern "C" void BigInteger_op_Division_m3628993410 ();
extern "C" void BigInteger_op_Multiply_m2420459962 ();
extern "C" void BigInteger_op_LeftShift_m1848949952 ();
extern "C" void BigInteger_op_RightShift_m3400645099 ();
extern "C" void BigInteger_op_Equality_m4238039561 ();
extern "C" void BigInteger_op_Inequality_m1859198082 ();
extern "C" void BigInteger_op_Equality_m70849326 ();
extern "C" void BigInteger_op_Inequality_m1778768838 ();
extern "C" void BigInteger_op_GreaterThan_m638226726 ();
extern "C" void BigInteger_op_LessThan_m2678691959 ();
extern "C" void BigInteger_op_GreaterThanOrEqual_m3465250365 ();
extern "C" void BigInteger_op_LessThanOrEqual_m2403737078 ();
extern "C" void Kernel_AddSameSign_m1011458853 ();
extern "C" void Kernel_Subtract_m2796122117 ();
extern "C" void Kernel_MinusEq_m469225165 ();
extern "C" void Kernel_PlusEq_m364008632 ();
extern "C" void Kernel_Compare_m3482954104 ();
extern "C" void Kernel_SingleByteDivideInPlace_m3966606311 ();
extern "C" void Kernel_DwordMod_m1670481718 ();
extern "C" void Kernel_DwordDivMod_m2601546874 ();
extern "C" void Kernel_multiByteDivide_m866259005 ();
extern "C" void Kernel_LeftShift_m3811357822 ();
extern "C" void Kernel_RightShift_m2142365962 ();
extern "C" void Kernel_Multiply_m1954393632 ();
extern "C" void Kernel_MultiplyMod2p32pmod_m1333838302 ();
extern "C" void Kernel_modInverse_m1758606707 ();
extern "C" void Kernel_modInverse_m2736680205 ();
extern "C" void ModulusRing__ctor_m4093869256 ();
extern "C" void ModulusRing_BarrettReduction_m2408871840 ();
extern "C" void ModulusRing_Multiply_m96746229 ();
extern "C" void ModulusRing_Difference_m2351720885 ();
extern "C" void ModulusRing_Pow_m2025138622 ();
extern "C" void ModulusRing_Pow_m3064208772 ();
extern "C" void PrimeGeneratorBase__ctor_m3635263837 ();
extern "C" void PrimeGeneratorBase_get_Confidence_m305234063 ();
extern "C" void PrimeGeneratorBase_get_PrimalityTest_m282858035 ();
extern "C" void PrimeGeneratorBase_get_TrialDivisionBounds_m4276340975 ();
extern "C" void SequentialSearchPrimeGeneratorBase__ctor_m520140196 ();
extern "C" void SequentialSearchPrimeGeneratorBase_GenerateSearchBase_m409061324 ();
extern "C" void SequentialSearchPrimeGeneratorBase_GenerateNewPrime_m2105446663 ();
extern "C" void SequentialSearchPrimeGeneratorBase_GenerateNewPrime_m3907457379 ();
extern "C" void SequentialSearchPrimeGeneratorBase_IsPrimeAcceptable_m2007849658 ();
extern "C" void PrimalityTest__ctor_m1858078853 ();
extern "C" void PrimalityTest_Invoke_m4206037601 ();
extern "C" void PrimalityTest_BeginInvoke_m1453150016 ();
extern "C" void PrimalityTest_EndInvoke_m3200086668 ();
extern "C" void PrimalityTests_GetSPPRounds_m871637100 ();
extern "C" void PrimalityTests_RabinMillerTest_m3226325974 ();
extern "C" void ASN1__ctor_m422521756 ();
extern "C" void ASN1__ctor_m1095438146 ();
extern "C" void ASN1__ctor_m1611489005 ();
extern "C" void ASN1_get_Count_m3317994203 ();
extern "C" void ASN1_get_Tag_m3154625155 ();
extern "C" void ASN1_get_Length_m2444412568 ();
extern "C" void ASN1_get_Value_m1819304453 ();
extern "C" void ASN1_set_Value_m175625800 ();
extern "C" void ASN1_CompareArray_m1535252745 ();
extern "C" void ASN1_CompareValue_m3412966293 ();
extern "C" void ASN1_Add_m2288114432 ();
extern "C" void ASN1_GetBytes_m1668622561 ();
extern "C" void ASN1_Decode_m2612428633 ();
extern "C" void ASN1_DecodeTLV_m2756715111 ();
extern "C" void ASN1_get_Item_m1951259524 ();
extern "C" void ASN1_Element_m1528007789 ();
extern "C" void ASN1_ToString_m397861702 ();
extern "C" void ASN1Convert_FromInt32_m819005996 ();
extern "C" void ASN1Convert_FromOid_m1513090420 ();
extern "C" void ASN1Convert_ToInt32_m1552911168 ();
extern "C" void ASN1Convert_ToOid_m2408239793 ();
extern "C" void ASN1Convert_ToDateTime_m3980897436 ();
extern "C" void BitConverterLE_GetUIntBytes_m807411464 ();
extern "C" void BitConverterLE_GetBytes_m734958349 ();
extern "C" void ARC4Managed__ctor_m1680223106 ();
extern "C" void ARC4Managed_Finalize_m2498238408 ();
extern "C" void ARC4Managed_Dispose_m2115957608 ();
extern "C" void ARC4Managed_get_Key_m2238158501 ();
extern "C" void ARC4Managed_set_Key_m3263334828 ();
extern "C" void ARC4Managed_get_CanReuseTransform_m3630055525 ();
extern "C" void ARC4Managed_CreateEncryptor_m1817801073 ();
extern "C" void ARC4Managed_CreateDecryptor_m810630513 ();
extern "C" void ARC4Managed_GenerateIV_m659906570 ();
extern "C" void ARC4Managed_GenerateKey_m3379407989 ();
extern "C" void ARC4Managed_KeySetup_m897077668 ();
extern "C" void ARC4Managed_CheckInput_m1098444684 ();
extern "C" void ARC4Managed_TransformBlock_m2909856413 ();
extern "C" void ARC4Managed_InternalTransformBlock_m2468969814 ();
extern "C" void ARC4Managed_TransformFinalBlock_m2167193542 ();
extern "C" void CryptoConvert_ToHex_m57122482 ();
extern "C" void HMAC__ctor_m3129484148 ();
extern "C" void HMAC_get_Key_m633315024 ();
extern "C" void HMAC_set_Key_m3197037643 ();
extern "C" void HMAC_Initialize_m608946158 ();
extern "C" void HMAC_HashFinal_m1669397226 ();
extern "C" void HMAC_HashCore_m2412365031 ();
extern "C" void HMAC_initializePad_m3602989081 ();
extern "C" void KeyBuilder_get_Rng_m277651138 ();
extern "C" void KeyBuilder_Key_m1565585763 ();
extern "C" void MD2__ctor_m4000919451 ();
extern "C" void MD2_Create_m3930551753 ();
extern "C" void MD2_Create_m2340992737 ();
extern "C" void MD2Managed__ctor_m382807057 ();
extern "C" void MD2Managed__cctor_m3904963690 ();
extern "C" void MD2Managed_Padding_m3245433322 ();
extern "C" void MD2Managed_Initialize_m2623837391 ();
extern "C" void MD2Managed_HashCore_m2710063179 ();
extern "C" void MD2Managed_HashFinal_m2009734036 ();
extern "C" void MD2Managed_MD2Transform_m2789759401 ();
extern "C" void MD4__ctor_m1424323227 ();
extern "C" void MD4_Create_m4156815636 ();
extern "C" void MD4_Create_m362115730 ();
extern "C" void MD4Managed__ctor_m306214366 ();
extern "C" void MD4Managed_Initialize_m3016287782 ();
extern "C" void MD4Managed_HashCore_m2553927469 ();
extern "C" void MD4Managed_HashFinal_m2800568498 ();
extern "C" void MD4Managed_Padding_m1469247200 ();
extern "C" void MD4Managed_F_m3644607136 ();
extern "C" void MD4Managed_G_m2155314467 ();
extern "C" void MD4Managed_H_m2132898000 ();
extern "C" void MD4Managed_ROL_m1557079893 ();
extern "C" void MD4Managed_FF_m1686378673 ();
extern "C" void MD4Managed_GG_m348726550 ();
extern "C" void MD4Managed_HH_m502459923 ();
extern "C" void MD4Managed_Encode_m2374965113 ();
extern "C" void MD4Managed_Decode_m2724071192 ();
extern "C" void MD4Managed_MD4Transform_m1384700328 ();
extern "C" void MD5SHA1__ctor_m141549212 ();
extern "C" void MD5SHA1_Initialize_m4176591359 ();
extern "C" void MD5SHA1_HashFinal_m53908192 ();
extern "C" void MD5SHA1_HashCore_m3587480270 ();
extern "C" void MD5SHA1_CreateSignature_m3994174688 ();
extern "C" void MD5SHA1_VerifySignature_m3951985362 ();
extern "C" void PKCS1__cctor_m2308867695 ();
extern "C" void PKCS1_Compare_m1301858489 ();
extern "C" void PKCS1_I2OSP_m2661230431 ();
extern "C" void PKCS1_OS2IP_m1981153093 ();
extern "C" void PKCS1_RSASP1_m2735422247 ();
extern "C" void PKCS1_RSAVP1_m4163980682 ();
extern "C" void PKCS1_Sign_v15_m1091855541 ();
extern "C" void PKCS1_Verify_v15_m429911942 ();
extern "C" void PKCS1_Verify_v15_m2387859380 ();
extern "C" void PKCS1_Encode_v15_m3235444401 ();
extern "C" void EncryptedPrivateKeyInfo__ctor_m1369443821 ();
extern "C" void EncryptedPrivateKeyInfo__ctor_m1780555356 ();
extern "C" void EncryptedPrivateKeyInfo_get_Algorithm_m484892361 ();
extern "C" void EncryptedPrivateKeyInfo_get_EncryptedData_m2501499073 ();
extern "C" void EncryptedPrivateKeyInfo_get_Salt_m2755441025 ();
extern "C" void EncryptedPrivateKeyInfo_get_IterationCount_m1961254953 ();
extern "C" void EncryptedPrivateKeyInfo_Decode_m3723838982 ();
extern "C" void PrivateKeyInfo__ctor_m4193825413 ();
extern "C" void PrivateKeyInfo__ctor_m236823045 ();
extern "C" void PrivateKeyInfo_get_PrivateKey_m2257075769 ();
extern "C" void PrivateKeyInfo_Decode_m1168779021 ();
extern "C" void PrivateKeyInfo_RemoveLeadingZero_m3607149124 ();
extern "C" void PrivateKeyInfo_Normalize_m18561129 ();
extern "C" void PrivateKeyInfo_DecodeRSA_m2527159846 ();
extern "C" void PrivateKeyInfo_DecodeDSA_m3518106339 ();
extern "C" void RC4__ctor_m1614144708 ();
extern "C" void RC4__cctor_m663776314 ();
extern "C" void RC4_get_IV_m3829665134 ();
extern "C" void RC4_set_IV_m2803946971 ();
extern "C" void RSAManaged__ctor_m2588544502 ();
extern "C" void RSAManaged__ctor_m2146688169 ();
extern "C" void RSAManaged_Finalize_m1814511608 ();
extern "C" void RSAManaged_GenerateKeyPair_m4206580424 ();
extern "C" void RSAManaged_get_KeySize_m2880143614 ();
extern "C" void RSAManaged_get_PublicOnly_m1179459602 ();
extern "C" void RSAManaged_DecryptValue_m1364493552 ();
extern "C" void RSAManaged_EncryptValue_m1230410218 ();
extern "C" void RSAManaged_ExportParameters_m2321887942 ();
extern "C" void RSAManaged_ImportParameters_m422789515 ();
extern "C" void RSAManaged_Dispose_m2829974002 ();
extern "C" void RSAManaged_ToXmlString_m2015408336 ();
extern "C" void RSAManaged_GetPaddedValue_m1577181261 ();
extern "C" void KeyGeneratedEventHandler__ctor_m519059567 ();
extern "C" void KeyGeneratedEventHandler_Invoke_m3074426872 ();
extern "C" void KeyGeneratedEventHandler_BeginInvoke_m3341643428 ();
extern "C" void KeyGeneratedEventHandler_EndInvoke_m2513620429 ();
extern "C" void ContentInfo__ctor_m1533696749 ();
extern "C" void ContentInfo__ctor_m3573770304 ();
extern "C" void ContentInfo__ctor_m1358525215 ();
extern "C" void ContentInfo__ctor_m3973273357 ();
extern "C" void ContentInfo_get_ASN1_m2002895343 ();
extern "C" void ContentInfo_get_Content_m2711381510 ();
extern "C" void ContentInfo_set_Content_m4086407349 ();
extern "C" void ContentInfo_get_ContentType_m3983016568 ();
extern "C" void ContentInfo_set_ContentType_m3006149204 ();
extern "C" void ContentInfo_GetASN1_m1506703338 ();
extern "C" void EncryptedData__ctor_m2779155492 ();
extern "C" void EncryptedData__ctor_m3679851962 ();
extern "C" void EncryptedData_get_EncryptionAlgorithm_m1085644144 ();
extern "C" void EncryptedData_get_EncryptedContent_m47715378 ();
extern "C" void Alert__ctor_m1393392031 ();
extern "C" void Alert__ctor_m1320563832 ();
extern "C" void Alert_get_Level_m2083224504 ();
extern "C" void Alert_get_Description_m1428909104 ();
extern "C" void Alert_get_IsWarning_m3430085096 ();
extern "C" void Alert_get_IsCloseNotify_m2968547217 ();
extern "C" void Alert_inferAlertLevel_m4178003709 ();
extern "C" void Alert_GetAlertMessage_m1333758696 ();
extern "C" void CertificateSelectionCallback__ctor_m4216130626 ();
extern "C" void CertificateSelectionCallback_Invoke_m3974939328 ();
extern "C" void CertificateSelectionCallback_BeginInvoke_m1529504173 ();
extern "C" void CertificateSelectionCallback_EndInvoke_m1890837795 ();
extern "C" void CertificateValidationCallback__ctor_m525670167 ();
extern "C" void CertificateValidationCallback_Invoke_m3368819440 ();
extern "C" void CertificateValidationCallback_BeginInvoke_m1555238601 ();
extern "C" void CertificateValidationCallback_EndInvoke_m455360328 ();
extern "C" void CertificateValidationCallback2__ctor_m486525434 ();
extern "C" void CertificateValidationCallback2_Invoke_m2696404194 ();
extern "C" void CertificateValidationCallback2_BeginInvoke_m3602893401 ();
extern "C" void CertificateValidationCallback2_EndInvoke_m1941583874 ();
extern "C" void CipherSuite__ctor_m1649596800 ();
extern "C" void CipherSuite__cctor_m18845967 ();
extern "C" void CipherSuite_get_EncryptionCipher_m1493897844 ();
extern "C" void CipherSuite_get_DecryptionCipher_m2103278815 ();
extern "C" void CipherSuite_get_ClientHMAC_m676756116 ();
extern "C" void CipherSuite_get_ServerHMAC_m1863563213 ();
extern "C" void CipherSuite_get_CipherAlgorithmType_m7547507 ();
extern "C" void CipherSuite_get_HashAlgorithmName_m4035231448 ();
extern "C" void CipherSuite_get_HashAlgorithmType_m1343605085 ();
extern "C" void CipherSuite_get_HashSize_m279623022 ();
extern "C" void CipherSuite_get_ExchangeAlgorithmType_m3822413550 ();
extern "C" void CipherSuite_get_CipherMode_m1340193226 ();
extern "C" void CipherSuite_get_Code_m1986756097 ();
extern "C" void CipherSuite_get_Name_m3234115705 ();
extern "C" void CipherSuite_get_IsExportable_m2351943288 ();
extern "C" void CipherSuite_get_KeyMaterialSize_m3030891731 ();
extern "C" void CipherSuite_get_KeyBlockSize_m3008502075 ();
extern "C" void CipherSuite_get_ExpandedKeyMaterialSize_m3112230581 ();
extern "C" void CipherSuite_get_EffectiveKeyBits_m4097074249 ();
extern "C" void CipherSuite_get_IvSize_m2894146290 ();
extern "C" void CipherSuite_get_Context_m528611831 ();
extern "C" void CipherSuite_set_Context_m2673566586 ();
extern "C" void CipherSuite_Write_m1117393086 ();
extern "C" void CipherSuite_Write_m2878525702 ();
extern "C" void CipherSuite_InitializeCipher_m3081234157 ();
extern "C" void CipherSuite_EncryptRecord_m1939294643 ();
extern "C" void CipherSuite_DecryptRecord_m4161872080 ();
extern "C" void CipherSuite_CreatePremasterSecret_m82851275 ();
extern "C" void CipherSuite_PRF_m205785501 ();
extern "C" void CipherSuite_Expand_m2588778289 ();
extern "C" void CipherSuite_createEncryptionCipher_m1823110658 ();
extern "C" void CipherSuite_createDecryptionCipher_m4171513940 ();
extern "C" void CipherSuiteCollection__ctor_m3924782681 ();
extern "C" void CipherSuiteCollection_System_Collections_IList_get_Item_m1665840681 ();
extern "C" void CipherSuiteCollection_System_Collections_IList_set_Item_m3478900428 ();
extern "C" void CipherSuiteCollection_System_Collections_ICollection_get_SyncRoot_m618580436 ();
extern "C" void CipherSuiteCollection_System_Collections_IEnumerable_GetEnumerator_m1681536898 ();
extern "C" void CipherSuiteCollection_System_Collections_IList_Contains_m1634347190 ();
extern "C" void CipherSuiteCollection_System_Collections_IList_IndexOf_m4213006854 ();
extern "C" void CipherSuiteCollection_System_Collections_IList_Insert_m2008070196 ();
extern "C" void CipherSuiteCollection_System_Collections_IList_Remove_m144704960 ();
extern "C" void CipherSuiteCollection_System_Collections_IList_RemoveAt_m2247851113 ();
extern "C" void CipherSuiteCollection_System_Collections_IList_Add_m1074649344 ();
extern "C" void CipherSuiteCollection_get_Item_m3735835164 ();
extern "C" void CipherSuiteCollection_get_Item_m653515770 ();
extern "C" void CipherSuiteCollection_set_Item_m592704906 ();
extern "C" void CipherSuiteCollection_get_Item_m2780174626 ();
extern "C" void CipherSuiteCollection_get_Count_m3813525106 ();
extern "C" void CipherSuiteCollection_CopyTo_m576332140 ();
extern "C" void CipherSuiteCollection_Clear_m2914820078 ();
extern "C" void CipherSuiteCollection_IndexOf_m3088990859 ();
extern "C" void CipherSuiteCollection_IndexOf_m685492255 ();
extern "C" void CipherSuiteCollection_Add_m3191239374 ();
extern "C" void CipherSuiteCollection_add_m2674530140 ();
extern "C" void CipherSuiteCollection_add_m1076941497 ();
extern "C" void CipherSuiteCollection_cultureAwareCompare_m4026891737 ();
extern "C" void CipherSuiteFactory_GetSupportedCiphers_m1331312394 ();
extern "C" void CipherSuiteFactory_GetTls1SupportedCiphers_m4063178148 ();
extern "C" void CipherSuiteFactory_GetSsl3SupportedCiphers_m1436345897 ();
extern "C" void ClientContext__ctor_m1540414373 ();
extern "C" void ClientContext_get_SslStream_m4172965777 ();
extern "C" void ClientContext_get_ClientHelloProtocol_m687123721 ();
extern "C" void ClientContext_set_ClientHelloProtocol_m2033712663 ();
extern "C" void ClientContext_Clear_m3230671407 ();
extern "C" void ClientRecordProtocol__ctor_m1030222096 ();
extern "C" void ClientRecordProtocol_GetMessage_m4174671419 ();
extern "C" void ClientRecordProtocol_ProcessHandshakeMessage_m2179640220 ();
extern "C" void ClientRecordProtocol_createClientHandshakeMessage_m955189407 ();
extern "C" void ClientRecordProtocol_createServerHandshakeMessage_m719154186 ();
extern "C" void ClientSessionCache__cctor_m1535243483 ();
extern "C" void ClientSessionCache_Add_m1577846793 ();
extern "C" void ClientSessionCache_FromHost_m3356571309 ();
extern "C" void ClientSessionCache_FromContext_m2612279874 ();
extern "C" void ClientSessionCache_SetContextInCache_m1849958011 ();
extern "C" void ClientSessionCache_SetContextFromCache_m543772420 ();
extern "C" void ClientSessionInfo__ctor_m200781604 ();
extern "C" void ClientSessionInfo__cctor_m1222200069 ();
extern "C" void ClientSessionInfo_Finalize_m2714646038 ();
extern "C" void ClientSessionInfo_get_HostName_m4100927670 ();
extern "C" void ClientSessionInfo_get_Id_m3432574791 ();
extern "C" void ClientSessionInfo_get_Valid_m266085941 ();
extern "C" void ClientSessionInfo_GetContext_m1607090003 ();
extern "C" void ClientSessionInfo_SetContext_m3958182732 ();
extern "C" void ClientSessionInfo_KeepAlive_m3825761449 ();
extern "C" void ClientSessionInfo_Dispose_m3094936779 ();
extern "C" void ClientSessionInfo_Dispose_m19476096 ();
extern "C" void ClientSessionInfo_CheckDisposed_m2780843044 ();
extern "C" void Context__ctor_m1933010603 ();
extern "C" void Context_get_AbbreviatedHandshake_m1283450948 ();
extern "C" void Context_set_AbbreviatedHandshake_m3236415809 ();
extern "C" void Context_get_ProtocolNegotiated_m2069490626 ();
extern "C" void Context_set_ProtocolNegotiated_m3332049274 ();
extern "C" void Context_get_SecurityProtocol_m2558379614 ();
extern "C" void Context_set_SecurityProtocol_m3637591545 ();
extern "C" void Context_get_SecurityProtocolFlags_m151222537 ();
extern "C" void Context_get_Protocol_m3375473980 ();
extern "C" void Context_get_SessionId_m3145132411 ();
extern "C" void Context_set_SessionId_m3350823672 ();
extern "C" void Context_get_CompressionMethod_m2648417057 ();
extern "C" void Context_set_CompressionMethod_m2403980270 ();
extern "C" void Context_get_ServerSettings_m398675875 ();
extern "C" void Context_get_ClientSettings_m1279324710 ();
extern "C" void Context_get_LastHandshakeMsg_m1594066191 ();
extern "C" void Context_set_LastHandshakeMsg_m2519018853 ();
extern "C" void Context_get_HandshakeState_m3595707 ();
extern "C" void Context_set_HandshakeState_m1690488869 ();
extern "C" void Context_get_ReceivedConnectionEnd_m1907593795 ();
extern "C" void Context_set_ReceivedConnectionEnd_m3576701554 ();
extern "C" void Context_get_SentConnectionEnd_m401517105 ();
extern "C" void Context_set_SentConnectionEnd_m162465705 ();
extern "C" void Context_get_SupportedCiphers_m4263361931 ();
extern "C" void Context_set_SupportedCiphers_m3597249771 ();
extern "C" void Context_get_HandshakeMessages_m279273501 ();
extern "C" void Context_get_WriteSequenceNumber_m723984201 ();
extern "C" void Context_set_WriteSequenceNumber_m580987874 ();
extern "C" void Context_get_ReadSequenceNumber_m3582050733 ();
extern "C" void Context_set_ReadSequenceNumber_m2996189414 ();
extern "C" void Context_get_ClientRandom_m2929287758 ();
extern "C" void Context_set_ClientRandom_m2620138301 ();
extern "C" void Context_get_ServerRandom_m3474718287 ();
extern "C" void Context_set_ServerRandom_m603384709 ();
extern "C" void Context_get_RandomCS_m356162013 ();
extern "C" void Context_set_RandomCS_m564871184 ();
extern "C" void Context_get_RandomSC_m1929134887 ();
extern "C" void Context_set_RandomSC_m1687935070 ();
extern "C" void Context_get_MasterSecret_m3410383318 ();
extern "C" void Context_set_MasterSecret_m308905945 ();
extern "C" void Context_get_ClientWriteKey_m2257942003 ();
extern "C" void Context_set_ClientWriteKey_m1677787488 ();
extern "C" void Context_get_ServerWriteKey_m4018642070 ();
extern "C" void Context_set_ServerWriteKey_m1838395661 ();
extern "C" void Context_get_ClientWriteIV_m2726740789 ();
extern "C" void Context_set_ClientWriteIV_m645053791 ();
extern "C" void Context_get_ServerWriteIV_m619755409 ();
extern "C" void Context_set_ServerWriteIV_m2460806526 ();
extern "C" void Context_get_RecordProtocol_m751043880 ();
extern "C" void Context_set_RecordProtocol_m1592092473 ();
extern "C" void Context_GetUnixTime_m1933377262 ();
extern "C" void Context_GetSecureRandomBytes_m843600736 ();
extern "C" void Context_Clear_m2220388284 ();
extern "C" void Context_ClearKeyInfo_m1711611987 ();
extern "C" void Context_DecodeProtocolCode_m494796791 ();
extern "C" void Context_ChangeProtocol_m91279606 ();
extern "C" void Context_get_Current_m3952135089 ();
extern "C" void Context_get_Negotiating_m3255494259 ();
extern "C" void Context_get_Read_m3972451212 ();
extern "C" void Context_get_Write_m193689081 ();
extern "C" void Context_StartSwitchingSecurityParameters_m1350107141 ();
extern "C" void Context_EndSwitchingSecurityParameters_m951771439 ();
extern "C" void TlsClientCertificate__ctor_m3870088466 ();
extern "C" void TlsClientCertificate_get_ClientCertificate_m3474951109 ();
extern "C" void TlsClientCertificate_Update_m2871223530 ();
extern "C" void TlsClientCertificate_GetClientCertificate_m1318921875 ();
extern "C" void TlsClientCertificate_SendCertificates_m1290742152 ();
extern "C" void TlsClientCertificate_ProcessAsSsl3_m1010799057 ();
extern "C" void TlsClientCertificate_ProcessAsTls1_m175582028 ();
extern "C" void TlsClientCertificate_FindParentCertificate_m292629370 ();
extern "C" void TlsClientCertificateVerify__ctor_m3725757645 ();
extern "C" void TlsClientCertificateVerify_Update_m2349762431 ();
extern "C" void TlsClientCertificateVerify_ProcessAsSsl3_m4281440818 ();
extern "C" void TlsClientCertificateVerify_ProcessAsTls1_m3714620977 ();
extern "C" void TlsClientCertificateVerify_getClientCertRSA_m1950298306 ();
extern "C" void TlsClientCertificateVerify_getUnsignedBigInteger_m4202515218 ();
extern "C" void TlsClientFinished__ctor_m3582310837 ();
extern "C" void TlsClientFinished__cctor_m823547282 ();
extern "C" void TlsClientFinished_Update_m1874184699 ();
extern "C" void TlsClientFinished_ProcessAsSsl3_m3712431540 ();
extern "C" void TlsClientFinished_ProcessAsTls1_m366904288 ();
extern "C" void TlsClientHello__ctor_m684319902 ();
extern "C" void TlsClientHello_Update_m2360830622 ();
extern "C" void TlsClientHello_ProcessAsSsl3_m3025764406 ();
extern "C" void TlsClientHello_ProcessAsTls1_m214976322 ();
extern "C" void TlsClientKeyExchange__ctor_m3216815788 ();
extern "C" void TlsClientKeyExchange_ProcessAsSsl3_m1197450518 ();
extern "C" void TlsClientKeyExchange_ProcessAsTls1_m447389176 ();
extern "C" void TlsClientKeyExchange_ProcessCommon_m2250894231 ();
extern "C" void TlsServerCertificate__ctor_m2064907602 ();
extern "C" void TlsServerCertificate_Update_m2284218846 ();
extern "C" void TlsServerCertificate_ProcessAsSsl3_m984023700 ();
extern "C" void TlsServerCertificate_ProcessAsTls1_m1003804327 ();
extern "C" void TlsServerCertificate_checkCertificateUsage_m2937881593 ();
extern "C" void TlsServerCertificate_validateCertificates_m2051954893 ();
extern "C" void TlsServerCertificate_checkServerIdentity_m627236923 ();
extern "C" void TlsServerCertificate_checkDomainName_m3854661665 ();
extern "C" void TlsServerCertificate_Match_m865697385 ();
extern "C" void TlsServerCertificateRequest__ctor_m2125663103 ();
extern "C" void TlsServerCertificateRequest_Update_m1099278369 ();
extern "C" void TlsServerCertificateRequest_ProcessAsSsl3_m3563106795 ();
extern "C" void TlsServerCertificateRequest_ProcessAsTls1_m1862660502 ();
extern "C" void TlsServerFinished__ctor_m2983114510 ();
extern "C" void TlsServerFinished__cctor_m2747700497 ();
extern "C" void TlsServerFinished_Update_m841801115 ();
extern "C" void TlsServerFinished_ProcessAsSsl3_m3221391108 ();
extern "C" void TlsServerFinished_ProcessAsTls1_m749942180 ();
extern "C" void TlsServerHello__ctor_m3506223265 ();
extern "C" void TlsServerHello_Update_m23191077 ();
extern "C" void TlsServerHello_ProcessAsSsl3_m2407954178 ();
extern "C" void TlsServerHello_ProcessAsTls1_m4113542019 ();
extern "C" void TlsServerHello_processProtocol_m1843286013 ();
extern "C" void TlsServerHelloDone__ctor_m433659303 ();
extern "C" void TlsServerHelloDone_ProcessAsSsl3_m3400236847 ();
extern "C" void TlsServerHelloDone_ProcessAsTls1_m3696684855 ();
extern "C" void TlsServerKeyExchange__ctor_m2476718815 ();
extern "C" void TlsServerKeyExchange_Update_m2297485777 ();
extern "C" void TlsServerKeyExchange_ProcessAsSsl3_m827932493 ();
extern "C" void TlsServerKeyExchange_ProcessAsTls1_m3464884619 ();
extern "C" void TlsServerKeyExchange_verifySignature_m4293518610 ();
extern "C" void HandshakeMessage__ctor_m2034673671 ();
extern "C" void HandshakeMessage__ctor_m1249436565 ();
extern "C" void HandshakeMessage__ctor_m878609821 ();
extern "C" void HandshakeMessage_get_Context_m800642781 ();
extern "C" void HandshakeMessage_get_HandshakeType_m280165016 ();
extern "C" void HandshakeMessage_get_ContentType_m3747812736 ();
extern "C" void HandshakeMessage_Process_m455376707 ();
extern "C" void HandshakeMessage_Update_m4254851083 ();
extern "C" void HandshakeMessage_EncodeMessage_m872692033 ();
extern "C" void HandshakeMessage_Compare_m3371152330 ();
extern "C" void HttpsClientStream__ctor_m4047578492 ();
extern "C" void HttpsClientStream_get_TrustFailure_m3656567659 ();
extern "C" void HttpsClientStream_RaiseServerCertificateValidation_m3451092113 ();
extern "C" void HttpsClientStream_U3CHttpsClientStreamU3Em__0_m4013616965 ();
extern "C" void HttpsClientStream_U3CHttpsClientStreamU3Em__1_m845478519 ();
extern "C" void PrivateKeySelectionCallback__ctor_m987086300 ();
extern "C" void PrivateKeySelectionCallback_Invoke_m2587741347 ();
extern "C" void PrivateKeySelectionCallback_BeginInvoke_m602585787 ();
extern "C" void PrivateKeySelectionCallback_EndInvoke_m715554842 ();
extern "C" void RecordProtocol__ctor_m2575003435 ();
extern "C" void RecordProtocol__cctor_m3988108282 ();
extern "C" void RecordProtocol_get_Context_m1248057354 ();
extern "C" void RecordProtocol_SendRecord_m3734241850 ();
extern "C" void RecordProtocol_ProcessChangeCipherSpec_m1652800479 ();
extern "C" void RecordProtocol_GetMessage_m1005402943 ();
extern "C" void RecordProtocol_BeginReceiveRecord_m1819664941 ();
extern "C" void RecordProtocol_InternalReceiveRecordCallback_m3030502302 ();
extern "C" void RecordProtocol_EndReceiveRecord_m908637232 ();
extern "C" void RecordProtocol_ReceiveRecord_m2708100793 ();
extern "C" void RecordProtocol_ReadRecordBuffer_m4167278120 ();
extern "C" void RecordProtocol_ReadClientHelloV2_m2117048537 ();
extern "C" void RecordProtocol_ReadStandardRecordBuffer_m871854570 ();
extern "C" void RecordProtocol_ProcessAlert_m1435473841 ();
extern "C" void RecordProtocol_SendAlert_m1591847065 ();
extern "C" void RecordProtocol_SendAlert_m1438883877 ();
extern "C" void RecordProtocol_SendAlert_m3376135484 ();
extern "C" void RecordProtocol_SendChangeCipherSpec_m2765603748 ();
extern "C" void RecordProtocol_BeginSendRecord_m1758967725 ();
extern "C" void RecordProtocol_InternalSendRecordCallback_m3525244677 ();
extern "C" void RecordProtocol_BeginSendRecord_m69707447 ();
extern "C" void RecordProtocol_EndSendRecord_m706267873 ();
extern "C" void RecordProtocol_SendRecord_m3284206208 ();
extern "C" void RecordProtocol_EncodeRecord_m4068158653 ();
extern "C" void RecordProtocol_EncodeRecord_m1038004725 ();
extern "C" void RecordProtocol_encryptRecordFragment_m2829067512 ();
extern "C" void RecordProtocol_decryptRecordFragment_m2696484119 ();
extern "C" void RecordProtocol_Compare_m3923473508 ();
extern "C" void RecordProtocol_ProcessCipherSpecV2Buffer_m4226882997 ();
extern "C" void RecordProtocol_MapV2CipherCode_m3168062753 ();
extern "C" void ReceiveRecordAsyncResult__ctor_m2154300256 ();
extern "C" void ReceiveRecordAsyncResult_get_Record_m906314389 ();
extern "C" void ReceiveRecordAsyncResult_get_ResultingBuffer_m4251431911 ();
extern "C" void ReceiveRecordAsyncResult_get_InitialBuffer_m3909001354 ();
extern "C" void ReceiveRecordAsyncResult_get_AsyncState_m605291345 ();
extern "C" void ReceiveRecordAsyncResult_get_AsyncException_m933790275 ();
extern "C" void ReceiveRecordAsyncResult_get_CompletedWithError_m2195810348 ();
extern "C" void ReceiveRecordAsyncResult_get_AsyncWaitHandle_m2660886631 ();
extern "C" void ReceiveRecordAsyncResult_get_IsCompleted_m2071034183 ();
extern "C" void ReceiveRecordAsyncResult_SetComplete_m3952692939 ();
extern "C" void ReceiveRecordAsyncResult_SetComplete_m2015699511 ();
extern "C" void ReceiveRecordAsyncResult_SetComplete_m1812808650 ();
extern "C" void SendRecordAsyncResult__ctor_m755282168 ();
extern "C" void SendRecordAsyncResult_get_Message_m3308066534 ();
extern "C" void SendRecordAsyncResult_get_AsyncState_m944538650 ();
extern "C" void SendRecordAsyncResult_get_AsyncException_m2357394367 ();
extern "C" void SendRecordAsyncResult_get_CompletedWithError_m3552731080 ();
extern "C" void SendRecordAsyncResult_get_AsyncWaitHandle_m236430258 ();
extern "C" void SendRecordAsyncResult_get_IsCompleted_m718960489 ();
extern "C" void SendRecordAsyncResult_SetComplete_m3183925613 ();
extern "C" void SendRecordAsyncResult_SetComplete_m52418369 ();
extern "C" void RSASslSignatureDeformatter__ctor_m4138457112 ();
extern "C" void RSASslSignatureDeformatter_VerifySignature_m3361685709 ();
extern "C" void RSASslSignatureDeformatter_SetHashAlgorithm_m1618534065 ();
extern "C" void RSASslSignatureDeformatter_SetKey_m377089225 ();
extern "C" void RSASslSignatureFormatter__ctor_m356293291 ();
extern "C" void RSASslSignatureFormatter_CreateSignature_m1712476209 ();
extern "C" void RSASslSignatureFormatter_SetHashAlgorithm_m1350789049 ();
extern "C" void RSASslSignatureFormatter_SetKey_m2867909730 ();
extern "C" void SecurityParameters__ctor_m2022418048 ();
extern "C" void SecurityParameters_get_Cipher_m1063581087 ();
extern "C" void SecurityParameters_set_Cipher_m3243262012 ();
extern "C" void SecurityParameters_get_ClientWriteMAC_m290543284 ();
extern "C" void SecurityParameters_set_ClientWriteMAC_m4234763313 ();
extern "C" void SecurityParameters_get_ServerWriteMAC_m3092245044 ();
extern "C" void SecurityParameters_set_ServerWriteMAC_m2950694019 ();
extern "C" void SecurityParameters_Clear_m3625150593 ();
extern "C" void SslCipherSuite__ctor_m1651142478 ();
extern "C" void SslCipherSuite_ComputeServerRecordMAC_m544101148 ();
extern "C" void SslCipherSuite_ComputeClientRecordMAC_m2895545260 ();
extern "C" void SslCipherSuite_ComputeMasterSecret_m2505358733 ();
extern "C" void SslCipherSuite_ComputeKeys_m39830501 ();
extern "C" void SslCipherSuite_prf_m180711818 ();
extern "C" void SslClientStream__ctor_m665219218 ();
extern "C" void SslClientStream__ctor_m2740959911 ();
extern "C" void SslClientStream__ctor_m1028290302 ();
extern "C" void SslClientStream__ctor_m2901368553 ();
extern "C" void SslClientStream__ctor_m3136062194 ();
extern "C" void SslClientStream_add_ServerCertValidation_m3989440226 ();
extern "C" void SslClientStream_remove_ServerCertValidation_m2153244654 ();
extern "C" void SslClientStream_add_ClientCertSelection_m3976789503 ();
extern "C" void SslClientStream_remove_ClientCertSelection_m4153072844 ();
extern "C" void SslClientStream_add_PrivateKeySelection_m302572209 ();
extern "C" void SslClientStream_remove_PrivateKeySelection_m765497415 ();
extern "C" void SslClientStream_add_ServerCertValidation2_m111614173 ();
extern "C" void SslClientStream_remove_ServerCertValidation2_m775112605 ();
extern "C" void SslClientStream_get_InputBuffer_m1496361990 ();
extern "C" void SslClientStream_get_ClientCertificates_m823297919 ();
extern "C" void SslClientStream_get_SelectedClientCertificate_m3564197154 ();
extern "C" void SslClientStream_get_ServerCertValidationDelegate_m1821601874 ();
extern "C" void SslClientStream_set_ServerCertValidationDelegate_m718527506 ();
extern "C" void SslClientStream_get_ClientCertSelectionDelegate_m2578027297 ();
extern "C" void SslClientStream_set_ClientCertSelectionDelegate_m3040495215 ();
extern "C" void SslClientStream_get_PrivateKeyCertSelectionDelegate_m593300911 ();
extern "C" void SslClientStream_set_PrivateKeyCertSelectionDelegate_m570404215 ();
extern "C" void SslClientStream_Finalize_m2099061251 ();
extern "C" void SslClientStream_Dispose_m3768043824 ();
extern "C" void SslClientStream_OnBeginNegotiateHandshake_m657883870 ();
extern "C" void SslClientStream_SafeReceiveRecord_m4103890754 ();
extern "C" void SslClientStream_OnNegotiateHandshakeCallback_m3697285055 ();
extern "C" void SslClientStream_OnLocalCertificateSelection_m2937715157 ();
extern "C" void SslClientStream_get_HaveRemoteValidation2Callback_m3527076427 ();
extern "C" void SslClientStream_OnRemoteCertificateValidation2_m462911753 ();
extern "C" void SslClientStream_OnRemoteCertificateValidation_m1077840005 ();
extern "C" void SslClientStream_RaiseServerCertificateValidation_m1966998054 ();
extern "C" void SslClientStream_RaiseServerCertificateValidation2_m1950146374 ();
extern "C" void SslClientStream_RaiseClientCertificateSelection_m2012516213 ();
extern "C" void SslClientStream_OnLocalPrivateKeySelection_m1524003374 ();
extern "C" void SslClientStream_RaisePrivateKeySelection_m3884094002 ();
extern "C" void SslHandshakeHash__ctor_m2169281860 ();
extern "C" void SslHandshakeHash_Initialize_m2904008621 ();
extern "C" void SslHandshakeHash_HashFinal_m2085770661 ();
extern "C" void SslHandshakeHash_HashCore_m491974851 ();
extern "C" void SslHandshakeHash_CreateSignature_m3373793317 ();
extern "C" void SslHandshakeHash_initializePad_m3858412911 ();
extern "C" void SslStreamBase__ctor_m1772390304 ();
extern "C" void SslStreamBase__cctor_m414404949 ();
extern "C" void SslStreamBase_AsyncHandshakeCallback_m3808198600 ();
extern "C" void SslStreamBase_get_MightNeedHandshake_m4001205381 ();
extern "C" void SslStreamBase_NegotiateHandshake_m3208295742 ();
extern "C" void SslStreamBase_RaiseLocalCertificateSelection_m1495337812 ();
extern "C" void SslStreamBase_RaiseRemoteCertificateValidation_m4089603057 ();
extern "C" void SslStreamBase_RaiseRemoteCertificateValidation2_m2332380679 ();
extern "C" void SslStreamBase_RaiseLocalPrivateKeySelection_m3341177607 ();
extern "C" void SslStreamBase_get_CheckCertRevocationStatus_m2587216488 ();
extern "C" void SslStreamBase_set_CheckCertRevocationStatus_m2274274923 ();
extern "C" void SslStreamBase_get_CipherAlgorithm_m3231557180 ();
extern "C" void SslStreamBase_get_CipherStrength_m2901011851 ();
extern "C" void SslStreamBase_get_HashAlgorithm_m1669198172 ();
extern "C" void SslStreamBase_get_HashStrength_m3464493701 ();
extern "C" void SslStreamBase_get_KeyExchangeStrength_m1007950534 ();
extern "C" void SslStreamBase_get_KeyExchangeAlgorithm_m4272137713 ();
extern "C" void SslStreamBase_get_SecurityProtocol_m3874437616 ();
extern "C" void SslStreamBase_get_ServerCertificate_m3445084879 ();
extern "C" void SslStreamBase_get_ServerCertificates_m1542603794 ();
extern "C" void SslStreamBase_BeginNegotiateHandshake_m361657815 ();
extern "C" void SslStreamBase_EndNegotiateHandshake_m2551208674 ();
extern "C" void SslStreamBase_BeginRead_m3935587377 ();
extern "C" void SslStreamBase_InternalBeginRead_m28581750 ();
extern "C" void SslStreamBase_InternalReadCallback_m3716251866 ();
extern "C" void SslStreamBase_InternalBeginWrite_m1167578983 ();
extern "C" void SslStreamBase_InternalWriteCallback_m1908010440 ();
extern "C" void SslStreamBase_BeginWrite_m4082615485 ();
extern "C" void SslStreamBase_EndRead_m269976927 ();
extern "C" void SslStreamBase_EndWrite_m3882228902 ();
extern "C" void SslStreamBase_Close_m2220408450 ();
extern "C" void SslStreamBase_Flush_m2233474192 ();
extern "C" void SslStreamBase_Read_m1140853820 ();
extern "C" void SslStreamBase_Read_m2130856565 ();
extern "C" void SslStreamBase_Seek_m3906062762 ();
extern "C" void SslStreamBase_SetLength_m1666023477 ();
extern "C" void SslStreamBase_Write_m4276739856 ();
extern "C" void SslStreamBase_Write_m4098670128 ();
extern "C" void SslStreamBase_get_CanRead_m3111230573 ();
extern "C" void SslStreamBase_get_CanSeek_m2857098779 ();
extern "C" void SslStreamBase_get_CanWrite_m4021455675 ();
extern "C" void SslStreamBase_get_Length_m3518888792 ();
extern "C" void SslStreamBase_get_Position_m729702628 ();
extern "C" void SslStreamBase_set_Position_m4050085577 ();
extern "C" void SslStreamBase_Finalize_m3438261461 ();
extern "C" void SslStreamBase_Dispose_m2592643090 ();
extern "C" void SslStreamBase_resetBuffer_m717654181 ();
extern "C" void SslStreamBase_checkDisposed_m167103266 ();
extern "C" void InternalAsyncResult__ctor_m1440820003 ();
extern "C" void InternalAsyncResult_get_ProceedAfterHandshake_m235102077 ();
extern "C" void InternalAsyncResult_get_FromWrite_m2276317514 ();
extern "C" void InternalAsyncResult_get_Buffer_m2345870937 ();
extern "C" void InternalAsyncResult_get_Offset_m3979641628 ();
extern "C" void InternalAsyncResult_get_Count_m163070607 ();
extern "C" void InternalAsyncResult_get_BytesRead_m1696748377 ();
extern "C" void InternalAsyncResult_get_AsyncState_m397037230 ();
extern "C" void InternalAsyncResult_get_AsyncException_m2172420170 ();
extern "C" void InternalAsyncResult_get_CompletedWithError_m2143240718 ();
extern "C" void InternalAsyncResult_get_AsyncWaitHandle_m1330063583 ();
extern "C" void InternalAsyncResult_get_IsCompleted_m3627122157 ();
extern "C" void InternalAsyncResult_SetComplete_m2262116736 ();
extern "C" void InternalAsyncResult_SetComplete_m3008594899 ();
extern "C" void InternalAsyncResult_SetComplete_m3038718358 ();
extern "C" void InternalAsyncResult_SetComplete_m2550592901 ();
extern "C" void TlsCipherSuite__ctor_m2875460209 ();
extern "C" void TlsCipherSuite_ComputeServerRecordMAC_m371468058 ();
extern "C" void TlsCipherSuite_ComputeClientRecordMAC_m824858403 ();
extern "C" void TlsCipherSuite_ComputeMasterSecret_m1531262657 ();
extern "C" void TlsCipherSuite_ComputeKeys_m570963112 ();
extern "C" void TlsClientSettings__ctor_m1818524944 ();
extern "C" void TlsClientSettings_get_TargetHost_m893351267 ();
extern "C" void TlsClientSettings_set_TargetHost_m2829519838 ();
extern "C" void TlsClientSettings_get_Certificates_m2866953556 ();
extern "C" void TlsClientSettings_set_Certificates_m4292277542 ();
extern "C" void TlsClientSettings_get_ClientCertificate_m2674240563 ();
extern "C" void TlsClientSettings_set_ClientCertificate_m630726241 ();
extern "C" void TlsClientSettings_UpdateCertificateRSA_m3222256668 ();
extern "C" void TlsException__ctor_m2548592610 ();
extern "C" void TlsException__ctor_m3516023461 ();
extern "C" void TlsException__ctor_m2453498583 ();
extern "C" void TlsException__ctor_m3578249047 ();
extern "C" void TlsException__ctor_m816616279 ();
extern "C" void TlsException__ctor_m4040306894 ();
extern "C" void TlsException_get_Alert_m2905729638 ();
extern "C" void TlsServerSettings__ctor_m1409413246 ();
extern "C" void TlsServerSettings_get_ServerKeyExchange_m4233741566 ();
extern "C" void TlsServerSettings_set_ServerKeyExchange_m1498303633 ();
extern "C" void TlsServerSettings_get_Certificates_m2571231720 ();
extern "C" void TlsServerSettings_set_Certificates_m1376349003 ();
extern "C" void TlsServerSettings_get_CertificateRSA_m3501361569 ();
extern "C" void TlsServerSettings_get_RsaParameters_m493035931 ();
extern "C" void TlsServerSettings_set_RsaParameters_m1718138909 ();
extern "C" void TlsServerSettings_set_SignedParams_m2240200951 ();
extern "C" void TlsServerSettings_get_CertificateRequest_m3263854561 ();
extern "C" void TlsServerSettings_set_CertificateRequest_m242460290 ();
extern "C" void TlsServerSettings_set_CertificateTypes_m1029670707 ();
extern "C" void TlsServerSettings_set_DistinguisedNames_m2513131932 ();
extern "C" void TlsServerSettings_UpdateCertificateRSA_m2005502988 ();
extern "C" void TlsStream__ctor_m2122154764 ();
extern "C" void TlsStream__ctor_m3106919526 ();
extern "C" void TlsStream_get_EOF_m3852778119 ();
extern "C" void TlsStream_get_CanWrite_m623673007 ();
extern "C" void TlsStream_get_CanRead_m601653260 ();
extern "C" void TlsStream_get_CanSeek_m2290267505 ();
extern "C" void TlsStream_get_Position_m1465622516 ();
extern "C" void TlsStream_set_Position_m3659917066 ();
extern "C" void TlsStream_get_Length_m2803130091 ();
extern "C" void TlsStream_ReadSmallValue_m3313813631 ();
extern "C" void TlsStream_ReadByte_m2853286986 ();
extern "C" void TlsStream_ReadInt16_m366065448 ();
extern "C" void TlsStream_ReadInt24_m3078897848 ();
extern "C" void TlsStream_ReadBytes_m1348995521 ();
extern "C" void TlsStream_Write_m3441769662 ();
extern "C" void TlsStream_Write_m2614401193 ();
extern "C" void TlsStream_WriteInt24_m509623838 ();
extern "C" void TlsStream_Write_m2647248461 ();
extern "C" void TlsStream_Write_m4019989511 ();
extern "C" void TlsStream_Reset_m678163232 ();
extern "C" void TlsStream_ToArray_m726083748 ();
extern "C" void TlsStream_Flush_m878954085 ();
extern "C" void TlsStream_SetLength_m1352953404 ();
extern "C" void TlsStream_Seek_m890504009 ();
extern "C" void TlsStream_Read_m2651268204 ();
extern "C" void TlsStream_Write_m4039524078 ();
extern "C" void ValidationResult_get_Trusted_m4246094877 ();
extern "C" void ValidationResult_get_ErrorCode_m1272221167 ();
extern "C" void AuthorityKeyIdentifierExtension__ctor_m2051298225 ();
extern "C" void AuthorityKeyIdentifierExtension_Decode_m2869485377 ();
extern "C" void AuthorityKeyIdentifierExtension_get_Identifier_m2434240800 ();
extern "C" void AuthorityKeyIdentifierExtension_ToString_m3811271106 ();
extern "C" void BasicConstraintsExtension__ctor_m2389333248 ();
extern "C" void BasicConstraintsExtension_Decode_m2057522670 ();
extern "C" void BasicConstraintsExtension_Encode_m2144959798 ();
extern "C" void BasicConstraintsExtension_get_CertificateAuthority_m2894004918 ();
extern "C" void BasicConstraintsExtension_ToString_m2748478730 ();
extern "C" void ExtendedKeyUsageExtension__ctor_m214837540 ();
extern "C" void ExtendedKeyUsageExtension_Decode_m3459786271 ();
extern "C" void ExtendedKeyUsageExtension_Encode_m774491648 ();
extern "C" void ExtendedKeyUsageExtension_get_KeyPurpose_m3904597834 ();
extern "C" void ExtendedKeyUsageExtension_ToString_m1490008558 ();
extern "C" void GeneralNames__ctor_m3190992281 ();
extern "C" void GeneralNames_get_DNSNames_m4093883004 ();
extern "C" void GeneralNames_get_IPAddresses_m2015024776 ();
extern "C" void GeneralNames_ToString_m382700226 ();
extern "C" void KeyUsageExtension__ctor_m2315851155 ();
extern "C" void KeyUsageExtension_Decode_m1806365563 ();
extern "C" void KeyUsageExtension_Encode_m2131290841 ();
extern "C" void KeyUsageExtension_Support_m3177031055 ();
extern "C" void KeyUsageExtension_ToString_m2625433363 ();
extern "C" void NetscapeCertTypeExtension__ctor_m1010860638 ();
extern "C" void NetscapeCertTypeExtension_Decode_m1747060714 ();
extern "C" void NetscapeCertTypeExtension_Support_m1919110708 ();
extern "C" void NetscapeCertTypeExtension_ToString_m698340900 ();
extern "C" void SubjectAltNameExtension__ctor_m3788410065 ();
extern "C" void SubjectAltNameExtension_Decode_m3115455640 ();
extern "C" void SubjectAltNameExtension_get_DNSNames_m407924658 ();
extern "C" void SubjectAltNameExtension_get_IPAddresses_m2158173565 ();
extern "C" void SubjectAltNameExtension_ToString_m1951517739 ();
extern "C" void PKCS12__ctor_m3686132698 ();
extern "C" void PKCS12__ctor_m420191098 ();
extern "C" void PKCS12__ctor_m3099905214 ();
extern "C" void PKCS12__cctor_m1298974184 ();
extern "C" void PKCS12_Decode_m2613582753 ();
extern "C" void PKCS12_Finalize_m1840705829 ();
extern "C" void PKCS12_set_Password_m3068396088 ();
extern "C" void PKCS12_get_IterationCount_m696607354 ();
extern "C" void PKCS12_set_IterationCount_m3674735921 ();
extern "C" void PKCS12_get_Keys_m511823029 ();
extern "C" void PKCS12_get_Certificates_m1466186602 ();
extern "C" void PKCS12_get_RNG_m1185158513 ();
extern "C" void PKCS12_Compare_m3287655509 ();
extern "C" void PKCS12_GetSymmetricAlgorithm_m25664652 ();
extern "C" void PKCS12_Decrypt_m2042746515 ();
extern "C" void PKCS12_Decrypt_m2043507842 ();
extern "C" void PKCS12_Encrypt_m2897433779 ();
extern "C" void PKCS12_GetExistingParameters_m2714337600 ();
extern "C" void PKCS12_AddPrivateKey_m1337147522 ();
extern "C" void PKCS12_ReadSafeBag_m1652738387 ();
extern "C" void PKCS12_CertificateSafeBag_m2063371683 ();
extern "C" void PKCS12_MAC_m2550261151 ();
extern "C" void PKCS12_GetBytes_m1862498661 ();
extern "C" void PKCS12_EncryptedContentInfo_m2113702247 ();
extern "C" void PKCS12_AddCertificate_m3243108831 ();
extern "C" void PKCS12_AddCertificate_m2221299882 ();
extern "C" void PKCS12_RemoveCertificate_m1410858512 ();
extern "C" void PKCS12_RemoveCertificate_m1604396208 ();
extern "C" void PKCS12_Clone_m1751747700 ();
extern "C" void PKCS12_get_MaximumPasswordLength_m532221644 ();
extern "C" void DeriveBytes__ctor_m3565159824 ();
extern "C" void DeriveBytes__cctor_m2393183491 ();
extern "C" void DeriveBytes_set_HashName_m48048765 ();
extern "C" void DeriveBytes_set_IterationCount_m70236908 ();
extern "C" void DeriveBytes_set_Password_m873301047 ();
extern "C" void DeriveBytes_set_Salt_m3037661865 ();
extern "C" void DeriveBytes_Adjust_m494128802 ();
extern "C" void DeriveBytes_Derive_m2548620875 ();
extern "C" void DeriveBytes_DeriveKey_m1007628320 ();
extern "C" void DeriveBytes_DeriveIV_m2578171456 ();
extern "C" void DeriveBytes_DeriveMAC_m836988865 ();
extern "C" void SafeBag__ctor_m2723828098 ();
extern "C" void SafeBag_get_BagOID_m276201203 ();
extern "C" void SafeBag_get_ASN1_m37969328 ();
extern "C" void X501__cctor_m1602829784 ();
extern "C" void X501_ToString_m773130296 ();
extern "C" void X501_ToString_m1796048840 ();
extern "C" void X501_AppendEntry_m3502912398 ();
extern "C" void X509Certificate__ctor_m3475459813 ();
extern "C" void X509Certificate__cctor_m1992081660 ();
extern "C" void X509Certificate_Parse_m4105209092 ();
extern "C" void X509Certificate_GetUnsignedBigInteger_m3662079033 ();
extern "C" void X509Certificate_get_DSA_m2596778069 ();
extern "C" void X509Certificate_set_DSA_m1138717006 ();
extern "C" void X509Certificate_get_Extensions_m1318191537 ();
extern "C" void X509Certificate_get_Hash_m278017058 ();
extern "C" void X509Certificate_get_IssuerName_m321432801 ();
extern "C" void X509Certificate_get_KeyAlgorithm_m2502051430 ();
extern "C" void X509Certificate_get_KeyAlgorithmParameters_m1306763353 ();
extern "C" void X509Certificate_set_KeyAlgorithmParameters_m2776139813 ();
extern "C" void X509Certificate_get_PublicKey_m3958210525 ();
extern "C" void X509Certificate_get_RSA_m3263620316 ();
extern "C" void X509Certificate_set_RSA_m1338906205 ();
extern "C" void X509Certificate_get_RawData_m3157491510 ();
extern "C" void X509Certificate_get_SerialNumber_m4227883404 ();
extern "C" void X509Certificate_get_Signature_m3247838418 ();
extern "C" void X509Certificate_get_SignatureAlgorithm_m2212960349 ();
extern "C" void X509Certificate_get_SubjectName_m2778083020 ();
extern "C" void X509Certificate_get_ValidFrom_m3399788748 ();
extern "C" void X509Certificate_get_ValidUntil_m3279023844 ();
extern "C" void X509Certificate_get_Version_m1547908015 ();
extern "C" void X509Certificate_get_IsCurrent_m3928620590 ();
extern "C" void X509Certificate_WasCurrent_m1989659195 ();
extern "C" void X509Certificate_VerifySignature_m4177921230 ();
extern "C" void X509Certificate_VerifySignature_m4183940143 ();
extern "C" void X509Certificate_VerifySignature_m2438360216 ();
extern "C" void X509Certificate_get_IsSelfSigned_m3372771984 ();
extern "C" void X509Certificate_GetIssuerName_m2937907079 ();
extern "C" void X509Certificate_GetSubjectName_m1513474468 ();
extern "C" void X509Certificate_PEM_m2313601497 ();
extern "C" void X509CertificateCollection__ctor_m1906954588 ();
extern "C" void X509CertificateCollection__ctor_m3725048025 ();
extern "C" void X509CertificateCollection_System_Collections_IEnumerable_GetEnumerator_m817417299 ();
extern "C" void X509CertificateCollection_get_Item_m3600069847 ();
extern "C" void X509CertificateCollection_Add_m1771938890 ();
extern "C" void X509CertificateCollection_AddRange_m3317735245 ();
extern "C" void X509CertificateCollection_Contains_m2732358455 ();
extern "C" void X509CertificateCollection_GetEnumerator_m501201006 ();
extern "C" void X509CertificateCollection_GetHashCode_m4070171117 ();
extern "C" void X509CertificateCollection_IndexOf_m3090707866 ();
extern "C" void X509CertificateCollection_Remove_m647567924 ();
extern "C" void X509CertificateCollection_Compare_m3213233510 ();
extern "C" void X509CertificateEnumerator__ctor_m1154100625 ();
extern "C" void X509CertificateEnumerator_System_Collections_IEnumerator_get_Current_m18197518 ();
extern "C" void X509CertificateEnumerator_System_Collections_IEnumerator_MoveNext_m2020102016 ();
extern "C" void X509CertificateEnumerator_System_Collections_IEnumerator_Reset_m116759774 ();
extern "C" void X509CertificateEnumerator_get_Current_m1453228696 ();
extern "C" void X509CertificateEnumerator_MoveNext_m2755272211 ();
extern "C" void X509CertificateEnumerator_Reset_m213133671 ();
extern "C" void X509Chain__ctor_m760047449 ();
extern "C" void X509Chain__ctor_m765311802 ();
extern "C" void X509Chain_get_Status_m1421504428 ();
extern "C" void X509Chain_get_TrustAnchors_m1309469929 ();
extern "C" void X509Chain_Build_m3136661355 ();
extern "C" void X509Chain_IsValid_m3583476780 ();
extern "C" void X509Chain_FindCertificateParent_m3719552933 ();
extern "C" void X509Chain_FindCertificateRoot_m3075924856 ();
extern "C" void X509Chain_IsTrusted_m1534983548 ();
extern "C" void X509Chain_IsParent_m3602540272 ();
extern "C" void X509Crl__ctor_m1587320464 ();
extern "C" void X509Crl_Parse_m2363685775 ();
extern "C" void X509Crl_get_Extensions_m2411577858 ();
extern "C" void X509Crl_get_Hash_m4087585872 ();
extern "C" void X509Crl_get_IssuerName_m788459859 ();
extern "C" void X509Crl_get_NextUpdate_m3052840772 ();
extern "C" void X509Crl_Compare_m161819802 ();
extern "C" void X509Crl_GetCrlEntry_m3776945200 ();
extern "C" void X509Crl_GetCrlEntry_m960893474 ();
extern "C" void X509Crl_GetHashName_m871377626 ();
extern "C" void X509Crl_VerifySignature_m318007533 ();
extern "C" void X509Crl_VerifySignature_m1498544153 ();
extern "C" void X509Crl_VerifySignature_m1270657239 ();
extern "C" void X509CrlEntry__ctor_m2223032288 ();
extern "C" void X509CrlEntry_get_SerialNumber_m1374642971 ();
extern "C" void X509CrlEntry_get_RevocationDate_m620227204 ();
extern "C" void X509CrlEntry_get_Extensions_m2956389379 ();
extern "C" void X509Extension__ctor_m555060717 ();
extern "C" void X509Extension__ctor_m110854038 ();
extern "C" void X509Extension_Decode_m2854984614 ();
extern "C" void X509Extension_Encode_m1708484574 ();
extern "C" void X509Extension_get_Oid_m3805899428 ();
extern "C" void X509Extension_get_Critical_m1973472787 ();
extern "C" void X509Extension_get_Value_m708908506 ();
extern "C" void X509Extension_Equals_m1966452623 ();
extern "C" void X509Extension_GetHashCode_m1676727038 ();
extern "C" void X509Extension_WriteLine_m881048957 ();
extern "C" void X509Extension_ToString_m3680485954 ();
extern "C" void X509ExtensionCollection__ctor_m2525499396 ();
extern "C" void X509ExtensionCollection__ctor_m385246626 ();
extern "C" void X509ExtensionCollection_System_Collections_IEnumerable_GetEnumerator_m200825157 ();
extern "C" void X509ExtensionCollection_IndexOf_m1736569985 ();
extern "C" void X509ExtensionCollection_get_Item_m2725489036 ();
extern "C" void X509Store__ctor_m1760505199 ();
extern "C" void X509Store_get_Certificates_m4140352539 ();
extern "C" void X509Store_get_Crls_m420681093 ();
extern "C" void X509Store_Load_m1402394016 ();
extern "C" void X509Store_LoadCertificate_m2669958390 ();
extern "C" void X509Store_LoadCrl_m2935225626 ();
extern "C" void X509Store_CheckStore_m2202048695 ();
extern "C" void X509Store_BuildCertificatesCollection_m1314134402 ();
extern "C" void X509Store_BuildCrlsCollection_m1438245430 ();
extern "C" void X509StoreManager_get_CurrentUser_m1265783936 ();
extern "C" void X509StoreManager_get_LocalMachine_m3731158609 ();
extern "C" void X509StoreManager_get_TrustedRootCertificates_m2693658780 ();
extern "C" void X509Stores__ctor_m3803881044 ();
extern "C" void X509Stores_get_TrustedRoot_m1991417710 ();
extern "C" void X509Stores_Open_m3585119639 ();
extern "C" void Locale_GetText_m1492548562 ();
extern "C" void Locale_GetText_m2618335777 ();
extern "C" void KeyBuilder_get_Rng_m1402732562 ();
extern "C" void KeyBuilder_Key_m2068773312 ();
extern "C" void KeyBuilder_IV_m1539373917 ();
extern "C" void SymmetricTransform__ctor_m2345069913 ();
extern "C" void SymmetricTransform_System_IDisposable_Dispose_m53307466 ();
extern "C" void SymmetricTransform_Finalize_m135047551 ();
extern "C" void SymmetricTransform_Dispose_m2605546990 ();
extern "C" void SymmetricTransform_get_CanReuseTransform_m3415952586 ();
extern "C" void SymmetricTransform_Transform_m2512799522 ();
extern "C" void SymmetricTransform_CBC_m2861918276 ();
extern "C" void SymmetricTransform_CFB_m502402308 ();
extern "C" void SymmetricTransform_OFB_m610835733 ();
extern "C" void SymmetricTransform_CTS_m2602812767 ();
extern "C" void SymmetricTransform_CheckInput_m2580534911 ();
extern "C" void SymmetricTransform_TransformBlock_m3335701745 ();
extern "C" void SymmetricTransform_get_KeepLastBlock_m1391419215 ();
extern "C" void SymmetricTransform_InternalTransformBlock_m713902379 ();
extern "C" void SymmetricTransform_Random_m2211116558 ();
extern "C" void SymmetricTransform_ThrowBadPaddingException_m59444602 ();
extern "C" void SymmetricTransform_FinalEncrypt_m1824314829 ();
extern "C" void SymmetricTransform_FinalDecrypt_m318329064 ();
extern "C" void SymmetricTransform_TransformFinalBlock_m3934660993 ();
extern "C" void Check_Source_m2478120104 ();
extern "C" void Check_SourceAndPredicate_m477734566 ();
extern "C" void ExtensionAttribute__ctor_m831032830 ();
extern "C" void Aes__ctor_m2077794217 ();
extern "C" void AesManaged__ctor_m2374737792 ();
extern "C" void AesManaged_GenerateIV_m3285618721 ();
extern "C" void AesManaged_GenerateKey_m3391128746 ();
extern "C" void AesManaged_CreateDecryptor_m169246569 ();
extern "C" void AesManaged_CreateEncryptor_m857427945 ();
extern "C" void AesManaged_get_IV_m2426183383 ();
extern "C" void AesManaged_set_IV_m4034845936 ();
extern "C" void AesManaged_get_Key_m578016737 ();
extern "C" void AesManaged_set_Key_m2435629111 ();
extern "C" void AesManaged_get_KeySize_m730303170 ();
extern "C" void AesManaged_set_KeySize_m2370352321 ();
extern "C" void AesManaged_CreateDecryptor_m3604052453 ();
extern "C" void AesManaged_CreateEncryptor_m615540925 ();
extern "C" void AesManaged_Dispose_m1169876397 ();
extern "C" void AesTransform__ctor_m2896269147 ();
extern "C" void AesTransform__cctor_m1118640786 ();
extern "C" void AesTransform_ECB_m663232370 ();
extern "C" void AesTransform_SubByte_m787919468 ();
extern "C" void AesTransform_Encrypt128_m745777362 ();
extern "C" void AesTransform_Decrypt128_m2221214126 ();
extern "C" void AddComponentMenu__ctor_m3629709883 ();
extern "C" void AnimationCurve__ctor_m3076316537 ();
extern "C" void AnimationCurve__ctor_m566668622 ();
extern "C" void AnimationCurve_Cleanup_m243787505 ();
extern "C" void AnimationCurve_Finalize_m4265665064 ();
extern "C" void AnimationCurve_Init_m1956642396 ();
extern "C" void Application_CallLowMemory_m1626726542 ();
extern "C" void Application_CallLogCallback_m3808806632 ();
extern "C" void Application_InvokeOnBeforeRender_m818804610 ();
extern "C" void LogCallback__ctor_m2513058278 ();
extern "C" void LogCallback_Invoke_m2853261436 ();
extern "C" void LogCallback_BeginInvoke_m1314223040 ();
extern "C" void LogCallback_EndInvoke_m3721814140 ();
extern "C" void LowMemoryCallback__ctor_m4170563135 ();
extern "C" void LowMemoryCallback_Invoke_m3732620697 ();
extern "C" void LowMemoryCallback_BeginInvoke_m3629193632 ();
extern "C" void LowMemoryCallback_EndInvoke_m80814921 ();
extern "C" void AsyncOperation_InternalDestroy_m1747224711 ();
extern "C" void AsyncOperation_Finalize_m1200536139 ();
extern "C" void AsyncOperation_InvokeCompletionEvent_m737953261 ();
extern "C" void AttributeHelperEngine_GetParentTypeDisallowingMultipleInclusion_m1021838127 ();
extern "C" void AttributeHelperEngine_GetRequiredComponents_m2091293497 ();
extern "C" void AttributeHelperEngine_CheckIsEditorScript_m2212222995 ();
extern "C" void AttributeHelperEngine_GetDefaultExecutionOrderFor_m1362990272 ();
extern "C" void AttributeHelperEngine__cctor_m3752452713 ();
extern "C" void Behaviour__ctor_m1233868026 ();
extern "C" void Behaviour_get_enabled_m1304335371 ();
extern "C" void UnmarshalledAttribute__ctor_m3412418881 ();
extern "C" void Camera_get_nearClipPlane_m167155657 ();
extern "C" void Camera_get_farClipPlane_m1366251422 ();
extern "C" void Camera_get_cullingMask_m2771596515 ();
extern "C" void Camera_get_eventMask_m3958975071 ();
extern "C" void Camera_get_pixelRect_m3857366896 ();
extern "C" void Camera_INTERNAL_get_pixelRect_m3102036321 ();
extern "C" void Camera_get_targetTexture_m903246907 ();
extern "C" void Camera_get_clearFlags_m3577200564 ();
extern "C" void Camera_ScreenPointToRay_m2744754050 ();
extern "C" void Camera_INTERNAL_CALL_ScreenPointToRay_m244366202 ();
extern "C" void Camera_get_allCamerasCount_m3527489092 ();
extern "C" void Camera_GetAllCameras_m557431570 ();
extern "C" void Camera_FireOnPreCull_m2186829015 ();
extern "C" void Camera_FireOnPreRender_m1563217163 ();
extern "C" void Camera_FireOnPostRender_m4290449375 ();
extern "C" void Camera_RaycastTry_m208704706 ();
extern "C" void Camera_INTERNAL_CALL_RaycastTry_m2903709223 ();
extern "C" void Camera_RaycastTry2D_m3780933465 ();
extern "C" void Camera_INTERNAL_CALL_RaycastTry2D_m700194433 ();
extern "C" void CameraCallback__ctor_m1713115355 ();
extern "C" void CameraCallback_Invoke_m144364913 ();
extern "C" void CameraCallback_BeginInvoke_m4085641401 ();
extern "C" void CameraCallback_EndInvoke_m2093829129 ();
extern "C" void ClassLibraryInitializer_Init_m4274939016 ();
extern "C" void Color__ctor_m211164015_AdjustorThunk ();
extern "C" void Color_ToString_m4292820139_AdjustorThunk ();
extern "C" void Color_GetHashCode_m3278237850_AdjustorThunk ();
extern "C" void Color_Equals_m3350585184_AdjustorThunk ();
extern "C" void Color_get_white_m3869699080 ();
extern "C" void Color_op_Implicit_m4266979643 ();
extern "C" void Component__ctor_m2592563656 ();
extern "C" void Component_get_gameObject_m2710707649 ();
extern "C" void Component_GetComponentFastPath_m3479205869 ();
extern "C" void Coroutine_ReleaseCoroutine_m329501834 ();
extern "C" void Coroutine_Finalize_m1627900536 ();
extern "C" void CSSMeasureFunc__ctor_m324136384 ();
extern "C" void CSSMeasureFunc_Invoke_m2998463313 ();
extern "C" void CSSMeasureFunc_BeginInvoke_m3083536890 ();
extern "C" void CSSMeasureFunc_EndInvoke_m231879325 ();
extern "C" void Native_CSSNodeGetMeasureFunc_m4025476427 ();
extern "C" void Native_CSSNodeMeasureInvoke_m1142609737 ();
extern "C" void Native__cctor_m2631422355 ();
extern "C" void CullingGroup_Finalize_m3186244642 ();
extern "C" void CullingGroup_Dispose_m1436928443 ();
extern "C" void CullingGroup_SendEvents_m3781512400 ();
extern "C" void CullingGroup_FinalizerFailure_m3512716862 ();
extern "C" void StateChanged__ctor_m58376528 ();
extern "C" void StateChanged_Invoke_m1291922880 ();
extern "C" void StateChanged_BeginInvoke_m1155489289 ();
extern "C" void StateChanged_EndInvoke_m2092849007 ();
extern "C" void Debug_get_unityLogger_m2700972930 ();
extern "C" void Debug_Log_m2305178311 ();
extern "C" void Debug_LogError_m1070074371 ();
extern "C" void Debug_LogException_m57812199 ();
extern "C" void Debug__cctor_m1871084662 ();
extern "C" void DebugLogHandler__ctor_m2673380417 ();
extern "C" void DebugLogHandler_Internal_Log_m1682417589 ();
extern "C" void DebugLogHandler_Internal_LogException_m3264502143 ();
extern "C" void DebugLogHandler_LogFormat_m3907003566 ();
extern "C" void DebugLogHandler_LogException_m611967214 ();
extern "C" void DefaultExecutionOrder_get_order_m3989798846 ();
extern "C" void Display__ctor_m1199150772 ();
extern "C" void Display__ctor_m906575725 ();
extern "C" void Display_RecreateDisplayList_m3512407007 ();
extern "C" void Display_FireDisplaysUpdated_m3194723814 ();
extern "C" void Display__cctor_m3585651147 ();
extern "C" void DisplaysUpdatedDelegate__ctor_m1345164380 ();
extern "C" void DisplaysUpdatedDelegate_Invoke_m1559204117 ();
extern "C" void DisplaysUpdatedDelegate_BeginInvoke_m3281943358 ();
extern "C" void DisplaysUpdatedDelegate_EndInvoke_m3619400991 ();
extern "C" void ArgumentCache__ctor_m2562784622 ();
extern "C" void ArgumentCache_get_unityObjectArgument_m2559081415 ();
extern "C" void ArgumentCache_get_unityObjectArgumentAssemblyTypeName_m3489746291 ();
extern "C" void ArgumentCache_get_intArgument_m1266469321 ();
extern "C" void ArgumentCache_get_floatArgument_m703828235 ();
extern "C" void ArgumentCache_get_stringArgument_m574520374 ();
extern "C" void ArgumentCache_get_boolArgument_m600683273 ();
extern "C" void ArgumentCache_TidyAssemblyTypeName_m2531759392 ();
extern "C" void ArgumentCache_OnBeforeSerialize_m2612988137 ();
extern "C" void ArgumentCache_OnAfterDeserialize_m4282301992 ();
extern "C" void BaseInvokableCall__ctor_m3419792134 ();
extern "C" void BaseInvokableCall_AllowInvoke_m1751538175 ();
extern "C" void InvokableCall__ctor_m2974797033 ();
extern "C" void InvokableCall_add_Delegate_m2772852427 ();
extern "C" void InvokableCall_remove_Delegate_m599836031 ();
extern "C" void InvokableCall_Invoke_m1952365720 ();
extern "C" void InvokableCallList__ctor_m386930243 ();
extern "C" void InvokableCallList_AddPersistentInvokableCall_m3306354928 ();
extern "C" void InvokableCallList_ClearPersistent_m1797712781 ();
extern "C" void InvokableCallList_PrepareInvoke_m2613780459 ();
extern "C" void PersistentCall__ctor_m2649643936 ();
extern "C" void PersistentCall_get_target_m4082064461 ();
extern "C" void PersistentCall_get_methodName_m2687182531 ();
extern "C" void PersistentCall_get_mode_m2357383679 ();
extern "C" void PersistentCall_get_arguments_m1844117721 ();
extern "C" void PersistentCall_IsValid_m2172430097 ();
extern "C" void PersistentCall_GetRuntimeCall_m1057898321 ();
extern "C" void PersistentCall_GetObjectCall_m2893916170 ();
extern "C" void PersistentCallGroup__ctor_m2062212555 ();
extern "C" void PersistentCallGroup_Initialize_m644878480 ();
extern "C" void UnityAction__ctor_m1074277545 ();
extern "C" void UnityAction_Invoke_m2515763748 ();
extern "C" void UnityAction_BeginInvoke_m3164501995 ();
extern "C" void UnityAction_EndInvoke_m650613571 ();
extern "C" void UnityEvent__ctor_m811779962 ();
extern "C" void UnityEvent_FindMethod_Impl_m3636630720 ();
extern "C" void UnityEvent_GetDelegate_m452312904 ();
extern "C" void UnityEventBase__ctor_m1419821417 ();
extern "C" void UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m303227655 ();
extern "C" void UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m1452364050 ();
extern "C" void UnityEventBase_FindMethod_m595753448 ();
extern "C" void UnityEventBase_FindMethod_m3931843611 ();
extern "C" void UnityEventBase_DirtyPersistentCalls_m1507352076 ();
extern "C" void UnityEventBase_RebuildPersistentCallsIfNeeded_m256317675 ();
extern "C" void UnityEventBase_PrepareInvoke_m3974578209 ();
extern "C" void UnityEventBase_ToString_m2520642732 ();
extern "C" void UnityEventBase_GetValidMethodInfo_m1004474682 ();
extern "C" void RenderPipelineManager_get_currentPipeline_m3103495424 ();
extern "C" void RenderPipelineManager_set_currentPipeline_m3602131277 ();
extern "C" void RenderPipelineManager_CleanupRenderPipeline_m309528094 ();
extern "C" void RenderPipelineManager_DoRenderLoop_Internal_m124400281 ();
extern "C" void RenderPipelineManager_PrepareRenderPipeline_m896043942 ();
extern "C" void ScriptableRenderContext__ctor_m242682668_AdjustorThunk ();
extern "C" void GameObject__ctor_m717643193 ();
extern "C" void GameObject_SendMessage_m239132133 ();
extern "C" void GameObject_Internal_CreateGameObject_m1823697062 ();
extern "C" void Gradient__ctor_m2009183950 ();
extern "C" void Gradient_Init_m1188969969 ();
extern "C" void Gradient_Cleanup_m2195794132 ();
extern "C" void Gradient_Finalize_m3210182836 ();
extern "C" void GUIElement__ctor_m3581839174 ();
extern "C" void GUILayer_HitTest_m1935830346 ();
extern "C" void GUILayer_INTERNAL_CALL_HitTest_m422396558 ();
extern "C" void Input_GetMouseButton_m294091231 ();
extern "C" void Input_GetMouseButtonDown_m3867833682 ();
extern "C" void Input_get_mousePosition_m3311395491 ();
extern "C" void Input_INTERNAL_get_mousePosition_m3613716531 ();
extern "C" void Input__cctor_m865649194 ();
extern "C" void DefaultValueAttribute__ctor_m2061771615 ();
extern "C" void DefaultValueAttribute_get_Value_m1203122007 ();
extern "C" void DefaultValueAttribute_Equals_m3424877481 ();
extern "C" void DefaultValueAttribute_GetHashCode_m2657444706 ();
extern "C" void LocalNotification_Destroy_m1982161855 ();
extern "C" void LocalNotification_Finalize_m874233874 ();
extern "C" void LocalNotification__cctor_m140754005 ();
extern "C" void RemoteNotification_Destroy_m4186758914 ();
extern "C" void RemoteNotification_Finalize_m2240751185 ();
extern "C" void Logger__ctor_m4234190541 ();
extern "C" void Logger_get_logHandler_m1719487699 ();
extern "C" void Logger_set_logHandler_m6066634 ();
extern "C" void Logger_get_logEnabled_m2290808316 ();
extern "C" void Logger_set_logEnabled_m1662937540 ();
extern "C" void Logger_get_filterLogType_m426955796 ();
extern "C" void Logger_set_filterLogType_m2745985065 ();
extern "C" void Logger_IsLogTypeAllowed_m1817789334 ();
extern "C" void Logger_GetString_m2492438982 ();
extern "C" void Logger_Log_m2914072330 ();
extern "C" void Logger_LogFormat_m1581286573 ();
extern "C" void Logger_LogException_m1366382550 ();
extern "C" void ManagedStreamHelpers_ValidateLoadFromStream_m2537091486 ();
extern "C" void ManagedStreamHelpers_ManagedStreamRead_m4024070239 ();
extern "C" void ManagedStreamHelpers_ManagedStreamSeek_m504681536 ();
extern "C" void ManagedStreamHelpers_ManagedStreamLength_m1632106524 ();
extern "C" void Mathf_Abs_m2765530332 ();
extern "C" void Mathf_Max_m3084430723 ();
extern "C" void Mathf_Approximately_m1455183363 ();
extern "C" void Mathf__cctor_m3961786392 ();
extern "C" void Matrix4x4__ctor_m614016869_AdjustorThunk ();
extern "C" void Matrix4x4_GetHashCode_m2086764285_AdjustorThunk ();
extern "C" void Matrix4x4_Equals_m1131202087_AdjustorThunk ();
extern "C" void Matrix4x4_GetColumn_m1315812967_AdjustorThunk ();
extern "C" void Matrix4x4_get_identity_m3045349954 ();
extern "C" void Matrix4x4_ToString_m861927258_AdjustorThunk ();
extern "C" void Matrix4x4__cctor_m770025791 ();
extern "C" void MonoBehaviour__ctor_m3125940511 ();
extern "C" void NativeClassAttribute__ctor_m714604756 ();
extern "C" void NativeClassAttribute_set_QualifiedNativeName_m875392448 ();
extern "C" void MessageEventArgs__ctor_m1689534235 ();
extern "C" void PlayerConnection__ctor_m527712293 ();
extern "C" void PlayerConnection_get_instance_m2190863265 ();
extern "C" void PlayerConnection_CreateInstance_m4274468596 ();
extern "C" void PlayerConnection_MessageCallbackInternal_m469132778 ();
extern "C" void PlayerConnection_ConnectedCallbackInternal_m1135070508 ();
extern "C" void PlayerConnection_DisconnectedCallback_m2631702132 ();
extern "C" void PlayerEditorConnectionEvents__ctor_m1455086094 ();
extern "C" void PlayerEditorConnectionEvents_InvokeMessageIdSubscribers_m2032614817 ();
extern "C" void U3CInvokeMessageIdSubscribersU3Ec__AnonStorey0__ctor_m1102967907 ();
extern "C" void U3CInvokeMessageIdSubscribersU3Ec__AnonStorey0_U3CU3Em__0_m974971376 ();
extern "C" void ConnectionChangeEvent__ctor_m1785426546 ();
extern "C" void MessageEvent__ctor_m3751876958 ();
extern "C" void MessageTypeSubscribers__ctor_m2303372877 ();
extern "C" void MessageTypeSubscribers_get_MessageTypeId_m2156269021 ();
extern "C" void Object__ctor_m387379410 ();
extern "C" void Object_set_hideFlags_m3010783544 ();
extern "C" void Object_ToString_m842583452 ();
extern "C" void Object_GetHashCode_m3084030858 ();
extern "C" void Object_Equals_m4058157391 ();
extern "C" void Object_op_Implicit_m3975891434 ();
extern "C" void Object_CompareBaseObjects_m3289384931 ();
extern "C" void Object_IsNativeObjectAlive_m2916611270 ();
extern "C" void Object_GetCachedPtr_m2018497938 ();
extern "C" void Object_op_Equality_m2743582905 ();
extern "C" void Object_op_Inequality_m3819358377 ();
extern "C" void Object__cctor_m3598847078 ();
extern "C" void Playable__ctor_m3942516336_AdjustorThunk ();
extern "C" void Playable_get_Null_m1034340665 ();
extern "C" void Playable_GetHandle_m2973432975_AdjustorThunk ();
extern "C" void Playable_Equals_m1196118830_AdjustorThunk ();
extern "C" void Playable__cctor_m2479433417 ();
extern "C" void PlayableAsset__ctor_m1918338898 ();
extern "C" void PlayableAsset_get_duration_m2883676437 ();
extern "C" void PlayableAsset_Internal_CreatePlayable_m4149673157 ();
extern "C" void PlayableAsset_Internal_GetPlayableAssetDuration_m3126328874 ();
extern "C" void PlayableBehaviour__ctor_m1062833712 ();
extern "C" void PlayableBehaviour_Clone_m1846106316 ();
extern "C" void PlayableBinding__cctor_m1831276805 ();
extern "C" void PlayableHandle_get_Null_m2516265857 ();
extern "C" void PlayableHandle_op_Equality_m2397680455 ();
extern "C" void PlayableHandle_Equals_m771590314_AdjustorThunk ();
extern "C" void PlayableHandle_GetHashCode_m1922874376_AdjustorThunk ();
extern "C" void PlayableHandle_CompareVersion_m2720829020 ();
extern "C" void PlayableOutput__ctor_m451176029_AdjustorThunk ();
extern "C" void PlayableOutput_GetHandle_m1350475571_AdjustorThunk ();
extern "C" void PlayableOutput_Equals_m2076999710_AdjustorThunk ();
extern "C" void PlayableOutput__cctor_m43115862 ();
extern "C" void PlayableOutputHandle_get_Null_m3044568124 ();
extern "C" void PlayableOutputHandle_GetHashCode_m4127769214_AdjustorThunk ();
extern "C" void PlayableOutputHandle_op_Equality_m1435240927 ();
extern "C" void PlayableOutputHandle_Equals_m669007655_AdjustorThunk ();
extern "C" void PlayableOutputHandle_CompareVersion_m3459929500 ();
extern "C" void PropertyName__ctor_m2196050290_AdjustorThunk ();
extern "C" void PropertyName__ctor_m1851822729_AdjustorThunk ();
extern "C" void PropertyName__ctor_m4077360820_AdjustorThunk ();
extern "C" void PropertyName_op_Equality_m2654803850 ();
extern "C" void PropertyName_op_Inequality_m3678544633 ();
extern "C" void PropertyName_GetHashCode_m1226794333_AdjustorThunk ();
extern "C" void PropertyName_Equals_m3895751744_AdjustorThunk ();
extern "C" void PropertyName_op_Implicit_m2033544225 ();
extern "C" void PropertyName_op_Implicit_m3783576796 ();
extern "C" void PropertyName_ToString_m1969781279_AdjustorThunk ();
extern "C" void PropertyNameUtils_PropertyNameFromString_m3539940257 ();
extern "C" void PropertyNameUtils_PropertyNameFromString_Injected_m3547369790 ();
extern "C" void Ray_get_direction_m1714502355_AdjustorThunk ();
extern "C" void Ray_ToString_m3743478486_AdjustorThunk ();
extern "C" void Rect_get_x_m4046749491_AdjustorThunk ();
extern "C" void Rect_get_y_m1845352024_AdjustorThunk ();
extern "C" void Rect_get_width_m710188174_AdjustorThunk ();
extern "C" void Rect_get_height_m262661000_AdjustorThunk ();
extern "C" void Rect_get_xMin_m490539547_AdjustorThunk ();
extern "C" void Rect_get_yMin_m1596955344_AdjustorThunk ();
extern "C" void Rect_get_xMax_m3140092843_AdjustorThunk ();
extern "C" void Rect_get_yMax_m1499979192_AdjustorThunk ();
extern "C" void Rect_Contains_m594593135_AdjustorThunk ();
extern "C" void Rect_GetHashCode_m1190625508_AdjustorThunk ();
extern "C" void Rect_Equals_m2317586560_AdjustorThunk ();
extern "C" void Rect_ToString_m2557162916_AdjustorThunk ();
extern "C" void RectTransform_SendReapplyDrivenProperties_m2257690066 ();
extern "C" void ReapplyDrivenProperties__ctor_m4026315179 ();
extern "C" void ReapplyDrivenProperties_Invoke_m2731383309 ();
extern "C" void ReapplyDrivenProperties_BeginInvoke_m287277383 ();
extern "C" void ReapplyDrivenProperties_EndInvoke_m3904559314 ();
extern "C" void RequireComponent__ctor_m3242490219 ();
extern "C" void Scene_get_handle_m2811252972_AdjustorThunk ();
extern "C" void Scene_GetHashCode_m3840110465_AdjustorThunk ();
extern "C" void Scene_Equals_m1182027264_AdjustorThunk ();
extern "C" void SceneManager_Internal_SceneLoaded_m2309132009 ();
extern "C" void SceneManager_Internal_SceneUnloaded_m2410300733 ();
extern "C" void SceneManager_Internal_ActiveSceneChanged_m667757325 ();
extern "C" void ScriptableObject__ctor_m3589061004 ();
extern "C" void ScriptableObject_Internal_CreateScriptableObject_m3652231296 ();
extern "C" void ScriptableObject_CreateInstance_m1780655698 ();
extern "C" void ScriptableObject_CreateInstanceFromType_m1134094777 ();
extern "C" void GeneratedByOldBindingsGeneratorAttribute__ctor_m1819602998 ();
extern "C" void RequiredByNativeCodeAttribute__ctor_m3493315205 ();
extern "C" void UsedByNativeCodeAttribute__ctor_m4001769068 ();
extern "C" void SendMouseEvents_SetMouseMoved_m2508391903 ();
extern "C" void SendMouseEvents_HitTestLegacyGUI_m3813264185 ();
extern "C" void SendMouseEvents_DoSendMouseEvents_m3686682366 ();
extern "C" void SendMouseEvents_SendEvents_m2210188208 ();
extern "C" void SendMouseEvents__cctor_m1686622767 ();
extern "C" void HitInfo_SendMessage_m2389970774_AdjustorThunk ();
extern "C" void HitInfo_op_Implicit_m1771688566 ();
extern "C" void HitInfo_Compare_m1712556816 ();
extern "C" void FormerlySerializedAsAttribute__ctor_m219264990 ();
extern "C" void SerializeField__ctor_m3401225370 ();
extern "C" void SetupCoroutine_InvokeMoveNext_m1274895524 ();
extern "C" void SetupCoroutine_InvokeMember_m3346816490 ();
extern "C" void StackTraceUtility_SetProjectFolder_m3603953530 ();
extern "C" void StackTraceUtility_ExtractStackTrace_m1291404532 ();
extern "C" void StackTraceUtility_IsSystemStacktraceType_m172104614 ();
extern "C" void StackTraceUtility_ExtractStringFromExceptionInternal_m3699450383 ();
extern "C" void StackTraceUtility_PostprocessStacktrace_m4246116829 ();
extern "C" void StackTraceUtility_ExtractFormattedStackTrace_m382049542 ();
extern "C" void StackTraceUtility__cctor_m1952330627 ();
extern "C" void Texture__ctor_m2053337444 ();
extern "C" void Texture2D__ctor_m975450915 ();
extern "C" void Texture2D_Internal_Create_m1993173660 ();
extern "C" void ThreadAndSerializationSafeAttribute__ctor_m211248632 ();
extern "C" void Transform__ctor_m963699039 ();
extern "C" void Transform_get_childCount_m849482613 ();
extern "C" void Transform_GetEnumerator_m3477035503 ();
extern "C" void Transform_GetChild_m4117681253 ();
extern "C" void Enumerator__ctor_m526857282 ();
extern "C" void Enumerator_get_Current_m3510733711 ();
extern "C" void Enumerator_MoveNext_m52240011 ();
extern "C" void Enumerator_Reset_m2946229432 ();
extern "C" void SpriteAtlasManager_RequestAtlas_m2195264182 ();
extern "C" void SpriteAtlasManager_Register_m1381418004 ();
extern "C" void SpriteAtlasManager__cctor_m4142695825 ();
extern "C" void RequestAtlasCallback__ctor_m2170763641 ();
extern "C" void RequestAtlasCallback_Invoke_m1258427264 ();
extern "C" void RequestAtlasCallback_BeginInvoke_m520960351 ();
extern "C" void RequestAtlasCallback_EndInvoke_m404102494 ();
extern "C" void UnhandledExceptionHandler_RegisterUECatcher_m663990726 ();
extern "C" void UnhandledExceptionHandler_HandleUnhandledException_m1441885697 ();
extern "C" void UnhandledExceptionHandler_PrintException_m2880095263 ();
extern "C" void UnhandledExceptionHandler_NativeUnhandledExceptionHandler_m4249568766 ();
extern "C" void UnityException__ctor_m2243312541 ();
extern "C" void UnityException__ctor_m3073494132 ();
extern "C" void UnityLogWriter__ctor_m958796054 ();
extern "C" void UnityLogWriter_WriteStringToUnityLog_m237899344 ();
extern "C" void UnityLogWriter_Init_m3153067179 ();
extern "C" void UnityLogWriter_Write_m398588393 ();
extern "C" void UnityLogWriter_Write_m4129027096 ();
extern "C" void UnityString_Format_m2513837565 ();
extern "C" void UnitySynchronizationContext__ctor_m1361814383 ();
extern "C" void UnitySynchronizationContext_Exec_m2445278172 ();
extern "C" void UnitySynchronizationContext_InitializeSynchronizationContext_m2662403408 ();
extern "C" void UnitySynchronizationContext_ExecuteTasks_m3600858646 ();
extern "C" void WorkRequest_Invoke_m595488964_AdjustorThunk ();
extern "C" void Vector3__ctor_m800217684_AdjustorThunk ();
extern "C" void Vector3_GetHashCode_m3101213102_AdjustorThunk ();
extern "C" void Vector3_Equals_m2043273264_AdjustorThunk ();
extern "C" void Vector3_ToString_m792339301_AdjustorThunk ();
extern "C" void Vector3__cctor_m1700583052 ();
extern "C" void Vector4__ctor_m3054764420_AdjustorThunk ();
extern "C" void Vector4_GetHashCode_m1136089151_AdjustorThunk ();
extern "C" void Vector4_Equals_m480591454_AdjustorThunk ();
extern "C" void Vector4_ToString_m2058083437_AdjustorThunk ();
extern "C" void Vector4__cctor_m1314409204 ();
extern "C" void WritableAttribute__ctor_m474281766 ();
extern "C" void MathfInternal__cctor_m4032146116 ();
extern "C" void NetFxCoreExtensions_CreateDelegate_m3544893108 ();
extern "C" void AudioClipPlayable_GetHandle_m334032914_AdjustorThunk ();
extern "C" void AudioClipPlayable_Equals_m2753778244_AdjustorThunk ();
extern "C" void AudioMixerPlayable_GetHandle_m15851785_AdjustorThunk ();
extern "C" void AudioMixerPlayable_Equals_m1432794900_AdjustorThunk ();
extern "C" void AudioClip__ctor_m2983411473 ();
extern "C" void AudioClip_get_ambisonic_m2163265490 ();
extern "C" void AudioClip_InvokePCMReaderCallback_Internal_m779593384 ();
extern "C" void AudioClip_InvokePCMSetPositionCallback_Internal_m1609205883 ();
extern "C" void PCMReaderCallback__ctor_m3697100253 ();
extern "C" void PCMReaderCallback_Invoke_m790641721 ();
extern "C" void PCMReaderCallback_BeginInvoke_m2104147499 ();
extern "C" void PCMReaderCallback_EndInvoke_m830652813 ();
extern "C" void PCMSetPositionCallback__ctor_m2841148579 ();
extern "C" void PCMSetPositionCallback_Invoke_m1319562623 ();
extern "C" void PCMSetPositionCallback_BeginInvoke_m3995879997 ();
extern "C" void PCMSetPositionCallback_EndInvoke_m4282839021 ();
extern "C" void AudioExtensionDefinition_GetExtensionType_m1990966740 ();
extern "C" void AudioExtensionManager_GetAudioListener_m3189978351 ();
extern "C" void AudioExtensionManager_AddSpatializerExtension_m2838969561 ();
extern "C" void AudioExtensionManager_AddAmbisonicDecoderExtension_m2920682946 ();
extern "C" void AudioExtensionManager_WriteExtensionProperties_m2859894919 ();
extern "C" void AudioExtensionManager_AddSpatializerExtension_m3723108260 ();
extern "C" void AudioExtensionManager_WriteExtensionProperties_m375337912 ();
extern "C" void AudioExtensionManager_GetListenerSpatializerExtensionType_m1875002759 ();
extern "C" void AudioExtensionManager_GetListenerSpatializerExtensionEditorType_m129125889 ();
extern "C" void AudioExtensionManager_GetSourceSpatializerExtensionType_m2709576922 ();
extern "C" void AudioExtensionManager_AddExtensionToManager_m3314625348 ();
extern "C" void AudioExtensionManager_RemoveExtensionFromManager_m2876808799 ();
extern "C" void AudioExtensionManager_Update_m244409286 ();
extern "C" void AudioExtensionManager_GetReadyToPlay_m855413997 ();
extern "C" void AudioExtensionManager_RegisterBuiltinDefinitions_m2288387409 ();
extern "C" void AudioExtensionManager__cctor_m3951697045 ();
extern "C" void AudioListener_GetNumExtensionProperties_m2279481642 ();
extern "C" void AudioListener_ReadExtensionName_m457651115 ();
extern "C" void AudioListener_INTERNAL_CALL_ReadExtensionName_m3825001381 ();
extern "C" void AudioListener_ReadExtensionPropertyName_m2682178932 ();
extern "C" void AudioListener_INTERNAL_CALL_ReadExtensionPropertyName_m3514206420 ();
extern "C" void AudioListener_ReadExtensionPropertyValue_m2214914024 ();
extern "C" void AudioListener_ClearExtensionProperties_m3758880915 ();
extern "C" void AudioListener_INTERNAL_CALL_ClearExtensionProperties_m1492170042 ();
extern "C" void AudioListener_AddExtension_m1451255105 ();
extern "C" void AudioListenerExtension_get_audioListener_m3307089112 ();
extern "C" void AudioListenerExtension_set_audioListener_m3864337887 ();
extern "C" void AudioListenerExtension_WriteExtensionProperty_m1940049502 ();
extern "C" void AudioListenerExtension_ExtensionUpdate_m131363356 ();
extern "C" void AudioSettings_GetSpatializerPluginName_m2848677236 ();
extern "C" void AudioSettings_InvokeOnAudioConfigurationChanged_m3184710774 ();
extern "C" void AudioSettings_InvokeOnAudioManagerUpdate_m2154005943 ();
extern "C" void AudioSettings_InvokeOnAudioSourcePlay_m2060412953 ();
extern "C" void AudioSettings_GetAmbisonicDecoderPluginName_m70128080 ();
extern "C" void AudioConfigurationChangeHandler__ctor_m3404555898 ();
extern "C" void AudioConfigurationChangeHandler_Invoke_m2449447955 ();
extern "C" void AudioConfigurationChangeHandler_BeginInvoke_m2216313367 ();
extern "C" void AudioConfigurationChangeHandler_EndInvoke_m250397530 ();
extern "C" void AudioSource_get_clip_m2340175708 ();
extern "C" void AudioSource_get_isPlaying_m1796357815 ();
extern "C" void AudioSource_get_spatializeInternal_m3026556515 ();
extern "C" void AudioSource_get_spatialize_m2640512310 ();
extern "C" void AudioSource_GetNumExtensionProperties_m709284950 ();
extern "C" void AudioSource_ReadExtensionName_m3575108826 ();
extern "C" void AudioSource_INTERNAL_CALL_ReadExtensionName_m940874956 ();
extern "C" void AudioSource_ReadExtensionPropertyName_m2931479574 ();
extern "C" void AudioSource_INTERNAL_CALL_ReadExtensionPropertyName_m1096137561 ();
extern "C" void AudioSource_ReadExtensionPropertyValue_m1555844817 ();
extern "C" void AudioSource_ClearExtensionProperties_m3916939876 ();
extern "C" void AudioSource_INTERNAL_CALL_ClearExtensionProperties_m3670533364 ();
extern "C" void AudioSource_AddSpatializerExtension_m437310066 ();
extern "C" void AudioSource_AddAmbisonicExtension_m2770743028 ();
extern "C" void AudioSourceExtension_get_audioSource_m1457396452 ();
extern "C" void AudioSourceExtension_set_audioSource_m2372617077 ();
extern "C" void AudioSourceExtension_WriteExtensionProperty_m2994663373 ();
extern "C" void AudioSourceExtension_Play_m2504555527 ();
extern "C" void AudioSourceExtension_Stop_m3164232815 ();
extern "C" void AudioSourceExtension_ExtensionUpdate_m1678361052 ();
extern "C" void GameCenterPlatform__ctor_m1979894975 ();
extern "C" void GameCenterPlatform_Internal_Authenticate_m2072365377 ();
extern "C" void GameCenterPlatform_Internal_Authenticated_m2873448009 ();
extern "C" void GameCenterPlatform_Internal_UserName_m2866948064 ();
extern "C" void GameCenterPlatform_Internal_UserID_m1391210441 ();
extern "C" void GameCenterPlatform_Internal_Underage_m3153493531 ();
extern "C" void GameCenterPlatform_Internal_UserImage_m3635042813 ();
extern "C" void GameCenterPlatform_Internal_LoadFriends_m2096768347 ();
extern "C" void GameCenterPlatform_Internal_LoadAchievementDescriptions_m4285687138 ();
extern "C" void GameCenterPlatform_Internal_LoadAchievements_m1476802759 ();
extern "C" void GameCenterPlatform_Internal_ReportProgress_m4247572756 ();
extern "C" void GameCenterPlatform_Internal_ReportScore_m3392134640 ();
extern "C" void GameCenterPlatform_Internal_LoadScores_m3934381776 ();
extern "C" void GameCenterPlatform_Internal_ShowAchievementsUI_m2604671237 ();
extern "C" void GameCenterPlatform_Internal_ShowLeaderboardUI_m2609988910 ();
extern "C" void GameCenterPlatform_Internal_LoadUsers_m2376904235 ();
extern "C" void GameCenterPlatform_Internal_ResetAllAchievements_m860406903 ();
extern "C" void GameCenterPlatform_Internal_ShowDefaultAchievementBanner_m3807438363 ();
extern "C" void GameCenterPlatform_ResetAllAchievements_m766731470 ();
extern "C" void GameCenterPlatform_ShowDefaultAchievementCompletionBanner_m1206567558 ();
extern "C" void GameCenterPlatform_ShowLeaderboardUI_m76802803 ();
extern "C" void GameCenterPlatform_Internal_ShowSpecificLeaderboardUI_m1324819955 ();
extern "C" void GameCenterPlatform_ClearAchievementDescriptions_m1201462552 ();
extern "C" void GameCenterPlatform_SetAchievementDescription_m3265147368 ();
extern "C" void GameCenterPlatform_SetAchievementDescriptionImage_m433180119 ();
extern "C" void GameCenterPlatform_TriggerAchievementDescriptionCallback_m1368418878 ();
extern "C" void GameCenterPlatform_AuthenticateCallbackWrapper_m3518532938 ();
extern "C" void GameCenterPlatform_ClearFriends_m3997297748 ();
extern "C" void GameCenterPlatform_SetFriends_m3156453279 ();
extern "C" void GameCenterPlatform_SetFriendImage_m2565530221 ();
extern "C" void GameCenterPlatform_TriggerFriendsCallbackWrapper_m3549927236 ();
extern "C" void GameCenterPlatform_AchievementCallbackWrapper_m2393842948 ();
extern "C" void GameCenterPlatform_ProgressCallbackWrapper_m1761357634 ();
extern "C" void GameCenterPlatform_ScoreCallbackWrapper_m1017495084 ();
extern "C" void GameCenterPlatform_ScoreLoaderCallbackWrapper_m2497892497 ();
extern "C" void GameCenterPlatform_UnityEngine_SocialPlatforms_ISocialPlatform_LoadFriends_m3712251814 ();
extern "C" void GameCenterPlatform_UnityEngine_SocialPlatforms_ISocialPlatform_Authenticate_m1902473855 ();
extern "C" void GameCenterPlatform_UnityEngine_SocialPlatforms_ISocialPlatform_Authenticate_m473704383 ();
extern "C" void GameCenterPlatform_get_localUser_m1695301324 ();
extern "C" void GameCenterPlatform_PopulateLocalUser_m3608849642 ();
extern "C" void GameCenterPlatform_LoadAchievementDescriptions_m2456499679 ();
extern "C" void GameCenterPlatform_ReportProgress_m546889208 ();
extern "C" void GameCenterPlatform_LoadAchievements_m2248650362 ();
extern "C" void GameCenterPlatform_ReportScore_m2892009460 ();
extern "C" void GameCenterPlatform_LoadScores_m4246413931 ();
extern "C" void GameCenterPlatform_LoadScores_m3012647922 ();
extern "C" void GameCenterPlatform_LeaderboardCallbackWrapper_m1512666017 ();
extern "C" void GameCenterPlatform_GetLoading_m3617241336 ();
extern "C" void GameCenterPlatform_VerifyAuthentication_m4030994568 ();
extern "C" void GameCenterPlatform_ShowAchievementsUI_m278451572 ();
extern "C" void GameCenterPlatform_ShowLeaderboardUI_m184621452 ();
extern "C" void GameCenterPlatform_ClearUsers_m694297429 ();
extern "C" void GameCenterPlatform_SetUser_m1396993377 ();
extern "C" void GameCenterPlatform_SetUserImage_m102480673 ();
extern "C" void GameCenterPlatform_TriggerUsersCallbackWrapper_m741337649 ();
extern "C" void GameCenterPlatform_LoadUsers_m3057656793 ();
extern "C" void GameCenterPlatform_SafeSetUserImage_m4217127318 ();
extern "C" void GameCenterPlatform_SafeClearArray_m1090926739 ();
extern "C" void GameCenterPlatform_CreateLeaderboard_m2519210070 ();
extern "C" void GameCenterPlatform_CreateAchievement_m2189925977 ();
extern "C" void GameCenterPlatform_TriggerResetAchievementCallback_m1901797103 ();
extern "C" void GameCenterPlatform__cctor_m2214882229 ();
extern "C" void U3CUnityEngine_SocialPlatforms_ISocialPlatform_AuthenticateU3Ec__AnonStorey0__ctor_m1526017887 ();
extern "C" void U3CUnityEngine_SocialPlatforms_ISocialPlatform_AuthenticateU3Ec__AnonStorey0_U3CU3Em__0_m327311159 ();
extern "C" void GcAchievementData_ToAchievement_m1338988672_AdjustorThunk ();
extern "C" void GcAchievementDescriptionData_ToAchievementDescription_m1696951345_AdjustorThunk ();
extern "C" void GcLeaderboard__ctor_m2813903688 ();
extern "C" void GcLeaderboard_Finalize_m1739848811 ();
extern "C" void GcLeaderboard_Contains_m1630680475 ();
extern "C" void GcLeaderboard_SetScores_m3774366612 ();
extern "C" void GcLeaderboard_SetLocalScore_m347955094 ();
extern "C" void GcLeaderboard_SetMaxRange_m1939957199 ();
extern "C" void GcLeaderboard_SetTitle_m2280154755 ();
extern "C" void GcLeaderboard_Internal_LoadScores_m30788333 ();
extern "C" void GcLeaderboard_Loading_m2486631462 ();
extern "C" void GcLeaderboard_Dispose_m4178548762 ();
extern "C" void GcScoreData_ToScore_m1700738966_AdjustorThunk ();
extern "C" void GcUserProfileData_ToUserProfile_m1762043327_AdjustorThunk ();
extern "C" void GcUserProfileData_AddToArray_m3145544780_AdjustorThunk ();
extern "C" void Achievement__ctor_m1186584419 ();
extern "C" void Achievement__ctor_m4204630465 ();
extern "C" void Achievement__ctor_m620345011 ();
extern "C" void Achievement_ToString_m448994531 ();
extern "C" void Achievement_get_id_m352935039 ();
extern "C" void Achievement_set_id_m2592364518 ();
extern "C" void Achievement_get_percentCompleted_m3640585090 ();
extern "C" void Achievement_set_percentCompleted_m1848274796 ();
extern "C" void Achievement_get_completed_m4043821161 ();
extern "C" void Achievement_get_hidden_m1021287233 ();
extern "C" void Achievement_get_lastReportedDate_m1514471 ();
extern "C" void AchievementDescription__ctor_m2820135995 ();
extern "C" void AchievementDescription_ToString_m2630143428 ();
extern "C" void AchievementDescription_SetImage_m2500569521 ();
extern "C" void AchievementDescription_get_id_m1271036810 ();
extern "C" void AchievementDescription_set_id_m3879160604 ();
extern "C" void AchievementDescription_get_title_m2019393923 ();
extern "C" void AchievementDescription_get_achievedDescription_m918716349 ();
extern "C" void AchievementDescription_get_unachievedDescription_m2407176132 ();
extern "C" void AchievementDescription_get_hidden_m289731872 ();
extern "C" void AchievementDescription_get_points_m2481189241 ();
extern "C" void Leaderboard__ctor_m641037685 ();
extern "C" void Leaderboard_ToString_m3949952499 ();
extern "C" void Leaderboard_SetLocalUserScore_m1702011555 ();
extern "C" void Leaderboard_SetMaxRange_m470751880 ();
extern "C" void Leaderboard_SetScores_m1422084449 ();
extern "C" void Leaderboard_SetTitle_m3471503950 ();
extern "C" void Leaderboard_GetUserFilter_m2675148982 ();
extern "C" void Leaderboard_get_id_m523269624 ();
extern "C" void Leaderboard_set_id_m1452185904 ();
extern "C" void Leaderboard_get_userScope_m3651500032 ();
extern "C" void Leaderboard_set_userScope_m1479044752 ();
extern "C" void Leaderboard_get_range_m4014757670 ();
extern "C" void Leaderboard_set_range_m3228718273 ();
extern "C" void Leaderboard_get_timeScope_m2150671680 ();
extern "C" void Leaderboard_set_timeScope_m1836561867 ();
extern "C" void LocalUser__ctor_m167732678 ();
extern "C" void LocalUser_SetFriends_m3239690636 ();
extern "C" void LocalUser_SetAuthenticated_m2694551542 ();
extern "C" void LocalUser_SetUnderage_m380634886 ();
extern "C" void LocalUser_get_authenticated_m2652473512 ();
extern "C" void Score__ctor_m645280141 ();
extern "C" void Score__ctor_m932833410 ();
extern "C" void Score_ToString_m4151138973 ();
extern "C" void Score_get_leaderboardID_m274030957 ();
extern "C" void Score_set_leaderboardID_m2942345544 ();
extern "C" void Score_get_value_m3908827736 ();
extern "C" void Score_set_value_m2016376629 ();
extern "C" void UserProfile__ctor_m110286049 ();
extern "C" void UserProfile__ctor_m2304939591 ();
extern "C" void UserProfile_ToString_m466929597 ();
extern "C" void UserProfile_SetUserName_m768170661 ();
extern "C" void UserProfile_SetUserID_m977688691 ();
extern "C" void UserProfile_SetImage_m2281807389 ();
extern "C" void UserProfile_get_userName_m3191800758 ();
extern "C" void UserProfile_get_id_m3617419551 ();
extern "C" void UserProfile_get_isFriend_m4217471323 ();
extern "C" void UserProfile_get_state_m3743219796 ();
extern "C" void Range__ctor_m2051987772_AdjustorThunk ();
extern "C" void Tile__ctor_m3471756680 ();
extern "C" void TileBase__ctor_m2441256229 ();
extern "C" void Analytics_GetUnityAnalyticsHandler_m3943512563 ();
extern "C" void Analytics_CustomEvent_m2416379667 ();
extern "C" void CustomEventData__ctor_m248325780 ();
extern "C" void CustomEventData_InternalCreate_m782104979 ();
extern "C" void CustomEventData_InternalDestroy_m998346656 ();
extern "C" void CustomEventData_AddString_m2143902135 ();
extern "C" void CustomEventData_AddBool_m436037313 ();
extern "C" void CustomEventData_AddChar_m2437065113 ();
extern "C" void CustomEventData_AddByte_m210020965 ();
extern "C" void CustomEventData_AddSByte_m3889282772 ();
extern "C" void CustomEventData_AddInt16_m1607163544 ();
extern "C" void CustomEventData_AddUInt16_m3424486269 ();
extern "C" void CustomEventData_AddInt32_m2136872868 ();
extern "C" void CustomEventData_AddUInt32_m911556407 ();
extern "C" void CustomEventData_AddInt64_m1450877831 ();
extern "C" void CustomEventData_AddUInt64_m33970678 ();
extern "C" void CustomEventData_AddDouble_m1838257319 ();
extern "C" void CustomEventData_Finalize_m2193402917 ();
extern "C" void CustomEventData_Dispose_m3028624316 ();
extern "C" void CustomEventData_Add_m1800489861 ();
extern "C" void CustomEventData_Add_m3718299101 ();
extern "C" void CustomEventData_Add_m818177505 ();
extern "C" void CustomEventData_Add_m365150271 ();
extern "C" void CustomEventData_Add_m3633493165 ();
extern "C" void CustomEventData_Add_m3549807151 ();
extern "C" void CustomEventData_Add_m1292879494 ();
extern "C" void CustomEventData_Add_m3529462958 ();
extern "C" void CustomEventData_Add_m39184950 ();
extern "C" void CustomEventData_Add_m3872278440 ();
extern "C" void CustomEventData_Add_m380105428 ();
extern "C" void CustomEventData_Add_m748231590 ();
extern "C" void CustomEventData_Add_m3177631423 ();
extern "C" void CustomEventData_Add_m2573195837 ();
extern "C" void CustomEventData_Add_m3144047368 ();
extern "C" void UnityAnalyticsHandler__ctor_m2295955364 ();
extern "C" void UnityAnalyticsHandler_InternalCreate_m3939206337 ();
extern "C" void UnityAnalyticsHandler_InternalDestroy_m316722488 ();
extern "C" void UnityAnalyticsHandler_Finalize_m1395391108 ();
extern "C" void UnityAnalyticsHandler_Dispose_m3249301322 ();
extern "C" void UnityAnalyticsHandler_CustomEvent_m2316692438 ();
extern "C" void UnityAnalyticsHandler_CustomEvent_m2476810961 ();
extern "C" void UnityAnalyticsHandler_SendCustomEventName_m2761615078 ();
extern "C" void UnityAnalyticsHandler_SendCustomEvent_m3141620171 ();
extern "C" void AnalyticsSessionInfo_CallSessionStateChanged_m2908054006 ();
extern "C" void SessionStateChanged__ctor_m4022175722 ();
extern "C" void SessionStateChanged_Invoke_m4131999127 ();
extern "C" void SessionStateChanged_BeginInvoke_m136008215 ();
extern "C" void SessionStateChanged_EndInvoke_m485529971 ();
extern "C" void RemoteSettings_CallOnUpdate_m837272010 ();
extern "C" void UpdatedEventHandler__ctor_m3030824834 ();
extern "C" void UpdatedEventHandler_Invoke_m54617308 ();
extern "C" void UpdatedEventHandler_BeginInvoke_m3089859649 ();
extern "C" void UpdatedEventHandler_EndInvoke_m1780394930 ();
extern "C" void WebRequestUtils_RedirectTo_m1499419735 ();
extern "C" void WebRequestUtils__cctor_m1941139123 ();
extern "C" void AnalyticsTracker__ctor_m3613341151 ();
extern "C" void AnalyticsTracker_get_eventName_m673691953 ();
extern "C" void AnalyticsTracker_set_eventName_m2482650980 ();
extern "C" void AnalyticsTracker_get_TP_m652212131 ();
extern "C" void AnalyticsTracker_set_TP_m2539166404 ();
extern "C" void AnalyticsTracker_Awake_m3542971843 ();
extern "C" void AnalyticsTracker_Start_m3995864915 ();
extern "C" void AnalyticsTracker_OnEnable_m2258709727 ();
extern "C" void AnalyticsTracker_OnDisable_m63278792 ();
extern "C" void AnalyticsTracker_OnApplicationPause_m1103455594 ();
extern "C" void AnalyticsTracker_OnDestroy_m2158658835 ();
extern "C" void AnalyticsTracker_TriggerEvent_m2952898466 ();
extern "C" void AnalyticsTracker_SendEvent_m2871678427 ();
extern "C" void AnalyticsTracker_BuildParameters_m284111758 ();
extern "C" void TrackableProperty__ctor_m1315091320 ();
extern "C" void TrackableProperty_get_fields_m2052693107 ();
extern "C" void TrackableProperty_set_fields_m2675049714 ();
extern "C" void TrackableProperty_GetHashCode_m1969014309 ();
extern "C" void FieldWithTarget__ctor_m2124372405 ();
extern "C" void FieldWithTarget_get_paramName_m2700030636 ();
extern "C" void FieldWithTarget_set_paramName_m2103279178 ();
extern "C" void FieldWithTarget_get_target_m1282286626 ();
extern "C" void FieldWithTarget_set_target_m342185399 ();
extern "C" void FieldWithTarget_get_fieldPath_m2089477078 ();
extern "C" void FieldWithTarget_set_fieldPath_m33337211 ();
extern "C" void FieldWithTarget_get_typeString_m1884618032 ();
extern "C" void FieldWithTarget_set_typeString_m2393875225 ();
extern "C" void FieldWithTarget_get_doStatic_m2444575716 ();
extern "C" void FieldWithTarget_set_doStatic_m2163203065 ();
extern "C" void FieldWithTarget_get_staticString_m1927797833 ();
extern "C" void FieldWithTarget_set_staticString_m2790139420 ();
extern "C" void FieldWithTarget_GetValue_m40081609 ();
extern const Il2CppMethodPointer g_MethodPointers[7895] = 
{
	Locale_GetText_m1500108805,
	Locale_GetText_m659940240,
	SafeHandleZeroOrMinusOneIsInvalid__ctor_m3262641181,
	SafeHandleZeroOrMinusOneIsInvalid_get_IsInvalid_m891415827,
	SafeWaitHandle__ctor_m1097298895,
	SafeWaitHandle_ReleaseHandle_m124655883,
	CodePointIndexer__ctor_m641528607,
	CodePointIndexer_ToIndex_m2746420843,
	TableRange__ctor_m336013596_AdjustorThunk,
	Contraction__ctor_m4145481304,
	ContractionComparer__ctor_m1527695121,
	ContractionComparer__cctor_m2310355412,
	ContractionComparer_Compare_m2517703923,
	Level2Map__ctor_m2637101496,
	Level2MapComparer__ctor_m3314476384,
	Level2MapComparer__cctor_m1504678356,
	Level2MapComparer_Compare_m2948085554,
	MSCompatUnicodeTable__cctor_m2698856002,
	MSCompatUnicodeTable_GetTailoringInfo_m2751095510,
	MSCompatUnicodeTable_BuildTailoringTables_m4143751451,
	MSCompatUnicodeTable_SetCJKReferences_m3476394623,
	MSCompatUnicodeTable_Category_m3438761433,
	MSCompatUnicodeTable_Level1_m3928110760,
	MSCompatUnicodeTable_Level2_m351989625,
	MSCompatUnicodeTable_Level3_m714284458,
	MSCompatUnicodeTable_IsIgnorable_m4153158312,
	MSCompatUnicodeTable_IsIgnorableNonSpacing_m4192456948,
	MSCompatUnicodeTable_ToKanaTypeInsensitive_m2988229978,
	MSCompatUnicodeTable_ToWidthCompat_m2059231301,
	MSCompatUnicodeTable_HasSpecialWeight_m1783500999,
	MSCompatUnicodeTable_IsHalfWidthKana_m2452526595,
	MSCompatUnicodeTable_IsHiragana_m2045669619,
	MSCompatUnicodeTable_IsJapaneseSmallLetter_m2872205620,
	MSCompatUnicodeTable_get_IsReady_m690130938,
	MSCompatUnicodeTable_GetResource_m2433291647,
	MSCompatUnicodeTable_UInt32FromBytePtr_m3054783212,
	MSCompatUnicodeTable_FillCJK_m1585672419,
	MSCompatUnicodeTable_FillCJKCore_m709191614,
	MSCompatUnicodeTableUtil__cctor_m1217423538,
	SimpleCollator__ctor_m3070788168,
	SimpleCollator__cctor_m919560922,
	SimpleCollator_SetCJKTable_m2575392057,
	SimpleCollator_GetNeutralCulture_m1445005125,
	SimpleCollator_Category_m2664735685,
	SimpleCollator_Level1_m514441013,
	SimpleCollator_Level2_m201463002,
	SimpleCollator_IsHalfKana_m1012930504,
	SimpleCollator_GetContraction_m740789706,
	SimpleCollator_GetContraction_m111857746,
	SimpleCollator_GetTailContraction_m1366162196,
	SimpleCollator_GetTailContraction_m3938007728,
	SimpleCollator_FilterOptions_m343004938,
	SimpleCollator_GetExtenderType_m1669010917,
	SimpleCollator_ToDashTypeValue_m4105598342,
	SimpleCollator_FilterExtender_m2713511102,
	SimpleCollator_IsIgnorable_m3327547977,
	SimpleCollator_IsSafe_m2235906205,
	SimpleCollator_GetSortKey_m2918984438,
	SimpleCollator_GetSortKey_m4279889981,
	SimpleCollator_GetSortKey_m881222275,
	SimpleCollator_FillSortKeyRaw_m3825701976,
	SimpleCollator_FillSurrogateSortKeyRaw_m3498490842,
	SimpleCollator_CompareOrdinal_m256887360,
	SimpleCollator_CompareQuick_m300970728,
	SimpleCollator_CompareOrdinalIgnoreCase_m3541555993,
	SimpleCollator_Compare_m3639527517,
	SimpleCollator_ClearBuffer_m1493829777,
	SimpleCollator_QuickCheckPossible_m2336776935,
	SimpleCollator_CompareInternal_m2566514740,
	SimpleCollator_CompareFlagPair_m2760041594,
	SimpleCollator_IsPrefix_m3900513470,
	SimpleCollator_IsPrefix_m622163661,
	SimpleCollator_IsPrefix_m2024733508,
	SimpleCollator_IsSuffix_m3042779660,
	SimpleCollator_IsSuffix_m432518715,
	SimpleCollator_QuickIndexOf_m864247376,
	SimpleCollator_IndexOf_m1766454037,
	SimpleCollator_IndexOfOrdinal_m2136019101,
	SimpleCollator_IndexOfOrdinalIgnoreCase_m4064724991,
	SimpleCollator_IndexOfSortKey_m708779630,
	SimpleCollator_IndexOf_m3590481976,
	SimpleCollator_LastIndexOf_m3772036014,
	SimpleCollator_LastIndexOfOrdinal_m1415648312,
	SimpleCollator_LastIndexOfOrdinalIgnoreCase_m2855724054,
	SimpleCollator_LastIndexOfSortKey_m257786269,
	SimpleCollator_LastIndexOf_m4081169027,
	SimpleCollator_MatchesForward_m3769579644,
	SimpleCollator_MatchesForwardCore_m1489578655,
	SimpleCollator_MatchesPrimitive_m4222486953,
	SimpleCollator_MatchesBackward_m3447956182,
	SimpleCollator_MatchesBackwardCore_m415114982,
	Context__ctor_m1030643923_AdjustorThunk,
	PreviousInfo__ctor_m2851947467_AdjustorThunk,
	SortKeyBuffer__ctor_m1945108209,
	SortKeyBuffer_Reset_m3773372570,
	SortKeyBuffer_Initialize_m611850055,
	SortKeyBuffer_AppendCJKExtension_m1152522895,
	SortKeyBuffer_AppendKana_m4023417654,
	SortKeyBuffer_AppendNormal_m3224853385,
	SortKeyBuffer_AppendLevel5_m1744389528,
	SortKeyBuffer_AppendBufferPrimitive_m1397593205,
	SortKeyBuffer_GetResultAndReset_m1229988782,
	SortKeyBuffer_GetOptimizedLength_m4230725576,
	SortKeyBuffer_GetResult_m3225648020,
	TailoringInfo__ctor_m2655216332,
	BigInteger__ctor_m1163451324,
	BigInteger__ctor_m4276013935,
	BigInteger__ctor_m1204451283,
	BigInteger__ctor_m602686585,
	BigInteger__ctor_m2767934931,
	BigInteger__cctor_m4083811350,
	BigInteger_get_Rng_m618323497,
	BigInteger_GenerateRandom_m2914817005,
	BigInteger_GenerateRandom_m3518857267,
	BigInteger_Randomize_m1383479350,
	BigInteger_Randomize_m3583174950,
	BigInteger_BitCount_m1373717292,
	BigInteger_TestBit_m2163967392,
	BigInteger_TestBit_m3545384379,
	BigInteger_SetBit_m1201518092,
	BigInteger_SetBit_m3612418635,
	BigInteger_LowestSetBit_m1526675678,
	BigInteger_GetBytes_m1296189873,
	BigInteger_ToString_m3484377,
	BigInteger_ToString_m1522914732,
	BigInteger_Normalize_m1829363676,
	BigInteger_Clear_m365822215,
	BigInteger_GetHashCode_m2393929432,
	BigInteger_ToString_m653860377,
	BigInteger_Equals_m2136904561,
	BigInteger_ModInverse_m3474433369,
	BigInteger_ModPow_m3085084927,
	BigInteger_IsProbablePrime_m1133942801,
	BigInteger_GeneratePseudoPrime_m3283596575,
	BigInteger_Incr2_m2251347137,
	BigInteger_op_Implicit_m932569639,
	BigInteger_op_Implicit_m285994883,
	BigInteger_op_Addition_m2673463548,
	BigInteger_op_Subtraction_m2191246285,
	BigInteger_op_Modulus_m319664938,
	BigInteger_op_Modulus_m2212009832,
	BigInteger_op_Division_m2255171013,
	BigInteger_op_Multiply_m4220659470,
	BigInteger_op_Multiply_m496362754,
	BigInteger_op_LeftShift_m1113089594,
	BigInteger_op_RightShift_m3254229068,
	BigInteger_op_Equality_m1834303355,
	BigInteger_op_Inequality_m2912048490,
	BigInteger_op_Equality_m498942008,
	BigInteger_op_Inequality_m206477400,
	BigInteger_op_GreaterThan_m3015037594,
	BigInteger_op_LessThan_m2208065797,
	BigInteger_op_GreaterThanOrEqual_m2434423199,
	BigInteger_op_LessThanOrEqual_m1554543193,
	Kernel_AddSameSign_m3443248778,
	Kernel_Subtract_m1850203865,
	Kernel_MinusEq_m4012755338,
	Kernel_PlusEq_m2110508277,
	Kernel_Compare_m3502948456,
	Kernel_SingleByteDivideInPlace_m4237050077,
	Kernel_DwordMod_m1683999714,
	Kernel_DwordDivMod_m3417043239,
	Kernel_multiByteDivide_m3706099675,
	Kernel_LeftShift_m1129695651,
	Kernel_RightShift_m1033490646,
	Kernel_MultiplyByDword_m1419153077,
	Kernel_Multiply_m3461261708,
	Kernel_MultiplyMod2p32pmod_m110351456,
	Kernel_modInverse_m663909403,
	Kernel_modInverse_m2047049350,
	ModulusRing__ctor_m935358764,
	ModulusRing_BarrettReduction_m3666592548,
	ModulusRing_Multiply_m1084630745,
	ModulusRing_Difference_m3162597472,
	ModulusRing_Pow_m2581654810,
	ModulusRing_Pow_m2784243082,
	PrimeGeneratorBase__ctor_m2106328266,
	PrimeGeneratorBase_get_Confidence_m2792095523,
	PrimeGeneratorBase_get_PrimalityTest_m665017960,
	PrimeGeneratorBase_get_TrialDivisionBounds_m1980307417,
	SequentialSearchPrimeGeneratorBase__ctor_m2428287067,
	SequentialSearchPrimeGeneratorBase_GenerateSearchBase_m1992894226,
	SequentialSearchPrimeGeneratorBase_GenerateNewPrime_m3382928048,
	SequentialSearchPrimeGeneratorBase_GenerateNewPrime_m1222965799,
	SequentialSearchPrimeGeneratorBase_IsPrimeAcceptable_m1765180,
	PrimalityTest__ctor_m3603898030,
	PrimalityTest_Invoke_m1441281691,
	PrimalityTest_BeginInvoke_m4069941682,
	PrimalityTest_EndInvoke_m1063884147,
	PrimalityTests_GetSPPRounds_m1689044817,
	PrimalityTests_Test_m2914274961,
	PrimalityTests_RabinMillerTest_m3119022782,
	PrimalityTests_SmallPrimeSppTest_m2762617466,
	Runtime_GetDisplayName_m896344743,
	ASN1__ctor_m3803179655,
	ASN1__ctor_m2577696247,
	ASN1__ctor_m3795696753,
	ASN1_get_Count_m510274956,
	ASN1_get_Tag_m3065754366,
	ASN1_get_Length_m1747607096,
	ASN1_get_Value_m994047287,
	ASN1_set_Value_m2844359354,
	ASN1_CompareArray_m1850577781,
	ASN1_CompareValue_m4047113686,
	ASN1_Add_m893163025,
	ASN1_GetBytes_m1657979790,
	ASN1_Decode_m894249739,
	ASN1_DecodeTLV_m3135978438,
	ASN1_get_Item_m1761258769,
	ASN1_Element_m443211517,
	ASN1_ToString_m3567391814,
	ASN1Convert_FromInt32_m2943578612,
	ASN1Convert_FromOid_m1591878611,
	ASN1Convert_ToInt32_m2142816542,
	ASN1Convert_ToOid_m3041377917,
	ASN1Convert_ToDateTime_m3552379352,
	BitConverterLE_GetUIntBytes_m2947082112,
	BitConverterLE_GetBytes_m3351943523,
	BitConverterLE_UShortFromBytes_m489294661,
	BitConverterLE_UIntFromBytes_m3748439035,
	BitConverterLE_ULongFromBytes_m268960415,
	BitConverterLE_ToInt16_m474007711,
	BitConverterLE_ToInt32_m2972956219,
	BitConverterLE_ToSingle_m1696218306,
	BitConverterLE_ToDouble_m3522125088,
	BlockProcessor__ctor_m3923969706,
	BlockProcessor_Finalize_m2837824247,
	BlockProcessor_Initialize_m3421916057,
	BlockProcessor_Core_m3319995037,
	BlockProcessor_Core_m1962461118,
	BlockProcessor_Final_m1037678916,
	CryptoConvert_ToInt32LE_m1342791765,
	CryptoConvert_ToUInt32LE_m3548106783,
	CryptoConvert_GetBytesLE_m1103380998,
	CryptoConvert_ToCapiPrivateKeyBlob_m2393656989,
	CryptoConvert_FromCapiPublicKeyBlob_m59550354,
	CryptoConvert_FromCapiPublicKeyBlob_m4230898509,
	CryptoConvert_ToCapiPublicKeyBlob_m435972191,
	CryptoConvert_ToCapiKeyBlob_m3562765047,
	DSAManaged__ctor_m825313286,
	DSAManaged_add_KeyGenerated_m2286829514,
	DSAManaged_remove_KeyGenerated_m413000032,
	DSAManaged_Finalize_m4258386870,
	DSAManaged_Generate_m2124060824,
	DSAManaged_GenerateKeyPair_m2354969948,
	DSAManaged_add_m366118670,
	DSAManaged_GenerateParams_m384040316,
	DSAManaged_get_Random_m2411009448,
	DSAManaged_get_KeySize_m489524026,
	DSAManaged_get_PublicOnly_m4157486105,
	DSAManaged_NormalizeArray_m2844092920,
	DSAManaged_ExportParameters_m3125165151,
	DSAManaged_ImportParameters_m4236950267,
	DSAManaged_CreateSignature_m1543369866,
	DSAManaged_VerifySignature_m2542559486,
	DSAManaged_Dispose_m885681596,
	KeyGeneratedEventHandler__ctor_m3788308642,
	KeyGeneratedEventHandler_Invoke_m3199187352,
	KeyGeneratedEventHandler_BeginInvoke_m2401238559,
	KeyGeneratedEventHandler_EndInvoke_m1876986809,
	KeyBuilder_get_Rng_m2329308910,
	KeyBuilder_Key_m1324353598,
	KeyBuilder_IV_m3167732669,
	KeyPairPersistence__ctor_m1451632529,
	KeyPairPersistence__ctor_m282806890,
	KeyPairPersistence__cctor_m1615101261,
	KeyPairPersistence_get_Filename_m3817558798,
	KeyPairPersistence_get_KeyValue_m913559137,
	KeyPairPersistence_set_KeyValue_m2997429917,
	KeyPairPersistence_Load_m3650468242,
	KeyPairPersistence_Save_m2616030637,
	KeyPairPersistence_Remove_m2462309025,
	KeyPairPersistence_get_UserPath_m3263003313,
	KeyPairPersistence_get_MachinePath_m3279896206,
	KeyPairPersistence__CanSecure_m2460431986,
	KeyPairPersistence__ProtectUser_m3343002560,
	KeyPairPersistence__ProtectMachine_m775740381,
	KeyPairPersistence__IsUserProtected_m2266257435,
	KeyPairPersistence__IsMachineProtected_m2666443600,
	KeyPairPersistence_CanSecure_m1918655102,
	KeyPairPersistence_ProtectUser_m691605374,
	KeyPairPersistence_ProtectMachine_m2277236355,
	KeyPairPersistence_IsUserProtected_m1144613255,
	KeyPairPersistence_IsMachineProtected_m2149987251,
	KeyPairPersistence_get_CanChange_m2639804921,
	KeyPairPersistence_get_UseDefaultKeyContainer_m3169890059,
	KeyPairPersistence_get_UseMachineKeyStore_m1676827332,
	KeyPairPersistence_get_ContainerName_m1970806748,
	KeyPairPersistence_Copy_m2514706484,
	KeyPairPersistence_FromXml_m615148467,
	KeyPairPersistence_ToXml_m507669548,
	MACAlgorithm__ctor_m1327376633,
	MACAlgorithm_Initialize_m2073140225,
	MACAlgorithm_Core_m1291994500,
	MACAlgorithm_Final_m295519667,
	PKCS1__cctor_m2725722605,
	PKCS1_Compare_m3440795783,
	PKCS1_I2OSP_m2868122941,
	PKCS1_OS2IP_m1068728329,
	PKCS1_RSAEP_m4105321444,
	PKCS1_RSASP1_m447018256,
	PKCS1_RSAVP1_m3458839365,
	PKCS1_Encrypt_v15_m385206638,
	PKCS1_Sign_v15_m2716890696,
	PKCS1_Verify_v15_m527674405,
	PKCS1_Verify_v15_m2131582328,
	PKCS1_Encode_v15_m424069037,
	EncryptedPrivateKeyInfo__ctor_m299012888,
	EncryptedPrivateKeyInfo__ctor_m647322062,
	EncryptedPrivateKeyInfo_get_Algorithm_m3741750336,
	EncryptedPrivateKeyInfo_get_EncryptedData_m2865828442,
	EncryptedPrivateKeyInfo_get_Salt_m267749150,
	EncryptedPrivateKeyInfo_get_IterationCount_m3882266506,
	EncryptedPrivateKeyInfo_Decode_m4290686938,
	PrivateKeyInfo__ctor_m2289224748,
	PrivateKeyInfo__ctor_m2773314483,
	PrivateKeyInfo_get_PrivateKey_m2864070573,
	PrivateKeyInfo_Decode_m2782298882,
	PrivateKeyInfo_RemoveLeadingZero_m3554138665,
	PrivateKeyInfo_Normalize_m2688416120,
	PrivateKeyInfo_DecodeRSA_m1604240262,
	PrivateKeyInfo_DecodeDSA_m2761492714,
	RSAManaged__ctor_m4104718293,
	RSAManaged_add_KeyGenerated_m2743778230,
	RSAManaged_remove_KeyGenerated_m1304981015,
	RSAManaged_Finalize_m996292420,
	RSAManaged_GenerateKeyPair_m1691973565,
	RSAManaged_get_KeySize_m388882796,
	RSAManaged_get_PublicOnly_m3311538867,
	RSAManaged_DecryptValue_m4061971567,
	RSAManaged_EncryptValue_m4114751905,
	RSAManaged_ExportParameters_m2767124364,
	RSAManaged_ImportParameters_m1077493558,
	RSAManaged_Dispose_m1706711259,
	RSAManaged_ToXmlString_m3234843640,
	RSAManaged_get_IsCrtPossible_m1210928227,
	RSAManaged_GetPaddedValue_m1049410827,
	KeyGeneratedEventHandler__ctor_m1299821668,
	KeyGeneratedEventHandler_Invoke_m3228903531,
	KeyGeneratedEventHandler_BeginInvoke_m714135090,
	KeyGeneratedEventHandler_EndInvoke_m1786385152,
	SymmetricTransform__ctor_m1975106537,
	SymmetricTransform_System_IDisposable_Dispose_m1742260198,
	SymmetricTransform_Finalize_m538552370,
	SymmetricTransform_Dispose_m596126426,
	SymmetricTransform_get_CanReuseTransform_m271089955,
	SymmetricTransform_Transform_m174988247,
	SymmetricTransform_CBC_m3692369966,
	SymmetricTransform_CFB_m709939617,
	SymmetricTransform_OFB_m3913775331,
	SymmetricTransform_CTS_m2019337593,
	SymmetricTransform_CheckInput_m1396199888,
	SymmetricTransform_TransformBlock_m2138610695,
	SymmetricTransform_get_KeepLastBlock_m3387053162,
	SymmetricTransform_InternalTransformBlock_m640960295,
	SymmetricTransform_Random_m2149136044,
	SymmetricTransform_ThrowBadPaddingException_m3379056639,
	SymmetricTransform_FinalEncrypt_m4159191400,
	SymmetricTransform_FinalDecrypt_m147017762,
	SymmetricTransform_TransformFinalBlock_m1249457332,
	ContentInfo__ctor_m2175703642,
	ContentInfo__ctor_m3806313524,
	ContentInfo__ctor_m4254357018,
	ContentInfo__ctor_m3774822752,
	ContentInfo_get_ASN1_m1622402385,
	ContentInfo_get_Content_m3812643298,
	ContentInfo_set_Content_m2819593409,
	ContentInfo_get_ContentType_m705567708,
	ContentInfo_set_ContentType_m1623352580,
	ContentInfo_GetASN1_m2294371716,
	EncryptedData__ctor_m3652070480,
	EncryptedData__ctor_m3289444422,
	EncryptedData_get_EncryptionAlgorithm_m1688795622,
	EncryptedData_get_EncryptedContent_m3858156333,
	StrongName__cctor_m3439962771,
	StrongName_get_PublicKey_m47263336,
	StrongName_get_PublicKeyToken_m1524306997,
	StrongName_get_TokenAlgorithm_m3765006973,
	PKCS12__ctor_m2188309115,
	PKCS12__ctor_m4293971484,
	PKCS12__ctor_m3397759167,
	PKCS12__cctor_m3109607630,
	PKCS12_Decode_m2321190257,
	PKCS12_Finalize_m3111395487,
	PKCS12_set_Password_m4004509738,
	PKCS12_get_IterationCount_m1734635322,
	PKCS12_set_IterationCount_m3469889302,
	PKCS12_get_Certificates_m1819594382,
	PKCS12_get_RNG_m403016427,
	PKCS12_Compare_m2821129482,
	PKCS12_GetSymmetricAlgorithm_m3630792258,
	PKCS12_Decrypt_m2054700421,
	PKCS12_Decrypt_m4089729530,
	PKCS12_Encrypt_m172278618,
	PKCS12_GetExistingParameters_m1811502439,
	PKCS12_AddPrivateKey_m627015820,
	PKCS12_ReadSafeBag_m2230528012,
	PKCS12_CertificateSafeBag_m1158114043,
	PKCS12_MAC_m3266389236,
	PKCS12_GetBytes_m3948157025,
	PKCS12_EncryptedContentInfo_m335804703,
	PKCS12_AddCertificate_m1113610247,
	PKCS12_AddCertificate_m1603297645,
	PKCS12_RemoveCertificate_m3573824403,
	PKCS12_RemoveCertificate_m4279394528,
	PKCS12_Clone_m1907616559,
	PKCS12_get_MaximumPasswordLength_m1622540742,
	DeriveBytes__ctor_m1630711249,
	DeriveBytes__cctor_m3363162864,
	DeriveBytes_set_HashName_m1466264544,
	DeriveBytes_set_IterationCount_m1012983914,
	DeriveBytes_set_Password_m1776827631,
	DeriveBytes_set_Salt_m4247292830,
	DeriveBytes_Adjust_m73010620,
	DeriveBytes_Derive_m1432759067,
	DeriveBytes_DeriveKey_m1070693331,
	DeriveBytes_DeriveIV_m1377512391,
	DeriveBytes_DeriveMAC_m333834467,
	SafeBag__ctor_m885430396,
	SafeBag_get_BagOID_m3200989054,
	SafeBag_get_ASN1_m3927983005,
	X501__cctor_m176587435,
	X501_ToString_m606787655,
	X501_ToString_m4265468278,
	X501_AppendEntry_m4126472065,
	X509Certificate__ctor_m3974010651,
	X509Certificate__cctor_m2281686218,
	X509Certificate_Parse_m2601788492,
	X509Certificate_GetUnsignedBigInteger_m125969945,
	X509Certificate_get_DSA_m2379531329,
	X509Certificate_get_IssuerName_m1190634728,
	X509Certificate_get_KeyAlgorithmParameters_m3328452325,
	X509Certificate_get_PublicKey_m1559630494,
	X509Certificate_get_RawData_m2126743996,
	X509Certificate_get_SubjectName_m4016633973,
	X509Certificate_get_ValidFrom_m1391977265,
	X509Certificate_get_ValidUntil_m4052955887,
	X509Certificate_GetIssuerName_m1761647748,
	X509Certificate_GetSubjectName_m196414200,
	X509Certificate_PEM_m2377423875,
	X509CertificateCollection__ctor_m4087327610,
	X509CertificateCollection_System_Collections_IEnumerable_GetEnumerator_m3571738219,
	X509CertificateCollection_get_Item_m3973324547,
	X509CertificateCollection_Add_m1343769477,
	X509CertificateCollection_GetEnumerator_m2813315757,
	X509CertificateCollection_GetHashCode_m3353991612,
	X509CertificateEnumerator__ctor_m2380153920,
	X509CertificateEnumerator_System_Collections_IEnumerator_get_Current_m425597426,
	X509CertificateEnumerator_System_Collections_IEnumerator_MoveNext_m1313749686,
	X509CertificateEnumerator_System_Collections_IEnumerator_Reset_m1280518935,
	X509CertificateEnumerator_get_Current_m3401736547,
	X509CertificateEnumerator_MoveNext_m470429900,
	X509CertificateEnumerator_Reset_m3629273990,
	X509Extension__ctor_m592474861,
	X509Extension_Decode_m3340505567,
	X509Extension_Equals_m1026821796,
	X509Extension_GetHashCode_m3905824097,
	X509Extension_WriteLine_m400107663,
	X509Extension_ToString_m1108071185,
	X509ExtensionCollection__ctor_m2229994607,
	X509ExtensionCollection__ctor_m1964702411,
	X509ExtensionCollection_System_Collections_IEnumerable_GetEnumerator_m3539740658,
	SecurityParser__ctor_m2190542591,
	SecurityParser_LoadXml_m2527250266,
	SecurityParser_ToXml_m2002718429,
	SecurityParser_OnStartParsing_m713460294,
	SecurityParser_OnProcessingInstruction_m156777986,
	SecurityParser_OnIgnorableWhitespace_m916920983,
	SecurityParser_OnStartElement_m1212613838,
	SecurityParser_OnEndElement_m2404776146,
	SecurityParser_OnChars_m1741899660,
	SecurityParser_OnEndParsing_m630820298,
	SmallXmlParser__ctor_m2019585097,
	SmallXmlParser_Error_m429104818,
	SmallXmlParser_UnexpectedEndError_m1157145880,
	SmallXmlParser_IsNameChar_m1965865578,
	SmallXmlParser_IsWhitespace_m2316828733,
	SmallXmlParser_SkipWhitespaces_m1707474564,
	SmallXmlParser_HandleWhitespaces_m1447467011,
	SmallXmlParser_SkipWhitespaces_m785728345,
	SmallXmlParser_Peek_m3745074368,
	SmallXmlParser_Read_m937609928,
	SmallXmlParser_Expect_m1449932214,
	SmallXmlParser_ReadUntil_m1248365356,
	SmallXmlParser_ReadName_m1261648197,
	SmallXmlParser_Parse_m781232591,
	SmallXmlParser_Cleanup_m814219253,
	SmallXmlParser_ReadContent_m1339062023,
	SmallXmlParser_HandleBufferedContent_m38070252,
	SmallXmlParser_ReadCharacters_m3036429763,
	SmallXmlParser_ReadReference_m2147555252,
	SmallXmlParser_ReadCharacterReference_m956624000,
	SmallXmlParser_ReadAttribute_m143848528,
	SmallXmlParser_ReadCDATASection_m181360986,
	SmallXmlParser_ReadComment_m4172889697,
	AttrListImpl__ctor_m2113556480,
	AttrListImpl_get_Length_m2184336069,
	AttrListImpl_GetName_m1423383098,
	AttrListImpl_GetValue_m119139426,
	AttrListImpl_GetValue_m525211908,
	AttrListImpl_get_Names_m2552952956,
	AttrListImpl_get_Values_m3882443736,
	AttrListImpl_Clear_m4127928365,
	AttrListImpl_Add_m3390079384,
	SmallXmlParserException__ctor_m3660590675,
	__Il2CppComDelegate_Finalize_m4081530528,
	__Il2CppComObject_Finalize_m542937421,
	AccessViolationException__ctor_m1988585420,
	AccessViolationException__ctor_m2115873943,
	ActivationContext_Finalize_m4190316793,
	ActivationContext_Dispose_m458277660,
	ActivationContext_Dispose_m3745846357,
	Activator_CreateInstance_m88608359,
	Activator_CreateInstance_m291998472,
	Activator_CreateInstance_m3002907732,
	Activator_CreateInstance_m3926027424,
	Activator_CreateInstance_m630832716,
	Activator_CheckType_m728321487,
	Activator_CheckAbstractType_m2922392263,
	Activator_CreateInstanceInternal_m1316031764,
	AppDomain__ctor_m858955160,
	AppDomain_add_UnhandledException_m1681775214,
	AppDomain_remove_UnhandledException_m1958459414,
	AppDomain_getFriendlyName_m2263966558,
	AppDomain_getCurDomain_m617417482,
	AppDomain_get_CurrentDomain_m4216043527,
	AppDomain_LoadAssembly_m1223519328,
	AppDomain_Load_m1049886220,
	AppDomain_Load_m2889673107,
	AppDomain_InternalSetContext_m3764293861,
	AppDomain_InternalGetContext_m4048436193,
	AppDomain_InternalGetDefaultContext_m2984334519,
	AppDomain_InternalGetProcessGuid_m4049533258,
	AppDomain_GetProcessGuid_m246918367,
	AppDomain_ToString_m2545816157,
	AppDomain_DoTypeResolve_m3724057253,
	AppDomainInitializer__ctor_m1409537730,
	AppDomainInitializer_Invoke_m1518556050,
	AppDomainInitializer_BeginInvoke_m3107563474,
	AppDomainInitializer_EndInvoke_m1844416365,
	AppDomainSetup__ctor_m2376813191,
	ApplicationException__ctor_m2375465202,
	ApplicationException__ctor_m3389601759,
	ApplicationException__ctor_m1140634725,
	ApplicationIdentity_ToString_m3720773450,
	ArgIterator_Equals_m2080485439_AdjustorThunk,
	ArgIterator_GetHashCode_m3119876063_AdjustorThunk,
	ArgumentException__ctor_m996376509,
	ArgumentException__ctor_m775170528,
	ArgumentException__ctor_m59237838,
	ArgumentException__ctor_m2239500235,
	ArgumentException__ctor_m2550830142,
	ArgumentException__ctor_m2187783241,
	ArgumentException_get_ParamName_m2634504495,
	ArgumentException_get_Message_m2068856061,
	ArgumentNullException__ctor_m348650132,
	ArgumentNullException__ctor_m2607242403,
	ArgumentNullException__ctor_m532419484,
	ArgumentNullException__ctor_m367278376,
	ArgumentOutOfRangeException__ctor_m3684908528,
	ArgumentOutOfRangeException__ctor_m2829693980,
	ArgumentOutOfRangeException__ctor_m1438035441,
	ArgumentOutOfRangeException__ctor_m2416666627,
	ArgumentOutOfRangeException__ctor_m3822258770,
	ArgumentOutOfRangeException_get_Message_m373040981,
	ArithmeticException__ctor_m3165890130,
	ArithmeticException__ctor_m2007013933,
	ArithmeticException__ctor_m1134843527,
	Array__ctor_m3029133333,
	Array_System_Collections_IList_get_Item_m3019299456,
	Array_System_Collections_IList_set_Item_m3504915579,
	Array_System_Collections_IList_Add_m991819160,
	Array_System_Collections_IList_Clear_m2159605959,
	Array_System_Collections_IList_Contains_m3113295757,
	Array_System_Collections_IList_IndexOf_m523778419,
	Array_System_Collections_IList_Insert_m2028037964,
	Array_System_Collections_IList_Remove_m912497296,
	Array_System_Collections_IList_RemoveAt_m866728844,
	Array_System_Collections_ICollection_get_Count_m1623835074,
	Array_InternalArray__ICollection_get_Count_m1984938948,
	Array_InternalArray__ICollection_get_IsReadOnly_m179114617,
	Array_InternalArray__ICollection_Clear_m3667028958,
	Array_InternalArray__RemoveAt_m2893298614,
	Array_get_Length_m1201869234,
	Array_get_LongLength_m63201949,
	Array_get_Rank_m3141701998,
	Array_GetRank_m1304432205,
	Array_GetLength_m3490892354,
	Array_GetLongLength_m2596545479,
	Array_GetLowerBound_m817811428,
	Array_GetValue_m1494120325,
	Array_SetValue_m3927045595,
	Array_GetValueImpl_m3982717948,
	Array_SetValueImpl_m1024233787,
	Array_FastCopy_m4105485745,
	Array_CreateInstanceImpl_m709568271,
	Array_get_IsSynchronized_m3379284387,
	Array_get_SyncRoot_m2565384370,
	Array_get_IsFixedSize_m693448514,
	Array_get_IsReadOnly_m3136842013,
	Array_GetEnumerator_m591799487,
	Array_GetUpperBound_m152106182,
	Array_GetValue_m610923591,
	Array_GetValue_m2708821274,
	Array_GetValue_m3357507770,
	Array_GetValue_m1430766714,
	Array_GetValue_m3155219782,
	Array_GetValue_m217652931,
	Array_SetValue_m1145170836,
	Array_SetValue_m3597386468,
	Array_SetValue_m1904644573,
	Array_SetValue_m2050226905,
	Array_SetValue_m2680909523,
	Array_SetValue_m682265580,
	Array_CreateInstance_m3751849812,
	Array_CreateInstance_m3124107544,
	Array_CreateInstance_m573036287,
	Array_CreateInstance_m666131206,
	Array_CreateInstance_m1673959785,
	Array_GetIntArray_m1192539842,
	Array_CreateInstance_m3882898988,
	Array_GetValue_m3754306304,
	Array_SetValue_m4108726102,
	Array_BinarySearch_m2098844084,
	Array_BinarySearch_m1298651580,
	Array_BinarySearch_m2500870911,
	Array_BinarySearch_m1629142740,
	Array_DoBinarySearch_m199061391,
	Array_Clear_m305290227,
	Array_ClearInternal_m2225062862,
	Array_Clone_m1115294170,
	Array_Copy_m392395275,
	Array_Copy_m1287631566,
	Array_Copy_m2652655318,
	Array_Copy_m3698620115,
	Array_IndexOf_m3436793169,
	Array_IndexOf_m3442506776,
	Array_IndexOf_m1260278658,
	Array_Initialize_m486260706,
	Array_LastIndexOf_m827387485,
	Array_LastIndexOf_m362377999,
	Array_LastIndexOf_m234007524,
	Array_get_swapper_m168880643,
	Array_Reverse_m3921569028,
	Array_Reverse_m3946279211,
	Array_Sort_m3032378294,
	Array_Sort_m243170594,
	Array_Sort_m1166375231,
	Array_Sort_m1504758793,
	Array_Sort_m2496372837,
	Array_Sort_m2232750079,
	Array_Sort_m2717880702,
	Array_Sort_m2668013149,
	Array_int_swapper_m683323463,
	Array_obj_swapper_m619320533,
	Array_slow_swapper_m285402653,
	Array_double_swapper_m1990147288,
	Array_new_gap_m3865331624,
	Array_combsort_m4188724368,
	Array_combsort_m6485776,
	Array_combsort_m1932213202,
	Array_qsort_m2839882233,
	Array_swap_m3590469174,
	Array_compare_m1305042596,
	Array_CopyTo_m332099161,
	Array_CopyTo_m657485551,
	Array_ConstrainedCopy_m1738878172,
	SimpleEnumerator__ctor_m3957398836,
	SimpleEnumerator_get_Current_m2189869981,
	SimpleEnumerator_MoveNext_m3424188367,
	SimpleEnumerator_Reset_m569135910,
	SimpleEnumerator_Clone_m1124827893,
	Swapper__ctor_m3765048366,
	Swapper_Invoke_m1478879831,
	Swapper_BeginInvoke_m2144994511,
	Swapper_EndInvoke_m539706856,
	ArrayTypeMismatchException__ctor_m3940730502,
	ArrayTypeMismatchException__ctor_m2955763082,
	ArrayTypeMismatchException__ctor_m847384952,
	AssemblyLoadEventHandler__ctor_m1692116705,
	AssemblyLoadEventHandler_Invoke_m3765877321,
	AssemblyLoadEventHandler_BeginInvoke_m368817848,
	AssemblyLoadEventHandler_EndInvoke_m240517751,
	AsyncCallback__ctor_m242081609,
	AsyncCallback_Invoke_m2380865724,
	AsyncCallback_BeginInvoke_m188169670,
	AsyncCallback_EndInvoke_m2524004693,
	Attribute__ctor_m2897993467,
	Attribute_CheckParameters_m2682049816,
	Attribute_GetCustomAttribute_m3805733629,
	Attribute_GetCustomAttribute_m135430056,
	Attribute_GetHashCode_m2188337821,
	Attribute_IsDefined_m1167411862,
	Attribute_IsDefined_m936380774,
	Attribute_IsDefined_m3771639299,
	Attribute_IsDefined_m2408875580,
	Attribute_Equals_m4006452586,
	AttributeUsageAttribute__ctor_m1170715599,
	AttributeUsageAttribute_get_AllowMultiple_m112078363,
	AttributeUsageAttribute_set_AllowMultiple_m1313295265,
	AttributeUsageAttribute_get_Inherited_m1370482972,
	AttributeUsageAttribute_set_Inherited_m2205070461,
	BitConverter__cctor_m3477343659,
	BitConverter_AmILittleEndian_m1543963744,
	BitConverter_DoubleWordsAreSwapped_m4151660403,
	BitConverter_DoubleToInt64Bits_m1839328798,
	BitConverter_GetBytes_m639363113,
	BitConverter_GetBytes_m927302127,
	BitConverter_PutBytes_m3569650077,
	BitConverter_ToInt64_m2454337822,
	BitConverter_ToString_m1278078580,
	BitConverter_ToString_m3550237875,
	Boolean__cctor_m1118817891,
	Boolean_System_IConvertible_ToType_m1911714260_AdjustorThunk,
	Boolean_System_IConvertible_ToBoolean_m3968084210_AdjustorThunk,
	Boolean_System_IConvertible_ToByte_m2164451010_AdjustorThunk,
	Boolean_System_IConvertible_ToChar_m172360724_AdjustorThunk,
	Boolean_System_IConvertible_ToDateTime_m420405767_AdjustorThunk,
	Boolean_System_IConvertible_ToDecimal_m3994102133_AdjustorThunk,
	Boolean_System_IConvertible_ToDouble_m3984970665_AdjustorThunk,
	Boolean_System_IConvertible_ToInt16_m1273677989_AdjustorThunk,
	Boolean_System_IConvertible_ToInt32_m3450098220_AdjustorThunk,
	Boolean_System_IConvertible_ToInt64_m3901326996_AdjustorThunk,
	Boolean_System_IConvertible_ToSByte_m2808560259_AdjustorThunk,
	Boolean_System_IConvertible_ToSingle_m3283266050_AdjustorThunk,
	Boolean_System_IConvertible_ToUInt16_m1513902759_AdjustorThunk,
	Boolean_System_IConvertible_ToUInt32_m2868542407_AdjustorThunk,
	Boolean_System_IConvertible_ToUInt64_m1116486545_AdjustorThunk,
	Boolean_CompareTo_m2978809512_AdjustorThunk,
	Boolean_Equals_m870298236_AdjustorThunk,
	Boolean_CompareTo_m384261152_AdjustorThunk,
	Boolean_Equals_m67531367_AdjustorThunk,
	Boolean_GetHashCode_m1880341182_AdjustorThunk,
	Boolean_Parse_m2649862989,
	Boolean_ToString_m3488188482_AdjustorThunk,
	Boolean_ToString_m2245119515_AdjustorThunk,
	Buffer_ByteLength_m2749019809,
	Buffer_BlockCopy_m3811116228,
	Buffer_ByteLengthInternal_m1933246543,
	Buffer_BlockCopyInternal_m2382420234,
	Byte_System_IConvertible_ToType_m88250113_AdjustorThunk,
	Byte_System_IConvertible_ToBoolean_m3245241636_AdjustorThunk,
	Byte_System_IConvertible_ToByte_m4110595847_AdjustorThunk,
	Byte_System_IConvertible_ToChar_m4280618669_AdjustorThunk,
	Byte_System_IConvertible_ToDateTime_m1684858944_AdjustorThunk,
	Byte_System_IConvertible_ToDecimal_m3821981855_AdjustorThunk,
	Byte_System_IConvertible_ToDouble_m3197564149_AdjustorThunk,
	Byte_System_IConvertible_ToInt16_m2803371991_AdjustorThunk,
	Byte_System_IConvertible_ToInt32_m2431199899_AdjustorThunk,
	Byte_System_IConvertible_ToInt64_m278954931_AdjustorThunk,
	Byte_System_IConvertible_ToSByte_m1717368897_AdjustorThunk,
	Byte_System_IConvertible_ToSingle_m1632252365_AdjustorThunk,
	Byte_System_IConvertible_ToUInt16_m1350916608_AdjustorThunk,
	Byte_System_IConvertible_ToUInt32_m4179074805_AdjustorThunk,
	Byte_System_IConvertible_ToUInt64_m1816849640_AdjustorThunk,
	Byte_CompareTo_m4289415996_AdjustorThunk,
	Byte_Equals_m737883369_AdjustorThunk,
	Byte_GetHashCode_m4191668055_AdjustorThunk,
	Byte_CompareTo_m223998094_AdjustorThunk,
	Byte_Equals_m3117312609_AdjustorThunk,
	Byte_Parse_m1111312940,
	Byte_Parse_m1682496037,
	Byte_Parse_m1988164567,
	Byte_TryParse_m939492826,
	Byte_TryParse_m2940174653,
	Byte_ToString_m1557822775_AdjustorThunk,
	Byte_ToString_m2608387306_AdjustorThunk,
	Byte_ToString_m1549340082_AdjustorThunk,
	Byte_ToString_m3614819476_AdjustorThunk,
	Char__cctor_m1102732931,
	Char_System_IConvertible_ToType_m2492435064_AdjustorThunk,
	Char_System_IConvertible_ToBoolean_m804699894_AdjustorThunk,
	Char_System_IConvertible_ToByte_m3404443014_AdjustorThunk,
	Char_System_IConvertible_ToChar_m3017032234_AdjustorThunk,
	Char_System_IConvertible_ToDateTime_m773762781_AdjustorThunk,
	Char_System_IConvertible_ToDecimal_m2171301353_AdjustorThunk,
	Char_System_IConvertible_ToDouble_m4144442232_AdjustorThunk,
	Char_System_IConvertible_ToInt16_m1057658865_AdjustorThunk,
	Char_System_IConvertible_ToInt32_m2846754021_AdjustorThunk,
	Char_System_IConvertible_ToInt64_m2958737617_AdjustorThunk,
	Char_System_IConvertible_ToSByte_m4109623252_AdjustorThunk,
	Char_System_IConvertible_ToSingle_m3590465548_AdjustorThunk,
	Char_System_IConvertible_ToUInt16_m2694042926_AdjustorThunk,
	Char_System_IConvertible_ToUInt32_m386846991_AdjustorThunk,
	Char_System_IConvertible_ToUInt64_m3445554083_AdjustorThunk,
	Char_GetDataTablePointers_m2369532848,
	Char_CompareTo_m1319704274_AdjustorThunk,
	Char_Equals_m3975975134_AdjustorThunk,
	Char_CompareTo_m1094532493_AdjustorThunk,
	Char_Equals_m2148257832_AdjustorThunk,
	Char_GetHashCode_m1812214884_AdjustorThunk,
	Char_GetUnicodeCategory_m2960412812,
	Char_IsDigit_m2282338421,
	Char_IsLetter_m4005249031,
	Char_IsLetterOrDigit_m537099147,
	Char_IsLower_m4136845248,
	Char_IsSurrogate_m2311845190,
	Char_IsWhiteSpace_m3221414563,
	Char_IsWhiteSpace_m1530390784,
	Char_CheckParameter_m132596558,
	Char_Parse_m3752310711,
	Char_ToLower_m3379788551,
	Char_ToLowerInvariant_m726107324,
	Char_ToLower_m1387747883,
	Char_ToUpper_m295432202,
	Char_ToUpperInvariant_m1232897003,
	Char_ToString_m2639631148_AdjustorThunk,
	Char_ToString_m3751942386_AdjustorThunk,
	CharEnumerator__ctor_m1229783824,
	CharEnumerator_System_Collections_IEnumerator_get_Current_m1145526303,
	CharEnumerator_System_IDisposable_Dispose_m3098382966,
	CharEnumerator_get_Current_m1662192463,
	CharEnumerator_Clone_m1869712149,
	CharEnumerator_MoveNext_m3514895061,
	CharEnumerator_Reset_m42259193,
	CLSCompliantAttribute__ctor_m3885160237,
	ArrayList__ctor_m2794524866,
	ArrayList__ctor_m1887728087,
	ArrayList__ctor_m154510267,
	ArrayList__ctor_m4194149953,
	ArrayList__cctor_m4132398432,
	ArrayList_get_Item_m2930495086,
	ArrayList_set_Item_m2576483835,
	ArrayList_get_Count_m887745903,
	ArrayList_get_Capacity_m4080213801,
	ArrayList_set_Capacity_m3248783950,
	ArrayList_get_IsReadOnly_m973064083,
	ArrayList_get_IsSynchronized_m3511241072,
	ArrayList_get_SyncRoot_m2760316478,
	ArrayList_EnsureCapacity_m4174950996,
	ArrayList_Shift_m1776187330,
	ArrayList_Add_m1032699014,
	ArrayList_Clear_m1311080298,
	ArrayList_Contains_m4195983465,
	ArrayList_IndexOf_m883124782,
	ArrayList_IndexOf_m2275492146,
	ArrayList_IndexOf_m3703138119,
	ArrayList_Insert_m1094798812,
	ArrayList_InsertRange_m2253703207,
	ArrayList_Remove_m799697853,
	ArrayList_RemoveAt_m3690146969,
	ArrayList_CopyTo_m757982293,
	ArrayList_CopyTo_m2576273666,
	ArrayList_CopyTo_m2945543338,
	ArrayList_GetEnumerator_m487894342,
	ArrayList_AddRange_m2663779696,
	ArrayList_Sort_m2874495278,
	ArrayList_Sort_m886700160,
	ArrayList_ToArray_m727107442,
	ArrayList_ToArray_m3807389525,
	ArrayList_Clone_m3646965770,
	ArrayList_ThrowNewArgumentOutOfRangeException_m3066565130,
	ArrayList_Synchronized_m3794807713,
	ArrayList_ReadOnly_m540923450,
	ArrayListWrapper__ctor_m4026520920,
	ArrayListWrapper_get_Item_m2058811544,
	ArrayListWrapper_set_Item_m813945246,
	ArrayListWrapper_get_Count_m780777509,
	ArrayListWrapper_get_Capacity_m2286970773,
	ArrayListWrapper_set_Capacity_m1257029602,
	ArrayListWrapper_get_IsReadOnly_m1675681686,
	ArrayListWrapper_get_IsSynchronized_m169618214,
	ArrayListWrapper_get_SyncRoot_m1638586154,
	ArrayListWrapper_Add_m2263171382,
	ArrayListWrapper_Clear_m3915659884,
	ArrayListWrapper_Contains_m3377454897,
	ArrayListWrapper_IndexOf_m2599761010,
	ArrayListWrapper_IndexOf_m1717139328,
	ArrayListWrapper_IndexOf_m3471093427,
	ArrayListWrapper_Insert_m3262528890,
	ArrayListWrapper_InsertRange_m2133272360,
	ArrayListWrapper_Remove_m3546682274,
	ArrayListWrapper_RemoveAt_m1795282462,
	ArrayListWrapper_CopyTo_m1200013472,
	ArrayListWrapper_CopyTo_m1723112735,
	ArrayListWrapper_CopyTo_m2793294031,
	ArrayListWrapper_GetEnumerator_m3997423037,
	ArrayListWrapper_AddRange_m1210497169,
	ArrayListWrapper_Clone_m4220701673,
	ArrayListWrapper_Sort_m3808628793,
	ArrayListWrapper_Sort_m3407883998,
	ArrayListWrapper_ToArray_m3335508598,
	ArrayListWrapper_ToArray_m441257990,
	FixedSizeArrayListWrapper__ctor_m3391186433,
	FixedSizeArrayListWrapper_get_ErrorMessage_m336185562,
	FixedSizeArrayListWrapper_get_Capacity_m2425305815,
	FixedSizeArrayListWrapper_set_Capacity_m538834318,
	FixedSizeArrayListWrapper_Add_m664694773,
	FixedSizeArrayListWrapper_AddRange_m3736495698,
	FixedSizeArrayListWrapper_Clear_m678082358,
	FixedSizeArrayListWrapper_Insert_m1499567299,
	FixedSizeArrayListWrapper_InsertRange_m1361439377,
	FixedSizeArrayListWrapper_Remove_m194226442,
	FixedSizeArrayListWrapper_RemoveAt_m283739216,
	ReadOnlyArrayListWrapper__ctor_m1765923926,
	ReadOnlyArrayListWrapper_get_ErrorMessage_m1735536626,
	ReadOnlyArrayListWrapper_get_IsReadOnly_m2043964128,
	ReadOnlyArrayListWrapper_get_Item_m736811773,
	ReadOnlyArrayListWrapper_set_Item_m1018028581,
	ReadOnlyArrayListWrapper_Sort_m2106818998,
	ReadOnlyArrayListWrapper_Sort_m3997210685,
	SimpleEnumerator__ctor_m3507490065,
	SimpleEnumerator__cctor_m4103124766,
	SimpleEnumerator_Clone_m3144817616,
	SimpleEnumerator_MoveNext_m1877966209,
	SimpleEnumerator_get_Current_m695847894,
	SimpleEnumerator_Reset_m601343442,
	SynchronizedArrayListWrapper__ctor_m2601384036,
	SynchronizedArrayListWrapper_get_Item_m1962441151,
	SynchronizedArrayListWrapper_set_Item_m2014675391,
	SynchronizedArrayListWrapper_get_Count_m2036547373,
	SynchronizedArrayListWrapper_get_Capacity_m3829125592,
	SynchronizedArrayListWrapper_set_Capacity_m1655621538,
	SynchronizedArrayListWrapper_get_IsReadOnly_m1029404986,
	SynchronizedArrayListWrapper_get_IsSynchronized_m3728150270,
	SynchronizedArrayListWrapper_get_SyncRoot_m2309850440,
	SynchronizedArrayListWrapper_Add_m1538663151,
	SynchronizedArrayListWrapper_Clear_m1283139032,
	SynchronizedArrayListWrapper_Contains_m2913803557,
	SynchronizedArrayListWrapper_IndexOf_m2677505790,
	SynchronizedArrayListWrapper_IndexOf_m2939937472,
	SynchronizedArrayListWrapper_IndexOf_m3214999213,
	SynchronizedArrayListWrapper_Insert_m1479935893,
	SynchronizedArrayListWrapper_InsertRange_m122501967,
	SynchronizedArrayListWrapper_Remove_m1131132228,
	SynchronizedArrayListWrapper_RemoveAt_m3605945430,
	SynchronizedArrayListWrapper_CopyTo_m1089777810,
	SynchronizedArrayListWrapper_CopyTo_m1685175620,
	SynchronizedArrayListWrapper_CopyTo_m1755929818,
	SynchronizedArrayListWrapper_GetEnumerator_m1244012135,
	SynchronizedArrayListWrapper_AddRange_m3358444564,
	SynchronizedArrayListWrapper_Clone_m1224772954,
	SynchronizedArrayListWrapper_Sort_m1477226622,
	SynchronizedArrayListWrapper_Sort_m1118104978,
	SynchronizedArrayListWrapper_ToArray_m2041291651,
	SynchronizedArrayListWrapper_ToArray_m4286441432,
	BitArray__ctor_m1601208956,
	BitArray__ctor_m2858832404,
	BitArray_getByte_m2751132649,
	BitArray_get_Count_m1638551981,
	BitArray_get_Item_m2283229009,
	BitArray_set_Item_m2424022219,
	BitArray_get_Length_m3419123068,
	BitArray_get_SyncRoot_m3960743097,
	BitArray_Clone_m1167667406,
	BitArray_CopyTo_m862538104,
	BitArray_Get_m669637547,
	BitArray_Set_m936522109,
	BitArray_GetEnumerator_m1592407388,
	BitArrayEnumerator__ctor_m4116091493,
	BitArrayEnumerator_Clone_m3844344686,
	BitArrayEnumerator_get_Current_m1633960344,
	BitArrayEnumerator_MoveNext_m298769991,
	BitArrayEnumerator_Reset_m2396240084,
	BitArrayEnumerator_checkVersion_m3010158598,
	CaseInsensitiveComparer__ctor_m1369638598,
	CaseInsensitiveComparer__ctor_m2570295078,
	CaseInsensitiveComparer__cctor_m2724493792,
	CaseInsensitiveComparer_get_DefaultInvariant_m2201964174,
	CaseInsensitiveComparer_Compare_m1348555498,
	CaseInsensitiveHashCodeProvider__ctor_m3591252511,
	CaseInsensitiveHashCodeProvider__ctor_m256093159,
	CaseInsensitiveHashCodeProvider__cctor_m2915843407,
	CaseInsensitiveHashCodeProvider_AreEqual_m2144889497,
	CaseInsensitiveHashCodeProvider_AreEqual_m1004254695,
	CaseInsensitiveHashCodeProvider_get_DefaultInvariant_m3219219398,
	CaseInsensitiveHashCodeProvider_GetHashCode_m2875073,
	CollectionBase__ctor_m3253555188,
	CollectionBase_System_Collections_ICollection_CopyTo_m2572720456,
	CollectionBase_System_Collections_ICollection_get_SyncRoot_m2606609939,
	CollectionBase_System_Collections_IList_Add_m134587319,
	CollectionBase_System_Collections_IList_Contains_m675999865,
	CollectionBase_System_Collections_IList_IndexOf_m3097638141,
	CollectionBase_System_Collections_IList_Insert_m583839462,
	CollectionBase_System_Collections_IList_Remove_m3085003941,
	CollectionBase_System_Collections_IList_get_Item_m1763001231,
	CollectionBase_System_Collections_IList_set_Item_m1574988689,
	CollectionBase_get_Count_m13611932,
	CollectionBase_GetEnumerator_m2711342643,
	CollectionBase_Clear_m3499885467,
	CollectionBase_RemoveAt_m860391196,
	CollectionBase_get_InnerList_m428229921,
	CollectionBase_get_List_m2159943927,
	CollectionBase_OnClear_m2583786747,
	CollectionBase_OnClearComplete_m3700619957,
	CollectionBase_OnInsert_m1665034841,
	CollectionBase_OnInsertComplete_m1401033198,
	CollectionBase_OnRemove_m345705727,
	CollectionBase_OnRemoveComplete_m3039635368,
	CollectionBase_OnSet_m2841116004,
	CollectionBase_OnSetComplete_m3196194211,
	CollectionBase_OnValidate_m1340864948,
	Comparer__ctor_m1347258397,
	Comparer__ctor_m2319474396,
	Comparer__cctor_m3696268315,
	Comparer_Compare_m736952830,
	DictionaryEntry__ctor_m3773202887_AdjustorThunk,
	DictionaryEntry_get_Key_m3364857942_AdjustorThunk,
	DictionaryEntry_get_Value_m3120388264_AdjustorThunk,
	KeyNotFoundException__ctor_m3628075698,
	KeyNotFoundException__ctor_m2109244851,
	KeyNotFoundException__ctor_m1802636253,
	Hashtable__ctor_m1143234103,
	Hashtable__ctor_m3292572634,
	Hashtable__ctor_m2612894583,
	Hashtable__ctor_m1589851152,
	Hashtable__ctor_m1818784074,
	Hashtable__ctor_m1202039326,
	Hashtable__ctor_m132973949,
	Hashtable__ctor_m347947491,
	Hashtable__ctor_m1435436894,
	Hashtable__ctor_m188879612,
	Hashtable__ctor_m505401515,
	Hashtable__ctor_m591103269,
	Hashtable__cctor_m2552068052,
	Hashtable_System_Collections_IEnumerable_GetEnumerator_m1972518728,
	Hashtable_set_comparer_m2837928355,
	Hashtable_set_hcp_m2961569771,
	Hashtable_get_Count_m78354934,
	Hashtable_get_SyncRoot_m2040127349,
	Hashtable_get_Keys_m2064563248,
	Hashtable_get_Values_m2272493742,
	Hashtable_get_Item_m235193985,
	Hashtable_set_Item_m1884260023,
	Hashtable_CopyTo_m4282595219,
	Hashtable_Add_m3165682791,
	Hashtable_Clear_m2559356064,
	Hashtable_Contains_m1437756016,
	Hashtable_GetEnumerator_m1856328481,
	Hashtable_Remove_m3981333743,
	Hashtable_ContainsKey_m3984503069,
	Hashtable_Clone_m4285620339,
	Hashtable_OnDeserialization_m3313245466,
	Hashtable_Synchronized_m2127876118,
	Hashtable_GetHash_m2417611000,
	Hashtable_KeyEquals_m94467689,
	Hashtable_AdjustThreshold_m4104561679,
	Hashtable_SetTable_m3625255072,
	Hashtable_Find_m2068965260,
	Hashtable_Rehash_m3009677313,
	Hashtable_PutImpl_m2943660476,
	Hashtable_CopyToArray_m3105490612,
	Hashtable_TestPrime_m706506726,
	Hashtable_CalcPrime_m851781627,
	Hashtable_ToPrime_m3167575262,
	Enumerator__ctor_m1432194560,
	Enumerator__cctor_m4285618846,
	Enumerator_FailFast_m4030123936,
	Enumerator_Reset_m2495282333,
	Enumerator_MoveNext_m2287774590,
	Enumerator_get_Entry_m1071522276,
	Enumerator_get_Key_m1593951509,
	Enumerator_get_Value_m3434206522,
	Enumerator_get_Current_m225470999,
	HashKeys__ctor_m3940316379,
	HashKeys_get_Count_m3449693751,
	HashKeys_get_SyncRoot_m3523836037,
	HashKeys_CopyTo_m2920154361,
	HashKeys_GetEnumerator_m4177351206,
	HashValues__ctor_m1391954076,
	HashValues_get_Count_m3804746689,
	HashValues_get_SyncRoot_m950352266,
	HashValues_CopyTo_m3240300716,
	HashValues_GetEnumerator_m2443659244,
	KeyMarker__ctor_m36625516,
	KeyMarker__cctor_m3640669474,
	SyncHashtable__ctor_m961400313,
	SyncHashtable__ctor_m2131022655,
	SyncHashtable_System_Collections_IEnumerable_GetEnumerator_m3586725612,
	SyncHashtable_get_Count_m3568720318,
	SyncHashtable_get_SyncRoot_m538137672,
	SyncHashtable_get_Keys_m991828786,
	SyncHashtable_get_Values_m3195252010,
	SyncHashtable_get_Item_m4185623069,
	SyncHashtable_set_Item_m2480293295,
	SyncHashtable_CopyTo_m1791448066,
	SyncHashtable_Add_m1878706264,
	SyncHashtable_Clear_m1362680156,
	SyncHashtable_Contains_m1954982520,
	SyncHashtable_GetEnumerator_m573170284,
	SyncHashtable_Remove_m2169965663,
	SyncHashtable_ContainsKey_m3862269555,
	SyncHashtable_Clone_m1436799670,
	SortedList__ctor_m3601209121,
	SortedList__ctor_m2023168972,
	SortedList__ctor_m2742363376,
	SortedList__ctor_m2926868609,
	SortedList__cctor_m2457773694,
	SortedList_System_Collections_IEnumerable_GetEnumerator_m2253009252,
	SortedList_get_Count_m2004715360,
	SortedList_get_SyncRoot_m4186691777,
	SortedList_get_IsFixedSize_m4019735192,
	SortedList_get_IsReadOnly_m2947941470,
	SortedList_get_Item_m2537724296,
	SortedList_set_Item_m808115396,
	SortedList_get_Capacity_m3176686970,
	SortedList_set_Capacity_m823537961,
	SortedList_Add_m2593684945,
	SortedList_Contains_m1266529256,
	SortedList_GetEnumerator_m191943740,
	SortedList_Remove_m3721013730,
	SortedList_CopyTo_m1731250790,
	SortedList_Clone_m761330528,
	SortedList_RemoveAt_m1338265277,
	SortedList_IndexOfKey_m17048843,
	SortedList_ContainsKey_m1346463948,
	SortedList_GetByIndex_m2411263875,
	SortedList_EnsureCapacity_m2515702407,
	SortedList_PutImpl_m340229810,
	SortedList_GetImpl_m4211164780,
	SortedList_InitTable_m3816943775,
	SortedList_Find_m3346310779,
	Enumerator__ctor_m56802941,
	Enumerator__cctor_m3687757050,
	Enumerator_Reset_m956450938,
	Enumerator_MoveNext_m630822851,
	Enumerator_get_Entry_m4266623009,
	Enumerator_get_Key_m4004039148,
	Enumerator_get_Value_m1871689937,
	Enumerator_get_Current_m2385427688,
	Enumerator_Clone_m1679634913,
	Stack__ctor_m4014281920,
	Stack__ctor_m682860280,
	Stack__ctor_m2632145311,
	Stack_Resize_m2616541472,
	Stack_get_Count_m3521085062,
	Stack_get_SyncRoot_m332712568,
	Stack_Clear_m1096186364,
	Stack_Clone_m3820343942,
	Stack_CopyTo_m204066610,
	Stack_GetEnumerator_m53775305,
	Stack_Peek_m1198535650,
	Stack_Pop_m4067695422,
	Stack_Push_m3591675239,
	Enumerator__ctor_m762187280,
	Enumerator_Clone_m1465212645,
	Enumerator_get_Current_m447003484,
	Enumerator_MoveNext_m552838171,
	Enumerator_Reset_m2987661950,
	Console__cctor_m4079772084,
	Console_SetEncodings_m1077977921,
	Console_get_Error_m1762483787,
	Console_Open_m3628545284,
	Console_OpenStandardError_m3305110800,
	Console_OpenStandardInput_m4006248948,
	Console_OpenStandardOutput_m1736171620,
	Console_SetOut_m1821852987,
	ContextBoundObject__ctor_m434475057,
	Convert__cctor_m1126066426,
	Convert_InternalFromBase64String_m2973573730,
	Convert_FromBase64String_m3889535293,
	Convert_ToBase64String_m3960133172,
	Convert_ToBase64String_m1698956971,
	Convert_ToBoolean_m181196939,
	Convert_ToBoolean_m3304463844,
	Convert_ToBoolean_m2066372582,
	Convert_ToBoolean_m1092989533,
	Convert_ToBoolean_m3291433592,
	Convert_ToBoolean_m3406248544,
	Convert_ToBoolean_m3849780071,
	Convert_ToBoolean_m815578138,
	Convert_ToBoolean_m3560274087,
	Convert_ToBoolean_m1821365203,
	Convert_ToBoolean_m3541395848,
	Convert_ToBoolean_m3686882876,
	Convert_ToBoolean_m2812258067,
	Convert_ToBoolean_m3024948542,
	Convert_ToByte_m1711335136,
	Convert_ToByte_m1359084456,
	Convert_ToByte_m4157197134,
	Convert_ToByte_m3676785831,
	Convert_ToByte_m866306097,
	Convert_ToByte_m1522154692,
	Convert_ToByte_m3415664427,
	Convert_ToByte_m1998596307,
	Convert_ToByte_m3660634296,
	Convert_ToByte_m3362055950,
	Convert_ToByte_m120001947,
	Convert_ToByte_m627183151,
	Convert_ToByte_m1655664624,
	Convert_ToByte_m2693208695,
	Convert_ToByte_m301731779,
	Convert_ToChar_m2909185475,
	Convert_ToChar_m1363087353,
	Convert_ToChar_m3767754668,
	Convert_ToChar_m2197668031,
	Convert_ToChar_m1651289017,
	Convert_ToChar_m769872090,
	Convert_ToChar_m1586478606,
	Convert_ToChar_m263958929,
	Convert_ToChar_m2065393292,
	Convert_ToChar_m1385558794,
	Convert_ToChar_m3616271797,
	Convert_ToDateTime_m3346118083,
	Convert_ToDateTime_m4192135626,
	Convert_ToDateTime_m4188556608,
	Convert_ToDateTime_m355355565,
	Convert_ToDateTime_m3877313006,
	Convert_ToDateTime_m2904116687,
	Convert_ToDateTime_m1240677581,
	Convert_ToDateTime_m2178216354,
	Convert_ToDateTime_m1691902540,
	Convert_ToDateTime_m1899241014,
	Convert_ToDecimal_m1907780385,
	Convert_ToDecimal_m2813599902,
	Convert_ToDecimal_m1028642325,
	Convert_ToDecimal_m3104762561,
	Convert_ToDecimal_m3602659554,
	Convert_ToDecimal_m759259367,
	Convert_ToDecimal_m3435225172,
	Convert_ToDecimal_m2105281924,
	Convert_ToDecimal_m4051675827,
	Convert_ToDecimal_m4173734781,
	Convert_ToDecimal_m4051859286,
	Convert_ToDecimal_m1174043729,
	Convert_ToDecimal_m1566524000,
	Convert_ToDecimal_m1342054916,
	Convert_ToDouble_m1759377896,
	Convert_ToDouble_m676071452,
	Convert_ToDouble_m359663363,
	Convert_ToDouble_m206312034,
	Convert_ToDouble_m1940577304,
	Convert_ToDouble_m3071019606,
	Convert_ToDouble_m358816045,
	Convert_ToDouble_m3582443749,
	Convert_ToDouble_m3605398720,
	Convert_ToDouble_m964392636,
	Convert_ToDouble_m3414375849,
	Convert_ToDouble_m1338516771,
	Convert_ToDouble_m1798060040,
	Convert_ToDouble_m1957959055,
	Convert_ToInt16_m3614160131,
	Convert_ToInt16_m2309364880,
	Convert_ToInt16_m3902136059,
	Convert_ToInt16_m1630070127,
	Convert_ToInt16_m3029401211,
	Convert_ToInt16_m2303614416,
	Convert_ToInt16_m1702084948,
	Convert_ToInt16_m4256038495,
	Convert_ToInt16_m1268881642,
	Convert_ToInt16_m2801254818,
	Convert_ToInt16_m3017105195,
	Convert_ToInt16_m4195031634,
	Convert_ToInt16_m3052677612,
	Convert_ToInt16_m4149411228,
	Convert_ToInt16_m1678654658,
	Convert_ToInt16_m1908371115,
	Convert_ToInt32_m1556403369,
	Convert_ToInt32_m732230018,
	Convert_ToInt32_m2208179825,
	Convert_ToInt32_m1333497284,
	Convert_ToInt32_m4262555422,
	Convert_ToInt32_m1535806496,
	Convert_ToInt32_m4078444369,
	Convert_ToInt32_m1206626159,
	Convert_ToInt32_m598318614,
	Convert_ToInt32_m3363061093,
	Convert_ToInt32_m2420320894,
	Convert_ToInt32_m575912531,
	Convert_ToInt32_m349566814,
	Convert_ToInt32_m1285495894,
	Convert_ToInt32_m189054182,
	Convert_ToInt64_m3812963483,
	Convert_ToInt64_m3641628104,
	Convert_ToInt64_m2621974401,
	Convert_ToInt64_m2143779060,
	Convert_ToInt64_m3660419282,
	Convert_ToInt64_m2984820389,
	Convert_ToInt64_m3216407986,
	Convert_ToInt64_m2741282783,
	Convert_ToInt64_m1063954313,
	Convert_ToInt64_m3844943708,
	Convert_ToInt64_m1328417215,
	Convert_ToInt64_m2740684590,
	Convert_ToInt64_m1102749412,
	Convert_ToInt64_m781390160,
	Convert_ToInt64_m2202161021,
	Convert_ToInt64_m2210211785,
	Convert_ToInt64_m2041309988,
	Convert_ToSByte_m863025055,
	Convert_ToSByte_m1485386302,
	Convert_ToSByte_m1209019644,
	Convert_ToSByte_m1294608859,
	Convert_ToSByte_m966005355,
	Convert_ToSByte_m2780065447,
	Convert_ToSByte_m3186677260,
	Convert_ToSByte_m763994626,
	Convert_ToSByte_m3414187568,
	Convert_ToSByte_m1378762269,
	Convert_ToSByte_m1804217813,
	Convert_ToSByte_m3354070798,
	Convert_ToSByte_m2415307335,
	Convert_ToSByte_m1950141290,
	Convert_ToSingle_m932543265,
	Convert_ToSingle_m3207606949,
	Convert_ToSingle_m4290228487,
	Convert_ToSingle_m188965971,
	Convert_ToSingle_m2332656787,
	Convert_ToSingle_m2789125164,
	Convert_ToSingle_m259335902,
	Convert_ToSingle_m1511457299,
	Convert_ToSingle_m913009971,
	Convert_ToSingle_m861086822,
	Convert_ToSingle_m3480326864,
	Convert_ToSingle_m2260413742,
	Convert_ToSingle_m3787160430,
	Convert_ToSingle_m2229731220,
	Convert_ToString_m94911272,
	Convert_ToString_m1537572958,
	Convert_ToUInt16_m3506724663,
	Convert_ToUInt16_m115274073,
	Convert_ToUInt16_m3490324414,
	Convert_ToUInt16_m3912085078,
	Convert_ToUInt16_m207185530,
	Convert_ToUInt16_m722491632,
	Convert_ToUInt16_m1222063852,
	Convert_ToUInt16_m4146807859,
	Convert_ToUInt16_m1828753276,
	Convert_ToUInt16_m2948714162,
	Convert_ToUInt16_m2011096748,
	Convert_ToUInt16_m418187698,
	Convert_ToUInt16_m3930851433,
	Convert_ToUInt16_m996906177,
	Convert_ToUInt32_m4249819190,
	Convert_ToUInt32_m823261086,
	Convert_ToUInt32_m3279699603,
	Convert_ToUInt32_m837934666,
	Convert_ToUInt32_m3154368742,
	Convert_ToUInt32_m3499511866,
	Convert_ToUInt32_m3902982566,
	Convert_ToUInt32_m1630022459,
	Convert_ToUInt32_m2888961780,
	Convert_ToUInt32_m3875914063,
	Convert_ToUInt32_m2993243305,
	Convert_ToUInt32_m2675516930,
	Convert_ToUInt32_m3219512886,
	Convert_ToUInt32_m731182432,
	Convert_ToUInt64_m2793460560,
	Convert_ToUInt64_m3441470682,
	Convert_ToUInt64_m651291461,
	Convert_ToUInt64_m1016581210,
	Convert_ToUInt64_m4062218541,
	Convert_ToUInt64_m1587419655,
	Convert_ToUInt64_m1283785253,
	Convert_ToUInt64_m3177931015,
	Convert_ToUInt64_m3453577592,
	Convert_ToUInt64_m371719104,
	Convert_ToUInt64_m1691859135,
	Convert_ToUInt64_m3212089508,
	Convert_ToUInt64_m1360931664,
	Convert_ToUInt64_m2801452660,
	Convert_ToUInt64_m3389700845,
	Convert_ChangeType_m3958886575,
	Convert_ToType_m2692801367,
	CultureAwareComparer__ctor_m2927916088,
	CultureAwareComparer_Compare_m2414611160,
	CultureAwareComparer_Equals_m1823500299,
	CultureAwareComparer_GetHashCode_m4059648712,
	CurrentSystemTimeZone__ctor_m2856852514,
	CurrentSystemTimeZone__ctor_m3263608395,
	CurrentSystemTimeZone_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m2860688338,
	CurrentSystemTimeZone_GetTimeZoneData_m907897977,
	CurrentSystemTimeZone_GetDaylightChanges_m3350100744,
	CurrentSystemTimeZone_GetUtcOffset_m1258737164,
	CurrentSystemTimeZone_OnDeserialization_m247697366,
	CurrentSystemTimeZone_GetDaylightTimeFromData_m3659587845,
	DateTime__ctor_m1162146585_AdjustorThunk,
	DateTime__ctor_m1469143154_AdjustorThunk,
	DateTime__ctor_m112945363_AdjustorThunk,
	DateTime__ctor_m1943919131_AdjustorThunk,
	DateTime__ctor_m3733363217_AdjustorThunk,
	DateTime__ctor_m128199834_AdjustorThunk,
	DateTime__ctor_m3814649173_AdjustorThunk,
	DateTime__cctor_m3687287262,
	DateTime_System_IConvertible_ToBoolean_m2129434697_AdjustorThunk,
	DateTime_System_IConvertible_ToByte_m349242609_AdjustorThunk,
	DateTime_System_IConvertible_ToChar_m649605060_AdjustorThunk,
	DateTime_System_IConvertible_ToDateTime_m807082907_AdjustorThunk,
	DateTime_System_IConvertible_ToDecimal_m2955402839_AdjustorThunk,
	DateTime_System_IConvertible_ToDouble_m2363360721_AdjustorThunk,
	DateTime_System_IConvertible_ToInt16_m1926912326_AdjustorThunk,
	DateTime_System_IConvertible_ToInt32_m2103877964_AdjustorThunk,
	DateTime_System_IConvertible_ToInt64_m4060354194_AdjustorThunk,
	DateTime_System_IConvertible_ToSByte_m1316244191_AdjustorThunk,
	DateTime_System_IConvertible_ToSingle_m2678801163_AdjustorThunk,
	DateTime_System_IConvertible_ToType_m250355434_AdjustorThunk,
	DateTime_System_IConvertible_ToUInt16_m4275199668_AdjustorThunk,
	DateTime_System_IConvertible_ToUInt32_m3504556377_AdjustorThunk,
	DateTime_System_IConvertible_ToUInt64_m3729346730_AdjustorThunk,
	DateTime_AbsoluteDays_m2581663874,
	DateTime_FromTicks_m612602014_AdjustorThunk,
	DateTime_get_Month_m3602563062_AdjustorThunk,
	DateTime_get_Day_m3723482842_AdjustorThunk,
	DateTime_get_DayOfWeek_m552950095_AdjustorThunk,
	DateTime_get_Hour_m3279703729_AdjustorThunk,
	DateTime_get_Minute_m2214880909_AdjustorThunk,
	DateTime_get_Second_m359294707_AdjustorThunk,
	DateTime_GetTimeMonotonic_m3035286822,
	DateTime_GetNow_m1798973790,
	DateTime_get_Now_m3197730460,
	DateTime_get_Ticks_m3634951895_AdjustorThunk,
	DateTime_get_Today_m3149731286,
	DateTime_get_UtcNow_m1166149790,
	DateTime_get_Year_m2465579539_AdjustorThunk,
	DateTime_get_Kind_m2353497618_AdjustorThunk,
	DateTime_Add_m82617556_AdjustorThunk,
	DateTime_AddTicks_m3326838593_AdjustorThunk,
	DateTime_AddMilliseconds_m2259736413_AdjustorThunk,
	DateTime_AddSeconds_m802575560_AdjustorThunk,
	DateTime_Compare_m3946132637,
	DateTime_CompareTo_m58681863_AdjustorThunk,
	DateTime_CompareTo_m2695414402_AdjustorThunk,
	DateTime_Equals_m2586536838_AdjustorThunk,
	DateTime_FromBinary_m3052269187,
	DateTime_SpecifyKind_m565564135,
	DateTime_DaysInMonth_m3804445593,
	DateTime_Equals_m2336408426_AdjustorThunk,
	DateTime_CheckDateTimeKind_m906499247_AdjustorThunk,
	DateTime_GetHashCode_m1398597643_AdjustorThunk,
	DateTime_IsLeapYear_m3908824970,
	DateTime_Parse_m1939827158,
	DateTime_Parse_m3813128948,
	DateTime_CoreParse_m337968780,
	DateTime_YearMonthDayFormats_m249964372,
	DateTime__ParseNumber_m4213919703,
	DateTime__ParseEnum_m2925373698,
	DateTime__ParseString_m4087287987,
	DateTime__ParseAmPm_m3930102169,
	DateTime__ParseTimeSeparator_m3385727945,
	DateTime__ParseDateSeparator_m4159984309,
	DateTime_IsLetter_m3492201953,
	DateTime__DoParse_m3124502042,
	DateTime_ParseExact_m567683756,
	DateTime_ParseExact_m1292664029,
	DateTime_CheckStyle_m1194998724,
	DateTime_ParseExact_m949581326,
	DateTime_Subtract_m355628981_AdjustorThunk,
	DateTime_ToString_m2872442757_AdjustorThunk,
	DateTime_ToString_m3415865025_AdjustorThunk,
	DateTime_ToString_m3913825546_AdjustorThunk,
	DateTime_ToLocalTime_m2237725792_AdjustorThunk,
	DateTime_ToUniversalTime_m688324665_AdjustorThunk,
	DateTime_op_Addition_m196538926,
	DateTime_op_Equality_m307014225,
	DateTime_op_GreaterThan_m3588908669,
	DateTime_op_GreaterThanOrEqual_m3282389234,
	DateTime_op_Inequality_m1476518596,
	DateTime_op_LessThan_m336891988,
	DateTime_op_LessThanOrEqual_m3737117479,
	DateTime_op_Subtraction_m2177242756,
	DateTimeOffset__ctor_m2507223661_AdjustorThunk,
	DateTimeOffset__ctor_m23940287_AdjustorThunk,
	DateTimeOffset__ctor_m1680326190_AdjustorThunk,
	DateTimeOffset__ctor_m947657984_AdjustorThunk,
	DateTimeOffset__cctor_m3359959350,
	DateTimeOffset_System_IComparable_CompareTo_m1808427407_AdjustorThunk,
	DateTimeOffset_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m2312432265_AdjustorThunk,
	DateTimeOffset_CompareTo_m1080840663_AdjustorThunk,
	DateTimeOffset_Equals_m4119379754_AdjustorThunk,
	DateTimeOffset_Equals_m3384866816_AdjustorThunk,
	DateTimeOffset_GetHashCode_m3032241378_AdjustorThunk,
	DateTimeOffset_ToString_m848849611_AdjustorThunk,
	DateTimeOffset_ToString_m3906072830_AdjustorThunk,
	DateTimeOffset_get_DateTime_m851552317_AdjustorThunk,
	DateTimeOffset_get_Offset_m1551860166_AdjustorThunk,
	DateTimeOffset_get_UtcDateTime_m2232550571_AdjustorThunk,
	DateTimeUtils_CountRepeat_m2243670922,
	DateTimeUtils_ZeroPad_m1269987479,
	DateTimeUtils_ParseQuotedString_m3545675658,
	DateTimeUtils_GetStandardPattern_m3224060921,
	DateTimeUtils_GetStandardPattern_m2938656100,
	DateTimeUtils_ToString_m3124505984,
	DateTimeUtils_ToString_m1521022922,
	DBNull__ctor_m1217818204,
	DBNull__ctor_m2976018176,
	DBNull__cctor_m3995179348,
	DBNull_System_IConvertible_ToBoolean_m3704708804,
	DBNull_System_IConvertible_ToByte_m1888019139,
	DBNull_System_IConvertible_ToChar_m559773690,
	DBNull_System_IConvertible_ToDateTime_m1427000021,
	DBNull_System_IConvertible_ToDecimal_m3607776546,
	DBNull_System_IConvertible_ToDouble_m886967017,
	DBNull_System_IConvertible_ToInt16_m2287362260,
	DBNull_System_IConvertible_ToInt32_m1074309452,
	DBNull_System_IConvertible_ToInt64_m2321239992,
	DBNull_System_IConvertible_ToSByte_m2073493841,
	DBNull_System_IConvertible_ToSingle_m368185513,
	DBNull_System_IConvertible_ToType_m3468123543,
	DBNull_System_IConvertible_ToUInt16_m298510550,
	DBNull_System_IConvertible_ToUInt32_m1371873998,
	DBNull_System_IConvertible_ToUInt64_m161428274,
	DBNull_ToString_m1261601810,
	DBNull_ToString_m3538632386,
	Decimal__ctor_m2606258124_AdjustorThunk,
	Decimal__ctor_m2814078077_AdjustorThunk,
	Decimal__ctor_m2684736918_AdjustorThunk,
	Decimal__ctor_m3672480254_AdjustorThunk,
	Decimal__ctor_m700533170_AdjustorThunk,
	Decimal__ctor_m3564855869_AdjustorThunk,
	Decimal__ctor_m1824806831_AdjustorThunk,
	Decimal__cctor_m3550966988,
	Decimal_System_IConvertible_ToType_m1469257424_AdjustorThunk,
	Decimal_System_IConvertible_ToBoolean_m3085764627_AdjustorThunk,
	Decimal_System_IConvertible_ToByte_m3781762660_AdjustorThunk,
	Decimal_System_IConvertible_ToChar_m2131880973_AdjustorThunk,
	Decimal_System_IConvertible_ToDateTime_m2162830290_AdjustorThunk,
	Decimal_System_IConvertible_ToDecimal_m3279028900_AdjustorThunk,
	Decimal_System_IConvertible_ToDouble_m2138187674_AdjustorThunk,
	Decimal_System_IConvertible_ToInt16_m2307666217_AdjustorThunk,
	Decimal_System_IConvertible_ToInt32_m4049826490_AdjustorThunk,
	Decimal_System_IConvertible_ToInt64_m4239752044_AdjustorThunk,
	Decimal_System_IConvertible_ToSByte_m3561412798_AdjustorThunk,
	Decimal_System_IConvertible_ToSingle_m222175769_AdjustorThunk,
	Decimal_System_IConvertible_ToUInt16_m11225590_AdjustorThunk,
	Decimal_System_IConvertible_ToUInt32_m4166707801_AdjustorThunk,
	Decimal_System_IConvertible_ToUInt64_m3027983897_AdjustorThunk,
	Decimal_GetBits_m2321028653,
	Decimal_Add_m3493327949,
	Decimal_Subtract_m3294185326,
	Decimal_GetHashCode_m76089080_AdjustorThunk,
	Decimal_u64_m862346687,
	Decimal_s64_m2192320321,
	Decimal_Equals_m4165660341,
	Decimal_Equals_m2099245511_AdjustorThunk,
	Decimal_IsZero_m648824807_AdjustorThunk,
	Decimal_Floor_m1365865268,
	Decimal_Multiply_m2733971897,
	Decimal_Divide_m3363200552,
	Decimal_Compare_m4142387002,
	Decimal_CompareTo_m2878181521_AdjustorThunk,
	Decimal_CompareTo_m2076266001_AdjustorThunk,
	Decimal_Equals_m422237635_AdjustorThunk,
	Decimal_Parse_m835038489,
	Decimal_ThrowAtPos_m2666100311,
	Decimal_ThrowInvalidExp_m3188247161,
	Decimal_stripStyles_m4154272644,
	Decimal_Parse_m942228482,
	Decimal_PerformParse_m1737153701,
	Decimal_ToString_m401229504_AdjustorThunk,
	Decimal_ToString_m3261532885_AdjustorThunk,
	Decimal_ToString_m522438381_AdjustorThunk,
	Decimal_decimal2UInt64_m3451140894,
	Decimal_decimal2Int64_m2813801429,
	Decimal_decimalIncr_m4211330891,
	Decimal_string2decimal_m1826023876,
	Decimal_decimalSetExponent_m835136832,
	Decimal_decimal2double_m1799673680,
	Decimal_decimalFloorAndTrunc_m102249924,
	Decimal_decimalMult_m137374035,
	Decimal_decimalDiv_m3364726341,
	Decimal_decimalCompare_m1981548760,
	Decimal_op_Increment_m3460311049,
	Decimal_op_Subtraction_m2243172173,
	Decimal_op_Multiply_m1703821494,
	Decimal_op_Division_m3334649010,
	Decimal_op_Explicit_m1152324651,
	Decimal_op_Explicit_m2905558026,
	Decimal_op_Explicit_m3588017808,
	Decimal_op_Explicit_m1941964535,
	Decimal_op_Explicit_m2997451292,
	Decimal_op_Explicit_m801735887,
	Decimal_op_Explicit_m2935360665,
	Decimal_op_Explicit_m3225117811,
	Decimal_op_Implicit_m1049928174,
	Decimal_op_Implicit_m1059519657,
	Decimal_op_Implicit_m3967362805,
	Decimal_op_Implicit_m2347208944,
	Decimal_op_Implicit_m4180805396,
	Decimal_op_Implicit_m74114740,
	Decimal_op_Implicit_m549347273,
	Decimal_op_Implicit_m2277896146,
	Decimal_op_Explicit_m1528918129,
	Decimal_op_Explicit_m1768772750,
	Decimal_op_Explicit_m2352650003,
	Decimal_op_Explicit_m4218697563,
	Decimal_op_Inequality_m2018481032,
	Decimal_op_Equality_m85401498,
	Decimal_op_GreaterThan_m2320203197,
	Decimal_op_LessThan_m1574523506,
	Delegate_get_Target_m1823718869,
	Delegate_CreateDelegate_internal_m1250678387,
	Delegate_SetMulticastInvoke_m3164144105,
	Delegate_arg_type_match_m3105837143,
	Delegate_return_type_match_m2444092117,
	Delegate_CreateDelegate_m4207012919,
	Delegate_CreateDelegate_m256783937,
	Delegate_CreateDelegate_m1478302819,
	Delegate_Clone_m2907474788,
	Delegate_Equals_m1837108824,
	Delegate_GetHashCode_m2081162287,
	Delegate_GetInvocationList_m2200569124,
	Delegate_Combine_m1239998138,
	Delegate_CombineImpl_m1454252388,
	Delegate_Remove_m1542158,
	Delegate_RemoveImpl_m3444092335,
	DebuggableAttribute__ctor_m3553871532,
	DebuggerBrowsableAttribute__ctor_m1784875164,
	DebuggerDisplayAttribute__ctor_m685030886,
	DebuggerDisplayAttribute_set_Name_m2283484547,
	DebuggerHiddenAttribute__ctor_m749702160,
	DebuggerStepThroughAttribute__ctor_m1431182322,
	DebuggerTypeProxyAttribute__ctor_m92548023,
	StackFrame__ctor_m3278099530,
	StackFrame__ctor_m2439904383,
	StackFrame_get_frame_info_m1451964570,
	StackFrame_GetFileLineNumber_m1458910347,
	StackFrame_GetFileName_m3260703013,
	StackFrame_GetSecureFileName_m1434779884,
	StackFrame_GetILOffset_m2643308193,
	StackFrame_GetMethod_m3382476531,
	StackFrame_GetNativeOffset_m516534153,
	StackFrame_GetInternalMethodName_m358119658,
	StackFrame_ToString_m3234247435,
	StackTrace__ctor_m690837410,
	StackTrace__ctor_m1990137641,
	StackTrace__ctor_m470329501,
	StackTrace_init_frames_m554340018,
	StackTrace_get_trace_m166904215,
	StackTrace_get_FrameCount_m2559157395,
	StackTrace_GetFrame_m1674626488,
	StackTrace_ToString_m604863459,
	DivideByZeroException__ctor_m2461769612,
	DivideByZeroException__ctor_m172548445,
	DllNotFoundException__ctor_m3972231607,
	DllNotFoundException__ctor_m2177317137,
	Double_System_IConvertible_ToType_m2118596693_AdjustorThunk,
	Double_System_IConvertible_ToBoolean_m3499772275_AdjustorThunk,
	Double_System_IConvertible_ToByte_m2323666643_AdjustorThunk,
	Double_System_IConvertible_ToChar_m1371902193_AdjustorThunk,
	Double_System_IConvertible_ToDateTime_m1838573054_AdjustorThunk,
	Double_System_IConvertible_ToDecimal_m3448152743_AdjustorThunk,
	Double_System_IConvertible_ToDouble_m2017853998_AdjustorThunk,
	Double_System_IConvertible_ToInt16_m3888266809_AdjustorThunk,
	Double_System_IConvertible_ToInt32_m902693213_AdjustorThunk,
	Double_System_IConvertible_ToInt64_m2173745845_AdjustorThunk,
	Double_System_IConvertible_ToSByte_m2692090898_AdjustorThunk,
	Double_System_IConvertible_ToSingle_m2757226682_AdjustorThunk,
	Double_System_IConvertible_ToUInt16_m3937706903_AdjustorThunk,
	Double_System_IConvertible_ToUInt32_m1948889007_AdjustorThunk,
	Double_System_IConvertible_ToUInt64_m2428224974_AdjustorThunk,
	Double_CompareTo_m4218094276_AdjustorThunk,
	Double_Equals_m1930767410_AdjustorThunk,
	Double_CompareTo_m263060497_AdjustorThunk,
	Double_Equals_m3607726724_AdjustorThunk,
	Double_GetHashCode_m2650551438_AdjustorThunk,
	Double_IsInfinity_m4057497359,
	Double_IsNaN_m2498995017,
	Double_IsNegativeInfinity_m2647344334,
	Double_IsPositiveInfinity_m2293949402,
	Double_Parse_m1874640025,
	Double_Parse_m3309721781,
	Double_Parse_m3093230639,
	Double_Parse_m3090470462,
	Double_TryParseStringConstant_m3224623453,
	Double_ParseImpl_m2682713328,
	Double_ToString_m3221644217_AdjustorThunk,
	Double_ToString_m1578634236_AdjustorThunk,
	Double_ToString_m1688271985_AdjustorThunk,
	EntryPointNotFoundException__ctor_m387630107,
	EntryPointNotFoundException__ctor_m2290852730,
	Enum__ctor_m3337276159,
	Enum__cctor_m2877062492,
	Enum_System_IConvertible_ToBoolean_m3975428596,
	Enum_System_IConvertible_ToByte_m1457733691,
	Enum_System_IConvertible_ToChar_m3072918045,
	Enum_System_IConvertible_ToDateTime_m1545541770,
	Enum_System_IConvertible_ToDecimal_m843595796,
	Enum_System_IConvertible_ToDouble_m3990924730,
	Enum_System_IConvertible_ToInt16_m2444520512,
	Enum_System_IConvertible_ToInt32_m3249873431,
	Enum_System_IConvertible_ToInt64_m3454953350,
	Enum_System_IConvertible_ToSByte_m1230613013,
	Enum_System_IConvertible_ToSingle_m1250279,
	Enum_System_IConvertible_ToType_m841565390,
	Enum_System_IConvertible_ToUInt16_m684863254,
	Enum_System_IConvertible_ToUInt32_m37109982,
	Enum_System_IConvertible_ToUInt64_m409681183,
	Enum_GetTypeCode_m3885836212,
	Enum_get_value_m4243904550,
	Enum_get_Value_m23588034,
	Enum_FindPosition_m1790631618,
	Enum_GetName_m2871384883,
	Enum_IsDefined_m1555923783,
	Enum_get_underlying_type_m68214181,
	Enum_GetUnderlyingType_m4160021701,
	Enum_FindName_m2628424663,
	Enum_GetValue_m3881691440,
	Enum_Parse_m1598758293,
	Enum_compare_value_to_m2618635368,
	Enum_CompareTo_m58040714,
	Enum_ToString_m1753515855,
	Enum_ToString_m3329141164,
	Enum_ToString_m997063565,
	Enum_ToString_m3343977455,
	Enum_ToObject_m412986634,
	Enum_ToObject_m1409448066,
	Enum_ToObject_m1790622089,
	Enum_ToObject_m3209825685,
	Enum_ToObject_m1546957196,
	Enum_ToObject_m2176493389,
	Enum_ToObject_m363118936,
	Enum_ToObject_m1273833310,
	Enum_ToObject_m2284871884,
	Enum_Equals_m1394678246,
	Enum_get_hashcode_m919294790,
	Enum_GetHashCode_m3709446324,
	Enum_FormatSpecifier_X_m849909662,
	Enum_FormatFlags_m663299508,
	Enum_Format_m402722146,
	Environment_get_SocketSecurityEnabled_m3565195618,
	Environment_get_NewLine_m921343066,
	Environment_get_Platform_m3623529198,
	Environment_GetOSVersionString_m2904473234,
	Environment_get_OSVersion_m2035335411,
	Environment_internalGetEnvironmentVariable_m4160102786,
	Environment_GetEnvironmentVariable_m3846363269,
	Environment_GetWindowsFolderPath_m1375613171,
	Environment_GetFolderPath_m3193907391,
	Environment_ReadXdgUserDir_m2465043690,
	Environment_InternalGetFolderPath_m4007292074,
	Environment_get_IsRunningOnWindows_m553665425,
	Environment_GetMachineConfigPath_m3780024625,
	Environment_internalGetHome_m3542614548,
	EventArgs__ctor_m3970052124,
	EventArgs__cctor_m3463673875,
	EventHandler__ctor_m2093873464,
	EventHandler_Invoke_m2399520837,
	EventHandler_BeginInvoke_m3395495502,
	EventHandler_EndInvoke_m2472865277,
	Exception__ctor_m1816951,
	Exception__ctor_m3415393023,
	Exception__ctor_m1394352773,
	Exception__ctor_m947608321,
	Exception_get_InnerException_m746750210,
	Exception_get_HResult_m527595245,
	Exception_set_HResult_m1515670465,
	Exception_get_ClassName_m2700395653,
	Exception_get_Message_m4112015361,
	Exception_get_StackTrace_m3281865502,
	Exception_ToString_m1002068351,
	Exception_GetFullNameForStackTrace_m2862456183,
	Exception_GetType_m3018136727,
	ExecutionEngineException__ctor_m1110340308,
	ExecutionEngineException__ctor_m72059654,
	FieldAccessException__ctor_m2436130539,
	FieldAccessException__ctor_m2518067006,
	FieldAccessException__ctor_m1966420921,
	FlagsAttribute__ctor_m3834565173,
	FormatException__ctor_m2169431372,
	FormatException__ctor_m3139021455,
	FormatException__ctor_m1619316639,
	GC_SuppressFinalize_m3884757609,
	Calendar__ctor_m2338449314,
	Calendar_Clone_m56132940,
	Calendar_CheckReadOnly_m2788277881,
	Calendar_get_EraNames_m2687264547,
	CCFixed_FromDateTime_m3382418001,
	CCFixed_day_of_week_m4026386843,
	CCGregorianCalendar_is_leap_year_m4254747021,
	CCGregorianCalendar_fixed_from_dmy_m3913745442,
	CCGregorianCalendar_year_from_fixed_m3411809018,
	CCGregorianCalendar_my_from_fixed_m2023602287,
	CCGregorianCalendar_dmy_from_fixed_m1125637985,
	CCGregorianCalendar_month_from_fixed_m1250031562,
	CCGregorianCalendar_day_from_fixed_m3231376753,
	CCGregorianCalendar_GetDayOfMonth_m3966148956,
	CCGregorianCalendar_GetMonth_m3860161554,
	CCGregorianCalendar_GetYear_m2797871736,
	CCMath_div_m2559077131,
	CCMath_mod_m1150743609,
	CCMath_div_mod_m1713025427,
	CompareInfo__ctor_m1936272694,
	CompareInfo__ctor_m213109871,
	CompareInfo__cctor_m2457604812,
	CompareInfo_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m703135226,
	CompareInfo_get_UseManagedCollation_m1122008926,
	CompareInfo_construct_compareinfo_m3985548769,
	CompareInfo_free_internal_collator_m3692842750,
	CompareInfo_internal_compare_m1398079027,
	CompareInfo_assign_sortkey_m1353932646,
	CompareInfo_internal_index_m3537664183,
	CompareInfo_Finalize_m3345386883,
	CompareInfo_internal_compare_managed_m202538890,
	CompareInfo_internal_compare_switch_m1789565379,
	CompareInfo_Compare_m2998687293,
	CompareInfo_Compare_m433658374,
	CompareInfo_Compare_m990145062,
	CompareInfo_Equals_m4096032872,
	CompareInfo_GetHashCode_m874156047,
	CompareInfo_GetSortKey_m2288950352,
	CompareInfo_IndexOf_m1642526294,
	CompareInfo_internal_index_managed_m847641856,
	CompareInfo_internal_index_switch_m653835107,
	CompareInfo_IndexOf_m1727054940,
	CompareInfo_IsPrefix_m2602982038,
	CompareInfo_IsSuffix_m2576535659,
	CompareInfo_LastIndexOf_m729157850,
	CompareInfo_LastIndexOf_m1490450230,
	CompareInfo_ToString_m566026374,
	CompareInfo_get_LCID_m1583067375,
	CultureInfo__ctor_m3525240161,
	CultureInfo__ctor_m33467521,
	CultureInfo__ctor_m924087270,
	CultureInfo__ctor_m3609353999,
	CultureInfo__ctor_m950742381,
	CultureInfo__cctor_m3631226733,
	CultureInfo_get_InvariantCulture_m1553973733,
	CultureInfo_get_CurrentCulture_m3186983941,
	CultureInfo_get_CurrentUICulture_m3611276231,
	CultureInfo_ConstructCurrentCulture_m2382699215,
	CultureInfo_ConstructCurrentUICulture_m2245112812,
	CultureInfo_get_Territory_m2825339350,
	CultureInfo_get_LCID_m2311161170,
	CultureInfo_get_Name_m3285309744,
	CultureInfo_get_Parent_m1061191803,
	CultureInfo_get_TextInfo_m1378976081,
	CultureInfo_get_IcuName_m3221843485,
	CultureInfo_Clone_m2116537561,
	CultureInfo_Equals_m1263117208,
	CultureInfo_GetHashCode_m48214238,
	CultureInfo_ToString_m3153525391,
	CultureInfo_get_CompareInfo_m3818339347,
	CultureInfo_get_IsNeutralCulture_m3301447976,
	CultureInfo_CheckNeutral_m977067172,
	CultureInfo_get_NumberFormat_m1751954804,
	CultureInfo_set_NumberFormat_m303999793,
	CultureInfo_get_DateTimeFormat_m3491120702,
	CultureInfo_set_DateTimeFormat_m1518630760,
	CultureInfo_get_IsReadOnly_m2338231366,
	CultureInfo_GetFormat_m3767138108,
	CultureInfo_Construct_m3927976456,
	CultureInfo_ConstructInternalLocaleFromName_m866611573,
	CultureInfo_ConstructInternalLocaleFromLcid_m1222245966,
	CultureInfo_ConstructInternalLocaleFromCurrentLocale_m2562214947,
	CultureInfo_construct_internal_locale_from_lcid_m2093041484,
	CultureInfo_construct_internal_locale_from_name_m3306341211,
	CultureInfo_construct_internal_locale_from_current_locale_m1848081026,
	CultureInfo_construct_datetime_format_m553349517,
	CultureInfo_construct_number_format_m1440279526,
	CultureInfo_ConstructInvariant_m2280761812,
	CultureInfo_CreateTextInfo_m1064081308,
	CultureInfo_insert_into_shared_tables_m3648892084,
	CultureInfo_GetCultureInfo_m2002634362,
	CultureInfo_GetCultureInfo_m1593052334,
	CultureInfo_CreateCulture_m203779851,
	DateTimeFormatInfo__ctor_m3648500895,
	DateTimeFormatInfo__ctor_m760991236,
	DateTimeFormatInfo__cctor_m2142440524,
	DateTimeFormatInfo_GetInstance_m4151193095,
	DateTimeFormatInfo_get_IsReadOnly_m1245526434,
	DateTimeFormatInfo_ReadOnly_m1624544977,
	DateTimeFormatInfo_Clone_m221763627,
	DateTimeFormatInfo_GetFormat_m1198654296,
	DateTimeFormatInfo_GetAbbreviatedMonthName_m3849932716,
	DateTimeFormatInfo_GetEraName_m2523233910,
	DateTimeFormatInfo_GetMonthName_m1976713297,
	DateTimeFormatInfo_get_RawAbbreviatedDayNames_m2200361064,
	DateTimeFormatInfo_get_RawAbbreviatedMonthNames_m2837152612,
	DateTimeFormatInfo_get_RawDayNames_m1788651075,
	DateTimeFormatInfo_get_RawMonthNames_m3259123252,
	DateTimeFormatInfo_get_AMDesignator_m1667043795,
	DateTimeFormatInfo_get_PMDesignator_m1920996550,
	DateTimeFormatInfo_get_DateSeparator_m3898223580,
	DateTimeFormatInfo_get_TimeSeparator_m2189288444,
	DateTimeFormatInfo_get_LongDatePattern_m10673428,
	DateTimeFormatInfo_get_ShortDatePattern_m800966562,
	DateTimeFormatInfo_get_ShortTimePattern_m214755676,
	DateTimeFormatInfo_get_LongTimePattern_m837638707,
	DateTimeFormatInfo_get_MonthDayPattern_m3900941703,
	DateTimeFormatInfo_get_YearMonthPattern_m2134005242,
	DateTimeFormatInfo_get_FullDateTimePattern_m4279931869,
	DateTimeFormatInfo_get_CurrentInfo_m1909081317,
	DateTimeFormatInfo_get_InvariantInfo_m3979751787,
	DateTimeFormatInfo_get_Calendar_m14822801,
	DateTimeFormatInfo_set_Calendar_m2427543037,
	DateTimeFormatInfo_get_RFC1123Pattern_m2072104945,
	DateTimeFormatInfo_get_RoundtripPattern_m343902144,
	DateTimeFormatInfo_get_SortableDateTimePattern_m2796530772,
	DateTimeFormatInfo_get_UniversalSortableDateTimePattern_m2797392990,
	DateTimeFormatInfo_GetAllDateTimePatternsInternal_m3656848191,
	DateTimeFormatInfo_FillAllDateTimePatterns_m3007362540,
	DateTimeFormatInfo_GetAllRawDateTimePatterns_m3294676931,
	DateTimeFormatInfo_GetDayName_m1769119731,
	DateTimeFormatInfo_GetAbbreviatedDayName_m783154435,
	DateTimeFormatInfo_FillInvariantPatterns_m308133250,
	DateTimeFormatInfo_PopulateCombinedList_m85478254,
	DaylightTime__ctor_m3356372841,
	DaylightTime_get_Start_m2838577138,
	DaylightTime_get_End_m99924226,
	DaylightTime_get_Delta_m2257589543,
	GregorianCalendar__ctor_m2638258818,
	GregorianCalendar__ctor_m2095203450,
	GregorianCalendar_get_Eras_m2448107167,
	GregorianCalendar_set_CalendarType_m321229003,
	GregorianCalendar_GetDayOfMonth_m3218095145,
	GregorianCalendar_GetDayOfWeek_m1831200356,
	GregorianCalendar_GetEra_m2672846977,
	GregorianCalendar_GetMonth_m207055093,
	GregorianCalendar_GetYear_m956119763,
	NumberFormatInfo__ctor_m3065442163,
	NumberFormatInfo__ctor_m3394184656,
	NumberFormatInfo__ctor_m2545500917,
	NumberFormatInfo__cctor_m2409688165,
	NumberFormatInfo_get_CurrencyDecimalDigits_m1054586595,
	NumberFormatInfo_get_CurrencyDecimalSeparator_m1336171097,
	NumberFormatInfo_get_CurrencyGroupSeparator_m3849499073,
	NumberFormatInfo_get_RawCurrencyGroupSizes_m2590170303,
	NumberFormatInfo_get_CurrencyNegativePattern_m683893117,
	NumberFormatInfo_get_CurrencyPositivePattern_m4188282411,
	NumberFormatInfo_get_CurrencySymbol_m1696803863,
	NumberFormatInfo_get_CurrentInfo_m681710701,
	NumberFormatInfo_get_InvariantInfo_m4171439859,
	NumberFormatInfo_get_NaNSymbol_m3664499422,
	NumberFormatInfo_get_NegativeInfinitySymbol_m432321453,
	NumberFormatInfo_get_NegativeSign_m3505425583,
	NumberFormatInfo_get_NumberDecimalDigits_m2175143232,
	NumberFormatInfo_get_NumberDecimalSeparator_m2141040615,
	NumberFormatInfo_get_NumberGroupSeparator_m2381910380,
	NumberFormatInfo_get_RawNumberGroupSizes_m1837935180,
	NumberFormatInfo_get_NumberNegativePattern_m2856862106,
	NumberFormatInfo_set_NumberNegativePattern_m15717523,
	NumberFormatInfo_get_PercentDecimalDigits_m3468741610,
	NumberFormatInfo_get_PercentDecimalSeparator_m3534534709,
	NumberFormatInfo_get_PercentGroupSeparator_m4188385544,
	NumberFormatInfo_get_RawPercentGroupSizes_m2784761932,
	NumberFormatInfo_get_PercentNegativePattern_m1520909170,
	NumberFormatInfo_get_PercentPositivePattern_m2808305050,
	NumberFormatInfo_get_PercentSymbol_m2370855822,
	NumberFormatInfo_get_PerMilleSymbol_m3407208990,
	NumberFormatInfo_get_PositiveInfinitySymbol_m1704514514,
	NumberFormatInfo_get_PositiveSign_m3460909578,
	NumberFormatInfo_GetFormat_m1404969485,
	NumberFormatInfo_Clone_m2450825682,
	NumberFormatInfo_GetInstance_m2393140909,
	RegionInfo__ctor_m1416434243,
	RegionInfo__ctor_m3049991405,
	RegionInfo_get_CurrentRegion_m3879384183,
	RegionInfo_GetByTerritory_m2765791561,
	RegionInfo_construct_internal_region_from_name_m1964467534,
	RegionInfo_get_CurrencyEnglishName_m2699486149,
	RegionInfo_get_CurrencySymbol_m1123963360,
	RegionInfo_get_DisplayName_m234317390,
	RegionInfo_get_EnglishName_m1834029122,
	RegionInfo_get_GeoId_m2009686238,
	RegionInfo_get_IsMetric_m2010743696,
	RegionInfo_get_ISOCurrencySymbol_m1527138053,
	RegionInfo_get_NativeName_m2791477888,
	RegionInfo_get_CurrencyNativeName_m1078600886,
	RegionInfo_get_Name_m3529597995,
	RegionInfo_get_ThreeLetterISORegionName_m3727743814,
	RegionInfo_get_ThreeLetterWindowsRegionName_m648134458,
	RegionInfo_get_TwoLetterISORegionName_m4044257697,
	RegionInfo_Equals_m4016944140,
	RegionInfo_GetHashCode_m2755242465,
	RegionInfo_ToString_m1434015883,
	SortKey__ctor_m3504451567,
	SortKey__ctor_m148594354,
	SortKey_Compare_m1772552206,
	SortKey_get_OriginalString_m1800238925,
	SortKey_get_KeyData_m2935146546,
	SortKey_Equals_m273349684,
	SortKey_GetHashCode_m3484627189,
	SortKey_ToString_m679734994,
	TextInfo__ctor_m1202670054,
	TextInfo__ctor_m3765241533,
	TextInfo_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m3013407797,
	TextInfo_get_ListSeparator_m3241381324,
	TextInfo_get_CultureName_m2324322044,
	TextInfo_Equals_m1956140142,
	TextInfo_GetHashCode_m543285532,
	TextInfo_ToString_m1949805935,
	TextInfo_ToLower_m941018871,
	TextInfo_ToUpper_m2328982835,
	TextInfo_ToLower_m2569245245,
	TextInfo_Clone_m2167794002,
	Guid__ctor_m366922919_AdjustorThunk,
	Guid__ctor_m1415939458_AdjustorThunk,
	Guid__ctor_m1840805599_AdjustorThunk,
	Guid__ctor_m4215161261_AdjustorThunk,
	Guid__cctor_m1259807860,
	Guid_CheckNull_m1432541912,
	Guid_CheckLength_m404985278,
	Guid_CheckArray_m979102941,
	Guid_Compare_m3988396397,
	Guid_CompareTo_m1283272613_AdjustorThunk,
	Guid_Equals_m996559241_AdjustorThunk,
	Guid_CompareTo_m2483260021_AdjustorThunk,
	Guid_Equals_m3913097454_AdjustorThunk,
	Guid_GetHashCode_m3315775205_AdjustorThunk,
	Guid_ToHex_m2758306647,
	Guid_NewGuid_m3277502664,
	Guid_AppendInt_m1197935714,
	Guid_AppendShort_m2300705645,
	Guid_AppendByte_m2657930108,
	Guid_BaseToString_m3503046643_AdjustorThunk,
	Guid_ToString_m2467832444_AdjustorThunk,
	Guid_ToString_m857218039_AdjustorThunk,
	Guid_ToString_m3497849028_AdjustorThunk,
	Guid_op_Equality_m3918449706,
	GuidParser__ctor_m954092722,
	GuidParser_Reset_m3895554130,
	GuidParser_AtEnd_m2681682394,
	GuidParser_ThrowFormatException_m2810643018,
	GuidParser_ParseHex_m3369382782,
	GuidParser_ParseOptChar_m4031216945,
	GuidParser_ParseChar_m1840855132,
	GuidParser_ParseGuid1_m103848066,
	GuidParser_ParseHexPrefix_m2147067076,
	GuidParser_ParseGuid2_m841714959,
	GuidParser_Parse_m820144007,
	IndexOutOfRangeException__ctor_m4192121744,
	IndexOutOfRangeException__ctor_m2238521308,
	IndexOutOfRangeException__ctor_m3954211171,
	Int16_System_IConvertible_ToBoolean_m608568030_AdjustorThunk,
	Int16_System_IConvertible_ToByte_m1672628155_AdjustorThunk,
	Int16_System_IConvertible_ToChar_m2735270145_AdjustorThunk,
	Int16_System_IConvertible_ToDateTime_m3461644636_AdjustorThunk,
	Int16_System_IConvertible_ToDecimal_m2621680868_AdjustorThunk,
	Int16_System_IConvertible_ToDouble_m567044138_AdjustorThunk,
	Int16_System_IConvertible_ToInt16_m1850971393_AdjustorThunk,
	Int16_System_IConvertible_ToInt32_m1937055083_AdjustorThunk,
	Int16_System_IConvertible_ToInt64_m2937777175_AdjustorThunk,
	Int16_System_IConvertible_ToSByte_m2180362671_AdjustorThunk,
	Int16_System_IConvertible_ToSingle_m2608377140_AdjustorThunk,
	Int16_System_IConvertible_ToType_m2143513339_AdjustorThunk,
	Int16_System_IConvertible_ToUInt16_m3718580411_AdjustorThunk,
	Int16_System_IConvertible_ToUInt32_m2354401397_AdjustorThunk,
	Int16_System_IConvertible_ToUInt64_m1213563982_AdjustorThunk,
	Int16_CompareTo_m4027690811_AdjustorThunk,
	Int16_Equals_m1346215590_AdjustorThunk,
	Int16_GetHashCode_m443056876_AdjustorThunk,
	Int16_CompareTo_m2086232926_AdjustorThunk,
	Int16_Equals_m3297601691_AdjustorThunk,
	Int16_Parse_m583677545,
	Int16_Parse_m1892243601,
	Int16_Parse_m4281000664,
	Int16_TryParse_m2772964988,
	Int16_ToString_m4080367874_AdjustorThunk,
	Int16_ToString_m2532926678_AdjustorThunk,
	Int16_ToString_m1946657537_AdjustorThunk,
	Int16_ToString_m1980211496_AdjustorThunk,
	Int32_System_IConvertible_ToBoolean_m2319908351_AdjustorThunk,
	Int32_System_IConvertible_ToByte_m4272893928_AdjustorThunk,
	Int32_System_IConvertible_ToChar_m2322934717_AdjustorThunk,
	Int32_System_IConvertible_ToDateTime_m715642928_AdjustorThunk,
	Int32_System_IConvertible_ToDecimal_m2713593576_AdjustorThunk,
	Int32_System_IConvertible_ToDouble_m1298176326_AdjustorThunk,
	Int32_System_IConvertible_ToInt16_m2439558523_AdjustorThunk,
	Int32_System_IConvertible_ToInt32_m1807903970_AdjustorThunk,
	Int32_System_IConvertible_ToInt64_m4037035890_AdjustorThunk,
	Int32_System_IConvertible_ToSByte_m4052844363_AdjustorThunk,
	Int32_System_IConvertible_ToSingle_m3592801059_AdjustorThunk,
	Int32_System_IConvertible_ToType_m2475977603_AdjustorThunk,
	Int32_System_IConvertible_ToUInt16_m512640774_AdjustorThunk,
	Int32_System_IConvertible_ToUInt32_m2688754728_AdjustorThunk,
	Int32_System_IConvertible_ToUInt64_m2142522567_AdjustorThunk,
	Int32_CompareTo_m2643288015_AdjustorThunk,
	Int32_Equals_m1484368690_AdjustorThunk,
	Int32_GetHashCode_m339244912_AdjustorThunk,
	Int32_CompareTo_m960498275_AdjustorThunk,
	Int32_Equals_m2128805555_AdjustorThunk,
	Int32_ProcessTrailingWhitespace_m3213104504,
	Int32_Parse_m1849335896,
	Int32_Parse_m2525031250,
	Int32_CheckStyle_m3873463410,
	Int32_JumpOverWhite_m1044411918,
	Int32_FindSign_m3693607241,
	Int32_FindCurrency_m3094038441,
	Int32_FindExponent_m2503482456,
	Int32_FindOther_m253001860,
	Int32_ValidDigit_m1948424466,
	Int32_GetFormatException_m3406637715,
	Int32_Parse_m3592018864,
	Int32_Parse_m501041078,
	Int32_Parse_m1540871473,
	Int32_TryParse_m86332859,
	Int32_TryParse_m1513256800,
	Int32_ToString_m942907109_AdjustorThunk,
	Int32_ToString_m1055050781_AdjustorThunk,
	Int32_ToString_m678894197_AdjustorThunk,
	Int32_ToString_m3162039764_AdjustorThunk,
	Int64_System_IConvertible_ToBoolean_m4098580936_AdjustorThunk,
	Int64_System_IConvertible_ToByte_m1049109362_AdjustorThunk,
	Int64_System_IConvertible_ToChar_m2468419666_AdjustorThunk,
	Int64_System_IConvertible_ToDateTime_m2709613486_AdjustorThunk,
	Int64_System_IConvertible_ToDecimal_m736076175_AdjustorThunk,
	Int64_System_IConvertible_ToDouble_m35851038_AdjustorThunk,
	Int64_System_IConvertible_ToInt16_m2310188060_AdjustorThunk,
	Int64_System_IConvertible_ToInt32_m149402608_AdjustorThunk,
	Int64_System_IConvertible_ToInt64_m1119652710_AdjustorThunk,
	Int64_System_IConvertible_ToSByte_m4250139882_AdjustorThunk,
	Int64_System_IConvertible_ToSingle_m3691596191_AdjustorThunk,
	Int64_System_IConvertible_ToType_m480516046_AdjustorThunk,
	Int64_System_IConvertible_ToUInt16_m1774985091_AdjustorThunk,
	Int64_System_IConvertible_ToUInt32_m490642399_AdjustorThunk,
	Int64_System_IConvertible_ToUInt64_m476448274_AdjustorThunk,
	Int64_CompareTo_m4140694992_AdjustorThunk,
	Int64_Equals_m3567984782_AdjustorThunk,
	Int64_GetHashCode_m2212758480_AdjustorThunk,
	Int64_CompareTo_m2587245040_AdjustorThunk,
	Int64_Equals_m1853265956_AdjustorThunk,
	Int64_Parse_m115698243,
	Int64_Parse_m2921227530,
	Int64_Parse_m3896306439,
	Int64_Parse_m3857086958,
	Int64_Parse_m4285359207,
	Int64_TryParse_m3030841212,
	Int64_TryParse_m1386765942,
	Int64_ToString_m1285539637_AdjustorThunk,
	Int64_ToString_m2384113553_AdjustorThunk,
	Int64_ToString_m2836968315_AdjustorThunk,
	Int64_ToString_m1263666479_AdjustorThunk,
	IntPtr__ctor_m1874125153_AdjustorThunk,
	IntPtr__ctor_m1846124433_AdjustorThunk,
	IntPtr__ctor_m3020263568_AdjustorThunk,
	IntPtr__ctor_m3650517410_AdjustorThunk,
	IntPtr_get_Size_m3603629221,
	IntPtr_Equals_m3250342679_AdjustorThunk,
	IntPtr_GetHashCode_m2382536356_AdjustorThunk,
	IntPtr_ToInt64_m3776154157_AdjustorThunk,
	IntPtr_ToPointer_m565950407_AdjustorThunk,
	IntPtr_ToString_m2819219677_AdjustorThunk,
	IntPtr_ToString_m1070034645_AdjustorThunk,
	IntPtr_op_Equality_m3978210203,
	IntPtr_op_Inequality_m1374147269,
	IntPtr_op_Explicit_m3440174878,
	IntPtr_op_Explicit_m1287079606,
	IntPtr_op_Explicit_m913523000,
	IntPtr_op_Explicit_m2003607512,
	IntPtr_op_Explicit_m3354255807,
	InvalidCastException__ctor_m2302931503,
	InvalidCastException__ctor_m1929418500,
	InvalidCastException__ctor_m395183990,
	InvalidOperationException__ctor_m3074049270,
	InvalidOperationException__ctor_m1754939824,
	InvalidOperationException__ctor_m1249789426,
	InvalidOperationException__ctor_m1909242104,
	BinaryReader__ctor_m3891178347,
	BinaryReader__ctor_m2947142779,
	BinaryReader_System_IDisposable_Dispose_m3066875955,
	BinaryReader_get_BaseStream_m436692523,
	BinaryReader_Close_m656404955,
	BinaryReader_Dispose_m1579899878,
	BinaryReader_FillBuffer_m4230478491,
	BinaryReader_Read_m2359026958,
	BinaryReader_Read_m2792699949,
	BinaryReader_Read_m3041790912,
	BinaryReader_ReadCharBytes_m4274906988,
	BinaryReader_Read7BitEncodedInt_m3619737175,
	BinaryReader_ReadBoolean_m3554613854,
	BinaryReader_ReadByte_m2801931525,
	BinaryReader_ReadBytes_m1713992062,
	BinaryReader_ReadChar_m914228394,
	BinaryReader_ReadDecimal_m768237421,
	BinaryReader_ReadDouble_m1160704559,
	BinaryReader_ReadInt16_m1101465428,
	BinaryReader_ReadInt32_m995919288,
	BinaryReader_ReadInt64_m1982603364,
	BinaryReader_ReadSByte_m3116176050,
	BinaryReader_ReadString_m3318513216,
	BinaryReader_ReadSingle_m2392231938,
	BinaryReader_ReadUInt16_m3883529060,
	BinaryReader_ReadUInt32_m4084076630,
	BinaryReader_ReadUInt64_m181663393,
	BinaryReader_CheckBuffer_m2312246899,
	Directory_CreateDirectory_m1758760249,
	Directory_CreateDirectoriesInternal_m3935921796,
	Directory_Exists_m3276502796,
	Directory_GetCurrentDirectory_m331201736,
	Directory_GetFiles_m235723336,
	Directory_GetFileSystemEntries_m3572746264,
	DirectoryInfo__ctor_m1609466484,
	DirectoryInfo__ctor_m2105118223,
	DirectoryInfo__ctor_m474244860,
	DirectoryInfo_Initialize_m428229676,
	DirectoryInfo_get_Exists_m1872834505,
	DirectoryInfo_get_Parent_m3132120194,
	DirectoryInfo_Create_m1869279555,
	DirectoryInfo_ToString_m443528604,
	DirectoryNotFoundException__ctor_m1947819234,
	DirectoryNotFoundException__ctor_m1202819030,
	DirectoryNotFoundException__ctor_m2906679049,
	EndOfStreamException__ctor_m2347655014,
	EndOfStreamException__ctor_m178835592,
	File_Delete_m2485719844,
	File_Exists_m2248590861,
	File_Open_m2870785110,
	File_OpenRead_m2214564889,
	File_OpenText_m276421639,
	FileLoadException__ctor_m521743258,
	FileLoadException__ctor_m3079274755,
	FileLoadException_get_Message_m98332663,
	FileLoadException_ToString_m3184166710,
	FileNotFoundException__ctor_m1923109332,
	FileNotFoundException__ctor_m3508239193,
	FileNotFoundException__ctor_m2268276037,
	FileNotFoundException_get_Message_m2819723224,
	FileNotFoundException_ToString_m260718854,
	FileStream__ctor_m3918629695,
	FileStream__ctor_m2298191702,
	FileStream__ctor_m1378073862,
	FileStream__ctor_m1627439574,
	FileStream__ctor_m199725632,
	FileStream_get_CanRead_m2205092554,
	FileStream_get_CanWrite_m3403378411,
	FileStream_get_CanSeek_m1969291698,
	FileStream_get_Length_m3023624910,
	FileStream_get_Position_m1789780299,
	FileStream_set_Position_m1342652146,
	FileStream_ReadByte_m1668565028,
	FileStream_WriteByte_m24018895,
	FileStream_Read_m2991800180,
	FileStream_ReadInternal_m4238080744,
	FileStream_BeginRead_m2129228277,
	FileStream_EndRead_m2497897596,
	FileStream_Write_m1947011602,
	FileStream_WriteInternal_m37363449,
	FileStream_BeginWrite_m3846105556,
	FileStream_EndWrite_m4253206090,
	FileStream_Seek_m3619933131,
	FileStream_SetLength_m3541353046,
	FileStream_Flush_m888958391,
	FileStream_Finalize_m274693354,
	FileStream_Dispose_m3584873965,
	FileStream_ReadSegment_m928397223,
	FileStream_WriteSegment_m527164273,
	FileStream_FlushBuffer_m533766994,
	FileStream_FlushBuffer_m2876878919,
	FileStream_FlushBufferIfDirty_m676211487,
	FileStream_RefillBuffer_m537701314,
	FileStream_ReadData_m406536788,
	FileStream_InitBuffer_m2348525094,
	FileStream_GetSecureFileName_m3144911672,
	FileStream_GetSecureFileName_m2765795265,
	ReadDelegate__ctor_m1101817851,
	ReadDelegate_Invoke_m2426149229,
	ReadDelegate_BeginInvoke_m923466772,
	ReadDelegate_EndInvoke_m1073758779,
	WriteDelegate__ctor_m944271225,
	WriteDelegate_Invoke_m1513952088,
	WriteDelegate_BeginInvoke_m3915988971,
	WriteDelegate_EndInvoke_m1108179134,
	FileStreamAsyncResult__ctor_m3777138729,
	FileStreamAsyncResult_CBWrapper_m3203659335,
	FileStreamAsyncResult_get_AsyncState_m2828107890,
	FileStreamAsyncResult_get_AsyncWaitHandle_m634220271,
	FileStreamAsyncResult_get_IsCompleted_m283701089,
	FileSystemInfo__ctor_m3361274585,
	FileSystemInfo__ctor_m3175765434,
	FileSystemInfo_get_FullName_m1008202315,
	FileSystemInfo_Refresh_m2155846864,
	FileSystemInfo_InternalRefresh_m2899045144,
	FileSystemInfo_CheckPath_m601048000,
	IOException__ctor_m2747881001,
	IOException__ctor_m239973922,
	IOException__ctor_m804076173,
	IOException__ctor_m2871752017,
	IOException__ctor_m324016635,
	IsolatedStorageException__ctor_m2374576592,
	IsolatedStorageException__ctor_m2598607174,
	IsolatedStorageException__ctor_m1969844842,
	MemoryStream__ctor_m152170362,
	MemoryStream__ctor_m3154874056,
	MemoryStream__ctor_m3060464295,
	MemoryStream_InternalConstructor_m2014775173,
	MemoryStream_CheckIfClosedThrowDisposed_m1274599527,
	MemoryStream_get_CanRead_m4260422867,
	MemoryStream_get_CanSeek_m3031870200,
	MemoryStream_get_CanWrite_m1144494425,
	MemoryStream_set_Capacity_m609551339,
	MemoryStream_get_Length_m2968868229,
	MemoryStream_get_Position_m3673270320,
	MemoryStream_set_Position_m1362836606,
	MemoryStream_Dispose_m2676754718,
	MemoryStream_Flush_m2402768916,
	MemoryStream_Read_m1905632689,
	MemoryStream_ReadByte_m1203405088,
	MemoryStream_Seek_m413807161,
	MemoryStream_CalculateNewCapacity_m4130758411,
	MemoryStream_Expand_m2060252436,
	MemoryStream_SetLength_m797959567,
	MemoryStream_ToArray_m3844442891,
	MemoryStream_Write_m58461126,
	MemoryStream_WriteByte_m957317921,
	MonoIO__cctor_m2224974877,
	MonoIO_GetException_m3894828512,
	MonoIO_GetException_m3130736866,
	MonoIO_CreateDirectory_m1218943223,
	MonoIO_GetFileSystemEntries_m3041356216,
	MonoIO_GetCurrentDirectory_m4279991629,
	MonoIO_DeleteFile_m4294701721,
	MonoIO_GetFileAttributes_m3850897511,
	MonoIO_GetFileType_m1683047928,
	MonoIO_ExistsFile_m545552615,
	MonoIO_ExistsDirectory_m1423241567,
	MonoIO_GetFileStat_m3378506111,
	MonoIO_Open_m4023759764,
	MonoIO_Close_m154153664,
	MonoIO_Read_m1573024126,
	MonoIO_Write_m4073260495,
	MonoIO_Seek_m2929164469,
	MonoIO_GetLength_m931400829,
	MonoIO_SetLength_m3496987753,
	MonoIO_get_ConsoleOutput_m2839318334,
	MonoIO_get_ConsoleInput_m270320281,
	MonoIO_get_ConsoleError_m459703959,
	MonoIO_get_VolumeSeparatorChar_m1461410365,
	MonoIO_get_DirectorySeparatorChar_m474903005,
	MonoIO_get_AltDirectorySeparatorChar_m3530465749,
	MonoIO_get_PathSeparator_m3158637569,
	MonoIO_RemapPath_m4185117495,
	NullStream__ctor_m1694643915,
	NullStream_get_CanRead_m244471328,
	NullStream_get_CanSeek_m3696771727,
	NullStream_get_CanWrite_m2701633067,
	NullStream_get_Length_m3809531554,
	NullStream_get_Position_m1314498788,
	NullStream_set_Position_m1303016132,
	NullStream_Flush_m608790423,
	NullStream_Read_m869508193,
	NullStream_ReadByte_m909255314,
	NullStream_Seek_m1958745155,
	NullStream_SetLength_m3931240662,
	NullStream_Write_m1347951148,
	NullStream_WriteByte_m540204641,
	Path__cctor_m1964914989,
	Path_Combine_m668557981,
	Path_CleanPath_m928099398,
	Path_GetDirectoryName_m3928521125,
	Path_GetFileName_m1682010817,
	Path_GetFullPath_m452249650,
	Path_WindowsDriveAdjustment_m2051393215,
	Path_InsecureGetFullPath_m2251214602,
	Path_IsDsc_m4246137163,
	Path_GetPathRoot_m3398115072,
	Path_IsPathRooted_m955005557,
	Path_GetInvalidPathChars_m4276546429,
	Path_GetServerAndShare_m1454975977,
	Path_SameRoot_m2647504235,
	Path_CanonicalizePath_m2319072634,
	PathTooLongException__ctor_m1890132073,
	PathTooLongException__ctor_m4167185363,
	PathTooLongException__ctor_m232413694,
	SearchPattern__cctor_m2509654630,
	Stream__ctor_m1504197473,
	Stream__cctor_m381764956,
	Stream_Dispose_m2041138381,
	Stream_Dispose_m1784135134,
	Stream_Close_m3852604827,
	Stream_ReadByte_m3485022462,
	Stream_WriteByte_m4148766345,
	Stream_BeginRead_m21201343,
	Stream_BeginWrite_m1364911803,
	Stream_EndRead_m347362531,
	Stream_EndWrite_m1831026058,
	StreamAsyncResult__ctor_m1151342516,
	StreamAsyncResult_SetComplete_m1586080444,
	StreamAsyncResult_SetComplete_m1418769851,
	StreamAsyncResult_get_AsyncState_m586245974,
	StreamAsyncResult_get_AsyncWaitHandle_m4196239148,
	StreamAsyncResult_get_IsCompleted_m3518571650,
	StreamAsyncResult_get_Exception_m1467024748,
	StreamAsyncResult_get_NBytes_m1123713136,
	StreamAsyncResult_get_Done_m2868486077,
	StreamAsyncResult_set_Done_m4141014649,
	StreamReader__ctor_m306838996,
	StreamReader__ctor_m1430778449,
	StreamReader__ctor_m772411305,
	StreamReader__ctor_m574213171,
	StreamReader__ctor_m476460896,
	StreamReader__cctor_m2999995226,
	StreamReader_Initialize_m3472901841,
	StreamReader_Dispose_m1707073584,
	StreamReader_DoChecks_m382520478,
	StreamReader_ReadBuffer_m2597661069,
	StreamReader_Peek_m840963052,
	StreamReader_Read_m4201100268,
	StreamReader_Read_m984462095,
	StreamReader_FindNextEOL_m1787012226,
	StreamReader_ReadLine_m1306910928,
	StreamReader_ReadToEnd_m2702940513,
	NullStreamReader__ctor_m2177862304,
	NullStreamReader_Peek_m962605665,
	NullStreamReader_Read_m3147588732,
	NullStreamReader_Read_m559467848,
	NullStreamReader_ReadLine_m1824535686,
	NullStreamReader_ReadToEnd_m699976792,
	StreamWriter__ctor_m2953689202,
	StreamWriter__ctor_m2151664980,
	StreamWriter__cctor_m3972042264,
	StreamWriter_Initialize_m136491558,
	StreamWriter_set_AutoFlush_m4040646994,
	StreamWriter_Dispose_m2868164241,
	StreamWriter_Flush_m2894376598,
	StreamWriter_FlushBytes_m3815561943,
	StreamWriter_Decode_m1893779174,
	StreamWriter_Write_m754966457,
	StreamWriter_LowLevelWrite_m1464925781,
	StreamWriter_LowLevelWrite_m1585693714,
	StreamWriter_Write_m411622688,
	StreamWriter_Write_m3672466093,
	StreamWriter_Write_m1157245398,
	StreamWriter_Close_m2543594019,
	StreamWriter_Finalize_m2960509706,
	StringReader__ctor_m293197912,
	StringReader_Dispose_m3582706645,
	StringReader_Peek_m517508988,
	StringReader_Read_m219013114,
	StringReader_Read_m1450996487,
	StringReader_ReadLine_m3378185754,
	StringReader_ReadToEnd_m3293493509,
	StringReader_CheckObjectDisposedException_m459857537,
	SynchronizedReader__ctor_m97537115,
	SynchronizedReader_Peek_m2068499653,
	SynchronizedReader_ReadLine_m603631510,
	SynchronizedReader_ReadToEnd_m1477645998,
	SynchronizedReader_Read_m1699992380,
	SynchronizedReader_Read_m1276095379,
	SynchronizedWriter__ctor_m3291303868,
	SynchronizedWriter_Close_m976773255,
	SynchronizedWriter_Flush_m2790170340,
	SynchronizedWriter_Write_m721687999,
	SynchronizedWriter_Write_m4139598715,
	SynchronizedWriter_Write_m2563334752,
	SynchronizedWriter_Write_m880103225,
	SynchronizedWriter_WriteLine_m189940250,
	SynchronizedWriter_WriteLine_m2909572594,
	TextReader__ctor_m1823118017,
	TextReader__cctor_m1403833620,
	TextReader_Dispose_m2213368838,
	TextReader_Dispose_m2560631502,
	TextReader_Peek_m2365689290,
	TextReader_Read_m13623654,
	TextReader_Read_m947437872,
	TextReader_ReadLine_m1272697447,
	TextReader_ReadToEnd_m2199643530,
	TextReader_Synchronized_m3878481166,
	NullTextReader__ctor_m2057671012,
	NullTextReader_ReadLine_m2049156248,
	TextWriter__ctor_m1254332623,
	TextWriter__cctor_m2254058948,
	TextWriter_Close_m1696769202,
	TextWriter_Dispose_m1209375003,
	TextWriter_Dispose_m3262285662,
	TextWriter_Flush_m3921915905,
	TextWriter_Synchronized_m921099061,
	TextWriter_Write_m4283680745,
	TextWriter_Write_m1908988917,
	TextWriter_Write_m1675905377,
	TextWriter_Write_m2294181007,
	TextWriter_WriteLine_m1222244591,
	TextWriter_WriteLine_m1258241687,
	NullTextWriter__ctor_m4250544285,
	NullTextWriter_Write_m2871250657,
	NullTextWriter_Write_m1953194581,
	NullTextWriter_Write_m3278725927,
	UnexceptionalStreamReader__ctor_m3446914697,
	UnexceptionalStreamReader__cctor_m364230527,
	UnexceptionalStreamReader_Peek_m2077668078,
	UnexceptionalStreamReader_Read_m3056287453,
	UnexceptionalStreamReader_Read_m3929647529,
	UnexceptionalStreamReader_CheckEOL_m1688596573,
	UnexceptionalStreamReader_ReadLine_m3183025907,
	UnexceptionalStreamReader_ReadToEnd_m2853978983,
	UnexceptionalStreamWriter__ctor_m2260891421,
	UnexceptionalStreamWriter_Flush_m2386651684,
	UnexceptionalStreamWriter_Write_m1684018641,
	UnexceptionalStreamWriter_Write_m2192175500,
	UnexceptionalStreamWriter_Write_m3307790284,
	UnexceptionalStreamWriter_Write_m2296388455,
	UnmanagedMemoryStream_get_CanRead_m3880114840,
	UnmanagedMemoryStream_get_CanSeek_m611005855,
	UnmanagedMemoryStream_get_CanWrite_m1101817676,
	UnmanagedMemoryStream_get_Length_m1106236018,
	UnmanagedMemoryStream_get_Position_m3512336964,
	UnmanagedMemoryStream_set_Position_m2743156219,
	UnmanagedMemoryStream_Read_m3618811389,
	UnmanagedMemoryStream_ReadByte_m1842835906,
	UnmanagedMemoryStream_Seek_m3376649204,
	UnmanagedMemoryStream_SetLength_m1197559161,
	UnmanagedMemoryStream_Flush_m2850088448,
	UnmanagedMemoryStream_Dispose_m296305050,
	UnmanagedMemoryStream_Write_m2848563498,
	UnmanagedMemoryStream_WriteByte_m2073206089,
	LocalDataStoreSlot__ctor_m2109704463,
	LocalDataStoreSlot__cctor_m1713086281,
	LocalDataStoreSlot_Finalize_m3310459962,
	MarshalByRefObject__ctor_m3580128209,
	MarshalByRefObject_get_ObjectIdentity_m1652221559,
	Math_Abs_m409923275,
	Math_Abs_m449664873,
	Math_Abs_m1960589991,
	Math_Floor_m685695611,
	Math_Max_m3051295998,
	Math_Min_m1276480861,
	Math_Round_m3365377037,
	Math_Round_m2262982208,
	Math_Pow_m1935598555,
	Math_Sqrt_m121065893,
	MemberAccessException__ctor_m1789979295,
	MemberAccessException__ctor_m4010303872,
	MemberAccessException__ctor_m3521037496,
	MethodAccessException__ctor_m1637646650,
	MethodAccessException__ctor_m3103093336,
	MissingFieldException__ctor_m188650395,
	MissingFieldException__ctor_m1971298103,
	MissingFieldException__ctor_m3080962593,
	MissingFieldException_get_Message_m1093724828,
	MissingMemberException__ctor_m766432918,
	MissingMemberException__ctor_m2292368174,
	MissingMemberException__ctor_m2506390741,
	MissingMemberException__ctor_m176814157,
	MissingMemberException_get_Message_m1601224464,
	MissingMethodException__ctor_m2581308231,
	MissingMethodException__ctor_m1031234694,
	MissingMethodException__ctor_m16286169,
	MissingMethodException__ctor_m2949116767,
	MissingMethodException_get_Message_m842428339,
	MonoAsyncCall__ctor_m3504457514,
	MonoCustomAttrs__cctor_m13843532,
	MonoCustomAttrs_IsUserCattrProvider_m2069719286,
	MonoCustomAttrs_GetCustomAttributesInternal_m3884961971,
	MonoCustomAttrs_GetPseudoCustomAttributes_m3610733686,
	MonoCustomAttrs_GetCustomAttributesBase_m2436230232,
	MonoCustomAttrs_GetCustomAttribute_m4275597331,
	MonoCustomAttrs_GetCustomAttributes_m3735333428,
	MonoCustomAttrs_GetCustomAttributes_m3649344832,
	MonoCustomAttrs_GetCustomAttributesDataInternal_m1105915382,
	MonoCustomAttrs_GetCustomAttributesData_m4191467674,
	MonoCustomAttrs_IsDefined_m3302438960,
	MonoCustomAttrs_IsDefinedInternal_m3560397479,
	MonoCustomAttrs_GetBasePropertyDefinition_m87202095,
	MonoCustomAttrs_GetBase_m204129966,
	MonoCustomAttrs_RetrieveAttributeUsage_m881586189,
	AttributeInfo__ctor_m914143849,
	AttributeInfo_get_Usage_m21368055,
	AttributeInfo_get_InheritanceLevel_m4227771319,
	MonoDocumentationNoteAttribute__ctor_m3324629758,
	MonoEnumInfo__ctor_m1643764635_AdjustorThunk,
	MonoEnumInfo__cctor_m2409779423,
	MonoEnumInfo_get_enum_info_m2018182279,
	MonoEnumInfo_get_Cache_m2364646175,
	MonoEnumInfo_GetInfo_m483821611,
	IntComparer__ctor_m498925939,
	IntComparer_Compare_m2390570492,
	IntComparer_Compare_m2288675866,
	LongComparer__ctor_m1782533220,
	LongComparer_Compare_m3581620395,
	LongComparer_Compare_m3608989338,
	SByteComparer__ctor_m1544949693,
	SByteComparer_Compare_m1249332637,
	SByteComparer_Compare_m1565703925,
	ShortComparer__ctor_m809988133,
	ShortComparer_Compare_m3794535082,
	ShortComparer_Compare_m2290445900,
	MonoTODOAttribute__ctor_m1347054473,
	MonoTODOAttribute__ctor_m2689597370,
	MonoTouchAOTHelper__cctor_m4150697742,
	MonoType_get_attributes_m1308912579,
	MonoType_GetDefaultConstructor_m844404926,
	MonoType_GetAttributeFlagsImpl_m3149937489,
	MonoType_GetConstructorImpl_m3454079076,
	MonoType_GetConstructors_internal_m342936590,
	MonoType_GetConstructors_m1583588365,
	MonoType_InternalGetEvent_m4167640615,
	MonoType_GetEvent_m1659059560,
	MonoType_GetField_m3686129833,
	MonoType_GetFields_internal_m2059525432,
	MonoType_GetFields_m4170770315,
	MonoType_GetInterfaces_m534239473,
	MonoType_GetMethodsByName_m3677183547,
	MonoType_GetMethods_m3707161063,
	MonoType_GetMethodImpl_m886303632,
	MonoType_GetPropertiesByName_m225472389,
	MonoType_GetPropertyImpl_m1936836523,
	MonoType_HasElementTypeImpl_m3143972582,
	MonoType_IsArrayImpl_m3271761795,
	MonoType_IsByRefImpl_m2577121498,
	MonoType_IsPointerImpl_m1487282369,
	MonoType_IsPrimitiveImpl_m2286522716,
	MonoType_IsSubclassOf_m3938220594,
	MonoType_InvokeMember_m2094661425,
	MonoType_GetElementType_m1981081120,
	MonoType_get_UnderlyingSystemType_m2032206497,
	MonoType_get_Assembly_m819582969,
	MonoType_get_AssemblyQualifiedName_m208321748,
	MonoType_getFullName_m132163685,
	MonoType_get_BaseType_m3678567033,
	MonoType_get_FullName_m3296291579,
	MonoType_IsDefined_m2186296028,
	MonoType_GetCustomAttributes_m3798502322,
	MonoType_GetCustomAttributes_m3840448869,
	MonoType_get_MemberType_m378348367,
	MonoType_get_Name_m381307003,
	MonoType_get_Namespace_m3829586408,
	MonoType_get_Module_m2928378227,
	MonoType_get_DeclaringType_m2734903002,
	MonoType_get_ReflectedType_m3946481517,
	MonoType_get_TypeHandle_m397824088,
	MonoType_ToString_m1100936579,
	MonoType_GetGenericArguments_m1335083072,
	MonoType_get_ContainsGenericParameters_m6578403,
	MonoType_get_IsGenericParameter_m1967179938,
	MonoType_GetGenericTypeDefinition_m1435681011,
	MonoType_CheckMethodSecurity_m1030836417,
	MonoType_ReorderParamArrayArguments_m2524972069,
	MonoTypeInfo__ctor_m1547907977,
	MulticastDelegate_Equals_m64371161,
	MulticastDelegate_GetHashCode_m2150985785,
	MulticastDelegate_GetInvocationList_m1417804319,
	MulticastDelegate_CombineImpl_m952498781,
	MulticastDelegate_BaseEquals_m603046058,
	MulticastDelegate_KPM_m2529894516,
	MulticastDelegate_RemoveImpl_m2938661263,
	MulticastNotSupportedException__ctor_m2664693452,
	MulticastNotSupportedException__ctor_m747815933,
	MulticastNotSupportedException__ctor_m199605124,
	NonSerializedAttribute__ctor_m100300183,
	NotImplementedException__ctor_m2570805563,
	NotImplementedException__ctor_m595125831,
	NotImplementedException__ctor_m2106393729,
	NotSupportedException__ctor_m2647670222,
	NotSupportedException__ctor_m4039520923,
	NotSupportedException__ctor_m1240477304,
	NullReferenceException__ctor_m2749761743,
	NullReferenceException__ctor_m1443281920,
	NullReferenceException__ctor_m172726003,
	NumberFormatter__ctor_m882423236,
	NumberFormatter__cctor_m3127417417,
	NumberFormatter_GetFormatterTables_m2527842287,
	NumberFormatter_GetTenPowerOf_m580279802,
	NumberFormatter_InitDecHexDigits_m1515890855,
	NumberFormatter_InitDecHexDigits_m4153623248,
	NumberFormatter_InitDecHexDigits_m523693063,
	NumberFormatter_FastToDecHex_m11094303,
	NumberFormatter_ToDecHex_m1152169443,
	NumberFormatter_FastDecHexLen_m2867570029,
	NumberFormatter_DecHexLen_m1580324117,
	NumberFormatter_DecHexLen_m3752403606,
	NumberFormatter_ScaleOrder_m2995984485,
	NumberFormatter_InitialFloatingPrecision_m180952382,
	NumberFormatter_ParsePrecision_m81415732,
	NumberFormatter_Init_m3482988837,
	NumberFormatter_InitHex_m1660820330,
	NumberFormatter_Init_m3611118981,
	NumberFormatter_Init_m743203113,
	NumberFormatter_Init_m2452070105,
	NumberFormatter_Init_m1055011748,
	NumberFormatter_Init_m4226945301,
	NumberFormatter_Init_m2055243600,
	NumberFormatter_ResetCharBuf_m3226660505,
	NumberFormatter_Resize_m1832105174,
	NumberFormatter_Append_m2953789120,
	NumberFormatter_Append_m1387470406,
	NumberFormatter_Append_m3558378514,
	NumberFormatter_GetNumberFormatInstance_m3068474261,
	NumberFormatter_set_CurrentCulture_m4133449828,
	NumberFormatter_get_IntegerDigits_m47711755,
	NumberFormatter_get_DecimalDigits_m2386777519,
	NumberFormatter_get_IsFloatingSource_m228797787,
	NumberFormatter_get_IsZero_m2438227288,
	NumberFormatter_get_IsZeroInteger_m4115179637,
	NumberFormatter_RoundPos_m679530010,
	NumberFormatter_RoundDecimal_m639068567,
	NumberFormatter_RoundBits_m3611922302,
	NumberFormatter_RemoveTrailingZeros_m2551313845,
	NumberFormatter_AddOneToDecHex_m3751539435,
	NumberFormatter_AddOneToDecHex_m3021247525,
	NumberFormatter_CountTrailingZeros_m35386702,
	NumberFormatter_CountTrailingZeros_m2729623164,
	NumberFormatter_GetInstance_m2657568193,
	NumberFormatter_Release_m3231894907,
	NumberFormatter_SetThreadCurrentCulture_m1936928120,
	NumberFormatter_NumberToString_m1101975758,
	NumberFormatter_NumberToString_m3052084606,
	NumberFormatter_NumberToString_m2171216354,
	NumberFormatter_NumberToString_m2879872021,
	NumberFormatter_NumberToString_m1903630728,
	NumberFormatter_NumberToString_m2443105739,
	NumberFormatter_NumberToString_m3446587791,
	NumberFormatter_NumberToString_m3799590785,
	NumberFormatter_NumberToString_m1231813599,
	NumberFormatter_NumberToString_m1232625936,
	NumberFormatter_NumberToString_m1326808136,
	NumberFormatter_NumberToString_m2193112985,
	NumberFormatter_NumberToString_m3758122717,
	NumberFormatter_NumberToString_m2131945648,
	NumberFormatter_NumberToString_m1983366083,
	NumberFormatter_NumberToString_m3542247186,
	NumberFormatter_NumberToString_m2872185530,
	NumberFormatter_FastIntegerToString_m3758483719,
	NumberFormatter_IntegerToString_m792550392,
	NumberFormatter_NumberToString_m2691816022,
	NumberFormatter_FormatCurrency_m329550722,
	NumberFormatter_FormatDecimal_m1064728487,
	NumberFormatter_FormatHexadecimal_m2081806955,
	NumberFormatter_FormatFixedPoint_m1540112410,
	NumberFormatter_FormatRoundtrip_m1541160154,
	NumberFormatter_FormatRoundtrip_m2836917638,
	NumberFormatter_FormatGeneral_m3897558037,
	NumberFormatter_FormatNumber_m210532640,
	NumberFormatter_FormatPercent_m948264060,
	NumberFormatter_FormatExponential_m3535129782,
	NumberFormatter_FormatExponential_m4223907376,
	NumberFormatter_FormatCustom_m2307374058,
	NumberFormatter_ZeroTrimEnd_m3695493236,
	NumberFormatter_IsZeroOnly_m301052596,
	NumberFormatter_AppendNonNegativeNumber_m3842278575,
	NumberFormatter_AppendIntegerString_m3975100308,
	NumberFormatter_AppendIntegerString_m1574486672,
	NumberFormatter_AppendDecimalString_m3929917867,
	NumberFormatter_AppendDecimalString_m2615915907,
	NumberFormatter_AppendIntegerStringWithGroupSeparator_m1886348349,
	NumberFormatter_AppendExponent_m2078024189,
	NumberFormatter_AppendOneDigit_m3331299856,
	NumberFormatter_FastAppendDigits_m2298647756,
	NumberFormatter_AppendDigits_m4058032180,
	NumberFormatter_AppendDigits_m3796019938,
	NumberFormatter_Multiply10_m3376371810,
	NumberFormatter_Divide10_m2887365473,
	NumberFormatter_GetClone_m3323601924,
	CustomInfo__ctor_m1296891841,
	CustomInfo_GetActiveSection_m861289010,
	CustomInfo_Parse_m4259014900,
	CustomInfo_Format_m1684327327,
	Object__ctor_m3446193457,
	Object_Equals_m3544058702,
	Object_Equals_m899471607,
	Object_Finalize_m1722074356,
	Object_GetHashCode_m1725373185,
	Object_GetType_m4087638284,
	Object_MemberwiseClone_m1134924534,
	Object_ToString_m1080586622,
	Object_ReferenceEquals_m1923645570,
	Object_InternalGetHashCode_m1442750202,
	ObjectDisposedException__ctor_m781126761,
	ObjectDisposedException__ctor_m4001580019,
	ObjectDisposedException__ctor_m2784909353,
	ObjectDisposedException_get_Message_m3136208161,
	ObsoleteAttribute__ctor_m1879591032,
	ObsoleteAttribute__ctor_m3274828507,
	ObsoleteAttribute__ctor_m1055576761,
	OperatingSystem__ctor_m983404079,
	OperatingSystem_get_Platform_m3729450976,
	OperatingSystem_Clone_m2635910214,
	OperatingSystem_ToString_m97079520,
	OrdinalComparer__ctor_m1284925701,
	OrdinalComparer_Compare_m3031790537,
	OrdinalComparer_Equals_m3606852489,
	OrdinalComparer_GetHashCode_m4105287100,
	OutOfMemoryException__ctor_m3233772617,
	OutOfMemoryException__ctor_m2276193125,
	OutOfMemoryException__ctor_m441591316,
	OverflowException__ctor_m3668245243,
	OverflowException__ctor_m1384916612,
	OverflowException__ctor_m3735634925,
	ParamArrayAttribute__ctor_m1696407647,
	PlatformNotSupportedException__ctor_m3311290888,
	PlatformNotSupportedException__ctor_m1475964536,
	RankException__ctor_m2090556091,
	RankException__ctor_m774219180,
	RankException__ctor_m2523038546,
	AmbiguousMatchException__ctor_m2375151799,
	AmbiguousMatchException__ctor_m3181965201,
	AmbiguousMatchException__ctor_m1115300120,
	Assembly__ctor_m2503447432,
	Assembly_get_code_base_m2040813,
	Assembly_get_fullname_m1845803503,
	Assembly_get_location_m329465134,
	Assembly_GetCodeBase_m1589103529,
	Assembly_get_FullName_m4007032079,
	Assembly_get_Location_m2735854603,
	Assembly_IsDefined_m3029835645,
	Assembly_GetCustomAttributes_m77163733,
	Assembly_GetManifestResourceInternal_m2271067159,
	Assembly_GetTypes_m3955978312,
	Assembly_GetTypes_m1975427444,
	Assembly_GetType_m2280899582,
	Assembly_GetType_m2764996290,
	Assembly_InternalGetType_m4001481222,
	Assembly_GetType_m3123598361,
	Assembly_FillName_m829560512,
	Assembly_GetName_m3194382543,
	Assembly_GetName_m3130637144,
	Assembly_UnprotectedGetName_m1763648659,
	Assembly_ToString_m2040911384,
	Assembly_Load_m1225372324,
	Assembly_GetExecutingAssembly_m1654616147,
	ResolveEventHolder__ctor_m341688792,
	AssemblyCompanyAttribute__ctor_m1315167307,
	AssemblyCopyrightAttribute__ctor_m4282510565,
	AssemblyDefaultAliasAttribute__ctor_m393492554,
	AssemblyDelaySignAttribute__ctor_m3802049740,
	AssemblyDescriptionAttribute__ctor_m3502566481,
	AssemblyFileVersionAttribute__ctor_m4281768118,
	AssemblyInformationalVersionAttribute__ctor_m1415206487,
	AssemblyKeyFileAttribute__ctor_m1870834162,
	AssemblyName__ctor_m144750582,
	AssemblyName__ctor_m1750531701,
	AssemblyName_get_Name_m770412142,
	AssemblyName_get_Flags_m1242999343,
	AssemblyName_get_FullName_m2833406749,
	AssemblyName_get_Version_m2538954018,
	AssemblyName_set_Version_m1461954998,
	AssemblyName_ToString_m2513744769,
	AssemblyName_get_IsPublicKeyValid_m1649981173,
	AssemblyName_InternalGetPublicKeyToken_m1069083009,
	AssemblyName_ComputePublicKeyToken_m1615069588,
	AssemblyName_SetPublicKey_m565542145,
	AssemblyName_SetPublicKeyToken_m1864317684,
	AssemblyName_Clone_m3092312624,
	AssemblyName_OnDeserialization_m772831047,
	AssemblyProductAttribute__ctor_m2217606340,
	AssemblyTitleAttribute__ctor_m4091976430,
	Binder__ctor_m2031026775,
	Binder__cctor_m2352550279,
	Binder_get_DefaultBinder_m2045865785,
	Binder_ConvertArgs_m3763414446,
	Binder_GetDerivedLevel_m1948575344,
	Binder_FindMostDerivedMatch_m3497124822,
	Default__ctor_m4273947660,
	Default_BindToMethod_m2514609917,
	Default_ReorderParameters_m4230856473,
	Default_IsArrayAssignable_m1201555924,
	Default_ChangeType_m2319690257,
	Default_ReorderArgumentArray_m266338901,
	Default_check_type_m377139779,
	Default_check_arguments_m2892567715,
	Default_SelectMethod_m1741803856,
	Default_SelectMethod_m4094796858,
	Default_GetBetterMethod_m372074302,
	Default_CompareCloserType_m4137505321,
	Default_SelectProperty_m3861348674,
	Default_check_arguments_with_score_m680913026,
	Default_check_type_with_score_m773560188,
	ConstructorInfo__ctor_m1808498097,
	ConstructorInfo__cctor_m2772849124,
	ConstructorInfo_get_MemberType_m4000823216,
	ConstructorInfo_Invoke_m4178483106,
	CustomAttributeData__ctor_m1227935591,
	CustomAttributeData_get_Constructor_m3981983546,
	CustomAttributeData_get_ConstructorArguments_m3052531565,
	CustomAttributeData_get_NamedArguments_m1329570588,
	CustomAttributeData_GetCustomAttributes_m1773804050,
	CustomAttributeData_GetCustomAttributes_m3510215830,
	CustomAttributeData_GetCustomAttributes_m4140466514,
	CustomAttributeData_GetCustomAttributes_m17259639,
	CustomAttributeData_ToString_m3967580330,
	CustomAttributeData_Equals_m3157954665,
	CustomAttributeData_GetHashCode_m2489456201,
	CustomAttributeNamedArgument_ToString_m1920457871_AdjustorThunk,
	CustomAttributeNamedArgument_Equals_m2602747387_AdjustorThunk,
	CustomAttributeNamedArgument_GetHashCode_m3609262192_AdjustorThunk,
	CustomAttributeTypedArgument_ToString_m3280070021_AdjustorThunk,
	CustomAttributeTypedArgument_Equals_m1480105269_AdjustorThunk,
	CustomAttributeTypedArgument_GetHashCode_m1148730416_AdjustorThunk,
	DefaultMemberAttribute__ctor_m3129895323,
	DefaultMemberAttribute_get_MemberName_m1752551129,
	AssemblyBuilder_get_Location_m2901826204,
	AssemblyBuilder_GetTypes_m2732780010,
	AssemblyBuilder_get_IsCompilerContext_m2403536670,
	AssemblyBuilder_not_supported_m738697247,
	AssemblyBuilder_UnprotectedGetName_m1473090115,
	ConstructorBuilder__ctor_m3380435570,
	ConstructorBuilder_get_CallingConvention_m416770567,
	ConstructorBuilder_get_TypeBuilder_m472195973,
	ConstructorBuilder_GetParameters_m2431579090,
	ConstructorBuilder_GetParametersInternal_m472900066,
	ConstructorBuilder_GetParameterCount_m1948820966,
	ConstructorBuilder_Invoke_m279985127,
	ConstructorBuilder_Invoke_m2799833077,
	ConstructorBuilder_get_MethodHandle_m1748976910,
	ConstructorBuilder_get_Attributes_m1398665057,
	ConstructorBuilder_get_ReflectedType_m964892251,
	ConstructorBuilder_get_DeclaringType_m2546517360,
	ConstructorBuilder_get_Name_m3145274236,
	ConstructorBuilder_IsDefined_m4089054386,
	ConstructorBuilder_GetCustomAttributes_m1738281631,
	ConstructorBuilder_GetCustomAttributes_m2389683246,
	ConstructorBuilder_GetILGenerator_m2071550933,
	ConstructorBuilder_GetILGenerator_m3080353190,
	ConstructorBuilder_GetToken_m3335315133,
	ConstructorBuilder_get_Module_m539373161,
	ConstructorBuilder_ToString_m620390697,
	ConstructorBuilder_fixup_m643618020,
	ConstructorBuilder_get_next_table_index_m1541009121,
	ConstructorBuilder_get_IsCompilerContext_m1774478654,
	ConstructorBuilder_not_supported_m3101107144,
	ConstructorBuilder_not_created_m343206691,
	EnumBuilder_get_Assembly_m565086802,
	EnumBuilder_get_AssemblyQualifiedName_m2614673987,
	EnumBuilder_get_BaseType_m3289929135,
	EnumBuilder_get_DeclaringType_m2594970671,
	EnumBuilder_get_FullName_m4113314092,
	EnumBuilder_get_Module_m997829433,
	EnumBuilder_get_Name_m1181153314,
	EnumBuilder_get_Namespace_m4211380120,
	EnumBuilder_get_ReflectedType_m2656989504,
	EnumBuilder_get_TypeHandle_m407547082,
	EnumBuilder_get_UnderlyingSystemType_m3966542618,
	EnumBuilder_GetAttributeFlagsImpl_m1076718463,
	EnumBuilder_GetConstructorImpl_m561979540,
	EnumBuilder_GetConstructors_m3085338377,
	EnumBuilder_GetCustomAttributes_m274406178,
	EnumBuilder_GetCustomAttributes_m1994953809,
	EnumBuilder_GetElementType_m2227593022,
	EnumBuilder_GetEvent_m694296250,
	EnumBuilder_GetField_m1672069908,
	EnumBuilder_GetFields_m1987286750,
	EnumBuilder_GetInterfaces_m1858647194,
	EnumBuilder_GetMethodImpl_m3445072901,
	EnumBuilder_GetMethods_m977878939,
	EnumBuilder_GetPropertyImpl_m2233824998,
	EnumBuilder_HasElementTypeImpl_m2534796953,
	EnumBuilder_InvokeMember_m2984316485,
	EnumBuilder_IsArrayImpl_m2802054820,
	EnumBuilder_IsByRefImpl_m1401428246,
	EnumBuilder_IsPointerImpl_m118503420,
	EnumBuilder_IsPrimitiveImpl_m3496261765,
	EnumBuilder_IsValueTypeImpl_m1769410956,
	EnumBuilder_IsDefined_m826867273,
	EnumBuilder_CreateNotSupportedException_m861705916,
	FieldBuilder_get_Attributes_m219928032,
	FieldBuilder_get_DeclaringType_m1073567308,
	FieldBuilder_get_FieldHandle_m1930105241,
	FieldBuilder_get_FieldType_m2522660795,
	FieldBuilder_get_Name_m3394280395,
	FieldBuilder_get_ReflectedType_m3513560358,
	FieldBuilder_GetCustomAttributes_m608818869,
	FieldBuilder_GetCustomAttributes_m3255372514,
	FieldBuilder_GetValue_m846706224,
	FieldBuilder_IsDefined_m2962848206,
	FieldBuilder_GetFieldOffset_m1720899837,
	FieldBuilder_SetValue_m3566470566,
	FieldBuilder_get_UMarshal_m2493034584,
	FieldBuilder_CreateNotSupportedException_m1374657726,
	FieldBuilder_get_Module_m1803345809,
	GenericTypeParameterBuilder_IsSubclassOf_m2493609931,
	GenericTypeParameterBuilder_GetAttributeFlagsImpl_m3990635610,
	GenericTypeParameterBuilder_GetConstructorImpl_m753999115,
	GenericTypeParameterBuilder_GetConstructors_m2634453164,
	GenericTypeParameterBuilder_GetEvent_m1329508473,
	GenericTypeParameterBuilder_GetField_m1889715417,
	GenericTypeParameterBuilder_GetFields_m2744857607,
	GenericTypeParameterBuilder_GetInterfaces_m3462466532,
	GenericTypeParameterBuilder_GetMethods_m4219420128,
	GenericTypeParameterBuilder_GetMethodImpl_m2980884362,
	GenericTypeParameterBuilder_GetPropertyImpl_m3626565850,
	GenericTypeParameterBuilder_HasElementTypeImpl_m150032775,
	GenericTypeParameterBuilder_IsAssignableFrom_m1102625234,
	GenericTypeParameterBuilder_IsInstanceOfType_m939262635,
	GenericTypeParameterBuilder_IsArrayImpl_m4154256820,
	GenericTypeParameterBuilder_IsByRefImpl_m3287374520,
	GenericTypeParameterBuilder_IsPointerImpl_m1987073912,
	GenericTypeParameterBuilder_IsPrimitiveImpl_m3285618665,
	GenericTypeParameterBuilder_IsValueTypeImpl_m289531264,
	GenericTypeParameterBuilder_InvokeMember_m1147727746,
	GenericTypeParameterBuilder_GetElementType_m460425678,
	GenericTypeParameterBuilder_get_UnderlyingSystemType_m1633586012,
	GenericTypeParameterBuilder_get_Assembly_m4223435044,
	GenericTypeParameterBuilder_get_AssemblyQualifiedName_m3052126742,
	GenericTypeParameterBuilder_get_BaseType_m943684461,
	GenericTypeParameterBuilder_get_FullName_m2984553906,
	GenericTypeParameterBuilder_IsDefined_m3409969362,
	GenericTypeParameterBuilder_GetCustomAttributes_m948809026,
	GenericTypeParameterBuilder_GetCustomAttributes_m4219671456,
	GenericTypeParameterBuilder_get_Name_m4223682894,
	GenericTypeParameterBuilder_get_Namespace_m10098664,
	GenericTypeParameterBuilder_get_Module_m2309239027,
	GenericTypeParameterBuilder_get_DeclaringType_m481244658,
	GenericTypeParameterBuilder_get_ReflectedType_m2468647034,
	GenericTypeParameterBuilder_get_TypeHandle_m3153660848,
	GenericTypeParameterBuilder_GetGenericArguments_m4175544120,
	GenericTypeParameterBuilder_GetGenericTypeDefinition_m3873940944,
	GenericTypeParameterBuilder_get_ContainsGenericParameters_m326624778,
	GenericTypeParameterBuilder_get_IsGenericParameter_m302075495,
	GenericTypeParameterBuilder_get_IsGenericType_m2448345782,
	GenericTypeParameterBuilder_get_IsGenericTypeDefinition_m2522764535,
	GenericTypeParameterBuilder_not_supported_m3311309618,
	GenericTypeParameterBuilder_ToString_m205292240,
	GenericTypeParameterBuilder_Equals_m3732267147,
	GenericTypeParameterBuilder_GetHashCode_m3376400717,
	GenericTypeParameterBuilder_MakeGenericType_m3812059673,
	ILGenerator__ctor_m4276427634,
	ILGenerator__cctor_m2225434446,
	ILGenerator_add_token_fixup_m524277590,
	ILGenerator_make_room_m2145189716,
	ILGenerator_emit_int_m3878412281,
	ILGenerator_ll_emit_m2330719544,
	ILGenerator_Emit_m2237324484,
	ILGenerator_Emit_m1154166167,
	ILGenerator_label_fixup_m1333664849,
	ILGenerator_Mono_GetCurrentOffset_m66508256,
	MethodBuilder_get_ContainsGenericParameters_m2360427483,
	MethodBuilder_get_MethodHandle_m3410394182,
	MethodBuilder_get_ReturnType_m133520348,
	MethodBuilder_get_ReflectedType_m3218931816,
	MethodBuilder_get_DeclaringType_m3992555149,
	MethodBuilder_get_Name_m2164650894,
	MethodBuilder_get_Attributes_m557318860,
	MethodBuilder_get_CallingConvention_m452128205,
	MethodBuilder_GetBaseDefinition_m1236478928,
	MethodBuilder_GetParameters_m2744380191,
	MethodBuilder_GetParameterCount_m3686046676,
	MethodBuilder_Invoke_m2609170815,
	MethodBuilder_IsDefined_m277230573,
	MethodBuilder_GetCustomAttributes_m4189138581,
	MethodBuilder_GetCustomAttributes_m2985734895,
	MethodBuilder_check_override_m4022641967,
	MethodBuilder_fixup_m2055556419,
	MethodBuilder_ToString_m1434929445,
	MethodBuilder_Equals_m303455341,
	MethodBuilder_GetHashCode_m4200125178,
	MethodBuilder_get_next_table_index_m397105556,
	MethodBuilder_NotSupported_m3832827859,
	MethodBuilder_MakeGenericMethod_m1039446169,
	MethodBuilder_get_IsGenericMethodDefinition_m1968682553,
	MethodBuilder_get_IsGenericMethod_m2084698038,
	MethodBuilder_GetGenericArguments_m3487349379,
	MethodBuilder_get_Module_m3640104193,
	MethodToken__ctor_m2157754371_AdjustorThunk,
	MethodToken__cctor_m963256158,
	MethodToken_Equals_m848415105_AdjustorThunk,
	MethodToken_GetHashCode_m715310535_AdjustorThunk,
	MethodToken_get_Token_m1103007335_AdjustorThunk,
	ModuleBuilder__cctor_m4239403761,
	ModuleBuilder_get_next_table_index_m3200252350,
	ModuleBuilder_GetTypes_m1324970686,
	ModuleBuilder_getToken_m3901973344,
	ModuleBuilder_GetToken_m848611450,
	ModuleBuilder_RegisterToken_m619657331,
	ModuleBuilder_GetTokenGenerator_m1794001162,
	ModuleBuilderTokenGenerator__ctor_m18313188,
	ModuleBuilderTokenGenerator_GetToken_m1488753242,
	OpCode__ctor_m1726332432_AdjustorThunk,
	OpCode_GetHashCode_m2246397448_AdjustorThunk,
	OpCode_Equals_m2955207306_AdjustorThunk,
	OpCode_ToString_m3107149020_AdjustorThunk,
	OpCode_get_Name_m3025990655_AdjustorThunk,
	OpCode_get_Size_m931123431_AdjustorThunk,
	OpCode_get_StackBehaviourPop_m2005666979_AdjustorThunk,
	OpCode_get_StackBehaviourPush_m3170452393_AdjustorThunk,
	OpCodeNames__cctor_m1400736817,
	OpCodes__cctor_m4217172955,
	ParameterBuilder_get_Attributes_m81931643,
	ParameterBuilder_get_Name_m3516894523,
	ParameterBuilder_get_Position_m395919688,
	PropertyBuilder_get_Attributes_m2279594537,
	PropertyBuilder_get_CanRead_m2409695466,
	PropertyBuilder_get_CanWrite_m2802360124,
	PropertyBuilder_get_DeclaringType_m1225045750,
	PropertyBuilder_get_Name_m2642909525,
	PropertyBuilder_get_PropertyType_m2926243180,
	PropertyBuilder_get_ReflectedType_m3401682742,
	PropertyBuilder_GetAccessors_m1237269319,
	PropertyBuilder_GetCustomAttributes_m837390663,
	PropertyBuilder_GetCustomAttributes_m387135554,
	PropertyBuilder_GetGetMethod_m2399017824,
	PropertyBuilder_GetIndexParameters_m2252963430,
	PropertyBuilder_GetSetMethod_m3147416675,
	PropertyBuilder_GetValue_m1434073069,
	PropertyBuilder_GetValue_m3294302440,
	PropertyBuilder_IsDefined_m3135037989,
	PropertyBuilder_SetValue_m2153976900,
	PropertyBuilder_SetValue_m1276310233,
	PropertyBuilder_get_Module_m1043329146,
	PropertyBuilder_not_supported_m1711805992,
	TypeBuilder_GetAttributeFlagsImpl_m2598685731,
	TypeBuilder_setup_internal_class_m128930060,
	TypeBuilder_create_generic_class_m261174092,
	TypeBuilder_get_Assembly_m223781556,
	TypeBuilder_get_AssemblyQualifiedName_m4188959294,
	TypeBuilder_get_BaseType_m2270822725,
	TypeBuilder_get_DeclaringType_m2338804447,
	TypeBuilder_get_UnderlyingSystemType_m65590438,
	TypeBuilder_get_FullName_m3642975229,
	TypeBuilder_get_Module_m1616194710,
	TypeBuilder_get_Name_m982983587,
	TypeBuilder_get_Namespace_m208582417,
	TypeBuilder_get_ReflectedType_m438795595,
	TypeBuilder_GetConstructorImpl_m3989283741,
	TypeBuilder_IsDefined_m3003472617,
	TypeBuilder_GetCustomAttributes_m3294702302,
	TypeBuilder_GetCustomAttributes_m1756217549,
	TypeBuilder_DefineConstructor_m1162809555,
	TypeBuilder_DefineConstructor_m2260052946,
	TypeBuilder_DefineDefaultConstructor_m3505124928,
	TypeBuilder_create_runtime_class_m857078028,
	TypeBuilder_is_nested_in_m2924220661,
	TypeBuilder_has_ctor_method_m877392841,
	TypeBuilder_CreateType_m3690229376,
	TypeBuilder_GetConstructors_m55221742,
	TypeBuilder_GetConstructorsInternal_m2910998681,
	TypeBuilder_GetElementType_m420773727,
	TypeBuilder_GetEvent_m696517673,
	TypeBuilder_GetField_m394689338,
	TypeBuilder_GetFields_m155238781,
	TypeBuilder_GetInterfaces_m1636926525,
	TypeBuilder_GetMethodsByName_m1570553827,
	TypeBuilder_GetMethods_m2466610851,
	TypeBuilder_GetMethodImpl_m2234341452,
	TypeBuilder_GetPropertyImpl_m955190697,
	TypeBuilder_HasElementTypeImpl_m3213799375,
	TypeBuilder_InvokeMember_m1316938506,
	TypeBuilder_IsArrayImpl_m244275415,
	TypeBuilder_IsByRefImpl_m3935315218,
	TypeBuilder_IsPointerImpl_m1316609598,
	TypeBuilder_IsPrimitiveImpl_m2172525888,
	TypeBuilder_IsValueTypeImpl_m683567880,
	TypeBuilder_MakeGenericType_m2575213963,
	TypeBuilder_get_TypeHandle_m3307184692,
	TypeBuilder_SetParent_m3238504574,
	TypeBuilder_get_next_table_index_m2276959831,
	TypeBuilder_get_IsCompilerContext_m3599164304,
	TypeBuilder_get_is_created_m4289290384,
	TypeBuilder_not_supported_m3120789591,
	TypeBuilder_check_not_created_m3504919281,
	TypeBuilder_check_created_m102704719,
	TypeBuilder_ToString_m3726280791,
	TypeBuilder_IsAssignableFrom_m1068163903,
	TypeBuilder_IsSubclassOf_m2364973326,
	TypeBuilder_IsAssignableTo_m326333311,
	TypeBuilder_GetGenericArguments_m4118364689,
	TypeBuilder_GetGenericTypeDefinition_m2118699508,
	TypeBuilder_get_ContainsGenericParameters_m204346989,
	TypeBuilder_get_IsGenericParameter_m2020796253,
	TypeBuilder_get_IsGenericTypeDefinition_m4127126086,
	TypeBuilder_get_IsGenericType_m2610618835,
	UnmanagedMarshal_ToMarshalAsAttribute_m1690298113,
	EventInfo__ctor_m910283199,
	EventInfo_get_EventHandlerType_m3748158604,
	EventInfo_get_MemberType_m3944376584,
	AddEventAdapter__ctor_m1612816262,
	AddEventAdapter_Invoke_m2396517987,
	AddEventAdapter_BeginInvoke_m206482786,
	AddEventAdapter_EndInvoke_m3041319155,
	FieldInfo__ctor_m499818762,
	FieldInfo_get_MemberType_m582595557,
	FieldInfo_get_IsLiteral_m2892539950,
	FieldInfo_get_IsStatic_m2429808216,
	FieldInfo_get_IsNotSerialized_m803345153,
	FieldInfo_SetValue_m1839240114,
	FieldInfo_GetFieldOffset_m1670647838,
	FieldInfo_GetUnmanagedMarshal_m1651794009,
	FieldInfo_get_UMarshal_m1787194349,
	FieldInfo_GetPseudoCustomAttributes_m1553561892,
	MemberFilter__ctor_m2871268628,
	MemberFilter_Invoke_m4055074395,
	MemberFilter_BeginInvoke_m2633951521,
	MemberFilter_EndInvoke_m313368938,
	MemberInfo__ctor_m738365750,
	MemberInfo_get_Module_m1214320798,
	MemberInfoSerializationHolder__ctor_m1793192197,
	MemberInfoSerializationHolder_Serialize_m1394200861,
	MemberInfoSerializationHolder_Serialize_m3986357309,
	MemberInfoSerializationHolder_GetRealObject_m2142596729,
	MethodBase__ctor_m2227469883,
	MethodBase_GetParameterCount_m1185680379,
	MethodBase_Invoke_m3351713316,
	MethodBase_get_CallingConvention_m2543146775,
	MethodBase_get_IsPublic_m3024571211,
	MethodBase_get_IsStatic_m508456077,
	MethodBase_get_IsVirtual_m487307940,
	MethodBase_get_IsAbstract_m2773230650,
	MethodBase_get_next_table_index_m3076840473,
	MethodBase_GetGenericArguments_m3457019161,
	MethodBase_get_ContainsGenericParameters_m2180266399,
	MethodBase_get_IsGenericMethodDefinition_m2490092960,
	MethodBase_get_IsGenericMethod_m3878297800,
	MethodInfo__ctor_m789055071,
	MethodInfo_get_MemberType_m4003411807,
	MethodInfo_get_ReturnType_m945454792,
	MethodInfo_MakeGenericMethod_m1848918275,
	MethodInfo_GetGenericArguments_m254251600,
	MethodInfo_get_IsGenericMethod_m667970124,
	MethodInfo_get_IsGenericMethodDefinition_m4275495935,
	MethodInfo_get_ContainsGenericParameters_m3315267095,
	Missing__ctor_m4139730968,
	Missing__cctor_m11351030,
	Module__ctor_m1002678804,
	Module__cctor_m125837104,
	Module_get_Assembly_m765085037,
	Module_GetCustomAttributes_m3382118587,
	Module_InternalGetTypes_m3630318529,
	Module_GetTypes_m2740322649,
	Module_IsDefined_m1481658834,
	Module_ToString_m4038533380,
	Module_filter_by_type_name_m848320559,
	Module_filter_by_type_name_ignore_case_m1757284250,
	MonoCMethod__ctor_m2895813043,
	MonoCMethod_GetParameters_m790514693,
	MonoCMethod_InternalInvoke_m4055125447,
	MonoCMethod_Invoke_m3006660650,
	MonoCMethod_Invoke_m362486451,
	MonoCMethod_get_MethodHandle_m3185544190,
	MonoCMethod_get_Attributes_m3566437198,
	MonoCMethod_get_CallingConvention_m3969797515,
	MonoCMethod_get_ReflectedType_m4171658207,
	MonoCMethod_get_DeclaringType_m671002914,
	MonoCMethod_get_Name_m1727551819,
	MonoCMethod_IsDefined_m2209325791,
	MonoCMethod_GetCustomAttributes_m3478280530,
	MonoCMethod_GetCustomAttributes_m1821120655,
	MonoCMethod_ToString_m2243555642,
	MonoEvent__ctor_m1226517236,
	MonoEvent_get_Attributes_m2933370075,
	MonoEvent_GetAddMethod_m4239159506,
	MonoEvent_get_DeclaringType_m2267637479,
	MonoEvent_get_ReflectedType_m1588732160,
	MonoEvent_get_Name_m3169484882,
	MonoEvent_ToString_m193961336,
	MonoEvent_IsDefined_m2332101501,
	MonoEvent_GetCustomAttributes_m3888560627,
	MonoEvent_GetCustomAttributes_m1886948559,
	MonoEventInfo_get_event_info_m99066771,
	MonoEventInfo_GetEventInfo_m3621453625,
	MonoField__ctor_m4070616665,
	MonoField_get_Attributes_m1135577176,
	MonoField_get_FieldHandle_m3720439131,
	MonoField_get_FieldType_m2853174462,
	MonoField_GetParentType_m4103890175,
	MonoField_get_ReflectedType_m3972152971,
	MonoField_get_DeclaringType_m1121037054,
	MonoField_get_Name_m596017830,
	MonoField_IsDefined_m3211024192,
	MonoField_GetCustomAttributes_m947388369,
	MonoField_GetCustomAttributes_m93951468,
	MonoField_GetFieldOffset_m1899891901,
	MonoField_GetValueInternal_m4270465109,
	MonoField_GetValue_m3005604917,
	MonoField_ToString_m1770889927,
	MonoField_SetValueInternal_m990605174,
	MonoField_SetValue_m326982726,
	MonoField_CheckGeneric_m1090723102,
	MonoGenericCMethod__ctor_m2398772755,
	MonoGenericCMethod_get_ReflectedType_m1913009136,
	MonoGenericMethod__ctor_m1702695577,
	MonoGenericMethod_get_ReflectedType_m3851317345,
	MonoMethod__ctor_m1390794006,
	MonoMethod_get_name_m949262704,
	MonoMethod_get_base_definition_m51581349,
	MonoMethod_GetBaseDefinition_m249649666,
	MonoMethod_get_ReturnType_m2613085097,
	MonoMethod_GetParameters_m740194689,
	MonoMethod_InternalInvoke_m542378430,
	MonoMethod_Invoke_m465456020,
	MonoMethod_get_MethodHandle_m2035895019,
	MonoMethod_get_Attributes_m402091681,
	MonoMethod_get_CallingConvention_m3515242436,
	MonoMethod_get_ReflectedType_m777453553,
	MonoMethod_get_DeclaringType_m3952561631,
	MonoMethod_get_Name_m1963247321,
	MonoMethod_IsDefined_m2816321146,
	MonoMethod_GetCustomAttributes_m809032985,
	MonoMethod_GetCustomAttributes_m951681242,
	MonoMethod_GetDllImportAttribute_m1781219005,
	MonoMethod_GetPseudoCustomAttributes_m3930606169,
	MonoMethod_ShouldPrintFullName_m3366167260,
	MonoMethod_ToString_m497053873,
	MonoMethod_MakeGenericMethod_m786043505,
	MonoMethod_MakeGenericMethod_impl_m2299472065,
	MonoMethod_GetGenericArguments_m2628852535,
	MonoMethod_get_IsGenericMethodDefinition_m3164616112,
	MonoMethod_get_IsGenericMethod_m151038164,
	MonoMethod_get_ContainsGenericParameters_m966846008,
	MonoMethodInfo_get_method_info_m3673440566,
	MonoMethodInfo_GetMethodInfo_m1772785548,
	MonoMethodInfo_GetDeclaringType_m3185305103,
	MonoMethodInfo_GetReturnType_m4027736285,
	MonoMethodInfo_GetAttributes_m2985493472,
	MonoMethodInfo_GetCallingConvention_m173365556,
	MonoMethodInfo_get_parameter_info_m3111561253,
	MonoMethodInfo_GetParametersInfo_m2428136279,
	MonoProperty__ctor_m1812699322,
	MonoProperty_CachePropertyInfo_m692654432,
	MonoProperty_get_Attributes_m3368325028,
	MonoProperty_get_CanRead_m1741287715,
	MonoProperty_get_CanWrite_m2077594845,
	MonoProperty_get_PropertyType_m694198643,
	MonoProperty_get_ReflectedType_m2866745655,
	MonoProperty_get_DeclaringType_m67029977,
	MonoProperty_get_Name_m3968699540,
	MonoProperty_GetAccessors_m2775370848,
	MonoProperty_GetGetMethod_m4275470950,
	MonoProperty_GetIndexParameters_m3544968989,
	MonoProperty_GetSetMethod_m579251830,
	MonoProperty_IsDefined_m3261039459,
	MonoProperty_GetCustomAttributes_m563003902,
	MonoProperty_GetCustomAttributes_m3357860972,
	MonoProperty_CreateGetterDelegate_m3913706848,
	MonoProperty_GetValue_m3818167631,
	MonoProperty_GetValue_m264044638,
	MonoProperty_SetValue_m236087792,
	MonoProperty_ToString_m618904038,
	MonoProperty_GetOptionalCustomModifiers_m3230855865,
	MonoProperty_GetRequiredCustomModifiers_m2199146939,
	MonoProperty_GetObjectData_m812207635,
	GetterAdapter__ctor_m3057432183,
	GetterAdapter_Invoke_m4219243567,
	GetterAdapter_BeginInvoke_m1327405488,
	GetterAdapter_EndInvoke_m1717440936,
	MonoPropertyInfo_get_property_info_m2439647802,
	MonoPropertyInfo_GetTypeModifiers_m366762850,
	ParameterInfo__ctor_m73859067,
	ParameterInfo__ctor_m3408505972,
	ParameterInfo__ctor_m2243459569,
	ParameterInfo_ToString_m4003100681,
	ParameterInfo_get_ParameterType_m4183362478,
	ParameterInfo_get_Attributes_m2978486817,
	ParameterInfo_get_IsIn_m1843450765,
	ParameterInfo_get_IsOptional_m1616592526,
	ParameterInfo_get_IsOut_m4088738266,
	ParameterInfo_get_IsRetval_m1400777994,
	ParameterInfo_get_Member_m3384122622,
	ParameterInfo_get_Name_m132787823,
	ParameterInfo_get_Position_m4138316983,
	ParameterInfo_GetCustomAttributes_m2182009624,
	ParameterInfo_IsDefined_m871444883,
	ParameterInfo_GetPseudoCustomAttributes_m3146711034,
	Pointer__ctor_m48965945,
	Pointer_Box_m1999707857,
	PropertyInfo__ctor_m1356365561,
	PropertyInfo_get_MemberType_m199392709,
	PropertyInfo_GetValue_m1153076048,
	PropertyInfo_SetValue_m3460960658,
	PropertyInfo_GetOptionalCustomModifiers_m3574501446,
	PropertyInfo_GetRequiredCustomModifiers_m2250392716,
	StrongNameKeyPair__ctor_m1018690296,
	StrongNameKeyPair_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m64041457,
	TargetException__ctor_m473994234,
	TargetException__ctor_m1461726186,
	TargetException__ctor_m424977850,
	TargetInvocationException__ctor_m3597313075,
	TargetInvocationException__ctor_m3012971882,
	TargetParameterCountException__ctor_m3690859315,
	TargetParameterCountException__ctor_m1313217406,
	TargetParameterCountException__ctor_m3120156734,
	TypeFilter__ctor_m1878643257,
	TypeFilter_Invoke_m3030351165,
	TypeFilter_BeginInvoke_m3323671216,
	TypeFilter_EndInvoke_m4094864109,
	ResolveEventArgs__ctor_m93077578,
	ResolveEventHandler__ctor_m3187065900,
	ResolveEventHandler_Invoke_m1841633108,
	ResolveEventHandler_BeginInvoke_m427632625,
	ResolveEventHandler_EndInvoke_m4137252560,
	NeutralResourcesLanguageAttribute__ctor_m1815010898,
	ResourceManager__ctor_m3572514865,
	ResourceManager__cctor_m3184761503,
	ResourceReader__ctor_m3266399222,
	ResourceReader__ctor_m1277725863,
	ResourceReader_System_Collections_IEnumerable_GetEnumerator_m3288141321,
	ResourceReader_System_IDisposable_Dispose_m1291672711,
	ResourceReader_ReadHeaders_m3430125333,
	ResourceReader_CreateResourceInfo_m3585089141,
	ResourceReader_Read7BitEncodedInt_m1423726517,
	ResourceReader_ReadValueVer2_m1431409153,
	ResourceReader_ReadValueVer1_m2263386586,
	ResourceReader_ReadNonPredefinedValue_m2833350557,
	ResourceReader_LoadResourceValues_m423366887,
	ResourceReader_Close_m1439213757,
	ResourceReader_GetEnumerator_m1325379635,
	ResourceReader_Dispose_m1468634256,
	ResourceCacheItem__ctor_m909295167_AdjustorThunk,
	ResourceEnumerator__ctor_m1885558666,
	ResourceEnumerator_get_Entry_m1054530742,
	ResourceEnumerator_get_Key_m390468842,
	ResourceEnumerator_get_Value_m1319048932,
	ResourceEnumerator_get_Current_m284725302,
	ResourceEnumerator_MoveNext_m1494841905,
	ResourceEnumerator_Reset_m2674885615,
	ResourceEnumerator_FillCache_m3498559180,
	ResourceInfo__ctor_m297963896_AdjustorThunk,
	ResourceSet__ctor_m1489955567,
	ResourceSet__ctor_m212014079,
	ResourceSet__ctor_m1948322296,
	ResourceSet__ctor_m909158515,
	ResourceSet_System_Collections_IEnumerable_GetEnumerator_m3678908000,
	ResourceSet_Dispose_m404370153,
	ResourceSet_Dispose_m527036616,
	ResourceSet_GetEnumerator_m2033896022,
	ResourceSet_GetObjectInternal_m1910120986,
	ResourceSet_GetObject_m1742653053,
	ResourceSet_GetObject_m1921946804,
	ResourceSet_ReadResources_m928108619,
	RuntimeResourceSet__ctor_m3660273965,
	RuntimeResourceSet__ctor_m4023448617,
	RuntimeResourceSet__ctor_m2855192960,
	RuntimeResourceSet_GetObject_m1369852933,
	RuntimeResourceSet_GetObject_m373416841,
	RuntimeResourceSet_CloneDisposableObjectIfPossible_m3448549430,
	SatelliteContractVersionAttribute__ctor_m4270552604,
	CompilationRelaxationsAttribute__ctor_m1638540989,
	CompilerGeneratedAttribute__ctor_m903969540,
	DecimalConstantAttribute__ctor_m579883733,
	DefaultDependencyAttribute__ctor_m552191618,
	FixedBufferAttribute__ctor_m2305776628,
	FixedBufferAttribute_get_ElementType_m784922463,
	FixedBufferAttribute_get_Length_m1055835580,
	InternalsVisibleToAttribute__ctor_m2209294008,
	RuntimeCompatibilityAttribute__ctor_m853651104,
	RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m4154715430,
	RuntimeHelpers_InitializeArray_m751293544,
	RuntimeHelpers_InitializeArray_m22859371,
	RuntimeHelpers_get_OffsetToStringData_m2182702420,
	StringFreezingAttribute__ctor_m1275687280,
	CriticalFinalizerObject__ctor_m1083616025,
	CriticalFinalizerObject_Finalize_m82872096,
	ReliabilityContractAttribute__ctor_m2586077688,
	ClassInterfaceAttribute__ctor_m2759037627,
	ComDefaultInterfaceAttribute__ctor_m1937741735,
	COMException__ctor_m314442114,
	COMException__ctor_m2380647294,
	COMException_ToString_m592648057,
	ComImportAttribute__ctor_m2388436170,
	ComVisibleAttribute__ctor_m29339631,
	DispIdAttribute__ctor_m2341845790,
	DllImportAttribute__ctor_m3723685963,
	DllImportAttribute_get_Value_m4024763376,
	ExternalException__ctor_m2380751802,
	ExternalException__ctor_m1530163908,
	FieldOffsetAttribute__ctor_m2729649564,
	GCHandle__ctor_m2029694511_AdjustorThunk,
	GCHandle_get_IsAllocated_m1141928539_AdjustorThunk,
	GCHandle_get_Target_m342603595_AdjustorThunk,
	GCHandle_Alloc_m3489159186,
	GCHandle_Free_m921347623_AdjustorThunk,
	GCHandle_GetTarget_m3327406846,
	GCHandle_GetTargetHandle_m4231843731,
	GCHandle_FreeHandle_m106505230,
	GCHandle_Equals_m3225201818_AdjustorThunk,
	GCHandle_GetHashCode_m2365118290_AdjustorThunk,
	GuidAttribute__ctor_m612200210,
	InAttribute__ctor_m228923551,
	InterfaceTypeAttribute__ctor_m342415882,
	Marshal__cctor_m2969155085,
	Marshal_copy_from_unmanaged_m1563762894,
	Marshal_Copy_m3898306904,
	Marshal_Copy_m3204855539,
	Marshal_ReadByte_m943398470,
	Marshal_WriteByte_m199556577,
	MarshalAsAttribute__ctor_m1960246711,
	MarshalDirectiveException__ctor_m961324852,
	MarshalDirectiveException__ctor_m4187866271,
	OptionalAttribute__ctor_m2155356768,
	OutAttribute__ctor_m1442270007,
	PreserveSigAttribute__ctor_m2960960257,
	SafeHandle__ctor_m20409610,
	SafeHandle_Close_m106241507,
	SafeHandle_DangerousAddRef_m3068760737,
	SafeHandle_DangerousGetHandle_m987171088,
	SafeHandle_DangerousRelease_m2343451392,
	SafeHandle_Dispose_m3045814524,
	SafeHandle_Dispose_m1778975602,
	SafeHandle_SetHandle_m105306455,
	SafeHandle_Finalize_m2145306507,
	TypeLibImportClassAttribute__ctor_m1502349925,
	TypeLibVersionAttribute__ctor_m3431410536,
	UnmanagedFunctionPointerAttribute__ctor_m323663599,
	ActivatedClientTypeEntry__ctor_m1819695246,
	ActivatedClientTypeEntry_get_ApplicationUrl_m3873673798,
	ActivatedClientTypeEntry_get_ContextAttributes_m3033756077,
	ActivatedClientTypeEntry_get_ObjectType_m1878753871,
	ActivatedClientTypeEntry_ToString_m2855942163,
	ActivatedServiceTypeEntry__ctor_m3343564895,
	ActivatedServiceTypeEntry_get_ObjectType_m1241835709,
	ActivatedServiceTypeEntry_ToString_m4113167625,
	ActivationServices_get_ConstructionActivator_m2979483328,
	ActivationServices_CreateProxyFromAttributes_m1090915031,
	ActivationServices_CreateConstructionCall_m2208125204,
	ActivationServices_AllocateUninitializedClassInstance_m2119168546,
	ActivationServices_EnableProxyActivation_m3920844405,
	AppDomainLevelActivator__ctor_m255127195,
	ConstructionLevelActivator__ctor_m2437471156,
	ContextLevelActivator__ctor_m3317000986,
	UrlAttribute_get_UrlValue_m2650265094,
	UrlAttribute_Equals_m4185440562,
	UrlAttribute_GetHashCode_m324081125,
	UrlAttribute_GetPropertiesForNewContext_m3068588212,
	UrlAttribute_IsContextOK_m4070904995,
	ChannelData__ctor_m3849569811,
	ChannelData_get_ServerProviders_m852758128,
	ChannelData_get_ClientProviders_m1218169940,
	ChannelData_get_CustomProperties_m3398830784,
	ChannelData_CopyFrom_m2178894067,
	ChannelInfo__ctor_m3311074949,
	ChannelInfo_get_ChannelData_m1785884967,
	ChannelServices__cctor_m3336674656,
	ChannelServices_CreateClientChannelSinkChain_m1344844420,
	ChannelServices_CreateClientChannelSinkChain_m3719553906,
	ChannelServices_RegisterChannel_m1635994151,
	ChannelServices_RegisterChannel_m394917815,
	ChannelServices_RegisterChannelConfig_m4010407106,
	ChannelServices_CreateProvider_m128710911,
	ChannelServices_GetCurrentChannelInfo_m4213029739,
	CrossAppDomainChannel__ctor_m4072987578,
	CrossAppDomainChannel__cctor_m2249114501,
	CrossAppDomainChannel_RegisterCrossAppDomainChannel_m4263628836,
	CrossAppDomainChannel_get_ChannelName_m1336765459,
	CrossAppDomainChannel_get_ChannelPriority_m2991035339,
	CrossAppDomainChannel_get_ChannelData_m1576303390,
	CrossAppDomainChannel_StartListening_m2105934298,
	CrossAppDomainChannel_CreateMessageSink_m884114675,
	CrossAppDomainData__ctor_m2872468782,
	CrossAppDomainData_get_DomainID_m2201551882,
	CrossAppDomainData_get_ProcessID_m2703215934,
	CrossAppDomainSink__ctor_m3633578688,
	CrossAppDomainSink__cctor_m4258184287,
	CrossAppDomainSink_GetSink_m1074615250,
	CrossAppDomainSink_get_TargetDomainId_m3724016769,
	SinkProviderData__ctor_m441492975,
	SinkProviderData_get_Children_m3216313090,
	SinkProviderData_get_Properties_m1995188978,
	ClientActivatedIdentity_GetServerObject_m709948078,
	ClientIdentity__ctor_m4235777231,
	ClientIdentity_get_ClientProxy_m630894956,
	ClientIdentity_set_ClientProxy_m3756692934,
	ClientIdentity_CreateObjRef_m606049357,
	ClientIdentity_get_TargetUri_m32374781,
	ConfigHandler__ctor_m3270116852,
	ConfigHandler_ValidatePath_m3545750813,
	ConfigHandler_CheckPath_m2304131255,
	ConfigHandler_OnStartParsing_m430780344,
	ConfigHandler_OnProcessingInstruction_m3787845528,
	ConfigHandler_OnIgnorableWhitespace_m189830949,
	ConfigHandler_OnStartElement_m112480001,
	ConfigHandler_ParseElement_m3001301197,
	ConfigHandler_OnEndElement_m1813247236,
	ConfigHandler_ReadCustomProviderData_m2457933861,
	ConfigHandler_ReadLifetine_m2261548993,
	ConfigHandler_ParseTime_m2994502506,
	ConfigHandler_ReadChannel_m1878857123,
	ConfigHandler_ReadProvider_m2393547467,
	ConfigHandler_ReadClientActivated_m2054330218,
	ConfigHandler_ReadServiceActivated_m2908609747,
	ConfigHandler_ReadClientWellKnown_m4188255355,
	ConfigHandler_ReadServiceWellKnown_m3819404742,
	ConfigHandler_ReadInteropXml_m1112646852,
	ConfigHandler_ReadPreload_m2358582171,
	ConfigHandler_GetNotNull_m2724870242,
	ConfigHandler_ExtractAssembly_m3414662013,
	ConfigHandler_OnChars_m2042791030,
	ConfigHandler_OnEndParsing_m1466408695,
	Context__ctor_m2358096070,
	Context__cctor_m154458549,
	Context_Finalize_m4146766630,
	Context_get_DefaultContext_m3129763904,
	Context_get_ContextID_m872742819,
	Context_get_ContextProperties_m3483560853,
	Context_get_IsDefaultContext_m1695961388,
	Context_get_NeedsContextSink_m4170912896,
	Context_RegisterDynamicProperty_m1394278564,
	Context_UnregisterDynamicProperty_m3767934252,
	Context_GetDynamicPropertyCollection_m4222645020,
	Context_NotifyGlobalDynamicSinks_m3154063652,
	Context_get_HasGlobalDynamicSinks_m2219829718,
	Context_NotifyDynamicSinks_m1606948064,
	Context_get_HasDynamicSinks_m166659476,
	Context_get_HasExitSinks_m3583696172,
	Context_GetProperty_m1437983912,
	Context_SetProperty_m2564048121,
	Context_Freeze_m2981637364,
	Context_ToString_m3508388157,
	Context_GetServerContextSinkChain_m41058259,
	Context_GetClientContextSinkChain_m4126474018,
	Context_CreateServerObjectSinkChain_m55752487,
	Context_CreateEnvoySink_m3796096298,
	Context_SwitchToContext_m683333002,
	Context_CreateNewContext_m3406392012,
	Context_DoCallBack_m1276336576,
	Context_AllocateDataSlot_m2865612260,
	Context_AllocateNamedDataSlot_m1602448103,
	Context_FreeNamedDataSlot_m1826329033,
	Context_GetData_m1620126795,
	Context_GetNamedDataSlot_m985647519,
	Context_SetData_m4157435612,
	ContextAttribute__ctor_m3943029900,
	ContextAttribute_get_Name_m2590719021,
	ContextAttribute_Equals_m281624901,
	ContextAttribute_Freeze_m3525118109,
	ContextAttribute_GetHashCode_m1066514836,
	ContextAttribute_GetPropertiesForNewContext_m3862978192,
	ContextAttribute_IsContextOK_m4085172616,
	ContextAttribute_IsNewContextOK_m2757508237,
	ContextCallbackObject__ctor_m1057769426,
	ContextCallbackObject_DoCallBack_m2365561453,
	CrossContextChannel__ctor_m1492884516,
	CrossContextDelegate__ctor_m2049533611,
	CrossContextDelegate_Invoke_m3821704953,
	CrossContextDelegate_BeginInvoke_m3957885468,
	CrossContextDelegate_EndInvoke_m150737163,
	DynamicPropertyCollection__ctor_m4108681380,
	DynamicPropertyCollection_get_HasProperties_m1156612739,
	DynamicPropertyCollection_RegisterDynamicProperty_m117639120,
	DynamicPropertyCollection_UnregisterDynamicProperty_m3421206888,
	DynamicPropertyCollection_NotifyMessage_m3140567461,
	DynamicPropertyCollection_FindProperty_m1056171965,
	DynamicPropertyReg__ctor_m1113221123,
	SynchronizationAttribute__ctor_m548131118,
	SynchronizationAttribute__ctor_m1680658202,
	SynchronizationAttribute_set_Locked_m3609947079,
	SynchronizationAttribute_ReleaseLock_m141792516,
	SynchronizationAttribute_GetPropertiesForNewContext_m1138224242,
	SynchronizationAttribute_GetClientContextSink_m1220088372,
	SynchronizationAttribute_GetServerContextSink_m4260393252,
	SynchronizationAttribute_IsContextOK_m3271333896,
	SynchronizationAttribute_ExitContext_m3277325561,
	SynchronizationAttribute_EnterContext_m970367295,
	SynchronizedClientContextSink__ctor_m3191072250,
	SynchronizedServerContextSink__ctor_m3863347475,
	EnvoyInfo__ctor_m3541351662,
	EnvoyInfo_get_EnvoySinks_m1163783610,
	FormatterData__ctor_m1072025512,
	Identity__ctor_m2977509058,
	Identity_get_ChannelSink_m2802483952,
	Identity_set_ChannelSink_m3209362843,
	Identity_get_ObjectUri_m1367937621,
	Identity_get_Disposed_m3822893581,
	Identity_set_Disposed_m1142682364,
	Identity_get_ClientDynamicProperties_m224584693,
	Identity_get_ServerDynamicProperties_m4177201799,
	InternalRemotingServices__cctor_m3528201971,
	InternalRemotingServices_GetCachedSoapAttribute_m676971033,
	LeaseManager__ctor_m2498920970,
	LeaseManager_SetPollTime_m2675030242,
	LeaseSink__ctor_m1931990775,
	LifetimeServices__cctor_m1930292675,
	LifetimeServices_set_LeaseManagerPollTime_m3041349301,
	LifetimeServices_set_LeaseTime_m4027116677,
	LifetimeServices_set_RenewOnCallTime_m1807637854,
	LifetimeServices_set_SponsorshipTimeout_m4057621160,
	ArgInfo__ctor_m4159974758,
	ArgInfo_GetInOutArgs_m3181936204,
	AsyncResult__ctor_m1801982601,
	AsyncResult_get_AsyncState_m3143114484,
	AsyncResult_get_AsyncWaitHandle_m529416838,
	AsyncResult_get_CompletedSynchronously_m1821815858,
	AsyncResult_get_IsCompleted_m3014818534,
	AsyncResult_get_EndInvokeCalled_m2256230767,
	AsyncResult_set_EndInvokeCalled_m1141779661,
	AsyncResult_get_AsyncDelegate_m3035985913,
	AsyncResult_get_NextSink_m3138582479,
	AsyncResult_AsyncProcessMessage_m652707586,
	AsyncResult_GetReplyMessage_m1358645350,
	AsyncResult_SetMessageCtrl_m2108631088,
	AsyncResult_SetCompletedSynchronously_m847983035,
	AsyncResult_EndInvoke_m172785111,
	AsyncResult_SyncProcessMessage_m3292051227,
	AsyncResult_get_CallMessage_m220967056,
	AsyncResult_set_CallMessage_m3225294145,
	CallContextRemotingData__ctor_m1902173160,
	CallContextRemotingData_Clone_m735009638,
	ClientContextTerminatorSink__ctor_m813223473,
	ConstructionCall__ctor_m1095431613,
	ConstructionCall__ctor_m2101270576,
	ConstructionCall_InitDictionary_m2564472215,
	ConstructionCall_set_IsContextOk_m3644001000,
	ConstructionCall_get_ActivationType_m2655866177,
	ConstructionCall_get_ActivationTypeName_m2245962217,
	ConstructionCall_get_Activator_m2794670563,
	ConstructionCall_set_Activator_m894344884,
	ConstructionCall_get_CallSiteActivationAttributes_m2423405968,
	ConstructionCall_SetActivationAttributes_m3955865312,
	ConstructionCall_get_ContextProperties_m3161236960,
	ConstructionCall_InitMethodProperty_m2720989452,
	ConstructionCall_get_Properties_m3202231363,
	ConstructionCallDictionary__ctor_m1181026802,
	ConstructionCallDictionary__cctor_m96353376,
	ConstructionCallDictionary_GetMethodProperty_m2304169240,
	ConstructionCallDictionary_SetMethodProperty_m552722302,
	EnvoyTerminatorSink__ctor_m859706469,
	EnvoyTerminatorSink__cctor_m2706474606,
	Header__ctor_m3826209702,
	Header__ctor_m1479211447,
	Header__ctor_m243167164,
	HeaderHandler__ctor_m1135507152,
	HeaderHandler_Invoke_m3271043078,
	HeaderHandler_BeginInvoke_m1603080229,
	HeaderHandler_EndInvoke_m1281900991,
	LogicalCallContext__ctor_m2817560434,
	LogicalCallContext__ctor_m1410930345,
	LogicalCallContext_SetData_m3089875600,
	LogicalCallContext_Clone_m3866404782,
	MethodCall__ctor_m884744044,
	MethodCall__ctor_m1742009723,
	MethodCall__ctor_m3497155135,
	MethodCall_System_Runtime_Remoting_Messaging_IInternalMessage_set_Uri_m3838354409,
	MethodCall_InitMethodProperty_m1459153458,
	MethodCall_get_Args_m2716733821,
	MethodCall_get_LogicalCallContext_m4057972022,
	MethodCall_get_MethodBase_m154116763,
	MethodCall_get_MethodName_m3514373525,
	MethodCall_get_MethodSignature_m231438085,
	MethodCall_get_Properties_m1466387397,
	MethodCall_InitDictionary_m3585539223,
	MethodCall_get_TypeName_m2708109644,
	MethodCall_get_Uri_m3826928422,
	MethodCall_set_Uri_m193158428,
	MethodCall_Init_m4035917907,
	MethodCall_ResolveMethod_m652866482,
	MethodCall_CastTo_m3102252139,
	MethodCall_GetTypeNameFromAssemblyQualifiedName_m1514492318,
	MethodCall_get_GenericArguments_m2561245121,
	MethodCallDictionary__ctor_m1706706460,
	MethodCallDictionary__cctor_m14416868,
	MethodDictionary__ctor_m2350470509,
	MethodDictionary_System_Collections_IEnumerable_GetEnumerator_m2001247713,
	MethodDictionary_set_MethodKeys_m2384280808,
	MethodDictionary_AllocInternalProperties_m3279357487,
	MethodDictionary_GetInternalProperties_m2405237846,
	MethodDictionary_IsOverridenKey_m3505085623,
	MethodDictionary_get_Item_m1955409235,
	MethodDictionary_set_Item_m94361284,
	MethodDictionary_GetMethodProperty_m4241101875,
	MethodDictionary_SetMethodProperty_m148196079,
	MethodDictionary_get_Values_m3928398530,
	MethodDictionary_Add_m1681874019,
	MethodDictionary_Remove_m2994596185,
	MethodDictionary_get_Count_m705114034,
	MethodDictionary_get_SyncRoot_m725319460,
	MethodDictionary_CopyTo_m1829913959,
	MethodDictionary_GetEnumerator_m3231228815,
	DictionaryEnumerator__ctor_m3172994930,
	DictionaryEnumerator_get_Current_m25673846,
	DictionaryEnumerator_MoveNext_m2795532047,
	DictionaryEnumerator_Reset_m739841666,
	DictionaryEnumerator_get_Entry_m1836786350,
	DictionaryEnumerator_get_Key_m1817568991,
	DictionaryEnumerator_get_Value_m611140825,
	MethodReturnDictionary__ctor_m1050817137,
	MethodReturnDictionary__cctor_m2264198370,
	MonoMethodMessage_get_Args_m2551945928,
	MonoMethodMessage_get_LogicalCallContext_m1259579640,
	MonoMethodMessage_get_MethodBase_m2960378457,
	MonoMethodMessage_get_MethodName_m3948558933,
	MonoMethodMessage_get_MethodSignature_m3074315531,
	MonoMethodMessage_get_TypeName_m2135069458,
	MonoMethodMessage_get_Uri_m148660560,
	MonoMethodMessage_set_Uri_m4050398394,
	MonoMethodMessage_get_Exception_m3516160438,
	MonoMethodMessage_get_OutArgCount_m3426781770,
	MonoMethodMessage_get_OutArgs_m3941705659,
	MonoMethodMessage_get_ReturnValue_m68851356,
	ObjRefSurrogate__ctor_m3417937042,
	ObjRefSurrogate_SetObjectData_m4212308433,
	RemotingSurrogate__ctor_m4190060429,
	RemotingSurrogate_SetObjectData_m748538920,
	RemotingSurrogateSelector__ctor_m3096157960,
	RemotingSurrogateSelector__cctor_m1496344619,
	RemotingSurrogateSelector_GetSurrogate_m245554266,
	ReturnMessage__ctor_m87180958,
	ReturnMessage__ctor_m1524202706,
	ReturnMessage_System_Runtime_Remoting_Messaging_IInternalMessage_set_Uri_m1763195932,
	ReturnMessage_get_Args_m1370072984,
	ReturnMessage_get_LogicalCallContext_m2771752071,
	ReturnMessage_get_MethodBase_m234441048,
	ReturnMessage_get_MethodName_m4111095637,
	ReturnMessage_get_MethodSignature_m3528607433,
	ReturnMessage_get_Properties_m2680922070,
	ReturnMessage_get_TypeName_m260498371,
	ReturnMessage_get_Uri_m553618093,
	ReturnMessage_set_Uri_m3542991743,
	ReturnMessage_get_Exception_m2983507843,
	ReturnMessage_get_OutArgs_m3497822294,
	ReturnMessage_get_ReturnValue_m3326584974,
	ServerContextTerminatorSink__ctor_m3664219034,
	ServerObjectTerminatorSink__ctor_m3525098792,
	StackBuilderSink__ctor_m64101652,
	SoapAttribute__ctor_m518099028,
	SoapAttribute_get_UseAttribute_m1707629656,
	SoapAttribute_get_XmlNamespace_m1834257290,
	SoapAttribute_SetReflectionObject_m4126214974,
	SoapFieldAttribute__ctor_m3774082701,
	SoapFieldAttribute_get_XmlElementName_m2058178999,
	SoapFieldAttribute_IsInteropXmlElement_m3647752714,
	SoapFieldAttribute_SetReflectionObject_m1610909618,
	SoapMethodAttribute__ctor_m2287295344,
	SoapMethodAttribute_get_UseAttribute_m2463822588,
	SoapMethodAttribute_get_XmlNamespace_m1545598731,
	SoapMethodAttribute_SetReflectionObject_m801625592,
	SoapParameterAttribute__ctor_m3529185311,
	SoapTypeAttribute__ctor_m1846542587,
	SoapTypeAttribute_get_UseAttribute_m2333117870,
	SoapTypeAttribute_get_XmlElementName_m3022579292,
	SoapTypeAttribute_get_XmlNamespace_m1625614089,
	SoapTypeAttribute_get_XmlTypeName_m90744578,
	SoapTypeAttribute_get_XmlTypeNamespace_m4178519243,
	SoapTypeAttribute_get_IsInteropXmlElement_m2959121094,
	SoapTypeAttribute_get_IsInteropXmlType_m3762924927,
	SoapTypeAttribute_SetReflectionObject_m3882901426,
	ObjRef__ctor_m2786642265,
	ObjRef__ctor_m3254298855,
	ObjRef__cctor_m871093012,
	ObjRef_get_IsReferenceToWellKnow_m2224633975,
	ObjRef_get_ChannelInfo_m3336748132,
	ObjRef_get_EnvoyInfo_m2326759189,
	ObjRef_set_EnvoyInfo_m1158264358,
	ObjRef_get_TypeInfo_m2948790669,
	ObjRef_set_TypeInfo_m2537720298,
	ObjRef_get_URI_m3387765577,
	ObjRef_set_URI_m3186050253,
	ObjRef_GetRealObject_m4233575813,
	ObjRef_UpdateChannelInfo_m1580754822,
	ObjRef_get_ServerType_m2919537993,
	ProviderData__ctor_m1369545012,
	ProviderData_CopyFrom_m3371583065,
	ProxyAttribute_CreateInstance_m2180868281,
	ProxyAttribute_CreateProxy_m3870939658,
	ProxyAttribute_GetPropertiesForNewContext_m3937741509,
	ProxyAttribute_IsContextOK_m4171219307,
	RealProxy__ctor_m3226966492,
	RealProxy__ctor_m2536418102,
	RealProxy__ctor_m58325570,
	RealProxy_InternalGetProxyType_m3275084161,
	RealProxy_GetProxiedType_m162235532,
	RealProxy_get_ObjectIdentity_m3187017140,
	RealProxy_InternalGetTransparentProxy_m1418543192,
	RealProxy_GetTransparentProxy_m2302031412,
	RealProxy_SetTargetDomain_m290727647,
	RemotingProxy__ctor_m34659926,
	RemotingProxy__ctor_m3434603057,
	RemotingProxy__cctor_m2837767763,
	RemotingProxy_get_TypeName_m2112259464,
	RemotingProxy_Finalize_m525159468,
	RemotingConfiguration__cctor_m1279995388,
	RemotingConfiguration_get_ApplicationName_m2327332192,
	RemotingConfiguration_set_ApplicationName_m3067372830,
	RemotingConfiguration_get_ProcessId_m809639351,
	RemotingConfiguration_LoadDefaultDelayedChannels_m4234959241,
	RemotingConfiguration_IsRemotelyActivatedClientType_m3273590860,
	RemotingConfiguration_RegisterActivatedClientType_m199437308,
	RemotingConfiguration_RegisterActivatedServiceType_m787552533,
	RemotingConfiguration_RegisterWellKnownClientType_m1023771625,
	RemotingConfiguration_RegisterWellKnownServiceType_m3537794122,
	RemotingConfiguration_RegisterChannelTemplate_m542601404,
	RemotingConfiguration_RegisterClientProviderTemplate_m945610249,
	RemotingConfiguration_RegisterServerProviderTemplate_m304276082,
	RemotingConfiguration_RegisterChannels_m1399160752,
	RemotingConfiguration_RegisterTypes_m931646882,
	RemotingConfiguration_SetCustomErrorsMode_m4130863040,
	RemotingException__ctor_m293416780,
	RemotingException__ctor_m1492161056,
	RemotingException__ctor_m1971504521,
	RemotingException__ctor_m3202151879,
	RemotingServices__cctor_m1277049829,
	RemotingServices_GetVirtualMethod_m2518771061,
	RemotingServices_IsTransparentProxy_m744747325,
	RemotingServices_GetServerTypeForUri_m550359732,
	RemotingServices_Unmarshal_m1289686908,
	RemotingServices_Unmarshal_m778773009,
	RemotingServices_GetRealProxy_m256026011,
	RemotingServices_GetMethodBaseFromMethodMessage_m2395724458,
	RemotingServices_GetMethodBaseFromName_m2424336723,
	RemotingServices_FindInterfaceMethod_m1015202004,
	RemotingServices_CreateClientProxy_m107493679,
	RemotingServices_CreateClientProxy_m400991789,
	RemotingServices_CreateClientProxyForContextBound_m203650324,
	RemotingServices_GetIdentityForUri_m3222906492,
	RemotingServices_RemoveAppNameFromUri_m618880628,
	RemotingServices_GetOrCreateClientIdentity_m2784245793,
	RemotingServices_GetClientChannelSinkChain_m457476899,
	RemotingServices_CreateWellKnownServerIdentity_m3649196724,
	RemotingServices_RegisterServerIdentity_m834217415,
	RemotingServices_GetProxyForRemoteObject_m3819863274,
	RemotingServices_GetRemoteObject_m1750813135,
	RemotingServices_RegisterInternalChannels_m275911897,
	RemotingServices_DisposeIdentity_m323595472,
	RemotingServices_GetNormalizedUri_m255324727,
	ServerIdentity__ctor_m1102360821,
	ServerIdentity_get_ObjectType_m512804537,
	ServerIdentity_CreateObjRef_m2011463462,
	TrackingServices__cctor_m2429798832,
	TrackingServices_NotifyUnmarshaledObject_m1471860047,
	SingleCallIdentity__ctor_m1736848410,
	SingletonIdentity__ctor_m1865259725,
	SoapServices__cctor_m2457007482,
	SoapServices_get_XmlNsForClrTypeWithAssembly_m2612822279,
	SoapServices_get_XmlNsForClrTypeWithNs_m3163533917,
	SoapServices_get_XmlNsForClrTypeWithNsAndAssembly_m2640095658,
	SoapServices_CodeXmlNamespaceForClrTypeNamespace_m4133277412,
	SoapServices_GetNameKey_m1092011777,
	SoapServices_GetAssemblyName_m2829699794,
	SoapServices_GetXmlElementForInteropType_m1640193632,
	SoapServices_GetXmlNamespaceForMethodCall_m1056000020,
	SoapServices_GetXmlNamespaceForMethodResponse_m2553670593,
	SoapServices_GetXmlTypeForInteropType_m1005719194,
	SoapServices_PreLoad_m6189216,
	SoapServices_PreLoad_m2104060828,
	SoapServices_RegisterInteropXmlElement_m908353777,
	SoapServices_RegisterInteropXmlType_m2453065344,
	SoapServices_EncodeNs_m2632503700,
	TypeInfo__ctor_m2715261247,
	TypeEntry__ctor_m3575940353,
	TypeEntry_get_AssemblyName_m3054311022,
	TypeEntry_set_AssemblyName_m499963479,
	TypeEntry_get_TypeName_m1232453208,
	TypeEntry_set_TypeName_m1952212701,
	TypeInfo__ctor_m2364486315,
	TypeInfo_get_TypeName_m2863368978,
	WellKnownClientTypeEntry__ctor_m1668350696,
	WellKnownClientTypeEntry_get_ApplicationUrl_m21167485,
	WellKnownClientTypeEntry_get_ObjectType_m4039108672,
	WellKnownClientTypeEntry_get_ObjectUrl_m544692699,
	WellKnownClientTypeEntry_ToString_m3394858157,
	WellKnownServiceTypeEntry__ctor_m2293108517,
	WellKnownServiceTypeEntry_get_Mode_m2923959592,
	WellKnownServiceTypeEntry_get_ObjectType_m1746130983,
	WellKnownServiceTypeEntry_get_ObjectUri_m3935359109,
	WellKnownServiceTypeEntry_ToString_m1431742318,
	ArrayFixupRecord__ctor_m3311888015,
	ArrayFixupRecord_FixupImpl_m1513658972,
	BaseFixupRecord__ctor_m3835164596,
	BaseFixupRecord_DoFixup_m1851697309,
	DelayedFixupRecord__ctor_m2181884632,
	DelayedFixupRecord_FixupImpl_m401936948,
	FixupRecord__ctor_m3656271538,
	FixupRecord_FixupImpl_m797154939,
	FormatterConverter__ctor_m3430055030,
	FormatterConverter_Convert_m3961524211,
	FormatterConverter_ToBoolean_m1798860026,
	FormatterConverter_ToInt16_m1177479242,
	FormatterConverter_ToInt32_m456787332,
	FormatterConverter_ToInt64_m4130265265,
	FormatterConverter_ToString_m3532187504,
	BinaryCommon__cctor_m936375461,
	BinaryCommon_IsPrimitive_m2659553809,
	BinaryCommon_GetTypeFromCode_m1100058929,
	BinaryCommon_SwapBytes_m1969176942,
	BinaryFormatter__ctor_m3409016867,
	BinaryFormatter__ctor_m4243617903,
	BinaryFormatter_get_DefaultSurrogateSelector_m1118519797,
	BinaryFormatter_set_AssemblyFormat_m2806325422,
	BinaryFormatter_get_Binder_m2353889501,
	BinaryFormatter_get_Context_m1683691927,
	BinaryFormatter_get_SurrogateSelector_m2311519614,
	BinaryFormatter_get_FilterLevel_m686970985,
	BinaryFormatter_Deserialize_m2376450417,
	BinaryFormatter_NoCheckDeserialize_m1340843907,
	BinaryFormatter_ReadBinaryHeader_m683624077,
	MessageFormatter_ReadMethodCall_m1820326579,
	MessageFormatter_ReadMethodResponse_m2419236021,
	ObjectReader__ctor_m653516426,
	ObjectReader_ReadObjectGraph_m3185940113,
	ObjectReader_ReadObjectGraph_m2086328326,
	ObjectReader_ReadNextObject_m1157177315,
	ObjectReader_ReadNextObject_m3256368101,
	ObjectReader_get_CurrentObject_m821465781,
	ObjectReader_ReadObject_m3369997476,
	ObjectReader_ReadAssembly_m4242168269,
	ObjectReader_ReadObjectInstance_m1968249236,
	ObjectReader_ReadRefTypeObjectInstance_m2451797494,
	ObjectReader_ReadObjectContent_m3969357595,
	ObjectReader_RegisterObject_m1843472302,
	ObjectReader_ReadStringIntance_m252569550,
	ObjectReader_ReadGenericArray_m1531022411,
	ObjectReader_ReadBoxedPrimitiveTypeValue_m3484375627,
	ObjectReader_ReadArrayOfPrimitiveType_m3868850930,
	ObjectReader_BlockRead_m1857167249,
	ObjectReader_ReadArrayOfObject_m596245004,
	ObjectReader_ReadArrayOfString_m839118322,
	ObjectReader_ReadSimpleArray_m1425418341,
	ObjectReader_ReadTypeMetadata_m339584867,
	ObjectReader_ReadValue_m4196722483,
	ObjectReader_SetObjectValue_m1775786951,
	ObjectReader_RecordFixup_m1730193250,
	ObjectReader_GetDeserializationType_m1430761280,
	ObjectReader_ReadType_m2493401618,
	ObjectReader_ReadPrimitiveTypeValue_m3966667252,
	ArrayNullFiller__ctor_m636923149,
	TypeMetadata__ctor_m327828241,
	FormatterServices_GetUninitializedObject_m666738276,
	FormatterServices_GetSafeUninitializedObject_m1769512035,
	MultiArrayFixupRecord__ctor_m2343861941,
	MultiArrayFixupRecord_FixupImpl_m772772236,
	ObjectManager__ctor_m2047177295,
	ObjectManager_DoFixups_m4157975760,
	ObjectManager_GetObjectRecord_m4081035814,
	ObjectManager_GetObject_m3854971286,
	ObjectManager_RaiseDeserializationEvent_m372858428,
	ObjectManager_RaiseOnDeserializingEvent_m3159578434,
	ObjectManager_RaiseOnDeserializedEvent_m1878246435,
	ObjectManager_AddFixup_m553412757,
	ObjectManager_RecordArrayElementFixup_m1940367603,
	ObjectManager_RecordArrayElementFixup_m665635846,
	ObjectManager_RecordDelayedFixup_m976599896,
	ObjectManager_RecordFixup_m2480262735,
	ObjectManager_RegisterObjectInternal_m1911644234,
	ObjectManager_RegisterObject_m344094489,
	ObjectRecord__ctor_m1406678827,
	ObjectRecord_SetMemberValue_m4070346018,
	ObjectRecord_SetArrayValue_m1003342309,
	ObjectRecord_SetMemberValue_m1871413147,
	ObjectRecord_get_IsInstanceReady_m1438161326,
	ObjectRecord_get_IsUnsolvedObjectReference_m467999924,
	ObjectRecord_get_IsRegistered_m823496992,
	ObjectRecord_DoFixups_m3951361198,
	ObjectRecord_RemoveFixup_m2600767340,
	ObjectRecord_UnchainFixup_m1537704317,
	ObjectRecord_ChainFixup_m1047594955,
	ObjectRecord_LoadData_m4129923566,
	ObjectRecord_get_HasPendingFixups_m3517138698,
	SerializationBinder__ctor_m807311882,
	SerializationCallbacks__ctor_m3030529948,
	SerializationCallbacks__cctor_m3792349727,
	SerializationCallbacks_get_HasDeserializedCallbacks_m2204430898,
	SerializationCallbacks_GetMethodsByAttribute_m1467320038,
	SerializationCallbacks_Invoke_m1529209864,
	SerializationCallbacks_RaiseOnDeserializing_m1899694156,
	SerializationCallbacks_RaiseOnDeserialized_m1392288165,
	SerializationCallbacks_GetSerializationCallbacks_m188332064,
	CallbackHandler__ctor_m3260575867,
	CallbackHandler_Invoke_m3213427438,
	CallbackHandler_BeginInvoke_m3425019071,
	CallbackHandler_EndInvoke_m3352431872,
	SerializationEntry__ctor_m69151158_AdjustorThunk,
	SerializationEntry_get_Name_m122287639_AdjustorThunk,
	SerializationEntry_get_Value_m3922915124_AdjustorThunk,
	SerializationException__ctor_m2606810985,
	SerializationException__ctor_m706051698,
	SerializationException__ctor_m244914060,
	SerializationInfo__ctor_m283007395,
	SerializationInfo_AddValue_m355239916,
	SerializationInfo_GetValue_m1114212844,
	SerializationInfo_SetType_m3163138465,
	SerializationInfo_GetEnumerator_m461080135,
	SerializationInfo_AddValue_m2159499637,
	SerializationInfo_GetBoolean_m500417771,
	SerializationInfo_GetInt16_m3836928717,
	SerializationInfo_GetInt32_m27214234,
	SerializationInfo_GetInt64_m4002248073,
	SerializationInfo_GetString_m2598891044,
	SerializationInfoEnumerator__ctor_m490872531,
	SerializationInfoEnumerator_System_Collections_IEnumerator_get_Current_m2134644067,
	SerializationInfoEnumerator_get_Current_m2556573355,
	SerializationInfoEnumerator_get_Name_m2267113070,
	SerializationInfoEnumerator_get_Value_m3733105889,
	SerializationInfoEnumerator_MoveNext_m3795119086,
	SerializationInfoEnumerator_Reset_m3017682611,
	StreamingContext__ctor_m3383325272_AdjustorThunk,
	StreamingContext__ctor_m3932145525_AdjustorThunk,
	StreamingContext_get_State_m135078841_AdjustorThunk,
	StreamingContext_Equals_m1148347797_AdjustorThunk,
	StreamingContext_GetHashCode_m685689445_AdjustorThunk,
	RuntimeFieldHandle__ctor_m4109695609_AdjustorThunk,
	RuntimeFieldHandle_get_Value_m1454285835_AdjustorThunk,
	RuntimeFieldHandle_Equals_m1824119433_AdjustorThunk,
	RuntimeFieldHandle_GetHashCode_m861234645_AdjustorThunk,
	RuntimeMethodHandle__ctor_m3876746557_AdjustorThunk,
	RuntimeMethodHandle__ctor_m783095629_AdjustorThunk,
	RuntimeMethodHandle_get_Value_m819780389_AdjustorThunk,
	RuntimeMethodHandle_Equals_m2047619131_AdjustorThunk,
	RuntimeMethodHandle_GetHashCode_m2344053433_AdjustorThunk,
	RuntimeTypeHandle__ctor_m2592940784_AdjustorThunk,
	RuntimeTypeHandle_get_Value_m341583565_AdjustorThunk,
	RuntimeTypeHandle_Equals_m3288488752_AdjustorThunk,
	RuntimeTypeHandle_GetHashCode_m3601639994_AdjustorThunk,
	SByte_System_IConvertible_ToBoolean_m3232667366_AdjustorThunk,
	SByte_System_IConvertible_ToByte_m2425403364_AdjustorThunk,
	SByte_System_IConvertible_ToChar_m3723688810_AdjustorThunk,
	SByte_System_IConvertible_ToDateTime_m2748674448_AdjustorThunk,
	SByte_System_IConvertible_ToDecimal_m228135877_AdjustorThunk,
	SByte_System_IConvertible_ToDouble_m4085354590_AdjustorThunk,
	SByte_System_IConvertible_ToInt16_m4117717194_AdjustorThunk,
	SByte_System_IConvertible_ToInt32_m1080684567_AdjustorThunk,
	SByte_System_IConvertible_ToInt64_m338116548_AdjustorThunk,
	SByte_System_IConvertible_ToSByte_m2998821402_AdjustorThunk,
	SByte_System_IConvertible_ToSingle_m218660642_AdjustorThunk,
	SByte_System_IConvertible_ToType_m2818990883_AdjustorThunk,
	SByte_System_IConvertible_ToUInt16_m2876433645_AdjustorThunk,
	SByte_System_IConvertible_ToUInt32_m98339127_AdjustorThunk,
	SByte_System_IConvertible_ToUInt64_m2171951220_AdjustorThunk,
	SByte_CompareTo_m1663211154_AdjustorThunk,
	SByte_Equals_m3029699812_AdjustorThunk,
	SByte_GetHashCode_m3416233913_AdjustorThunk,
	SByte_CompareTo_m1820876469_AdjustorThunk,
	SByte_Equals_m1983290383_AdjustorThunk,
	SByte_Parse_m3989199642,
	SByte_Parse_m3790253308,
	SByte_Parse_m1842460049,
	SByte_TryParse_m3168549138,
	SByte_ToString_m2540408260_AdjustorThunk,
	SByte_ToString_m995206019_AdjustorThunk,
	SByte_ToString_m1817830399_AdjustorThunk,
	SByte_ToString_m3691134368_AdjustorThunk,
	AllowPartiallyTrustedCallersAttribute__ctor_m1393518081,
	CodeAccessPermission__ctor_m3911337023,
	CodeAccessPermission_Equals_m593433957,
	CodeAccessPermission_GetHashCode_m2903672344,
	CodeAccessPermission_ToString_m2062487043,
	CodeAccessPermission_Element_m217284982,
	CodeAccessPermission_ThrowInvalidPermission_m1868625873,
	AsymmetricAlgorithm__ctor_m291671647,
	AsymmetricAlgorithm_System_IDisposable_Dispose_m88189229,
	AsymmetricAlgorithm_get_KeySize_m1902598726,
	AsymmetricAlgorithm_set_KeySize_m514272367,
	AsymmetricAlgorithm_Clear_m1058357189,
	AsymmetricAlgorithm_GetNamedParam_m840831014,
	AsymmetricKeyExchangeFormatter__ctor_m4040727494,
	AsymmetricSignatureDeformatter__ctor_m3492573234,
	AsymmetricSignatureFormatter__ctor_m2850916843,
	Base64Constants__cctor_m2233542957,
	CryptoConfig__cctor_m2675611865,
	CryptoConfig_Initialize_m3418844644,
	CryptoConfig_CreateFromName_m804979997,
	CryptoConfig_CreateFromName_m1179263245,
	CryptoConfig_MapNameToOID_m3314400358,
	CryptoConfig_EncodeOID_m1263679715,
	CryptoConfig_EncodeLongNumber_m3991462207,
	CryptographicException__ctor_m1849887889,
	CryptographicException__ctor_m2064601648,
	CryptographicException__ctor_m3038635483,
	CryptographicException__ctor_m227323875,
	CryptographicException__ctor_m4220601442,
	CryptographicUnexpectedOperationException__ctor_m172375967,
	CryptographicUnexpectedOperationException__ctor_m2989532558,
	CryptographicUnexpectedOperationException__ctor_m2348287976,
	CspParameters__ctor_m2651133894,
	CspParameters__ctor_m2808884695,
	CspParameters__ctor_m2150729816,
	CspParameters__ctor_m871400341,
	CspParameters_get_Flags_m4242149968,
	CspParameters_set_Flags_m1836067686,
	DES__ctor_m2071078587,
	DES__cctor_m3263083662,
	DES_Create_m50114166,
	DES_Create_m988832518,
	DES_IsWeakKey_m3741566582,
	DES_IsSemiWeakKey_m540835628,
	DES_get_Key_m1041051377,
	DES_set_Key_m3627031068,
	DESCryptoServiceProvider__ctor_m3142145469,
	DESCryptoServiceProvider_CreateDecryptor_m3671349511,
	DESCryptoServiceProvider_CreateEncryptor_m2932690635,
	DESCryptoServiceProvider_GenerateIV_m3248784923,
	DESCryptoServiceProvider_GenerateKey_m2289968366,
	DESTransform__ctor_m1440260789,
	DESTransform__cctor_m4175857897,
	DESTransform_CipherFunct_m1470689048,
	DESTransform_Permutation_m1379612147,
	DESTransform_BSwap_m1755799969,
	DESTransform_SetKey_m213337146,
	DESTransform_ProcessBlock_m273492051,
	DESTransform_ECB_m2502013171,
	DESTransform_GetStrongKey_m2146352829,
	DSA__ctor_m3689799073,
	DSA_Create_m2752565669,
	DSA_Create_m561737319,
	DSA_ZeroizePrivateKey_m1359079785,
	DSA_FromXmlString_m3131159162,
	DSA_ToXmlString_m721158241,
	DSACryptoServiceProvider__ctor_m3196392230,
	DSACryptoServiceProvider__ctor_m1140958945,
	DSACryptoServiceProvider__ctor_m1444452395,
	DSACryptoServiceProvider__cctor_m3923475969,
	DSACryptoServiceProvider_Finalize_m2646587258,
	DSACryptoServiceProvider_get_KeySize_m51612726,
	DSACryptoServiceProvider_get_PublicOnly_m3652955571,
	DSACryptoServiceProvider_ExportParameters_m330548664,
	DSACryptoServiceProvider_ImportParameters_m2126638017,
	DSACryptoServiceProvider_CreateSignature_m3316725619,
	DSACryptoServiceProvider_VerifySignature_m3390963116,
	DSACryptoServiceProvider_Dispose_m3580099521,
	DSACryptoServiceProvider_OnKeyGenerated_m2360698721,
	DSASignatureDeformatter__ctor_m626251588,
	DSASignatureDeformatter__ctor_m4204963265,
	DSASignatureDeformatter_SetHashAlgorithm_m1774837202,
	DSASignatureDeformatter_SetKey_m700940086,
	DSASignatureDeformatter_VerifySignature_m2492922105,
	DSASignatureDescription__ctor_m3857331962,
	DSASignatureFormatter__ctor_m2808259383,
	DSASignatureFormatter_CreateSignature_m1203887686,
	DSASignatureFormatter_SetHashAlgorithm_m1735504798,
	DSASignatureFormatter_SetKey_m1731353802,
	HashAlgorithm__ctor_m1707064752,
	HashAlgorithm_System_IDisposable_Dispose_m592015170,
	HashAlgorithm_get_CanReuseTransform_m3839548304,
	HashAlgorithm_ComputeHash_m3223123939,
	HashAlgorithm_ComputeHash_m1624199078,
	HashAlgorithm_Create_m389128258,
	HashAlgorithm_get_Hash_m3278937893,
	HashAlgorithm_get_HashSize_m257897831,
	HashAlgorithm_Dispose_m2612993633,
	HashAlgorithm_TransformBlock_m772172284,
	HashAlgorithm_TransformFinalBlock_m1323739666,
	HMAC__ctor_m552637390,
	HMAC_get_BlockSizeValue_m1423446315,
	HMAC_set_BlockSizeValue_m1637616519,
	HMAC_set_HashName_m3407898326,
	HMAC_get_Key_m2998777640,
	HMAC_set_Key_m2022302723,
	HMAC_get_Block_m2704784173,
	HMAC_KeySetup_m2898857670,
	HMAC_Dispose_m1734594900,
	HMAC_HashCore_m1588024292,
	HMAC_HashFinal_m3818613210,
	HMAC_Initialize_m1955285998,
	HMAC_Create_m2869975659,
	HMAC_Create_m822913551,
	HMACMD5__ctor_m1560771008,
	HMACMD5__ctor_m349827019,
	HMACRIPEMD160__ctor_m3551533459,
	HMACRIPEMD160__ctor_m924157401,
	HMACSHA1__ctor_m896219247,
	HMACSHA1__ctor_m2286467978,
	HMACSHA256__ctor_m2142671648,
	HMACSHA256__ctor_m1030208081,
	HMACSHA384__ctor_m1367037561,
	HMACSHA384__ctor_m1694133122,
	HMACSHA384__cctor_m3488584827,
	HMACSHA384_set_ProduceLegacyHmacValues_m1479681577,
	HMACSHA512__ctor_m149122110,
	HMACSHA512__ctor_m562212266,
	HMACSHA512__cctor_m3792482634,
	HMACSHA512_set_ProduceLegacyHmacValues_m237511312,
	KeyedHashAlgorithm__ctor_m4208701600,
	KeyedHashAlgorithm_Finalize_m1701091456,
	KeyedHashAlgorithm_get_Key_m3725253812,
	KeyedHashAlgorithm_set_Key_m2903603585,
	KeyedHashAlgorithm_Dispose_m1778000470,
	KeyedHashAlgorithm_ZeroizeKey_m1138297311,
	KeySizes__ctor_m416378169,
	KeySizes_get_MaxSize_m192874535,
	KeySizes_get_MinSize_m1626877941,
	KeySizes_get_SkipSize_m2105865932,
	KeySizes_IsLegal_m989369878,
	KeySizes_IsLegalKeySize_m1950532129,
	MACTripleDES__ctor_m2400801372,
	MACTripleDES_Setup_m2341973299,
	MACTripleDES_Finalize_m2622871706,
	MACTripleDES_Dispose_m247372160,
	MACTripleDES_Initialize_m385376470,
	MACTripleDES_HashCore_m2727696638,
	MACTripleDES_HashFinal_m1042999865,
	MD5__ctor_m596613171,
	MD5_Create_m4125528865,
	MD5_Create_m205545373,
	MD5CryptoServiceProvider__ctor_m753386372,
	MD5CryptoServiceProvider__cctor_m270258082,
	MD5CryptoServiceProvider_Finalize_m955267910,
	MD5CryptoServiceProvider_Dispose_m1620185781,
	MD5CryptoServiceProvider_HashCore_m2121415868,
	MD5CryptoServiceProvider_HashFinal_m2818288565,
	MD5CryptoServiceProvider_Initialize_m3474556849,
	MD5CryptoServiceProvider_ProcessBlock_m182282920,
	MD5CryptoServiceProvider_ProcessFinalBlock_m799316274,
	MD5CryptoServiceProvider_AddLength_m462814318,
	RandomNumberGenerator__ctor_m3840730272,
	RandomNumberGenerator_Create_m4206972973,
	RandomNumberGenerator_Create_m2425005426,
	RC2__ctor_m3631573358,
	RC2_Create_m1850727163,
	RC2_Create_m246291559,
	RC2_get_EffectiveKeySize_m4238040213,
	RC2_get_KeySize_m2670902482,
	RC2_set_KeySize_m2041602034,
	RC2CryptoServiceProvider__ctor_m253482583,
	RC2CryptoServiceProvider_get_EffectiveKeySize_m1930909438,
	RC2CryptoServiceProvider_CreateDecryptor_m1625321546,
	RC2CryptoServiceProvider_CreateEncryptor_m4236515671,
	RC2CryptoServiceProvider_GenerateIV_m1845440477,
	RC2CryptoServiceProvider_GenerateKey_m353453894,
	RC2Transform__ctor_m844199803,
	RC2Transform__cctor_m2771795387,
	RC2Transform_ECB_m846182354,
	Rijndael__ctor_m2910605669,
	Rijndael_Create_m2744073199,
	Rijndael_Create_m3396896623,
	RijndaelManaged__ctor_m3266499975,
	RijndaelManaged_GenerateIV_m1915080978,
	RijndaelManaged_GenerateKey_m1787764991,
	RijndaelManaged_CreateDecryptor_m3673627565,
	RijndaelManaged_CreateEncryptor_m3840576876,
	RijndaelManagedTransform__ctor_m1483700773,
	RijndaelManagedTransform_System_IDisposable_Dispose_m4058554074,
	RijndaelManagedTransform_get_CanReuseTransform_m3197918900,
	RijndaelManagedTransform_TransformBlock_m2185477921,
	RijndaelManagedTransform_TransformFinalBlock_m3059836191,
	RijndaelTransform__ctor_m4128219783,
	RijndaelTransform__cctor_m4016021323,
	RijndaelTransform_Clear_m3250384753,
	RijndaelTransform_ECB_m1050161573,
	RijndaelTransform_SubByte_m1285705810,
	RijndaelTransform_Encrypt128_m3469715421,
	RijndaelTransform_Encrypt192_m2684119719,
	RijndaelTransform_Encrypt256_m2620886660,
	RijndaelTransform_Decrypt128_m2721620345,
	RijndaelTransform_Decrypt192_m2892346114,
	RijndaelTransform_Decrypt256_m3633649152,
	RIPEMD160__ctor_m3973466306,
	RIPEMD160Managed__ctor_m1524938579,
	RIPEMD160Managed_Initialize_m3387271859,
	RIPEMD160Managed_HashCore_m997955926,
	RIPEMD160Managed_HashFinal_m2196059574,
	RIPEMD160Managed_Finalize_m3695408240,
	RIPEMD160Managed_ProcessBlock_m720135161,
	RIPEMD160Managed_Compress_m3442516984,
	RIPEMD160Managed_CompressFinal_m2548849686,
	RIPEMD160Managed_ROL_m663937787,
	RIPEMD160Managed_F_m27732081,
	RIPEMD160Managed_G_m2542142523,
	RIPEMD160Managed_H_m1727654113,
	RIPEMD160Managed_I_m2826723322,
	RIPEMD160Managed_J_m3895786971,
	RIPEMD160Managed_FF_m1909724065,
	RIPEMD160Managed_GG_m3306331971,
	RIPEMD160Managed_HH_m945423221,
	RIPEMD160Managed_II_m1874445900,
	RIPEMD160Managed_JJ_m1236397107,
	RIPEMD160Managed_FFF_m3028505343,
	RIPEMD160Managed_GGG_m3380835391,
	RIPEMD160Managed_HHH_m1160663704,
	RIPEMD160Managed_III_m3527519578,
	RIPEMD160Managed_JJJ_m442232833,
	RNGCryptoServiceProvider__ctor_m943297446,
	RNGCryptoServiceProvider__cctor_m966452489,
	RNGCryptoServiceProvider_Check_m437531604,
	RNGCryptoServiceProvider_RngOpen_m1081408587,
	RNGCryptoServiceProvider_RngInitialize_m3078861152,
	RNGCryptoServiceProvider_RngGetBytes_m3536057600,
	RNGCryptoServiceProvider_RngClose_m2288642125,
	RNGCryptoServiceProvider_GetBytes_m1534062158,
	RNGCryptoServiceProvider_GetNonZeroBytes_m4259469323,
	RNGCryptoServiceProvider_Finalize_m2708995255,
	RSA__ctor_m3625671390,
	RSA_Create_m3650625912,
	RSA_Create_m3773847354,
	RSA_ZeroizePrivateKey_m1340799204,
	RSA_FromXmlString_m3696452870,
	RSA_ToXmlString_m4181509890,
	RSACryptoServiceProvider__ctor_m367282860,
	RSACryptoServiceProvider__ctor_m1534247216,
	RSACryptoServiceProvider__ctor_m1453217386,
	RSACryptoServiceProvider__cctor_m3138589205,
	RSACryptoServiceProvider_Common_m1337984034,
	RSACryptoServiceProvider_Finalize_m3061148868,
	RSACryptoServiceProvider_get_KeySize_m2842481204,
	RSACryptoServiceProvider_get_PublicOnly_m3250695170,
	RSACryptoServiceProvider_DecryptValue_m4214132066,
	RSACryptoServiceProvider_EncryptValue_m1796613593,
	RSACryptoServiceProvider_ExportParameters_m4155150051,
	RSACryptoServiceProvider_ImportParameters_m1846951175,
	RSACryptoServiceProvider_Dispose_m1440214926,
	RSACryptoServiceProvider_OnKeyGenerated_m3780506407,
	RSAPKCS1KeyExchangeFormatter__ctor_m371779760,
	RSAPKCS1KeyExchangeFormatter_CreateKeyExchange_m1481364295,
	RSAPKCS1KeyExchangeFormatter_SetRSAKey_m2334878093,
	RSAPKCS1SHA1SignatureDescription__ctor_m1453323230,
	RSAPKCS1SignatureDeformatter__ctor_m922759933,
	RSAPKCS1SignatureDeformatter__ctor_m94662501,
	RSAPKCS1SignatureDeformatter_SetHashAlgorithm_m2028328074,
	RSAPKCS1SignatureDeformatter_SetKey_m1241529138,
	RSAPKCS1SignatureDeformatter_VerifySignature_m3971152561,
	RSAPKCS1SignatureFormatter__ctor_m3354498740,
	RSAPKCS1SignatureFormatter_CreateSignature_m4150834117,
	RSAPKCS1SignatureFormatter_SetHashAlgorithm_m1125255771,
	RSAPKCS1SignatureFormatter_SetKey_m3922233774,
	SHA1__ctor_m4034446490,
	SHA1_Create_m51473861,
	SHA1_Create_m3635804177,
	SHA1CryptoServiceProvider__ctor_m2333822830,
	SHA1CryptoServiceProvider_Finalize_m206632601,
	SHA1CryptoServiceProvider_Dispose_m455058617,
	SHA1CryptoServiceProvider_HashCore_m1649570722,
	SHA1CryptoServiceProvider_HashFinal_m247104950,
	SHA1CryptoServiceProvider_Initialize_m3672850906,
	SHA1Internal__ctor_m2325882877,
	SHA1Internal_HashCore_m3378282166,
	SHA1Internal_HashFinal_m1656072795,
	SHA1Internal_Initialize_m3917808401,
	SHA1Internal_ProcessBlock_m1171620405,
	SHA1Internal_InitialiseBuff_m4266595327,
	SHA1Internal_FillBuff_m3772419989,
	SHA1Internal_ProcessFinalBlock_m2445418988,
	SHA1Internal_AddLength_m2664273459,
	SHA1Managed__ctor_m1463248351,
	SHA1Managed_HashCore_m791876697,
	SHA1Managed_HashFinal_m2802009378,
	SHA1Managed_Initialize_m927269960,
	SHA256__ctor_m1589541066,
	SHA256_Create_m1972474237,
	SHA256_Create_m3761074742,
	SHA256Managed__ctor_m1100320217,
	SHA256Managed_HashCore_m2504427447,
	SHA256Managed_HashFinal_m2812455299,
	SHA256Managed_Initialize_m1098343159,
	SHA256Managed_ProcessBlock_m1968446734,
	SHA256Managed_ProcessFinalBlock_m1034177166,
	SHA256Managed_AddLength_m1753316025,
	SHA384__ctor_m42777444,
	SHA384_Create_m2545117731,
	SHA384_Create_m186640052,
	SHA384Managed__ctor_m2760360676,
	SHA384Managed_Initialize_m2368260672,
	SHA384Managed_Initialize_m1472779757,
	SHA384Managed_HashCore_m2058781025,
	SHA384Managed_HashFinal_m1091129534,
	SHA384Managed_update_m832600628,
	SHA384Managed_processWord_m3910418284,
	SHA384Managed_unpackWord_m3305815558,
	SHA384Managed_adjustByteCounts_m1577367448,
	SHA384Managed_processLength_m1998016938,
	SHA384Managed_processBlock_m1044762650,
	SHA512__ctor_m849092553,
	SHA512_Create_m3996179329,
	SHA512_Create_m2046618513,
	SHA512Managed__ctor_m1907512623,
	SHA512Managed_Initialize_m1341762678,
	SHA512Managed_Initialize_m3709419690,
	SHA512Managed_HashCore_m3736500170,
	SHA512Managed_HashFinal_m4222140059,
	SHA512Managed_update_m1980741561,
	SHA512Managed_processWord_m3086419716,
	SHA512Managed_unpackWord_m186287524,
	SHA512Managed_adjustByteCounts_m3584504717,
	SHA512Managed_processLength_m560611531,
	SHA512Managed_processBlock_m1321711005,
	SHA512Managed_rotateRight_m3684641738,
	SHA512Managed_Ch_m3989645947,
	SHA512Managed_Maj_m2919184232,
	SHA512Managed_Sum0_m2877570381,
	SHA512Managed_Sum1_m2188034114,
	SHA512Managed_Sigma0_m1705360206,
	SHA512Managed_Sigma1_m2986763090,
	SHAConstants__cctor_m123851408,
	SignatureDescription__ctor_m1612965412,
	SignatureDescription_set_DeformatterAlgorithm_m1821656658,
	SignatureDescription_set_DigestAlgorithm_m2466896255,
	SignatureDescription_set_FormatterAlgorithm_m1935147294,
	SignatureDescription_set_KeyAlgorithm_m377164898,
	SymmetricAlgorithm__ctor_m1122125023,
	SymmetricAlgorithm_System_IDisposable_Dispose_m2796869492,
	SymmetricAlgorithm_Finalize_m1190243561,
	SymmetricAlgorithm_Clear_m3481857600,
	SymmetricAlgorithm_Dispose_m3325643277,
	SymmetricAlgorithm_get_BlockSize_m1273428540,
	SymmetricAlgorithm_set_BlockSize_m3183805403,
	SymmetricAlgorithm_get_FeedbackSize_m3223966159,
	SymmetricAlgorithm_get_IV_m72293949,
	SymmetricAlgorithm_set_IV_m2177422850,
	SymmetricAlgorithm_get_Key_m2698285671,
	SymmetricAlgorithm_set_Key_m932106934,
	SymmetricAlgorithm_get_KeySize_m210518352,
	SymmetricAlgorithm_set_KeySize_m2404708908,
	SymmetricAlgorithm_get_LegalKeySizes_m2001160061,
	SymmetricAlgorithm_get_Mode_m358157376,
	SymmetricAlgorithm_set_Mode_m3137309540,
	SymmetricAlgorithm_get_Padding_m2219535380,
	SymmetricAlgorithm_set_Padding_m4286987756,
	SymmetricAlgorithm_CreateDecryptor_m1065208203,
	SymmetricAlgorithm_CreateEncryptor_m2069911176,
	SymmetricAlgorithm_Create_m1465901834,
	ToBase64Transform_System_IDisposable_Dispose_m745662615,
	ToBase64Transform_Finalize_m3609906449,
	ToBase64Transform_get_CanReuseTransform_m3440077908,
	ToBase64Transform_get_InputBlockSize_m2587476540,
	ToBase64Transform_get_OutputBlockSize_m3091865724,
	ToBase64Transform_Dispose_m2067590776,
	ToBase64Transform_TransformBlock_m1687230426,
	ToBase64Transform_InternalTransformBlock_m1011363023,
	ToBase64Transform_TransformFinalBlock_m1716618444,
	ToBase64Transform_InternalTransformFinalBlock_m2826977121,
	TripleDES__ctor_m3764042420,
	TripleDES_get_Key_m4270652781,
	TripleDES_set_Key_m785520234,
	TripleDES_IsWeakKey_m321623027,
	TripleDES_Create_m810966163,
	TripleDES_Create_m3990140880,
	TripleDESCryptoServiceProvider__ctor_m1548678773,
	TripleDESCryptoServiceProvider_GenerateIV_m4042888525,
	TripleDESCryptoServiceProvider_GenerateKey_m3908710279,
	TripleDESCryptoServiceProvider_CreateDecryptor_m564196624,
	TripleDESCryptoServiceProvider_CreateEncryptor_m4119148710,
	TripleDESTransform__ctor_m1399930954,
	TripleDESTransform_ECB_m3064597145,
	TripleDESTransform_GetStrongKey_m2843858850,
	X509Certificate__ctor_m2598704077,
	X509Certificate__ctor_m772299980,
	X509Certificate__ctor_m676817484,
	X509Certificate__ctor_m3195389856,
	X509Certificate_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m2916969062,
	X509Certificate_tostr_m2358917317,
	X509Certificate_Equals_m1506120451,
	X509Certificate_GetCertHash_m386807738,
	X509Certificate_GetCertHashString_m1746844672,
	X509Certificate_GetEffectiveDateString_m280558803,
	X509Certificate_GetExpirationDateString_m2049787079,
	X509Certificate_GetHashCode_m3341056423,
	X509Certificate_GetIssuerName_m1334954263,
	X509Certificate_GetName_m865911396,
	X509Certificate_GetPublicKey_m2681245673,
	X509Certificate_GetRawCertData_m3615746927,
	X509Certificate_ToString_m3600669461,
	X509Certificate_ToString_m3384700977,
	X509Certificate_get_Issuer_m4136295997,
	X509Certificate_get_Subject_m52101678,
	X509Certificate_Equals_m2980846026,
	X509Certificate_Import_m1670206153,
	X509Certificate_Reset_m2166992089,
	SecurityPermission__ctor_m1598538662,
	SecurityPermission_set_Flags_m3835982641,
	SecurityPermission_IsUnrestricted_m3853515820,
	SecurityPermission_IsSubsetOf_m2984130754,
	SecurityPermission_ToXml_m66315343,
	SecurityPermission_IsEmpty_m3170835685,
	SecurityPermission_Cast_m1737603092,
	SecurityPermissionAttribute_set_SkipVerification_m3395114830,
	StrongNamePublicKeyBlob_Equals_m3786157380,
	StrongNamePublicKeyBlob_GetHashCode_m707552328,
	StrongNamePublicKeyBlob_ToString_m3447543794,
	PermissionSet__ctor_m2510521410,
	PermissionSet__ctor_m503708646,
	PermissionSet_set_DeclarativeSecurity_m84390744,
	PermissionSet_CreateFromBinaryFormat_m2890811445,
	ApplicationTrust__ctor_m2779199393,
	Evidence__ctor_m4143335037,
	Evidence_get_Count_m1102568452,
	Evidence_get_SyncRoot_m3796355461,
	Evidence_get_HostEvidenceList_m241892834,
	Evidence_get_AssemblyEvidenceList_m69417573,
	Evidence_CopyTo_m3720445569,
	Evidence_Equals_m846405073,
	Evidence_GetEnumerator_m2624028338,
	Evidence_GetHashCode_m1726229036,
	EvidenceEnumerator__ctor_m4246181871,
	EvidenceEnumerator_MoveNext_m595197927,
	EvidenceEnumerator_Reset_m1353628469,
	EvidenceEnumerator_get_Current_m2741679296,
	Hash__ctor_m897128064,
	Hash__ctor_m3945204650,
	Hash_ToString_m2479841340,
	Hash_GetData_m2427324099,
	StrongName_get_Name_m3251809947,
	StrongName_get_PublicKey_m1623125475,
	StrongName_get_Version_m3532428238,
	StrongName_Equals_m3137197790,
	StrongName_GetHashCode_m3018610381,
	StrongName_ToString_m2399807839,
	WindowsIdentity__ctor_m3795789436,
	WindowsIdentity__cctor_m163644234,
	WindowsIdentity_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m2645016059,
	WindowsIdentity_Dispose_m953714069,
	WindowsIdentity_GetCurrentToken_m2622243291,
	WindowsIdentity_GetTokenName_m3719110404,
	SecurityContext__ctor_m1210181915,
	SecurityContext__ctor_m3972088427,
	SecurityContext_Capture_m2535726366,
	SecurityContext_get_FlowSuppressed_m9276115,
	SecurityContext_get_CompressedStack_m3409548514,
	SecurityCriticalAttribute__ctor_m248669408,
	SecurityElement__ctor_m1254201806,
	SecurityElement__ctor_m3829440501,
	SecurityElement__cctor_m3840129476,
	SecurityElement_get_Children_m3081891010,
	SecurityElement_get_Tag_m956161580,
	SecurityElement_set_Text_m3231999538,
	SecurityElement_AddAttribute_m779359869,
	SecurityElement_AddChild_m1712177507,
	SecurityElement_Escape_m3400820581,
	SecurityElement_Unescape_m3674705609,
	SecurityElement_IsValidAttributeName_m3209599636,
	SecurityElement_IsValidAttributeValue_m1091331824,
	SecurityElement_IsValidTag_m1338833153,
	SecurityElement_IsValidText_m2963081070,
	SecurityElement_SearchForChildByTag_m3962923931,
	SecurityElement_ToString_m1897021376,
	SecurityElement_ToXml_m1158908822,
	SecurityElement_GetAttribute_m3701624767,
	SecurityAttribute__ctor_m2817753236,
	SecurityAttribute_get_Name_m737229083,
	SecurityAttribute_get_Value_m3667996049,
	SecurityException__ctor_m2999055479,
	SecurityException__ctor_m1157428826,
	SecurityException__ctor_m2303508220,
	SecurityException_get_Demanded_m584995367,
	SecurityException_get_FirstPermissionThatFailed_m3516293922,
	SecurityException_get_PermissionState_m394476706,
	SecurityException_get_PermissionType_m300429407,
	SecurityException_get_GrantedSet_m4020109840,
	SecurityException_get_RefusedSet_m341501771,
	SecurityException_ToString_m1678274603,
	SecurityFrame__ctor_m287375657_AdjustorThunk,
	SecurityFrame__GetSecurityStack_m2417479436,
	SecurityFrame_InitFromRuntimeFrame_m629713065_AdjustorThunk,
	SecurityFrame_get_Assembly_m1882709798_AdjustorThunk,
	SecurityFrame_get_Domain_m4128432817_AdjustorThunk,
	SecurityFrame_ToString_m3398776129_AdjustorThunk,
	SecurityFrame_GetStack_m3665290341,
	SecurityManager__cctor_m1484737609,
	SecurityManager_get_SecurityEnabled_m684733761,
	SecurityManager_Decode_m1399211754,
	SecurityManager_Decode_m948428671,
	SecuritySafeCriticalAttribute__ctor_m1135903908,
	SuppressUnmanagedCodeSecurityAttribute__ctor_m1831174959,
	UnverifiableCodeAttribute__ctor_m3439683954,
	SerializableAttribute__ctor_m3301598451,
	Single_System_IConvertible_ToBoolean_m4195264544_AdjustorThunk,
	Single_System_IConvertible_ToByte_m2163323549_AdjustorThunk,
	Single_System_IConvertible_ToChar_m3157121886_AdjustorThunk,
	Single_System_IConvertible_ToDateTime_m1798462033_AdjustorThunk,
	Single_System_IConvertible_ToDecimal_m2823837807_AdjustorThunk,
	Single_System_IConvertible_ToDouble_m2992811009_AdjustorThunk,
	Single_System_IConvertible_ToInt16_m356698018_AdjustorThunk,
	Single_System_IConvertible_ToInt32_m922147041_AdjustorThunk,
	Single_System_IConvertible_ToInt64_m875097587_AdjustorThunk,
	Single_System_IConvertible_ToSByte_m172131650_AdjustorThunk,
	Single_System_IConvertible_ToSingle_m2195342775_AdjustorThunk,
	Single_System_IConvertible_ToType_m3191890887_AdjustorThunk,
	Single_System_IConvertible_ToUInt16_m3732101497_AdjustorThunk,
	Single_System_IConvertible_ToUInt32_m1724267434_AdjustorThunk,
	Single_System_IConvertible_ToUInt64_m2303184156_AdjustorThunk,
	Single_CompareTo_m67370061_AdjustorThunk,
	Single_Equals_m2142477588_AdjustorThunk,
	Single_CompareTo_m656830570_AdjustorThunk,
	Single_Equals_m3179646383_AdjustorThunk,
	Single_GetHashCode_m3263933369_AdjustorThunk,
	Single_IsInfinity_m1761771528,
	Single_IsNaN_m1192835548,
	Single_IsNegativeInfinity_m1017463537,
	Single_IsPositiveInfinity_m1234252695,
	Single_Parse_m579890978,
	Single_ToString_m1434489600_AdjustorThunk,
	Single_ToString_m80967036_AdjustorThunk,
	Single_ToString_m929050888_AdjustorThunk,
	StackOverflowException__ctor_m4052237075,
	StackOverflowException__ctor_m4043369449,
	StackOverflowException__ctor_m187608528,
	String__ctor_m1254661065,
	String__ctor_m3140314943,
	String__ctor_m906858938,
	String__ctor_m3325256682,
	String__cctor_m2122519985,
	String_System_IConvertible_ToBoolean_m1378750767,
	String_System_IConvertible_ToByte_m3927732158,
	String_System_IConvertible_ToChar_m854595448,
	String_System_IConvertible_ToDateTime_m1467551385,
	String_System_IConvertible_ToDecimal_m8092706,
	String_System_IConvertible_ToDouble_m1038698567,
	String_System_IConvertible_ToInt16_m291669052,
	String_System_IConvertible_ToInt32_m2512576351,
	String_System_IConvertible_ToInt64_m54193686,
	String_System_IConvertible_ToSByte_m1346562548,
	String_System_IConvertible_ToSingle_m3859507638,
	String_System_IConvertible_ToType_m804510055,
	String_System_IConvertible_ToUInt16_m690306049,
	String_System_IConvertible_ToUInt32_m655826677,
	String_System_IConvertible_ToUInt64_m1787836301,
	String_System_Collections_Generic_IEnumerableU3CcharU3E_GetEnumerator_m1131017081,
	String_System_Collections_IEnumerable_GetEnumerator_m290724930,
	String_Equals_m3510555014,
	String_Equals_m1448975066,
	String_Equals_m672522368,
	String_get_Chars_m2227998457,
	String_Clone_m3315204899,
	String_CopyTo_m4208391136,
	String_ToCharArray_m3872542010,
	String_ToCharArray_m2678128660,
	String_Split_m4016464379,
	String_Split_m1309689757,
	String_Split_m2753730040,
	String_Split_m2680706933,
	String_Split_m993848168,
	String_Substring_m4139251900,
	String_Substring_m2404821311,
	String_SubstringUnchecked_m3803949323,
	String_Trim_m2749071318,
	String_Trim_m361890761,
	String_TrimStart_m4243963028,
	String_TrimEnd_m498525724,
	String_FindNotWhiteSpace_m842093031,
	String_FindNotInTable_m1859306728,
	String_Compare_m492058313,
	String_Compare_m3204107571,
	String_Compare_m1081297498,
	String_Compare_m950386926,
	String_CompareTo_m3788835867,
	String_CompareTo_m1140222528,
	String_CompareOrdinal_m344226304,
	String_CompareOrdinalUnchecked_m2969867142,
	String_CompareOrdinalCaseInsensitiveUnchecked_m3603703299,
	String_EndsWith_m2915171759,
	String_IndexOfAny_m2419327538,
	String_IndexOfAny_m2024923686,
	String_IndexOfAny_m1269729619,
	String_IndexOfAnyUnchecked_m3584206985,
	String_IndexOf_m3925697364,
	String_IndexOf_m2477875696,
	String_IndexOfOrdinal_m1909812369,
	String_IndexOfOrdinalUnchecked_m2854806724,
	String_IndexOfOrdinalIgnoreCaseUnchecked_m170710479,
	String_IndexOf_m2153404505,
	String_IndexOf_m3796136682,
	String_IndexOf_m577986163,
	String_IndexOfUnchecked_m1397064270,
	String_IndexOf_m374129865,
	String_IndexOf_m1325451196,
	String_IndexOf_m1042034519,
	String_LastIndexOfAny_m2182679387,
	String_LastIndexOfAnyUnchecked_m4157161479,
	String_LastIndexOf_m4176159581,
	String_LastIndexOf_m1340797273,
	String_LastIndexOf_m4050967138,
	String_LastIndexOfUnchecked_m103383808,
	String_LastIndexOf_m2642828268,
	String_LastIndexOf_m2381323542,
	String_IsNullOrEmpty_m3560539680,
	String_PadRight_m1765948888,
	String_StartsWith_m2938647967,
	String_Replace_m2363393026,
	String_Replace_m3637142237,
	String_ReplaceUnchecked_m1744670392,
	String_ReplaceFallback_m457580136,
	String_Remove_m108136435,
	String_ToLower_m772084117,
	String_ToLower_m1166643094,
	String_ToLowerInvariant_m3401715076,
	String_ToUpperInvariant_m2873657664,
	String_ToString_m4194906941,
	String_ToString_m4195444978,
	String_Format_m1366448113,
	String_Format_m2216003994,
	String_Format_m2132242274,
	String_Format_m2496254338,
	String_Format_m3845522687,
	String_FormatHelper_m2208728312,
	String_Concat_m2669786253,
	String_Concat_m3876450496,
	String_Concat_m47853911,
	String_Concat_m4209604784,
	String_Concat_m1894493014,
	String_Concat_m2733933624,
	String_Concat_m2040094846,
	String_ConcatInternal_m2804258664,
	String_Insert_m2432934541,
	String_Join_m2919922570,
	String_Join_m2171177581,
	String_JoinUnchecked_m2289791537,
	String_get_Length_m3102021951,
	String_ParseFormatSpecifier_m3912043387,
	String_ParseDecimal_m2788274796,
	String_InternalSetChar_m3256793723,
	String_InternalSetLength_m1600004936,
	String_GetHashCode_m3205775290,
	String_GetCaseInsensitiveHashCode_m2313840820,
	String_CreateString_m1272503540,
	String_CreateString_m648189005,
	String_CreateString_m214616560,
	String_CreateString_m1720740393,
	String_CreateString_m3924920656,
	String_CreateString_m3287943429,
	String_CreateString_m3897475374,
	String_CreateString_m2877097702,
	String_memcpy4_m726498586,
	String_memcpy2_m396091641,
	String_memcpy1_m363580472,
	String_memcpy_m1517557384,
	String_CharCopy_m3297852267,
	String_CharCopyReverse_m448059676,
	String_CharCopy_m668588423,
	String_CharCopy_m1770647809,
	String_CharCopyReverse_m3178202005,
	String_InternalSplit_m2194779229,
	String_InternalAllocateStr_m146585826,
	String_op_Equality_m532451312,
	String_op_Inequality_m1315478967,
	StringComparer__ctor_m3127041636,
	StringComparer__cctor_m3312141915,
	StringComparer_get_InvariantCultureIgnoreCase_m2981715268,
	StringComparer_Compare_m3002408790,
	StringComparer_Equals_m3422250467,
	StringComparer_GetHashCode_m4117579396,
	SystemException__ctor_m3831273481,
	SystemException__ctor_m2834575246,
	SystemException__ctor_m2071093010,
	SystemException__ctor_m3421368584,
	ASCIIEncoding__ctor_m1796837888,
	ASCIIEncoding_GetByteCount_m1894082081,
	ASCIIEncoding_GetByteCount_m231792613,
	ASCIIEncoding_GetBytes_m2888701808,
	ASCIIEncoding_GetBytes_m641040454,
	ASCIIEncoding_GetBytes_m3674966356,
	ASCIIEncoding_GetBytes_m864289360,
	ASCIIEncoding_GetCharCount_m2530213940,
	ASCIIEncoding_GetChars_m293253735,
	ASCIIEncoding_GetChars_m3032899947,
	ASCIIEncoding_GetMaxByteCount_m2654379646,
	ASCIIEncoding_GetMaxCharCount_m1439292005,
	ASCIIEncoding_GetString_m3084825367,
	ASCIIEncoding_GetBytes_m687126587,
	ASCIIEncoding_GetByteCount_m1478581917,
	ASCIIEncoding_GetDecoder_m763127219,
	Decoder__ctor_m1987520440,
	Decoder_set_Fallback_m2700469140,
	Decoder_get_FallbackBuffer_m2240311834,
	DecoderExceptionFallback__ctor_m67358718,
	DecoderExceptionFallback_CreateFallbackBuffer_m2543099475,
	DecoderExceptionFallback_Equals_m1068623654,
	DecoderExceptionFallback_GetHashCode_m2030355712,
	DecoderExceptionFallbackBuffer__ctor_m2542196856,
	DecoderExceptionFallbackBuffer_get_Remaining_m1536658658,
	DecoderExceptionFallbackBuffer_Fallback_m633606720,
	DecoderExceptionFallbackBuffer_GetNextChar_m2071376374,
	DecoderFallback__ctor_m4073266908,
	DecoderFallback__cctor_m3632383896,
	DecoderFallback_get_ExceptionFallback_m1100223985,
	DecoderFallback_get_ReplacementFallback_m1288894870,
	DecoderFallback_get_StandardSafeFallback_m2441865702,
	DecoderFallbackBuffer__ctor_m215246348,
	DecoderFallbackBuffer_Reset_m2389418149,
	DecoderFallbackException__ctor_m4249827266,
	DecoderFallbackException__ctor_m3764406113,
	DecoderFallbackException__ctor_m3290567523,
	DecoderReplacementFallback__ctor_m3512596817,
	DecoderReplacementFallback__ctor_m3316979574,
	DecoderReplacementFallback_get_DefaultString_m3362905477,
	DecoderReplacementFallback_CreateFallbackBuffer_m1460208184,
	DecoderReplacementFallback_Equals_m2141877731,
	DecoderReplacementFallback_GetHashCode_m2724846204,
	DecoderReplacementFallbackBuffer__ctor_m1582989656,
	DecoderReplacementFallbackBuffer_get_Remaining_m3730867186,
	DecoderReplacementFallbackBuffer_Fallback_m387963607,
	DecoderReplacementFallbackBuffer_GetNextChar_m3169801814,
	DecoderReplacementFallbackBuffer_Reset_m3676412794,
	EncoderExceptionFallback__ctor_m720310507,
	EncoderExceptionFallback_CreateFallbackBuffer_m1565914304,
	EncoderExceptionFallback_Equals_m3767715126,
	EncoderExceptionFallback_GetHashCode_m2099422526,
	EncoderExceptionFallbackBuffer__ctor_m3920263034,
	EncoderExceptionFallbackBuffer_get_Remaining_m1878305272,
	EncoderExceptionFallbackBuffer_Fallback_m4284418952,
	EncoderExceptionFallbackBuffer_Fallback_m2850919124,
	EncoderExceptionFallbackBuffer_GetNextChar_m2132786544,
	EncoderFallback__ctor_m3426198995,
	EncoderFallback__cctor_m284215335,
	EncoderFallback_get_ExceptionFallback_m1464972749,
	EncoderFallback_get_ReplacementFallback_m3827922266,
	EncoderFallback_get_StandardSafeFallback_m1851577631,
	EncoderFallbackBuffer__ctor_m3569152056,
	EncoderFallbackException__ctor_m2123956912,
	EncoderFallbackException__ctor_m781728286,
	EncoderFallbackException__ctor_m2977126351,
	EncoderFallbackException__ctor_m2113416521,
	EncoderReplacementFallback__ctor_m2633060057,
	EncoderReplacementFallback__ctor_m1548856421,
	EncoderReplacementFallback_get_DefaultString_m1152386198,
	EncoderReplacementFallback_CreateFallbackBuffer_m2288764815,
	EncoderReplacementFallback_Equals_m1754666140,
	EncoderReplacementFallback_GetHashCode_m2810870492,
	EncoderReplacementFallbackBuffer__ctor_m3187926460,
	EncoderReplacementFallbackBuffer_get_Remaining_m596312523,
	EncoderReplacementFallbackBuffer_Fallback_m4112181947,
	EncoderReplacementFallbackBuffer_Fallback_m1000484618,
	EncoderReplacementFallbackBuffer_Fallback_m4121272877,
	EncoderReplacementFallbackBuffer_GetNextChar_m865947909,
	Encoding__ctor_m455606385,
	Encoding__ctor_m2364596850,
	Encoding__cctor_m3823259146,
	Encoding___m1546889080,
	Encoding_get_IsReadOnly_m799279676,
	Encoding_get_DecoderFallback_m968544798,
	Encoding_set_DecoderFallback_m3013932350,
	Encoding_get_EncoderFallback_m789384937,
	Encoding_SetFallbackInternal_m994007432,
	Encoding_Equals_m452169602,
	Encoding_GetByteCount_m4227186505,
	Encoding_GetByteCount_m3010343368,
	Encoding_GetBytes_m3488625454,
	Encoding_GetBytes_m2873502639,
	Encoding_GetBytes_m1593548296,
	Encoding_GetBytes_m1025265848,
	Encoding_GetChars_m435610014,
	Encoding_GetDecoder_m53929652,
	Encoding_InvokeI18N_m3158856541,
	Encoding_GetEncoding_m773485090,
	Encoding_Clone_m1538624630,
	Encoding_GetEncoding_m535151945,
	Encoding_GetHashCode_m3895090026,
	Encoding_GetPreamble_m2460099611,
	Encoding_GetString_m2468918681,
	Encoding_GetString_m3245103957,
	Encoding_get_ASCII_m2386167277,
	Encoding_get_BigEndianUnicode_m747476343,
	Encoding_InternalCodePage_m3807295418,
	Encoding_get_Default_m1892828879,
	Encoding_get_ISOLatin1_m2102831549,
	Encoding_get_UTF7_m3894984704,
	Encoding_get_UTF8_m1678229949,
	Encoding_get_UTF8Unmarked_m4114693721,
	Encoding_get_UTF8UnmarkedUnsafe_m4093082537,
	Encoding_get_Unicode_m3505795070,
	Encoding_get_UTF32_m3017339306,
	Encoding_get_BigEndianUTF32_m334537726,
	Encoding_GetByteCount_m2900855870,
	Encoding_GetBytes_m3914031873,
	ForwardingDecoder__ctor_m663073553,
	ForwardingDecoder_GetChars_m1871710230,
	Latin1Encoding__ctor_m249219529,
	Latin1Encoding_GetByteCount_m3568441451,
	Latin1Encoding_GetByteCount_m794715241,
	Latin1Encoding_GetBytes_m3181550128,
	Latin1Encoding_GetBytes_m2652421294,
	Latin1Encoding_GetBytes_m3220298131,
	Latin1Encoding_GetBytes_m459498377,
	Latin1Encoding_GetCharCount_m1673711890,
	Latin1Encoding_GetChars_m1671534508,
	Latin1Encoding_GetMaxByteCount_m3041479951,
	Latin1Encoding_GetMaxCharCount_m1595185718,
	Latin1Encoding_GetString_m58970960,
	Latin1Encoding_GetString_m4212017743,
	StringBuilder__ctor_m3733079872,
	StringBuilder__ctor_m2868659477,
	StringBuilder__ctor_m3704793828,
	StringBuilder__ctor_m2602435243,
	StringBuilder__ctor_m473439139,
	StringBuilder__ctor_m750369356,
	StringBuilder__ctor_m520041810,
	StringBuilder_get_Capacity_m4150733028,
	StringBuilder_set_Capacity_m1854997739,
	StringBuilder_get_Length_m567963887,
	StringBuilder_set_Length_m3370055684,
	StringBuilder_get_Chars_m819910185,
	StringBuilder_set_Chars_m3142390845,
	StringBuilder_ToString_m2645140727,
	StringBuilder_ToString_m3822198548,
	StringBuilder_Remove_m1534981456,
	StringBuilder_Replace_m880021320,
	StringBuilder_Replace_m3626715581,
	StringBuilder_Append_m4203923530,
	StringBuilder_Append_m1994745533,
	StringBuilder_Append_m426372261,
	StringBuilder_Append_m234831009,
	StringBuilder_Append_m2407468139,
	StringBuilder_Append_m3281326376,
	StringBuilder_Append_m4271635713,
	StringBuilder_Append_m4169899056,
	StringBuilder_AppendFormat_m2847832545,
	StringBuilder_AppendFormat_m2246691282,
	StringBuilder_AppendFormat_m2570135551,
	StringBuilder_AppendFormat_m2147218040,
	StringBuilder_AppendFormat_m2206827616,
	StringBuilder_Insert_m2512051818,
	StringBuilder_Insert_m4102175982,
	StringBuilder_Insert_m345379931,
	StringBuilder_InternalEnsureCapacity_m2076946123,
	UnicodeEncoding__ctor_m1220127606,
	UnicodeEncoding__ctor_m199533177,
	UnicodeEncoding__ctor_m239671188,
	UnicodeEncoding_GetByteCount_m1953964001,
	UnicodeEncoding_GetByteCount_m3174918053,
	UnicodeEncoding_GetByteCount_m3150074527,
	UnicodeEncoding_GetBytes_m572689233,
	UnicodeEncoding_GetBytes_m535551194,
	UnicodeEncoding_GetBytes_m3268668497,
	UnicodeEncoding_GetBytesInternal_m2544128545,
	UnicodeEncoding_GetCharCount_m3065712957,
	UnicodeEncoding_GetChars_m3267083662,
	UnicodeEncoding_GetString_m1015926560,
	UnicodeEncoding_GetCharsInternal_m3032390807,
	UnicodeEncoding_GetMaxByteCount_m4079936646,
	UnicodeEncoding_GetMaxCharCount_m687012648,
	UnicodeEncoding_GetDecoder_m763929548,
	UnicodeEncoding_GetPreamble_m3175003997,
	UnicodeEncoding_Equals_m3524788131,
	UnicodeEncoding_GetHashCode_m1369621258,
	UnicodeEncoding_CopyChars_m768483352,
	UnicodeDecoder__ctor_m3834315701,
	UnicodeDecoder_GetChars_m4285425991,
	UTF32Encoding__ctor_m470477956,
	UTF32Encoding__ctor_m1739533329,
	UTF32Encoding__ctor_m970607301,
	UTF32Encoding_GetByteCount_m1402347958,
	UTF32Encoding_GetBytes_m4274563278,
	UTF32Encoding_GetCharCount_m800650398,
	UTF32Encoding_GetChars_m4077621955,
	UTF32Encoding_GetMaxByteCount_m1299013354,
	UTF32Encoding_GetMaxCharCount_m1345454425,
	UTF32Encoding_GetDecoder_m2722361452,
	UTF32Encoding_GetPreamble_m1463944593,
	UTF32Encoding_Equals_m496542124,
	UTF32Encoding_GetHashCode_m1430962791,
	UTF32Encoding_GetByteCount_m2178484365,
	UTF32Encoding_GetByteCount_m1567370710,
	UTF32Encoding_GetBytes_m3528139859,
	UTF32Encoding_GetBytes_m275935333,
	UTF32Encoding_GetString_m3750925481,
	UTF32Decoder__ctor_m1099217577,
	UTF32Decoder_GetChars_m2709987282,
	UTF7Encoding__ctor_m955668561,
	UTF7Encoding__ctor_m2115977681,
	UTF7Encoding__cctor_m2972476810,
	UTF7Encoding_GetHashCode_m3728423310,
	UTF7Encoding_Equals_m3440788742,
	UTF7Encoding_InternalGetByteCount_m2627284808,
	UTF7Encoding_GetByteCount_m2460219089,
	UTF7Encoding_InternalGetBytes_m4200488742,
	UTF7Encoding_GetBytes_m2085816167,
	UTF7Encoding_InternalGetCharCount_m973241758,
	UTF7Encoding_GetCharCount_m2309839641,
	UTF7Encoding_InternalGetChars_m3577722124,
	UTF7Encoding_GetChars_m2378901927,
	UTF7Encoding_GetMaxByteCount_m1237040058,
	UTF7Encoding_GetMaxCharCount_m4023184980,
	UTF7Encoding_GetDecoder_m2480715294,
	UTF7Encoding_GetByteCount_m4153636640,
	UTF7Encoding_GetByteCount_m260339288,
	UTF7Encoding_GetBytes_m3276108052,
	UTF7Encoding_GetBytes_m565634380,
	UTF7Encoding_GetString_m753427559,
	UTF7Decoder__ctor_m219610152,
	UTF7Decoder_GetChars_m728310123,
	UTF8Encoding__ctor_m1946666566,
	UTF8Encoding__ctor_m4021640945,
	UTF8Encoding__ctor_m913665375,
	UTF8Encoding_InternalGetByteCount_m625368048,
	UTF8Encoding_InternalGetByteCount_m921360822,
	UTF8Encoding_GetByteCount_m584436224,
	UTF8Encoding_GetByteCount_m2144817935,
	UTF8Encoding_InternalGetBytes_m4284173573,
	UTF8Encoding_InternalGetBytes_m324560689,
	UTF8Encoding_GetBytes_m1324440997,
	UTF8Encoding_GetBytes_m1555704259,
	UTF8Encoding_GetBytes_m1154207544,
	UTF8Encoding_InternalGetCharCount_m2193460697,
	UTF8Encoding_InternalGetCharCount_m2948469514,
	UTF8Encoding_Fallback_m4168354327,
	UTF8Encoding_Fallback_m731144313,
	UTF8Encoding_GetCharCount_m1768270997,
	UTF8Encoding_InternalGetChars_m3696140585,
	UTF8Encoding_InternalGetChars_m2862833516,
	UTF8Encoding_GetChars_m2967239546,
	UTF8Encoding_GetMaxByteCount_m1472884927,
	UTF8Encoding_GetMaxCharCount_m654358457,
	UTF8Encoding_GetDecoder_m2371355084,
	UTF8Encoding_GetPreamble_m88423642,
	UTF8Encoding_Equals_m3263540439,
	UTF8Encoding_GetHashCode_m371853634,
	UTF8Encoding_GetByteCount_m3476056399,
	UTF8Encoding_GetString_m774016146,
	UTF8Decoder__ctor_m920506496,
	UTF8Decoder_GetChars_m4196807859,
	CompressedStack__ctor_m1057186699,
	CompressedStack__ctor_m112312717,
	CompressedStack_CreateCopy_m233976566,
	CompressedStack_Capture_m1021096277,
	CompressedStack_IsEmpty_m1519192734,
	EventWaitHandle__ctor_m2331070755,
	EventWaitHandle_IsManualReset_m1306907451,
	EventWaitHandle_Reset_m2787136410,
	EventWaitHandle_Set_m1536670572,
	ExecutionContext__ctor_m1553713310,
	ExecutionContext__ctor_m1482236790,
	ExecutionContext__ctor_m184114300,
	ExecutionContext_Capture_m3220815208,
	ExecutionContext_get_SecurityContext_m390895419,
	ExecutionContext_set_SecurityContext_m2351306740,
	ExecutionContext_get_FlowSuppressed_m2397910917,
	ExecutionContext_IsFlowSuppressed_m300887835,
	Interlocked_CompareExchange_m2168439704,
	ManualResetEvent__ctor_m2560393312,
	Monitor_Enter_m309883778,
	Monitor_Exit_m1372154428,
	Monitor_Monitor_pulse_m1802009538,
	Monitor_Monitor_test_synchronised_m2218470822,
	Monitor_Pulse_m2424390085,
	Monitor_Monitor_wait_m1777647501,
	Monitor_Wait_m864905364,
	Mutex__ctor_m2870296590,
	Mutex_CreateMutex_internal_m663697220,
	Mutex_ReleaseMutex_internal_m999149428,
	Mutex_ReleaseMutex_m1972465564,
	NativeEventCalls_CreateEvent_internal_m4252918095,
	NativeEventCalls_SetEvent_internal_m1514150800,
	NativeEventCalls_ResetEvent_internal_m3468928099,
	NativeEventCalls_CloseEvent_internal_m303970804,
	SendOrPostCallback__ctor_m416373199,
	SendOrPostCallback_Invoke_m2866084970,
	SendOrPostCallback_BeginInvoke_m1762431920,
	SendOrPostCallback_EndInvoke_m1650533905,
	SynchronizationContext__ctor_m4018090004,
	SynchronizationContext_get_Current_m925115792,
	SynchronizationContext_SetSynchronizationContext_m3224732528,
	SynchronizationLockException__ctor_m4232452005,
	SynchronizationLockException__ctor_m3095735310,
	SynchronizationLockException__ctor_m1644078897,
	Thread__ctor_m1218933919,
	Thread__cctor_m2410562571,
	Thread_get_CurrentContext_m2699842704,
	Thread_CurrentThread_internal_m891913787,
	Thread_get_CurrentThread_m3312024903,
	Thread_FreeLocalSlotValues_m995133886,
	Thread_GetDomainID_m3556191158,
	Thread_Thread_internal_m3936584328,
	Thread_Thread_init_m440407393,
	Thread_GetCachedCurrentCulture_m2839467004,
	Thread_GetSerializedCurrentCulture_m4005324856,
	Thread_SetCachedCurrentCulture_m1281481126,
	Thread_GetCachedCurrentUICulture_m85626342,
	Thread_GetSerializedCurrentUICulture_m2578392406,
	Thread_SetCachedCurrentUICulture_m1065553103,
	Thread_get_CurrentCulture_m640162036,
	Thread_get_CurrentUICulture_m3790386189,
	Thread_set_IsBackground_m2646175327,
	Thread_SetName_internal_m98894528,
	Thread_set_Name_m3218111234,
	Thread_Start_m1856932251,
	Thread_Thread_free_internal_m640920438,
	Thread_Finalize_m773434565,
	Thread_SetState_m3834474597,
	Thread_ClrState_m396238162,
	Thread_GetNewManagedId_m3868873597,
	Thread_GetNewManagedId_internal_m3419403436,
	Thread_get_ExecutionContext_m2718260421,
	Thread_get_ManagedThreadId_m2540698636,
	Thread_GetHashCode_m1763772112,
	Thread_GetCompressedStack_m2176559759,
	ThreadAbortException__ctor_m1292743711,
	ThreadAbortException__ctor_m3273094655,
	ThreadInterruptedException__ctor_m765906674,
	ThreadInterruptedException__ctor_m264383818,
	ThreadPool_QueueUserWorkItem_m3667709369,
	ThreadStart__ctor_m176658305,
	ThreadStart_Invoke_m896459897,
	ThreadStart_BeginInvoke_m1657674684,
	ThreadStart_EndInvoke_m2226254000,
	ThreadStateException__ctor_m4239605846,
	ThreadStateException__ctor_m1274041360,
	Timer__cctor_m3385754584,
	Timer_Change_m876470576,
	Timer_Dispose_m2687405967,
	Timer_Change_m2141970086,
	Scheduler__ctor_m3901549269,
	Scheduler__cctor_m2851743699,
	Scheduler_get_Instance_m3397409287,
	Scheduler_Remove_m4169009551,
	Scheduler_Change_m2902544748,
	Scheduler_Add_m3132001641,
	Scheduler_InternalRemove_m2025230331,
	Scheduler_SchedulerThread_m44863378,
	Scheduler_ShrinkIfNeeded_m122939492,
	TimerComparer__ctor_m3265754441,
	TimerComparer_Compare_m3785057619,
	TimerCallback__ctor_m54121087,
	TimerCallback_Invoke_m3208045712,
	TimerCallback_BeginInvoke_m1465134106,
	TimerCallback_EndInvoke_m1103917671,
	WaitCallback__ctor_m559565817,
	WaitCallback_Invoke_m46906208,
	WaitCallback_BeginInvoke_m1413555015,
	WaitCallback_EndInvoke_m3269205806,
	WaitHandle__ctor_m3660572202,
	WaitHandle__cctor_m1541354027,
	WaitHandle_System_IDisposable_Dispose_m1844882303,
	WaitHandle_get_Handle_m2944703788,
	WaitHandle_set_Handle_m477214614,
	WaitHandle_WaitOne_internal_m2537485944,
	WaitHandle_Dispose_m3946988443,
	WaitHandle_WaitOne_m712082545,
	WaitHandle_WaitOne_m3888614806,
	WaitHandle_CheckDisposed_m2071914923,
	WaitHandle_Finalize_m1085828110,
	ThreadStaticAttribute__ctor_m778228724,
	TimeSpan__ctor_m1003715641_AdjustorThunk,
	TimeSpan__ctor_m959658474_AdjustorThunk,
	TimeSpan__ctor_m2308561733_AdjustorThunk,
	TimeSpan__cctor_m3835473733,
	TimeSpan_CalculateTicks_m3509558447,
	TimeSpan_get_Days_m2704054316_AdjustorThunk,
	TimeSpan_get_Hours_m781259545_AdjustorThunk,
	TimeSpan_get_Milliseconds_m2373414053_AdjustorThunk,
	TimeSpan_get_Minutes_m2209932375_AdjustorThunk,
	TimeSpan_get_Seconds_m4112581433_AdjustorThunk,
	TimeSpan_get_Ticks_m3409200361_AdjustorThunk,
	TimeSpan_get_TotalDays_m3448510689_AdjustorThunk,
	TimeSpan_get_TotalHours_m3010569118_AdjustorThunk,
	TimeSpan_get_TotalMilliseconds_m1329041946_AdjustorThunk,
	TimeSpan_get_TotalMinutes_m3004298888_AdjustorThunk,
	TimeSpan_get_TotalSeconds_m1045324781_AdjustorThunk,
	TimeSpan_Add_m1021118328_AdjustorThunk,
	TimeSpan_Compare_m1407526137,
	TimeSpan_CompareTo_m3267940589_AdjustorThunk,
	TimeSpan_CompareTo_m3198449526_AdjustorThunk,
	TimeSpan_Equals_m1555084785_AdjustorThunk,
	TimeSpan_Duration_m234222681_AdjustorThunk,
	TimeSpan_Equals_m2659880825_AdjustorThunk,
	TimeSpan_FromDays_m3939268644,
	TimeSpan_FromHours_m3350197084,
	TimeSpan_FromMinutes_m3157462784,
	TimeSpan_FromSeconds_m3012122711,
	TimeSpan_FromMilliseconds_m229107921,
	TimeSpan_From_m144412854,
	TimeSpan_GetHashCode_m3640128864_AdjustorThunk,
	TimeSpan_Negate_m1765346103_AdjustorThunk,
	TimeSpan_Subtract_m454324943_AdjustorThunk,
	TimeSpan_ToString_m472795270_AdjustorThunk,
	TimeSpan_op_Addition_m3775597963,
	TimeSpan_op_Equality_m463025364,
	TimeSpan_op_GreaterThan_m687125154,
	TimeSpan_op_GreaterThanOrEqual_m691354355,
	TimeSpan_op_Inequality_m2743874126,
	TimeSpan_op_LessThan_m2900781947,
	TimeSpan_op_LessThanOrEqual_m3930938175,
	TimeSpan_op_Subtraction_m2472412875,
	TimeZone__ctor_m3782519645,
	TimeZone__cctor_m744168230,
	TimeZone_get_CurrentTimeZone_m3918037878,
	TimeZone_IsDaylightSavingTime_m914872925,
	TimeZone_IsDaylightSavingTime_m1594110884,
	TimeZone_ToLocalTime_m1094100684,
	TimeZone_ToUniversalTime_m376605559,
	TimeZone_GetLocalTimeDiff_m1621322960,
	TimeZone_GetLocalTimeDiff_m3151638654,
	Type__ctor_m930283407,
	Type__cctor_m3931795912,
	Type_FilterName_impl_m191564066,
	Type_FilterNameIgnoreCase_impl_m2106047663,
	Type_FilterAttribute_impl_m1030794727,
	Type_get_Attributes_m364001767,
	Type_get_DeclaringType_m752513822,
	Type_get_HasElementType_m1699071148,
	Type_get_IsAbstract_m3394539562,
	Type_get_IsArray_m745419188,
	Type_get_IsByRef_m184143203,
	Type_get_IsClass_m1661969002,
	Type_get_IsContextful_m2705557627,
	Type_get_IsEnum_m1948977141,
	Type_get_IsExplicitLayout_m599632686,
	Type_get_IsInterface_m788827182,
	Type_get_IsMarshalByRef_m1952159066,
	Type_get_IsPointer_m513263405,
	Type_get_IsPrimitive_m3385012571,
	Type_get_IsSealed_m1431910581,
	Type_get_IsSerializable_m3603849484,
	Type_get_IsValueType_m2191848361,
	Type_get_MemberType_m1088770117,
	Type_get_ReflectedType_m3500185842,
	Type_get_TypeHandle_m3673387995,
	Type_Equals_m843526326,
	Type_Equals_m911247780,
	Type_EqualsInternal_m2297504039,
	Type_internal_from_handle_m2084068818,
	Type_internal_from_name_m1153252880,
	Type_GetType_m931458164,
	Type_GetType_m418477468,
	Type_GetTypeCodeInternal_m197246513,
	Type_GetTypeCode_m2853463675,
	Type_GetTypeFromHandle_m3171413529,
	Type_type_is_subtype_of_m473101958,
	Type_type_is_assignable_from_m1933252983,
	Type_IsSubclassOf_m61888076,
	Type_IsAssignableFrom_m3874020498,
	Type_IsInstanceOfType_m2795469003,
	Type_GetField_m1925245366,
	Type_GetHashCode_m3495876822,
	Type_GetMethod_m2744573080,
	Type_GetMethod_m1772015249,
	Type_GetMethod_m3484360104,
	Type_GetMethod_m3039077285,
	Type_GetProperty_m3255922200,
	Type_GetProperty_m1532833315,
	Type_GetProperty_m567419265,
	Type_GetProperty_m1936989742,
	Type_GetProperty_m3035335666,
	Type_IsArrayImpl_m330107357,
	Type_IsValueTypeImpl_m238784021,
	Type_IsContextfulImpl_m4293596303,
	Type_IsMarshalByRefImpl_m2853733014,
	Type_GetConstructor_m272770726,
	Type_GetConstructor_m2099858645,
	Type_GetConstructor_m3078335309,
	Type_ToString_m2928545488,
	Type_get_IsSystemType_m660774764,
	Type_GetGenericArguments_m2511307491,
	Type_get_ContainsGenericParameters_m13871687,
	Type_get_IsGenericTypeDefinition_m724009791,
	Type_GetGenericTypeDefinition_impl_m1657286486,
	Type_GetGenericTypeDefinition_m239171289,
	Type_get_IsGenericType_m2317020012,
	Type_MakeGenericType_m1957991317,
	Type_MakeGenericType_m4233399084,
	Type_get_IsGenericParameter_m1326572191,
	Type_get_IsNested_m2978419709,
	Type_GetPseudoCustomAttributes_m2605516109,
	TypedReference_Equals_m3326408017_AdjustorThunk,
	TypedReference_GetHashCode_m4094619062_AdjustorThunk,
	TypeInitializationException__ctor_m1757867175,
	TypeLoadException__ctor_m2399716549,
	TypeLoadException__ctor_m2719585591,
	TypeLoadException__ctor_m243829707,
	TypeLoadException__ctor_m3378867843,
	TypeLoadException_get_Message_m3625839464,
	UInt16_System_IConvertible_ToBoolean_m1772144888_AdjustorThunk,
	UInt16_System_IConvertible_ToByte_m2672866721_AdjustorThunk,
	UInt16_System_IConvertible_ToChar_m872046169_AdjustorThunk,
	UInt16_System_IConvertible_ToDateTime_m2415933095_AdjustorThunk,
	UInt16_System_IConvertible_ToDecimal_m3032145820_AdjustorThunk,
	UInt16_System_IConvertible_ToDouble_m1831865283_AdjustorThunk,
	UInt16_System_IConvertible_ToInt16_m2949227146_AdjustorThunk,
	UInt16_System_IConvertible_ToInt32_m3293594369_AdjustorThunk,
	UInt16_System_IConvertible_ToInt64_m552616453_AdjustorThunk,
	UInt16_System_IConvertible_ToSByte_m3483965902_AdjustorThunk,
	UInt16_System_IConvertible_ToSingle_m167759059_AdjustorThunk,
	UInt16_System_IConvertible_ToType_m379025759_AdjustorThunk,
	UInt16_System_IConvertible_ToUInt16_m1894970504_AdjustorThunk,
	UInt16_System_IConvertible_ToUInt32_m2992238646_AdjustorThunk,
	UInt16_System_IConvertible_ToUInt64_m1362953793_AdjustorThunk,
	UInt16_CompareTo_m4006209376_AdjustorThunk,
	UInt16_Equals_m2071734182_AdjustorThunk,
	UInt16_GetHashCode_m1814971889_AdjustorThunk,
	UInt16_CompareTo_m4172686937_AdjustorThunk,
	UInt16_Equals_m3634007042_AdjustorThunk,
	UInt16_Parse_m1722685482,
	UInt16_Parse_m1446859916,
	UInt16_TryParse_m78750700,
	UInt16_TryParse_m209638213,
	UInt16_ToString_m1179315216_AdjustorThunk,
	UInt16_ToString_m1842294654_AdjustorThunk,
	UInt16_ToString_m45510773_AdjustorThunk,
	UInt16_ToString_m3156601356_AdjustorThunk,
	UInt32_System_IConvertible_ToBoolean_m670198194_AdjustorThunk,
	UInt32_System_IConvertible_ToByte_m3042305990_AdjustorThunk,
	UInt32_System_IConvertible_ToChar_m28659192_AdjustorThunk,
	UInt32_System_IConvertible_ToDateTime_m2962576325_AdjustorThunk,
	UInt32_System_IConvertible_ToDecimal_m2984733657_AdjustorThunk,
	UInt32_System_IConvertible_ToDouble_m2650942160_AdjustorThunk,
	UInt32_System_IConvertible_ToInt16_m2400636590_AdjustorThunk,
	UInt32_System_IConvertible_ToInt32_m1930688566_AdjustorThunk,
	UInt32_System_IConvertible_ToInt64_m3440018551_AdjustorThunk,
	UInt32_System_IConvertible_ToSByte_m3762512969_AdjustorThunk,
	UInt32_System_IConvertible_ToSingle_m3653506289_AdjustorThunk,
	UInt32_System_IConvertible_ToType_m3214923870_AdjustorThunk,
	UInt32_System_IConvertible_ToUInt16_m3301943048_AdjustorThunk,
	UInt32_System_IConvertible_ToUInt32_m1272215724_AdjustorThunk,
	UInt32_System_IConvertible_ToUInt64_m3268886020_AdjustorThunk,
	UInt32_CompareTo_m3050130732_AdjustorThunk,
	UInt32_Equals_m4051847291_AdjustorThunk,
	UInt32_GetHashCode_m329710466_AdjustorThunk,
	UInt32_CompareTo_m4043248230_AdjustorThunk,
	UInt32_Equals_m3622421884_AdjustorThunk,
	UInt32_Parse_m2924183215,
	UInt32_Parse_m2694396900,
	UInt32_Parse_m51087447,
	UInt32_Parse_m1290187410,
	UInt32_TryParse_m3247026924,
	UInt32_TryParse_m1105936683,
	UInt32_ToString_m1877455711_AdjustorThunk,
	UInt32_ToString_m2846686701_AdjustorThunk,
	UInt32_ToString_m2637752551_AdjustorThunk,
	UInt32_ToString_m3640631563_AdjustorThunk,
	UInt64_System_IConvertible_ToBoolean_m1215656230_AdjustorThunk,
	UInt64_System_IConvertible_ToByte_m510547492_AdjustorThunk,
	UInt64_System_IConvertible_ToChar_m1705077510_AdjustorThunk,
	UInt64_System_IConvertible_ToDateTime_m1487984179_AdjustorThunk,
	UInt64_System_IConvertible_ToDecimal_m4073095233_AdjustorThunk,
	UInt64_System_IConvertible_ToDouble_m3893284830_AdjustorThunk,
	UInt64_System_IConvertible_ToInt16_m1822107030_AdjustorThunk,
	UInt64_System_IConvertible_ToInt32_m3311852403_AdjustorThunk,
	UInt64_System_IConvertible_ToInt64_m2160517190_AdjustorThunk,
	UInt64_System_IConvertible_ToSByte_m83726035_AdjustorThunk,
	UInt64_System_IConvertible_ToSingle_m113327491_AdjustorThunk,
	UInt64_System_IConvertible_ToType_m1456734333_AdjustorThunk,
	UInt64_System_IConvertible_ToUInt16_m298580982_AdjustorThunk,
	UInt64_System_IConvertible_ToUInt32_m3366122957_AdjustorThunk,
	UInt64_System_IConvertible_ToUInt64_m769495286_AdjustorThunk,
	UInt64_CompareTo_m1716157644_AdjustorThunk,
	UInt64_Equals_m1119348279_AdjustorThunk,
	UInt64_GetHashCode_m808575860_AdjustorThunk,
	UInt64_CompareTo_m4190558311_AdjustorThunk,
	UInt64_Equals_m2428524726_AdjustorThunk,
	UInt64_Parse_m1110924678,
	UInt64_Parse_m722310325,
	UInt64_Parse_m1872577878,
	UInt64_TryParse_m1628442159,
	UInt64_ToString_m2334142373_AdjustorThunk,
	UInt64_ToString_m479609776_AdjustorThunk,
	UInt64_ToString_m623444807_AdjustorThunk,
	UInt64_ToString_m2651165391_AdjustorThunk,
	UIntPtr__ctor_m2323485797_AdjustorThunk,
	UIntPtr__cctor_m807304978,
	UIntPtr_Equals_m1643140424_AdjustorThunk,
	UIntPtr_GetHashCode_m1760467123_AdjustorThunk,
	UIntPtr_ToString_m3571085837_AdjustorThunk,
	UnauthorizedAccessException__ctor_m1702503491,
	UnauthorizedAccessException__ctor_m3177808355,
	UnauthorizedAccessException__ctor_m3346029613,
	UnhandledExceptionEventArgs__ctor_m3873837110,
	UnhandledExceptionEventArgs_get_ExceptionObject_m2012398684,
	UnhandledExceptionEventArgs_get_IsTerminating_m3281711421,
	UnhandledExceptionEventHandler__ctor_m4070891721,
	UnhandledExceptionEventHandler_Invoke_m651596339,
	UnhandledExceptionEventHandler_BeginInvoke_m1490584764,
	UnhandledExceptionEventHandler_EndInvoke_m1267772348,
	ValueType__ctor_m3630339402,
	ValueType_InternalEquals_m3908153565,
	ValueType_DefaultEquals_m3990640312,
	ValueType_Equals_m2470668449,
	ValueType_InternalGetHashCode_m2863233978,
	ValueType_GetHashCode_m321390148,
	ValueType_ToString_m1188808705,
	Version__ctor_m174414257,
	Version__ctor_m2903744032,
	Version__ctor_m3414114207,
	Version__ctor_m997668137,
	Version__ctor_m54823598,
	Version_CheckedSet_m1756601458,
	Version_get_Build_m3504770588,
	Version_get_Major_m441588429,
	Version_get_Minor_m3891662079,
	Version_get_Revision_m2221074109,
	Version_Clone_m2238283684,
	Version_CompareTo_m1839308617,
	Version_Equals_m572287887,
	Version_CompareTo_m3067004787,
	Version_Equals_m2191984290,
	Version_GetHashCode_m3793907397,
	Version_ToString_m146400385,
	Version_CreateFromString_m1195539406,
	Version_op_Equality_m2886948052,
	Version_op_Inequality_m1171013002,
	WeakReference__ctor_m3191982620,
	WeakReference__ctor_m1159415219,
	WeakReference__ctor_m1420801414,
	WeakReference__ctor_m3560408400,
	WeakReference_AllocateHandle_m3568204303,
	WeakReference_get_IsAlive_m2821973386,
	WeakReference_get_Target_m2733015050,
	WeakReference_Finalize_m1590068820,
	Locale_GetText_m2626776732,
	Locale_GetText_m3635108345,
	HybridDictionary__ctor_m3166222034,
	HybridDictionary__ctor_m2525772109,
	HybridDictionary_System_Collections_IEnumerable_GetEnumerator_m1770827537,
	HybridDictionary_get_inner_m72932363,
	HybridDictionary_get_Count_m4009529679,
	HybridDictionary_get_Item_m3000032940,
	HybridDictionary_set_Item_m3169884881,
	HybridDictionary_get_SyncRoot_m94558822,
	HybridDictionary_Add_m3500347722,
	HybridDictionary_CopyTo_m3885249862,
	HybridDictionary_GetEnumerator_m174533349,
	HybridDictionary_Remove_m852034280,
	HybridDictionary_Switch_m1535303149,
	ListDictionary__ctor_m2698263728,
	ListDictionary__ctor_m2304480032,
	ListDictionary_System_Collections_IEnumerable_GetEnumerator_m425290031,
	ListDictionary_FindEntry_m1451975503,
	ListDictionary_FindEntry_m2656042279,
	ListDictionary_AddImpl_m998935006,
	ListDictionary_get_Count_m1430205299,
	ListDictionary_get_SyncRoot_m3621968267,
	ListDictionary_CopyTo_m728934192,
	ListDictionary_get_Item_m3430875462,
	ListDictionary_set_Item_m3558077738,
	ListDictionary_Add_m3900846117,
	ListDictionary_Clear_m1929591539,
	ListDictionary_GetEnumerator_m1036563447,
	ListDictionary_Remove_m3582970803,
	DictionaryNode__ctor_m2812764942,
	DictionaryNodeEnumerator__ctor_m3105575145,
	DictionaryNodeEnumerator_FailFast_m962276816,
	DictionaryNodeEnumerator_MoveNext_m2252273661,
	DictionaryNodeEnumerator_Reset_m1713695554,
	DictionaryNodeEnumerator_get_Current_m2052208030,
	DictionaryNodeEnumerator_get_DictionaryNode_m2904091029,
	DictionaryNodeEnumerator_get_Entry_m3422544269,
	DictionaryNodeEnumerator_get_Key_m1136331636,
	DictionaryNodeEnumerator_get_Value_m1963801786,
	NameObjectCollectionBase__ctor_m1140799417,
	NameObjectCollectionBase__ctor_m1872199601,
	NameObjectCollectionBase_System_Collections_ICollection_get_SyncRoot_m4265226444,
	NameObjectCollectionBase_System_Collections_ICollection_CopyTo_m3856561326,
	NameObjectCollectionBase_Init_m1954961079,
	NameObjectCollectionBase_get_Keys_m2738950250,
	NameObjectCollectionBase_GetEnumerator_m934528883,
	NameObjectCollectionBase_get_Count_m1728031609,
	NameObjectCollectionBase_OnDeserialization_m2828687802,
	NameObjectCollectionBase_get_IsReadOnly_m2819799010,
	NameObjectCollectionBase_BaseAdd_m2255957085,
	NameObjectCollectionBase_BaseGet_m3933727077,
	NameObjectCollectionBase_BaseGet_m3607011638,
	NameObjectCollectionBase_BaseGetKey_m4113912735,
	NameObjectCollectionBase_FindFirstMatchedItem_m2942906622,
	_Item__ctor_m2570877,
	_KeysEnumerator__ctor_m1715162644,
	_KeysEnumerator_get_Current_m427090870,
	_KeysEnumerator_MoveNext_m1292840571,
	_KeysEnumerator_Reset_m2886153187,
	KeysCollection__ctor_m787396037,
	KeysCollection_System_Collections_ICollection_CopyTo_m4003097592,
	KeysCollection_System_Collections_ICollection_get_SyncRoot_m347632918,
	KeysCollection_get_Count_m705263402,
	KeysCollection_GetEnumerator_m207991736,
	NameValueCollection__ctor_m2376174040,
	NameValueCollection__ctor_m3698979679,
	NameValueCollection_Add_m872765989,
	NameValueCollection_Get_m136787640,
	NameValueCollection_AsSingleString_m1841990898,
	NameValueCollection_GetKey_m2727023721,
	NameValueCollection_InvalidateCachedArrays_m2315504090,
	TypeConverterAttribute__ctor_m2343876285,
	TypeConverterAttribute__ctor_m2796508157,
	TypeConverterAttribute__cctor_m3401448577,
	TypeConverterAttribute_Equals_m1475102403,
	TypeConverterAttribute_GetHashCode_m2802665213,
	TypeConverterAttribute_get_ConverterTypeName_m2003125732,
	DefaultUriParser__ctor_m3211200812,
	DefaultUriParser__ctor_m3199576676,
	MonoTODOAttribute__ctor_m423555603,
	MonoTODOAttribute__ctor_m2017426088,
	DefaultCertificatePolicy__ctor_m4284737171,
	DefaultCertificatePolicy_CheckValidationResult_m2232677212,
	FileWebRequest__ctor_m875314294,
	FileWebRequest__ctor_m2545559098,
	FileWebRequestCreator__ctor_m2001750601,
	FileWebRequestCreator_Create_m2420640644,
	FtpRequestCreator__ctor_m3936842167,
	FtpRequestCreator_Create_m142425646,
	FtpWebRequest__ctor_m2851367638,
	FtpWebRequest__cctor_m2161426154,
	FtpWebRequest_U3CcallbackU3Em__B_m1047401003,
	GlobalProxySelection_get_Select_m1877492349,
	HttpRequestCreator__ctor_m1871791882,
	HttpRequestCreator_Create_m2191635693,
	HttpVersion__cctor_m3376169429,
	HttpWebRequest__ctor_m2736359513,
	HttpWebRequest__ctor_m3658280394,
	HttpWebRequest__cctor_m1749648113,
	HttpWebRequest_get_Address_m3019577225,
	HttpWebRequest_get_ServicePoint_m899481075,
	HttpWebRequest_GetServicePoint_m3427236345,
	IPAddress__ctor_m2998688361,
	IPAddress__ctor_m3961987861,
	IPAddress__cctor_m4181497143,
	IPAddress_SwapShort_m1858624640,
	IPAddress_HostToNetworkOrder_m962297178,
	IPAddress_NetworkToHostOrder_m4114126828,
	IPAddress_Parse_m253184166,
	IPAddress_TryParse_m1179585744,
	IPAddress_ParseIPV4_m180622572,
	IPAddress_ParseIPV6_m3727203205,
	IPAddress_get_InternalIPv4Address_m4258149000,
	IPAddress_get_ScopeId_m2903494700,
	IPAddress_get_AddressFamily_m1298645662,
	IPAddress_IsLoopback_m3034121712,
	IPAddress_ToString_m1512291888,
	IPAddress_ToString_m1991813227,
	IPAddress_Equals_m3877822454,
	IPAddress_GetHashCode_m3138408096,
	IPAddress_Hash_m739624159,
	IPv6Address__ctor_m4062223255,
	IPv6Address__ctor_m951761819,
	IPv6Address__ctor_m1666403,
	IPv6Address__cctor_m1600435160,
	IPv6Address_Parse_m121713681,
	IPv6Address_Fill_m3054533284,
	IPv6Address_TryParse_m3087512846,
	IPv6Address_TryParse_m1452498452,
	IPv6Address_get_Address_m2230496963,
	IPv6Address_get_ScopeId_m1821907027,
	IPv6Address_set_ScopeId_m1505919733,
	IPv6Address_IsLoopback_m124461899,
	IPv6Address_SwapUShort_m3847599182,
	IPv6Address_AsIPv4Int_m404489331,
	IPv6Address_IsIPv4Compatible_m1526179885,
	IPv6Address_IsIPv4Mapped_m571396438,
	IPv6Address_ToString_m831693711,
	IPv6Address_ToString_m3233213616,
	IPv6Address_Equals_m1759133691,
	IPv6Address_GetHashCode_m2357397747,
	IPv6Address_Hash_m1631808157,
	RemoteCertificateValidationCallback__ctor_m3841569000,
	RemoteCertificateValidationCallback_Invoke_m2353857608,
	RemoteCertificateValidationCallback_BeginInvoke_m368301197,
	RemoteCertificateValidationCallback_EndInvoke_m4102898920,
	ServicePoint__ctor_m541371194,
	ServicePoint_get_Address_m3846712954,
	ServicePoint_get_CurrentConnections_m1105853250,
	ServicePoint_get_IdleSince_m2729517043,
	ServicePoint_set_IdleSince_m2026649210,
	ServicePoint_set_Expect100Continue_m4032784863,
	ServicePoint_set_UseNagleAlgorithm_m519465156,
	ServicePoint_set_SendContinue_m3746141504,
	ServicePoint_set_UsesProxy_m3389395357,
	ServicePoint_set_UseConnect_m3820634640,
	ServicePoint_get_AvailableForRecycling_m4187650904,
	ServicePointManager__cctor_m2362068564,
	ServicePointManager_get_CertificatePolicy_m1315874930,
	ServicePointManager_get_CheckCertificateRevocationList_m1108407563,
	ServicePointManager_get_SecurityProtocol_m3491844700,
	ServicePointManager_get_ServerCertificateValidationCallback_m1866385585,
	ServicePointManager_FindServicePoint_m2815391183,
	ServicePointManager_RecycleServicePoints_m3938343390,
	SPKey__ctor_m3457578310,
	SPKey_GetHashCode_m2652803391,
	SPKey_Equals_m228165283,
	WebHeaderCollection__ctor_m3543537335,
	WebHeaderCollection__ctor_m1894830221,
	WebHeaderCollection__ctor_m4254771387,
	WebHeaderCollection__cctor_m370270238,
	WebHeaderCollection_Add_m2149172933,
	WebHeaderCollection_AddWithoutValidate_m1614536327,
	WebHeaderCollection_IsRestricted_m3157354787,
	WebHeaderCollection_OnDeserialization_m921538671,
	WebHeaderCollection_ToString_m789018140,
	WebHeaderCollection_get_Count_m1031696890,
	WebHeaderCollection_get_Keys_m1617211394,
	WebHeaderCollection_Get_m1807072431,
	WebHeaderCollection_GetKey_m56874696,
	WebHeaderCollection_GetEnumerator_m152674609,
	WebHeaderCollection_IsHeaderValue_m4234904120,
	WebHeaderCollection_IsHeaderName_m694174817,
	WebProxy__ctor_m3438920520,
	WebProxy__ctor_m151208726,
	WebProxy__ctor_m2644648429,
	WebProxy_GetProxy_m3358069819,
	WebProxy_IsBypassed_m1890758997,
	WebProxy_CheckBypassList_m1500623066,
	WebRequest__ctor_m2656955935,
	WebRequest__ctor_m3488295241,
	WebRequest__cctor_m1135262946,
	WebRequest_AddDynamicPrefix_m3639969267,
	WebRequest_get_DefaultWebProxy_m3315946553,
	WebRequest_GetDefaultWebProxy_m291030002,
	WebRequest_AddPrefix_m680934778,
	AsnEncodedData__ctor_m1686559169,
	AsnEncodedData__ctor_m1066383994,
	AsnEncodedData__ctor_m3367282397,
	AsnEncodedData_get_Oid_m3751842987,
	AsnEncodedData_set_Oid_m516923593,
	AsnEncodedData_get_RawData_m15804319,
	AsnEncodedData_set_RawData_m1637570500,
	AsnEncodedData_CopyFrom_m2098061996,
	AsnEncodedData_ToString_m103952896,
	AsnEncodedData_Default_m3235139048,
	AsnEncodedData_BasicConstraintsExtension_m4146259797,
	AsnEncodedData_EnhancedKeyUsageExtension_m3786455932,
	AsnEncodedData_KeyUsageExtension_m1588063621,
	AsnEncodedData_SubjectKeyIdentifierExtension_m1004541233,
	AsnEncodedData_SubjectAltName_m3675814266,
	AsnEncodedData_NetscapeCertType_m3168148730,
	Oid__ctor_m1870915178,
	Oid__ctor_m4119000323,
	Oid__ctor_m2873505868,
	Oid__ctor_m2474010395,
	Oid_get_FriendlyName_m1688331560,
	Oid_get_Value_m1894942781,
	Oid_GetName_m880017793,
	OidCollection__ctor_m2607409884,
	OidCollection_System_Collections_ICollection_CopyTo_m75121772,
	OidCollection_System_Collections_IEnumerable_GetEnumerator_m3574465405,
	OidCollection_get_Count_m2266807653,
	OidCollection_get_Item_m37537949,
	OidCollection_get_SyncRoot_m1484651087,
	OidCollection_Add_m1191707284,
	OidEnumerator__ctor_m2522427274,
	OidEnumerator_System_Collections_IEnumerator_get_Current_m2376888734,
	OidEnumerator_MoveNext_m10888191,
	OidEnumerator_Reset_m830666839,
	PublicKey__ctor_m1750800224,
	PublicKey_get_EncodedKeyValue_m736788861,
	PublicKey_get_EncodedParameters_m2990914164,
	PublicKey_get_Key_m1548320275,
	PublicKey_get_Oid_m3517176937,
	PublicKey_GetUnsignedBigInteger_m1007476432,
	PublicKey_DecodeDSA_m165869145,
	PublicKey_DecodeRSA_m1730960699,
	X500DistinguishedName__ctor_m1201939721,
	X500DistinguishedName_Decode_m11295179,
	X500DistinguishedName_GetSeparator_m1302505900,
	X500DistinguishedName_DecodeRawData_m4171517381,
	X500DistinguishedName_Canonize_m543730904,
	X500DistinguishedName_AreEqual_m1796807539,
	X509BasicConstraintsExtension__ctor_m3135938564,
	X509BasicConstraintsExtension__ctor_m2548391895,
	X509BasicConstraintsExtension__ctor_m4095953927,
	X509BasicConstraintsExtension_get_CertificateAuthority_m3674544514,
	X509BasicConstraintsExtension_get_HasPathLengthConstraint_m414713914,
	X509BasicConstraintsExtension_get_PathLengthConstraint_m3246637991,
	X509BasicConstraintsExtension_CopyFrom_m340687556,
	X509BasicConstraintsExtension_Decode_m3249055662,
	X509BasicConstraintsExtension_Encode_m3998660200,
	X509BasicConstraintsExtension_ToString_m2890073721,
	X509Certificate2__ctor_m308198123,
	X509Certificate2__cctor_m484391723,
	X509Certificate2_get_Extensions_m1023292323,
	X509Certificate2_get_IssuerName_m1460999157,
	X509Certificate2_get_NotAfter_m2823396055,
	X509Certificate2_get_NotBefore_m3393295511,
	X509Certificate2_get_PrivateKey_m2166338226,
	X509Certificate2_get_PublicKey_m3138038966,
	X509Certificate2_get_SerialNumber_m1302737622,
	X509Certificate2_get_SignatureAlgorithm_m3770353299,
	X509Certificate2_get_SubjectName_m1275144177,
	X509Certificate2_get_Thumbprint_m4049803910,
	X509Certificate2_get_Version_m3027027448,
	X509Certificate2_GetNameInfo_m3374661201,
	X509Certificate2_Find_m657946865,
	X509Certificate2_GetValueAsString_m1993705590,
	X509Certificate2_ImportPkcs12_m3788361047,
	X509Certificate2_Import_m2733344782,
	X509Certificate2_Reset_m789147374,
	X509Certificate2_ToString_m345725120,
	X509Certificate2_ToString_m807561955,
	X509Certificate2_AppendBuffer_m1828255695,
	X509Certificate2_Verify_m1956073113,
	X509Certificate2_get_MonoCertificate_m3193125116,
	X509Certificate2Collection__ctor_m3907597786,
	X509Certificate2Collection__ctor_m2544859497,
	X509Certificate2Collection_get_Item_m1966030007,
	X509Certificate2Collection_Add_m1999011476,
	X509Certificate2Collection_AddRange_m3259897552,
	X509Certificate2Collection_Contains_m2676920466,
	X509Certificate2Collection_Find_m1361050413,
	X509Certificate2Collection_GetEnumerator_m108001564,
	X509Certificate2Enumerator__ctor_m1875203866,
	X509Certificate2Enumerator_System_Collections_IEnumerator_get_Current_m2303160842,
	X509Certificate2Enumerator_System_Collections_IEnumerator_MoveNext_m327021471,
	X509Certificate2Enumerator_System_Collections_IEnumerator_Reset_m2982496364,
	X509Certificate2Enumerator_get_Current_m2011474582,
	X509Certificate2Enumerator_MoveNext_m4110494477,
	X509Certificate2Enumerator_Reset_m2524280618,
	X509CertificateCollection__ctor_m1036914496,
	X509CertificateCollection__ctor_m2100269461,
	X509CertificateCollection_get_Item_m1784093064,
	X509CertificateCollection_AddRange_m1575676587,
	X509CertificateCollection_GetEnumerator_m2744545608,
	X509CertificateCollection_GetHashCode_m2550441373,
	X509CertificateEnumerator__ctor_m444840300,
	X509CertificateEnumerator_System_Collections_IEnumerator_get_Current_m490380195,
	X509CertificateEnumerator_System_Collections_IEnumerator_MoveNext_m2751375897,
	X509CertificateEnumerator_System_Collections_IEnumerator_Reset_m1237476061,
	X509CertificateEnumerator_get_Current_m4152720733,
	X509CertificateEnumerator_MoveNext_m783626541,
	X509CertificateEnumerator_Reset_m3529991512,
	X509Chain__ctor_m2295765266,
	X509Chain__ctor_m1673580690,
	X509Chain__cctor_m2749740026,
	X509Chain_get_ChainPolicy_m1264065711,
	X509Chain_Build_m3453334197,
	X509Chain_Reset_m3331458161,
	X509Chain_get_Roots_m744023777,
	X509Chain_get_CertificateAuthorities_m657405698,
	X509Chain_get_CertificateCollection_m3748443862,
	X509Chain_BuildChainFrom_m487452279,
	X509Chain_SelectBestFromCollection_m4267993714,
	X509Chain_FindParent_m3735542056,
	X509Chain_IsChainComplete_m1242041874,
	X509Chain_IsSelfIssued_m1485661908,
	X509Chain_ValidateChain_m3813329889,
	X509Chain_Process_m1041844227,
	X509Chain_PrepareForNextCertificate_m301529593,
	X509Chain_WrapUp_m1930673592,
	X509Chain_ProcessCertificateExtensions_m3038036694,
	X509Chain_IsSignedWith_m3624544465,
	X509Chain_GetSubjectKeyIdentifier_m2959801487,
	X509Chain_GetAuthorityKeyIdentifier_m3631641129,
	X509Chain_GetAuthorityKeyIdentifier_m3113434308,
	X509Chain_GetAuthorityKeyIdentifier_m585911764,
	X509Chain_CheckRevocationOnChain_m1902396785,
	X509Chain_CheckRevocation_m2319573292,
	X509Chain_CheckRevocation_m48989749,
	X509Chain_FindCrl_m3014024698,
	X509Chain_ProcessCrlExtensions_m2373206719,
	X509Chain_ProcessCrlEntryExtensions_m2033824136,
	X509ChainElement__ctor_m1568656711,
	X509ChainElement_get_Certificate_m3417167996,
	X509ChainElement_get_ChainElementStatus_m192323064,
	X509ChainElement_get_StatusFlags_m1096301318,
	X509ChainElement_set_StatusFlags_m1290770496,
	X509ChainElement_Count_m2358318203,
	X509ChainElement_Set_m2649997160,
	X509ChainElement_UncompressFlags_m4045409131,
	X509ChainElementCollection__ctor_m2015381107,
	X509ChainElementCollection_System_Collections_ICollection_CopyTo_m631034808,
	X509ChainElementCollection_System_Collections_IEnumerable_GetEnumerator_m2401724130,
	X509ChainElementCollection_get_Count_m3868431546,
	X509ChainElementCollection_get_Item_m3912577230,
	X509ChainElementCollection_get_SyncRoot_m2383237448,
	X509ChainElementCollection_GetEnumerator_m4097956872,
	X509ChainElementCollection_Add_m847233729,
	X509ChainElementCollection_Clear_m3786857277,
	X509ChainElementCollection_Contains_m2056386153,
	X509ChainElementEnumerator__ctor_m1560965757,
	X509ChainElementEnumerator_System_Collections_IEnumerator_get_Current_m2638688153,
	X509ChainElementEnumerator_get_Current_m3357365272,
	X509ChainElementEnumerator_MoveNext_m1362717986,
	X509ChainElementEnumerator_Reset_m922121085,
	X509ChainPolicy__ctor_m2726805601,
	X509ChainPolicy_get_ExtraStore_m2990822534,
	X509ChainPolicy_get_RevocationFlag_m3281186121,
	X509ChainPolicy_get_RevocationMode_m210982597,
	X509ChainPolicy_get_VerificationFlags_m1593716000,
	X509ChainPolicy_get_VerificationTime_m1721451404,
	X509ChainPolicy_Reset_m2849766180,
	X509ChainStatus__ctor_m3489936283_AdjustorThunk,
	X509ChainStatus_get_Status_m335128257_AdjustorThunk,
	X509ChainStatus_set_Status_m4280193025_AdjustorThunk,
	X509ChainStatus_set_StatusInformation_m4272839486_AdjustorThunk,
	X509ChainStatus_GetInformation_m1839183247,
	X509EnhancedKeyUsageExtension__ctor_m730911859,
	X509EnhancedKeyUsageExtension_CopyFrom_m979756146,
	X509EnhancedKeyUsageExtension_Decode_m2986058248,
	X509EnhancedKeyUsageExtension_ToString_m3261438540,
	X509Extension__ctor_m2799141655,
	X509Extension__ctor_m406737987,
	X509Extension_get_Critical_m3055848086,
	X509Extension_set_Critical_m2709766182,
	X509Extension_CopyFrom_m1301243670,
	X509Extension_FormatUnkownData_m3784592395,
	X509ExtensionCollection__ctor_m3961408697,
	X509ExtensionCollection_System_Collections_ICollection_CopyTo_m161674631,
	X509ExtensionCollection_System_Collections_IEnumerable_GetEnumerator_m1063171060,
	X509ExtensionCollection_get_Count_m224995060,
	X509ExtensionCollection_get_SyncRoot_m4031705892,
	X509ExtensionCollection_get_Item_m1068911087,
	X509ExtensionCollection_GetEnumerator_m3085262559,
	X509ExtensionEnumerator__ctor_m984586973,
	X509ExtensionEnumerator_System_Collections_IEnumerator_get_Current_m4104731131,
	X509ExtensionEnumerator_get_Current_m2694119415,
	X509ExtensionEnumerator_MoveNext_m3210325396,
	X509ExtensionEnumerator_Reset_m801981063,
	X509KeyUsageExtension__ctor_m107365671,
	X509KeyUsageExtension__ctor_m1195762189,
	X509KeyUsageExtension__ctor_m1265507522,
	X509KeyUsageExtension_get_KeyUsages_m1532518629,
	X509KeyUsageExtension_CopyFrom_m554319546,
	X509KeyUsageExtension_GetValidFlags_m3424324572,
	X509KeyUsageExtension_Decode_m2836274340,
	X509KeyUsageExtension_Encode_m1072002440,
	X509KeyUsageExtension_ToString_m1381174620,
	X509Store__ctor_m3994237834,
	X509Store_get_Certificates_m1702400367,
	X509Store_get_Factory_m579812897,
	X509Store_get_Store_m3626430760,
	X509Store_Close_m4270546084,
	X509Store_Open_m53237587,
	X509SubjectKeyIdentifierExtension__ctor_m2623171705,
	X509SubjectKeyIdentifierExtension__ctor_m3657184001,
	X509SubjectKeyIdentifierExtension__ctor_m2884178020,
	X509SubjectKeyIdentifierExtension__ctor_m2686017024,
	X509SubjectKeyIdentifierExtension__ctor_m2795298815,
	X509SubjectKeyIdentifierExtension__ctor_m1645984076,
	X509SubjectKeyIdentifierExtension_get_SubjectKeyIdentifier_m4232312249,
	X509SubjectKeyIdentifierExtension_CopyFrom_m262411069,
	X509SubjectKeyIdentifierExtension_FromHexChar_m3542757554,
	X509SubjectKeyIdentifierExtension_FromHexChars_m882804205,
	X509SubjectKeyIdentifierExtension_FromHex_m3217166338,
	X509SubjectKeyIdentifierExtension_Decode_m269290275,
	X509SubjectKeyIdentifierExtension_Encode_m1665948625,
	X509SubjectKeyIdentifierExtension_ToString_m3993080498,
	BaseMachine__ctor_m2808867664,
	BaseMachine_Scan_m2557486571,
	Capture__ctor_m1869455468,
	Capture__ctor_m1121012478,
	Capture_get_Index_m3907808290,
	Capture_get_Length_m3971179031,
	Capture_get_Value_m4071717308,
	Capture_ToString_m3697979501,
	Capture_get_Text_m74686649,
	CaptureCollection__ctor_m1463284443,
	CaptureCollection_get_Count_m86765405,
	CaptureCollection_SetValue_m1524796401,
	CaptureCollection_get_SyncRoot_m888355354,
	CaptureCollection_CopyTo_m3413021498,
	CaptureCollection_GetEnumerator_m812510493,
	CategoryUtils_CategoryFromName_m3811753124,
	CategoryUtils_IsCategory_m4155413800,
	CategoryUtils_IsCategory_m2163137776,
	FactoryCache__ctor_m3864976125,
	FactoryCache_Add_m2489922677,
	FactoryCache_Cleanup_m2082071718,
	FactoryCache_Lookup_m1054485078,
	Key__ctor_m2851356833,
	Key_GetHashCode_m1728174491,
	Key_Equals_m700341561,
	Key_ToString_m2395030159,
	Group__ctor_m657414250,
	Group__ctor_m2985966418,
	Group__ctor_m1050548384,
	Group__cctor_m3420511086,
	Group_get_Captures_m1103670571,
	Group_get_Success_m55565450,
	GroupCollection__ctor_m2650234459,
	GroupCollection_get_Count_m3082209498,
	GroupCollection_get_Item_m3590181931,
	GroupCollection_SetValue_m4030022841,
	GroupCollection_get_SyncRoot_m597033413,
	GroupCollection_CopyTo_m2153963735,
	GroupCollection_GetEnumerator_m2139291211,
	Interpreter__ctor_m4034007428,
	Interpreter_ReadProgramCount_m2060549378,
	Interpreter_Scan_m3093498884,
	Interpreter_Reset_m196471111,
	Interpreter_Eval_m3147225683,
	Interpreter_EvalChar_m2388057289,
	Interpreter_TryMatch_m2950454473,
	Interpreter_IsPosition_m4237205282,
	Interpreter_IsWordChar_m1873249047,
	Interpreter_GetString_m3892511047,
	Interpreter_Open_m3558015365,
	Interpreter_Close_m1079184275,
	Interpreter_Balance_m2378028983,
	Interpreter_Checkpoint_m2292620714,
	Interpreter_Backtrack_m358817158,
	Interpreter_ResetGroups_m929204024,
	Interpreter_GetLastDefined_m45218908,
	Interpreter_CreateMark_m2549972701,
	Interpreter_GetGroupInfo_m1313779362,
	Interpreter_PopulateGroup_m1203148445,
	Interpreter_GenerateMatch_m4133769251,
	IntStack_Pop_m4044529908_AdjustorThunk,
	IntStack_Push_m3404206713_AdjustorThunk,
	IntStack_get_Count_m2431504187_AdjustorThunk,
	IntStack_set_Count_m688359232_AdjustorThunk,
	RepeatContext__ctor_m4019822713,
	RepeatContext_get_Count_m4176031860,
	RepeatContext_set_Count_m3205893559,
	RepeatContext_get_Start_m2009793756,
	RepeatContext_set_Start_m250610698,
	RepeatContext_get_IsMinimum_m2089709823,
	RepeatContext_get_IsMaximum_m4195905978,
	RepeatContext_get_IsLazy_m662975735,
	RepeatContext_get_Expression_m513395332,
	RepeatContext_get_Previous_m2451857091,
	InterpreterFactory__ctor_m926229535,
	InterpreterFactory_NewInstance_m1772001723,
	InterpreterFactory_get_GroupCount_m3704859672,
	InterpreterFactory_get_Gap_m4180186475,
	InterpreterFactory_set_Gap_m4223065257,
	InterpreterFactory_get_Mapping_m2580782401,
	InterpreterFactory_set_Mapping_m4265804398,
	InterpreterFactory_get_NamesMapping_m3153572372,
	InterpreterFactory_set_NamesMapping_m1645343155,
	Interval__ctor_m3102752716_AdjustorThunk,
	Interval_get_Empty_m1612289276,
	Interval_get_IsDiscontiguous_m681417648_AdjustorThunk,
	Interval_get_IsSingleton_m895795660_AdjustorThunk,
	Interval_get_IsEmpty_m2966462965_AdjustorThunk,
	Interval_get_Size_m3182553814_AdjustorThunk,
	Interval_IsDisjoint_m1320129171_AdjustorThunk,
	Interval_IsAdjacent_m1087758723_AdjustorThunk,
	Interval_Contains_m386969978_AdjustorThunk,
	Interval_Contains_m3687697697_AdjustorThunk,
	Interval_Intersects_m1543757554_AdjustorThunk,
	Interval_Merge_m3917739616_AdjustorThunk,
	Interval_CompareTo_m745948360_AdjustorThunk,
	IntervalCollection__ctor_m918788359,
	IntervalCollection_get_Item_m783730233,
	IntervalCollection_Add_m3169764839,
	IntervalCollection_Normalize_m1295588297,
	IntervalCollection_GetMetaCollection_m1267446608,
	IntervalCollection_Optimize_m2126698842,
	IntervalCollection_get_Count_m632632423,
	IntervalCollection_get_SyncRoot_m2894804251,
	IntervalCollection_CopyTo_m2496767053,
	IntervalCollection_GetEnumerator_m1596418039,
	CostDelegate__ctor_m578116942,
	CostDelegate_Invoke_m2940437879,
	CostDelegate_BeginInvoke_m3941820692,
	CostDelegate_EndInvoke_m3443741528,
	Enumerator__ctor_m174366350,
	Enumerator_get_Current_m4102274120,
	Enumerator_MoveNext_m1689981771,
	Enumerator_Reset_m2833936142,
	LinkRef__ctor_m3104548595,
	LinkStack__ctor_m281815638,
	LinkStack_Push_m1448127355,
	LinkStack_Pop_m3803592339,
	Mark_get_IsDefined_m3609987897_AdjustorThunk,
	Mark_get_Index_m873878044_AdjustorThunk,
	Mark_get_Length_m1016015124_AdjustorThunk,
	Match__ctor_m238110389,
	Match__ctor_m2112467114,
	Match__ctor_m2842759013,
	Match__cctor_m1524008222,
	Match_get_Empty_m1922684559,
	Match_get_Groups_m376908515,
	Match_NextMatch_m1246497778,
	Match_get_Regex_m2048620843,
	MatchCollection__ctor_m2881869702,
	MatchCollection_get_Count_m2375168608,
	MatchCollection_get_Item_m265458295,
	MatchCollection_get_SyncRoot_m3959276890,
	MatchCollection_CopyTo_m2972287649,
	MatchCollection_GetEnumerator_m1809159483,
	MatchCollection_TryToGet_m3109984710,
	MatchCollection_get_FullList_m4240090779,
	Enumerator__ctor_m157141097,
	Enumerator_System_Collections_IEnumerator_Reset_m3082080361,
	Enumerator_System_Collections_IEnumerator_get_Current_m2240246121,
	Enumerator_System_Collections_IEnumerator_MoveNext_m1904606682,
	MRUList__ctor_m2174943652,
	MRUList_Use_m2846104550,
	MRUList_Evict_m1811132593,
	Node__ctor_m2510706571,
	PatternCompiler__ctor_m3722632908,
	PatternCompiler_EncodeOp_m1577698176,
	PatternCompiler_GetMachineFactory_m505229676,
	PatternCompiler_EmitFalse_m3671235209,
	PatternCompiler_EmitTrue_m3052543991,
	PatternCompiler_EmitCount_m3024087504,
	PatternCompiler_EmitCharacter_m4144999552,
	PatternCompiler_EmitCategory_m1974828108,
	PatternCompiler_EmitNotCategory_m3788048959,
	PatternCompiler_EmitRange_m2418012862,
	PatternCompiler_EmitSet_m2888416005,
	PatternCompiler_EmitString_m2907993839,
	PatternCompiler_EmitPosition_m3185465351,
	PatternCompiler_EmitOpen_m811998928,
	PatternCompiler_EmitClose_m3128219862,
	PatternCompiler_EmitBalanceStart_m1924342539,
	PatternCompiler_EmitBalance_m1652518181,
	PatternCompiler_EmitReference_m2216765694,
	PatternCompiler_EmitIfDefined_m1468354804,
	PatternCompiler_EmitSub_m1049866462,
	PatternCompiler_EmitTest_m1359345437,
	PatternCompiler_EmitBranch_m3705995786,
	PatternCompiler_EmitJump_m1958236741,
	PatternCompiler_EmitRepeat_m3353380693,
	PatternCompiler_EmitUntil_m181962929,
	PatternCompiler_EmitFastRepeat_m2138837181,
	PatternCompiler_EmitIn_m1522679842,
	PatternCompiler_EmitAnchor_m3028352968,
	PatternCompiler_EmitInfo_m1450139948,
	PatternCompiler_NewLink_m3979462017,
	PatternCompiler_ResolveLink_m164156451,
	PatternCompiler_EmitBranchEnd_m2911363091,
	PatternCompiler_EmitAlternationEnd_m242176802,
	PatternCompiler_MakeFlags_m319497320,
	PatternCompiler_Emit_m1106278210,
	PatternCompiler_Emit_m2601956873,
	PatternCompiler_Emit_m593203751,
	PatternCompiler_get_CurrentAddress_m3694601918,
	PatternCompiler_BeginLink_m3852665130,
	PatternCompiler_EmitLink_m1763248820,
	PatternLinkStack__ctor_m4063965746,
	PatternLinkStack_set_BaseAddress_m3909468559,
	PatternLinkStack_get_OffsetAddress_m3459546459,
	PatternLinkStack_set_OffsetAddress_m3407729897,
	PatternLinkStack_GetOffset_m787873589,
	PatternLinkStack_GetCurrent_m3175483000,
	PatternLinkStack_SetCurrent_m2772557551,
	QuickSearch__ctor_m2205088277,
	QuickSearch__cctor_m210554867,
	QuickSearch_get_Length_m877323884,
	QuickSearch_Search_m872205891,
	QuickSearch_SetupShiftTable_m971330239,
	QuickSearch_GetShiftDistance_m2244908312,
	QuickSearch_GetChar_m2194097073,
	Regex__ctor_m2953022631,
	Regex__ctor_m1281000962,
	Regex__ctor_m371066603,
	Regex__ctor_m2397604299,
	Regex__cctor_m3162765483,
	Regex_validate_options_m3793258073,
	Regex_Init_m3238975230,
	Regex_InitNewRegex_m4214253127,
	Regex_CreateMachineFactory_m37122244,
	Regex_get_RightToLeft_m596212595,
	Regex_GetGroupIndex_m3620870088,
	Regex_default_startat_m2015953969,
	Regex_IsMatch_m1298252676,
	Regex_IsMatch_m1844270105,
	Regex_Match_m1753254329,
	Regex_Matches_m3884828707,
	Regex_Matches_m1303075336,
	Regex_ToString_m30946562,
	Regex_get_Gap_m993289954,
	Regex_CreateMachine_m3816343126,
	Regex_GetGroupNamesArray_m1470390606,
	Regex_get_GroupNumbers_m4144353649,
	Alternation__ctor_m3937462547,
	Alternation_get_Alternatives_m2599806281,
	Alternation_AddAlternative_m2534753035,
	Alternation_Compile_m3814532839,
	Alternation_GetWidth_m3813024235,
	AnchorInfo__ctor_m1329460025,
	AnchorInfo__ctor_m1145966173,
	AnchorInfo__ctor_m2587556851,
	AnchorInfo_get_Offset_m2032118130,
	AnchorInfo_get_Width_m3275617445,
	AnchorInfo_get_Length_m3963962564,
	AnchorInfo_get_IsUnknownWidth_m3823293466,
	AnchorInfo_get_IsComplete_m473968426,
	AnchorInfo_get_Substring_m3135972819,
	AnchorInfo_get_IgnoreCase_m3064680505,
	AnchorInfo_get_Position_m1494480134,
	AnchorInfo_get_IsSubstring_m1604771791,
	AnchorInfo_get_IsPosition_m3903535620,
	AnchorInfo_GetInterval_m3214481456,
	Assertion__ctor_m986873442,
	Assertion_get_TrueExpression_m2657609650,
	Assertion_set_TrueExpression_m591545232,
	Assertion_get_FalseExpression_m3458447370,
	Assertion_set_FalseExpression_m3796428453,
	Assertion_GetWidth_m3363495984,
	BackslashNumber__ctor_m119971167,
	BackslashNumber_ResolveReference_m546725419,
	BackslashNumber_Compile_m3847188744,
	BalancingGroup__ctor_m3920400059,
	BalancingGroup_set_Balance_m1168008001,
	BalancingGroup_Compile_m2333330309,
	CaptureAssertion__ctor_m203256103,
	CaptureAssertion_set_CapturingGroup_m2339232823,
	CaptureAssertion_Compile_m991063116,
	CaptureAssertion_IsComplex_m762484280,
	CaptureAssertion_get_Alternate_m1528927867,
	CapturingGroup__ctor_m349869863,
	CapturingGroup_get_Index_m1859381638,
	CapturingGroup_set_Index_m1160728315,
	CapturingGroup_get_Name_m3221585584,
	CapturingGroup_set_Name_m402876030,
	CapturingGroup_get_IsNamed_m3262980019,
	CapturingGroup_Compile_m1038721027,
	CapturingGroup_IsComplex_m800801524,
	CapturingGroup_CompareTo_m1280739012,
	CharacterClass__ctor_m3535361022,
	CharacterClass__ctor_m3753319170,
	CharacterClass__cctor_m3458400496,
	CharacterClass_AddCategory_m2429202189,
	CharacterClass_AddCharacter_m1723760262,
	CharacterClass_AddRange_m2748186401,
	CharacterClass_Compile_m2702964455,
	CharacterClass_GetWidth_m3398986822,
	CharacterClass_IsComplex_m3017285671,
	CharacterClass_GetIntervalCost_m2636988507,
	CompositeExpression__ctor_m2373332310,
	CompositeExpression_get_Expressions_m423326886,
	CompositeExpression_GetWidth_m2603012898,
	CompositeExpression_IsComplex_m4270689462,
	Expression__ctor_m1698997405,
	Expression_GetFixedWidth_m1913178642,
	Expression_GetAnchorInfo_m1368236587,
	ExpressionAssertion__ctor_m958345060,
	ExpressionAssertion_set_Reverse_m968216331,
	ExpressionAssertion_set_Negate_m2078812428,
	ExpressionAssertion_get_TestExpression_m3948843979,
	ExpressionAssertion_set_TestExpression_m156845531,
	ExpressionAssertion_Compile_m2275114285,
	ExpressionAssertion_IsComplex_m3586836482,
	ExpressionCollection__ctor_m2296781218,
	ExpressionCollection_Add_m44362897,
	ExpressionCollection_get_Item_m1218952264,
	ExpressionCollection_set_Item_m514568351,
	ExpressionCollection_OnValidate_m3600058657,
	Group__ctor_m21310139,
	Group_AppendExpression_m4182799354,
	Group_Compile_m3459444973,
	Group_GetWidth_m1951523732,
	Group_GetAnchorInfo_m3067905628,
	Literal__ctor_m3944583913,
	Literal_CompileLiteral_m2295396345,
	Literal_Compile_m3069156456,
	Literal_GetWidth_m2642201717,
	Literal_GetAnchorInfo_m2455849048,
	Literal_IsComplex_m1836923678,
	NonBacktrackingGroup__ctor_m100607149,
	NonBacktrackingGroup_Compile_m50089232,
	NonBacktrackingGroup_IsComplex_m1617853001,
	Parser__ctor_m994026618,
	Parser_ParseDecimal_m3607451297,
	Parser_ParseOctal_m707484128,
	Parser_ParseHex_m107542399,
	Parser_ParseNumber_m4036076299,
	Parser_ParseName_m795481024,
	Parser_ParseRegularExpression_m94923152,
	Parser_GetMapping_m1042730142,
	Parser_ParseGroup_m1087700183,
	Parser_ParseGroupingConstruct_m595122439,
	Parser_ParseAssertionType_m534871963,
	Parser_ParseOptions_m405080860,
	Parser_ParseCharacterClass_m2750765194,
	Parser_ParseRepetitionBounds_m3016884405,
	Parser_ParseUnicodeCategory_m365050900,
	Parser_ParseSpecial_m2277867298,
	Parser_ParseEscape_m118911671,
	Parser_ParseName_m4099893475,
	Parser_IsNameChar_m3039549884,
	Parser_ParseNumber_m2192324642,
	Parser_ParseDigit_m826611528,
	Parser_ConsumeWhitespace_m557069978,
	Parser_ResolveReferences_m1171255232,
	Parser_HandleExplicitNumericGroups_m2400775214,
	Parser_IsIgnoreCase_m1568471712,
	Parser_IsMultiline_m2672509604,
	Parser_IsExplicitCapture_m2908769234,
	Parser_IsSingleline_m4271082409,
	Parser_IsIgnorePatternWhitespace_m213713411,
	Parser_IsECMAScript_m3290963469,
	Parser_NewParseException_m2148015940,
	PositionAssertion__ctor_m2606603788,
	PositionAssertion_Compile_m2554548798,
	PositionAssertion_GetWidth_m1818973288,
	PositionAssertion_IsComplex_m3193019255,
	PositionAssertion_GetAnchorInfo_m1351470229,
	Reference__ctor_m432585372,
	Reference_get_CapturingGroup_m3385097773,
	Reference_set_CapturingGroup_m4031127075,
	Reference_get_IgnoreCase_m2455052324,
	Reference_Compile_m4270342402,
	Reference_GetWidth_m2118856808,
	Reference_IsComplex_m2224049982,
	RegularExpression__ctor_m190838148,
	RegularExpression_set_GroupCount_m907963075,
	RegularExpression_Compile_m1271264672,
	Repetition__ctor_m2150805284,
	Repetition_get_Expression_m2931223493,
	Repetition_set_Expression_m2410579300,
	Repetition_get_Minimum_m1505192843,
	Repetition_Compile_m3606158506,
	Repetition_GetWidth_m2532613934,
	Repetition_GetAnchorInfo_m1189699050,
	Uri__ctor_m2889882320,
	Uri__ctor_m3306082530,
	Uri__ctor_m989934718,
	Uri__ctor_m1783104167,
	Uri__ctor_m3032927579,
	Uri__cctor_m1161855378,
	Uri_Merge_m1266107362,
	Uri_get_AbsoluteUri_m1866097448,
	Uri_get_Authority_m2389499900,
	Uri_get_Host_m820743893,
	Uri_get_IsFile_m2531602476,
	Uri_get_IsLoopback_m262667925,
	Uri_get_IsUnc_m3063098029,
	Uri_get_Scheme_m3427514748,
	Uri_get_IsAbsoluteUri_m1364944176,
	Uri_get_OriginalString_m3974371109,
	Uri_CheckHostName_m3929918188,
	Uri_IsIPv4Address_m3644737395,
	Uri_IsDomainAddress_m3525740579,
	Uri_CheckSchemeName_m1082429697,
	Uri_IsAlpha_m319673644,
	Uri_Equals_m2964182858,
	Uri_InternalEquals_m3661401220,
	Uri_GetHashCode_m1009930693,
	Uri_GetLeftPart_m3893306517,
	Uri_FromHex_m686355279,
	Uri_HexEscape_m2931306801,
	Uri_IsHexDigit_m3446062221,
	Uri_IsHexEncoding_m2663971616,
	Uri_AppendQueryAndFragment_m3441955563,
	Uri_ToString_m1672165748,
	Uri_EscapeString_m4163751340,
	Uri_EscapeString_m2527607796,
	Uri_ParseUri_m4141183113,
	Uri_Unescape_m2753918802,
	Uri_Unescape_m3041304630,
	Uri_ParseAsWindowsUNC_m4264642627,
	Uri_ParseAsWindowsAbsoluteFilePath_m3582223925,
	Uri_ParseAsUnixAbsoluteFilePath_m1263181827,
	Uri_Parse_m2500470841,
	Uri_ParseNoExceptions_m3436493051,
	Uri_CompactEscaped_m2276642188,
	Uri_Reduce_m433901093,
	Uri_HexUnescapeMultiByte_m2288286187,
	Uri_GetSchemeDelimiter_m2083514957,
	Uri_GetDefaultPort_m1829954335,
	Uri_GetOpaqueWiseSchemeDelimiter_m917397071,
	Uri_IsPredefinedScheme_m1895878122,
	Uri_get_Parser_m4141899287,
	Uri_EnsureAbsoluteUri_m2092394955,
	Uri_op_Equality_m1239543256,
	UriScheme__ctor_m3911154691_AdjustorThunk,
	UriFormatException__ctor_m2640744054,
	UriFormatException__ctor_m1390276363,
	UriFormatException__ctor_m4068537724,
	UriParser__ctor_m457649601,
	UriParser__cctor_m1276965653,
	UriParser_InitializeAndValidate_m3236017038,
	UriParser_OnRegister_m3456593883,
	UriParser_set_SchemeName_m895622768,
	UriParser_get_DefaultPort_m838775600,
	UriParser_set_DefaultPort_m2606622307,
	UriParser_CreateDefaults_m433879834,
	UriParser_InternalRegister_m2443930864,
	UriParser_GetParser_m2758076409,
	Locale_GetText_m2131320010,
	BigInteger__ctor_m2308028736,
	BigInteger__ctor_m3171712754,
	BigInteger__ctor_m615970763,
	BigInteger__ctor_m362145393,
	BigInteger__ctor_m243072073,
	BigInteger__cctor_m2442185530,
	BigInteger_get_Rng_m2491015065,
	BigInteger_GenerateRandom_m2808881327,
	BigInteger_GenerateRandom_m1605528331,
	BigInteger_BitCount_m1686537047,
	BigInteger_TestBit_m316041159,
	BigInteger_SetBit_m1692600095,
	BigInteger_SetBit_m1046987020,
	BigInteger_LowestSetBit_m2473842285,
	BigInteger_GetBytes_m2037018530,
	BigInteger_ToString_m1741067599,
	BigInteger_ToString_m2080477611,
	BigInteger_Normalize_m769597197,
	BigInteger_Clear_m3473385376,
	BigInteger_GetHashCode_m2482243794,
	BigInteger_ToString_m2162538040,
	BigInteger_Equals_m3286409704,
	BigInteger_ModInverse_m1816311639,
	BigInteger_ModPow_m2564492426,
	BigInteger_GeneratePseudoPrime_m3506510940,
	BigInteger_Incr2_m1100972076,
	BigInteger_op_Implicit_m2994795725,
	BigInteger_op_Implicit_m11032657,
	BigInteger_op_Addition_m2099490839,
	BigInteger_op_Subtraction_m51355653,
	BigInteger_op_Modulus_m1177843781,
	BigInteger_op_Modulus_m2613660825,
	BigInteger_op_Division_m3628993410,
	BigInteger_op_Multiply_m2420459962,
	BigInteger_op_LeftShift_m1848949952,
	BigInteger_op_RightShift_m3400645099,
	BigInteger_op_Equality_m4238039561,
	BigInteger_op_Inequality_m1859198082,
	BigInteger_op_Equality_m70849326,
	BigInteger_op_Inequality_m1778768838,
	BigInteger_op_GreaterThan_m638226726,
	BigInteger_op_LessThan_m2678691959,
	BigInteger_op_GreaterThanOrEqual_m3465250365,
	BigInteger_op_LessThanOrEqual_m2403737078,
	Kernel_AddSameSign_m1011458853,
	Kernel_Subtract_m2796122117,
	Kernel_MinusEq_m469225165,
	Kernel_PlusEq_m364008632,
	Kernel_Compare_m3482954104,
	Kernel_SingleByteDivideInPlace_m3966606311,
	Kernel_DwordMod_m1670481718,
	Kernel_DwordDivMod_m2601546874,
	Kernel_multiByteDivide_m866259005,
	Kernel_LeftShift_m3811357822,
	Kernel_RightShift_m2142365962,
	Kernel_Multiply_m1954393632,
	Kernel_MultiplyMod2p32pmod_m1333838302,
	Kernel_modInverse_m1758606707,
	Kernel_modInverse_m2736680205,
	ModulusRing__ctor_m4093869256,
	ModulusRing_BarrettReduction_m2408871840,
	ModulusRing_Multiply_m96746229,
	ModulusRing_Difference_m2351720885,
	ModulusRing_Pow_m2025138622,
	ModulusRing_Pow_m3064208772,
	PrimeGeneratorBase__ctor_m3635263837,
	PrimeGeneratorBase_get_Confidence_m305234063,
	PrimeGeneratorBase_get_PrimalityTest_m282858035,
	PrimeGeneratorBase_get_TrialDivisionBounds_m4276340975,
	SequentialSearchPrimeGeneratorBase__ctor_m520140196,
	SequentialSearchPrimeGeneratorBase_GenerateSearchBase_m409061324,
	SequentialSearchPrimeGeneratorBase_GenerateNewPrime_m2105446663,
	SequentialSearchPrimeGeneratorBase_GenerateNewPrime_m3907457379,
	SequentialSearchPrimeGeneratorBase_IsPrimeAcceptable_m2007849658,
	PrimalityTest__ctor_m1858078853,
	PrimalityTest_Invoke_m4206037601,
	PrimalityTest_BeginInvoke_m1453150016,
	PrimalityTest_EndInvoke_m3200086668,
	PrimalityTests_GetSPPRounds_m871637100,
	PrimalityTests_RabinMillerTest_m3226325974,
	ASN1__ctor_m422521756,
	ASN1__ctor_m1095438146,
	ASN1__ctor_m1611489005,
	ASN1_get_Count_m3317994203,
	ASN1_get_Tag_m3154625155,
	ASN1_get_Length_m2444412568,
	ASN1_get_Value_m1819304453,
	ASN1_set_Value_m175625800,
	ASN1_CompareArray_m1535252745,
	ASN1_CompareValue_m3412966293,
	ASN1_Add_m2288114432,
	ASN1_GetBytes_m1668622561,
	ASN1_Decode_m2612428633,
	ASN1_DecodeTLV_m2756715111,
	ASN1_get_Item_m1951259524,
	ASN1_Element_m1528007789,
	ASN1_ToString_m397861702,
	ASN1Convert_FromInt32_m819005996,
	ASN1Convert_FromOid_m1513090420,
	ASN1Convert_ToInt32_m1552911168,
	ASN1Convert_ToOid_m2408239793,
	ASN1Convert_ToDateTime_m3980897436,
	BitConverterLE_GetUIntBytes_m807411464,
	BitConverterLE_GetBytes_m734958349,
	ARC4Managed__ctor_m1680223106,
	ARC4Managed_Finalize_m2498238408,
	ARC4Managed_Dispose_m2115957608,
	ARC4Managed_get_Key_m2238158501,
	ARC4Managed_set_Key_m3263334828,
	ARC4Managed_get_CanReuseTransform_m3630055525,
	ARC4Managed_CreateEncryptor_m1817801073,
	ARC4Managed_CreateDecryptor_m810630513,
	ARC4Managed_GenerateIV_m659906570,
	ARC4Managed_GenerateKey_m3379407989,
	ARC4Managed_KeySetup_m897077668,
	ARC4Managed_CheckInput_m1098444684,
	ARC4Managed_TransformBlock_m2909856413,
	ARC4Managed_InternalTransformBlock_m2468969814,
	ARC4Managed_TransformFinalBlock_m2167193542,
	CryptoConvert_ToHex_m57122482,
	HMAC__ctor_m3129484148,
	HMAC_get_Key_m633315024,
	HMAC_set_Key_m3197037643,
	HMAC_Initialize_m608946158,
	HMAC_HashFinal_m1669397226,
	HMAC_HashCore_m2412365031,
	HMAC_initializePad_m3602989081,
	KeyBuilder_get_Rng_m277651138,
	KeyBuilder_Key_m1565585763,
	MD2__ctor_m4000919451,
	MD2_Create_m3930551753,
	MD2_Create_m2340992737,
	MD2Managed__ctor_m382807057,
	MD2Managed__cctor_m3904963690,
	MD2Managed_Padding_m3245433322,
	MD2Managed_Initialize_m2623837391,
	MD2Managed_HashCore_m2710063179,
	MD2Managed_HashFinal_m2009734036,
	MD2Managed_MD2Transform_m2789759401,
	MD4__ctor_m1424323227,
	MD4_Create_m4156815636,
	MD4_Create_m362115730,
	MD4Managed__ctor_m306214366,
	MD4Managed_Initialize_m3016287782,
	MD4Managed_HashCore_m2553927469,
	MD4Managed_HashFinal_m2800568498,
	MD4Managed_Padding_m1469247200,
	MD4Managed_F_m3644607136,
	MD4Managed_G_m2155314467,
	MD4Managed_H_m2132898000,
	MD4Managed_ROL_m1557079893,
	MD4Managed_FF_m1686378673,
	MD4Managed_GG_m348726550,
	MD4Managed_HH_m502459923,
	MD4Managed_Encode_m2374965113,
	MD4Managed_Decode_m2724071192,
	MD4Managed_MD4Transform_m1384700328,
	MD5SHA1__ctor_m141549212,
	MD5SHA1_Initialize_m4176591359,
	MD5SHA1_HashFinal_m53908192,
	MD5SHA1_HashCore_m3587480270,
	MD5SHA1_CreateSignature_m3994174688,
	MD5SHA1_VerifySignature_m3951985362,
	PKCS1__cctor_m2308867695,
	PKCS1_Compare_m1301858489,
	PKCS1_I2OSP_m2661230431,
	PKCS1_OS2IP_m1981153093,
	PKCS1_RSASP1_m2735422247,
	PKCS1_RSAVP1_m4163980682,
	PKCS1_Sign_v15_m1091855541,
	PKCS1_Verify_v15_m429911942,
	PKCS1_Verify_v15_m2387859380,
	PKCS1_Encode_v15_m3235444401,
	EncryptedPrivateKeyInfo__ctor_m1369443821,
	EncryptedPrivateKeyInfo__ctor_m1780555356,
	EncryptedPrivateKeyInfo_get_Algorithm_m484892361,
	EncryptedPrivateKeyInfo_get_EncryptedData_m2501499073,
	EncryptedPrivateKeyInfo_get_Salt_m2755441025,
	EncryptedPrivateKeyInfo_get_IterationCount_m1961254953,
	EncryptedPrivateKeyInfo_Decode_m3723838982,
	PrivateKeyInfo__ctor_m4193825413,
	PrivateKeyInfo__ctor_m236823045,
	PrivateKeyInfo_get_PrivateKey_m2257075769,
	PrivateKeyInfo_Decode_m1168779021,
	PrivateKeyInfo_RemoveLeadingZero_m3607149124,
	PrivateKeyInfo_Normalize_m18561129,
	PrivateKeyInfo_DecodeRSA_m2527159846,
	PrivateKeyInfo_DecodeDSA_m3518106339,
	RC4__ctor_m1614144708,
	RC4__cctor_m663776314,
	RC4_get_IV_m3829665134,
	RC4_set_IV_m2803946971,
	RSAManaged__ctor_m2588544502,
	RSAManaged__ctor_m2146688169,
	RSAManaged_Finalize_m1814511608,
	RSAManaged_GenerateKeyPair_m4206580424,
	RSAManaged_get_KeySize_m2880143614,
	RSAManaged_get_PublicOnly_m1179459602,
	RSAManaged_DecryptValue_m1364493552,
	RSAManaged_EncryptValue_m1230410218,
	RSAManaged_ExportParameters_m2321887942,
	RSAManaged_ImportParameters_m422789515,
	RSAManaged_Dispose_m2829974002,
	RSAManaged_ToXmlString_m2015408336,
	RSAManaged_GetPaddedValue_m1577181261,
	KeyGeneratedEventHandler__ctor_m519059567,
	KeyGeneratedEventHandler_Invoke_m3074426872,
	KeyGeneratedEventHandler_BeginInvoke_m3341643428,
	KeyGeneratedEventHandler_EndInvoke_m2513620429,
	ContentInfo__ctor_m1533696749,
	ContentInfo__ctor_m3573770304,
	ContentInfo__ctor_m1358525215,
	ContentInfo__ctor_m3973273357,
	ContentInfo_get_ASN1_m2002895343,
	ContentInfo_get_Content_m2711381510,
	ContentInfo_set_Content_m4086407349,
	ContentInfo_get_ContentType_m3983016568,
	ContentInfo_set_ContentType_m3006149204,
	ContentInfo_GetASN1_m1506703338,
	EncryptedData__ctor_m2779155492,
	EncryptedData__ctor_m3679851962,
	EncryptedData_get_EncryptionAlgorithm_m1085644144,
	EncryptedData_get_EncryptedContent_m47715378,
	Alert__ctor_m1393392031,
	Alert__ctor_m1320563832,
	Alert_get_Level_m2083224504,
	Alert_get_Description_m1428909104,
	Alert_get_IsWarning_m3430085096,
	Alert_get_IsCloseNotify_m2968547217,
	Alert_inferAlertLevel_m4178003709,
	Alert_GetAlertMessage_m1333758696,
	CertificateSelectionCallback__ctor_m4216130626,
	CertificateSelectionCallback_Invoke_m3974939328,
	CertificateSelectionCallback_BeginInvoke_m1529504173,
	CertificateSelectionCallback_EndInvoke_m1890837795,
	CertificateValidationCallback__ctor_m525670167,
	CertificateValidationCallback_Invoke_m3368819440,
	CertificateValidationCallback_BeginInvoke_m1555238601,
	CertificateValidationCallback_EndInvoke_m455360328,
	CertificateValidationCallback2__ctor_m486525434,
	CertificateValidationCallback2_Invoke_m2696404194,
	CertificateValidationCallback2_BeginInvoke_m3602893401,
	CertificateValidationCallback2_EndInvoke_m1941583874,
	CipherSuite__ctor_m1649596800,
	CipherSuite__cctor_m18845967,
	CipherSuite_get_EncryptionCipher_m1493897844,
	CipherSuite_get_DecryptionCipher_m2103278815,
	CipherSuite_get_ClientHMAC_m676756116,
	CipherSuite_get_ServerHMAC_m1863563213,
	CipherSuite_get_CipherAlgorithmType_m7547507,
	CipherSuite_get_HashAlgorithmName_m4035231448,
	CipherSuite_get_HashAlgorithmType_m1343605085,
	CipherSuite_get_HashSize_m279623022,
	CipherSuite_get_ExchangeAlgorithmType_m3822413550,
	CipherSuite_get_CipherMode_m1340193226,
	CipherSuite_get_Code_m1986756097,
	CipherSuite_get_Name_m3234115705,
	CipherSuite_get_IsExportable_m2351943288,
	CipherSuite_get_KeyMaterialSize_m3030891731,
	CipherSuite_get_KeyBlockSize_m3008502075,
	CipherSuite_get_ExpandedKeyMaterialSize_m3112230581,
	CipherSuite_get_EffectiveKeyBits_m4097074249,
	CipherSuite_get_IvSize_m2894146290,
	CipherSuite_get_Context_m528611831,
	CipherSuite_set_Context_m2673566586,
	CipherSuite_Write_m1117393086,
	CipherSuite_Write_m2878525702,
	CipherSuite_InitializeCipher_m3081234157,
	CipherSuite_EncryptRecord_m1939294643,
	CipherSuite_DecryptRecord_m4161872080,
	CipherSuite_CreatePremasterSecret_m82851275,
	CipherSuite_PRF_m205785501,
	CipherSuite_Expand_m2588778289,
	CipherSuite_createEncryptionCipher_m1823110658,
	CipherSuite_createDecryptionCipher_m4171513940,
	CipherSuiteCollection__ctor_m3924782681,
	CipherSuiteCollection_System_Collections_IList_get_Item_m1665840681,
	CipherSuiteCollection_System_Collections_IList_set_Item_m3478900428,
	CipherSuiteCollection_System_Collections_ICollection_get_SyncRoot_m618580436,
	CipherSuiteCollection_System_Collections_IEnumerable_GetEnumerator_m1681536898,
	CipherSuiteCollection_System_Collections_IList_Contains_m1634347190,
	CipherSuiteCollection_System_Collections_IList_IndexOf_m4213006854,
	CipherSuiteCollection_System_Collections_IList_Insert_m2008070196,
	CipherSuiteCollection_System_Collections_IList_Remove_m144704960,
	CipherSuiteCollection_System_Collections_IList_RemoveAt_m2247851113,
	CipherSuiteCollection_System_Collections_IList_Add_m1074649344,
	CipherSuiteCollection_get_Item_m3735835164,
	CipherSuiteCollection_get_Item_m653515770,
	CipherSuiteCollection_set_Item_m592704906,
	CipherSuiteCollection_get_Item_m2780174626,
	CipherSuiteCollection_get_Count_m3813525106,
	CipherSuiteCollection_CopyTo_m576332140,
	CipherSuiteCollection_Clear_m2914820078,
	CipherSuiteCollection_IndexOf_m3088990859,
	CipherSuiteCollection_IndexOf_m685492255,
	CipherSuiteCollection_Add_m3191239374,
	CipherSuiteCollection_add_m2674530140,
	CipherSuiteCollection_add_m1076941497,
	CipherSuiteCollection_cultureAwareCompare_m4026891737,
	CipherSuiteFactory_GetSupportedCiphers_m1331312394,
	CipherSuiteFactory_GetTls1SupportedCiphers_m4063178148,
	CipherSuiteFactory_GetSsl3SupportedCiphers_m1436345897,
	ClientContext__ctor_m1540414373,
	ClientContext_get_SslStream_m4172965777,
	ClientContext_get_ClientHelloProtocol_m687123721,
	ClientContext_set_ClientHelloProtocol_m2033712663,
	ClientContext_Clear_m3230671407,
	ClientRecordProtocol__ctor_m1030222096,
	ClientRecordProtocol_GetMessage_m4174671419,
	ClientRecordProtocol_ProcessHandshakeMessage_m2179640220,
	ClientRecordProtocol_createClientHandshakeMessage_m955189407,
	ClientRecordProtocol_createServerHandshakeMessage_m719154186,
	ClientSessionCache__cctor_m1535243483,
	ClientSessionCache_Add_m1577846793,
	ClientSessionCache_FromHost_m3356571309,
	ClientSessionCache_FromContext_m2612279874,
	ClientSessionCache_SetContextInCache_m1849958011,
	ClientSessionCache_SetContextFromCache_m543772420,
	ClientSessionInfo__ctor_m200781604,
	ClientSessionInfo__cctor_m1222200069,
	ClientSessionInfo_Finalize_m2714646038,
	ClientSessionInfo_get_HostName_m4100927670,
	ClientSessionInfo_get_Id_m3432574791,
	ClientSessionInfo_get_Valid_m266085941,
	ClientSessionInfo_GetContext_m1607090003,
	ClientSessionInfo_SetContext_m3958182732,
	ClientSessionInfo_KeepAlive_m3825761449,
	ClientSessionInfo_Dispose_m3094936779,
	ClientSessionInfo_Dispose_m19476096,
	ClientSessionInfo_CheckDisposed_m2780843044,
	Context__ctor_m1933010603,
	Context_get_AbbreviatedHandshake_m1283450948,
	Context_set_AbbreviatedHandshake_m3236415809,
	Context_get_ProtocolNegotiated_m2069490626,
	Context_set_ProtocolNegotiated_m3332049274,
	Context_get_SecurityProtocol_m2558379614,
	Context_set_SecurityProtocol_m3637591545,
	Context_get_SecurityProtocolFlags_m151222537,
	Context_get_Protocol_m3375473980,
	Context_get_SessionId_m3145132411,
	Context_set_SessionId_m3350823672,
	Context_get_CompressionMethod_m2648417057,
	Context_set_CompressionMethod_m2403980270,
	Context_get_ServerSettings_m398675875,
	Context_get_ClientSettings_m1279324710,
	Context_get_LastHandshakeMsg_m1594066191,
	Context_set_LastHandshakeMsg_m2519018853,
	Context_get_HandshakeState_m3595707,
	Context_set_HandshakeState_m1690488869,
	Context_get_ReceivedConnectionEnd_m1907593795,
	Context_set_ReceivedConnectionEnd_m3576701554,
	Context_get_SentConnectionEnd_m401517105,
	Context_set_SentConnectionEnd_m162465705,
	Context_get_SupportedCiphers_m4263361931,
	Context_set_SupportedCiphers_m3597249771,
	Context_get_HandshakeMessages_m279273501,
	Context_get_WriteSequenceNumber_m723984201,
	Context_set_WriteSequenceNumber_m580987874,
	Context_get_ReadSequenceNumber_m3582050733,
	Context_set_ReadSequenceNumber_m2996189414,
	Context_get_ClientRandom_m2929287758,
	Context_set_ClientRandom_m2620138301,
	Context_get_ServerRandom_m3474718287,
	Context_set_ServerRandom_m603384709,
	Context_get_RandomCS_m356162013,
	Context_set_RandomCS_m564871184,
	Context_get_RandomSC_m1929134887,
	Context_set_RandomSC_m1687935070,
	Context_get_MasterSecret_m3410383318,
	Context_set_MasterSecret_m308905945,
	Context_get_ClientWriteKey_m2257942003,
	Context_set_ClientWriteKey_m1677787488,
	Context_get_ServerWriteKey_m4018642070,
	Context_set_ServerWriteKey_m1838395661,
	Context_get_ClientWriteIV_m2726740789,
	Context_set_ClientWriteIV_m645053791,
	Context_get_ServerWriteIV_m619755409,
	Context_set_ServerWriteIV_m2460806526,
	Context_get_RecordProtocol_m751043880,
	Context_set_RecordProtocol_m1592092473,
	Context_GetUnixTime_m1933377262,
	Context_GetSecureRandomBytes_m843600736,
	Context_Clear_m2220388284,
	Context_ClearKeyInfo_m1711611987,
	Context_DecodeProtocolCode_m494796791,
	Context_ChangeProtocol_m91279606,
	Context_get_Current_m3952135089,
	Context_get_Negotiating_m3255494259,
	Context_get_Read_m3972451212,
	Context_get_Write_m193689081,
	Context_StartSwitchingSecurityParameters_m1350107141,
	Context_EndSwitchingSecurityParameters_m951771439,
	TlsClientCertificate__ctor_m3870088466,
	TlsClientCertificate_get_ClientCertificate_m3474951109,
	TlsClientCertificate_Update_m2871223530,
	TlsClientCertificate_GetClientCertificate_m1318921875,
	TlsClientCertificate_SendCertificates_m1290742152,
	TlsClientCertificate_ProcessAsSsl3_m1010799057,
	TlsClientCertificate_ProcessAsTls1_m175582028,
	TlsClientCertificate_FindParentCertificate_m292629370,
	TlsClientCertificateVerify__ctor_m3725757645,
	TlsClientCertificateVerify_Update_m2349762431,
	TlsClientCertificateVerify_ProcessAsSsl3_m4281440818,
	TlsClientCertificateVerify_ProcessAsTls1_m3714620977,
	TlsClientCertificateVerify_getClientCertRSA_m1950298306,
	TlsClientCertificateVerify_getUnsignedBigInteger_m4202515218,
	TlsClientFinished__ctor_m3582310837,
	TlsClientFinished__cctor_m823547282,
	TlsClientFinished_Update_m1874184699,
	TlsClientFinished_ProcessAsSsl3_m3712431540,
	TlsClientFinished_ProcessAsTls1_m366904288,
	TlsClientHello__ctor_m684319902,
	TlsClientHello_Update_m2360830622,
	TlsClientHello_ProcessAsSsl3_m3025764406,
	TlsClientHello_ProcessAsTls1_m214976322,
	TlsClientKeyExchange__ctor_m3216815788,
	TlsClientKeyExchange_ProcessAsSsl3_m1197450518,
	TlsClientKeyExchange_ProcessAsTls1_m447389176,
	TlsClientKeyExchange_ProcessCommon_m2250894231,
	TlsServerCertificate__ctor_m2064907602,
	TlsServerCertificate_Update_m2284218846,
	TlsServerCertificate_ProcessAsSsl3_m984023700,
	TlsServerCertificate_ProcessAsTls1_m1003804327,
	TlsServerCertificate_checkCertificateUsage_m2937881593,
	TlsServerCertificate_validateCertificates_m2051954893,
	TlsServerCertificate_checkServerIdentity_m627236923,
	TlsServerCertificate_checkDomainName_m3854661665,
	TlsServerCertificate_Match_m865697385,
	TlsServerCertificateRequest__ctor_m2125663103,
	TlsServerCertificateRequest_Update_m1099278369,
	TlsServerCertificateRequest_ProcessAsSsl3_m3563106795,
	TlsServerCertificateRequest_ProcessAsTls1_m1862660502,
	TlsServerFinished__ctor_m2983114510,
	TlsServerFinished__cctor_m2747700497,
	TlsServerFinished_Update_m841801115,
	TlsServerFinished_ProcessAsSsl3_m3221391108,
	TlsServerFinished_ProcessAsTls1_m749942180,
	TlsServerHello__ctor_m3506223265,
	TlsServerHello_Update_m23191077,
	TlsServerHello_ProcessAsSsl3_m2407954178,
	TlsServerHello_ProcessAsTls1_m4113542019,
	TlsServerHello_processProtocol_m1843286013,
	TlsServerHelloDone__ctor_m433659303,
	TlsServerHelloDone_ProcessAsSsl3_m3400236847,
	TlsServerHelloDone_ProcessAsTls1_m3696684855,
	TlsServerKeyExchange__ctor_m2476718815,
	TlsServerKeyExchange_Update_m2297485777,
	TlsServerKeyExchange_ProcessAsSsl3_m827932493,
	TlsServerKeyExchange_ProcessAsTls1_m3464884619,
	TlsServerKeyExchange_verifySignature_m4293518610,
	HandshakeMessage__ctor_m2034673671,
	HandshakeMessage__ctor_m1249436565,
	HandshakeMessage__ctor_m878609821,
	HandshakeMessage_get_Context_m800642781,
	HandshakeMessage_get_HandshakeType_m280165016,
	HandshakeMessage_get_ContentType_m3747812736,
	HandshakeMessage_Process_m455376707,
	HandshakeMessage_Update_m4254851083,
	HandshakeMessage_EncodeMessage_m872692033,
	HandshakeMessage_Compare_m3371152330,
	HttpsClientStream__ctor_m4047578492,
	HttpsClientStream_get_TrustFailure_m3656567659,
	HttpsClientStream_RaiseServerCertificateValidation_m3451092113,
	HttpsClientStream_U3CHttpsClientStreamU3Em__0_m4013616965,
	HttpsClientStream_U3CHttpsClientStreamU3Em__1_m845478519,
	PrivateKeySelectionCallback__ctor_m987086300,
	PrivateKeySelectionCallback_Invoke_m2587741347,
	PrivateKeySelectionCallback_BeginInvoke_m602585787,
	PrivateKeySelectionCallback_EndInvoke_m715554842,
	RecordProtocol__ctor_m2575003435,
	RecordProtocol__cctor_m3988108282,
	RecordProtocol_get_Context_m1248057354,
	RecordProtocol_SendRecord_m3734241850,
	RecordProtocol_ProcessChangeCipherSpec_m1652800479,
	RecordProtocol_GetMessage_m1005402943,
	RecordProtocol_BeginReceiveRecord_m1819664941,
	RecordProtocol_InternalReceiveRecordCallback_m3030502302,
	RecordProtocol_EndReceiveRecord_m908637232,
	RecordProtocol_ReceiveRecord_m2708100793,
	RecordProtocol_ReadRecordBuffer_m4167278120,
	RecordProtocol_ReadClientHelloV2_m2117048537,
	RecordProtocol_ReadStandardRecordBuffer_m871854570,
	RecordProtocol_ProcessAlert_m1435473841,
	RecordProtocol_SendAlert_m1591847065,
	RecordProtocol_SendAlert_m1438883877,
	RecordProtocol_SendAlert_m3376135484,
	RecordProtocol_SendChangeCipherSpec_m2765603748,
	RecordProtocol_BeginSendRecord_m1758967725,
	RecordProtocol_InternalSendRecordCallback_m3525244677,
	RecordProtocol_BeginSendRecord_m69707447,
	RecordProtocol_EndSendRecord_m706267873,
	RecordProtocol_SendRecord_m3284206208,
	RecordProtocol_EncodeRecord_m4068158653,
	RecordProtocol_EncodeRecord_m1038004725,
	RecordProtocol_encryptRecordFragment_m2829067512,
	RecordProtocol_decryptRecordFragment_m2696484119,
	RecordProtocol_Compare_m3923473508,
	RecordProtocol_ProcessCipherSpecV2Buffer_m4226882997,
	RecordProtocol_MapV2CipherCode_m3168062753,
	ReceiveRecordAsyncResult__ctor_m2154300256,
	ReceiveRecordAsyncResult_get_Record_m906314389,
	ReceiveRecordAsyncResult_get_ResultingBuffer_m4251431911,
	ReceiveRecordAsyncResult_get_InitialBuffer_m3909001354,
	ReceiveRecordAsyncResult_get_AsyncState_m605291345,
	ReceiveRecordAsyncResult_get_AsyncException_m933790275,
	ReceiveRecordAsyncResult_get_CompletedWithError_m2195810348,
	ReceiveRecordAsyncResult_get_AsyncWaitHandle_m2660886631,
	ReceiveRecordAsyncResult_get_IsCompleted_m2071034183,
	ReceiveRecordAsyncResult_SetComplete_m3952692939,
	ReceiveRecordAsyncResult_SetComplete_m2015699511,
	ReceiveRecordAsyncResult_SetComplete_m1812808650,
	SendRecordAsyncResult__ctor_m755282168,
	SendRecordAsyncResult_get_Message_m3308066534,
	SendRecordAsyncResult_get_AsyncState_m944538650,
	SendRecordAsyncResult_get_AsyncException_m2357394367,
	SendRecordAsyncResult_get_CompletedWithError_m3552731080,
	SendRecordAsyncResult_get_AsyncWaitHandle_m236430258,
	SendRecordAsyncResult_get_IsCompleted_m718960489,
	SendRecordAsyncResult_SetComplete_m3183925613,
	SendRecordAsyncResult_SetComplete_m52418369,
	RSASslSignatureDeformatter__ctor_m4138457112,
	RSASslSignatureDeformatter_VerifySignature_m3361685709,
	RSASslSignatureDeformatter_SetHashAlgorithm_m1618534065,
	RSASslSignatureDeformatter_SetKey_m377089225,
	RSASslSignatureFormatter__ctor_m356293291,
	RSASslSignatureFormatter_CreateSignature_m1712476209,
	RSASslSignatureFormatter_SetHashAlgorithm_m1350789049,
	RSASslSignatureFormatter_SetKey_m2867909730,
	SecurityParameters__ctor_m2022418048,
	SecurityParameters_get_Cipher_m1063581087,
	SecurityParameters_set_Cipher_m3243262012,
	SecurityParameters_get_ClientWriteMAC_m290543284,
	SecurityParameters_set_ClientWriteMAC_m4234763313,
	SecurityParameters_get_ServerWriteMAC_m3092245044,
	SecurityParameters_set_ServerWriteMAC_m2950694019,
	SecurityParameters_Clear_m3625150593,
	SslCipherSuite__ctor_m1651142478,
	SslCipherSuite_ComputeServerRecordMAC_m544101148,
	SslCipherSuite_ComputeClientRecordMAC_m2895545260,
	SslCipherSuite_ComputeMasterSecret_m2505358733,
	SslCipherSuite_ComputeKeys_m39830501,
	SslCipherSuite_prf_m180711818,
	SslClientStream__ctor_m665219218,
	SslClientStream__ctor_m2740959911,
	SslClientStream__ctor_m1028290302,
	SslClientStream__ctor_m2901368553,
	SslClientStream__ctor_m3136062194,
	SslClientStream_add_ServerCertValidation_m3989440226,
	SslClientStream_remove_ServerCertValidation_m2153244654,
	SslClientStream_add_ClientCertSelection_m3976789503,
	SslClientStream_remove_ClientCertSelection_m4153072844,
	SslClientStream_add_PrivateKeySelection_m302572209,
	SslClientStream_remove_PrivateKeySelection_m765497415,
	SslClientStream_add_ServerCertValidation2_m111614173,
	SslClientStream_remove_ServerCertValidation2_m775112605,
	SslClientStream_get_InputBuffer_m1496361990,
	SslClientStream_get_ClientCertificates_m823297919,
	SslClientStream_get_SelectedClientCertificate_m3564197154,
	SslClientStream_get_ServerCertValidationDelegate_m1821601874,
	SslClientStream_set_ServerCertValidationDelegate_m718527506,
	SslClientStream_get_ClientCertSelectionDelegate_m2578027297,
	SslClientStream_set_ClientCertSelectionDelegate_m3040495215,
	SslClientStream_get_PrivateKeyCertSelectionDelegate_m593300911,
	SslClientStream_set_PrivateKeyCertSelectionDelegate_m570404215,
	SslClientStream_Finalize_m2099061251,
	SslClientStream_Dispose_m3768043824,
	SslClientStream_OnBeginNegotiateHandshake_m657883870,
	SslClientStream_SafeReceiveRecord_m4103890754,
	SslClientStream_OnNegotiateHandshakeCallback_m3697285055,
	SslClientStream_OnLocalCertificateSelection_m2937715157,
	SslClientStream_get_HaveRemoteValidation2Callback_m3527076427,
	SslClientStream_OnRemoteCertificateValidation2_m462911753,
	SslClientStream_OnRemoteCertificateValidation_m1077840005,
	SslClientStream_RaiseServerCertificateValidation_m1966998054,
	SslClientStream_RaiseServerCertificateValidation2_m1950146374,
	SslClientStream_RaiseClientCertificateSelection_m2012516213,
	SslClientStream_OnLocalPrivateKeySelection_m1524003374,
	SslClientStream_RaisePrivateKeySelection_m3884094002,
	SslHandshakeHash__ctor_m2169281860,
	SslHandshakeHash_Initialize_m2904008621,
	SslHandshakeHash_HashFinal_m2085770661,
	SslHandshakeHash_HashCore_m491974851,
	SslHandshakeHash_CreateSignature_m3373793317,
	SslHandshakeHash_initializePad_m3858412911,
	SslStreamBase__ctor_m1772390304,
	SslStreamBase__cctor_m414404949,
	SslStreamBase_AsyncHandshakeCallback_m3808198600,
	SslStreamBase_get_MightNeedHandshake_m4001205381,
	SslStreamBase_NegotiateHandshake_m3208295742,
	SslStreamBase_RaiseLocalCertificateSelection_m1495337812,
	SslStreamBase_RaiseRemoteCertificateValidation_m4089603057,
	SslStreamBase_RaiseRemoteCertificateValidation2_m2332380679,
	SslStreamBase_RaiseLocalPrivateKeySelection_m3341177607,
	SslStreamBase_get_CheckCertRevocationStatus_m2587216488,
	SslStreamBase_set_CheckCertRevocationStatus_m2274274923,
	SslStreamBase_get_CipherAlgorithm_m3231557180,
	SslStreamBase_get_CipherStrength_m2901011851,
	SslStreamBase_get_HashAlgorithm_m1669198172,
	SslStreamBase_get_HashStrength_m3464493701,
	SslStreamBase_get_KeyExchangeStrength_m1007950534,
	SslStreamBase_get_KeyExchangeAlgorithm_m4272137713,
	SslStreamBase_get_SecurityProtocol_m3874437616,
	SslStreamBase_get_ServerCertificate_m3445084879,
	SslStreamBase_get_ServerCertificates_m1542603794,
	SslStreamBase_BeginNegotiateHandshake_m361657815,
	SslStreamBase_EndNegotiateHandshake_m2551208674,
	SslStreamBase_BeginRead_m3935587377,
	SslStreamBase_InternalBeginRead_m28581750,
	SslStreamBase_InternalReadCallback_m3716251866,
	SslStreamBase_InternalBeginWrite_m1167578983,
	SslStreamBase_InternalWriteCallback_m1908010440,
	SslStreamBase_BeginWrite_m4082615485,
	SslStreamBase_EndRead_m269976927,
	SslStreamBase_EndWrite_m3882228902,
	SslStreamBase_Close_m2220408450,
	SslStreamBase_Flush_m2233474192,
	SslStreamBase_Read_m1140853820,
	SslStreamBase_Read_m2130856565,
	SslStreamBase_Seek_m3906062762,
	SslStreamBase_SetLength_m1666023477,
	SslStreamBase_Write_m4276739856,
	SslStreamBase_Write_m4098670128,
	SslStreamBase_get_CanRead_m3111230573,
	SslStreamBase_get_CanSeek_m2857098779,
	SslStreamBase_get_CanWrite_m4021455675,
	SslStreamBase_get_Length_m3518888792,
	SslStreamBase_get_Position_m729702628,
	SslStreamBase_set_Position_m4050085577,
	SslStreamBase_Finalize_m3438261461,
	SslStreamBase_Dispose_m2592643090,
	SslStreamBase_resetBuffer_m717654181,
	SslStreamBase_checkDisposed_m167103266,
	InternalAsyncResult__ctor_m1440820003,
	InternalAsyncResult_get_ProceedAfterHandshake_m235102077,
	InternalAsyncResult_get_FromWrite_m2276317514,
	InternalAsyncResult_get_Buffer_m2345870937,
	InternalAsyncResult_get_Offset_m3979641628,
	InternalAsyncResult_get_Count_m163070607,
	InternalAsyncResult_get_BytesRead_m1696748377,
	InternalAsyncResult_get_AsyncState_m397037230,
	InternalAsyncResult_get_AsyncException_m2172420170,
	InternalAsyncResult_get_CompletedWithError_m2143240718,
	InternalAsyncResult_get_AsyncWaitHandle_m1330063583,
	InternalAsyncResult_get_IsCompleted_m3627122157,
	InternalAsyncResult_SetComplete_m2262116736,
	InternalAsyncResult_SetComplete_m3008594899,
	InternalAsyncResult_SetComplete_m3038718358,
	InternalAsyncResult_SetComplete_m2550592901,
	TlsCipherSuite__ctor_m2875460209,
	TlsCipherSuite_ComputeServerRecordMAC_m371468058,
	TlsCipherSuite_ComputeClientRecordMAC_m824858403,
	TlsCipherSuite_ComputeMasterSecret_m1531262657,
	TlsCipherSuite_ComputeKeys_m570963112,
	TlsClientSettings__ctor_m1818524944,
	TlsClientSettings_get_TargetHost_m893351267,
	TlsClientSettings_set_TargetHost_m2829519838,
	TlsClientSettings_get_Certificates_m2866953556,
	TlsClientSettings_set_Certificates_m4292277542,
	TlsClientSettings_get_ClientCertificate_m2674240563,
	TlsClientSettings_set_ClientCertificate_m630726241,
	TlsClientSettings_UpdateCertificateRSA_m3222256668,
	TlsException__ctor_m2548592610,
	TlsException__ctor_m3516023461,
	TlsException__ctor_m2453498583,
	TlsException__ctor_m3578249047,
	TlsException__ctor_m816616279,
	TlsException__ctor_m4040306894,
	TlsException_get_Alert_m2905729638,
	TlsServerSettings__ctor_m1409413246,
	TlsServerSettings_get_ServerKeyExchange_m4233741566,
	TlsServerSettings_set_ServerKeyExchange_m1498303633,
	TlsServerSettings_get_Certificates_m2571231720,
	TlsServerSettings_set_Certificates_m1376349003,
	TlsServerSettings_get_CertificateRSA_m3501361569,
	TlsServerSettings_get_RsaParameters_m493035931,
	TlsServerSettings_set_RsaParameters_m1718138909,
	TlsServerSettings_set_SignedParams_m2240200951,
	TlsServerSettings_get_CertificateRequest_m3263854561,
	TlsServerSettings_set_CertificateRequest_m242460290,
	TlsServerSettings_set_CertificateTypes_m1029670707,
	TlsServerSettings_set_DistinguisedNames_m2513131932,
	TlsServerSettings_UpdateCertificateRSA_m2005502988,
	TlsStream__ctor_m2122154764,
	TlsStream__ctor_m3106919526,
	TlsStream_get_EOF_m3852778119,
	TlsStream_get_CanWrite_m623673007,
	TlsStream_get_CanRead_m601653260,
	TlsStream_get_CanSeek_m2290267505,
	TlsStream_get_Position_m1465622516,
	TlsStream_set_Position_m3659917066,
	TlsStream_get_Length_m2803130091,
	TlsStream_ReadSmallValue_m3313813631,
	TlsStream_ReadByte_m2853286986,
	TlsStream_ReadInt16_m366065448,
	TlsStream_ReadInt24_m3078897848,
	TlsStream_ReadBytes_m1348995521,
	TlsStream_Write_m3441769662,
	TlsStream_Write_m2614401193,
	TlsStream_WriteInt24_m509623838,
	TlsStream_Write_m2647248461,
	TlsStream_Write_m4019989511,
	TlsStream_Reset_m678163232,
	TlsStream_ToArray_m726083748,
	TlsStream_Flush_m878954085,
	TlsStream_SetLength_m1352953404,
	TlsStream_Seek_m890504009,
	TlsStream_Read_m2651268204,
	TlsStream_Write_m4039524078,
	ValidationResult_get_Trusted_m4246094877,
	ValidationResult_get_ErrorCode_m1272221167,
	AuthorityKeyIdentifierExtension__ctor_m2051298225,
	AuthorityKeyIdentifierExtension_Decode_m2869485377,
	AuthorityKeyIdentifierExtension_get_Identifier_m2434240800,
	AuthorityKeyIdentifierExtension_ToString_m3811271106,
	BasicConstraintsExtension__ctor_m2389333248,
	BasicConstraintsExtension_Decode_m2057522670,
	BasicConstraintsExtension_Encode_m2144959798,
	BasicConstraintsExtension_get_CertificateAuthority_m2894004918,
	BasicConstraintsExtension_ToString_m2748478730,
	ExtendedKeyUsageExtension__ctor_m214837540,
	ExtendedKeyUsageExtension_Decode_m3459786271,
	ExtendedKeyUsageExtension_Encode_m774491648,
	ExtendedKeyUsageExtension_get_KeyPurpose_m3904597834,
	ExtendedKeyUsageExtension_ToString_m1490008558,
	GeneralNames__ctor_m3190992281,
	GeneralNames_get_DNSNames_m4093883004,
	GeneralNames_get_IPAddresses_m2015024776,
	GeneralNames_ToString_m382700226,
	KeyUsageExtension__ctor_m2315851155,
	KeyUsageExtension_Decode_m1806365563,
	KeyUsageExtension_Encode_m2131290841,
	KeyUsageExtension_Support_m3177031055,
	KeyUsageExtension_ToString_m2625433363,
	NetscapeCertTypeExtension__ctor_m1010860638,
	NetscapeCertTypeExtension_Decode_m1747060714,
	NetscapeCertTypeExtension_Support_m1919110708,
	NetscapeCertTypeExtension_ToString_m698340900,
	SubjectAltNameExtension__ctor_m3788410065,
	SubjectAltNameExtension_Decode_m3115455640,
	SubjectAltNameExtension_get_DNSNames_m407924658,
	SubjectAltNameExtension_get_IPAddresses_m2158173565,
	SubjectAltNameExtension_ToString_m1951517739,
	PKCS12__ctor_m3686132698,
	PKCS12__ctor_m420191098,
	PKCS12__ctor_m3099905214,
	PKCS12__cctor_m1298974184,
	PKCS12_Decode_m2613582753,
	PKCS12_Finalize_m1840705829,
	PKCS12_set_Password_m3068396088,
	PKCS12_get_IterationCount_m696607354,
	PKCS12_set_IterationCount_m3674735921,
	PKCS12_get_Keys_m511823029,
	PKCS12_get_Certificates_m1466186602,
	PKCS12_get_RNG_m1185158513,
	PKCS12_Compare_m3287655509,
	PKCS12_GetSymmetricAlgorithm_m25664652,
	PKCS12_Decrypt_m2042746515,
	PKCS12_Decrypt_m2043507842,
	PKCS12_Encrypt_m2897433779,
	PKCS12_GetExistingParameters_m2714337600,
	PKCS12_AddPrivateKey_m1337147522,
	PKCS12_ReadSafeBag_m1652738387,
	PKCS12_CertificateSafeBag_m2063371683,
	PKCS12_MAC_m2550261151,
	PKCS12_GetBytes_m1862498661,
	PKCS12_EncryptedContentInfo_m2113702247,
	PKCS12_AddCertificate_m3243108831,
	PKCS12_AddCertificate_m2221299882,
	PKCS12_RemoveCertificate_m1410858512,
	PKCS12_RemoveCertificate_m1604396208,
	PKCS12_Clone_m1751747700,
	PKCS12_get_MaximumPasswordLength_m532221644,
	DeriveBytes__ctor_m3565159824,
	DeriveBytes__cctor_m2393183491,
	DeriveBytes_set_HashName_m48048765,
	DeriveBytes_set_IterationCount_m70236908,
	DeriveBytes_set_Password_m873301047,
	DeriveBytes_set_Salt_m3037661865,
	DeriveBytes_Adjust_m494128802,
	DeriveBytes_Derive_m2548620875,
	DeriveBytes_DeriveKey_m1007628320,
	DeriveBytes_DeriveIV_m2578171456,
	DeriveBytes_DeriveMAC_m836988865,
	SafeBag__ctor_m2723828098,
	SafeBag_get_BagOID_m276201203,
	SafeBag_get_ASN1_m37969328,
	X501__cctor_m1602829784,
	X501_ToString_m773130296,
	X501_ToString_m1796048840,
	X501_AppendEntry_m3502912398,
	X509Certificate__ctor_m3475459813,
	X509Certificate__cctor_m1992081660,
	X509Certificate_Parse_m4105209092,
	X509Certificate_GetUnsignedBigInteger_m3662079033,
	X509Certificate_get_DSA_m2596778069,
	X509Certificate_set_DSA_m1138717006,
	X509Certificate_get_Extensions_m1318191537,
	X509Certificate_get_Hash_m278017058,
	X509Certificate_get_IssuerName_m321432801,
	X509Certificate_get_KeyAlgorithm_m2502051430,
	X509Certificate_get_KeyAlgorithmParameters_m1306763353,
	X509Certificate_set_KeyAlgorithmParameters_m2776139813,
	X509Certificate_get_PublicKey_m3958210525,
	X509Certificate_get_RSA_m3263620316,
	X509Certificate_set_RSA_m1338906205,
	X509Certificate_get_RawData_m3157491510,
	X509Certificate_get_SerialNumber_m4227883404,
	X509Certificate_get_Signature_m3247838418,
	X509Certificate_get_SignatureAlgorithm_m2212960349,
	X509Certificate_get_SubjectName_m2778083020,
	X509Certificate_get_ValidFrom_m3399788748,
	X509Certificate_get_ValidUntil_m3279023844,
	X509Certificate_get_Version_m1547908015,
	X509Certificate_get_IsCurrent_m3928620590,
	X509Certificate_WasCurrent_m1989659195,
	X509Certificate_VerifySignature_m4177921230,
	X509Certificate_VerifySignature_m4183940143,
	X509Certificate_VerifySignature_m2438360216,
	X509Certificate_get_IsSelfSigned_m3372771984,
	X509Certificate_GetIssuerName_m2937907079,
	X509Certificate_GetSubjectName_m1513474468,
	X509Certificate_PEM_m2313601497,
	X509CertificateCollection__ctor_m1906954588,
	X509CertificateCollection__ctor_m3725048025,
	X509CertificateCollection_System_Collections_IEnumerable_GetEnumerator_m817417299,
	X509CertificateCollection_get_Item_m3600069847,
	X509CertificateCollection_Add_m1771938890,
	X509CertificateCollection_AddRange_m3317735245,
	X509CertificateCollection_Contains_m2732358455,
	X509CertificateCollection_GetEnumerator_m501201006,
	X509CertificateCollection_GetHashCode_m4070171117,
	X509CertificateCollection_IndexOf_m3090707866,
	X509CertificateCollection_Remove_m647567924,
	X509CertificateCollection_Compare_m3213233510,
	X509CertificateEnumerator__ctor_m1154100625,
	X509CertificateEnumerator_System_Collections_IEnumerator_get_Current_m18197518,
	X509CertificateEnumerator_System_Collections_IEnumerator_MoveNext_m2020102016,
	X509CertificateEnumerator_System_Collections_IEnumerator_Reset_m116759774,
	X509CertificateEnumerator_get_Current_m1453228696,
	X509CertificateEnumerator_MoveNext_m2755272211,
	X509CertificateEnumerator_Reset_m213133671,
	X509Chain__ctor_m760047449,
	X509Chain__ctor_m765311802,
	X509Chain_get_Status_m1421504428,
	X509Chain_get_TrustAnchors_m1309469929,
	X509Chain_Build_m3136661355,
	X509Chain_IsValid_m3583476780,
	X509Chain_FindCertificateParent_m3719552933,
	X509Chain_FindCertificateRoot_m3075924856,
	X509Chain_IsTrusted_m1534983548,
	X509Chain_IsParent_m3602540272,
	X509Crl__ctor_m1587320464,
	X509Crl_Parse_m2363685775,
	X509Crl_get_Extensions_m2411577858,
	X509Crl_get_Hash_m4087585872,
	X509Crl_get_IssuerName_m788459859,
	X509Crl_get_NextUpdate_m3052840772,
	X509Crl_Compare_m161819802,
	X509Crl_GetCrlEntry_m3776945200,
	X509Crl_GetCrlEntry_m960893474,
	X509Crl_GetHashName_m871377626,
	X509Crl_VerifySignature_m318007533,
	X509Crl_VerifySignature_m1498544153,
	X509Crl_VerifySignature_m1270657239,
	X509CrlEntry__ctor_m2223032288,
	X509CrlEntry_get_SerialNumber_m1374642971,
	X509CrlEntry_get_RevocationDate_m620227204,
	X509CrlEntry_get_Extensions_m2956389379,
	X509Extension__ctor_m555060717,
	X509Extension__ctor_m110854038,
	X509Extension_Decode_m2854984614,
	X509Extension_Encode_m1708484574,
	X509Extension_get_Oid_m3805899428,
	X509Extension_get_Critical_m1973472787,
	X509Extension_get_Value_m708908506,
	X509Extension_Equals_m1966452623,
	X509Extension_GetHashCode_m1676727038,
	X509Extension_WriteLine_m881048957,
	X509Extension_ToString_m3680485954,
	X509ExtensionCollection__ctor_m2525499396,
	X509ExtensionCollection__ctor_m385246626,
	X509ExtensionCollection_System_Collections_IEnumerable_GetEnumerator_m200825157,
	X509ExtensionCollection_IndexOf_m1736569985,
	X509ExtensionCollection_get_Item_m2725489036,
	X509Store__ctor_m1760505199,
	X509Store_get_Certificates_m4140352539,
	X509Store_get_Crls_m420681093,
	X509Store_Load_m1402394016,
	X509Store_LoadCertificate_m2669958390,
	X509Store_LoadCrl_m2935225626,
	X509Store_CheckStore_m2202048695,
	X509Store_BuildCertificatesCollection_m1314134402,
	X509Store_BuildCrlsCollection_m1438245430,
	X509StoreManager_get_CurrentUser_m1265783936,
	X509StoreManager_get_LocalMachine_m3731158609,
	X509StoreManager_get_TrustedRootCertificates_m2693658780,
	X509Stores__ctor_m3803881044,
	X509Stores_get_TrustedRoot_m1991417710,
	X509Stores_Open_m3585119639,
	Locale_GetText_m1492548562,
	Locale_GetText_m2618335777,
	KeyBuilder_get_Rng_m1402732562,
	KeyBuilder_Key_m2068773312,
	KeyBuilder_IV_m1539373917,
	SymmetricTransform__ctor_m2345069913,
	SymmetricTransform_System_IDisposable_Dispose_m53307466,
	SymmetricTransform_Finalize_m135047551,
	SymmetricTransform_Dispose_m2605546990,
	SymmetricTransform_get_CanReuseTransform_m3415952586,
	SymmetricTransform_Transform_m2512799522,
	SymmetricTransform_CBC_m2861918276,
	SymmetricTransform_CFB_m502402308,
	SymmetricTransform_OFB_m610835733,
	SymmetricTransform_CTS_m2602812767,
	SymmetricTransform_CheckInput_m2580534911,
	SymmetricTransform_TransformBlock_m3335701745,
	SymmetricTransform_get_KeepLastBlock_m1391419215,
	SymmetricTransform_InternalTransformBlock_m713902379,
	SymmetricTransform_Random_m2211116558,
	SymmetricTransform_ThrowBadPaddingException_m59444602,
	SymmetricTransform_FinalEncrypt_m1824314829,
	SymmetricTransform_FinalDecrypt_m318329064,
	SymmetricTransform_TransformFinalBlock_m3934660993,
	Check_Source_m2478120104,
	Check_SourceAndPredicate_m477734566,
	ExtensionAttribute__ctor_m831032830,
	Aes__ctor_m2077794217,
	AesManaged__ctor_m2374737792,
	AesManaged_GenerateIV_m3285618721,
	AesManaged_GenerateKey_m3391128746,
	AesManaged_CreateDecryptor_m169246569,
	AesManaged_CreateEncryptor_m857427945,
	AesManaged_get_IV_m2426183383,
	AesManaged_set_IV_m4034845936,
	AesManaged_get_Key_m578016737,
	AesManaged_set_Key_m2435629111,
	AesManaged_get_KeySize_m730303170,
	AesManaged_set_KeySize_m2370352321,
	AesManaged_CreateDecryptor_m3604052453,
	AesManaged_CreateEncryptor_m615540925,
	AesManaged_Dispose_m1169876397,
	AesTransform__ctor_m2896269147,
	AesTransform__cctor_m1118640786,
	AesTransform_ECB_m663232370,
	AesTransform_SubByte_m787919468,
	AesTransform_Encrypt128_m745777362,
	AesTransform_Decrypt128_m2221214126,
	AddComponentMenu__ctor_m3629709883,
	AnimationCurve__ctor_m3076316537,
	AnimationCurve__ctor_m566668622,
	AnimationCurve_Cleanup_m243787505,
	AnimationCurve_Finalize_m4265665064,
	AnimationCurve_Init_m1956642396,
	Application_CallLowMemory_m1626726542,
	Application_CallLogCallback_m3808806632,
	Application_InvokeOnBeforeRender_m818804610,
	LogCallback__ctor_m2513058278,
	LogCallback_Invoke_m2853261436,
	LogCallback_BeginInvoke_m1314223040,
	LogCallback_EndInvoke_m3721814140,
	LowMemoryCallback__ctor_m4170563135,
	LowMemoryCallback_Invoke_m3732620697,
	LowMemoryCallback_BeginInvoke_m3629193632,
	LowMemoryCallback_EndInvoke_m80814921,
	AsyncOperation_InternalDestroy_m1747224711,
	AsyncOperation_Finalize_m1200536139,
	AsyncOperation_InvokeCompletionEvent_m737953261,
	AttributeHelperEngine_GetParentTypeDisallowingMultipleInclusion_m1021838127,
	AttributeHelperEngine_GetRequiredComponents_m2091293497,
	AttributeHelperEngine_CheckIsEditorScript_m2212222995,
	AttributeHelperEngine_GetDefaultExecutionOrderFor_m1362990272,
	AttributeHelperEngine__cctor_m3752452713,
	Behaviour__ctor_m1233868026,
	Behaviour_get_enabled_m1304335371,
	UnmarshalledAttribute__ctor_m3412418881,
	Camera_get_nearClipPlane_m167155657,
	Camera_get_farClipPlane_m1366251422,
	Camera_get_cullingMask_m2771596515,
	Camera_get_eventMask_m3958975071,
	Camera_get_pixelRect_m3857366896,
	Camera_INTERNAL_get_pixelRect_m3102036321,
	Camera_get_targetTexture_m903246907,
	Camera_get_clearFlags_m3577200564,
	Camera_ScreenPointToRay_m2744754050,
	Camera_INTERNAL_CALL_ScreenPointToRay_m244366202,
	Camera_get_allCamerasCount_m3527489092,
	Camera_GetAllCameras_m557431570,
	Camera_FireOnPreCull_m2186829015,
	Camera_FireOnPreRender_m1563217163,
	Camera_FireOnPostRender_m4290449375,
	Camera_RaycastTry_m208704706,
	Camera_INTERNAL_CALL_RaycastTry_m2903709223,
	Camera_RaycastTry2D_m3780933465,
	Camera_INTERNAL_CALL_RaycastTry2D_m700194433,
	CameraCallback__ctor_m1713115355,
	CameraCallback_Invoke_m144364913,
	CameraCallback_BeginInvoke_m4085641401,
	CameraCallback_EndInvoke_m2093829129,
	ClassLibraryInitializer_Init_m4274939016,
	Color__ctor_m211164015_AdjustorThunk,
	Color_ToString_m4292820139_AdjustorThunk,
	Color_GetHashCode_m3278237850_AdjustorThunk,
	Color_Equals_m3350585184_AdjustorThunk,
	Color_get_white_m3869699080,
	Color_op_Implicit_m4266979643,
	Component__ctor_m2592563656,
	Component_get_gameObject_m2710707649,
	Component_GetComponentFastPath_m3479205869,
	Coroutine_ReleaseCoroutine_m329501834,
	Coroutine_Finalize_m1627900536,
	CSSMeasureFunc__ctor_m324136384,
	CSSMeasureFunc_Invoke_m2998463313,
	CSSMeasureFunc_BeginInvoke_m3083536890,
	CSSMeasureFunc_EndInvoke_m231879325,
	Native_CSSNodeGetMeasureFunc_m4025476427,
	Native_CSSNodeMeasureInvoke_m1142609737,
	Native__cctor_m2631422355,
	CullingGroup_Finalize_m3186244642,
	CullingGroup_Dispose_m1436928443,
	CullingGroup_SendEvents_m3781512400,
	CullingGroup_FinalizerFailure_m3512716862,
	StateChanged__ctor_m58376528,
	StateChanged_Invoke_m1291922880,
	StateChanged_BeginInvoke_m1155489289,
	StateChanged_EndInvoke_m2092849007,
	Debug_get_unityLogger_m2700972930,
	Debug_Log_m2305178311,
	Debug_LogError_m1070074371,
	Debug_LogException_m57812199,
	Debug__cctor_m1871084662,
	DebugLogHandler__ctor_m2673380417,
	DebugLogHandler_Internal_Log_m1682417589,
	DebugLogHandler_Internal_LogException_m3264502143,
	DebugLogHandler_LogFormat_m3907003566,
	DebugLogHandler_LogException_m611967214,
	DefaultExecutionOrder_get_order_m3989798846,
	Display__ctor_m1199150772,
	Display__ctor_m906575725,
	Display_RecreateDisplayList_m3512407007,
	Display_FireDisplaysUpdated_m3194723814,
	Display__cctor_m3585651147,
	DisplaysUpdatedDelegate__ctor_m1345164380,
	DisplaysUpdatedDelegate_Invoke_m1559204117,
	DisplaysUpdatedDelegate_BeginInvoke_m3281943358,
	DisplaysUpdatedDelegate_EndInvoke_m3619400991,
	ArgumentCache__ctor_m2562784622,
	ArgumentCache_get_unityObjectArgument_m2559081415,
	ArgumentCache_get_unityObjectArgumentAssemblyTypeName_m3489746291,
	ArgumentCache_get_intArgument_m1266469321,
	ArgumentCache_get_floatArgument_m703828235,
	ArgumentCache_get_stringArgument_m574520374,
	ArgumentCache_get_boolArgument_m600683273,
	ArgumentCache_TidyAssemblyTypeName_m2531759392,
	ArgumentCache_OnBeforeSerialize_m2612988137,
	ArgumentCache_OnAfterDeserialize_m4282301992,
	BaseInvokableCall__ctor_m3419792134,
	BaseInvokableCall_AllowInvoke_m1751538175,
	InvokableCall__ctor_m2974797033,
	InvokableCall_add_Delegate_m2772852427,
	InvokableCall_remove_Delegate_m599836031,
	InvokableCall_Invoke_m1952365720,
	InvokableCallList__ctor_m386930243,
	InvokableCallList_AddPersistentInvokableCall_m3306354928,
	InvokableCallList_ClearPersistent_m1797712781,
	InvokableCallList_PrepareInvoke_m2613780459,
	PersistentCall__ctor_m2649643936,
	PersistentCall_get_target_m4082064461,
	PersistentCall_get_methodName_m2687182531,
	PersistentCall_get_mode_m2357383679,
	PersistentCall_get_arguments_m1844117721,
	PersistentCall_IsValid_m2172430097,
	PersistentCall_GetRuntimeCall_m1057898321,
	PersistentCall_GetObjectCall_m2893916170,
	PersistentCallGroup__ctor_m2062212555,
	PersistentCallGroup_Initialize_m644878480,
	UnityAction__ctor_m1074277545,
	UnityAction_Invoke_m2515763748,
	UnityAction_BeginInvoke_m3164501995,
	UnityAction_EndInvoke_m650613571,
	UnityEvent__ctor_m811779962,
	UnityEvent_FindMethod_Impl_m3636630720,
	UnityEvent_GetDelegate_m452312904,
	UnityEventBase__ctor_m1419821417,
	UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m303227655,
	UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m1452364050,
	UnityEventBase_FindMethod_m595753448,
	UnityEventBase_FindMethod_m3931843611,
	UnityEventBase_DirtyPersistentCalls_m1507352076,
	UnityEventBase_RebuildPersistentCallsIfNeeded_m256317675,
	UnityEventBase_PrepareInvoke_m3974578209,
	UnityEventBase_ToString_m2520642732,
	UnityEventBase_GetValidMethodInfo_m1004474682,
	RenderPipelineManager_get_currentPipeline_m3103495424,
	RenderPipelineManager_set_currentPipeline_m3602131277,
	RenderPipelineManager_CleanupRenderPipeline_m309528094,
	RenderPipelineManager_DoRenderLoop_Internal_m124400281,
	RenderPipelineManager_PrepareRenderPipeline_m896043942,
	ScriptableRenderContext__ctor_m242682668_AdjustorThunk,
	GameObject__ctor_m717643193,
	GameObject_SendMessage_m239132133,
	GameObject_Internal_CreateGameObject_m1823697062,
	Gradient__ctor_m2009183950,
	Gradient_Init_m1188969969,
	Gradient_Cleanup_m2195794132,
	Gradient_Finalize_m3210182836,
	GUIElement__ctor_m3581839174,
	GUILayer_HitTest_m1935830346,
	GUILayer_INTERNAL_CALL_HitTest_m422396558,
	Input_GetMouseButton_m294091231,
	Input_GetMouseButtonDown_m3867833682,
	Input_get_mousePosition_m3311395491,
	Input_INTERNAL_get_mousePosition_m3613716531,
	Input__cctor_m865649194,
	DefaultValueAttribute__ctor_m2061771615,
	DefaultValueAttribute_get_Value_m1203122007,
	DefaultValueAttribute_Equals_m3424877481,
	DefaultValueAttribute_GetHashCode_m2657444706,
	LocalNotification_Destroy_m1982161855,
	LocalNotification_Finalize_m874233874,
	LocalNotification__cctor_m140754005,
	RemoteNotification_Destroy_m4186758914,
	RemoteNotification_Finalize_m2240751185,
	Logger__ctor_m4234190541,
	Logger_get_logHandler_m1719487699,
	Logger_set_logHandler_m6066634,
	Logger_get_logEnabled_m2290808316,
	Logger_set_logEnabled_m1662937540,
	Logger_get_filterLogType_m426955796,
	Logger_set_filterLogType_m2745985065,
	Logger_IsLogTypeAllowed_m1817789334,
	Logger_GetString_m2492438982,
	Logger_Log_m2914072330,
	Logger_LogFormat_m1581286573,
	Logger_LogException_m1366382550,
	ManagedStreamHelpers_ValidateLoadFromStream_m2537091486,
	ManagedStreamHelpers_ManagedStreamRead_m4024070239,
	ManagedStreamHelpers_ManagedStreamSeek_m504681536,
	ManagedStreamHelpers_ManagedStreamLength_m1632106524,
	Mathf_Abs_m2765530332,
	Mathf_Max_m3084430723,
	Mathf_Approximately_m1455183363,
	Mathf__cctor_m3961786392,
	Matrix4x4__ctor_m614016869_AdjustorThunk,
	Matrix4x4_GetHashCode_m2086764285_AdjustorThunk,
	Matrix4x4_Equals_m1131202087_AdjustorThunk,
	Matrix4x4_GetColumn_m1315812967_AdjustorThunk,
	Matrix4x4_get_identity_m3045349954,
	Matrix4x4_ToString_m861927258_AdjustorThunk,
	Matrix4x4__cctor_m770025791,
	MonoBehaviour__ctor_m3125940511,
	NativeClassAttribute__ctor_m714604756,
	NativeClassAttribute_set_QualifiedNativeName_m875392448,
	MessageEventArgs__ctor_m1689534235,
	PlayerConnection__ctor_m527712293,
	PlayerConnection_get_instance_m2190863265,
	PlayerConnection_CreateInstance_m4274468596,
	PlayerConnection_MessageCallbackInternal_m469132778,
	PlayerConnection_ConnectedCallbackInternal_m1135070508,
	PlayerConnection_DisconnectedCallback_m2631702132,
	PlayerEditorConnectionEvents__ctor_m1455086094,
	PlayerEditorConnectionEvents_InvokeMessageIdSubscribers_m2032614817,
	U3CInvokeMessageIdSubscribersU3Ec__AnonStorey0__ctor_m1102967907,
	U3CInvokeMessageIdSubscribersU3Ec__AnonStorey0_U3CU3Em__0_m974971376,
	ConnectionChangeEvent__ctor_m1785426546,
	MessageEvent__ctor_m3751876958,
	MessageTypeSubscribers__ctor_m2303372877,
	MessageTypeSubscribers_get_MessageTypeId_m2156269021,
	Object__ctor_m387379410,
	Object_set_hideFlags_m3010783544,
	Object_ToString_m842583452,
	Object_GetHashCode_m3084030858,
	Object_Equals_m4058157391,
	Object_op_Implicit_m3975891434,
	Object_CompareBaseObjects_m3289384931,
	Object_IsNativeObjectAlive_m2916611270,
	Object_GetCachedPtr_m2018497938,
	Object_op_Equality_m2743582905,
	Object_op_Inequality_m3819358377,
	Object__cctor_m3598847078,
	Playable__ctor_m3942516336_AdjustorThunk,
	Playable_get_Null_m1034340665,
	Playable_GetHandle_m2973432975_AdjustorThunk,
	Playable_Equals_m1196118830_AdjustorThunk,
	Playable__cctor_m2479433417,
	PlayableAsset__ctor_m1918338898,
	PlayableAsset_get_duration_m2883676437,
	PlayableAsset_Internal_CreatePlayable_m4149673157,
	PlayableAsset_Internal_GetPlayableAssetDuration_m3126328874,
	PlayableBehaviour__ctor_m1062833712,
	PlayableBehaviour_Clone_m1846106316,
	PlayableBinding__cctor_m1831276805,
	PlayableHandle_get_Null_m2516265857,
	PlayableHandle_op_Equality_m2397680455,
	PlayableHandle_Equals_m771590314_AdjustorThunk,
	PlayableHandle_GetHashCode_m1922874376_AdjustorThunk,
	PlayableHandle_CompareVersion_m2720829020,
	PlayableOutput__ctor_m451176029_AdjustorThunk,
	PlayableOutput_GetHandle_m1350475571_AdjustorThunk,
	PlayableOutput_Equals_m2076999710_AdjustorThunk,
	PlayableOutput__cctor_m43115862,
	PlayableOutputHandle_get_Null_m3044568124,
	PlayableOutputHandle_GetHashCode_m4127769214_AdjustorThunk,
	PlayableOutputHandle_op_Equality_m1435240927,
	PlayableOutputHandle_Equals_m669007655_AdjustorThunk,
	PlayableOutputHandle_CompareVersion_m3459929500,
	PropertyName__ctor_m2196050290_AdjustorThunk,
	PropertyName__ctor_m1851822729_AdjustorThunk,
	PropertyName__ctor_m4077360820_AdjustorThunk,
	PropertyName_op_Equality_m2654803850,
	PropertyName_op_Inequality_m3678544633,
	PropertyName_GetHashCode_m1226794333_AdjustorThunk,
	PropertyName_Equals_m3895751744_AdjustorThunk,
	PropertyName_op_Implicit_m2033544225,
	PropertyName_op_Implicit_m3783576796,
	PropertyName_ToString_m1969781279_AdjustorThunk,
	PropertyNameUtils_PropertyNameFromString_m3539940257,
	PropertyNameUtils_PropertyNameFromString_Injected_m3547369790,
	Ray_get_direction_m1714502355_AdjustorThunk,
	Ray_ToString_m3743478486_AdjustorThunk,
	Rect_get_x_m4046749491_AdjustorThunk,
	Rect_get_y_m1845352024_AdjustorThunk,
	Rect_get_width_m710188174_AdjustorThunk,
	Rect_get_height_m262661000_AdjustorThunk,
	Rect_get_xMin_m490539547_AdjustorThunk,
	Rect_get_yMin_m1596955344_AdjustorThunk,
	Rect_get_xMax_m3140092843_AdjustorThunk,
	Rect_get_yMax_m1499979192_AdjustorThunk,
	Rect_Contains_m594593135_AdjustorThunk,
	Rect_GetHashCode_m1190625508_AdjustorThunk,
	Rect_Equals_m2317586560_AdjustorThunk,
	Rect_ToString_m2557162916_AdjustorThunk,
	RectTransform_SendReapplyDrivenProperties_m2257690066,
	ReapplyDrivenProperties__ctor_m4026315179,
	ReapplyDrivenProperties_Invoke_m2731383309,
	ReapplyDrivenProperties_BeginInvoke_m287277383,
	ReapplyDrivenProperties_EndInvoke_m3904559314,
	RequireComponent__ctor_m3242490219,
	Scene_get_handle_m2811252972_AdjustorThunk,
	Scene_GetHashCode_m3840110465_AdjustorThunk,
	Scene_Equals_m1182027264_AdjustorThunk,
	SceneManager_Internal_SceneLoaded_m2309132009,
	SceneManager_Internal_SceneUnloaded_m2410300733,
	SceneManager_Internal_ActiveSceneChanged_m667757325,
	ScriptableObject__ctor_m3589061004,
	ScriptableObject_Internal_CreateScriptableObject_m3652231296,
	ScriptableObject_CreateInstance_m1780655698,
	ScriptableObject_CreateInstanceFromType_m1134094777,
	GeneratedByOldBindingsGeneratorAttribute__ctor_m1819602998,
	RequiredByNativeCodeAttribute__ctor_m3493315205,
	UsedByNativeCodeAttribute__ctor_m4001769068,
	SendMouseEvents_SetMouseMoved_m2508391903,
	SendMouseEvents_HitTestLegacyGUI_m3813264185,
	SendMouseEvents_DoSendMouseEvents_m3686682366,
	SendMouseEvents_SendEvents_m2210188208,
	SendMouseEvents__cctor_m1686622767,
	HitInfo_SendMessage_m2389970774_AdjustorThunk,
	HitInfo_op_Implicit_m1771688566,
	HitInfo_Compare_m1712556816,
	FormerlySerializedAsAttribute__ctor_m219264990,
	SerializeField__ctor_m3401225370,
	SetupCoroutine_InvokeMoveNext_m1274895524,
	SetupCoroutine_InvokeMember_m3346816490,
	StackTraceUtility_SetProjectFolder_m3603953530,
	StackTraceUtility_ExtractStackTrace_m1291404532,
	StackTraceUtility_IsSystemStacktraceType_m172104614,
	StackTraceUtility_ExtractStringFromExceptionInternal_m3699450383,
	StackTraceUtility_PostprocessStacktrace_m4246116829,
	StackTraceUtility_ExtractFormattedStackTrace_m382049542,
	StackTraceUtility__cctor_m1952330627,
	Texture__ctor_m2053337444,
	Texture2D__ctor_m975450915,
	Texture2D_Internal_Create_m1993173660,
	ThreadAndSerializationSafeAttribute__ctor_m211248632,
	Transform__ctor_m963699039,
	Transform_get_childCount_m849482613,
	Transform_GetEnumerator_m3477035503,
	Transform_GetChild_m4117681253,
	Enumerator__ctor_m526857282,
	Enumerator_get_Current_m3510733711,
	Enumerator_MoveNext_m52240011,
	Enumerator_Reset_m2946229432,
	SpriteAtlasManager_RequestAtlas_m2195264182,
	SpriteAtlasManager_Register_m1381418004,
	SpriteAtlasManager__cctor_m4142695825,
	RequestAtlasCallback__ctor_m2170763641,
	RequestAtlasCallback_Invoke_m1258427264,
	RequestAtlasCallback_BeginInvoke_m520960351,
	RequestAtlasCallback_EndInvoke_m404102494,
	UnhandledExceptionHandler_RegisterUECatcher_m663990726,
	UnhandledExceptionHandler_HandleUnhandledException_m1441885697,
	UnhandledExceptionHandler_PrintException_m2880095263,
	UnhandledExceptionHandler_NativeUnhandledExceptionHandler_m4249568766,
	UnityException__ctor_m2243312541,
	UnityException__ctor_m3073494132,
	UnityLogWriter__ctor_m958796054,
	UnityLogWriter_WriteStringToUnityLog_m237899344,
	UnityLogWriter_Init_m3153067179,
	UnityLogWriter_Write_m398588393,
	UnityLogWriter_Write_m4129027096,
	UnityString_Format_m2513837565,
	UnitySynchronizationContext__ctor_m1361814383,
	UnitySynchronizationContext_Exec_m2445278172,
	UnitySynchronizationContext_InitializeSynchronizationContext_m2662403408,
	UnitySynchronizationContext_ExecuteTasks_m3600858646,
	WorkRequest_Invoke_m595488964_AdjustorThunk,
	Vector3__ctor_m800217684_AdjustorThunk,
	Vector3_GetHashCode_m3101213102_AdjustorThunk,
	Vector3_Equals_m2043273264_AdjustorThunk,
	Vector3_ToString_m792339301_AdjustorThunk,
	Vector3__cctor_m1700583052,
	Vector4__ctor_m3054764420_AdjustorThunk,
	Vector4_GetHashCode_m1136089151_AdjustorThunk,
	Vector4_Equals_m480591454_AdjustorThunk,
	Vector4_ToString_m2058083437_AdjustorThunk,
	Vector4__cctor_m1314409204,
	WritableAttribute__ctor_m474281766,
	MathfInternal__cctor_m4032146116,
	NetFxCoreExtensions_CreateDelegate_m3544893108,
	AudioClipPlayable_GetHandle_m334032914_AdjustorThunk,
	AudioClipPlayable_Equals_m2753778244_AdjustorThunk,
	AudioMixerPlayable_GetHandle_m15851785_AdjustorThunk,
	AudioMixerPlayable_Equals_m1432794900_AdjustorThunk,
	AudioClip__ctor_m2983411473,
	AudioClip_get_ambisonic_m2163265490,
	AudioClip_InvokePCMReaderCallback_Internal_m779593384,
	AudioClip_InvokePCMSetPositionCallback_Internal_m1609205883,
	PCMReaderCallback__ctor_m3697100253,
	PCMReaderCallback_Invoke_m790641721,
	PCMReaderCallback_BeginInvoke_m2104147499,
	PCMReaderCallback_EndInvoke_m830652813,
	PCMSetPositionCallback__ctor_m2841148579,
	PCMSetPositionCallback_Invoke_m1319562623,
	PCMSetPositionCallback_BeginInvoke_m3995879997,
	PCMSetPositionCallback_EndInvoke_m4282839021,
	AudioExtensionDefinition_GetExtensionType_m1990966740,
	AudioExtensionManager_GetAudioListener_m3189978351,
	AudioExtensionManager_AddSpatializerExtension_m2838969561,
	AudioExtensionManager_AddAmbisonicDecoderExtension_m2920682946,
	AudioExtensionManager_WriteExtensionProperties_m2859894919,
	AudioExtensionManager_AddSpatializerExtension_m3723108260,
	AudioExtensionManager_WriteExtensionProperties_m375337912,
	AudioExtensionManager_GetListenerSpatializerExtensionType_m1875002759,
	AudioExtensionManager_GetListenerSpatializerExtensionEditorType_m129125889,
	AudioExtensionManager_GetSourceSpatializerExtensionType_m2709576922,
	AudioExtensionManager_AddExtensionToManager_m3314625348,
	AudioExtensionManager_RemoveExtensionFromManager_m2876808799,
	AudioExtensionManager_Update_m244409286,
	AudioExtensionManager_GetReadyToPlay_m855413997,
	AudioExtensionManager_RegisterBuiltinDefinitions_m2288387409,
	AudioExtensionManager__cctor_m3951697045,
	AudioListener_GetNumExtensionProperties_m2279481642,
	AudioListener_ReadExtensionName_m457651115,
	AudioListener_INTERNAL_CALL_ReadExtensionName_m3825001381,
	AudioListener_ReadExtensionPropertyName_m2682178932,
	AudioListener_INTERNAL_CALL_ReadExtensionPropertyName_m3514206420,
	AudioListener_ReadExtensionPropertyValue_m2214914024,
	AudioListener_ClearExtensionProperties_m3758880915,
	AudioListener_INTERNAL_CALL_ClearExtensionProperties_m1492170042,
	AudioListener_AddExtension_m1451255105,
	AudioListenerExtension_get_audioListener_m3307089112,
	AudioListenerExtension_set_audioListener_m3864337887,
	AudioListenerExtension_WriteExtensionProperty_m1940049502,
	AudioListenerExtension_ExtensionUpdate_m131363356,
	AudioSettings_GetSpatializerPluginName_m2848677236,
	AudioSettings_InvokeOnAudioConfigurationChanged_m3184710774,
	AudioSettings_InvokeOnAudioManagerUpdate_m2154005943,
	AudioSettings_InvokeOnAudioSourcePlay_m2060412953,
	AudioSettings_GetAmbisonicDecoderPluginName_m70128080,
	AudioConfigurationChangeHandler__ctor_m3404555898,
	AudioConfigurationChangeHandler_Invoke_m2449447955,
	AudioConfigurationChangeHandler_BeginInvoke_m2216313367,
	AudioConfigurationChangeHandler_EndInvoke_m250397530,
	AudioSource_get_clip_m2340175708,
	AudioSource_get_isPlaying_m1796357815,
	AudioSource_get_spatializeInternal_m3026556515,
	AudioSource_get_spatialize_m2640512310,
	AudioSource_GetNumExtensionProperties_m709284950,
	AudioSource_ReadExtensionName_m3575108826,
	AudioSource_INTERNAL_CALL_ReadExtensionName_m940874956,
	AudioSource_ReadExtensionPropertyName_m2931479574,
	AudioSource_INTERNAL_CALL_ReadExtensionPropertyName_m1096137561,
	AudioSource_ReadExtensionPropertyValue_m1555844817,
	AudioSource_ClearExtensionProperties_m3916939876,
	AudioSource_INTERNAL_CALL_ClearExtensionProperties_m3670533364,
	AudioSource_AddSpatializerExtension_m437310066,
	AudioSource_AddAmbisonicExtension_m2770743028,
	AudioSourceExtension_get_audioSource_m1457396452,
	AudioSourceExtension_set_audioSource_m2372617077,
	AudioSourceExtension_WriteExtensionProperty_m2994663373,
	AudioSourceExtension_Play_m2504555527,
	AudioSourceExtension_Stop_m3164232815,
	AudioSourceExtension_ExtensionUpdate_m1678361052,
	GameCenterPlatform__ctor_m1979894975,
	GameCenterPlatform_Internal_Authenticate_m2072365377,
	GameCenterPlatform_Internal_Authenticated_m2873448009,
	GameCenterPlatform_Internal_UserName_m2866948064,
	GameCenterPlatform_Internal_UserID_m1391210441,
	GameCenterPlatform_Internal_Underage_m3153493531,
	GameCenterPlatform_Internal_UserImage_m3635042813,
	GameCenterPlatform_Internal_LoadFriends_m2096768347,
	GameCenterPlatform_Internal_LoadAchievementDescriptions_m4285687138,
	GameCenterPlatform_Internal_LoadAchievements_m1476802759,
	GameCenterPlatform_Internal_ReportProgress_m4247572756,
	GameCenterPlatform_Internal_ReportScore_m3392134640,
	GameCenterPlatform_Internal_LoadScores_m3934381776,
	GameCenterPlatform_Internal_ShowAchievementsUI_m2604671237,
	GameCenterPlatform_Internal_ShowLeaderboardUI_m2609988910,
	GameCenterPlatform_Internal_LoadUsers_m2376904235,
	GameCenterPlatform_Internal_ResetAllAchievements_m860406903,
	GameCenterPlatform_Internal_ShowDefaultAchievementBanner_m3807438363,
	GameCenterPlatform_ResetAllAchievements_m766731470,
	GameCenterPlatform_ShowDefaultAchievementCompletionBanner_m1206567558,
	GameCenterPlatform_ShowLeaderboardUI_m76802803,
	GameCenterPlatform_Internal_ShowSpecificLeaderboardUI_m1324819955,
	GameCenterPlatform_ClearAchievementDescriptions_m1201462552,
	GameCenterPlatform_SetAchievementDescription_m3265147368,
	GameCenterPlatform_SetAchievementDescriptionImage_m433180119,
	GameCenterPlatform_TriggerAchievementDescriptionCallback_m1368418878,
	GameCenterPlatform_AuthenticateCallbackWrapper_m3518532938,
	GameCenterPlatform_ClearFriends_m3997297748,
	GameCenterPlatform_SetFriends_m3156453279,
	GameCenterPlatform_SetFriendImage_m2565530221,
	GameCenterPlatform_TriggerFriendsCallbackWrapper_m3549927236,
	GameCenterPlatform_AchievementCallbackWrapper_m2393842948,
	GameCenterPlatform_ProgressCallbackWrapper_m1761357634,
	GameCenterPlatform_ScoreCallbackWrapper_m1017495084,
	GameCenterPlatform_ScoreLoaderCallbackWrapper_m2497892497,
	GameCenterPlatform_UnityEngine_SocialPlatforms_ISocialPlatform_LoadFriends_m3712251814,
	GameCenterPlatform_UnityEngine_SocialPlatforms_ISocialPlatform_Authenticate_m1902473855,
	GameCenterPlatform_UnityEngine_SocialPlatforms_ISocialPlatform_Authenticate_m473704383,
	GameCenterPlatform_get_localUser_m1695301324,
	GameCenterPlatform_PopulateLocalUser_m3608849642,
	GameCenterPlatform_LoadAchievementDescriptions_m2456499679,
	GameCenterPlatform_ReportProgress_m546889208,
	GameCenterPlatform_LoadAchievements_m2248650362,
	GameCenterPlatform_ReportScore_m2892009460,
	GameCenterPlatform_LoadScores_m4246413931,
	GameCenterPlatform_LoadScores_m3012647922,
	GameCenterPlatform_LeaderboardCallbackWrapper_m1512666017,
	GameCenterPlatform_GetLoading_m3617241336,
	GameCenterPlatform_VerifyAuthentication_m4030994568,
	GameCenterPlatform_ShowAchievementsUI_m278451572,
	GameCenterPlatform_ShowLeaderboardUI_m184621452,
	GameCenterPlatform_ClearUsers_m694297429,
	GameCenterPlatform_SetUser_m1396993377,
	GameCenterPlatform_SetUserImage_m102480673,
	GameCenterPlatform_TriggerUsersCallbackWrapper_m741337649,
	GameCenterPlatform_LoadUsers_m3057656793,
	GameCenterPlatform_SafeSetUserImage_m4217127318,
	GameCenterPlatform_SafeClearArray_m1090926739,
	GameCenterPlatform_CreateLeaderboard_m2519210070,
	GameCenterPlatform_CreateAchievement_m2189925977,
	GameCenterPlatform_TriggerResetAchievementCallback_m1901797103,
	GameCenterPlatform__cctor_m2214882229,
	U3CUnityEngine_SocialPlatforms_ISocialPlatform_AuthenticateU3Ec__AnonStorey0__ctor_m1526017887,
	U3CUnityEngine_SocialPlatforms_ISocialPlatform_AuthenticateU3Ec__AnonStorey0_U3CU3Em__0_m327311159,
	GcAchievementData_ToAchievement_m1338988672_AdjustorThunk,
	GcAchievementDescriptionData_ToAchievementDescription_m1696951345_AdjustorThunk,
	GcLeaderboard__ctor_m2813903688,
	GcLeaderboard_Finalize_m1739848811,
	GcLeaderboard_Contains_m1630680475,
	GcLeaderboard_SetScores_m3774366612,
	GcLeaderboard_SetLocalScore_m347955094,
	GcLeaderboard_SetMaxRange_m1939957199,
	GcLeaderboard_SetTitle_m2280154755,
	GcLeaderboard_Internal_LoadScores_m30788333,
	GcLeaderboard_Loading_m2486631462,
	GcLeaderboard_Dispose_m4178548762,
	GcScoreData_ToScore_m1700738966_AdjustorThunk,
	GcUserProfileData_ToUserProfile_m1762043327_AdjustorThunk,
	GcUserProfileData_AddToArray_m3145544780_AdjustorThunk,
	Achievement__ctor_m1186584419,
	Achievement__ctor_m4204630465,
	Achievement__ctor_m620345011,
	Achievement_ToString_m448994531,
	Achievement_get_id_m352935039,
	Achievement_set_id_m2592364518,
	Achievement_get_percentCompleted_m3640585090,
	Achievement_set_percentCompleted_m1848274796,
	Achievement_get_completed_m4043821161,
	Achievement_get_hidden_m1021287233,
	Achievement_get_lastReportedDate_m1514471,
	AchievementDescription__ctor_m2820135995,
	AchievementDescription_ToString_m2630143428,
	AchievementDescription_SetImage_m2500569521,
	AchievementDescription_get_id_m1271036810,
	AchievementDescription_set_id_m3879160604,
	AchievementDescription_get_title_m2019393923,
	AchievementDescription_get_achievedDescription_m918716349,
	AchievementDescription_get_unachievedDescription_m2407176132,
	AchievementDescription_get_hidden_m289731872,
	AchievementDescription_get_points_m2481189241,
	Leaderboard__ctor_m641037685,
	Leaderboard_ToString_m3949952499,
	Leaderboard_SetLocalUserScore_m1702011555,
	Leaderboard_SetMaxRange_m470751880,
	Leaderboard_SetScores_m1422084449,
	Leaderboard_SetTitle_m3471503950,
	Leaderboard_GetUserFilter_m2675148982,
	Leaderboard_get_id_m523269624,
	Leaderboard_set_id_m1452185904,
	Leaderboard_get_userScope_m3651500032,
	Leaderboard_set_userScope_m1479044752,
	Leaderboard_get_range_m4014757670,
	Leaderboard_set_range_m3228718273,
	Leaderboard_get_timeScope_m2150671680,
	Leaderboard_set_timeScope_m1836561867,
	LocalUser__ctor_m167732678,
	LocalUser_SetFriends_m3239690636,
	LocalUser_SetAuthenticated_m2694551542,
	LocalUser_SetUnderage_m380634886,
	LocalUser_get_authenticated_m2652473512,
	Score__ctor_m645280141,
	Score__ctor_m932833410,
	Score_ToString_m4151138973,
	Score_get_leaderboardID_m274030957,
	Score_set_leaderboardID_m2942345544,
	Score_get_value_m3908827736,
	Score_set_value_m2016376629,
	UserProfile__ctor_m110286049,
	UserProfile__ctor_m2304939591,
	UserProfile_ToString_m466929597,
	UserProfile_SetUserName_m768170661,
	UserProfile_SetUserID_m977688691,
	UserProfile_SetImage_m2281807389,
	UserProfile_get_userName_m3191800758,
	UserProfile_get_id_m3617419551,
	UserProfile_get_isFriend_m4217471323,
	UserProfile_get_state_m3743219796,
	Range__ctor_m2051987772_AdjustorThunk,
	Tile__ctor_m3471756680,
	TileBase__ctor_m2441256229,
	Analytics_GetUnityAnalyticsHandler_m3943512563,
	Analytics_CustomEvent_m2416379667,
	CustomEventData__ctor_m248325780,
	CustomEventData_InternalCreate_m782104979,
	CustomEventData_InternalDestroy_m998346656,
	CustomEventData_AddString_m2143902135,
	CustomEventData_AddBool_m436037313,
	CustomEventData_AddChar_m2437065113,
	CustomEventData_AddByte_m210020965,
	CustomEventData_AddSByte_m3889282772,
	CustomEventData_AddInt16_m1607163544,
	CustomEventData_AddUInt16_m3424486269,
	CustomEventData_AddInt32_m2136872868,
	CustomEventData_AddUInt32_m911556407,
	CustomEventData_AddInt64_m1450877831,
	CustomEventData_AddUInt64_m33970678,
	CustomEventData_AddDouble_m1838257319,
	CustomEventData_Finalize_m2193402917,
	CustomEventData_Dispose_m3028624316,
	CustomEventData_Add_m1800489861,
	CustomEventData_Add_m3718299101,
	CustomEventData_Add_m818177505,
	CustomEventData_Add_m365150271,
	CustomEventData_Add_m3633493165,
	CustomEventData_Add_m3549807151,
	CustomEventData_Add_m1292879494,
	CustomEventData_Add_m3529462958,
	CustomEventData_Add_m39184950,
	CustomEventData_Add_m3872278440,
	CustomEventData_Add_m380105428,
	CustomEventData_Add_m748231590,
	CustomEventData_Add_m3177631423,
	CustomEventData_Add_m2573195837,
	CustomEventData_Add_m3144047368,
	UnityAnalyticsHandler__ctor_m2295955364,
	UnityAnalyticsHandler_InternalCreate_m3939206337,
	UnityAnalyticsHandler_InternalDestroy_m316722488,
	UnityAnalyticsHandler_Finalize_m1395391108,
	UnityAnalyticsHandler_Dispose_m3249301322,
	UnityAnalyticsHandler_CustomEvent_m2316692438,
	UnityAnalyticsHandler_CustomEvent_m2476810961,
	UnityAnalyticsHandler_SendCustomEventName_m2761615078,
	UnityAnalyticsHandler_SendCustomEvent_m3141620171,
	AnalyticsSessionInfo_CallSessionStateChanged_m2908054006,
	SessionStateChanged__ctor_m4022175722,
	SessionStateChanged_Invoke_m4131999127,
	SessionStateChanged_BeginInvoke_m136008215,
	SessionStateChanged_EndInvoke_m485529971,
	RemoteSettings_CallOnUpdate_m837272010,
	UpdatedEventHandler__ctor_m3030824834,
	UpdatedEventHandler_Invoke_m54617308,
	UpdatedEventHandler_BeginInvoke_m3089859649,
	UpdatedEventHandler_EndInvoke_m1780394930,
	WebRequestUtils_RedirectTo_m1499419735,
	WebRequestUtils__cctor_m1941139123,
	AnalyticsTracker__ctor_m3613341151,
	AnalyticsTracker_get_eventName_m673691953,
	AnalyticsTracker_set_eventName_m2482650980,
	AnalyticsTracker_get_TP_m652212131,
	AnalyticsTracker_set_TP_m2539166404,
	AnalyticsTracker_Awake_m3542971843,
	AnalyticsTracker_Start_m3995864915,
	AnalyticsTracker_OnEnable_m2258709727,
	AnalyticsTracker_OnDisable_m63278792,
	AnalyticsTracker_OnApplicationPause_m1103455594,
	AnalyticsTracker_OnDestroy_m2158658835,
	AnalyticsTracker_TriggerEvent_m2952898466,
	AnalyticsTracker_SendEvent_m2871678427,
	AnalyticsTracker_BuildParameters_m284111758,
	TrackableProperty__ctor_m1315091320,
	TrackableProperty_get_fields_m2052693107,
	TrackableProperty_set_fields_m2675049714,
	TrackableProperty_GetHashCode_m1969014309,
	FieldWithTarget__ctor_m2124372405,
	FieldWithTarget_get_paramName_m2700030636,
	FieldWithTarget_set_paramName_m2103279178,
	FieldWithTarget_get_target_m1282286626,
	FieldWithTarget_set_target_m342185399,
	FieldWithTarget_get_fieldPath_m2089477078,
	FieldWithTarget_set_fieldPath_m33337211,
	FieldWithTarget_get_typeString_m1884618032,
	FieldWithTarget_set_typeString_m2393875225,
	FieldWithTarget_get_doStatic_m2444575716,
	FieldWithTarget_set_doStatic_m2163203065,
	FieldWithTarget_get_staticString_m1927797833,
	FieldWithTarget_set_staticString_m2790139420,
	FieldWithTarget_GetValue_m40081609,
};
