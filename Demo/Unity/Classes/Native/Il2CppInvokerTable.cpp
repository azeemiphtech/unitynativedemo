﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// System.Object[]
struct ObjectU5BU5D_t4290686858;
// System.Exception
struct Exception_t3361176243;
// System.String
struct String_t;
// System.MulticastDelegate
struct MulticastDelegate_t69367374;
// Mono.Globalization.Unicode.Contraction[]
struct ContractionU5BU5D_t398144010;
// Mono.Globalization.Unicode.Contraction
struct Contraction_t3257534955;
// Mono.Globalization.Unicode.Level2Map[]
struct Level2MapU5BU5D_t70716484;
// Mono.Globalization.Unicode.Level2Map
struct Level2Map_t3905868985;
// Mono.Globalization.Unicode.CodePointIndexer
struct CodePointIndexer_t1824690040;
// System.Byte[]
struct ByteU5BU5D_t1014014593;
// System.Reflection.MethodBase
struct MethodBase_t674153939;
// System.Reflection.Module
struct Module_t2594760207;
// System.Runtime.Serialization.ISurrogateSelector
struct ISurrogateSelector_t1684653753;
// System.Runtime.Remoting.Messaging.Header[]
struct HeaderU5BU5D_t1468929288;
// System.Runtime.Remoting.Messaging.Header
struct Header_t2445250629;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t3667883434;
// System.Text.StringBuilder
struct StringBuilder_t2076519010;
// System.Text.EncoderFallbackBuffer
struct EncoderFallbackBuffer_t523190862;
// System.Char[]
struct CharU5BU5D_t2864810077;
// System.Text.DecoderFallbackBuffer
struct DecoderFallbackBuffer_t1939825529;
// System.Int64[]
struct Int64U5BU5D_t3591546736;
// System.String[]
struct StringU5BU5D_t522578821;
// System.Collections.Specialized.ListDictionary/DictionaryNode
struct DictionaryNode_t4253121028;
// System.Net.IPAddress
struct IPAddress_t1846197371;
// System.Net.IPv6Address
struct IPv6Address_t2439851581;
// System.UriFormatException
struct UriFormatException_t2325300979;
// UnityEngine.SocialPlatforms.Impl.UserProfile[]
struct UserProfileU5BU5D_t4189818181;
// UnityEngine.SocialPlatforms.Impl.UserProfile
struct UserProfile_t3211800844;
// System.Int32[]
struct Int32U5BU5D_t2856113350;
// System.Reflection.CustomAttributeNamedArgument[]
struct CustomAttributeNamedArgumentU5BU5D_t3127017202;
// System.Reflection.CustomAttributeTypedArgument[]
struct CustomAttributeTypedArgumentU5BU5D_t2668173852;
// System.Collections.Hashtable
struct Hashtable_t3529848683;
// System.Collections.ArrayList
struct ArrayList_t655337682;
// System.Runtime.Serialization.IFormatterConverter
struct IFormatterConverter_t1975945934;
// System.UInt16[]
struct UInt16U5BU5D_t30367060;
// System.IntPtr[]
struct IntPtrU5BU5D_t2893013242;
// System.Collections.IDictionary
struct IDictionary_t1911576662;
// Mono.Globalization.Unicode.CodePointIndexer/TableRange[]
struct TableRangeU5BU5D_t1413233363;
// UnityEngine.GameObject
struct GameObject_t3732643618;
// UnityEngine.Camera
struct Camera_t101201881;
// System.Security.Cryptography.RandomNumberGenerator
struct RandomNumberGenerator_t4107616314;
// System.Type
struct Type_t;
// System.MonoEnumInfo/SByteComparer
struct SByteComparer_t1095340729;
// System.MonoEnumInfo/ShortComparer
struct ShortComparer_t4228648934;
// System.MonoEnumInfo/IntComparer
struct IntComparer_t8372293;
// System.MonoEnumInfo/LongComparer
struct LongComparer_t1854865843;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t449703515;
// System.Collections.Generic.Queue`1<UnityEngine.UnitySynchronizationContext/WorkRequest>
struct Queue_1_t1920222844;
// UnityEngine.Texture2D
struct Texture2D_t359445654;
// System.Boolean[]
struct BooleanU5BU5D_t129461842;
// System.Reflection.MemberInfo
struct MemberInfo_t;
// System.Collections.Generic.Stack`1<System.Object>
struct Stack_1_t3886787695;
// System.Collections.Generic.Queue`1<System.Object>
struct Queue_1_t2668810036;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t1174585319;
// System.Threading.SendOrPostCallback
struct SendOrPostCallback_t2649904830;
// System.Threading.ManualResetEvent
struct ManualResetEvent_t3770519771;
// System.Void
struct Void_t1078098495;
// System.Byte
struct Byte_t880488320;
// System.Double
struct Double_t1454265465;
// System.UInt16
struct UInt16_t11709673;
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t1684908833;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t2624639528;
// System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>
struct List_1_t546271037;
// System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>
struct Dictionary_2_t448564729;
// System.Collections.Generic.Dictionary`2<System.Object,System.Int32>
struct Dictionary_2_t960027029;
// System.Reflection.MethodInfo[]
struct MethodInfoU5BU5D_t2077906545;
// System.Reflection.TypeFilter
struct TypeFilter_t139072584;
// System.Reflection.Assembly
struct Assembly_t2665685420;
// System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>
struct List_1_t2599265951;
// System.IO.Stream
struct Stream_t2349536632;
// UnityEngine.Playables.PlayableBinding[]
struct PlayableBindingU5BU5D_t207642577;
// UnityEngine.Object
struct Object_t2461733472;
// System.Collections.Generic.Dictionary`2<System.IntPtr,System.Object>
struct Dictionary_2_t287235217;

struct Object_t2461733472_marshaled_com;

struct ObjectU5BU5D_t4290686858;
struct ContractionU5BU5D_t398144010;
struct Level2MapU5BU5D_t70716484;
struct ByteU5BU5D_t1014014593;
struct HeaderU5BU5D_t1468929288;
struct CharU5BU5D_t2864810077;
struct Int64U5BU5D_t3591546736;
struct StringU5BU5D_t522578821;
struct UserProfileU5BU5D_t4189818181;
struct Int32U5BU5D_t2856113350;
struct CustomAttributeNamedArgumentU5BU5D_t3127017202;
struct CustomAttributeTypedArgumentU5BU5D_t2668173852;


#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef HEADER_T2445250629_H
#define HEADER_T2445250629_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Remoting.Messaging.Header
struct  Header_t2445250629  : public RuntimeObject
{
public:
	// System.String System.Runtime.Remoting.Messaging.Header::HeaderNamespace
	String_t* ___HeaderNamespace_0;
	// System.Boolean System.Runtime.Remoting.Messaging.Header::MustUnderstand
	bool ___MustUnderstand_1;
	// System.String System.Runtime.Remoting.Messaging.Header::Name
	String_t* ___Name_2;
	// System.Object System.Runtime.Remoting.Messaging.Header::Value
	RuntimeObject * ___Value_3;

public:
	inline static int32_t get_offset_of_HeaderNamespace_0() { return static_cast<int32_t>(offsetof(Header_t2445250629, ___HeaderNamespace_0)); }
	inline String_t* get_HeaderNamespace_0() const { return ___HeaderNamespace_0; }
	inline String_t** get_address_of_HeaderNamespace_0() { return &___HeaderNamespace_0; }
	inline void set_HeaderNamespace_0(String_t* value)
	{
		___HeaderNamespace_0 = value;
		Il2CppCodeGenWriteBarrier((&___HeaderNamespace_0), value);
	}

	inline static int32_t get_offset_of_MustUnderstand_1() { return static_cast<int32_t>(offsetof(Header_t2445250629, ___MustUnderstand_1)); }
	inline bool get_MustUnderstand_1() const { return ___MustUnderstand_1; }
	inline bool* get_address_of_MustUnderstand_1() { return &___MustUnderstand_1; }
	inline void set_MustUnderstand_1(bool value)
	{
		___MustUnderstand_1 = value;
	}

	inline static int32_t get_offset_of_Name_2() { return static_cast<int32_t>(offsetof(Header_t2445250629, ___Name_2)); }
	inline String_t* get_Name_2() const { return ___Name_2; }
	inline String_t** get_address_of_Name_2() { return &___Name_2; }
	inline void set_Name_2(String_t* value)
	{
		___Name_2 = value;
		Il2CppCodeGenWriteBarrier((&___Name_2), value);
	}

	inline static int32_t get_offset_of_Value_3() { return static_cast<int32_t>(offsetof(Header_t2445250629, ___Value_3)); }
	inline RuntimeObject * get_Value_3() const { return ___Value_3; }
	inline RuntimeObject ** get_address_of_Value_3() { return &___Value_3; }
	inline void set_Value_3(RuntimeObject * value)
	{
		___Value_3 = value;
		Il2CppCodeGenWriteBarrier((&___Value_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEADER_T2445250629_H
#ifndef SERIALIZATIONINFO_T3667883434_H
#define SERIALIZATIONINFO_T3667883434_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.SerializationInfo
struct  SerializationInfo_t3667883434  : public RuntimeObject
{
public:
	// System.Collections.Hashtable System.Runtime.Serialization.SerializationInfo::serialized
	Hashtable_t3529848683 * ___serialized_0;
	// System.Collections.ArrayList System.Runtime.Serialization.SerializationInfo::values
	ArrayList_t655337682 * ___values_1;
	// System.String System.Runtime.Serialization.SerializationInfo::assemblyName
	String_t* ___assemblyName_2;
	// System.String System.Runtime.Serialization.SerializationInfo::fullTypeName
	String_t* ___fullTypeName_3;
	// System.Runtime.Serialization.IFormatterConverter System.Runtime.Serialization.SerializationInfo::converter
	RuntimeObject* ___converter_4;

public:
	inline static int32_t get_offset_of_serialized_0() { return static_cast<int32_t>(offsetof(SerializationInfo_t3667883434, ___serialized_0)); }
	inline Hashtable_t3529848683 * get_serialized_0() const { return ___serialized_0; }
	inline Hashtable_t3529848683 ** get_address_of_serialized_0() { return &___serialized_0; }
	inline void set_serialized_0(Hashtable_t3529848683 * value)
	{
		___serialized_0 = value;
		Il2CppCodeGenWriteBarrier((&___serialized_0), value);
	}

	inline static int32_t get_offset_of_values_1() { return static_cast<int32_t>(offsetof(SerializationInfo_t3667883434, ___values_1)); }
	inline ArrayList_t655337682 * get_values_1() const { return ___values_1; }
	inline ArrayList_t655337682 ** get_address_of_values_1() { return &___values_1; }
	inline void set_values_1(ArrayList_t655337682 * value)
	{
		___values_1 = value;
		Il2CppCodeGenWriteBarrier((&___values_1), value);
	}

	inline static int32_t get_offset_of_assemblyName_2() { return static_cast<int32_t>(offsetof(SerializationInfo_t3667883434, ___assemblyName_2)); }
	inline String_t* get_assemblyName_2() const { return ___assemblyName_2; }
	inline String_t** get_address_of_assemblyName_2() { return &___assemblyName_2; }
	inline void set_assemblyName_2(String_t* value)
	{
		___assemblyName_2 = value;
		Il2CppCodeGenWriteBarrier((&___assemblyName_2), value);
	}

	inline static int32_t get_offset_of_fullTypeName_3() { return static_cast<int32_t>(offsetof(SerializationInfo_t3667883434, ___fullTypeName_3)); }
	inline String_t* get_fullTypeName_3() const { return ___fullTypeName_3; }
	inline String_t** get_address_of_fullTypeName_3() { return &___fullTypeName_3; }
	inline void set_fullTypeName_3(String_t* value)
	{
		___fullTypeName_3 = value;
		Il2CppCodeGenWriteBarrier((&___fullTypeName_3), value);
	}

	inline static int32_t get_offset_of_converter_4() { return static_cast<int32_t>(offsetof(SerializationInfo_t3667883434, ___converter_4)); }
	inline RuntimeObject* get_converter_4() const { return ___converter_4; }
	inline RuntimeObject** get_address_of_converter_4() { return &___converter_4; }
	inline void set_converter_4(RuntimeObject* value)
	{
		___converter_4 = value;
		Il2CppCodeGenWriteBarrier((&___converter_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZATIONINFO_T3667883434_H
#ifndef STRINGBUILDER_T2076519010_H
#define STRINGBUILDER_T2076519010_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.StringBuilder
struct  StringBuilder_t2076519010  : public RuntimeObject
{
public:
	// System.Int32 System.Text.StringBuilder::_length
	int32_t ____length_1;
	// System.String System.Text.StringBuilder::_str
	String_t* ____str_2;
	// System.String System.Text.StringBuilder::_cached_str
	String_t* ____cached_str_3;
	// System.Int32 System.Text.StringBuilder::_maxCapacity
	int32_t ____maxCapacity_4;

public:
	inline static int32_t get_offset_of__length_1() { return static_cast<int32_t>(offsetof(StringBuilder_t2076519010, ____length_1)); }
	inline int32_t get__length_1() const { return ____length_1; }
	inline int32_t* get_address_of__length_1() { return &____length_1; }
	inline void set__length_1(int32_t value)
	{
		____length_1 = value;
	}

	inline static int32_t get_offset_of__str_2() { return static_cast<int32_t>(offsetof(StringBuilder_t2076519010, ____str_2)); }
	inline String_t* get__str_2() const { return ____str_2; }
	inline String_t** get_address_of__str_2() { return &____str_2; }
	inline void set__str_2(String_t* value)
	{
		____str_2 = value;
		Il2CppCodeGenWriteBarrier((&____str_2), value);
	}

	inline static int32_t get_offset_of__cached_str_3() { return static_cast<int32_t>(offsetof(StringBuilder_t2076519010, ____cached_str_3)); }
	inline String_t* get__cached_str_3() const { return ____cached_str_3; }
	inline String_t** get_address_of__cached_str_3() { return &____cached_str_3; }
	inline void set__cached_str_3(String_t* value)
	{
		____cached_str_3 = value;
		Il2CppCodeGenWriteBarrier((&____cached_str_3), value);
	}

	inline static int32_t get_offset_of__maxCapacity_4() { return static_cast<int32_t>(offsetof(StringBuilder_t2076519010, ____maxCapacity_4)); }
	inline int32_t get__maxCapacity_4() const { return ____maxCapacity_4; }
	inline int32_t* get_address_of__maxCapacity_4() { return &____maxCapacity_4; }
	inline void set__maxCapacity_4(int32_t value)
	{
		____maxCapacity_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGBUILDER_T2076519010_H
#ifndef ENCODERFALLBACKBUFFER_T523190862_H
#define ENCODERFALLBACKBUFFER_T523190862_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.EncoderFallbackBuffer
struct  EncoderFallbackBuffer_t523190862  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCODERFALLBACKBUFFER_T523190862_H
#ifndef DECODERFALLBACKBUFFER_T1939825529_H
#define DECODERFALLBACKBUFFER_T1939825529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.DecoderFallbackBuffer
struct  DecoderFallbackBuffer_t1939825529  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DECODERFALLBACKBUFFER_T1939825529_H
#ifndef DICTIONARYNODE_T4253121028_H
#define DICTIONARYNODE_T4253121028_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Specialized.ListDictionary/DictionaryNode
struct  DictionaryNode_t4253121028  : public RuntimeObject
{
public:
	// System.Object System.Collections.Specialized.ListDictionary/DictionaryNode::key
	RuntimeObject * ___key_0;
	// System.Object System.Collections.Specialized.ListDictionary/DictionaryNode::value
	RuntimeObject * ___value_1;
	// System.Collections.Specialized.ListDictionary/DictionaryNode System.Collections.Specialized.ListDictionary/DictionaryNode::next
	DictionaryNode_t4253121028 * ___next_2;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(DictionaryNode_t4253121028, ___key_0)); }
	inline RuntimeObject * get_key_0() const { return ___key_0; }
	inline RuntimeObject ** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(RuntimeObject * value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(DictionaryNode_t4253121028, ___value_1)); }
	inline RuntimeObject * get_value_1() const { return ___value_1; }
	inline RuntimeObject ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(RuntimeObject * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}

	inline static int32_t get_offset_of_next_2() { return static_cast<int32_t>(offsetof(DictionaryNode_t4253121028, ___next_2)); }
	inline DictionaryNode_t4253121028 * get_next_2() const { return ___next_2; }
	inline DictionaryNode_t4253121028 ** get_address_of_next_2() { return &___next_2; }
	inline void set_next_2(DictionaryNode_t4253121028 * value)
	{
		___next_2 = value;
		Il2CppCodeGenWriteBarrier((&___next_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARYNODE_T4253121028_H
#ifndef IPV6ADDRESS_T2439851581_H
#define IPV6ADDRESS_T2439851581_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.IPv6Address
struct  IPv6Address_t2439851581  : public RuntimeObject
{
public:
	// System.UInt16[] System.Net.IPv6Address::address
	UInt16U5BU5D_t30367060* ___address_0;
	// System.Int32 System.Net.IPv6Address::prefixLength
	int32_t ___prefixLength_1;
	// System.Int64 System.Net.IPv6Address::scopeId
	int64_t ___scopeId_2;

public:
	inline static int32_t get_offset_of_address_0() { return static_cast<int32_t>(offsetof(IPv6Address_t2439851581, ___address_0)); }
	inline UInt16U5BU5D_t30367060* get_address_0() const { return ___address_0; }
	inline UInt16U5BU5D_t30367060** get_address_of_address_0() { return &___address_0; }
	inline void set_address_0(UInt16U5BU5D_t30367060* value)
	{
		___address_0 = value;
		Il2CppCodeGenWriteBarrier((&___address_0), value);
	}

	inline static int32_t get_offset_of_prefixLength_1() { return static_cast<int32_t>(offsetof(IPv6Address_t2439851581, ___prefixLength_1)); }
	inline int32_t get_prefixLength_1() const { return ___prefixLength_1; }
	inline int32_t* get_address_of_prefixLength_1() { return &___prefixLength_1; }
	inline void set_prefixLength_1(int32_t value)
	{
		___prefixLength_1 = value;
	}

	inline static int32_t get_offset_of_scopeId_2() { return static_cast<int32_t>(offsetof(IPv6Address_t2439851581, ___scopeId_2)); }
	inline int64_t get_scopeId_2() const { return ___scopeId_2; }
	inline int64_t* get_address_of_scopeId_2() { return &___scopeId_2; }
	inline void set_scopeId_2(int64_t value)
	{
		___scopeId_2 = value;
	}
};

struct IPv6Address_t2439851581_StaticFields
{
public:
	// System.Net.IPv6Address System.Net.IPv6Address::Loopback
	IPv6Address_t2439851581 * ___Loopback_3;
	// System.Net.IPv6Address System.Net.IPv6Address::Unspecified
	IPv6Address_t2439851581 * ___Unspecified_4;

public:
	inline static int32_t get_offset_of_Loopback_3() { return static_cast<int32_t>(offsetof(IPv6Address_t2439851581_StaticFields, ___Loopback_3)); }
	inline IPv6Address_t2439851581 * get_Loopback_3() const { return ___Loopback_3; }
	inline IPv6Address_t2439851581 ** get_address_of_Loopback_3() { return &___Loopback_3; }
	inline void set_Loopback_3(IPv6Address_t2439851581 * value)
	{
		___Loopback_3 = value;
		Il2CppCodeGenWriteBarrier((&___Loopback_3), value);
	}

	inline static int32_t get_offset_of_Unspecified_4() { return static_cast<int32_t>(offsetof(IPv6Address_t2439851581_StaticFields, ___Unspecified_4)); }
	inline IPv6Address_t2439851581 * get_Unspecified_4() const { return ___Unspecified_4; }
	inline IPv6Address_t2439851581 ** get_address_of_Unspecified_4() { return &___Unspecified_4; }
	inline void set_Unspecified_4(IPv6Address_t2439851581 * value)
	{
		___Unspecified_4 = value;
		Il2CppCodeGenWriteBarrier((&___Unspecified_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IPV6ADDRESS_T2439851581_H
#ifndef LEVEL2MAP_T3905868985_H
#define LEVEL2MAP_T3905868985_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Globalization.Unicode.Level2Map
struct  Level2Map_t3905868985  : public RuntimeObject
{
public:
	// System.Byte Mono.Globalization.Unicode.Level2Map::Source
	uint8_t ___Source_0;
	// System.Byte Mono.Globalization.Unicode.Level2Map::Replace
	uint8_t ___Replace_1;

public:
	inline static int32_t get_offset_of_Source_0() { return static_cast<int32_t>(offsetof(Level2Map_t3905868985, ___Source_0)); }
	inline uint8_t get_Source_0() const { return ___Source_0; }
	inline uint8_t* get_address_of_Source_0() { return &___Source_0; }
	inline void set_Source_0(uint8_t value)
	{
		___Source_0 = value;
	}

	inline static int32_t get_offset_of_Replace_1() { return static_cast<int32_t>(offsetof(Level2Map_t3905868985, ___Replace_1)); }
	inline uint8_t get_Replace_1() const { return ___Replace_1; }
	inline uint8_t* get_address_of_Replace_1() { return &___Replace_1; }
	inline void set_Replace_1(uint8_t value)
	{
		___Replace_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEVEL2MAP_T3905868985_H
#ifndef CONTRACTION_T3257534955_H
#define CONTRACTION_T3257534955_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Globalization.Unicode.Contraction
struct  Contraction_t3257534955  : public RuntimeObject
{
public:
	// System.Char[] Mono.Globalization.Unicode.Contraction::Source
	CharU5BU5D_t2864810077* ___Source_0;
	// System.String Mono.Globalization.Unicode.Contraction::Replacement
	String_t* ___Replacement_1;
	// System.Byte[] Mono.Globalization.Unicode.Contraction::SortKey
	ByteU5BU5D_t1014014593* ___SortKey_2;

public:
	inline static int32_t get_offset_of_Source_0() { return static_cast<int32_t>(offsetof(Contraction_t3257534955, ___Source_0)); }
	inline CharU5BU5D_t2864810077* get_Source_0() const { return ___Source_0; }
	inline CharU5BU5D_t2864810077** get_address_of_Source_0() { return &___Source_0; }
	inline void set_Source_0(CharU5BU5D_t2864810077* value)
	{
		___Source_0 = value;
		Il2CppCodeGenWriteBarrier((&___Source_0), value);
	}

	inline static int32_t get_offset_of_Replacement_1() { return static_cast<int32_t>(offsetof(Contraction_t3257534955, ___Replacement_1)); }
	inline String_t* get_Replacement_1() const { return ___Replacement_1; }
	inline String_t** get_address_of_Replacement_1() { return &___Replacement_1; }
	inline void set_Replacement_1(String_t* value)
	{
		___Replacement_1 = value;
		Il2CppCodeGenWriteBarrier((&___Replacement_1), value);
	}

	inline static int32_t get_offset_of_SortKey_2() { return static_cast<int32_t>(offsetof(Contraction_t3257534955, ___SortKey_2)); }
	inline ByteU5BU5D_t1014014593* get_SortKey_2() const { return ___SortKey_2; }
	inline ByteU5BU5D_t1014014593** get_address_of_SortKey_2() { return &___SortKey_2; }
	inline void set_SortKey_2(ByteU5BU5D_t1014014593* value)
	{
		___SortKey_2 = value;
		Il2CppCodeGenWriteBarrier((&___SortKey_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTRACTION_T3257534955_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::length
	int32_t ___length_0;
	// System.Char System.String::start_char
	Il2CppChar ___start_char_1;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(String_t, ___length_0)); }
	inline int32_t get_length_0() const { return ___length_0; }
	inline int32_t* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(int32_t value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_start_char_1() { return static_cast<int32_t>(offsetof(String_t, ___start_char_1)); }
	inline Il2CppChar get_start_char_1() const { return ___start_char_1; }
	inline Il2CppChar* get_address_of_start_char_1() { return &___start_char_1; }
	inline void set_start_char_1(Il2CppChar value)
	{
		___start_char_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_2;
	// System.Char[] System.String::WhiteChars
	CharU5BU5D_t2864810077* ___WhiteChars_3;

public:
	inline static int32_t get_offset_of_Empty_2() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_2)); }
	inline String_t* get_Empty_2() const { return ___Empty_2; }
	inline String_t** get_address_of_Empty_2() { return &___Empty_2; }
	inline void set_Empty_2(String_t* value)
	{
		___Empty_2 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_2), value);
	}

	inline static int32_t get_offset_of_WhiteChars_3() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___WhiteChars_3)); }
	inline CharU5BU5D_t2864810077* get_WhiteChars_3() const { return ___WhiteChars_3; }
	inline CharU5BU5D_t2864810077** get_address_of_WhiteChars_3() { return &___WhiteChars_3; }
	inline void set_WhiteChars_3(CharU5BU5D_t2864810077* value)
	{
		___WhiteChars_3 = value;
		Il2CppCodeGenWriteBarrier((&___WhiteChars_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef EXCEPTION_T3361176243_H
#define EXCEPTION_T3361176243_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t3361176243  : public RuntimeObject
{
public:
	// System.IntPtr[] System.Exception::trace_ips
	IntPtrU5BU5D_t2893013242* ___trace_ips_0;
	// System.Exception System.Exception::inner_exception
	Exception_t3361176243 * ___inner_exception_1;
	// System.String System.Exception::message
	String_t* ___message_2;
	// System.String System.Exception::help_link
	String_t* ___help_link_3;
	// System.String System.Exception::class_name
	String_t* ___class_name_4;
	// System.String System.Exception::stack_trace
	String_t* ___stack_trace_5;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_6;
	// System.Int32 System.Exception::remote_stack_index
	int32_t ___remote_stack_index_7;
	// System.Int32 System.Exception::hresult
	int32_t ___hresult_8;
	// System.String System.Exception::source
	String_t* ___source_9;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_10;

public:
	inline static int32_t get_offset_of_trace_ips_0() { return static_cast<int32_t>(offsetof(Exception_t3361176243, ___trace_ips_0)); }
	inline IntPtrU5BU5D_t2893013242* get_trace_ips_0() const { return ___trace_ips_0; }
	inline IntPtrU5BU5D_t2893013242** get_address_of_trace_ips_0() { return &___trace_ips_0; }
	inline void set_trace_ips_0(IntPtrU5BU5D_t2893013242* value)
	{
		___trace_ips_0 = value;
		Il2CppCodeGenWriteBarrier((&___trace_ips_0), value);
	}

	inline static int32_t get_offset_of_inner_exception_1() { return static_cast<int32_t>(offsetof(Exception_t3361176243, ___inner_exception_1)); }
	inline Exception_t3361176243 * get_inner_exception_1() const { return ___inner_exception_1; }
	inline Exception_t3361176243 ** get_address_of_inner_exception_1() { return &___inner_exception_1; }
	inline void set_inner_exception_1(Exception_t3361176243 * value)
	{
		___inner_exception_1 = value;
		Il2CppCodeGenWriteBarrier((&___inner_exception_1), value);
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(Exception_t3361176243, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier((&___message_2), value);
	}

	inline static int32_t get_offset_of_help_link_3() { return static_cast<int32_t>(offsetof(Exception_t3361176243, ___help_link_3)); }
	inline String_t* get_help_link_3() const { return ___help_link_3; }
	inline String_t** get_address_of_help_link_3() { return &___help_link_3; }
	inline void set_help_link_3(String_t* value)
	{
		___help_link_3 = value;
		Il2CppCodeGenWriteBarrier((&___help_link_3), value);
	}

	inline static int32_t get_offset_of_class_name_4() { return static_cast<int32_t>(offsetof(Exception_t3361176243, ___class_name_4)); }
	inline String_t* get_class_name_4() const { return ___class_name_4; }
	inline String_t** get_address_of_class_name_4() { return &___class_name_4; }
	inline void set_class_name_4(String_t* value)
	{
		___class_name_4 = value;
		Il2CppCodeGenWriteBarrier((&___class_name_4), value);
	}

	inline static int32_t get_offset_of_stack_trace_5() { return static_cast<int32_t>(offsetof(Exception_t3361176243, ___stack_trace_5)); }
	inline String_t* get_stack_trace_5() const { return ___stack_trace_5; }
	inline String_t** get_address_of_stack_trace_5() { return &___stack_trace_5; }
	inline void set_stack_trace_5(String_t* value)
	{
		___stack_trace_5 = value;
		Il2CppCodeGenWriteBarrier((&___stack_trace_5), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_6() { return static_cast<int32_t>(offsetof(Exception_t3361176243, ____remoteStackTraceString_6)); }
	inline String_t* get__remoteStackTraceString_6() const { return ____remoteStackTraceString_6; }
	inline String_t** get_address_of__remoteStackTraceString_6() { return &____remoteStackTraceString_6; }
	inline void set__remoteStackTraceString_6(String_t* value)
	{
		____remoteStackTraceString_6 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_6), value);
	}

	inline static int32_t get_offset_of_remote_stack_index_7() { return static_cast<int32_t>(offsetof(Exception_t3361176243, ___remote_stack_index_7)); }
	inline int32_t get_remote_stack_index_7() const { return ___remote_stack_index_7; }
	inline int32_t* get_address_of_remote_stack_index_7() { return &___remote_stack_index_7; }
	inline void set_remote_stack_index_7(int32_t value)
	{
		___remote_stack_index_7 = value;
	}

	inline static int32_t get_offset_of_hresult_8() { return static_cast<int32_t>(offsetof(Exception_t3361176243, ___hresult_8)); }
	inline int32_t get_hresult_8() const { return ___hresult_8; }
	inline int32_t* get_address_of_hresult_8() { return &___hresult_8; }
	inline void set_hresult_8(int32_t value)
	{
		___hresult_8 = value;
	}

	inline static int32_t get_offset_of_source_9() { return static_cast<int32_t>(offsetof(Exception_t3361176243, ___source_9)); }
	inline String_t* get_source_9() const { return ___source_9; }
	inline String_t** get_address_of_source_9() { return &___source_9; }
	inline void set_source_9(String_t* value)
	{
		___source_9 = value;
		Il2CppCodeGenWriteBarrier((&___source_9), value);
	}

	inline static int32_t get_offset_of__data_10() { return static_cast<int32_t>(offsetof(Exception_t3361176243, ____data_10)); }
	inline RuntimeObject* get__data_10() const { return ____data_10; }
	inline RuntimeObject** get_address_of__data_10() { return &____data_10; }
	inline void set__data_10(RuntimeObject* value)
	{
		____data_10 = value;
		Il2CppCodeGenWriteBarrier((&____data_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTION_T3361176243_H
#ifndef CODEPOINTINDEXER_T1824690040_H
#define CODEPOINTINDEXER_T1824690040_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Globalization.Unicode.CodePointIndexer
struct  CodePointIndexer_t1824690040  : public RuntimeObject
{
public:
	// Mono.Globalization.Unicode.CodePointIndexer/TableRange[] Mono.Globalization.Unicode.CodePointIndexer::ranges
	TableRangeU5BU5D_t1413233363* ___ranges_0;
	// System.Int32 Mono.Globalization.Unicode.CodePointIndexer::TotalCount
	int32_t ___TotalCount_1;
	// System.Int32 Mono.Globalization.Unicode.CodePointIndexer::defaultIndex
	int32_t ___defaultIndex_2;
	// System.Int32 Mono.Globalization.Unicode.CodePointIndexer::defaultCP
	int32_t ___defaultCP_3;

public:
	inline static int32_t get_offset_of_ranges_0() { return static_cast<int32_t>(offsetof(CodePointIndexer_t1824690040, ___ranges_0)); }
	inline TableRangeU5BU5D_t1413233363* get_ranges_0() const { return ___ranges_0; }
	inline TableRangeU5BU5D_t1413233363** get_address_of_ranges_0() { return &___ranges_0; }
	inline void set_ranges_0(TableRangeU5BU5D_t1413233363* value)
	{
		___ranges_0 = value;
		Il2CppCodeGenWriteBarrier((&___ranges_0), value);
	}

	inline static int32_t get_offset_of_TotalCount_1() { return static_cast<int32_t>(offsetof(CodePointIndexer_t1824690040, ___TotalCount_1)); }
	inline int32_t get_TotalCount_1() const { return ___TotalCount_1; }
	inline int32_t* get_address_of_TotalCount_1() { return &___TotalCount_1; }
	inline void set_TotalCount_1(int32_t value)
	{
		___TotalCount_1 = value;
	}

	inline static int32_t get_offset_of_defaultIndex_2() { return static_cast<int32_t>(offsetof(CodePointIndexer_t1824690040, ___defaultIndex_2)); }
	inline int32_t get_defaultIndex_2() const { return ___defaultIndex_2; }
	inline int32_t* get_address_of_defaultIndex_2() { return &___defaultIndex_2; }
	inline void set_defaultIndex_2(int32_t value)
	{
		___defaultIndex_2 = value;
	}

	inline static int32_t get_offset_of_defaultCP_3() { return static_cast<int32_t>(offsetof(CodePointIndexer_t1824690040, ___defaultCP_3)); }
	inline int32_t get_defaultCP_3() const { return ___defaultCP_3; }
	inline int32_t* get_address_of_defaultCP_3() { return &___defaultCP_3; }
	inline void set_defaultCP_3(int32_t value)
	{
		___defaultCP_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CODEPOINTINDEXER_T1824690040_H
#ifndef MEMBERINFO_T_H
#define MEMBERINFO_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MemberInfo
struct  MemberInfo_t  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERINFO_T_H
#ifndef VALUETYPE_T1920584095_H
#define VALUETYPE_T1920584095_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t1920584095  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t1920584095_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t1920584095_marshaled_com
{
};
#endif // VALUETYPE_T1920584095_H
#ifndef LABELFIXUP_T2685311059_H
#define LABELFIXUP_T2685311059_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.Emit.ILGenerator/LabelFixup
struct  LabelFixup_t2685311059 
{
public:
	// System.Int32 System.Reflection.Emit.ILGenerator/LabelFixup::offset
	int32_t ___offset_0;
	// System.Int32 System.Reflection.Emit.ILGenerator/LabelFixup::pos
	int32_t ___pos_1;
	// System.Int32 System.Reflection.Emit.ILGenerator/LabelFixup::label_idx
	int32_t ___label_idx_2;

public:
	inline static int32_t get_offset_of_offset_0() { return static_cast<int32_t>(offsetof(LabelFixup_t2685311059, ___offset_0)); }
	inline int32_t get_offset_0() const { return ___offset_0; }
	inline int32_t* get_address_of_offset_0() { return &___offset_0; }
	inline void set_offset_0(int32_t value)
	{
		___offset_0 = value;
	}

	inline static int32_t get_offset_of_pos_1() { return static_cast<int32_t>(offsetof(LabelFixup_t2685311059, ___pos_1)); }
	inline int32_t get_pos_1() const { return ___pos_1; }
	inline int32_t* get_address_of_pos_1() { return &___pos_1; }
	inline void set_pos_1(int32_t value)
	{
		___pos_1 = value;
	}

	inline static int32_t get_offset_of_label_idx_2() { return static_cast<int32_t>(offsetof(LabelFixup_t2685311059, ___label_idx_2)); }
	inline int32_t get_label_idx_2() const { return ___label_idx_2; }
	inline int32_t* get_address_of_label_idx_2() { return &___label_idx_2; }
	inline void set_label_idx_2(int32_t value)
	{
		___label_idx_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LABELFIXUP_T2685311059_H
#ifndef HITINFO_T184839221_H
#define HITINFO_T184839221_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SendMouseEvents/HitInfo
struct  HitInfo_t184839221 
{
public:
	// UnityEngine.GameObject UnityEngine.SendMouseEvents/HitInfo::target
	GameObject_t3732643618 * ___target_0;
	// UnityEngine.Camera UnityEngine.SendMouseEvents/HitInfo::camera
	Camera_t101201881 * ___camera_1;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(HitInfo_t184839221, ___target_0)); }
	inline GameObject_t3732643618 * get_target_0() const { return ___target_0; }
	inline GameObject_t3732643618 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(GameObject_t3732643618 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}

	inline static int32_t get_offset_of_camera_1() { return static_cast<int32_t>(offsetof(HitInfo_t184839221, ___camera_1)); }
	inline Camera_t101201881 * get_camera_1() const { return ___camera_1; }
	inline Camera_t101201881 ** get_address_of_camera_1() { return &___camera_1; }
	inline void set_camera_1(Camera_t101201881 * value)
	{
		___camera_1 = value;
		Il2CppCodeGenWriteBarrier((&___camera_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.SendMouseEvents/HitInfo
struct HitInfo_t184839221_marshaled_pinvoke
{
	GameObject_t3732643618 * ___target_0;
	Camera_t101201881 * ___camera_1;
};
// Native definition for COM marshalling of UnityEngine.SendMouseEvents/HitInfo
struct HitInfo_t184839221_marshaled_com
{
	GameObject_t3732643618 * ___target_0;
	Camera_t101201881 * ___camera_1;
};
#endif // HITINFO_T184839221_H
#ifndef COLOR_T4024113822_H
#define COLOR_T4024113822_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t4024113822 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t4024113822, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t4024113822, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t4024113822, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t4024113822, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T4024113822_H
#ifndef SCENE_T3023997566_H
#define SCENE_T3023997566_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SceneManagement.Scene
struct  Scene_t3023997566 
{
public:
	// System.Int32 UnityEngine.SceneManagement.Scene::m_Handle
	int32_t ___m_Handle_0;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(Scene_t3023997566, ___m_Handle_0)); }
	inline int32_t get_m_Handle_0() const { return ___m_Handle_0; }
	inline int32_t* get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(int32_t value)
	{
		___m_Handle_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCENE_T3023997566_H
#ifndef MATRIX4X4_T3534180298_H
#define MATRIX4X4_T3534180298_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Matrix4x4
struct  Matrix4x4_t3534180298 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_t3534180298, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_t3534180298, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_t3534180298, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_t3534180298, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_t3534180298, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_t3534180298, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_t3534180298, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_t3534180298, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_t3534180298, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_t3534180298, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_t3534180298, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_t3534180298, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_t3534180298, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_t3534180298, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_t3534180298, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_t3534180298, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_t3534180298_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_t3534180298  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_t3534180298  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_t3534180298_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_t3534180298  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_t3534180298 * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_t3534180298  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_t3534180298_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_t3534180298  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_t3534180298 * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_t3534180298  value)
	{
		___identityMatrix_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIX4X4_T3534180298_H
#ifndef VECTOR4_T4052901136_H
#define VECTOR4_T4052901136_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector4
struct  Vector4_t4052901136 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_0;
	// System.Single UnityEngine.Vector4::y
	float ___y_1;
	// System.Single UnityEngine.Vector4::z
	float ___z_2;
	// System.Single UnityEngine.Vector4::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector4_t4052901136, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector4_t4052901136, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Vector4_t4052901136, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Vector4_t4052901136, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Vector4_t4052901136_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_t4052901136  ___zeroVector_4;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_t4052901136  ___oneVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_t4052901136  ___positiveInfinityVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_t4052901136  ___negativeInfinityVector_7;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector4_t4052901136_StaticFields, ___zeroVector_4)); }
	inline Vector4_t4052901136  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector4_t4052901136 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector4_t4052901136  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector4_t4052901136_StaticFields, ___oneVector_5)); }
	inline Vector4_t4052901136  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector4_t4052901136 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector4_t4052901136  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_6() { return static_cast<int32_t>(offsetof(Vector4_t4052901136_StaticFields, ___positiveInfinityVector_6)); }
	inline Vector4_t4052901136  get_positiveInfinityVector_6() const { return ___positiveInfinityVector_6; }
	inline Vector4_t4052901136 * get_address_of_positiveInfinityVector_6() { return &___positiveInfinityVector_6; }
	inline void set_positiveInfinityVector_6(Vector4_t4052901136  value)
	{
		___positiveInfinityVector_6 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_t4052901136_StaticFields, ___negativeInfinityVector_7)); }
	inline Vector4_t4052901136  get_negativeInfinityVector_7() const { return ___negativeInfinityVector_7; }
	inline Vector4_t4052901136 * get_address_of_negativeInfinityVector_7() { return &___negativeInfinityVector_7; }
	inline void set_negativeInfinityVector_7(Vector4_t4052901136  value)
	{
		___negativeInfinityVector_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4_T4052901136_H
#ifndef CULLINGGROUPEVENT_T157813866_H
#define CULLINGGROUPEVENT_T157813866_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CullingGroupEvent
struct  CullingGroupEvent_t157813866 
{
public:
	// System.Int32 UnityEngine.CullingGroupEvent::m_Index
	int32_t ___m_Index_0;
	// System.Byte UnityEngine.CullingGroupEvent::m_PrevState
	uint8_t ___m_PrevState_1;
	// System.Byte UnityEngine.CullingGroupEvent::m_ThisState
	uint8_t ___m_ThisState_2;

public:
	inline static int32_t get_offset_of_m_Index_0() { return static_cast<int32_t>(offsetof(CullingGroupEvent_t157813866, ___m_Index_0)); }
	inline int32_t get_m_Index_0() const { return ___m_Index_0; }
	inline int32_t* get_address_of_m_Index_0() { return &___m_Index_0; }
	inline void set_m_Index_0(int32_t value)
	{
		___m_Index_0 = value;
	}

	inline static int32_t get_offset_of_m_PrevState_1() { return static_cast<int32_t>(offsetof(CullingGroupEvent_t157813866, ___m_PrevState_1)); }
	inline uint8_t get_m_PrevState_1() const { return ___m_PrevState_1; }
	inline uint8_t* get_address_of_m_PrevState_1() { return &___m_PrevState_1; }
	inline void set_m_PrevState_1(uint8_t value)
	{
		___m_PrevState_1 = value;
	}

	inline static int32_t get_offset_of_m_ThisState_2() { return static_cast<int32_t>(offsetof(CullingGroupEvent_t157813866, ___m_ThisState_2)); }
	inline uint8_t get_m_ThisState_2() const { return ___m_ThisState_2; }
	inline uint8_t* get_address_of_m_ThisState_2() { return &___m_ThisState_2; }
	inline void set_m_ThisState_2(uint8_t value)
	{
		___m_ThisState_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CULLINGGROUPEVENT_T157813866_H
#ifndef VECTOR3_T3239555143_H
#define VECTOR3_T3239555143_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3239555143 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_0;
	// System.Single UnityEngine.Vector3::y
	float ___y_1;
	// System.Single UnityEngine.Vector3::z
	float ___z_2;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector3_t3239555143, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector3_t3239555143, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Vector3_t3239555143, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}
};

struct Vector3_t3239555143_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3239555143  ___zeroVector_3;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3239555143  ___oneVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3239555143  ___upVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3239555143  ___downVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3239555143  ___leftVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3239555143  ___rightVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3239555143  ___forwardVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3239555143  ___backVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3239555143  ___positiveInfinityVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3239555143  ___negativeInfinityVector_12;

public:
	inline static int32_t get_offset_of_zeroVector_3() { return static_cast<int32_t>(offsetof(Vector3_t3239555143_StaticFields, ___zeroVector_3)); }
	inline Vector3_t3239555143  get_zeroVector_3() const { return ___zeroVector_3; }
	inline Vector3_t3239555143 * get_address_of_zeroVector_3() { return &___zeroVector_3; }
	inline void set_zeroVector_3(Vector3_t3239555143  value)
	{
		___zeroVector_3 = value;
	}

	inline static int32_t get_offset_of_oneVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3239555143_StaticFields, ___oneVector_4)); }
	inline Vector3_t3239555143  get_oneVector_4() const { return ___oneVector_4; }
	inline Vector3_t3239555143 * get_address_of_oneVector_4() { return &___oneVector_4; }
	inline void set_oneVector_4(Vector3_t3239555143  value)
	{
		___oneVector_4 = value;
	}

	inline static int32_t get_offset_of_upVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3239555143_StaticFields, ___upVector_5)); }
	inline Vector3_t3239555143  get_upVector_5() const { return ___upVector_5; }
	inline Vector3_t3239555143 * get_address_of_upVector_5() { return &___upVector_5; }
	inline void set_upVector_5(Vector3_t3239555143  value)
	{
		___upVector_5 = value;
	}

	inline static int32_t get_offset_of_downVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3239555143_StaticFields, ___downVector_6)); }
	inline Vector3_t3239555143  get_downVector_6() const { return ___downVector_6; }
	inline Vector3_t3239555143 * get_address_of_downVector_6() { return &___downVector_6; }
	inline void set_downVector_6(Vector3_t3239555143  value)
	{
		___downVector_6 = value;
	}

	inline static int32_t get_offset_of_leftVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3239555143_StaticFields, ___leftVector_7)); }
	inline Vector3_t3239555143  get_leftVector_7() const { return ___leftVector_7; }
	inline Vector3_t3239555143 * get_address_of_leftVector_7() { return &___leftVector_7; }
	inline void set_leftVector_7(Vector3_t3239555143  value)
	{
		___leftVector_7 = value;
	}

	inline static int32_t get_offset_of_rightVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3239555143_StaticFields, ___rightVector_8)); }
	inline Vector3_t3239555143  get_rightVector_8() const { return ___rightVector_8; }
	inline Vector3_t3239555143 * get_address_of_rightVector_8() { return &___rightVector_8; }
	inline void set_rightVector_8(Vector3_t3239555143  value)
	{
		___rightVector_8 = value;
	}

	inline static int32_t get_offset_of_forwardVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3239555143_StaticFields, ___forwardVector_9)); }
	inline Vector3_t3239555143  get_forwardVector_9() const { return ___forwardVector_9; }
	inline Vector3_t3239555143 * get_address_of_forwardVector_9() { return &___forwardVector_9; }
	inline void set_forwardVector_9(Vector3_t3239555143  value)
	{
		___forwardVector_9 = value;
	}

	inline static int32_t get_offset_of_backVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3239555143_StaticFields, ___backVector_10)); }
	inline Vector3_t3239555143  get_backVector_10() const { return ___backVector_10; }
	inline Vector3_t3239555143 * get_address_of_backVector_10() { return &___backVector_10; }
	inline void set_backVector_10(Vector3_t3239555143  value)
	{
		___backVector_10 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3239555143_StaticFields, ___positiveInfinityVector_11)); }
	inline Vector3_t3239555143  get_positiveInfinityVector_11() const { return ___positiveInfinityVector_11; }
	inline Vector3_t3239555143 * get_address_of_positiveInfinityVector_11() { return &___positiveInfinityVector_11; }
	inline void set_positiveInfinityVector_11(Vector3_t3239555143  value)
	{
		___positiveInfinityVector_11 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3239555143_StaticFields, ___negativeInfinityVector_12)); }
	inline Vector3_t3239555143  get_negativeInfinityVector_12() const { return ___negativeInfinityVector_12; }
	inline Vector3_t3239555143 * get_address_of_negativeInfinityVector_12() { return &___negativeInfinityVector_12; }
	inline void set_negativeInfinityVector_12(Vector3_t3239555143  value)
	{
		___negativeInfinityVector_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3239555143_H
#ifndef RECT_T1111852907_H
#define RECT_T1111852907_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rect
struct  Rect_t1111852907 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t1111852907, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t1111852907, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t1111852907, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t1111852907, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T1111852907_H
#ifndef KEYFRAME_T3396755990_H
#define KEYFRAME_T3396755990_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Keyframe
struct  Keyframe_t3396755990 
{
public:
	// System.Single UnityEngine.Keyframe::m_Time
	float ___m_Time_0;
	// System.Single UnityEngine.Keyframe::m_Value
	float ___m_Value_1;
	// System.Single UnityEngine.Keyframe::m_InTangent
	float ___m_InTangent_2;
	// System.Single UnityEngine.Keyframe::m_OutTangent
	float ___m_OutTangent_3;

public:
	inline static int32_t get_offset_of_m_Time_0() { return static_cast<int32_t>(offsetof(Keyframe_t3396755990, ___m_Time_0)); }
	inline float get_m_Time_0() const { return ___m_Time_0; }
	inline float* get_address_of_m_Time_0() { return &___m_Time_0; }
	inline void set_m_Time_0(float value)
	{
		___m_Time_0 = value;
	}

	inline static int32_t get_offset_of_m_Value_1() { return static_cast<int32_t>(offsetof(Keyframe_t3396755990, ___m_Value_1)); }
	inline float get_m_Value_1() const { return ___m_Value_1; }
	inline float* get_address_of_m_Value_1() { return &___m_Value_1; }
	inline void set_m_Value_1(float value)
	{
		___m_Value_1 = value;
	}

	inline static int32_t get_offset_of_m_InTangent_2() { return static_cast<int32_t>(offsetof(Keyframe_t3396755990, ___m_InTangent_2)); }
	inline float get_m_InTangent_2() const { return ___m_InTangent_2; }
	inline float* get_address_of_m_InTangent_2() { return &___m_InTangent_2; }
	inline void set_m_InTangent_2(float value)
	{
		___m_InTangent_2 = value;
	}

	inline static int32_t get_offset_of_m_OutTangent_3() { return static_cast<int32_t>(offsetof(Keyframe_t3396755990, ___m_OutTangent_3)); }
	inline float get_m_OutTangent_3() const { return ___m_OutTangent_3; }
	inline float* get_address_of_m_OutTangent_3() { return &___m_OutTangent_3; }
	inline void set_m_OutTangent_3(float value)
	{
		___m_OutTangent_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYFRAME_T3396755990_H
#ifndef PROPERTYNAME_T666104458_H
#define PROPERTYNAME_T666104458_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PropertyName
struct  PropertyName_t666104458 
{
public:
	// System.Int32 UnityEngine.PropertyName::id
	int32_t ___id_0;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(PropertyName_t666104458, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYNAME_T666104458_H
#ifndef GCACHIEVEMENTDATA_T3167706508_H
#define GCACHIEVEMENTDATA_T3167706508_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SocialPlatforms.GameCenter.GcAchievementData
struct  GcAchievementData_t3167706508 
{
public:
	// System.String UnityEngine.SocialPlatforms.GameCenter.GcAchievementData::m_Identifier
	String_t* ___m_Identifier_0;
	// System.Double UnityEngine.SocialPlatforms.GameCenter.GcAchievementData::m_PercentCompleted
	double ___m_PercentCompleted_1;
	// System.Int32 UnityEngine.SocialPlatforms.GameCenter.GcAchievementData::m_Completed
	int32_t ___m_Completed_2;
	// System.Int32 UnityEngine.SocialPlatforms.GameCenter.GcAchievementData::m_Hidden
	int32_t ___m_Hidden_3;
	// System.Int32 UnityEngine.SocialPlatforms.GameCenter.GcAchievementData::m_LastReportedDate
	int32_t ___m_LastReportedDate_4;

public:
	inline static int32_t get_offset_of_m_Identifier_0() { return static_cast<int32_t>(offsetof(GcAchievementData_t3167706508, ___m_Identifier_0)); }
	inline String_t* get_m_Identifier_0() const { return ___m_Identifier_0; }
	inline String_t** get_address_of_m_Identifier_0() { return &___m_Identifier_0; }
	inline void set_m_Identifier_0(String_t* value)
	{
		___m_Identifier_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Identifier_0), value);
	}

	inline static int32_t get_offset_of_m_PercentCompleted_1() { return static_cast<int32_t>(offsetof(GcAchievementData_t3167706508, ___m_PercentCompleted_1)); }
	inline double get_m_PercentCompleted_1() const { return ___m_PercentCompleted_1; }
	inline double* get_address_of_m_PercentCompleted_1() { return &___m_PercentCompleted_1; }
	inline void set_m_PercentCompleted_1(double value)
	{
		___m_PercentCompleted_1 = value;
	}

	inline static int32_t get_offset_of_m_Completed_2() { return static_cast<int32_t>(offsetof(GcAchievementData_t3167706508, ___m_Completed_2)); }
	inline int32_t get_m_Completed_2() const { return ___m_Completed_2; }
	inline int32_t* get_address_of_m_Completed_2() { return &___m_Completed_2; }
	inline void set_m_Completed_2(int32_t value)
	{
		___m_Completed_2 = value;
	}

	inline static int32_t get_offset_of_m_Hidden_3() { return static_cast<int32_t>(offsetof(GcAchievementData_t3167706508, ___m_Hidden_3)); }
	inline int32_t get_m_Hidden_3() const { return ___m_Hidden_3; }
	inline int32_t* get_address_of_m_Hidden_3() { return &___m_Hidden_3; }
	inline void set_m_Hidden_3(int32_t value)
	{
		___m_Hidden_3 = value;
	}

	inline static int32_t get_offset_of_m_LastReportedDate_4() { return static_cast<int32_t>(offsetof(GcAchievementData_t3167706508, ___m_LastReportedDate_4)); }
	inline int32_t get_m_LastReportedDate_4() const { return ___m_LastReportedDate_4; }
	inline int32_t* get_address_of_m_LastReportedDate_4() { return &___m_LastReportedDate_4; }
	inline void set_m_LastReportedDate_4(int32_t value)
	{
		___m_LastReportedDate_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.SocialPlatforms.GameCenter.GcAchievementData
struct GcAchievementData_t3167706508_marshaled_pinvoke
{
	char* ___m_Identifier_0;
	double ___m_PercentCompleted_1;
	int32_t ___m_Completed_2;
	int32_t ___m_Hidden_3;
	int32_t ___m_LastReportedDate_4;
};
// Native definition for COM marshalling of UnityEngine.SocialPlatforms.GameCenter.GcAchievementData
struct GcAchievementData_t3167706508_marshaled_com
{
	Il2CppChar* ___m_Identifier_0;
	double ___m_PercentCompleted_1;
	int32_t ___m_Completed_2;
	int32_t ___m_Hidden_3;
	int32_t ___m_LastReportedDate_4;
};
#endif // GCACHIEVEMENTDATA_T3167706508_H
#ifndef GUID_T_H
#define GUID_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Guid
struct  Guid_t 
{
public:
	// System.Int32 System.Guid::_a
	int32_t ____a_0;
	// System.Int16 System.Guid::_b
	int16_t ____b_1;
	// System.Int16 System.Guid::_c
	int16_t ____c_2;
	// System.Byte System.Guid::_d
	uint8_t ____d_3;
	// System.Byte System.Guid::_e
	uint8_t ____e_4;
	// System.Byte System.Guid::_f
	uint8_t ____f_5;
	// System.Byte System.Guid::_g
	uint8_t ____g_6;
	// System.Byte System.Guid::_h
	uint8_t ____h_7;
	// System.Byte System.Guid::_i
	uint8_t ____i_8;
	// System.Byte System.Guid::_j
	uint8_t ____j_9;
	// System.Byte System.Guid::_k
	uint8_t ____k_10;

public:
	inline static int32_t get_offset_of__a_0() { return static_cast<int32_t>(offsetof(Guid_t, ____a_0)); }
	inline int32_t get__a_0() const { return ____a_0; }
	inline int32_t* get_address_of__a_0() { return &____a_0; }
	inline void set__a_0(int32_t value)
	{
		____a_0 = value;
	}

	inline static int32_t get_offset_of__b_1() { return static_cast<int32_t>(offsetof(Guid_t, ____b_1)); }
	inline int16_t get__b_1() const { return ____b_1; }
	inline int16_t* get_address_of__b_1() { return &____b_1; }
	inline void set__b_1(int16_t value)
	{
		____b_1 = value;
	}

	inline static int32_t get_offset_of__c_2() { return static_cast<int32_t>(offsetof(Guid_t, ____c_2)); }
	inline int16_t get__c_2() const { return ____c_2; }
	inline int16_t* get_address_of__c_2() { return &____c_2; }
	inline void set__c_2(int16_t value)
	{
		____c_2 = value;
	}

	inline static int32_t get_offset_of__d_3() { return static_cast<int32_t>(offsetof(Guid_t, ____d_3)); }
	inline uint8_t get__d_3() const { return ____d_3; }
	inline uint8_t* get_address_of__d_3() { return &____d_3; }
	inline void set__d_3(uint8_t value)
	{
		____d_3 = value;
	}

	inline static int32_t get_offset_of__e_4() { return static_cast<int32_t>(offsetof(Guid_t, ____e_4)); }
	inline uint8_t get__e_4() const { return ____e_4; }
	inline uint8_t* get_address_of__e_4() { return &____e_4; }
	inline void set__e_4(uint8_t value)
	{
		____e_4 = value;
	}

	inline static int32_t get_offset_of__f_5() { return static_cast<int32_t>(offsetof(Guid_t, ____f_5)); }
	inline uint8_t get__f_5() const { return ____f_5; }
	inline uint8_t* get_address_of__f_5() { return &____f_5; }
	inline void set__f_5(uint8_t value)
	{
		____f_5 = value;
	}

	inline static int32_t get_offset_of__g_6() { return static_cast<int32_t>(offsetof(Guid_t, ____g_6)); }
	inline uint8_t get__g_6() const { return ____g_6; }
	inline uint8_t* get_address_of__g_6() { return &____g_6; }
	inline void set__g_6(uint8_t value)
	{
		____g_6 = value;
	}

	inline static int32_t get_offset_of__h_7() { return static_cast<int32_t>(offsetof(Guid_t, ____h_7)); }
	inline uint8_t get__h_7() const { return ____h_7; }
	inline uint8_t* get_address_of__h_7() { return &____h_7; }
	inline void set__h_7(uint8_t value)
	{
		____h_7 = value;
	}

	inline static int32_t get_offset_of__i_8() { return static_cast<int32_t>(offsetof(Guid_t, ____i_8)); }
	inline uint8_t get__i_8() const { return ____i_8; }
	inline uint8_t* get_address_of__i_8() { return &____i_8; }
	inline void set__i_8(uint8_t value)
	{
		____i_8 = value;
	}

	inline static int32_t get_offset_of__j_9() { return static_cast<int32_t>(offsetof(Guid_t, ____j_9)); }
	inline uint8_t get__j_9() const { return ____j_9; }
	inline uint8_t* get_address_of__j_9() { return &____j_9; }
	inline void set__j_9(uint8_t value)
	{
		____j_9 = value;
	}

	inline static int32_t get_offset_of__k_10() { return static_cast<int32_t>(offsetof(Guid_t, ____k_10)); }
	inline uint8_t get__k_10() const { return ____k_10; }
	inline uint8_t* get_address_of__k_10() { return &____k_10; }
	inline void set__k_10(uint8_t value)
	{
		____k_10 = value;
	}
};

struct Guid_t_StaticFields
{
public:
	// System.Guid System.Guid::Empty
	Guid_t  ___Empty_11;
	// System.Object System.Guid::_rngAccess
	RuntimeObject * ____rngAccess_12;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_rng
	RandomNumberGenerator_t4107616314 * ____rng_13;

public:
	inline static int32_t get_offset_of_Empty_11() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ___Empty_11)); }
	inline Guid_t  get_Empty_11() const { return ___Empty_11; }
	inline Guid_t * get_address_of_Empty_11() { return &___Empty_11; }
	inline void set_Empty_11(Guid_t  value)
	{
		___Empty_11 = value;
	}

	inline static int32_t get_offset_of__rngAccess_12() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rngAccess_12)); }
	inline RuntimeObject * get__rngAccess_12() const { return ____rngAccess_12; }
	inline RuntimeObject ** get_address_of__rngAccess_12() { return &____rngAccess_12; }
	inline void set__rngAccess_12(RuntimeObject * value)
	{
		____rngAccess_12 = value;
		Il2CppCodeGenWriteBarrier((&____rngAccess_12), value);
	}

	inline static int32_t get_offset_of__rng_13() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rng_13)); }
	inline RandomNumberGenerator_t4107616314 * get__rng_13() const { return ____rng_13; }
	inline RandomNumberGenerator_t4107616314 ** get_address_of__rng_13() { return &____rng_13; }
	inline void set__rng_13(RandomNumberGenerator_t4107616314 * value)
	{
		____rng_13 = value;
		Il2CppCodeGenWriteBarrier((&____rng_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUID_T_H
#ifndef MONOENUMINFO_T776332500_H
#define MONOENUMINFO_T776332500_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MonoEnumInfo
struct  MonoEnumInfo_t776332500 
{
public:
	// System.Type System.MonoEnumInfo::utype
	Type_t * ___utype_0;
	// System.Array System.MonoEnumInfo::values
	RuntimeArray * ___values_1;
	// System.String[] System.MonoEnumInfo::names
	StringU5BU5D_t522578821* ___names_2;
	// System.Collections.Hashtable System.MonoEnumInfo::name_hash
	Hashtable_t3529848683 * ___name_hash_3;

public:
	inline static int32_t get_offset_of_utype_0() { return static_cast<int32_t>(offsetof(MonoEnumInfo_t776332500, ___utype_0)); }
	inline Type_t * get_utype_0() const { return ___utype_0; }
	inline Type_t ** get_address_of_utype_0() { return &___utype_0; }
	inline void set_utype_0(Type_t * value)
	{
		___utype_0 = value;
		Il2CppCodeGenWriteBarrier((&___utype_0), value);
	}

	inline static int32_t get_offset_of_values_1() { return static_cast<int32_t>(offsetof(MonoEnumInfo_t776332500, ___values_1)); }
	inline RuntimeArray * get_values_1() const { return ___values_1; }
	inline RuntimeArray ** get_address_of_values_1() { return &___values_1; }
	inline void set_values_1(RuntimeArray * value)
	{
		___values_1 = value;
		Il2CppCodeGenWriteBarrier((&___values_1), value);
	}

	inline static int32_t get_offset_of_names_2() { return static_cast<int32_t>(offsetof(MonoEnumInfo_t776332500, ___names_2)); }
	inline StringU5BU5D_t522578821* get_names_2() const { return ___names_2; }
	inline StringU5BU5D_t522578821** get_address_of_names_2() { return &___names_2; }
	inline void set_names_2(StringU5BU5D_t522578821* value)
	{
		___names_2 = value;
		Il2CppCodeGenWriteBarrier((&___names_2), value);
	}

	inline static int32_t get_offset_of_name_hash_3() { return static_cast<int32_t>(offsetof(MonoEnumInfo_t776332500, ___name_hash_3)); }
	inline Hashtable_t3529848683 * get_name_hash_3() const { return ___name_hash_3; }
	inline Hashtable_t3529848683 ** get_address_of_name_hash_3() { return &___name_hash_3; }
	inline void set_name_hash_3(Hashtable_t3529848683 * value)
	{
		___name_hash_3 = value;
		Il2CppCodeGenWriteBarrier((&___name_hash_3), value);
	}
};

struct MonoEnumInfo_t776332500_StaticFields
{
public:
	// System.Collections.Hashtable System.MonoEnumInfo::global_cache
	Hashtable_t3529848683 * ___global_cache_5;
	// System.Object System.MonoEnumInfo::global_cache_monitor
	RuntimeObject * ___global_cache_monitor_6;
	// System.MonoEnumInfo/SByteComparer System.MonoEnumInfo::sbyte_comparer
	SByteComparer_t1095340729 * ___sbyte_comparer_7;
	// System.MonoEnumInfo/ShortComparer System.MonoEnumInfo::short_comparer
	ShortComparer_t4228648934 * ___short_comparer_8;
	// System.MonoEnumInfo/IntComparer System.MonoEnumInfo::int_comparer
	IntComparer_t8372293 * ___int_comparer_9;
	// System.MonoEnumInfo/LongComparer System.MonoEnumInfo::long_comparer
	LongComparer_t1854865843 * ___long_comparer_10;

public:
	inline static int32_t get_offset_of_global_cache_5() { return static_cast<int32_t>(offsetof(MonoEnumInfo_t776332500_StaticFields, ___global_cache_5)); }
	inline Hashtable_t3529848683 * get_global_cache_5() const { return ___global_cache_5; }
	inline Hashtable_t3529848683 ** get_address_of_global_cache_5() { return &___global_cache_5; }
	inline void set_global_cache_5(Hashtable_t3529848683 * value)
	{
		___global_cache_5 = value;
		Il2CppCodeGenWriteBarrier((&___global_cache_5), value);
	}

	inline static int32_t get_offset_of_global_cache_monitor_6() { return static_cast<int32_t>(offsetof(MonoEnumInfo_t776332500_StaticFields, ___global_cache_monitor_6)); }
	inline RuntimeObject * get_global_cache_monitor_6() const { return ___global_cache_monitor_6; }
	inline RuntimeObject ** get_address_of_global_cache_monitor_6() { return &___global_cache_monitor_6; }
	inline void set_global_cache_monitor_6(RuntimeObject * value)
	{
		___global_cache_monitor_6 = value;
		Il2CppCodeGenWriteBarrier((&___global_cache_monitor_6), value);
	}

	inline static int32_t get_offset_of_sbyte_comparer_7() { return static_cast<int32_t>(offsetof(MonoEnumInfo_t776332500_StaticFields, ___sbyte_comparer_7)); }
	inline SByteComparer_t1095340729 * get_sbyte_comparer_7() const { return ___sbyte_comparer_7; }
	inline SByteComparer_t1095340729 ** get_address_of_sbyte_comparer_7() { return &___sbyte_comparer_7; }
	inline void set_sbyte_comparer_7(SByteComparer_t1095340729 * value)
	{
		___sbyte_comparer_7 = value;
		Il2CppCodeGenWriteBarrier((&___sbyte_comparer_7), value);
	}

	inline static int32_t get_offset_of_short_comparer_8() { return static_cast<int32_t>(offsetof(MonoEnumInfo_t776332500_StaticFields, ___short_comparer_8)); }
	inline ShortComparer_t4228648934 * get_short_comparer_8() const { return ___short_comparer_8; }
	inline ShortComparer_t4228648934 ** get_address_of_short_comparer_8() { return &___short_comparer_8; }
	inline void set_short_comparer_8(ShortComparer_t4228648934 * value)
	{
		___short_comparer_8 = value;
		Il2CppCodeGenWriteBarrier((&___short_comparer_8), value);
	}

	inline static int32_t get_offset_of_int_comparer_9() { return static_cast<int32_t>(offsetof(MonoEnumInfo_t776332500_StaticFields, ___int_comparer_9)); }
	inline IntComparer_t8372293 * get_int_comparer_9() const { return ___int_comparer_9; }
	inline IntComparer_t8372293 ** get_address_of_int_comparer_9() { return &___int_comparer_9; }
	inline void set_int_comparer_9(IntComparer_t8372293 * value)
	{
		___int_comparer_9 = value;
		Il2CppCodeGenWriteBarrier((&___int_comparer_9), value);
	}

	inline static int32_t get_offset_of_long_comparer_10() { return static_cast<int32_t>(offsetof(MonoEnumInfo_t776332500_StaticFields, ___long_comparer_10)); }
	inline LongComparer_t1854865843 * get_long_comparer_10() const { return ___long_comparer_10; }
	inline LongComparer_t1854865843 ** get_address_of_long_comparer_10() { return &___long_comparer_10; }
	inline void set_long_comparer_10(LongComparer_t1854865843 * value)
	{
		___long_comparer_10 = value;
		Il2CppCodeGenWriteBarrier((&___long_comparer_10), value);
	}
};

struct MonoEnumInfo_t776332500_ThreadStaticFields
{
public:
	// System.Collections.Hashtable System.MonoEnumInfo::cache
	Hashtable_t3529848683 * ___cache_4;

public:
	inline static int32_t get_offset_of_cache_4() { return static_cast<int32_t>(offsetof(MonoEnumInfo_t776332500_ThreadStaticFields, ___cache_4)); }
	inline Hashtable_t3529848683 * get_cache_4() const { return ___cache_4; }
	inline Hashtable_t3529848683 ** get_address_of_cache_4() { return &___cache_4; }
	inline void set_cache_4(Hashtable_t3529848683 * value)
	{
		___cache_4 = value;
		Il2CppCodeGenWriteBarrier((&___cache_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MonoEnumInfo
struct MonoEnumInfo_t776332500_marshaled_pinvoke
{
	Type_t * ___utype_0;
	RuntimeArray * ___values_1;
	char** ___names_2;
	Hashtable_t3529848683 * ___name_hash_3;
};
// Native definition for COM marshalling of System.MonoEnumInfo
struct MonoEnumInfo_t776332500_marshaled_com
{
	Type_t * ___utype_0;
	RuntimeArray * ___values_1;
	Il2CppChar** ___names_2;
	Hashtable_t3529848683 * ___name_hash_3;
};
#endif // MONOENUMINFO_T776332500_H
#ifndef ENUMERATOR_T2209518572_H
#define ENUMERATOR_T2209518572_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<System.Int32>
struct  Enumerator_t2209518572 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t449703515 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	int32_t ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_t2209518572, ___l_0)); }
	inline List_1_t449703515 * get_l_0() const { return ___l_0; }
	inline List_1_t449703515 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t449703515 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t2209518572, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t2209518572, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t2209518572, ___current_3)); }
	inline int32_t get_current_3() const { return ___current_3; }
	inline int32_t* get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(int32_t value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T2209518572_H
#ifndef ENUMERATOR_T3479511895_H
#define ENUMERATOR_T3479511895_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Queue`1/Enumerator<UnityEngine.UnitySynchronizationContext/WorkRequest>
struct  Enumerator_t3479511895 
{
public:
	// System.Collections.Generic.Queue`1<T> System.Collections.Generic.Queue`1/Enumerator::q
	Queue_1_t1920222844 * ___q_0;
	// System.Int32 System.Collections.Generic.Queue`1/Enumerator::idx
	int32_t ___idx_1;
	// System.Int32 System.Collections.Generic.Queue`1/Enumerator::ver
	int32_t ___ver_2;

public:
	inline static int32_t get_offset_of_q_0() { return static_cast<int32_t>(offsetof(Enumerator_t3479511895, ___q_0)); }
	inline Queue_1_t1920222844 * get_q_0() const { return ___q_0; }
	inline Queue_1_t1920222844 ** get_address_of_q_0() { return &___q_0; }
	inline void set_q_0(Queue_1_t1920222844 * value)
	{
		___q_0 = value;
		Il2CppCodeGenWriteBarrier((&___q_0), value);
	}

	inline static int32_t get_offset_of_idx_1() { return static_cast<int32_t>(offsetof(Enumerator_t3479511895, ___idx_1)); }
	inline int32_t get_idx_1() const { return ___idx_1; }
	inline int32_t* get_address_of_idx_1() { return &___idx_1; }
	inline void set_idx_1(int32_t value)
	{
		___idx_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t3479511895, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T3479511895_H
#ifndef SERIALIZATIONENTRY_T4251740542_H
#define SERIALIZATIONENTRY_T4251740542_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.SerializationEntry
struct  SerializationEntry_t4251740542 
{
public:
	// System.String System.Runtime.Serialization.SerializationEntry::name
	String_t* ___name_0;
	// System.Type System.Runtime.Serialization.SerializationEntry::objectType
	Type_t * ___objectType_1;
	// System.Object System.Runtime.Serialization.SerializationEntry::value
	RuntimeObject * ___value_2;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(SerializationEntry_t4251740542, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_objectType_1() { return static_cast<int32_t>(offsetof(SerializationEntry_t4251740542, ___objectType_1)); }
	inline Type_t * get_objectType_1() const { return ___objectType_1; }
	inline Type_t ** get_address_of_objectType_1() { return &___objectType_1; }
	inline void set_objectType_1(Type_t * value)
	{
		___objectType_1 = value;
		Il2CppCodeGenWriteBarrier((&___objectType_1), value);
	}

	inline static int32_t get_offset_of_value_2() { return static_cast<int32_t>(offsetof(SerializationEntry_t4251740542, ___value_2)); }
	inline RuntimeObject * get_value_2() const { return ___value_2; }
	inline RuntimeObject ** get_address_of_value_2() { return &___value_2; }
	inline void set_value_2(RuntimeObject * value)
	{
		___value_2 = value;
		Il2CppCodeGenWriteBarrier((&___value_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Runtime.Serialization.SerializationEntry
struct SerializationEntry_t4251740542_marshaled_pinvoke
{
	char* ___name_0;
	Type_t * ___objectType_1;
	Il2CppIUnknown* ___value_2;
};
// Native definition for COM marshalling of System.Runtime.Serialization.SerializationEntry
struct SerializationEntry_t4251740542_marshaled_com
{
	Il2CppChar* ___name_0;
	Type_t * ___objectType_1;
	Il2CppIUnknown* ___value_2;
};
#endif // SERIALIZATIONENTRY_T4251740542_H
#ifndef ENUM_T750633987_H
#define ENUM_T750633987_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t750633987  : public ValueType_t1920584095
{
public:

public:
};

struct Enum_t750633987_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t2864810077* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t750633987_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t2864810077* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t2864810077** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t2864810077* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t750633987_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t750633987_marshaled_com
{
};
#endif // ENUM_T750633987_H
#ifndef INTERVAL_T797373189_H
#define INTERVAL_T797373189_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.Interval
struct  Interval_t797373189 
{
public:
	// System.Int32 System.Text.RegularExpressions.Interval::low
	int32_t ___low_0;
	// System.Int32 System.Text.RegularExpressions.Interval::high
	int32_t ___high_1;
	// System.Boolean System.Text.RegularExpressions.Interval::contiguous
	bool ___contiguous_2;

public:
	inline static int32_t get_offset_of_low_0() { return static_cast<int32_t>(offsetof(Interval_t797373189, ___low_0)); }
	inline int32_t get_low_0() const { return ___low_0; }
	inline int32_t* get_address_of_low_0() { return &___low_0; }
	inline void set_low_0(int32_t value)
	{
		___low_0 = value;
	}

	inline static int32_t get_offset_of_high_1() { return static_cast<int32_t>(offsetof(Interval_t797373189, ___high_1)); }
	inline int32_t get_high_1() const { return ___high_1; }
	inline int32_t* get_address_of_high_1() { return &___high_1; }
	inline void set_high_1(int32_t value)
	{
		___high_1 = value;
	}

	inline static int32_t get_offset_of_contiguous_2() { return static_cast<int32_t>(offsetof(Interval_t797373189, ___contiguous_2)); }
	inline bool get_contiguous_2() const { return ___contiguous_2; }
	inline bool* get_address_of_contiguous_2() { return &___contiguous_2; }
	inline void set_contiguous_2(bool value)
	{
		___contiguous_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Text.RegularExpressions.Interval
struct Interval_t797373189_marshaled_pinvoke
{
	int32_t ___low_0;
	int32_t ___high_1;
	int32_t ___contiguous_2;
};
// Native definition for COM marshalling of System.Text.RegularExpressions.Interval
struct Interval_t797373189_marshaled_com
{
	int32_t ___low_0;
	int32_t ___high_1;
	int32_t ___contiguous_2;
};
#endif // INTERVAL_T797373189_H
#ifndef CSSSIZE_T2436738465_H
#define CSSSIZE_T2436738465_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CSSLayout.CSSSize
struct  CSSSize_t2436738465 
{
public:
	// System.Single UnityEngine.CSSLayout.CSSSize::width
	float ___width_0;
	// System.Single UnityEngine.CSSLayout.CSSSize::height
	float ___height_1;

public:
	inline static int32_t get_offset_of_width_0() { return static_cast<int32_t>(offsetof(CSSSize_t2436738465, ___width_0)); }
	inline float get_width_0() const { return ___width_0; }
	inline float* get_address_of_width_0() { return &___width_0; }
	inline void set_width_0(float value)
	{
		___width_0 = value;
	}

	inline static int32_t get_offset_of_height_1() { return static_cast<int32_t>(offsetof(CSSSize_t2436738465, ___height_1)); }
	inline float get_height_1() const { return ___height_1; }
	inline float* get_address_of_height_1() { return &___height_1; }
	inline void set_height_1(float value)
	{
		___height_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CSSSIZE_T2436738465_H
#ifndef GCACHIEVEMENTDESCRIPTIONDATA_T2898970914_H
#define GCACHIEVEMENTDESCRIPTIONDATA_T2898970914_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData
struct  GcAchievementDescriptionData_t2898970914 
{
public:
	// System.String UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData::m_Identifier
	String_t* ___m_Identifier_0;
	// System.String UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData::m_Title
	String_t* ___m_Title_1;
	// UnityEngine.Texture2D UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData::m_Image
	Texture2D_t359445654 * ___m_Image_2;
	// System.String UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData::m_AchievedDescription
	String_t* ___m_AchievedDescription_3;
	// System.String UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData::m_UnachievedDescription
	String_t* ___m_UnachievedDescription_4;
	// System.Int32 UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData::m_Hidden
	int32_t ___m_Hidden_5;
	// System.Int32 UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData::m_Points
	int32_t ___m_Points_6;

public:
	inline static int32_t get_offset_of_m_Identifier_0() { return static_cast<int32_t>(offsetof(GcAchievementDescriptionData_t2898970914, ___m_Identifier_0)); }
	inline String_t* get_m_Identifier_0() const { return ___m_Identifier_0; }
	inline String_t** get_address_of_m_Identifier_0() { return &___m_Identifier_0; }
	inline void set_m_Identifier_0(String_t* value)
	{
		___m_Identifier_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Identifier_0), value);
	}

	inline static int32_t get_offset_of_m_Title_1() { return static_cast<int32_t>(offsetof(GcAchievementDescriptionData_t2898970914, ___m_Title_1)); }
	inline String_t* get_m_Title_1() const { return ___m_Title_1; }
	inline String_t** get_address_of_m_Title_1() { return &___m_Title_1; }
	inline void set_m_Title_1(String_t* value)
	{
		___m_Title_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Title_1), value);
	}

	inline static int32_t get_offset_of_m_Image_2() { return static_cast<int32_t>(offsetof(GcAchievementDescriptionData_t2898970914, ___m_Image_2)); }
	inline Texture2D_t359445654 * get_m_Image_2() const { return ___m_Image_2; }
	inline Texture2D_t359445654 ** get_address_of_m_Image_2() { return &___m_Image_2; }
	inline void set_m_Image_2(Texture2D_t359445654 * value)
	{
		___m_Image_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Image_2), value);
	}

	inline static int32_t get_offset_of_m_AchievedDescription_3() { return static_cast<int32_t>(offsetof(GcAchievementDescriptionData_t2898970914, ___m_AchievedDescription_3)); }
	inline String_t* get_m_AchievedDescription_3() const { return ___m_AchievedDescription_3; }
	inline String_t** get_address_of_m_AchievedDescription_3() { return &___m_AchievedDescription_3; }
	inline void set_m_AchievedDescription_3(String_t* value)
	{
		___m_AchievedDescription_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_AchievedDescription_3), value);
	}

	inline static int32_t get_offset_of_m_UnachievedDescription_4() { return static_cast<int32_t>(offsetof(GcAchievementDescriptionData_t2898970914, ___m_UnachievedDescription_4)); }
	inline String_t* get_m_UnachievedDescription_4() const { return ___m_UnachievedDescription_4; }
	inline String_t** get_address_of_m_UnachievedDescription_4() { return &___m_UnachievedDescription_4; }
	inline void set_m_UnachievedDescription_4(String_t* value)
	{
		___m_UnachievedDescription_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_UnachievedDescription_4), value);
	}

	inline static int32_t get_offset_of_m_Hidden_5() { return static_cast<int32_t>(offsetof(GcAchievementDescriptionData_t2898970914, ___m_Hidden_5)); }
	inline int32_t get_m_Hidden_5() const { return ___m_Hidden_5; }
	inline int32_t* get_address_of_m_Hidden_5() { return &___m_Hidden_5; }
	inline void set_m_Hidden_5(int32_t value)
	{
		___m_Hidden_5 = value;
	}

	inline static int32_t get_offset_of_m_Points_6() { return static_cast<int32_t>(offsetof(GcAchievementDescriptionData_t2898970914, ___m_Points_6)); }
	inline int32_t get_m_Points_6() const { return ___m_Points_6; }
	inline int32_t* get_address_of_m_Points_6() { return &___m_Points_6; }
	inline void set_m_Points_6(int32_t value)
	{
		___m_Points_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData
struct GcAchievementDescriptionData_t2898970914_marshaled_pinvoke
{
	char* ___m_Identifier_0;
	char* ___m_Title_1;
	Texture2D_t359445654 * ___m_Image_2;
	char* ___m_AchievedDescription_3;
	char* ___m_UnachievedDescription_4;
	int32_t ___m_Hidden_5;
	int32_t ___m_Points_6;
};
// Native definition for COM marshalling of UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData
struct GcAchievementDescriptionData_t2898970914_marshaled_com
{
	Il2CppChar* ___m_Identifier_0;
	Il2CppChar* ___m_Title_1;
	Texture2D_t359445654 * ___m_Image_2;
	Il2CppChar* ___m_AchievedDescription_3;
	Il2CppChar* ___m_UnachievedDescription_4;
	int32_t ___m_Hidden_5;
	int32_t ___m_Points_6;
};
#endif // GCACHIEVEMENTDESCRIPTIONDATA_T2898970914_H
#ifndef GCUSERPROFILEDATA_T3262991188_H
#define GCUSERPROFILEDATA_T3262991188_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData
struct  GcUserProfileData_t3262991188 
{
public:
	// System.String UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData::userName
	String_t* ___userName_0;
	// System.String UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData::userID
	String_t* ___userID_1;
	// System.Int32 UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData::isFriend
	int32_t ___isFriend_2;
	// UnityEngine.Texture2D UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData::image
	Texture2D_t359445654 * ___image_3;

public:
	inline static int32_t get_offset_of_userName_0() { return static_cast<int32_t>(offsetof(GcUserProfileData_t3262991188, ___userName_0)); }
	inline String_t* get_userName_0() const { return ___userName_0; }
	inline String_t** get_address_of_userName_0() { return &___userName_0; }
	inline void set_userName_0(String_t* value)
	{
		___userName_0 = value;
		Il2CppCodeGenWriteBarrier((&___userName_0), value);
	}

	inline static int32_t get_offset_of_userID_1() { return static_cast<int32_t>(offsetof(GcUserProfileData_t3262991188, ___userID_1)); }
	inline String_t* get_userID_1() const { return ___userID_1; }
	inline String_t** get_address_of_userID_1() { return &___userID_1; }
	inline void set_userID_1(String_t* value)
	{
		___userID_1 = value;
		Il2CppCodeGenWriteBarrier((&___userID_1), value);
	}

	inline static int32_t get_offset_of_isFriend_2() { return static_cast<int32_t>(offsetof(GcUserProfileData_t3262991188, ___isFriend_2)); }
	inline int32_t get_isFriend_2() const { return ___isFriend_2; }
	inline int32_t* get_address_of_isFriend_2() { return &___isFriend_2; }
	inline void set_isFriend_2(int32_t value)
	{
		___isFriend_2 = value;
	}

	inline static int32_t get_offset_of_image_3() { return static_cast<int32_t>(offsetof(GcUserProfileData_t3262991188, ___image_3)); }
	inline Texture2D_t359445654 * get_image_3() const { return ___image_3; }
	inline Texture2D_t359445654 ** get_address_of_image_3() { return &___image_3; }
	inline void set_image_3(Texture2D_t359445654 * value)
	{
		___image_3 = value;
		Il2CppCodeGenWriteBarrier((&___image_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData
struct GcUserProfileData_t3262991188_marshaled_pinvoke
{
	char* ___userName_0;
	char* ___userID_1;
	int32_t ___isFriend_2;
	Texture2D_t359445654 * ___image_3;
};
// Native definition for COM marshalling of UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData
struct GcUserProfileData_t3262991188_marshaled_com
{
	Il2CppChar* ___userName_0;
	Il2CppChar* ___userID_1;
	int32_t ___isFriend_2;
	Texture2D_t359445654 * ___image_3;
};
#endif // GCUSERPROFILEDATA_T3262991188_H
#ifndef LABELDATA_T1059919410_H
#define LABELDATA_T1059919410_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.Emit.ILGenerator/LabelData
struct  LabelData_t1059919410 
{
public:
	// System.Int32 System.Reflection.Emit.ILGenerator/LabelData::addr
	int32_t ___addr_0;
	// System.Int32 System.Reflection.Emit.ILGenerator/LabelData::maxStack
	int32_t ___maxStack_1;

public:
	inline static int32_t get_offset_of_addr_0() { return static_cast<int32_t>(offsetof(LabelData_t1059919410, ___addr_0)); }
	inline int32_t get_addr_0() const { return ___addr_0; }
	inline int32_t* get_address_of_addr_0() { return &___addr_0; }
	inline void set_addr_0(int32_t value)
	{
		___addr_0 = value;
	}

	inline static int32_t get_offset_of_maxStack_1() { return static_cast<int32_t>(offsetof(LabelData_t1059919410, ___maxStack_1)); }
	inline int32_t get_maxStack_1() const { return ___maxStack_1; }
	inline int32_t* get_address_of_maxStack_1() { return &___maxStack_1; }
	inline void set_maxStack_1(int32_t value)
	{
		___maxStack_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LABELDATA_T1059919410_H
#ifndef CUSTOMATTRIBUTETYPEDARGUMENT_T2667702721_H
#define CUSTOMATTRIBUTETYPEDARGUMENT_T2667702721_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.CustomAttributeTypedArgument
struct  CustomAttributeTypedArgument_t2667702721 
{
public:
	// System.Type System.Reflection.CustomAttributeTypedArgument::argumentType
	Type_t * ___argumentType_0;
	// System.Object System.Reflection.CustomAttributeTypedArgument::value
	RuntimeObject * ___value_1;

public:
	inline static int32_t get_offset_of_argumentType_0() { return static_cast<int32_t>(offsetof(CustomAttributeTypedArgument_t2667702721, ___argumentType_0)); }
	inline Type_t * get_argumentType_0() const { return ___argumentType_0; }
	inline Type_t ** get_address_of_argumentType_0() { return &___argumentType_0; }
	inline void set_argumentType_0(Type_t * value)
	{
		___argumentType_0 = value;
		Il2CppCodeGenWriteBarrier((&___argumentType_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(CustomAttributeTypedArgument_t2667702721, ___value_1)); }
	inline RuntimeObject * get_value_1() const { return ___value_1; }
	inline RuntimeObject ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(RuntimeObject * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Reflection.CustomAttributeTypedArgument
struct CustomAttributeTypedArgument_t2667702721_marshaled_pinvoke
{
	Type_t * ___argumentType_0;
	Il2CppIUnknown* ___value_1;
};
// Native definition for COM marshalling of System.Reflection.CustomAttributeTypedArgument
struct CustomAttributeTypedArgument_t2667702721_marshaled_com
{
	Type_t * ___argumentType_0;
	Il2CppIUnknown* ___value_1;
};
#endif // CUSTOMATTRIBUTETYPEDARGUMENT_T2667702721_H
#ifndef PARAMETERMODIFIER_T969812002_H
#define PARAMETERMODIFIER_T969812002_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.ParameterModifier
struct  ParameterModifier_t969812002 
{
public:
	// System.Boolean[] System.Reflection.ParameterModifier::_byref
	BooleanU5BU5D_t129461842* ____byref_0;

public:
	inline static int32_t get_offset_of__byref_0() { return static_cast<int32_t>(offsetof(ParameterModifier_t969812002, ____byref_0)); }
	inline BooleanU5BU5D_t129461842* get__byref_0() const { return ____byref_0; }
	inline BooleanU5BU5D_t129461842** get_address_of__byref_0() { return &____byref_0; }
	inline void set__byref_0(BooleanU5BU5D_t129461842* value)
	{
		____byref_0 = value;
		Il2CppCodeGenWriteBarrier((&____byref_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Reflection.ParameterModifier
struct ParameterModifier_t969812002_marshaled_pinvoke
{
	int32_t* ____byref_0;
};
// Native definition for COM marshalling of System.Reflection.ParameterModifier
struct ParameterModifier_t969812002_marshaled_com
{
	int32_t* ____byref_0;
};
#endif // PARAMETERMODIFIER_T969812002_H
#ifndef SLOT_T1141617207_H
#define SLOT_T1141617207_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.SortedList/Slot
struct  Slot_t1141617207 
{
public:
	// System.Object System.Collections.SortedList/Slot::key
	RuntimeObject * ___key_0;
	// System.Object System.Collections.SortedList/Slot::value
	RuntimeObject * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(Slot_t1141617207, ___key_0)); }
	inline RuntimeObject * get_key_0() const { return ___key_0; }
	inline RuntimeObject ** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(RuntimeObject * value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(Slot_t1141617207, ___value_1)); }
	inline RuntimeObject * get_value_1() const { return ___value_1; }
	inline RuntimeObject ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(RuntimeObject * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Collections.SortedList/Slot
struct Slot_t1141617207_marshaled_pinvoke
{
	Il2CppIUnknown* ___key_0;
	Il2CppIUnknown* ___value_1;
};
// Native definition for COM marshalling of System.Collections.SortedList/Slot
struct Slot_t1141617207_marshaled_com
{
	Il2CppIUnknown* ___key_0;
	Il2CppIUnknown* ___value_1;
};
#endif // SLOT_T1141617207_H
#ifndef RESOURCECACHEITEM_T3632973466_H
#define RESOURCECACHEITEM_T3632973466_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Resources.ResourceReader/ResourceCacheItem
struct  ResourceCacheItem_t3632973466 
{
public:
	// System.String System.Resources.ResourceReader/ResourceCacheItem::ResourceName
	String_t* ___ResourceName_0;
	// System.Object System.Resources.ResourceReader/ResourceCacheItem::ResourceValue
	RuntimeObject * ___ResourceValue_1;

public:
	inline static int32_t get_offset_of_ResourceName_0() { return static_cast<int32_t>(offsetof(ResourceCacheItem_t3632973466, ___ResourceName_0)); }
	inline String_t* get_ResourceName_0() const { return ___ResourceName_0; }
	inline String_t** get_address_of_ResourceName_0() { return &___ResourceName_0; }
	inline void set_ResourceName_0(String_t* value)
	{
		___ResourceName_0 = value;
		Il2CppCodeGenWriteBarrier((&___ResourceName_0), value);
	}

	inline static int32_t get_offset_of_ResourceValue_1() { return static_cast<int32_t>(offsetof(ResourceCacheItem_t3632973466, ___ResourceValue_1)); }
	inline RuntimeObject * get_ResourceValue_1() const { return ___ResourceValue_1; }
	inline RuntimeObject ** get_address_of_ResourceValue_1() { return &___ResourceValue_1; }
	inline void set_ResourceValue_1(RuntimeObject * value)
	{
		___ResourceValue_1 = value;
		Il2CppCodeGenWriteBarrier((&___ResourceValue_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Resources.ResourceReader/ResourceCacheItem
struct ResourceCacheItem_t3632973466_marshaled_pinvoke
{
	char* ___ResourceName_0;
	Il2CppIUnknown* ___ResourceValue_1;
};
// Native definition for COM marshalling of System.Resources.ResourceReader/ResourceCacheItem
struct ResourceCacheItem_t3632973466_marshaled_com
{
	Il2CppChar* ___ResourceName_0;
	Il2CppIUnknown* ___ResourceValue_1;
};
#endif // RESOURCECACHEITEM_T3632973466_H
#ifndef SLOT_T2102621701_H
#define SLOT_T2102621701_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Hashtable/Slot
struct  Slot_t2102621701 
{
public:
	// System.Object System.Collections.Hashtable/Slot::key
	RuntimeObject * ___key_0;
	// System.Object System.Collections.Hashtable/Slot::value
	RuntimeObject * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(Slot_t2102621701, ___key_0)); }
	inline RuntimeObject * get_key_0() const { return ___key_0; }
	inline RuntimeObject ** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(RuntimeObject * value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(Slot_t2102621701, ___value_1)); }
	inline RuntimeObject * get_value_1() const { return ___value_1; }
	inline RuntimeObject ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(RuntimeObject * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Collections.Hashtable/Slot
struct Slot_t2102621701_marshaled_pinvoke
{
	Il2CppIUnknown* ___key_0;
	Il2CppIUnknown* ___value_1;
};
// Native definition for COM marshalling of System.Collections.Hashtable/Slot
struct Slot_t2102621701_marshaled_com
{
	Il2CppIUnknown* ___key_0;
	Il2CppIUnknown* ___value_1;
};
#endif // SLOT_T2102621701_H
#ifndef LINK_T3525875035_H
#define LINK_T3525875035_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Link
struct  Link_t3525875035 
{
public:
	// System.Int32 System.Collections.Generic.Link::HashCode
	int32_t ___HashCode_0;
	// System.Int32 System.Collections.Generic.Link::Next
	int32_t ___Next_1;

public:
	inline static int32_t get_offset_of_HashCode_0() { return static_cast<int32_t>(offsetof(Link_t3525875035, ___HashCode_0)); }
	inline int32_t get_HashCode_0() const { return ___HashCode_0; }
	inline int32_t* get_address_of_HashCode_0() { return &___HashCode_0; }
	inline void set_HashCode_0(int32_t value)
	{
		___HashCode_0 = value;
	}

	inline static int32_t get_offset_of_Next_1() { return static_cast<int32_t>(offsetof(Link_t3525875035, ___Next_1)); }
	inline int32_t get_Next_1() const { return ___Next_1; }
	inline int32_t* get_address_of_Next_1() { return &___Next_1; }
	inline void set_Next_1(int32_t value)
	{
		___Next_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINK_T3525875035_H
#ifndef KEYVALUEPAIR_2_T2877556189_H
#define KEYVALUEPAIR_2_T2877556189_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>
struct  KeyValuePair_2_t2877556189 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	RuntimeObject * ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	int32_t ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2877556189, ___key_0)); }
	inline RuntimeObject * get_key_0() const { return ___key_0; }
	inline RuntimeObject ** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(RuntimeObject * value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2877556189, ___value_1)); }
	inline int32_t get_value_1() const { return ___value_1; }
	inline int32_t* get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(int32_t value)
	{
		___value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T2877556189_H
#ifndef KEYVALUEPAIR_2_T2366093889_H
#define KEYVALUEPAIR_2_T2366093889_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>
struct  KeyValuePair_2_t2366093889 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	RuntimeObject * ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	bool ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2366093889, ___key_0)); }
	inline RuntimeObject * get_key_0() const { return ___key_0; }
	inline RuntimeObject ** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(RuntimeObject * value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2366093889, ___value_1)); }
	inline bool get_value_1() const { return ___value_1; }
	inline bool* get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(bool value)
	{
		___value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T2366093889_H
#ifndef ILTOKENINFO_T281295013_H
#define ILTOKENINFO_T281295013_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.Emit.ILTokenInfo
struct  ILTokenInfo_t281295013 
{
public:
	// System.Reflection.MemberInfo System.Reflection.Emit.ILTokenInfo::member
	MemberInfo_t * ___member_0;
	// System.Int32 System.Reflection.Emit.ILTokenInfo::code_pos
	int32_t ___code_pos_1;

public:
	inline static int32_t get_offset_of_member_0() { return static_cast<int32_t>(offsetof(ILTokenInfo_t281295013, ___member_0)); }
	inline MemberInfo_t * get_member_0() const { return ___member_0; }
	inline MemberInfo_t ** get_address_of_member_0() { return &___member_0; }
	inline void set_member_0(MemberInfo_t * value)
	{
		___member_0 = value;
		Il2CppCodeGenWriteBarrier((&___member_0), value);
	}

	inline static int32_t get_offset_of_code_pos_1() { return static_cast<int32_t>(offsetof(ILTokenInfo_t281295013, ___code_pos_1)); }
	inline int32_t get_code_pos_1() const { return ___code_pos_1; }
	inline int32_t* get_address_of_code_pos_1() { return &___code_pos_1; }
	inline void set_code_pos_1(int32_t value)
	{
		___code_pos_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Reflection.Emit.ILTokenInfo
struct ILTokenInfo_t281295013_marshaled_pinvoke
{
	MemberInfo_t * ___member_0;
	int32_t ___code_pos_1;
};
// Native definition for COM marshalling of System.Reflection.Emit.ILTokenInfo
struct ILTokenInfo_t281295013_marshaled_com
{
	MemberInfo_t * ___member_0;
	int32_t ___code_pos_1;
};
#endif // ILTOKENINFO_T281295013_H
#ifndef TABLERANGE_T4125455382_H
#define TABLERANGE_T4125455382_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Globalization.Unicode.CodePointIndexer/TableRange
struct  TableRange_t4125455382 
{
public:
	// System.Int32 Mono.Globalization.Unicode.CodePointIndexer/TableRange::Start
	int32_t ___Start_0;
	// System.Int32 Mono.Globalization.Unicode.CodePointIndexer/TableRange::End
	int32_t ___End_1;
	// System.Int32 Mono.Globalization.Unicode.CodePointIndexer/TableRange::Count
	int32_t ___Count_2;
	// System.Int32 Mono.Globalization.Unicode.CodePointIndexer/TableRange::IndexStart
	int32_t ___IndexStart_3;
	// System.Int32 Mono.Globalization.Unicode.CodePointIndexer/TableRange::IndexEnd
	int32_t ___IndexEnd_4;

public:
	inline static int32_t get_offset_of_Start_0() { return static_cast<int32_t>(offsetof(TableRange_t4125455382, ___Start_0)); }
	inline int32_t get_Start_0() const { return ___Start_0; }
	inline int32_t* get_address_of_Start_0() { return &___Start_0; }
	inline void set_Start_0(int32_t value)
	{
		___Start_0 = value;
	}

	inline static int32_t get_offset_of_End_1() { return static_cast<int32_t>(offsetof(TableRange_t4125455382, ___End_1)); }
	inline int32_t get_End_1() const { return ___End_1; }
	inline int32_t* get_address_of_End_1() { return &___End_1; }
	inline void set_End_1(int32_t value)
	{
		___End_1 = value;
	}

	inline static int32_t get_offset_of_Count_2() { return static_cast<int32_t>(offsetof(TableRange_t4125455382, ___Count_2)); }
	inline int32_t get_Count_2() const { return ___Count_2; }
	inline int32_t* get_address_of_Count_2() { return &___Count_2; }
	inline void set_Count_2(int32_t value)
	{
		___Count_2 = value;
	}

	inline static int32_t get_offset_of_IndexStart_3() { return static_cast<int32_t>(offsetof(TableRange_t4125455382, ___IndexStart_3)); }
	inline int32_t get_IndexStart_3() const { return ___IndexStart_3; }
	inline int32_t* get_address_of_IndexStart_3() { return &___IndexStart_3; }
	inline void set_IndexStart_3(int32_t value)
	{
		___IndexStart_3 = value;
	}

	inline static int32_t get_offset_of_IndexEnd_4() { return static_cast<int32_t>(offsetof(TableRange_t4125455382, ___IndexEnd_4)); }
	inline int32_t get_IndexEnd_4() const { return ___IndexEnd_4; }
	inline int32_t* get_address_of_IndexEnd_4() { return &___IndexEnd_4; }
	inline void set_IndexEnd_4(int32_t value)
	{
		___IndexEnd_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TABLERANGE_T4125455382_H
#ifndef ENUMERATOR_T1041909981_H
#define ENUMERATOR_T1041909981_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Stack`1/Enumerator<System.Object>
struct  Enumerator_t1041909981 
{
public:
	// System.Collections.Generic.Stack`1<T> System.Collections.Generic.Stack`1/Enumerator::parent
	Stack_1_t3886787695 * ___parent_0;
	// System.Int32 System.Collections.Generic.Stack`1/Enumerator::idx
	int32_t ___idx_1;
	// System.Int32 System.Collections.Generic.Stack`1/Enumerator::_version
	int32_t ____version_2;

public:
	inline static int32_t get_offset_of_parent_0() { return static_cast<int32_t>(offsetof(Enumerator_t1041909981, ___parent_0)); }
	inline Stack_1_t3886787695 * get_parent_0() const { return ___parent_0; }
	inline Stack_1_t3886787695 ** get_address_of_parent_0() { return &___parent_0; }
	inline void set_parent_0(Stack_1_t3886787695 * value)
	{
		___parent_0 = value;
		Il2CppCodeGenWriteBarrier((&___parent_0), value);
	}

	inline static int32_t get_offset_of_idx_1() { return static_cast<int32_t>(offsetof(Enumerator_t1041909981, ___idx_1)); }
	inline int32_t get_idx_1() const { return ___idx_1; }
	inline int32_t* get_address_of_idx_1() { return &___idx_1; }
	inline void set_idx_1(int32_t value)
	{
		___idx_1 = value;
	}

	inline static int32_t get_offset_of__version_2() { return static_cast<int32_t>(offsetof(Enumerator_t1041909981, ____version_2)); }
	inline int32_t get__version_2() const { return ____version_2; }
	inline int32_t* get_address_of__version_2() { return &____version_2; }
	inline void set__version_2(int32_t value)
	{
		____version_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T1041909981_H
#ifndef ENUMERATOR_T4228099087_H
#define ENUMERATOR_T4228099087_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Queue`1/Enumerator<System.Object>
struct  Enumerator_t4228099087 
{
public:
	// System.Collections.Generic.Queue`1<T> System.Collections.Generic.Queue`1/Enumerator::q
	Queue_1_t2668810036 * ___q_0;
	// System.Int32 System.Collections.Generic.Queue`1/Enumerator::idx
	int32_t ___idx_1;
	// System.Int32 System.Collections.Generic.Queue`1/Enumerator::ver
	int32_t ___ver_2;

public:
	inline static int32_t get_offset_of_q_0() { return static_cast<int32_t>(offsetof(Enumerator_t4228099087, ___q_0)); }
	inline Queue_1_t2668810036 * get_q_0() const { return ___q_0; }
	inline Queue_1_t2668810036 ** get_address_of_q_0() { return &___q_0; }
	inline void set_q_0(Queue_1_t2668810036 * value)
	{
		___q_0 = value;
		Il2CppCodeGenWriteBarrier((&___q_0), value);
	}

	inline static int32_t get_offset_of_idx_1() { return static_cast<int32_t>(offsetof(Enumerator_t4228099087, ___idx_1)); }
	inline int32_t get_idx_1() const { return ___idx_1; }
	inline int32_t* get_address_of_idx_1() { return &___idx_1; }
	inline void set_idx_1(int32_t value)
	{
		___idx_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t4228099087, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T4228099087_H
#ifndef MARK_T4050795874_H
#define MARK_T4050795874_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.Mark
struct  Mark_t4050795874 
{
public:
	// System.Int32 System.Text.RegularExpressions.Mark::Start
	int32_t ___Start_0;
	// System.Int32 System.Text.RegularExpressions.Mark::End
	int32_t ___End_1;
	// System.Int32 System.Text.RegularExpressions.Mark::Previous
	int32_t ___Previous_2;

public:
	inline static int32_t get_offset_of_Start_0() { return static_cast<int32_t>(offsetof(Mark_t4050795874, ___Start_0)); }
	inline int32_t get_Start_0() const { return ___Start_0; }
	inline int32_t* get_address_of_Start_0() { return &___Start_0; }
	inline void set_Start_0(int32_t value)
	{
		___Start_0 = value;
	}

	inline static int32_t get_offset_of_End_1() { return static_cast<int32_t>(offsetof(Mark_t4050795874, ___End_1)); }
	inline int32_t get_End_1() const { return ___End_1; }
	inline int32_t* get_address_of_End_1() { return &___End_1; }
	inline void set_End_1(int32_t value)
	{
		___End_1 = value;
	}

	inline static int32_t get_offset_of_Previous_2() { return static_cast<int32_t>(offsetof(Mark_t4050795874, ___Previous_2)); }
	inline int32_t get_Previous_2() const { return ___Previous_2; }
	inline int32_t* get_address_of_Previous_2() { return &___Previous_2; }
	inline void set_Previous_2(int32_t value)
	{
		___Previous_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MARK_T4050795874_H
#ifndef URISCHEME_T2622429923_H
#define URISCHEME_T2622429923_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Uri/UriScheme
struct  UriScheme_t2622429923 
{
public:
	// System.String System.Uri/UriScheme::scheme
	String_t* ___scheme_0;
	// System.String System.Uri/UriScheme::delimiter
	String_t* ___delimiter_1;
	// System.Int32 System.Uri/UriScheme::defaultPort
	int32_t ___defaultPort_2;

public:
	inline static int32_t get_offset_of_scheme_0() { return static_cast<int32_t>(offsetof(UriScheme_t2622429923, ___scheme_0)); }
	inline String_t* get_scheme_0() const { return ___scheme_0; }
	inline String_t** get_address_of_scheme_0() { return &___scheme_0; }
	inline void set_scheme_0(String_t* value)
	{
		___scheme_0 = value;
		Il2CppCodeGenWriteBarrier((&___scheme_0), value);
	}

	inline static int32_t get_offset_of_delimiter_1() { return static_cast<int32_t>(offsetof(UriScheme_t2622429923, ___delimiter_1)); }
	inline String_t* get_delimiter_1() const { return ___delimiter_1; }
	inline String_t** get_address_of_delimiter_1() { return &___delimiter_1; }
	inline void set_delimiter_1(String_t* value)
	{
		___delimiter_1 = value;
		Il2CppCodeGenWriteBarrier((&___delimiter_1), value);
	}

	inline static int32_t get_offset_of_defaultPort_2() { return static_cast<int32_t>(offsetof(UriScheme_t2622429923, ___defaultPort_2)); }
	inline int32_t get_defaultPort_2() const { return ___defaultPort_2; }
	inline int32_t* get_address_of_defaultPort_2() { return &___defaultPort_2; }
	inline void set_defaultPort_2(int32_t value)
	{
		___defaultPort_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Uri/UriScheme
struct UriScheme_t2622429923_marshaled_pinvoke
{
	char* ___scheme_0;
	char* ___delimiter_1;
	int32_t ___defaultPort_2;
};
// Native definition for COM marshalling of System.Uri/UriScheme
struct UriScheme_t2622429923_marshaled_com
{
	Il2CppChar* ___scheme_0;
	Il2CppChar* ___delimiter_1;
	int32_t ___defaultPort_2;
};
#endif // URISCHEME_T2622429923_H
#ifndef GCHANDLE_T4215464244_H
#define GCHANDLE_T4215464244_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.GCHandle
struct  GCHandle_t4215464244 
{
public:
	// System.Int32 System.Runtime.InteropServices.GCHandle::handle
	int32_t ___handle_0;

public:
	inline static int32_t get_offset_of_handle_0() { return static_cast<int32_t>(offsetof(GCHandle_t4215464244, ___handle_0)); }
	inline int32_t get_handle_0() const { return ___handle_0; }
	inline int32_t* get_address_of_handle_0() { return &___handle_0; }
	inline void set_handle_0(int32_t value)
	{
		___handle_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GCHANDLE_T4215464244_H
#ifndef ENUMERATOR_T2934400376_H
#define ENUMERATOR_T2934400376_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<System.Object>
struct  Enumerator_t2934400376 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t1174585319 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	RuntimeObject * ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_t2934400376, ___l_0)); }
	inline List_1_t1174585319 * get_l_0() const { return ___l_0; }
	inline List_1_t1174585319 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t1174585319 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t2934400376, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t2934400376, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t2934400376, ___current_3)); }
	inline RuntimeObject * get_current_3() const { return ___current_3; }
	inline RuntimeObject ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(RuntimeObject * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T2934400376_H
#ifndef KEYVALUEPAIR_2_T3602437993_H
#define KEYVALUEPAIR_2_T3602437993_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
struct  KeyValuePair_2_t3602437993 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	RuntimeObject * ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	RuntimeObject * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t3602437993, ___key_0)); }
	inline RuntimeObject * get_key_0() const { return ___key_0; }
	inline RuntimeObject ** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(RuntimeObject * value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t3602437993, ___value_1)); }
	inline RuntimeObject * get_value_1() const { return ___value_1; }
	inline RuntimeObject ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(RuntimeObject * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T3602437993_H
#ifndef RANGE_T1266129112_H
#define RANGE_T1266129112_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SocialPlatforms.Range
struct  Range_t1266129112 
{
public:
	// System.Int32 UnityEngine.SocialPlatforms.Range::from
	int32_t ___from_0;
	// System.Int32 UnityEngine.SocialPlatforms.Range::count
	int32_t ___count_1;

public:
	inline static int32_t get_offset_of_from_0() { return static_cast<int32_t>(offsetof(Range_t1266129112, ___from_0)); }
	inline int32_t get_from_0() const { return ___from_0; }
	inline int32_t* get_address_of_from_0() { return &___from_0; }
	inline void set_from_0(int32_t value)
	{
		___from_0 = value;
	}

	inline static int32_t get_offset_of_count_1() { return static_cast<int32_t>(offsetof(Range_t1266129112, ___count_1)); }
	inline int32_t get_count_1() const { return ___count_1; }
	inline int32_t* get_address_of_count_1() { return &___count_1; }
	inline void set_count_1(int32_t value)
	{
		___count_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RANGE_T1266129112_H
#ifndef GCSCOREDATA_T3955401213_H
#define GCSCOREDATA_T3955401213_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SocialPlatforms.GameCenter.GcScoreData
struct  GcScoreData_t3955401213 
{
public:
	// System.String UnityEngine.SocialPlatforms.GameCenter.GcScoreData::m_Category
	String_t* ___m_Category_0;
	// System.UInt32 UnityEngine.SocialPlatforms.GameCenter.GcScoreData::m_ValueLow
	uint32_t ___m_ValueLow_1;
	// System.Int32 UnityEngine.SocialPlatforms.GameCenter.GcScoreData::m_ValueHigh
	int32_t ___m_ValueHigh_2;
	// System.Int32 UnityEngine.SocialPlatforms.GameCenter.GcScoreData::m_Date
	int32_t ___m_Date_3;
	// System.String UnityEngine.SocialPlatforms.GameCenter.GcScoreData::m_FormattedValue
	String_t* ___m_FormattedValue_4;
	// System.String UnityEngine.SocialPlatforms.GameCenter.GcScoreData::m_PlayerID
	String_t* ___m_PlayerID_5;
	// System.Int32 UnityEngine.SocialPlatforms.GameCenter.GcScoreData::m_Rank
	int32_t ___m_Rank_6;

public:
	inline static int32_t get_offset_of_m_Category_0() { return static_cast<int32_t>(offsetof(GcScoreData_t3955401213, ___m_Category_0)); }
	inline String_t* get_m_Category_0() const { return ___m_Category_0; }
	inline String_t** get_address_of_m_Category_0() { return &___m_Category_0; }
	inline void set_m_Category_0(String_t* value)
	{
		___m_Category_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Category_0), value);
	}

	inline static int32_t get_offset_of_m_ValueLow_1() { return static_cast<int32_t>(offsetof(GcScoreData_t3955401213, ___m_ValueLow_1)); }
	inline uint32_t get_m_ValueLow_1() const { return ___m_ValueLow_1; }
	inline uint32_t* get_address_of_m_ValueLow_1() { return &___m_ValueLow_1; }
	inline void set_m_ValueLow_1(uint32_t value)
	{
		___m_ValueLow_1 = value;
	}

	inline static int32_t get_offset_of_m_ValueHigh_2() { return static_cast<int32_t>(offsetof(GcScoreData_t3955401213, ___m_ValueHigh_2)); }
	inline int32_t get_m_ValueHigh_2() const { return ___m_ValueHigh_2; }
	inline int32_t* get_address_of_m_ValueHigh_2() { return &___m_ValueHigh_2; }
	inline void set_m_ValueHigh_2(int32_t value)
	{
		___m_ValueHigh_2 = value;
	}

	inline static int32_t get_offset_of_m_Date_3() { return static_cast<int32_t>(offsetof(GcScoreData_t3955401213, ___m_Date_3)); }
	inline int32_t get_m_Date_3() const { return ___m_Date_3; }
	inline int32_t* get_address_of_m_Date_3() { return &___m_Date_3; }
	inline void set_m_Date_3(int32_t value)
	{
		___m_Date_3 = value;
	}

	inline static int32_t get_offset_of_m_FormattedValue_4() { return static_cast<int32_t>(offsetof(GcScoreData_t3955401213, ___m_FormattedValue_4)); }
	inline String_t* get_m_FormattedValue_4() const { return ___m_FormattedValue_4; }
	inline String_t** get_address_of_m_FormattedValue_4() { return &___m_FormattedValue_4; }
	inline void set_m_FormattedValue_4(String_t* value)
	{
		___m_FormattedValue_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_FormattedValue_4), value);
	}

	inline static int32_t get_offset_of_m_PlayerID_5() { return static_cast<int32_t>(offsetof(GcScoreData_t3955401213, ___m_PlayerID_5)); }
	inline String_t* get_m_PlayerID_5() const { return ___m_PlayerID_5; }
	inline String_t** get_address_of_m_PlayerID_5() { return &___m_PlayerID_5; }
	inline void set_m_PlayerID_5(String_t* value)
	{
		___m_PlayerID_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_PlayerID_5), value);
	}

	inline static int32_t get_offset_of_m_Rank_6() { return static_cast<int32_t>(offsetof(GcScoreData_t3955401213, ___m_Rank_6)); }
	inline int32_t get_m_Rank_6() const { return ___m_Rank_6; }
	inline int32_t* get_address_of_m_Rank_6() { return &___m_Rank_6; }
	inline void set_m_Rank_6(int32_t value)
	{
		___m_Rank_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.SocialPlatforms.GameCenter.GcScoreData
struct GcScoreData_t3955401213_marshaled_pinvoke
{
	char* ___m_Category_0;
	uint32_t ___m_ValueLow_1;
	int32_t ___m_ValueHigh_2;
	int32_t ___m_Date_3;
	char* ___m_FormattedValue_4;
	char* ___m_PlayerID_5;
	int32_t ___m_Rank_6;
};
// Native definition for COM marshalling of UnityEngine.SocialPlatforms.GameCenter.GcScoreData
struct GcScoreData_t3955401213_marshaled_com
{
	Il2CppChar* ___m_Category_0;
	uint32_t ___m_ValueLow_1;
	int32_t ___m_ValueHigh_2;
	int32_t ___m_Date_3;
	Il2CppChar* ___m_FormattedValue_4;
	Il2CppChar* ___m_PlayerID_5;
	int32_t ___m_Rank_6;
};
#endif // GCSCOREDATA_T3955401213_H
#ifndef WORKREQUEST_T2547429811_H
#define WORKREQUEST_T2547429811_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UnitySynchronizationContext/WorkRequest
struct  WorkRequest_t2547429811 
{
public:
	// System.Threading.SendOrPostCallback UnityEngine.UnitySynchronizationContext/WorkRequest::m_DelagateCallback
	SendOrPostCallback_t2649904830 * ___m_DelagateCallback_0;
	// System.Object UnityEngine.UnitySynchronizationContext/WorkRequest::m_DelagateState
	RuntimeObject * ___m_DelagateState_1;
	// System.Threading.ManualResetEvent UnityEngine.UnitySynchronizationContext/WorkRequest::m_WaitHandle
	ManualResetEvent_t3770519771 * ___m_WaitHandle_2;

public:
	inline static int32_t get_offset_of_m_DelagateCallback_0() { return static_cast<int32_t>(offsetof(WorkRequest_t2547429811, ___m_DelagateCallback_0)); }
	inline SendOrPostCallback_t2649904830 * get_m_DelagateCallback_0() const { return ___m_DelagateCallback_0; }
	inline SendOrPostCallback_t2649904830 ** get_address_of_m_DelagateCallback_0() { return &___m_DelagateCallback_0; }
	inline void set_m_DelagateCallback_0(SendOrPostCallback_t2649904830 * value)
	{
		___m_DelagateCallback_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_DelagateCallback_0), value);
	}

	inline static int32_t get_offset_of_m_DelagateState_1() { return static_cast<int32_t>(offsetof(WorkRequest_t2547429811, ___m_DelagateState_1)); }
	inline RuntimeObject * get_m_DelagateState_1() const { return ___m_DelagateState_1; }
	inline RuntimeObject ** get_address_of_m_DelagateState_1() { return &___m_DelagateState_1; }
	inline void set_m_DelagateState_1(RuntimeObject * value)
	{
		___m_DelagateState_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_DelagateState_1), value);
	}

	inline static int32_t get_offset_of_m_WaitHandle_2() { return static_cast<int32_t>(offsetof(WorkRequest_t2547429811, ___m_WaitHandle_2)); }
	inline ManualResetEvent_t3770519771 * get_m_WaitHandle_2() const { return ___m_WaitHandle_2; }
	inline ManualResetEvent_t3770519771 ** get_address_of_m_WaitHandle_2() { return &___m_WaitHandle_2; }
	inline void set_m_WaitHandle_2(ManualResetEvent_t3770519771 * value)
	{
		___m_WaitHandle_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_WaitHandle_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.UnitySynchronizationContext/WorkRequest
struct WorkRequest_t2547429811_marshaled_pinvoke
{
	Il2CppMethodPointer ___m_DelagateCallback_0;
	Il2CppIUnknown* ___m_DelagateState_1;
	ManualResetEvent_t3770519771 * ___m_WaitHandle_2;
};
// Native definition for COM marshalling of UnityEngine.UnitySynchronizationContext/WorkRequest
struct WorkRequest_t2547429811_marshaled_com
{
	Il2CppMethodPointer ___m_DelagateCallback_0;
	Il2CppIUnknown* ___m_DelagateState_1;
	ManualResetEvent_t3770519771 * ___m_WaitHandle_2;
};
#endif // WORKREQUEST_T2547429811_H
#ifndef RESOURCEINFO_T1773833009_H
#define RESOURCEINFO_T1773833009_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Resources.ResourceReader/ResourceInfo
struct  ResourceInfo_t1773833009 
{
public:
	// System.Int64 System.Resources.ResourceReader/ResourceInfo::ValuePosition
	int64_t ___ValuePosition_0;
	// System.String System.Resources.ResourceReader/ResourceInfo::ResourceName
	String_t* ___ResourceName_1;
	// System.Int32 System.Resources.ResourceReader/ResourceInfo::TypeIndex
	int32_t ___TypeIndex_2;

public:
	inline static int32_t get_offset_of_ValuePosition_0() { return static_cast<int32_t>(offsetof(ResourceInfo_t1773833009, ___ValuePosition_0)); }
	inline int64_t get_ValuePosition_0() const { return ___ValuePosition_0; }
	inline int64_t* get_address_of_ValuePosition_0() { return &___ValuePosition_0; }
	inline void set_ValuePosition_0(int64_t value)
	{
		___ValuePosition_0 = value;
	}

	inline static int32_t get_offset_of_ResourceName_1() { return static_cast<int32_t>(offsetof(ResourceInfo_t1773833009, ___ResourceName_1)); }
	inline String_t* get_ResourceName_1() const { return ___ResourceName_1; }
	inline String_t** get_address_of_ResourceName_1() { return &___ResourceName_1; }
	inline void set_ResourceName_1(String_t* value)
	{
		___ResourceName_1 = value;
		Il2CppCodeGenWriteBarrier((&___ResourceName_1), value);
	}

	inline static int32_t get_offset_of_TypeIndex_2() { return static_cast<int32_t>(offsetof(ResourceInfo_t1773833009, ___TypeIndex_2)); }
	inline int32_t get_TypeIndex_2() const { return ___TypeIndex_2; }
	inline int32_t* get_address_of_TypeIndex_2() { return &___TypeIndex_2; }
	inline void set_TypeIndex_2(int32_t value)
	{
		___TypeIndex_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Resources.ResourceReader/ResourceInfo
struct ResourceInfo_t1773833009_marshaled_pinvoke
{
	int64_t ___ValuePosition_0;
	char* ___ResourceName_1;
	int32_t ___TypeIndex_2;
};
// Native definition for COM marshalling of System.Resources.ResourceReader/ResourceInfo
struct ResourceInfo_t1773833009_marshaled_com
{
	int64_t ___ValuePosition_0;
	Il2CppChar* ___ResourceName_1;
	int32_t ___TypeIndex_2;
};
#endif // RESOURCEINFO_T1773833009_H
#ifndef SYSTEMEXCEPTION_T3635077273_H
#define SYSTEMEXCEPTION_T3635077273_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t3635077273  : public Exception_t3361176243
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T3635077273_H
#ifndef UINT64_T2265270229_H
#define UINT64_T2265270229_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UInt64
struct  UInt64_t2265270229 
{
public:
	// System.UInt64 System.UInt64::m_value
	uint64_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(UInt64_t2265270229, ___m_value_0)); }
	inline uint64_t get_m_value_0() const { return ___m_value_0; }
	inline uint64_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint64_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINT64_T2265270229_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef UINT32_T2739056616_H
#define UINT32_T2739056616_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UInt32
struct  UInt32_t2739056616 
{
public:
	// System.UInt32 System.UInt32::m_value
	uint32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(UInt32_t2739056616, ___m_value_0)); }
	inline uint32_t get_m_value_0() const { return ___m_value_0; }
	inline uint32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINT32_T2739056616_H
#ifndef UINT16_T11709673_H
#define UINT16_T11709673_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UInt16
struct  UInt16_t11709673 
{
public:
	// System.UInt16 System.UInt16::m_value
	uint16_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(UInt16_t11709673, ___m_value_2)); }
	inline uint16_t get_m_value_2() const { return ___m_value_2; }
	inline uint16_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(uint16_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINT16_T11709673_H
#ifndef SINGLE_T496865882_H
#define SINGLE_T496865882_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t496865882 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t496865882, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T496865882_H
#ifndef INT64_T384086013_H
#define INT64_T384086013_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int64
struct  Int64_t384086013 
{
public:
	// System.Int64 System.Int64::m_value
	int64_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int64_t384086013, ___m_value_0)); }
	inline int64_t get_m_value_0() const { return ___m_value_0; }
	inline int64_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int64_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT64_T384086013_H
#ifndef INT16_T769030278_H
#define INT16_T769030278_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int16
struct  Int16_t769030278 
{
public:
	// System.Int16 System.Int16::m_value
	int16_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int16_t769030278, ___m_value_0)); }
	inline int16_t get_m_value_0() const { return ___m_value_0; }
	inline int16_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int16_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT16_T769030278_H
#ifndef DOUBLE_T1454265465_H
#define DOUBLE_T1454265465_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Double
struct  Double_t1454265465 
{
public:
	// System.Double System.Double::m_value
	double ___m_value_13;

public:
	inline static int32_t get_offset_of_m_value_13() { return static_cast<int32_t>(offsetof(Double_t1454265465, ___m_value_13)); }
	inline double get_m_value_13() const { return ___m_value_13; }
	inline double* get_address_of_m_value_13() { return &___m_value_13; }
	inline void set_m_value_13(double value)
	{
		___m_value_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOUBLE_T1454265465_H
#ifndef METHODTOKEN_T1430837147_H
#define METHODTOKEN_T1430837147_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.Emit.MethodToken
struct  MethodToken_t1430837147 
{
public:
	// System.Int32 System.Reflection.Emit.MethodToken::tokValue
	int32_t ___tokValue_0;

public:
	inline static int32_t get_offset_of_tokValue_0() { return static_cast<int32_t>(offsetof(MethodToken_t1430837147, ___tokValue_0)); }
	inline int32_t get_tokValue_0() const { return ___tokValue_0; }
	inline int32_t* get_address_of_tokValue_0() { return &___tokValue_0; }
	inline void set_tokValue_0(int32_t value)
	{
		___tokValue_0 = value;
	}
};

struct MethodToken_t1430837147_StaticFields
{
public:
	// System.Reflection.Emit.MethodToken System.Reflection.Emit.MethodToken::Empty
	MethodToken_t1430837147  ___Empty_1;

public:
	inline static int32_t get_offset_of_Empty_1() { return static_cast<int32_t>(offsetof(MethodToken_t1430837147_StaticFields, ___Empty_1)); }
	inline MethodToken_t1430837147  get_Empty_1() const { return ___Empty_1; }
	inline MethodToken_t1430837147 * get_address_of_Empty_1() { return &___Empty_1; }
	inline void set_Empty_1(MethodToken_t1430837147  value)
	{
		___Empty_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // METHODTOKEN_T1430837147_H
#ifndef DECIMAL_T2656901032_H
#define DECIMAL_T2656901032_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Decimal
struct  Decimal_t2656901032 
{
public:
	// System.UInt32 System.Decimal::flags
	uint32_t ___flags_5;
	// System.UInt32 System.Decimal::hi
	uint32_t ___hi_6;
	// System.UInt32 System.Decimal::lo
	uint32_t ___lo_7;
	// System.UInt32 System.Decimal::mid
	uint32_t ___mid_8;

public:
	inline static int32_t get_offset_of_flags_5() { return static_cast<int32_t>(offsetof(Decimal_t2656901032, ___flags_5)); }
	inline uint32_t get_flags_5() const { return ___flags_5; }
	inline uint32_t* get_address_of_flags_5() { return &___flags_5; }
	inline void set_flags_5(uint32_t value)
	{
		___flags_5 = value;
	}

	inline static int32_t get_offset_of_hi_6() { return static_cast<int32_t>(offsetof(Decimal_t2656901032, ___hi_6)); }
	inline uint32_t get_hi_6() const { return ___hi_6; }
	inline uint32_t* get_address_of_hi_6() { return &___hi_6; }
	inline void set_hi_6(uint32_t value)
	{
		___hi_6 = value;
	}

	inline static int32_t get_offset_of_lo_7() { return static_cast<int32_t>(offsetof(Decimal_t2656901032, ___lo_7)); }
	inline uint32_t get_lo_7() const { return ___lo_7; }
	inline uint32_t* get_address_of_lo_7() { return &___lo_7; }
	inline void set_lo_7(uint32_t value)
	{
		___lo_7 = value;
	}

	inline static int32_t get_offset_of_mid_8() { return static_cast<int32_t>(offsetof(Decimal_t2656901032, ___mid_8)); }
	inline uint32_t get_mid_8() const { return ___mid_8; }
	inline uint32_t* get_address_of_mid_8() { return &___mid_8; }
	inline void set_mid_8(uint32_t value)
	{
		___mid_8 = value;
	}
};

struct Decimal_t2656901032_StaticFields
{
public:
	// System.Decimal System.Decimal::MinValue
	Decimal_t2656901032  ___MinValue_0;
	// System.Decimal System.Decimal::MaxValue
	Decimal_t2656901032  ___MaxValue_1;
	// System.Decimal System.Decimal::MinusOne
	Decimal_t2656901032  ___MinusOne_2;
	// System.Decimal System.Decimal::One
	Decimal_t2656901032  ___One_3;
	// System.Decimal System.Decimal::MaxValueDiv10
	Decimal_t2656901032  ___MaxValueDiv10_4;

public:
	inline static int32_t get_offset_of_MinValue_0() { return static_cast<int32_t>(offsetof(Decimal_t2656901032_StaticFields, ___MinValue_0)); }
	inline Decimal_t2656901032  get_MinValue_0() const { return ___MinValue_0; }
	inline Decimal_t2656901032 * get_address_of_MinValue_0() { return &___MinValue_0; }
	inline void set_MinValue_0(Decimal_t2656901032  value)
	{
		___MinValue_0 = value;
	}

	inline static int32_t get_offset_of_MaxValue_1() { return static_cast<int32_t>(offsetof(Decimal_t2656901032_StaticFields, ___MaxValue_1)); }
	inline Decimal_t2656901032  get_MaxValue_1() const { return ___MaxValue_1; }
	inline Decimal_t2656901032 * get_address_of_MaxValue_1() { return &___MaxValue_1; }
	inline void set_MaxValue_1(Decimal_t2656901032  value)
	{
		___MaxValue_1 = value;
	}

	inline static int32_t get_offset_of_MinusOne_2() { return static_cast<int32_t>(offsetof(Decimal_t2656901032_StaticFields, ___MinusOne_2)); }
	inline Decimal_t2656901032  get_MinusOne_2() const { return ___MinusOne_2; }
	inline Decimal_t2656901032 * get_address_of_MinusOne_2() { return &___MinusOne_2; }
	inline void set_MinusOne_2(Decimal_t2656901032  value)
	{
		___MinusOne_2 = value;
	}

	inline static int32_t get_offset_of_One_3() { return static_cast<int32_t>(offsetof(Decimal_t2656901032_StaticFields, ___One_3)); }
	inline Decimal_t2656901032  get_One_3() const { return ___One_3; }
	inline Decimal_t2656901032 * get_address_of_One_3() { return &___One_3; }
	inline void set_One_3(Decimal_t2656901032  value)
	{
		___One_3 = value;
	}

	inline static int32_t get_offset_of_MaxValueDiv10_4() { return static_cast<int32_t>(offsetof(Decimal_t2656901032_StaticFields, ___MaxValueDiv10_4)); }
	inline Decimal_t2656901032  get_MaxValueDiv10_4() const { return ___MaxValueDiv10_4; }
	inline Decimal_t2656901032 * get_address_of_MaxValueDiv10_4() { return &___MaxValueDiv10_4; }
	inline void set_MaxValueDiv10_4(Decimal_t2656901032  value)
	{
		___MaxValueDiv10_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DECIMAL_T2656901032_H
#ifndef CHAR_T3572527572_H
#define CHAR_T3572527572_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Char
struct  Char_t3572527572 
{
public:
	// System.Char System.Char::m_value
	Il2CppChar ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Char_t3572527572, ___m_value_2)); }
	inline Il2CppChar get_m_value_2() const { return ___m_value_2; }
	inline Il2CppChar* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(Il2CppChar value)
	{
		___m_value_2 = value;
	}
};

struct Char_t3572527572_StaticFields
{
public:
	// System.Byte* System.Char::category_data
	uint8_t* ___category_data_3;
	// System.Byte* System.Char::numeric_data
	uint8_t* ___numeric_data_4;
	// System.Double* System.Char::numeric_data_values
	double* ___numeric_data_values_5;
	// System.UInt16* System.Char::to_lower_data_low
	uint16_t* ___to_lower_data_low_6;
	// System.UInt16* System.Char::to_lower_data_high
	uint16_t* ___to_lower_data_high_7;
	// System.UInt16* System.Char::to_upper_data_low
	uint16_t* ___to_upper_data_low_8;
	// System.UInt16* System.Char::to_upper_data_high
	uint16_t* ___to_upper_data_high_9;

public:
	inline static int32_t get_offset_of_category_data_3() { return static_cast<int32_t>(offsetof(Char_t3572527572_StaticFields, ___category_data_3)); }
	inline uint8_t* get_category_data_3() const { return ___category_data_3; }
	inline uint8_t** get_address_of_category_data_3() { return &___category_data_3; }
	inline void set_category_data_3(uint8_t* value)
	{
		___category_data_3 = value;
	}

	inline static int32_t get_offset_of_numeric_data_4() { return static_cast<int32_t>(offsetof(Char_t3572527572_StaticFields, ___numeric_data_4)); }
	inline uint8_t* get_numeric_data_4() const { return ___numeric_data_4; }
	inline uint8_t** get_address_of_numeric_data_4() { return &___numeric_data_4; }
	inline void set_numeric_data_4(uint8_t* value)
	{
		___numeric_data_4 = value;
	}

	inline static int32_t get_offset_of_numeric_data_values_5() { return static_cast<int32_t>(offsetof(Char_t3572527572_StaticFields, ___numeric_data_values_5)); }
	inline double* get_numeric_data_values_5() const { return ___numeric_data_values_5; }
	inline double** get_address_of_numeric_data_values_5() { return &___numeric_data_values_5; }
	inline void set_numeric_data_values_5(double* value)
	{
		___numeric_data_values_5 = value;
	}

	inline static int32_t get_offset_of_to_lower_data_low_6() { return static_cast<int32_t>(offsetof(Char_t3572527572_StaticFields, ___to_lower_data_low_6)); }
	inline uint16_t* get_to_lower_data_low_6() const { return ___to_lower_data_low_6; }
	inline uint16_t** get_address_of_to_lower_data_low_6() { return &___to_lower_data_low_6; }
	inline void set_to_lower_data_low_6(uint16_t* value)
	{
		___to_lower_data_low_6 = value;
	}

	inline static int32_t get_offset_of_to_lower_data_high_7() { return static_cast<int32_t>(offsetof(Char_t3572527572_StaticFields, ___to_lower_data_high_7)); }
	inline uint16_t* get_to_lower_data_high_7() const { return ___to_lower_data_high_7; }
	inline uint16_t** get_address_of_to_lower_data_high_7() { return &___to_lower_data_high_7; }
	inline void set_to_lower_data_high_7(uint16_t* value)
	{
		___to_lower_data_high_7 = value;
	}

	inline static int32_t get_offset_of_to_upper_data_low_8() { return static_cast<int32_t>(offsetof(Char_t3572527572_StaticFields, ___to_upper_data_low_8)); }
	inline uint16_t* get_to_upper_data_low_8() const { return ___to_upper_data_low_8; }
	inline uint16_t** get_address_of_to_upper_data_low_8() { return &___to_upper_data_low_8; }
	inline void set_to_upper_data_low_8(uint16_t* value)
	{
		___to_upper_data_low_8 = value;
	}

	inline static int32_t get_offset_of_to_upper_data_high_9() { return static_cast<int32_t>(offsetof(Char_t3572527572_StaticFields, ___to_upper_data_high_9)); }
	inline uint16_t* get_to_upper_data_high_9() const { return ___to_upper_data_high_9; }
	inline uint16_t** get_address_of_to_upper_data_high_9() { return &___to_upper_data_high_9; }
	inline void set_to_upper_data_high_9(uint16_t* value)
	{
		___to_upper_data_high_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHAR_T3572527572_H
#ifndef DSAPARAMETERS_T2781005375_H
#define DSAPARAMETERS_T2781005375_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.DSAParameters
struct  DSAParameters_t2781005375 
{
public:
	// System.Int32 System.Security.Cryptography.DSAParameters::Counter
	int32_t ___Counter_0;
	// System.Byte[] System.Security.Cryptography.DSAParameters::G
	ByteU5BU5D_t1014014593* ___G_1;
	// System.Byte[] System.Security.Cryptography.DSAParameters::J
	ByteU5BU5D_t1014014593* ___J_2;
	// System.Byte[] System.Security.Cryptography.DSAParameters::P
	ByteU5BU5D_t1014014593* ___P_3;
	// System.Byte[] System.Security.Cryptography.DSAParameters::Q
	ByteU5BU5D_t1014014593* ___Q_4;
	// System.Byte[] System.Security.Cryptography.DSAParameters::Seed
	ByteU5BU5D_t1014014593* ___Seed_5;
	// System.Byte[] System.Security.Cryptography.DSAParameters::X
	ByteU5BU5D_t1014014593* ___X_6;
	// System.Byte[] System.Security.Cryptography.DSAParameters::Y
	ByteU5BU5D_t1014014593* ___Y_7;

public:
	inline static int32_t get_offset_of_Counter_0() { return static_cast<int32_t>(offsetof(DSAParameters_t2781005375, ___Counter_0)); }
	inline int32_t get_Counter_0() const { return ___Counter_0; }
	inline int32_t* get_address_of_Counter_0() { return &___Counter_0; }
	inline void set_Counter_0(int32_t value)
	{
		___Counter_0 = value;
	}

	inline static int32_t get_offset_of_G_1() { return static_cast<int32_t>(offsetof(DSAParameters_t2781005375, ___G_1)); }
	inline ByteU5BU5D_t1014014593* get_G_1() const { return ___G_1; }
	inline ByteU5BU5D_t1014014593** get_address_of_G_1() { return &___G_1; }
	inline void set_G_1(ByteU5BU5D_t1014014593* value)
	{
		___G_1 = value;
		Il2CppCodeGenWriteBarrier((&___G_1), value);
	}

	inline static int32_t get_offset_of_J_2() { return static_cast<int32_t>(offsetof(DSAParameters_t2781005375, ___J_2)); }
	inline ByteU5BU5D_t1014014593* get_J_2() const { return ___J_2; }
	inline ByteU5BU5D_t1014014593** get_address_of_J_2() { return &___J_2; }
	inline void set_J_2(ByteU5BU5D_t1014014593* value)
	{
		___J_2 = value;
		Il2CppCodeGenWriteBarrier((&___J_2), value);
	}

	inline static int32_t get_offset_of_P_3() { return static_cast<int32_t>(offsetof(DSAParameters_t2781005375, ___P_3)); }
	inline ByteU5BU5D_t1014014593* get_P_3() const { return ___P_3; }
	inline ByteU5BU5D_t1014014593** get_address_of_P_3() { return &___P_3; }
	inline void set_P_3(ByteU5BU5D_t1014014593* value)
	{
		___P_3 = value;
		Il2CppCodeGenWriteBarrier((&___P_3), value);
	}

	inline static int32_t get_offset_of_Q_4() { return static_cast<int32_t>(offsetof(DSAParameters_t2781005375, ___Q_4)); }
	inline ByteU5BU5D_t1014014593* get_Q_4() const { return ___Q_4; }
	inline ByteU5BU5D_t1014014593** get_address_of_Q_4() { return &___Q_4; }
	inline void set_Q_4(ByteU5BU5D_t1014014593* value)
	{
		___Q_4 = value;
		Il2CppCodeGenWriteBarrier((&___Q_4), value);
	}

	inline static int32_t get_offset_of_Seed_5() { return static_cast<int32_t>(offsetof(DSAParameters_t2781005375, ___Seed_5)); }
	inline ByteU5BU5D_t1014014593* get_Seed_5() const { return ___Seed_5; }
	inline ByteU5BU5D_t1014014593** get_address_of_Seed_5() { return &___Seed_5; }
	inline void set_Seed_5(ByteU5BU5D_t1014014593* value)
	{
		___Seed_5 = value;
		Il2CppCodeGenWriteBarrier((&___Seed_5), value);
	}

	inline static int32_t get_offset_of_X_6() { return static_cast<int32_t>(offsetof(DSAParameters_t2781005375, ___X_6)); }
	inline ByteU5BU5D_t1014014593* get_X_6() const { return ___X_6; }
	inline ByteU5BU5D_t1014014593** get_address_of_X_6() { return &___X_6; }
	inline void set_X_6(ByteU5BU5D_t1014014593* value)
	{
		___X_6 = value;
		Il2CppCodeGenWriteBarrier((&___X_6), value);
	}

	inline static int32_t get_offset_of_Y_7() { return static_cast<int32_t>(offsetof(DSAParameters_t2781005375, ___Y_7)); }
	inline ByteU5BU5D_t1014014593* get_Y_7() const { return ___Y_7; }
	inline ByteU5BU5D_t1014014593** get_address_of_Y_7() { return &___Y_7; }
	inline void set_Y_7(ByteU5BU5D_t1014014593* value)
	{
		___Y_7 = value;
		Il2CppCodeGenWriteBarrier((&___Y_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Security.Cryptography.DSAParameters
struct DSAParameters_t2781005375_marshaled_pinvoke
{
	int32_t ___Counter_0;
	uint8_t* ___G_1;
	uint8_t* ___J_2;
	uint8_t* ___P_3;
	uint8_t* ___Q_4;
	uint8_t* ___Seed_5;
	uint8_t* ___X_6;
	uint8_t* ___Y_7;
};
// Native definition for COM marshalling of System.Security.Cryptography.DSAParameters
struct DSAParameters_t2781005375_marshaled_com
{
	int32_t ___Counter_0;
	uint8_t* ___G_1;
	uint8_t* ___J_2;
	uint8_t* ___P_3;
	uint8_t* ___Q_4;
	uint8_t* ___Seed_5;
	uint8_t* ___X_6;
	uint8_t* ___Y_7;
};
#endif // DSAPARAMETERS_T2781005375_H
#ifndef BYTE_T880488320_H
#define BYTE_T880488320_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Byte
struct  Byte_t880488320 
{
public:
	// System.Byte System.Byte::m_value
	uint8_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Byte_t880488320, ___m_value_2)); }
	inline uint8_t get_m_value_2() const { return ___m_value_2; }
	inline uint8_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(uint8_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BYTE_T880488320_H
#ifndef OPCODE_T376486760_H
#define OPCODE_T376486760_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.Emit.OpCode
struct  OpCode_t376486760 
{
public:
	// System.Byte System.Reflection.Emit.OpCode::op1
	uint8_t ___op1_0;
	// System.Byte System.Reflection.Emit.OpCode::op2
	uint8_t ___op2_1;
	// System.Byte System.Reflection.Emit.OpCode::push
	uint8_t ___push_2;
	// System.Byte System.Reflection.Emit.OpCode::pop
	uint8_t ___pop_3;
	// System.Byte System.Reflection.Emit.OpCode::size
	uint8_t ___size_4;
	// System.Byte System.Reflection.Emit.OpCode::type
	uint8_t ___type_5;
	// System.Byte System.Reflection.Emit.OpCode::args
	uint8_t ___args_6;
	// System.Byte System.Reflection.Emit.OpCode::flow
	uint8_t ___flow_7;

public:
	inline static int32_t get_offset_of_op1_0() { return static_cast<int32_t>(offsetof(OpCode_t376486760, ___op1_0)); }
	inline uint8_t get_op1_0() const { return ___op1_0; }
	inline uint8_t* get_address_of_op1_0() { return &___op1_0; }
	inline void set_op1_0(uint8_t value)
	{
		___op1_0 = value;
	}

	inline static int32_t get_offset_of_op2_1() { return static_cast<int32_t>(offsetof(OpCode_t376486760, ___op2_1)); }
	inline uint8_t get_op2_1() const { return ___op2_1; }
	inline uint8_t* get_address_of_op2_1() { return &___op2_1; }
	inline void set_op2_1(uint8_t value)
	{
		___op2_1 = value;
	}

	inline static int32_t get_offset_of_push_2() { return static_cast<int32_t>(offsetof(OpCode_t376486760, ___push_2)); }
	inline uint8_t get_push_2() const { return ___push_2; }
	inline uint8_t* get_address_of_push_2() { return &___push_2; }
	inline void set_push_2(uint8_t value)
	{
		___push_2 = value;
	}

	inline static int32_t get_offset_of_pop_3() { return static_cast<int32_t>(offsetof(OpCode_t376486760, ___pop_3)); }
	inline uint8_t get_pop_3() const { return ___pop_3; }
	inline uint8_t* get_address_of_pop_3() { return &___pop_3; }
	inline void set_pop_3(uint8_t value)
	{
		___pop_3 = value;
	}

	inline static int32_t get_offset_of_size_4() { return static_cast<int32_t>(offsetof(OpCode_t376486760, ___size_4)); }
	inline uint8_t get_size_4() const { return ___size_4; }
	inline uint8_t* get_address_of_size_4() { return &___size_4; }
	inline void set_size_4(uint8_t value)
	{
		___size_4 = value;
	}

	inline static int32_t get_offset_of_type_5() { return static_cast<int32_t>(offsetof(OpCode_t376486760, ___type_5)); }
	inline uint8_t get_type_5() const { return ___type_5; }
	inline uint8_t* get_address_of_type_5() { return &___type_5; }
	inline void set_type_5(uint8_t value)
	{
		___type_5 = value;
	}

	inline static int32_t get_offset_of_args_6() { return static_cast<int32_t>(offsetof(OpCode_t376486760, ___args_6)); }
	inline uint8_t get_args_6() const { return ___args_6; }
	inline uint8_t* get_address_of_args_6() { return &___args_6; }
	inline void set_args_6(uint8_t value)
	{
		___args_6 = value;
	}

	inline static int32_t get_offset_of_flow_7() { return static_cast<int32_t>(offsetof(OpCode_t376486760, ___flow_7)); }
	inline uint8_t get_flow_7() const { return ___flow_7; }
	inline uint8_t* get_address_of_flow_7() { return &___flow_7; }
	inline void set_flow_7(uint8_t value)
	{
		___flow_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPCODE_T376486760_H
#ifndef SBYTE_T2530277047_H
#define SBYTE_T2530277047_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SByte
struct  SByte_t2530277047 
{
public:
	// System.SByte System.SByte::m_value
	int8_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(SByte_t2530277047, ___m_value_0)); }
	inline int8_t get_m_value_0() const { return ___m_value_0; }
	inline int8_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int8_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SBYTE_T2530277047_H
#ifndef INT32_T2571135199_H
#define INT32_T2571135199_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2571135199 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t2571135199, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2571135199_H
#ifndef TIMESPAN_T2986675223_H
#define TIMESPAN_T2986675223_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TimeSpan
struct  TimeSpan_t2986675223 
{
public:
	// System.Int64 System.TimeSpan::_ticks
	int64_t ____ticks_3;

public:
	inline static int32_t get_offset_of__ticks_3() { return static_cast<int32_t>(offsetof(TimeSpan_t2986675223, ____ticks_3)); }
	inline int64_t get__ticks_3() const { return ____ticks_3; }
	inline int64_t* get_address_of__ticks_3() { return &____ticks_3; }
	inline void set__ticks_3(int64_t value)
	{
		____ticks_3 = value;
	}
};

struct TimeSpan_t2986675223_StaticFields
{
public:
	// System.TimeSpan System.TimeSpan::MaxValue
	TimeSpan_t2986675223  ___MaxValue_0;
	// System.TimeSpan System.TimeSpan::MinValue
	TimeSpan_t2986675223  ___MinValue_1;
	// System.TimeSpan System.TimeSpan::Zero
	TimeSpan_t2986675223  ___Zero_2;

public:
	inline static int32_t get_offset_of_MaxValue_0() { return static_cast<int32_t>(offsetof(TimeSpan_t2986675223_StaticFields, ___MaxValue_0)); }
	inline TimeSpan_t2986675223  get_MaxValue_0() const { return ___MaxValue_0; }
	inline TimeSpan_t2986675223 * get_address_of_MaxValue_0() { return &___MaxValue_0; }
	inline void set_MaxValue_0(TimeSpan_t2986675223  value)
	{
		___MaxValue_0 = value;
	}

	inline static int32_t get_offset_of_MinValue_1() { return static_cast<int32_t>(offsetof(TimeSpan_t2986675223_StaticFields, ___MinValue_1)); }
	inline TimeSpan_t2986675223  get_MinValue_1() const { return ___MinValue_1; }
	inline TimeSpan_t2986675223 * get_address_of_MinValue_1() { return &___MinValue_1; }
	inline void set_MinValue_1(TimeSpan_t2986675223  value)
	{
		___MinValue_1 = value;
	}

	inline static int32_t get_offset_of_Zero_2() { return static_cast<int32_t>(offsetof(TimeSpan_t2986675223_StaticFields, ___Zero_2)); }
	inline TimeSpan_t2986675223  get_Zero_2() const { return ___Zero_2; }
	inline TimeSpan_t2986675223 * get_address_of_Zero_2() { return &___Zero_2; }
	inline void set_Zero_2(TimeSpan_t2986675223  value)
	{
		___Zero_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESPAN_T2986675223_H
#ifndef BOOLEAN_T2059672899_H
#define BOOLEAN_T2059672899_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t2059672899 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t2059672899, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t2059672899_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t2059672899_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t2059672899_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T2059672899_H
#ifndef DICTIONARYENTRY_T1385909157_H
#define DICTIONARYENTRY_T1385909157_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.DictionaryEntry
struct  DictionaryEntry_t1385909157 
{
public:
	// System.Object System.Collections.DictionaryEntry::_key
	RuntimeObject * ____key_0;
	// System.Object System.Collections.DictionaryEntry::_value
	RuntimeObject * ____value_1;

public:
	inline static int32_t get_offset_of__key_0() { return static_cast<int32_t>(offsetof(DictionaryEntry_t1385909157, ____key_0)); }
	inline RuntimeObject * get__key_0() const { return ____key_0; }
	inline RuntimeObject ** get_address_of__key_0() { return &____key_0; }
	inline void set__key_0(RuntimeObject * value)
	{
		____key_0 = value;
		Il2CppCodeGenWriteBarrier((&____key_0), value);
	}

	inline static int32_t get_offset_of__value_1() { return static_cast<int32_t>(offsetof(DictionaryEntry_t1385909157, ____value_1)); }
	inline RuntimeObject * get__value_1() const { return ____value_1; }
	inline RuntimeObject ** get_address_of__value_1() { return &____value_1; }
	inline void set__value_1(RuntimeObject * value)
	{
		____value_1 = value;
		Il2CppCodeGenWriteBarrier((&____value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Collections.DictionaryEntry
struct DictionaryEntry_t1385909157_marshaled_pinvoke
{
	Il2CppIUnknown* ____key_0;
	Il2CppIUnknown* ____value_1;
};
// Native definition for COM marshalling of System.Collections.DictionaryEntry
struct DictionaryEntry_t1385909157_marshaled_com
{
	Il2CppIUnknown* ____key_0;
	Il2CppIUnknown* ____value_1;
};
#endif // DICTIONARYENTRY_T1385909157_H
#ifndef VOID_T1078098495_H
#define VOID_T1078098495_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1078098495 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1078098495_H
#ifndef RSAPARAMETERS_T2749614896_H
#define RSAPARAMETERS_T2749614896_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.RSAParameters
struct  RSAParameters_t2749614896 
{
public:
	// System.Byte[] System.Security.Cryptography.RSAParameters::P
	ByteU5BU5D_t1014014593* ___P_0;
	// System.Byte[] System.Security.Cryptography.RSAParameters::Q
	ByteU5BU5D_t1014014593* ___Q_1;
	// System.Byte[] System.Security.Cryptography.RSAParameters::D
	ByteU5BU5D_t1014014593* ___D_2;
	// System.Byte[] System.Security.Cryptography.RSAParameters::DP
	ByteU5BU5D_t1014014593* ___DP_3;
	// System.Byte[] System.Security.Cryptography.RSAParameters::DQ
	ByteU5BU5D_t1014014593* ___DQ_4;
	// System.Byte[] System.Security.Cryptography.RSAParameters::InverseQ
	ByteU5BU5D_t1014014593* ___InverseQ_5;
	// System.Byte[] System.Security.Cryptography.RSAParameters::Modulus
	ByteU5BU5D_t1014014593* ___Modulus_6;
	// System.Byte[] System.Security.Cryptography.RSAParameters::Exponent
	ByteU5BU5D_t1014014593* ___Exponent_7;

public:
	inline static int32_t get_offset_of_P_0() { return static_cast<int32_t>(offsetof(RSAParameters_t2749614896, ___P_0)); }
	inline ByteU5BU5D_t1014014593* get_P_0() const { return ___P_0; }
	inline ByteU5BU5D_t1014014593** get_address_of_P_0() { return &___P_0; }
	inline void set_P_0(ByteU5BU5D_t1014014593* value)
	{
		___P_0 = value;
		Il2CppCodeGenWriteBarrier((&___P_0), value);
	}

	inline static int32_t get_offset_of_Q_1() { return static_cast<int32_t>(offsetof(RSAParameters_t2749614896, ___Q_1)); }
	inline ByteU5BU5D_t1014014593* get_Q_1() const { return ___Q_1; }
	inline ByteU5BU5D_t1014014593** get_address_of_Q_1() { return &___Q_1; }
	inline void set_Q_1(ByteU5BU5D_t1014014593* value)
	{
		___Q_1 = value;
		Il2CppCodeGenWriteBarrier((&___Q_1), value);
	}

	inline static int32_t get_offset_of_D_2() { return static_cast<int32_t>(offsetof(RSAParameters_t2749614896, ___D_2)); }
	inline ByteU5BU5D_t1014014593* get_D_2() const { return ___D_2; }
	inline ByteU5BU5D_t1014014593** get_address_of_D_2() { return &___D_2; }
	inline void set_D_2(ByteU5BU5D_t1014014593* value)
	{
		___D_2 = value;
		Il2CppCodeGenWriteBarrier((&___D_2), value);
	}

	inline static int32_t get_offset_of_DP_3() { return static_cast<int32_t>(offsetof(RSAParameters_t2749614896, ___DP_3)); }
	inline ByteU5BU5D_t1014014593* get_DP_3() const { return ___DP_3; }
	inline ByteU5BU5D_t1014014593** get_address_of_DP_3() { return &___DP_3; }
	inline void set_DP_3(ByteU5BU5D_t1014014593* value)
	{
		___DP_3 = value;
		Il2CppCodeGenWriteBarrier((&___DP_3), value);
	}

	inline static int32_t get_offset_of_DQ_4() { return static_cast<int32_t>(offsetof(RSAParameters_t2749614896, ___DQ_4)); }
	inline ByteU5BU5D_t1014014593* get_DQ_4() const { return ___DQ_4; }
	inline ByteU5BU5D_t1014014593** get_address_of_DQ_4() { return &___DQ_4; }
	inline void set_DQ_4(ByteU5BU5D_t1014014593* value)
	{
		___DQ_4 = value;
		Il2CppCodeGenWriteBarrier((&___DQ_4), value);
	}

	inline static int32_t get_offset_of_InverseQ_5() { return static_cast<int32_t>(offsetof(RSAParameters_t2749614896, ___InverseQ_5)); }
	inline ByteU5BU5D_t1014014593* get_InverseQ_5() const { return ___InverseQ_5; }
	inline ByteU5BU5D_t1014014593** get_address_of_InverseQ_5() { return &___InverseQ_5; }
	inline void set_InverseQ_5(ByteU5BU5D_t1014014593* value)
	{
		___InverseQ_5 = value;
		Il2CppCodeGenWriteBarrier((&___InverseQ_5), value);
	}

	inline static int32_t get_offset_of_Modulus_6() { return static_cast<int32_t>(offsetof(RSAParameters_t2749614896, ___Modulus_6)); }
	inline ByteU5BU5D_t1014014593* get_Modulus_6() const { return ___Modulus_6; }
	inline ByteU5BU5D_t1014014593** get_address_of_Modulus_6() { return &___Modulus_6; }
	inline void set_Modulus_6(ByteU5BU5D_t1014014593* value)
	{
		___Modulus_6 = value;
		Il2CppCodeGenWriteBarrier((&___Modulus_6), value);
	}

	inline static int32_t get_offset_of_Exponent_7() { return static_cast<int32_t>(offsetof(RSAParameters_t2749614896, ___Exponent_7)); }
	inline ByteU5BU5D_t1014014593* get_Exponent_7() const { return ___Exponent_7; }
	inline ByteU5BU5D_t1014014593** get_address_of_Exponent_7() { return &___Exponent_7; }
	inline void set_Exponent_7(ByteU5BU5D_t1014014593* value)
	{
		___Exponent_7 = value;
		Il2CppCodeGenWriteBarrier((&___Exponent_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Security.Cryptography.RSAParameters
struct RSAParameters_t2749614896_marshaled_pinvoke
{
	uint8_t* ___P_0;
	uint8_t* ___Q_1;
	uint8_t* ___D_2;
	uint8_t* ___DP_3;
	uint8_t* ___DQ_4;
	uint8_t* ___InverseQ_5;
	uint8_t* ___Modulus_6;
	uint8_t* ___Exponent_7;
};
// Native definition for COM marshalling of System.Security.Cryptography.RSAParameters
struct RSAParameters_t2749614896_marshaled_com
{
	uint8_t* ___P_0;
	uint8_t* ___Q_1;
	uint8_t* ___D_2;
	uint8_t* ___DP_3;
	uint8_t* ___DQ_4;
	uint8_t* ___InverseQ_5;
	uint8_t* ___Modulus_6;
	uint8_t* ___Exponent_7;
};
#endif // RSAPARAMETERS_T2749614896_H
#ifndef METHODBASE_T674153939_H
#define METHODBASE_T674153939_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MethodBase
struct  MethodBase_t674153939  : public MemberInfo_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // METHODBASE_T674153939_H
#ifndef EXTENDERTYPE_T1616581759_H
#define EXTENDERTYPE_T1616581759_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Globalization.Unicode.SimpleCollator/ExtenderType
struct  ExtenderType_t1616581759 
{
public:
	// System.Int32 Mono.Globalization.Unicode.SimpleCollator/ExtenderType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ExtenderType_t1616581759, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTENDERTYPE_T1616581759_H
#ifndef ENUMERATOR_T2164740473_H
#define ENUMERATOR_T2164740473_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>
struct  Enumerator_t2164740473 
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator::dictionary
	Dictionary_2_t1684908833 * ___dictionary_0;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::stamp
	int32_t ___stamp_2;
	// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator::current
	KeyValuePair_2_t3602437993  ___current_3;

public:
	inline static int32_t get_offset_of_dictionary_0() { return static_cast<int32_t>(offsetof(Enumerator_t2164740473, ___dictionary_0)); }
	inline Dictionary_2_t1684908833 * get_dictionary_0() const { return ___dictionary_0; }
	inline Dictionary_2_t1684908833 ** get_address_of_dictionary_0() { return &___dictionary_0; }
	inline void set_dictionary_0(Dictionary_2_t1684908833 * value)
	{
		___dictionary_0 = value;
		Il2CppCodeGenWriteBarrier((&___dictionary_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t2164740473, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_stamp_2() { return static_cast<int32_t>(offsetof(Enumerator_t2164740473, ___stamp_2)); }
	inline int32_t get_stamp_2() const { return ___stamp_2; }
	inline int32_t* get_address_of_stamp_2() { return &___stamp_2; }
	inline void set_stamp_2(int32_t value)
	{
		___stamp_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t2164740473, ___current_3)); }
	inline KeyValuePair_2_t3602437993  get_current_3() const { return ___current_3; }
	inline KeyValuePair_2_t3602437993 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(KeyValuePair_2_t3602437993  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T2164740473_H
#ifndef ANALYTICSRESULT_T1606825939_H
#define ANALYTICSRESULT_T1606825939_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsResult
struct  AnalyticsResult_t1606825939 
{
public:
	// System.Int32 UnityEngine.Analytics.AnalyticsResult::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AnalyticsResult_t1606825939, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANALYTICSRESULT_T1606825939_H
#ifndef TIMESCOPE_T1786414419_H
#define TIMESCOPE_T1786414419_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SocialPlatforms.TimeScope
struct  TimeScope_t1786414419 
{
public:
	// System.Int32 UnityEngine.SocialPlatforms.TimeScope::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TimeScope_t1786414419, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESCOPE_T1786414419_H
#ifndef CONFIDENCEFACTOR_T3678829215_H
#define CONFIDENCEFACTOR_T3678829215_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Math.Prime.ConfidenceFactor
struct  ConfidenceFactor_t3678829215 
{
public:
	// System.Int32 Mono.Math.Prime.ConfidenceFactor::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ConfidenceFactor_t3678829215, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIDENCEFACTOR_T3678829215_H
#ifndef DAYOFWEEK_T61775933_H
#define DAYOFWEEK_T61775933_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DayOfWeek
struct  DayOfWeek_t61775933 
{
public:
	// System.Int32 System.DayOfWeek::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DayOfWeek_t61775933, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DAYOFWEEK_T61775933_H
#ifndef PLAYABLEGRAPH_T1897216728_H
#define PLAYABLEGRAPH_T1897216728_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.PlayableGraph
struct  PlayableGraph_t1897216728 
{
public:
	// System.IntPtr UnityEngine.Playables.PlayableGraph::m_Handle
	intptr_t ___m_Handle_0;
	// System.Int32 UnityEngine.Playables.PlayableGraph::m_Version
	int32_t ___m_Version_1;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(PlayableGraph_t1897216728, ___m_Handle_0)); }
	inline intptr_t get_m_Handle_0() const { return ___m_Handle_0; }
	inline intptr_t* get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(intptr_t value)
	{
		___m_Handle_0 = value;
	}

	inline static int32_t get_offset_of_m_Version_1() { return static_cast<int32_t>(offsetof(PlayableGraph_t1897216728, ___m_Version_1)); }
	inline int32_t get_m_Version_1() const { return ___m_Version_1; }
	inline int32_t* get_address_of_m_Version_1() { return &___m_Version_1; }
	inline void set_m_Version_1(int32_t value)
	{
		___m_Version_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYABLEGRAPH_T1897216728_H
#ifndef LOGTYPE_T1259498875_H
#define LOGTYPE_T1259498875_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.LogType
struct  LogType_t1259498875 
{
public:
	// System.Int32 UnityEngine.LogType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LogType_t1259498875, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGTYPE_T1259498875_H
#ifndef SCRIPTABLERENDERCONTEXT_T1486166028_H
#define SCRIPTABLERENDERCONTEXT_T1486166028_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.ScriptableRenderContext
struct  ScriptableRenderContext_t1486166028 
{
public:
	// System.IntPtr UnityEngine.Experimental.Rendering.ScriptableRenderContext::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(ScriptableRenderContext_t1486166028, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCRIPTABLERENDERCONTEXT_T1486166028_H
#ifndef USERSCOPE_T2912238712_H
#define USERSCOPE_T2912238712_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SocialPlatforms.UserScope
struct  UserScope_t2912238712 
{
public:
	// System.Int32 UnityEngine.SocialPlatforms.UserScope::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(UserScope_t2912238712, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // USERSCOPE_T2912238712_H
#ifndef RUNTIMEFIELDHANDLE_T1782795956_H
#define RUNTIMEFIELDHANDLE_T1782795956_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.RuntimeFieldHandle
struct  RuntimeFieldHandle_t1782795956 
{
public:
	// System.IntPtr System.RuntimeFieldHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeFieldHandle_t1782795956, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEFIELDHANDLE_T1782795956_H
#ifndef USERSTATE_T707530005_H
#define USERSTATE_T707530005_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SocialPlatforms.UserState
struct  UserState_t707530005 
{
public:
	// System.Int32 UnityEngine.SocialPlatforms.UserState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(UserState_t707530005, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // USERSTATE_T707530005_H
#ifndef SIGN_T3270271764_H
#define SIGN_T3270271764_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Math.BigInteger/Sign
struct  Sign_t3270271764 
{
public:
	// System.Int32 Mono.Math.BigInteger/Sign::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Sign_t3270271764, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIGN_T3270271764_H
#ifndef PARAMETERATTRIBUTES_T3383706087_H
#define PARAMETERATTRIBUTES_T3383706087_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.ParameterAttributes
struct  ParameterAttributes_t3383706087 
{
public:
	// System.Int32 System.Reflection.ParameterAttributes::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ParameterAttributes_t3383706087, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARAMETERATTRIBUTES_T3383706087_H
#ifndef RUNTIMETYPEHANDLE_T637624852_H
#define RUNTIMETYPEHANDLE_T637624852_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.RuntimeTypeHandle
struct  RuntimeTypeHandle_t637624852 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_t637624852, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMETYPEHANDLE_T637624852_H
#ifndef PERSISTENTLISTENERMODE_T677115295_H
#define PERSISTENTLISTENERMODE_T677115295_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.PersistentListenerMode
struct  PersistentListenerMode_t677115295 
{
public:
	// System.Int32 UnityEngine.Events.PersistentListenerMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PersistentListenerMode_t677115295, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PERSISTENTLISTENERMODE_T677115295_H
#ifndef TYPEATTRIBUTES_T3122562390_H
#define TYPEATTRIBUTES_T3122562390_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.TypeAttributes
struct  TypeAttributes_t3122562390 
{
public:
	// System.Int32 System.Reflection.TypeAttributes::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TypeAttributes_t3122562390, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPEATTRIBUTES_T3122562390_H
#ifndef DATASTREAMTYPE_T1550112629_H
#define DATASTREAMTYPE_T1550112629_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.DataStreamType
struct  DataStreamType_t1550112629 
{
public:
	// System.Int32 UnityEngine.Playables.DataStreamType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DataStreamType_t1550112629, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATASTREAMTYPE_T1550112629_H
#ifndef SECURITYACTION_T4061768731_H
#define SECURITYACTION_T4061768731_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Permissions.SecurityAction
struct  SecurityAction_t4061768731 
{
public:
	// System.Int32 System.Security.Permissions.SecurityAction::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SecurityAction_t4061768731, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECURITYACTION_T4061768731_H
#ifndef RESOURCEATTRIBUTES_T860636606_H
#define RESOURCEATTRIBUTES_T860636606_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.ResourceAttributes
struct  ResourceAttributes_t860636606 
{
public:
	// System.Int32 System.Reflection.ResourceAttributes::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ResourceAttributes_t860636606, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESOURCEATTRIBUTES_T860636606_H
#ifndef FORMATEXCEPTION_T4223230285_H
#define FORMATEXCEPTION_T4223230285_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.FormatException
struct  FormatException_t4223230285  : public SystemException_t3635077273
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORMATEXCEPTION_T4223230285_H
#ifndef METHODIMPLATTRIBUTES_T3639404534_H
#define METHODIMPLATTRIBUTES_T3639404534_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MethodImplAttributes
struct  MethodImplAttributes_t3639404534 
{
public:
	// System.Int32 System.Reflection.MethodImplAttributes::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MethodImplAttributes_t3639404534, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // METHODIMPLATTRIBUTES_T3639404534_H
#ifndef BINDINGFLAGS_T3268090012_H
#define BINDINGFLAGS_T3268090012_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.BindingFlags
struct  BindingFlags_t3268090012 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BindingFlags_t3268090012, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDINGFLAGS_T3268090012_H
#ifndef COMPAREOPTIONS_T3933814440_H
#define COMPAREOPTIONS_T3933814440_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Globalization.CompareOptions
struct  CompareOptions_t3933814440 
{
public:
	// System.Int32 System.Globalization.CompareOptions::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CompareOptions_t3933814440, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPAREOPTIONS_T3933814440_H
#ifndef DELEGATE_T96267039_H
#define DELEGATE_T96267039_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t96267039  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t2624639528 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t96267039, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t96267039, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t96267039, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t96267039, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t96267039, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t96267039, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t96267039, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t96267039, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t96267039, ___data_8)); }
	inline DelegateData_t2624639528 * get_data_8() const { return ___data_8; }
	inline DelegateData_t2624639528 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t2624639528 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T96267039_H
#ifndef MEMBERTYPES_T2339173058_H
#define MEMBERTYPES_T2339173058_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MemberTypes
struct  MemberTypes_t2339173058 
{
public:
	// System.Int32 System.Reflection.MemberTypes::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MemberTypes_t2339173058, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERTYPES_T2339173058_H
#ifndef ENUMERATOR_T2306086094_H
#define ENUMERATOR_T2306086094_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>
struct  Enumerator_t2306086094 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t546271037 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	CustomAttributeTypedArgument_t2667702721  ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_t2306086094, ___l_0)); }
	inline List_1_t546271037 * get_l_0() const { return ___l_0; }
	inline List_1_t546271037 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t546271037 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t2306086094, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t2306086094, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t2306086094, ___current_3)); }
	inline CustomAttributeTypedArgument_t2667702721  get_current_3() const { return ___current_3; }
	inline CustomAttributeTypedArgument_t2667702721 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(CustomAttributeTypedArgument_t2667702721  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T2306086094_H
#ifndef ENUMERATOR_T928396369_H
#define ENUMERATOR_T928396369_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>
struct  Enumerator_t928396369 
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator::dictionary
	Dictionary_2_t448564729 * ___dictionary_0;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::stamp
	int32_t ___stamp_2;
	// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator::current
	KeyValuePair_2_t2366093889  ___current_3;

public:
	inline static int32_t get_offset_of_dictionary_0() { return static_cast<int32_t>(offsetof(Enumerator_t928396369, ___dictionary_0)); }
	inline Dictionary_2_t448564729 * get_dictionary_0() const { return ___dictionary_0; }
	inline Dictionary_2_t448564729 ** get_address_of_dictionary_0() { return &___dictionary_0; }
	inline void set_dictionary_0(Dictionary_2_t448564729 * value)
	{
		___dictionary_0 = value;
		Il2CppCodeGenWriteBarrier((&___dictionary_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t928396369, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_stamp_2() { return static_cast<int32_t>(offsetof(Enumerator_t928396369, ___stamp_2)); }
	inline int32_t get_stamp_2() const { return ___stamp_2; }
	inline int32_t* get_address_of_stamp_2() { return &___stamp_2; }
	inline void set_stamp_2(int32_t value)
	{
		___stamp_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t928396369, ___current_3)); }
	inline KeyValuePair_2_t2366093889  get_current_3() const { return ___current_3; }
	inline KeyValuePair_2_t2366093889 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(KeyValuePair_2_t2366093889  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T928396369_H
#ifndef TYPETAG_T1137063594_H
#define TYPETAG_T1137063594_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.Formatters.Binary.TypeTag
struct  TypeTag_t1137063594 
{
public:
	// System.Byte System.Runtime.Serialization.Formatters.Binary.TypeTag::value__
	uint8_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TypeTag_t1137063594, ___value___1)); }
	inline uint8_t get_value___1() const { return ___value___1; }
	inline uint8_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(uint8_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPETAG_T1137063594_H
#ifndef CLIENTCERTIFICATETYPE_T194976790_H
#define CLIENTCERTIFICATETYPE_T194976790_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.Handshake.ClientCertificateType
struct  ClientCertificateType_t194976790 
{
public:
	// System.Int32 Mono.Security.Protocol.Tls.Handshake.ClientCertificateType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ClientCertificateType_t194976790, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLIENTCERTIFICATETYPE_T194976790_H
#ifndef MONOIOERROR_T3087938678_H
#define MONOIOERROR_T3087938678_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.MonoIOError
struct  MonoIOError_t3087938678 
{
public:
	// System.Int32 System.IO.MonoIOError::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MonoIOError_t3087938678, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOIOERROR_T3087938678_H
#ifndef CUSTOMATTRIBUTENAMEDARGUMENT_T425730339_H
#define CUSTOMATTRIBUTENAMEDARGUMENT_T425730339_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.CustomAttributeNamedArgument
struct  CustomAttributeNamedArgument_t425730339 
{
public:
	// System.Reflection.CustomAttributeTypedArgument System.Reflection.CustomAttributeNamedArgument::typedArgument
	CustomAttributeTypedArgument_t2667702721  ___typedArgument_0;
	// System.Reflection.MemberInfo System.Reflection.CustomAttributeNamedArgument::memberInfo
	MemberInfo_t * ___memberInfo_1;

public:
	inline static int32_t get_offset_of_typedArgument_0() { return static_cast<int32_t>(offsetof(CustomAttributeNamedArgument_t425730339, ___typedArgument_0)); }
	inline CustomAttributeTypedArgument_t2667702721  get_typedArgument_0() const { return ___typedArgument_0; }
	inline CustomAttributeTypedArgument_t2667702721 * get_address_of_typedArgument_0() { return &___typedArgument_0; }
	inline void set_typedArgument_0(CustomAttributeTypedArgument_t2667702721  value)
	{
		___typedArgument_0 = value;
	}

	inline static int32_t get_offset_of_memberInfo_1() { return static_cast<int32_t>(offsetof(CustomAttributeNamedArgument_t425730339, ___memberInfo_1)); }
	inline MemberInfo_t * get_memberInfo_1() const { return ___memberInfo_1; }
	inline MemberInfo_t ** get_address_of_memberInfo_1() { return &___memberInfo_1; }
	inline void set_memberInfo_1(MemberInfo_t * value)
	{
		___memberInfo_1 = value;
		Il2CppCodeGenWriteBarrier((&___memberInfo_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Reflection.CustomAttributeNamedArgument
struct CustomAttributeNamedArgument_t425730339_marshaled_pinvoke
{
	CustomAttributeTypedArgument_t2667702721_marshaled_pinvoke ___typedArgument_0;
	MemberInfo_t * ___memberInfo_1;
};
// Native definition for COM marshalling of System.Reflection.CustomAttributeNamedArgument
struct CustomAttributeNamedArgument_t425730339_marshaled_com
{
	CustomAttributeTypedArgument_t2667702721_marshaled_com ___typedArgument_0;
	MemberInfo_t * ___memberInfo_1;
};
#endif // CUSTOMATTRIBUTENAMEDARGUMENT_T425730339_H
#ifndef UNICODECATEGORY_T1362072295_H
#define UNICODECATEGORY_T1362072295_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Globalization.UnicodeCategory
struct  UnicodeCategory_t1362072295 
{
public:
	// System.Int32 System.Globalization.UnicodeCategory::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(UnicodeCategory_t1362072295, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNICODECATEGORY_T1362072295_H
#ifndef TYPECODE_T2052347577_H
#define TYPECODE_T2052347577_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TypeCode
struct  TypeCode_t2052347577 
{
public:
	// System.Int32 System.TypeCode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TypeCode_t2052347577, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPECODE_T2052347577_H
#ifndef KEYVALUEPAIR_2_T2204764377_H
#define KEYVALUEPAIR_2_T2204764377_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Object>
struct  KeyValuePair_2_t2204764377 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	intptr_t ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	RuntimeObject * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2204764377, ___key_0)); }
	inline intptr_t get_key_0() const { return ___key_0; }
	inline intptr_t* get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(intptr_t value)
	{
		___key_0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2204764377, ___value_1)); }
	inline RuntimeObject * get_value_1() const { return ___value_1; }
	inline RuntimeObject ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(RuntimeObject * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T2204764377_H
#ifndef ENUMERATOR_T1439858669_H
#define ENUMERATOR_T1439858669_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>
struct  Enumerator_t1439858669 
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator::dictionary
	Dictionary_2_t960027029 * ___dictionary_0;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::stamp
	int32_t ___stamp_2;
	// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator::current
	KeyValuePair_2_t2877556189  ___current_3;

public:
	inline static int32_t get_offset_of_dictionary_0() { return static_cast<int32_t>(offsetof(Enumerator_t1439858669, ___dictionary_0)); }
	inline Dictionary_2_t960027029 * get_dictionary_0() const { return ___dictionary_0; }
	inline Dictionary_2_t960027029 ** get_address_of_dictionary_0() { return &___dictionary_0; }
	inline void set_dictionary_0(Dictionary_2_t960027029 * value)
	{
		___dictionary_0 = value;
		Il2CppCodeGenWriteBarrier((&___dictionary_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t1439858669, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_stamp_2() { return static_cast<int32_t>(offsetof(Enumerator_t1439858669, ___stamp_2)); }
	inline int32_t get_stamp_2() const { return ___stamp_2; }
	inline int32_t* get_address_of_stamp_2() { return &___stamp_2; }
	inline void set_stamp_2(int32_t value)
	{
		___stamp_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t1439858669, ___current_3)); }
	inline KeyValuePair_2_t2877556189  get_current_3() const { return ___current_3; }
	inline KeyValuePair_2_t2877556189 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(KeyValuePair_2_t2877556189  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T1439858669_H
#ifndef PLAYABLEOUTPUTHANDLE_T2719521870_H
#define PLAYABLEOUTPUTHANDLE_T2719521870_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.PlayableOutputHandle
struct  PlayableOutputHandle_t2719521870 
{
public:
	// System.IntPtr UnityEngine.Playables.PlayableOutputHandle::m_Handle
	intptr_t ___m_Handle_0;
	// System.Int32 UnityEngine.Playables.PlayableOutputHandle::m_Version
	int32_t ___m_Version_1;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(PlayableOutputHandle_t2719521870, ___m_Handle_0)); }
	inline intptr_t get_m_Handle_0() const { return ___m_Handle_0; }
	inline intptr_t* get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(intptr_t value)
	{
		___m_Handle_0 = value;
	}

	inline static int32_t get_offset_of_m_Version_1() { return static_cast<int32_t>(offsetof(PlayableOutputHandle_t2719521870, ___m_Version_1)); }
	inline int32_t get_m_Version_1() const { return ___m_Version_1; }
	inline int32_t* get_address_of_m_Version_1() { return &___m_Version_1; }
	inline void set_m_Version_1(int32_t value)
	{
		___m_Version_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYABLEOUTPUTHANDLE_T2719521870_H
#ifndef URIHOSTNAMETYPE_T1837236037_H
#define URIHOSTNAMETYPE_T1837236037_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UriHostNameType
struct  UriHostNameType_t1837236037 
{
public:
	// System.Int32 System.UriHostNameType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(UriHostNameType_t1837236037, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // URIHOSTNAMETYPE_T1837236037_H
#ifndef X509KEYUSAGEFLAGS_T1189859534_H
#define X509KEYUSAGEFLAGS_T1189859534_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509KeyUsageFlags
struct  X509KeyUsageFlags_t1189859534 
{
public:
	// System.Int32 System.Security.Cryptography.X509Certificates.X509KeyUsageFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(X509KeyUsageFlags_t1189859534, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509KEYUSAGEFLAGS_T1189859534_H
#ifndef OBJECT_T2461733472_H
#define OBJECT_T2461733472_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t2461733472  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t2461733472, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t2461733472_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t2461733472_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t2461733472_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t2461733472_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T2461733472_H
#ifndef POSITION_T3652736430_H
#define POSITION_T3652736430_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.Position
struct  Position_t3652736430 
{
public:
	// System.UInt16 System.Text.RegularExpressions.Position::value__
	uint16_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Position_t3652736430, ___value___1)); }
	inline uint16_t get_value___1() const { return ___value___1; }
	inline uint16_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(uint16_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSITION_T3652736430_H
#ifndef REGEXOPTIONS_T2920399184_H
#define REGEXOPTIONS_T2920399184_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.RegexOptions
struct  RegexOptions_t2920399184 
{
public:
	// System.Int32 System.Text.RegularExpressions.RegexOptions::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RegexOptions_t2920399184, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REGEXOPTIONS_T2920399184_H
#ifndef CSPPROVIDERFLAGS_T2530719326_H
#define CSPPROVIDERFLAGS_T2530719326_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.CspProviderFlags
struct  CspProviderFlags_t2530719326 
{
public:
	// System.Int32 System.Security.Cryptography.CspProviderFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CspProviderFlags_t2530719326, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CSPPROVIDERFLAGS_T2530719326_H
#ifndef METHODATTRIBUTES_T3855721989_H
#define METHODATTRIBUTES_T3855721989_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MethodAttributes
struct  MethodAttributes_t3855721989 
{
public:
	// System.Int32 System.Reflection.MethodAttributes::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MethodAttributes_t3855721989, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // METHODATTRIBUTES_T3855721989_H
#ifndef CIPHERMODE_T1257099160_H
#define CIPHERMODE_T1257099160_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.CipherMode
struct  CipherMode_t1257099160 
{
public:
	// System.Int32 System.Security.Cryptography.CipherMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CipherMode_t1257099160, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CIPHERMODE_T1257099160_H
#ifndef OPFLAGS_T3924773783_H
#define OPFLAGS_T3924773783_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.OpFlags
struct  OpFlags_t3924773783 
{
public:
	// System.UInt16 System.Text.RegularExpressions.OpFlags::value__
	uint16_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(OpFlags_t3924773783, ___value___1)); }
	inline uint16_t get_value___1() const { return ___value___1; }
	inline uint16_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(uint16_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPFLAGS_T3924773783_H
#ifndef CATEGORY_T2116501226_H
#define CATEGORY_T2116501226_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.Category
struct  Category_t2116501226 
{
public:
	// System.UInt16 System.Text.RegularExpressions.Category::value__
	uint16_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Category_t2116501226, ___value___1)); }
	inline uint16_t get_value___1() const { return ___value___1; }
	inline uint16_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(uint16_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CATEGORY_T2116501226_H
#ifndef PADDINGMODE_T218725588_H
#define PADDINGMODE_T218725588_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.PaddingMode
struct  PaddingMode_t218725588 
{
public:
	// System.Int32 System.Security.Cryptography.PaddingMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PaddingMode_t218725588, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PADDINGMODE_T218725588_H
#ifndef X509VERIFICATIONFLAGS_T1372417108_H
#define X509VERIFICATIONFLAGS_T1372417108_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509VerificationFlags
struct  X509VerificationFlags_t1372417108 
{
public:
	// System.Int32 System.Security.Cryptography.X509Certificates.X509VerificationFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(X509VerificationFlags_t1372417108, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509VERIFICATIONFLAGS_T1372417108_H
#ifndef X509REVOCATIONMODE_T2217493564_H
#define X509REVOCATIONMODE_T2217493564_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509RevocationMode
struct  X509RevocationMode_t2217493564 
{
public:
	// System.Int32 System.Security.Cryptography.X509Certificates.X509RevocationMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(X509RevocationMode_t2217493564, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509REVOCATIONMODE_T2217493564_H
#ifndef X509REVOCATIONFLAG_T3762606324_H
#define X509REVOCATIONFLAG_T3762606324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509RevocationFlag
struct  X509RevocationFlag_t3762606324 
{
public:
	// System.Int32 System.Security.Cryptography.X509Certificates.X509RevocationFlag::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(X509RevocationFlag_t3762606324, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509REVOCATIONFLAG_T3762606324_H
#ifndef ASSEMBLYNAMEFLAGS_T899890916_H
#define ASSEMBLYNAMEFLAGS_T899890916_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.AssemblyNameFlags
struct  AssemblyNameFlags_t899890916 
{
public:
	// System.Int32 System.Reflection.AssemblyNameFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AssemblyNameFlags_t899890916, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSEMBLYNAMEFLAGS_T899890916_H
#ifndef X509CHAINSTATUSFLAGS_T2184184987_H
#define X509CHAINSTATUSFLAGS_T2184184987_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509ChainStatusFlags
struct  X509ChainStatusFlags_t2184184987 
{
public:
	// System.Int32 System.Security.Cryptography.X509Certificates.X509ChainStatusFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(X509ChainStatusFlags_t2184184987, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509CHAINSTATUSFLAGS_T2184184987_H
#ifndef ASNDECODESTATUS_T4190820181_H
#define ASNDECODESTATUS_T4190820181_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.AsnDecodeStatus
struct  AsnDecodeStatus_t4190820181 
{
public:
	// System.Int32 System.Security.Cryptography.AsnDecodeStatus::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AsnDecodeStatus_t4190820181, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASNDECODESTATUS_T4190820181_H
#ifndef PROPERTYATTRIBUTES_T820793723_H
#define PROPERTYATTRIBUTES_T820793723_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.PropertyAttributes
struct  PropertyAttributes_t820793723 
{
public:
	// System.Int32 System.Reflection.PropertyAttributes::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PropertyAttributes_t820793723, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYATTRIBUTES_T820793723_H
#ifndef SECURITYPROTOCOLTYPE_T26325249_H
#define SECURITYPROTOCOLTYPE_T26325249_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.SecurityProtocolType
struct  SecurityProtocolType_t26325249 
{
public:
	// System.Int32 System.Net.SecurityProtocolType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SecurityProtocolType_t26325249, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECURITYPROTOCOLTYPE_T26325249_H
#ifndef ADDRESSFAMILY_T4183665415_H
#define ADDRESSFAMILY_T4183665415_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Sockets.AddressFamily
struct  AddressFamily_t4183665415 
{
public:
	// System.Int32 System.Net.Sockets.AddressFamily::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AddressFamily_t4183665415, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADDRESSFAMILY_T4183665415_H
#ifndef DATETIMEKIND_T3511769276_H
#define DATETIMEKIND_T3511769276_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTimeKind
struct  DateTimeKind_t3511769276 
{
public:
	// System.Int32 System.DateTimeKind::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DateTimeKind_t3511769276, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMEKIND_T3511769276_H
#ifndef NULLABLE_1_T950977049_H
#define NULLABLE_1_T950977049_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.TimeSpan>
struct  Nullable_1_t950977049 
{
public:
	// T System.Nullable`1::value
	TimeSpan_t2986675223  ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t950977049, ___value_0)); }
	inline TimeSpan_t2986675223  get_value_0() const { return ___value_0; }
	inline TimeSpan_t2986675223 * get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(TimeSpan_t2986675223  value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t950977049, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T950977049_H
#ifndef FIELDATTRIBUTES_T1107365117_H
#define FIELDATTRIBUTES_T1107365117_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.FieldAttributes
struct  FieldAttributes_t1107365117 
{
public:
	// System.Int32 System.Reflection.FieldAttributes::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FieldAttributes_t1107365117, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIELDATTRIBUTES_T1107365117_H
#ifndef STREAMINGCONTEXTSTATES_T4185925513_H
#define STREAMINGCONTEXTSTATES_T4185925513_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.StreamingContextStates
struct  StreamingContextStates_t4185925513 
{
public:
	// System.Int32 System.Runtime.Serialization.StreamingContextStates::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(StreamingContextStates_t4185925513, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAMINGCONTEXTSTATES_T4185925513_H
#ifndef PLATFORMID_T2641264986_H
#define PLATFORMID_T2641264986_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.PlatformID
struct  PlatformID_t2641264986 
{
public:
	// System.Int32 System.PlatformID::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PlatformID_t2641264986, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLATFORMID_T2641264986_H
#ifndef SIGN_T3270271765_H
#define SIGN_T3270271765_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Math.BigInteger/Sign
struct  Sign_t3270271765 
{
public:
	// System.Int32 Mono.Math.BigInteger/Sign::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Sign_t3270271765, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIGN_T3270271765_H
#ifndef X509CHAINSTATUSFLAGS_T991867662_H
#define X509CHAINSTATUSFLAGS_T991867662_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.X509.X509ChainStatusFlags
struct  X509ChainStatusFlags_t991867662 
{
public:
	// System.Int32 Mono.Security.X509.X509ChainStatusFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(X509ChainStatusFlags_t991867662, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509CHAINSTATUSFLAGS_T991867662_H
#ifndef PLAYABLEHANDLE_T3938927722_H
#define PLAYABLEHANDLE_T3938927722_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.PlayableHandle
struct  PlayableHandle_t3938927722 
{
public:
	// System.IntPtr UnityEngine.Playables.PlayableHandle::m_Handle
	intptr_t ___m_Handle_0;
	// System.Int32 UnityEngine.Playables.PlayableHandle::m_Version
	int32_t ___m_Version_1;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(PlayableHandle_t3938927722, ___m_Handle_0)); }
	inline intptr_t get_m_Handle_0() const { return ___m_Handle_0; }
	inline intptr_t* get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(intptr_t value)
	{
		___m_Handle_0 = value;
	}

	inline static int32_t get_offset_of_m_Version_1() { return static_cast<int32_t>(offsetof(PlayableHandle_t3938927722, ___m_Version_1)); }
	inline int32_t get_m_Version_1() const { return ___m_Version_1; }
	inline int32_t* get_address_of_m_Version_1() { return &___m_Version_1; }
	inline void set_m_Version_1(int32_t value)
	{
		___m_Version_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYABLEHANDLE_T3938927722_H
#ifndef FILEATTRIBUTES_T1797260272_H
#define FILEATTRIBUTES_T1797260272_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.FileAttributes
struct  FileAttributes_t1797260272 
{
public:
	// System.Int32 System.IO.FileAttributes::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FileAttributes_t1797260272, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILEATTRIBUTES_T1797260272_H
#ifndef MONOFILETYPE_T2271609547_H
#define MONOFILETYPE_T2271609547_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.MonoFileType
struct  MonoFileType_t2271609547 
{
public:
	// System.Int32 System.IO.MonoFileType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MonoFileType_t2271609547, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOFILETYPE_T2271609547_H
#ifndef WELLKNOWNOBJECTMODE_T2386524380_H
#define WELLKNOWNOBJECTMODE_T2386524380_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Remoting.WellKnownObjectMode
struct  WellKnownObjectMode_t2386524380 
{
public:
	// System.Int32 System.Runtime.Remoting.WellKnownObjectMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(WellKnownObjectMode_t2386524380, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WELLKNOWNOBJECTMODE_T2386524380_H
#ifndef CALLINGCONVENTIONS_T1168634181_H
#define CALLINGCONVENTIONS_T1168634181_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.CallingConventions
struct  CallingConventions_t1168634181 
{
public:
	// System.Int32 System.Reflection.CallingConventions::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CallingConventions_t1168634181, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CALLINGCONVENTIONS_T1168634181_H
#ifndef RAY_T1113537957_H
#define RAY_T1113537957_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Ray
struct  Ray_t1113537957 
{
public:
	// UnityEngine.Vector3 UnityEngine.Ray::m_Origin
	Vector3_t3239555143  ___m_Origin_0;
	// UnityEngine.Vector3 UnityEngine.Ray::m_Direction
	Vector3_t3239555143  ___m_Direction_1;

public:
	inline static int32_t get_offset_of_m_Origin_0() { return static_cast<int32_t>(offsetof(Ray_t1113537957, ___m_Origin_0)); }
	inline Vector3_t3239555143  get_m_Origin_0() const { return ___m_Origin_0; }
	inline Vector3_t3239555143 * get_address_of_m_Origin_0() { return &___m_Origin_0; }
	inline void set_m_Origin_0(Vector3_t3239555143  value)
	{
		___m_Origin_0 = value;
	}

	inline static int32_t get_offset_of_m_Direction_1() { return static_cast<int32_t>(offsetof(Ray_t1113537957, ___m_Direction_1)); }
	inline Vector3_t3239555143  get_m_Direction_1() const { return ___m_Direction_1; }
	inline Vector3_t3239555143 * get_address_of_m_Direction_1() { return &___m_Direction_1; }
	inline void set_m_Direction_1(Vector3_t3239555143  value)
	{
		___m_Direction_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAY_T1113537957_H
#ifndef CAMERACLEARFLAGS_T4003071365_H
#define CAMERACLEARFLAGS_T4003071365_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CameraClearFlags
struct  CameraClearFlags_t4003071365 
{
public:
	// System.Int32 UnityEngine.CameraClearFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CameraClearFlags_t4003071365, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERACLEARFLAGS_T4003071365_H
#ifndef RUNTIMEMETHODHANDLE_T2877888302_H
#define RUNTIMEMETHODHANDLE_T2877888302_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.RuntimeMethodHandle
struct  RuntimeMethodHandle_t2877888302 
{
public:
	// System.IntPtr System.RuntimeMethodHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeMethodHandle_t2877888302, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEMETHODHANDLE_T2877888302_H
#ifndef CONTENTTYPE_T3977715531_H
#define CONTENTTYPE_T3977715531_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.ContentType
struct  ContentType_t3977715531 
{
public:
	// System.Byte Mono.Security.Protocol.Tls.ContentType::value__
	uint8_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ContentType_t3977715531, ___value___1)); }
	inline uint8_t get_value___1() const { return ___value___1; }
	inline uint8_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(uint8_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTENTTYPE_T3977715531_H
#ifndef CONFIDENCEFACTOR_T3678829216_H
#define CONFIDENCEFACTOR_T3678829216_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Math.Prime.ConfidenceFactor
struct  ConfidenceFactor_t3678829216 
{
public:
	// System.Int32 Mono.Math.Prime.ConfidenceFactor::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ConfidenceFactor_t3678829216, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIDENCEFACTOR_T3678829216_H
#ifndef TYPEFILTERLEVEL_T3266183943_H
#define TYPEFILTERLEVEL_T3266183943_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.Formatters.TypeFilterLevel
struct  TypeFilterLevel_t3266183943 
{
public:
	// System.Int32 System.Runtime.Serialization.Formatters.TypeFilterLevel::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TypeFilterLevel_t3266183943, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPEFILTERLEVEL_T3266183943_H
#ifndef HANDSHAKETYPE_T2678014388_H
#define HANDSHAKETYPE_T2678014388_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.Handshake.HandshakeType
struct  HandshakeType_t2678014388 
{
public:
	// System.Byte Mono.Security.Protocol.Tls.Handshake.HandshakeType::value__
	uint8_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(HandshakeType_t2678014388, ___value___1)); }
	inline uint8_t get_value___1() const { return ___value___1; }
	inline uint8_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(uint8_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HANDSHAKETYPE_T2678014388_H
#ifndef SECURITYCOMPRESSIONTYPE_T1053362341_H
#define SECURITYCOMPRESSIONTYPE_T1053362341_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.SecurityCompressionType
struct  SecurityCompressionType_t1053362341 
{
public:
	// System.Int32 Mono.Security.Protocol.Tls.SecurityCompressionType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SecurityCompressionType_t1053362341, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECURITYCOMPRESSIONTYPE_T1053362341_H
#ifndef SECURITYPROTOCOLTYPE_T4172395504_H
#define SECURITYPROTOCOLTYPE_T4172395504_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.SecurityProtocolType
struct  SecurityProtocolType_t4172395504 
{
public:
	// System.Int32 Mono.Security.Protocol.Tls.SecurityProtocolType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SecurityProtocolType_t4172395504, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECURITYPROTOCOLTYPE_T4172395504_H
#ifndef EXCHANGEALGORITHMTYPE_T608439419_H
#define EXCHANGEALGORITHMTYPE_T608439419_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.ExchangeAlgorithmType
struct  ExchangeAlgorithmType_t608439419 
{
public:
	// System.Int32 Mono.Security.Protocol.Tls.ExchangeAlgorithmType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ExchangeAlgorithmType_t608439419, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCHANGEALGORITHMTYPE_T608439419_H
#ifndef HASHALGORITHMTYPE_T2406960816_H
#define HASHALGORITHMTYPE_T2406960816_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.HashAlgorithmType
struct  HashAlgorithmType_t2406960816 
{
public:
	// System.Int32 Mono.Security.Protocol.Tls.HashAlgorithmType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(HashAlgorithmType_t2406960816, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HASHALGORITHMTYPE_T2406960816_H
#ifndef CIPHERALGORITHMTYPE_T3078901768_H
#define CIPHERALGORITHMTYPE_T3078901768_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.CipherAlgorithmType
struct  CipherAlgorithmType_t3078901768 
{
public:
	// System.Int32 Mono.Security.Protocol.Tls.CipherAlgorithmType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CipherAlgorithmType_t3078901768, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CIPHERALGORITHMTYPE_T3078901768_H
#ifndef ALERTDESCRIPTION_T952063977_H
#define ALERTDESCRIPTION_T952063977_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.AlertDescription
struct  AlertDescription_t952063977 
{
public:
	// System.Byte Mono.Security.Protocol.Tls.AlertDescription::value__
	uint8_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AlertDescription_t952063977, ___value___1)); }
	inline uint8_t get_value___1() const { return ___value___1; }
	inline uint8_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(uint8_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ALERTDESCRIPTION_T952063977_H
#ifndef EVENTATTRIBUTES_T1621333428_H
#define EVENTATTRIBUTES_T1621333428_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.EventAttributes
struct  EventAttributes_t1621333428 
{
public:
	// System.Int32 System.Reflection.EventAttributes::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(EventAttributes_t1621333428, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTATTRIBUTES_T1621333428_H
#ifndef ALERTLEVEL_T912398071_H
#define ALERTLEVEL_T912398071_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.AlertLevel
struct  AlertLevel_t912398071 
{
public:
	// System.Byte Mono.Security.Protocol.Tls.AlertLevel::value__
	uint8_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AlertLevel_t912398071, ___value___1)); }
	inline uint8_t get_value___1() const { return ___value___1; }
	inline uint8_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(uint8_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ALERTLEVEL_T912398071_H
#ifndef HANDSHAKESTATE_T3800758483_H
#define HANDSHAKESTATE_T3800758483_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.HandshakeState
struct  HandshakeState_t3800758483 
{
public:
	// System.Int32 Mono.Security.Protocol.Tls.HandshakeState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(HandshakeState_t3800758483, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HANDSHAKESTATE_T3800758483_H
#ifndef STACKBEHAVIOUR_T1946418481_H
#define STACKBEHAVIOUR_T1946418481_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.Emit.StackBehaviour
struct  StackBehaviour_t1946418481 
{
public:
	// System.Int32 System.Reflection.Emit.StackBehaviour::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(StackBehaviour_t1946418481, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STACKBEHAVIOUR_T1946418481_H
#ifndef MONOMETHODINFO_T1860908649_H
#define MONOMETHODINFO_T1860908649_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MonoMethodInfo
struct  MonoMethodInfo_t1860908649 
{
public:
	// System.Type System.Reflection.MonoMethodInfo::parent
	Type_t * ___parent_0;
	// System.Type System.Reflection.MonoMethodInfo::ret
	Type_t * ___ret_1;
	// System.Reflection.MethodAttributes System.Reflection.MonoMethodInfo::attrs
	int32_t ___attrs_2;
	// System.Reflection.MethodImplAttributes System.Reflection.MonoMethodInfo::iattrs
	int32_t ___iattrs_3;
	// System.Reflection.CallingConventions System.Reflection.MonoMethodInfo::callconv
	int32_t ___callconv_4;

public:
	inline static int32_t get_offset_of_parent_0() { return static_cast<int32_t>(offsetof(MonoMethodInfo_t1860908649, ___parent_0)); }
	inline Type_t * get_parent_0() const { return ___parent_0; }
	inline Type_t ** get_address_of_parent_0() { return &___parent_0; }
	inline void set_parent_0(Type_t * value)
	{
		___parent_0 = value;
		Il2CppCodeGenWriteBarrier((&___parent_0), value);
	}

	inline static int32_t get_offset_of_ret_1() { return static_cast<int32_t>(offsetof(MonoMethodInfo_t1860908649, ___ret_1)); }
	inline Type_t * get_ret_1() const { return ___ret_1; }
	inline Type_t ** get_address_of_ret_1() { return &___ret_1; }
	inline void set_ret_1(Type_t * value)
	{
		___ret_1 = value;
		Il2CppCodeGenWriteBarrier((&___ret_1), value);
	}

	inline static int32_t get_offset_of_attrs_2() { return static_cast<int32_t>(offsetof(MonoMethodInfo_t1860908649, ___attrs_2)); }
	inline int32_t get_attrs_2() const { return ___attrs_2; }
	inline int32_t* get_address_of_attrs_2() { return &___attrs_2; }
	inline void set_attrs_2(int32_t value)
	{
		___attrs_2 = value;
	}

	inline static int32_t get_offset_of_iattrs_3() { return static_cast<int32_t>(offsetof(MonoMethodInfo_t1860908649, ___iattrs_3)); }
	inline int32_t get_iattrs_3() const { return ___iattrs_3; }
	inline int32_t* get_address_of_iattrs_3() { return &___iattrs_3; }
	inline void set_iattrs_3(int32_t value)
	{
		___iattrs_3 = value;
	}

	inline static int32_t get_offset_of_callconv_4() { return static_cast<int32_t>(offsetof(MonoMethodInfo_t1860908649, ___callconv_4)); }
	inline int32_t get_callconv_4() const { return ___callconv_4; }
	inline int32_t* get_address_of_callconv_4() { return &___callconv_4; }
	inline void set_callconv_4(int32_t value)
	{
		___callconv_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Reflection.MonoMethodInfo
struct MonoMethodInfo_t1860908649_marshaled_pinvoke
{
	Type_t * ___parent_0;
	Type_t * ___ret_1;
	int32_t ___attrs_2;
	int32_t ___iattrs_3;
	int32_t ___callconv_4;
};
// Native definition for COM marshalling of System.Reflection.MonoMethodInfo
struct MonoMethodInfo_t1860908649_marshaled_com
{
	Type_t * ___parent_0;
	Type_t * ___ret_1;
	int32_t ___attrs_2;
	int32_t ___iattrs_3;
	int32_t ___callconv_4;
};
#endif // MONOMETHODINFO_T1860908649_H
#ifndef MONOEVENTINFO_T802398473_H
#define MONOEVENTINFO_T802398473_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MonoEventInfo
struct  MonoEventInfo_t802398473 
{
public:
	// System.Type System.Reflection.MonoEventInfo::declaring_type
	Type_t * ___declaring_type_0;
	// System.Type System.Reflection.MonoEventInfo::reflected_type
	Type_t * ___reflected_type_1;
	// System.String System.Reflection.MonoEventInfo::name
	String_t* ___name_2;
	// System.Reflection.MethodInfo System.Reflection.MonoEventInfo::add_method
	MethodInfo_t * ___add_method_3;
	// System.Reflection.MethodInfo System.Reflection.MonoEventInfo::remove_method
	MethodInfo_t * ___remove_method_4;
	// System.Reflection.MethodInfo System.Reflection.MonoEventInfo::raise_method
	MethodInfo_t * ___raise_method_5;
	// System.Reflection.EventAttributes System.Reflection.MonoEventInfo::attrs
	int32_t ___attrs_6;
	// System.Reflection.MethodInfo[] System.Reflection.MonoEventInfo::other_methods
	MethodInfoU5BU5D_t2077906545* ___other_methods_7;

public:
	inline static int32_t get_offset_of_declaring_type_0() { return static_cast<int32_t>(offsetof(MonoEventInfo_t802398473, ___declaring_type_0)); }
	inline Type_t * get_declaring_type_0() const { return ___declaring_type_0; }
	inline Type_t ** get_address_of_declaring_type_0() { return &___declaring_type_0; }
	inline void set_declaring_type_0(Type_t * value)
	{
		___declaring_type_0 = value;
		Il2CppCodeGenWriteBarrier((&___declaring_type_0), value);
	}

	inline static int32_t get_offset_of_reflected_type_1() { return static_cast<int32_t>(offsetof(MonoEventInfo_t802398473, ___reflected_type_1)); }
	inline Type_t * get_reflected_type_1() const { return ___reflected_type_1; }
	inline Type_t ** get_address_of_reflected_type_1() { return &___reflected_type_1; }
	inline void set_reflected_type_1(Type_t * value)
	{
		___reflected_type_1 = value;
		Il2CppCodeGenWriteBarrier((&___reflected_type_1), value);
	}

	inline static int32_t get_offset_of_name_2() { return static_cast<int32_t>(offsetof(MonoEventInfo_t802398473, ___name_2)); }
	inline String_t* get_name_2() const { return ___name_2; }
	inline String_t** get_address_of_name_2() { return &___name_2; }
	inline void set_name_2(String_t* value)
	{
		___name_2 = value;
		Il2CppCodeGenWriteBarrier((&___name_2), value);
	}

	inline static int32_t get_offset_of_add_method_3() { return static_cast<int32_t>(offsetof(MonoEventInfo_t802398473, ___add_method_3)); }
	inline MethodInfo_t * get_add_method_3() const { return ___add_method_3; }
	inline MethodInfo_t ** get_address_of_add_method_3() { return &___add_method_3; }
	inline void set_add_method_3(MethodInfo_t * value)
	{
		___add_method_3 = value;
		Il2CppCodeGenWriteBarrier((&___add_method_3), value);
	}

	inline static int32_t get_offset_of_remove_method_4() { return static_cast<int32_t>(offsetof(MonoEventInfo_t802398473, ___remove_method_4)); }
	inline MethodInfo_t * get_remove_method_4() const { return ___remove_method_4; }
	inline MethodInfo_t ** get_address_of_remove_method_4() { return &___remove_method_4; }
	inline void set_remove_method_4(MethodInfo_t * value)
	{
		___remove_method_4 = value;
		Il2CppCodeGenWriteBarrier((&___remove_method_4), value);
	}

	inline static int32_t get_offset_of_raise_method_5() { return static_cast<int32_t>(offsetof(MonoEventInfo_t802398473, ___raise_method_5)); }
	inline MethodInfo_t * get_raise_method_5() const { return ___raise_method_5; }
	inline MethodInfo_t ** get_address_of_raise_method_5() { return &___raise_method_5; }
	inline void set_raise_method_5(MethodInfo_t * value)
	{
		___raise_method_5 = value;
		Il2CppCodeGenWriteBarrier((&___raise_method_5), value);
	}

	inline static int32_t get_offset_of_attrs_6() { return static_cast<int32_t>(offsetof(MonoEventInfo_t802398473, ___attrs_6)); }
	inline int32_t get_attrs_6() const { return ___attrs_6; }
	inline int32_t* get_address_of_attrs_6() { return &___attrs_6; }
	inline void set_attrs_6(int32_t value)
	{
		___attrs_6 = value;
	}

	inline static int32_t get_offset_of_other_methods_7() { return static_cast<int32_t>(offsetof(MonoEventInfo_t802398473, ___other_methods_7)); }
	inline MethodInfoU5BU5D_t2077906545* get_other_methods_7() const { return ___other_methods_7; }
	inline MethodInfoU5BU5D_t2077906545** get_address_of_other_methods_7() { return &___other_methods_7; }
	inline void set_other_methods_7(MethodInfoU5BU5D_t2077906545* value)
	{
		___other_methods_7 = value;
		Il2CppCodeGenWriteBarrier((&___other_methods_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Reflection.MonoEventInfo
struct MonoEventInfo_t802398473_marshaled_pinvoke
{
	Type_t * ___declaring_type_0;
	Type_t * ___reflected_type_1;
	char* ___name_2;
	MethodInfo_t * ___add_method_3;
	MethodInfo_t * ___remove_method_4;
	MethodInfo_t * ___raise_method_5;
	int32_t ___attrs_6;
	MethodInfoU5BU5D_t2077906545* ___other_methods_7;
};
// Native definition for COM marshalling of System.Reflection.MonoEventInfo
struct MonoEventInfo_t802398473_marshaled_com
{
	Type_t * ___declaring_type_0;
	Type_t * ___reflected_type_1;
	Il2CppChar* ___name_2;
	MethodInfo_t * ___add_method_3;
	MethodInfo_t * ___remove_method_4;
	MethodInfo_t * ___raise_method_5;
	int32_t ___attrs_6;
	MethodInfoU5BU5D_t2077906545* ___other_methods_7;
};
#endif // MONOEVENTINFO_T802398473_H
#ifndef MODULE_T2594760207_H
#define MODULE_T2594760207_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.Module
struct  Module_t2594760207  : public RuntimeObject
{
public:
	// System.IntPtr System.Reflection.Module::_impl
	intptr_t ____impl_3;
	// System.Reflection.Assembly System.Reflection.Module::assembly
	Assembly_t2665685420 * ___assembly_4;
	// System.String System.Reflection.Module::fqname
	String_t* ___fqname_5;
	// System.String System.Reflection.Module::name
	String_t* ___name_6;
	// System.String System.Reflection.Module::scopename
	String_t* ___scopename_7;
	// System.Boolean System.Reflection.Module::is_resource
	bool ___is_resource_8;
	// System.Int32 System.Reflection.Module::token
	int32_t ___token_9;

public:
	inline static int32_t get_offset_of__impl_3() { return static_cast<int32_t>(offsetof(Module_t2594760207, ____impl_3)); }
	inline intptr_t get__impl_3() const { return ____impl_3; }
	inline intptr_t* get_address_of__impl_3() { return &____impl_3; }
	inline void set__impl_3(intptr_t value)
	{
		____impl_3 = value;
	}

	inline static int32_t get_offset_of_assembly_4() { return static_cast<int32_t>(offsetof(Module_t2594760207, ___assembly_4)); }
	inline Assembly_t2665685420 * get_assembly_4() const { return ___assembly_4; }
	inline Assembly_t2665685420 ** get_address_of_assembly_4() { return &___assembly_4; }
	inline void set_assembly_4(Assembly_t2665685420 * value)
	{
		___assembly_4 = value;
		Il2CppCodeGenWriteBarrier((&___assembly_4), value);
	}

	inline static int32_t get_offset_of_fqname_5() { return static_cast<int32_t>(offsetof(Module_t2594760207, ___fqname_5)); }
	inline String_t* get_fqname_5() const { return ___fqname_5; }
	inline String_t** get_address_of_fqname_5() { return &___fqname_5; }
	inline void set_fqname_5(String_t* value)
	{
		___fqname_5 = value;
		Il2CppCodeGenWriteBarrier((&___fqname_5), value);
	}

	inline static int32_t get_offset_of_name_6() { return static_cast<int32_t>(offsetof(Module_t2594760207, ___name_6)); }
	inline String_t* get_name_6() const { return ___name_6; }
	inline String_t** get_address_of_name_6() { return &___name_6; }
	inline void set_name_6(String_t* value)
	{
		___name_6 = value;
		Il2CppCodeGenWriteBarrier((&___name_6), value);
	}

	inline static int32_t get_offset_of_scopename_7() { return static_cast<int32_t>(offsetof(Module_t2594760207, ___scopename_7)); }
	inline String_t* get_scopename_7() const { return ___scopename_7; }
	inline String_t** get_address_of_scopename_7() { return &___scopename_7; }
	inline void set_scopename_7(String_t* value)
	{
		___scopename_7 = value;
		Il2CppCodeGenWriteBarrier((&___scopename_7), value);
	}

	inline static int32_t get_offset_of_is_resource_8() { return static_cast<int32_t>(offsetof(Module_t2594760207, ___is_resource_8)); }
	inline bool get_is_resource_8() const { return ___is_resource_8; }
	inline bool* get_address_of_is_resource_8() { return &___is_resource_8; }
	inline void set_is_resource_8(bool value)
	{
		___is_resource_8 = value;
	}

	inline static int32_t get_offset_of_token_9() { return static_cast<int32_t>(offsetof(Module_t2594760207, ___token_9)); }
	inline int32_t get_token_9() const { return ___token_9; }
	inline int32_t* get_address_of_token_9() { return &___token_9; }
	inline void set_token_9(int32_t value)
	{
		___token_9 = value;
	}
};

struct Module_t2594760207_StaticFields
{
public:
	// System.Reflection.TypeFilter System.Reflection.Module::FilterTypeName
	TypeFilter_t139072584 * ___FilterTypeName_1;
	// System.Reflection.TypeFilter System.Reflection.Module::FilterTypeNameIgnoreCase
	TypeFilter_t139072584 * ___FilterTypeNameIgnoreCase_2;

public:
	inline static int32_t get_offset_of_FilterTypeName_1() { return static_cast<int32_t>(offsetof(Module_t2594760207_StaticFields, ___FilterTypeName_1)); }
	inline TypeFilter_t139072584 * get_FilterTypeName_1() const { return ___FilterTypeName_1; }
	inline TypeFilter_t139072584 ** get_address_of_FilterTypeName_1() { return &___FilterTypeName_1; }
	inline void set_FilterTypeName_1(TypeFilter_t139072584 * value)
	{
		___FilterTypeName_1 = value;
		Il2CppCodeGenWriteBarrier((&___FilterTypeName_1), value);
	}

	inline static int32_t get_offset_of_FilterTypeNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Module_t2594760207_StaticFields, ___FilterTypeNameIgnoreCase_2)); }
	inline TypeFilter_t139072584 * get_FilterTypeNameIgnoreCase_2() const { return ___FilterTypeNameIgnoreCase_2; }
	inline TypeFilter_t139072584 ** get_address_of_FilterTypeNameIgnoreCase_2() { return &___FilterTypeNameIgnoreCase_2; }
	inline void set_FilterTypeNameIgnoreCase_2(TypeFilter_t139072584 * value)
	{
		___FilterTypeNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((&___FilterTypeNameIgnoreCase_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODULE_T2594760207_H
#ifndef ENUMERATOR_T64113712_H
#define ENUMERATOR_T64113712_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>
struct  Enumerator_t64113712 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t2599265951 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	CustomAttributeNamedArgument_t425730339  ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_t64113712, ___l_0)); }
	inline List_1_t2599265951 * get_l_0() const { return ___l_0; }
	inline List_1_t2599265951 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t2599265951 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t64113712, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t64113712, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t64113712, ___current_3)); }
	inline CustomAttributeNamedArgument_t425730339  get_current_3() const { return ___current_3; }
	inline CustomAttributeNamedArgument_t425730339 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(CustomAttributeNamedArgument_t425730339  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T64113712_H
#ifndef PLAYABLEOUTPUT_T3840687608_H
#define PLAYABLEOUTPUT_T3840687608_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.PlayableOutput
struct  PlayableOutput_t3840687608 
{
public:
	// UnityEngine.Playables.PlayableOutputHandle UnityEngine.Playables.PlayableOutput::m_Handle
	PlayableOutputHandle_t2719521870  ___m_Handle_0;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(PlayableOutput_t3840687608, ___m_Handle_0)); }
	inline PlayableOutputHandle_t2719521870  get_m_Handle_0() const { return ___m_Handle_0; }
	inline PlayableOutputHandle_t2719521870 * get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(PlayableOutputHandle_t2719521870  value)
	{
		___m_Handle_0 = value;
	}
};

struct PlayableOutput_t3840687608_StaticFields
{
public:
	// UnityEngine.Playables.PlayableOutput UnityEngine.Playables.PlayableOutput::m_NullPlayableOutput
	PlayableOutput_t3840687608  ___m_NullPlayableOutput_1;

public:
	inline static int32_t get_offset_of_m_NullPlayableOutput_1() { return static_cast<int32_t>(offsetof(PlayableOutput_t3840687608_StaticFields, ___m_NullPlayableOutput_1)); }
	inline PlayableOutput_t3840687608  get_m_NullPlayableOutput_1() const { return ___m_NullPlayableOutput_1; }
	inline PlayableOutput_t3840687608 * get_address_of_m_NullPlayableOutput_1() { return &___m_NullPlayableOutput_1; }
	inline void set_m_NullPlayableOutput_1(PlayableOutput_t3840687608  value)
	{
		___m_NullPlayableOutput_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYABLEOUTPUT_T3840687608_H
#ifndef IPADDRESS_T1846197371_H
#define IPADDRESS_T1846197371_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.IPAddress
struct  IPAddress_t1846197371  : public RuntimeObject
{
public:
	// System.Int64 System.Net.IPAddress::m_Address
	int64_t ___m_Address_0;
	// System.Net.Sockets.AddressFamily System.Net.IPAddress::m_Family
	int32_t ___m_Family_1;
	// System.UInt16[] System.Net.IPAddress::m_Numbers
	UInt16U5BU5D_t30367060* ___m_Numbers_2;
	// System.Int64 System.Net.IPAddress::m_ScopeId
	int64_t ___m_ScopeId_3;

public:
	inline static int32_t get_offset_of_m_Address_0() { return static_cast<int32_t>(offsetof(IPAddress_t1846197371, ___m_Address_0)); }
	inline int64_t get_m_Address_0() const { return ___m_Address_0; }
	inline int64_t* get_address_of_m_Address_0() { return &___m_Address_0; }
	inline void set_m_Address_0(int64_t value)
	{
		___m_Address_0 = value;
	}

	inline static int32_t get_offset_of_m_Family_1() { return static_cast<int32_t>(offsetof(IPAddress_t1846197371, ___m_Family_1)); }
	inline int32_t get_m_Family_1() const { return ___m_Family_1; }
	inline int32_t* get_address_of_m_Family_1() { return &___m_Family_1; }
	inline void set_m_Family_1(int32_t value)
	{
		___m_Family_1 = value;
	}

	inline static int32_t get_offset_of_m_Numbers_2() { return static_cast<int32_t>(offsetof(IPAddress_t1846197371, ___m_Numbers_2)); }
	inline UInt16U5BU5D_t30367060* get_m_Numbers_2() const { return ___m_Numbers_2; }
	inline UInt16U5BU5D_t30367060** get_address_of_m_Numbers_2() { return &___m_Numbers_2; }
	inline void set_m_Numbers_2(UInt16U5BU5D_t30367060* value)
	{
		___m_Numbers_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Numbers_2), value);
	}

	inline static int32_t get_offset_of_m_ScopeId_3() { return static_cast<int32_t>(offsetof(IPAddress_t1846197371, ___m_ScopeId_3)); }
	inline int64_t get_m_ScopeId_3() const { return ___m_ScopeId_3; }
	inline int64_t* get_address_of_m_ScopeId_3() { return &___m_ScopeId_3; }
	inline void set_m_ScopeId_3(int64_t value)
	{
		___m_ScopeId_3 = value;
	}
};

struct IPAddress_t1846197371_StaticFields
{
public:
	// System.Net.IPAddress System.Net.IPAddress::Any
	IPAddress_t1846197371 * ___Any_4;
	// System.Net.IPAddress System.Net.IPAddress::Broadcast
	IPAddress_t1846197371 * ___Broadcast_5;
	// System.Net.IPAddress System.Net.IPAddress::Loopback
	IPAddress_t1846197371 * ___Loopback_6;
	// System.Net.IPAddress System.Net.IPAddress::None
	IPAddress_t1846197371 * ___None_7;
	// System.Net.IPAddress System.Net.IPAddress::IPv6Any
	IPAddress_t1846197371 * ___IPv6Any_8;
	// System.Net.IPAddress System.Net.IPAddress::IPv6Loopback
	IPAddress_t1846197371 * ___IPv6Loopback_9;
	// System.Net.IPAddress System.Net.IPAddress::IPv6None
	IPAddress_t1846197371 * ___IPv6None_10;

public:
	inline static int32_t get_offset_of_Any_4() { return static_cast<int32_t>(offsetof(IPAddress_t1846197371_StaticFields, ___Any_4)); }
	inline IPAddress_t1846197371 * get_Any_4() const { return ___Any_4; }
	inline IPAddress_t1846197371 ** get_address_of_Any_4() { return &___Any_4; }
	inline void set_Any_4(IPAddress_t1846197371 * value)
	{
		___Any_4 = value;
		Il2CppCodeGenWriteBarrier((&___Any_4), value);
	}

	inline static int32_t get_offset_of_Broadcast_5() { return static_cast<int32_t>(offsetof(IPAddress_t1846197371_StaticFields, ___Broadcast_5)); }
	inline IPAddress_t1846197371 * get_Broadcast_5() const { return ___Broadcast_5; }
	inline IPAddress_t1846197371 ** get_address_of_Broadcast_5() { return &___Broadcast_5; }
	inline void set_Broadcast_5(IPAddress_t1846197371 * value)
	{
		___Broadcast_5 = value;
		Il2CppCodeGenWriteBarrier((&___Broadcast_5), value);
	}

	inline static int32_t get_offset_of_Loopback_6() { return static_cast<int32_t>(offsetof(IPAddress_t1846197371_StaticFields, ___Loopback_6)); }
	inline IPAddress_t1846197371 * get_Loopback_6() const { return ___Loopback_6; }
	inline IPAddress_t1846197371 ** get_address_of_Loopback_6() { return &___Loopback_6; }
	inline void set_Loopback_6(IPAddress_t1846197371 * value)
	{
		___Loopback_6 = value;
		Il2CppCodeGenWriteBarrier((&___Loopback_6), value);
	}

	inline static int32_t get_offset_of_None_7() { return static_cast<int32_t>(offsetof(IPAddress_t1846197371_StaticFields, ___None_7)); }
	inline IPAddress_t1846197371 * get_None_7() const { return ___None_7; }
	inline IPAddress_t1846197371 ** get_address_of_None_7() { return &___None_7; }
	inline void set_None_7(IPAddress_t1846197371 * value)
	{
		___None_7 = value;
		Il2CppCodeGenWriteBarrier((&___None_7), value);
	}

	inline static int32_t get_offset_of_IPv6Any_8() { return static_cast<int32_t>(offsetof(IPAddress_t1846197371_StaticFields, ___IPv6Any_8)); }
	inline IPAddress_t1846197371 * get_IPv6Any_8() const { return ___IPv6Any_8; }
	inline IPAddress_t1846197371 ** get_address_of_IPv6Any_8() { return &___IPv6Any_8; }
	inline void set_IPv6Any_8(IPAddress_t1846197371 * value)
	{
		___IPv6Any_8 = value;
		Il2CppCodeGenWriteBarrier((&___IPv6Any_8), value);
	}

	inline static int32_t get_offset_of_IPv6Loopback_9() { return static_cast<int32_t>(offsetof(IPAddress_t1846197371_StaticFields, ___IPv6Loopback_9)); }
	inline IPAddress_t1846197371 * get_IPv6Loopback_9() const { return ___IPv6Loopback_9; }
	inline IPAddress_t1846197371 ** get_address_of_IPv6Loopback_9() { return &___IPv6Loopback_9; }
	inline void set_IPv6Loopback_9(IPAddress_t1846197371 * value)
	{
		___IPv6Loopback_9 = value;
		Il2CppCodeGenWriteBarrier((&___IPv6Loopback_9), value);
	}

	inline static int32_t get_offset_of_IPv6None_10() { return static_cast<int32_t>(offsetof(IPAddress_t1846197371_StaticFields, ___IPv6None_10)); }
	inline IPAddress_t1846197371 * get_IPv6None_10() const { return ___IPv6None_10; }
	inline IPAddress_t1846197371 ** get_address_of_IPv6None_10() { return &___IPv6None_10; }
	inline void set_IPv6None_10(IPAddress_t1846197371 * value)
	{
		___IPv6None_10 = value;
		Il2CppCodeGenWriteBarrier((&___IPv6None_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IPADDRESS_T1846197371_H
#ifndef MONOIOSTAT_T421315798_H
#define MONOIOSTAT_T421315798_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.MonoIOStat
struct  MonoIOStat_t421315798 
{
public:
	// System.String System.IO.MonoIOStat::Name
	String_t* ___Name_0;
	// System.IO.FileAttributes System.IO.MonoIOStat::Attributes
	int32_t ___Attributes_1;
	// System.Int64 System.IO.MonoIOStat::Length
	int64_t ___Length_2;
	// System.Int64 System.IO.MonoIOStat::CreationTime
	int64_t ___CreationTime_3;
	// System.Int64 System.IO.MonoIOStat::LastAccessTime
	int64_t ___LastAccessTime_4;
	// System.Int64 System.IO.MonoIOStat::LastWriteTime
	int64_t ___LastWriteTime_5;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(MonoIOStat_t421315798, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier((&___Name_0), value);
	}

	inline static int32_t get_offset_of_Attributes_1() { return static_cast<int32_t>(offsetof(MonoIOStat_t421315798, ___Attributes_1)); }
	inline int32_t get_Attributes_1() const { return ___Attributes_1; }
	inline int32_t* get_address_of_Attributes_1() { return &___Attributes_1; }
	inline void set_Attributes_1(int32_t value)
	{
		___Attributes_1 = value;
	}

	inline static int32_t get_offset_of_Length_2() { return static_cast<int32_t>(offsetof(MonoIOStat_t421315798, ___Length_2)); }
	inline int64_t get_Length_2() const { return ___Length_2; }
	inline int64_t* get_address_of_Length_2() { return &___Length_2; }
	inline void set_Length_2(int64_t value)
	{
		___Length_2 = value;
	}

	inline static int32_t get_offset_of_CreationTime_3() { return static_cast<int32_t>(offsetof(MonoIOStat_t421315798, ___CreationTime_3)); }
	inline int64_t get_CreationTime_3() const { return ___CreationTime_3; }
	inline int64_t* get_address_of_CreationTime_3() { return &___CreationTime_3; }
	inline void set_CreationTime_3(int64_t value)
	{
		___CreationTime_3 = value;
	}

	inline static int32_t get_offset_of_LastAccessTime_4() { return static_cast<int32_t>(offsetof(MonoIOStat_t421315798, ___LastAccessTime_4)); }
	inline int64_t get_LastAccessTime_4() const { return ___LastAccessTime_4; }
	inline int64_t* get_address_of_LastAccessTime_4() { return &___LastAccessTime_4; }
	inline void set_LastAccessTime_4(int64_t value)
	{
		___LastAccessTime_4 = value;
	}

	inline static int32_t get_offset_of_LastWriteTime_5() { return static_cast<int32_t>(offsetof(MonoIOStat_t421315798, ___LastWriteTime_5)); }
	inline int64_t get_LastWriteTime_5() const { return ___LastWriteTime_5; }
	inline int64_t* get_address_of_LastWriteTime_5() { return &___LastWriteTime_5; }
	inline void set_LastWriteTime_5(int64_t value)
	{
		___LastWriteTime_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.IO.MonoIOStat
struct MonoIOStat_t421315798_marshaled_pinvoke
{
	char* ___Name_0;
	int32_t ___Attributes_1;
	int64_t ___Length_2;
	int64_t ___CreationTime_3;
	int64_t ___LastAccessTime_4;
	int64_t ___LastWriteTime_5;
};
// Native definition for COM marshalling of System.IO.MonoIOStat
struct MonoIOStat_t421315798_marshaled_com
{
	Il2CppChar* ___Name_0;
	int32_t ___Attributes_1;
	int64_t ___Length_2;
	int64_t ___CreationTime_3;
	int64_t ___LastAccessTime_4;
	int64_t ___LastWriteTime_5;
};
#endif // MONOIOSTAT_T421315798_H
#ifndef AUDIOMIXERPLAYABLE_T1017334687_H
#define AUDIOMIXERPLAYABLE_T1017334687_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Audio.AudioMixerPlayable
struct  AudioMixerPlayable_t1017334687 
{
public:
	// UnityEngine.Playables.PlayableHandle UnityEngine.Audio.AudioMixerPlayable::m_Handle
	PlayableHandle_t3938927722  ___m_Handle_0;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(AudioMixerPlayable_t1017334687, ___m_Handle_0)); }
	inline PlayableHandle_t3938927722  get_m_Handle_0() const { return ___m_Handle_0; }
	inline PlayableHandle_t3938927722 * get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(PlayableHandle_t3938927722  value)
	{
		___m_Handle_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOMIXERPLAYABLE_T1017334687_H
#ifndef AUDIOCLIPPLAYABLE_T3812371941_H
#define AUDIOCLIPPLAYABLE_T3812371941_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Audio.AudioClipPlayable
struct  AudioClipPlayable_t3812371941 
{
public:
	// UnityEngine.Playables.PlayableHandle UnityEngine.Audio.AudioClipPlayable::m_Handle
	PlayableHandle_t3938927722  ___m_Handle_0;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(AudioClipPlayable_t3812371941, ___m_Handle_0)); }
	inline PlayableHandle_t3938927722  get_m_Handle_0() const { return ___m_Handle_0; }
	inline PlayableHandle_t3938927722 * get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(PlayableHandle_t3938927722  value)
	{
		___m_Handle_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOCLIPPLAYABLE_T3812371941_H
#ifndef USERPROFILE_T3211800844_H
#define USERPROFILE_T3211800844_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SocialPlatforms.Impl.UserProfile
struct  UserProfile_t3211800844  : public RuntimeObject
{
public:
	// System.String UnityEngine.SocialPlatforms.Impl.UserProfile::m_UserName
	String_t* ___m_UserName_0;
	// System.String UnityEngine.SocialPlatforms.Impl.UserProfile::m_ID
	String_t* ___m_ID_1;
	// System.Boolean UnityEngine.SocialPlatforms.Impl.UserProfile::m_IsFriend
	bool ___m_IsFriend_2;
	// UnityEngine.SocialPlatforms.UserState UnityEngine.SocialPlatforms.Impl.UserProfile::m_State
	int32_t ___m_State_3;
	// UnityEngine.Texture2D UnityEngine.SocialPlatforms.Impl.UserProfile::m_Image
	Texture2D_t359445654 * ___m_Image_4;

public:
	inline static int32_t get_offset_of_m_UserName_0() { return static_cast<int32_t>(offsetof(UserProfile_t3211800844, ___m_UserName_0)); }
	inline String_t* get_m_UserName_0() const { return ___m_UserName_0; }
	inline String_t** get_address_of_m_UserName_0() { return &___m_UserName_0; }
	inline void set_m_UserName_0(String_t* value)
	{
		___m_UserName_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_UserName_0), value);
	}

	inline static int32_t get_offset_of_m_ID_1() { return static_cast<int32_t>(offsetof(UserProfile_t3211800844, ___m_ID_1)); }
	inline String_t* get_m_ID_1() const { return ___m_ID_1; }
	inline String_t** get_address_of_m_ID_1() { return &___m_ID_1; }
	inline void set_m_ID_1(String_t* value)
	{
		___m_ID_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_ID_1), value);
	}

	inline static int32_t get_offset_of_m_IsFriend_2() { return static_cast<int32_t>(offsetof(UserProfile_t3211800844, ___m_IsFriend_2)); }
	inline bool get_m_IsFriend_2() const { return ___m_IsFriend_2; }
	inline bool* get_address_of_m_IsFriend_2() { return &___m_IsFriend_2; }
	inline void set_m_IsFriend_2(bool value)
	{
		___m_IsFriend_2 = value;
	}

	inline static int32_t get_offset_of_m_State_3() { return static_cast<int32_t>(offsetof(UserProfile_t3211800844, ___m_State_3)); }
	inline int32_t get_m_State_3() const { return ___m_State_3; }
	inline int32_t* get_address_of_m_State_3() { return &___m_State_3; }
	inline void set_m_State_3(int32_t value)
	{
		___m_State_3 = value;
	}

	inline static int32_t get_offset_of_m_Image_4() { return static_cast<int32_t>(offsetof(UserProfile_t3211800844, ___m_Image_4)); }
	inline Texture2D_t359445654 * get_m_Image_4() const { return ___m_Image_4; }
	inline Texture2D_t359445654 ** get_address_of_m_Image_4() { return &___m_Image_4; }
	inline void set_m_Image_4(Texture2D_t359445654 * value)
	{
		___m_Image_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Image_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // USERPROFILE_T3211800844_H
#ifndef CONTEXT_T3303670129_H
#define CONTEXT_T3303670129_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Globalization.Unicode.SimpleCollator/Context
struct  Context_t3303670129 
{
public:
	// System.Globalization.CompareOptions Mono.Globalization.Unicode.SimpleCollator/Context::Option
	int32_t ___Option_0;
	// System.Byte* Mono.Globalization.Unicode.SimpleCollator/Context::NeverMatchFlags
	uint8_t* ___NeverMatchFlags_1;
	// System.Byte* Mono.Globalization.Unicode.SimpleCollator/Context::AlwaysMatchFlags
	uint8_t* ___AlwaysMatchFlags_2;
	// System.Byte* Mono.Globalization.Unicode.SimpleCollator/Context::Buffer1
	uint8_t* ___Buffer1_3;
	// System.Byte* Mono.Globalization.Unicode.SimpleCollator/Context::Buffer2
	uint8_t* ___Buffer2_4;
	// System.Int32 Mono.Globalization.Unicode.SimpleCollator/Context::PrevCode
	int32_t ___PrevCode_5;
	// System.Byte* Mono.Globalization.Unicode.SimpleCollator/Context::PrevSortKey
	uint8_t* ___PrevSortKey_6;
	// System.Boolean Mono.Globalization.Unicode.SimpleCollator/Context::QuickCheckPossible
	bool ___QuickCheckPossible_7;

public:
	inline static int32_t get_offset_of_Option_0() { return static_cast<int32_t>(offsetof(Context_t3303670129, ___Option_0)); }
	inline int32_t get_Option_0() const { return ___Option_0; }
	inline int32_t* get_address_of_Option_0() { return &___Option_0; }
	inline void set_Option_0(int32_t value)
	{
		___Option_0 = value;
	}

	inline static int32_t get_offset_of_NeverMatchFlags_1() { return static_cast<int32_t>(offsetof(Context_t3303670129, ___NeverMatchFlags_1)); }
	inline uint8_t* get_NeverMatchFlags_1() const { return ___NeverMatchFlags_1; }
	inline uint8_t** get_address_of_NeverMatchFlags_1() { return &___NeverMatchFlags_1; }
	inline void set_NeverMatchFlags_1(uint8_t* value)
	{
		___NeverMatchFlags_1 = value;
	}

	inline static int32_t get_offset_of_AlwaysMatchFlags_2() { return static_cast<int32_t>(offsetof(Context_t3303670129, ___AlwaysMatchFlags_2)); }
	inline uint8_t* get_AlwaysMatchFlags_2() const { return ___AlwaysMatchFlags_2; }
	inline uint8_t** get_address_of_AlwaysMatchFlags_2() { return &___AlwaysMatchFlags_2; }
	inline void set_AlwaysMatchFlags_2(uint8_t* value)
	{
		___AlwaysMatchFlags_2 = value;
	}

	inline static int32_t get_offset_of_Buffer1_3() { return static_cast<int32_t>(offsetof(Context_t3303670129, ___Buffer1_3)); }
	inline uint8_t* get_Buffer1_3() const { return ___Buffer1_3; }
	inline uint8_t** get_address_of_Buffer1_3() { return &___Buffer1_3; }
	inline void set_Buffer1_3(uint8_t* value)
	{
		___Buffer1_3 = value;
	}

	inline static int32_t get_offset_of_Buffer2_4() { return static_cast<int32_t>(offsetof(Context_t3303670129, ___Buffer2_4)); }
	inline uint8_t* get_Buffer2_4() const { return ___Buffer2_4; }
	inline uint8_t** get_address_of_Buffer2_4() { return &___Buffer2_4; }
	inline void set_Buffer2_4(uint8_t* value)
	{
		___Buffer2_4 = value;
	}

	inline static int32_t get_offset_of_PrevCode_5() { return static_cast<int32_t>(offsetof(Context_t3303670129, ___PrevCode_5)); }
	inline int32_t get_PrevCode_5() const { return ___PrevCode_5; }
	inline int32_t* get_address_of_PrevCode_5() { return &___PrevCode_5; }
	inline void set_PrevCode_5(int32_t value)
	{
		___PrevCode_5 = value;
	}

	inline static int32_t get_offset_of_PrevSortKey_6() { return static_cast<int32_t>(offsetof(Context_t3303670129, ___PrevSortKey_6)); }
	inline uint8_t* get_PrevSortKey_6() const { return ___PrevSortKey_6; }
	inline uint8_t** get_address_of_PrevSortKey_6() { return &___PrevSortKey_6; }
	inline void set_PrevSortKey_6(uint8_t* value)
	{
		___PrevSortKey_6 = value;
	}

	inline static int32_t get_offset_of_QuickCheckPossible_7() { return static_cast<int32_t>(offsetof(Context_t3303670129, ___QuickCheckPossible_7)); }
	inline bool get_QuickCheckPossible_7() const { return ___QuickCheckPossible_7; }
	inline bool* get_address_of_QuickCheckPossible_7() { return &___QuickCheckPossible_7; }
	inline void set_QuickCheckPossible_7(bool value)
	{
		___QuickCheckPossible_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Mono.Globalization.Unicode.SimpleCollator/Context
struct Context_t3303670129_marshaled_pinvoke
{
	int32_t ___Option_0;
	uint8_t* ___NeverMatchFlags_1;
	uint8_t* ___AlwaysMatchFlags_2;
	uint8_t* ___Buffer1_3;
	uint8_t* ___Buffer2_4;
	int32_t ___PrevCode_5;
	uint8_t* ___PrevSortKey_6;
	int32_t ___QuickCheckPossible_7;
};
// Native definition for COM marshalling of Mono.Globalization.Unicode.SimpleCollator/Context
struct Context_t3303670129_marshaled_com
{
	int32_t ___Option_0;
	uint8_t* ___NeverMatchFlags_1;
	uint8_t* ___AlwaysMatchFlags_2;
	uint8_t* ___Buffer1_3;
	uint8_t* ___Buffer2_4;
	int32_t ___PrevCode_5;
	uint8_t* ___PrevSortKey_6;
	int32_t ___QuickCheckPossible_7;
};
#endif // CONTEXT_T3303670129_H
#ifndef URIFORMATEXCEPTION_T2325300979_H
#define URIFORMATEXCEPTION_T2325300979_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UriFormatException
struct  UriFormatException_t2325300979  : public FormatException_t4223230285
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // URIFORMATEXCEPTION_T2325300979_H
#ifndef MULTICASTDELEGATE_T69367374_H
#define MULTICASTDELEGATE_T69367374_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t69367374  : public Delegate_t96267039
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t69367374 * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t69367374 * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t69367374, ___prev_9)); }
	inline MulticastDelegate_t69367374 * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t69367374 ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t69367374 * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t69367374, ___kpm_next_10)); }
	inline MulticastDelegate_t69367374 * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t69367374 ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t69367374 * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T69367374_H
#ifndef STREAMINGCONTEXT_T1316667400_H
#define STREAMINGCONTEXT_T1316667400_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.StreamingContext
struct  StreamingContext_t1316667400 
{
public:
	// System.Runtime.Serialization.StreamingContextStates System.Runtime.Serialization.StreamingContext::state
	int32_t ___state_0;
	// System.Object System.Runtime.Serialization.StreamingContext::additional
	RuntimeObject * ___additional_1;

public:
	inline static int32_t get_offset_of_state_0() { return static_cast<int32_t>(offsetof(StreamingContext_t1316667400, ___state_0)); }
	inline int32_t get_state_0() const { return ___state_0; }
	inline int32_t* get_address_of_state_0() { return &___state_0; }
	inline void set_state_0(int32_t value)
	{
		___state_0 = value;
	}

	inline static int32_t get_offset_of_additional_1() { return static_cast<int32_t>(offsetof(StreamingContext_t1316667400, ___additional_1)); }
	inline RuntimeObject * get_additional_1() const { return ___additional_1; }
	inline RuntimeObject ** get_address_of_additional_1() { return &___additional_1; }
	inline void set_additional_1(RuntimeObject * value)
	{
		___additional_1 = value;
		Il2CppCodeGenWriteBarrier((&___additional_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Runtime.Serialization.StreamingContext
struct StreamingContext_t1316667400_marshaled_pinvoke
{
	int32_t ___state_0;
	Il2CppIUnknown* ___additional_1;
};
// Native definition for COM marshalling of System.Runtime.Serialization.StreamingContext
struct StreamingContext_t1316667400_marshaled_com
{
	int32_t ___state_0;
	Il2CppIUnknown* ___additional_1;
};
#endif // STREAMINGCONTEXT_T1316667400_H
#ifndef PLAYABLE_T1760619938_H
#define PLAYABLE_T1760619938_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.Playable
struct  Playable_t1760619938 
{
public:
	// UnityEngine.Playables.PlayableHandle UnityEngine.Playables.Playable::m_Handle
	PlayableHandle_t3938927722  ___m_Handle_0;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(Playable_t1760619938, ___m_Handle_0)); }
	inline PlayableHandle_t3938927722  get_m_Handle_0() const { return ___m_Handle_0; }
	inline PlayableHandle_t3938927722 * get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(PlayableHandle_t3938927722  value)
	{
		___m_Handle_0 = value;
	}
};

struct Playable_t1760619938_StaticFields
{
public:
	// UnityEngine.Playables.Playable UnityEngine.Playables.Playable::m_NullPlayable
	Playable_t1760619938  ___m_NullPlayable_1;

public:
	inline static int32_t get_offset_of_m_NullPlayable_1() { return static_cast<int32_t>(offsetof(Playable_t1760619938_StaticFields, ___m_NullPlayable_1)); }
	inline Playable_t1760619938  get_m_NullPlayable_1() const { return ___m_NullPlayable_1; }
	inline Playable_t1760619938 * get_address_of_m_NullPlayable_1() { return &___m_NullPlayable_1; }
	inline void set_m_NullPlayable_1(Playable_t1760619938  value)
	{
		___m_NullPlayable_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYABLE_T1760619938_H
#ifndef MONORESOURCE_T2965441204_H
#define MONORESOURCE_T2965441204_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.Emit.MonoResource
struct  MonoResource_t2965441204 
{
public:
	// System.Byte[] System.Reflection.Emit.MonoResource::data
	ByteU5BU5D_t1014014593* ___data_0;
	// System.String System.Reflection.Emit.MonoResource::name
	String_t* ___name_1;
	// System.String System.Reflection.Emit.MonoResource::filename
	String_t* ___filename_2;
	// System.Reflection.ResourceAttributes System.Reflection.Emit.MonoResource::attrs
	int32_t ___attrs_3;
	// System.Int32 System.Reflection.Emit.MonoResource::offset
	int32_t ___offset_4;
	// System.IO.Stream System.Reflection.Emit.MonoResource::stream
	Stream_t2349536632 * ___stream_5;

public:
	inline static int32_t get_offset_of_data_0() { return static_cast<int32_t>(offsetof(MonoResource_t2965441204, ___data_0)); }
	inline ByteU5BU5D_t1014014593* get_data_0() const { return ___data_0; }
	inline ByteU5BU5D_t1014014593** get_address_of_data_0() { return &___data_0; }
	inline void set_data_0(ByteU5BU5D_t1014014593* value)
	{
		___data_0 = value;
		Il2CppCodeGenWriteBarrier((&___data_0), value);
	}

	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(MonoResource_t2965441204, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier((&___name_1), value);
	}

	inline static int32_t get_offset_of_filename_2() { return static_cast<int32_t>(offsetof(MonoResource_t2965441204, ___filename_2)); }
	inline String_t* get_filename_2() const { return ___filename_2; }
	inline String_t** get_address_of_filename_2() { return &___filename_2; }
	inline void set_filename_2(String_t* value)
	{
		___filename_2 = value;
		Il2CppCodeGenWriteBarrier((&___filename_2), value);
	}

	inline static int32_t get_offset_of_attrs_3() { return static_cast<int32_t>(offsetof(MonoResource_t2965441204, ___attrs_3)); }
	inline int32_t get_attrs_3() const { return ___attrs_3; }
	inline int32_t* get_address_of_attrs_3() { return &___attrs_3; }
	inline void set_attrs_3(int32_t value)
	{
		___attrs_3 = value;
	}

	inline static int32_t get_offset_of_offset_4() { return static_cast<int32_t>(offsetof(MonoResource_t2965441204, ___offset_4)); }
	inline int32_t get_offset_4() const { return ___offset_4; }
	inline int32_t* get_address_of_offset_4() { return &___offset_4; }
	inline void set_offset_4(int32_t value)
	{
		___offset_4 = value;
	}

	inline static int32_t get_offset_of_stream_5() { return static_cast<int32_t>(offsetof(MonoResource_t2965441204, ___stream_5)); }
	inline Stream_t2349536632 * get_stream_5() const { return ___stream_5; }
	inline Stream_t2349536632 ** get_address_of_stream_5() { return &___stream_5; }
	inline void set_stream_5(Stream_t2349536632 * value)
	{
		___stream_5 = value;
		Il2CppCodeGenWriteBarrier((&___stream_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Reflection.Emit.MonoResource
struct MonoResource_t2965441204_marshaled_pinvoke
{
	uint8_t* ___data_0;
	char* ___name_1;
	char* ___filename_2;
	int32_t ___attrs_3;
	int32_t ___offset_4;
	Stream_t2349536632 * ___stream_5;
};
// Native definition for COM marshalling of System.Reflection.Emit.MonoResource
struct MonoResource_t2965441204_marshaled_com
{
	uint8_t* ___data_0;
	Il2CppChar* ___name_1;
	Il2CppChar* ___filename_2;
	int32_t ___attrs_3;
	int32_t ___offset_4;
	Stream_t2349536632 * ___stream_5;
};
#endif // MONORESOURCE_T2965441204_H
#ifndef REFEMITPERMISSIONSET_T945207422_H
#define REFEMITPERMISSIONSET_T945207422_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.Emit.RefEmitPermissionSet
struct  RefEmitPermissionSet_t945207422 
{
public:
	// System.Security.Permissions.SecurityAction System.Reflection.Emit.RefEmitPermissionSet::action
	int32_t ___action_0;
	// System.String System.Reflection.Emit.RefEmitPermissionSet::pset
	String_t* ___pset_1;

public:
	inline static int32_t get_offset_of_action_0() { return static_cast<int32_t>(offsetof(RefEmitPermissionSet_t945207422, ___action_0)); }
	inline int32_t get_action_0() const { return ___action_0; }
	inline int32_t* get_address_of_action_0() { return &___action_0; }
	inline void set_action_0(int32_t value)
	{
		___action_0 = value;
	}

	inline static int32_t get_offset_of_pset_1() { return static_cast<int32_t>(offsetof(RefEmitPermissionSet_t945207422, ___pset_1)); }
	inline String_t* get_pset_1() const { return ___pset_1; }
	inline String_t** get_address_of_pset_1() { return &___pset_1; }
	inline void set_pset_1(String_t* value)
	{
		___pset_1 = value;
		Il2CppCodeGenWriteBarrier((&___pset_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Reflection.Emit.RefEmitPermissionSet
struct RefEmitPermissionSet_t945207422_marshaled_pinvoke
{
	int32_t ___action_0;
	char* ___pset_1;
};
// Native definition for COM marshalling of System.Reflection.Emit.RefEmitPermissionSet
struct RefEmitPermissionSet_t945207422_marshaled_com
{
	int32_t ___action_0;
	Il2CppChar* ___pset_1;
};
#endif // REFEMITPERMISSIONSET_T945207422_H
#ifndef X509CHAINSTATUS_T177750891_H
#define X509CHAINSTATUS_T177750891_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509ChainStatus
struct  X509ChainStatus_t177750891 
{
public:
	// System.Security.Cryptography.X509Certificates.X509ChainStatusFlags System.Security.Cryptography.X509Certificates.X509ChainStatus::status
	int32_t ___status_0;
	// System.String System.Security.Cryptography.X509Certificates.X509ChainStatus::info
	String_t* ___info_1;

public:
	inline static int32_t get_offset_of_status_0() { return static_cast<int32_t>(offsetof(X509ChainStatus_t177750891, ___status_0)); }
	inline int32_t get_status_0() const { return ___status_0; }
	inline int32_t* get_address_of_status_0() { return &___status_0; }
	inline void set_status_0(int32_t value)
	{
		___status_0 = value;
	}

	inline static int32_t get_offset_of_info_1() { return static_cast<int32_t>(offsetof(X509ChainStatus_t177750891, ___info_1)); }
	inline String_t* get_info_1() const { return ___info_1; }
	inline String_t** get_address_of_info_1() { return &___info_1; }
	inline void set_info_1(String_t* value)
	{
		___info_1 = value;
		Il2CppCodeGenWriteBarrier((&___info_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Security.Cryptography.X509Certificates.X509ChainStatus
struct X509ChainStatus_t177750891_marshaled_pinvoke
{
	int32_t ___status_0;
	char* ___info_1;
};
// Native definition for COM marshalling of System.Security.Cryptography.X509Certificates.X509ChainStatus
struct X509ChainStatus_t177750891_marshaled_com
{
	int32_t ___status_0;
	Il2CppChar* ___info_1;
};
#endif // X509CHAINSTATUS_T177750891_H
#ifndef DATETIME_T507798353_H
#define DATETIME_T507798353_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t507798353 
{
public:
	// System.TimeSpan System.DateTime::ticks
	TimeSpan_t2986675223  ___ticks_0;
	// System.DateTimeKind System.DateTime::kind
	int32_t ___kind_1;

public:
	inline static int32_t get_offset_of_ticks_0() { return static_cast<int32_t>(offsetof(DateTime_t507798353, ___ticks_0)); }
	inline TimeSpan_t2986675223  get_ticks_0() const { return ___ticks_0; }
	inline TimeSpan_t2986675223 * get_address_of_ticks_0() { return &___ticks_0; }
	inline void set_ticks_0(TimeSpan_t2986675223  value)
	{
		___ticks_0 = value;
	}

	inline static int32_t get_offset_of_kind_1() { return static_cast<int32_t>(offsetof(DateTime_t507798353, ___kind_1)); }
	inline int32_t get_kind_1() const { return ___kind_1; }
	inline int32_t* get_address_of_kind_1() { return &___kind_1; }
	inline void set_kind_1(int32_t value)
	{
		___kind_1 = value;
	}
};

struct DateTime_t507798353_StaticFields
{
public:
	// System.DateTime System.DateTime::MaxValue
	DateTime_t507798353  ___MaxValue_2;
	// System.DateTime System.DateTime::MinValue
	DateTime_t507798353  ___MinValue_3;
	// System.String[] System.DateTime::ParseTimeFormats
	StringU5BU5D_t522578821* ___ParseTimeFormats_4;
	// System.String[] System.DateTime::ParseYearDayMonthFormats
	StringU5BU5D_t522578821* ___ParseYearDayMonthFormats_5;
	// System.String[] System.DateTime::ParseYearMonthDayFormats
	StringU5BU5D_t522578821* ___ParseYearMonthDayFormats_6;
	// System.String[] System.DateTime::ParseDayMonthYearFormats
	StringU5BU5D_t522578821* ___ParseDayMonthYearFormats_7;
	// System.String[] System.DateTime::ParseMonthDayYearFormats
	StringU5BU5D_t522578821* ___ParseMonthDayYearFormats_8;
	// System.String[] System.DateTime::MonthDayShortFormats
	StringU5BU5D_t522578821* ___MonthDayShortFormats_9;
	// System.String[] System.DateTime::DayMonthShortFormats
	StringU5BU5D_t522578821* ___DayMonthShortFormats_10;
	// System.Int32[] System.DateTime::daysmonth
	Int32U5BU5D_t2856113350* ___daysmonth_11;
	// System.Int32[] System.DateTime::daysmonthleap
	Int32U5BU5D_t2856113350* ___daysmonthleap_12;
	// System.Object System.DateTime::to_local_time_span_object
	RuntimeObject * ___to_local_time_span_object_13;
	// System.Int64 System.DateTime::last_now
	int64_t ___last_now_14;

public:
	inline static int32_t get_offset_of_MaxValue_2() { return static_cast<int32_t>(offsetof(DateTime_t507798353_StaticFields, ___MaxValue_2)); }
	inline DateTime_t507798353  get_MaxValue_2() const { return ___MaxValue_2; }
	inline DateTime_t507798353 * get_address_of_MaxValue_2() { return &___MaxValue_2; }
	inline void set_MaxValue_2(DateTime_t507798353  value)
	{
		___MaxValue_2 = value;
	}

	inline static int32_t get_offset_of_MinValue_3() { return static_cast<int32_t>(offsetof(DateTime_t507798353_StaticFields, ___MinValue_3)); }
	inline DateTime_t507798353  get_MinValue_3() const { return ___MinValue_3; }
	inline DateTime_t507798353 * get_address_of_MinValue_3() { return &___MinValue_3; }
	inline void set_MinValue_3(DateTime_t507798353  value)
	{
		___MinValue_3 = value;
	}

	inline static int32_t get_offset_of_ParseTimeFormats_4() { return static_cast<int32_t>(offsetof(DateTime_t507798353_StaticFields, ___ParseTimeFormats_4)); }
	inline StringU5BU5D_t522578821* get_ParseTimeFormats_4() const { return ___ParseTimeFormats_4; }
	inline StringU5BU5D_t522578821** get_address_of_ParseTimeFormats_4() { return &___ParseTimeFormats_4; }
	inline void set_ParseTimeFormats_4(StringU5BU5D_t522578821* value)
	{
		___ParseTimeFormats_4 = value;
		Il2CppCodeGenWriteBarrier((&___ParseTimeFormats_4), value);
	}

	inline static int32_t get_offset_of_ParseYearDayMonthFormats_5() { return static_cast<int32_t>(offsetof(DateTime_t507798353_StaticFields, ___ParseYearDayMonthFormats_5)); }
	inline StringU5BU5D_t522578821* get_ParseYearDayMonthFormats_5() const { return ___ParseYearDayMonthFormats_5; }
	inline StringU5BU5D_t522578821** get_address_of_ParseYearDayMonthFormats_5() { return &___ParseYearDayMonthFormats_5; }
	inline void set_ParseYearDayMonthFormats_5(StringU5BU5D_t522578821* value)
	{
		___ParseYearDayMonthFormats_5 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearDayMonthFormats_5), value);
	}

	inline static int32_t get_offset_of_ParseYearMonthDayFormats_6() { return static_cast<int32_t>(offsetof(DateTime_t507798353_StaticFields, ___ParseYearMonthDayFormats_6)); }
	inline StringU5BU5D_t522578821* get_ParseYearMonthDayFormats_6() const { return ___ParseYearMonthDayFormats_6; }
	inline StringU5BU5D_t522578821** get_address_of_ParseYearMonthDayFormats_6() { return &___ParseYearMonthDayFormats_6; }
	inline void set_ParseYearMonthDayFormats_6(StringU5BU5D_t522578821* value)
	{
		___ParseYearMonthDayFormats_6 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearMonthDayFormats_6), value);
	}

	inline static int32_t get_offset_of_ParseDayMonthYearFormats_7() { return static_cast<int32_t>(offsetof(DateTime_t507798353_StaticFields, ___ParseDayMonthYearFormats_7)); }
	inline StringU5BU5D_t522578821* get_ParseDayMonthYearFormats_7() const { return ___ParseDayMonthYearFormats_7; }
	inline StringU5BU5D_t522578821** get_address_of_ParseDayMonthYearFormats_7() { return &___ParseDayMonthYearFormats_7; }
	inline void set_ParseDayMonthYearFormats_7(StringU5BU5D_t522578821* value)
	{
		___ParseDayMonthYearFormats_7 = value;
		Il2CppCodeGenWriteBarrier((&___ParseDayMonthYearFormats_7), value);
	}

	inline static int32_t get_offset_of_ParseMonthDayYearFormats_8() { return static_cast<int32_t>(offsetof(DateTime_t507798353_StaticFields, ___ParseMonthDayYearFormats_8)); }
	inline StringU5BU5D_t522578821* get_ParseMonthDayYearFormats_8() const { return ___ParseMonthDayYearFormats_8; }
	inline StringU5BU5D_t522578821** get_address_of_ParseMonthDayYearFormats_8() { return &___ParseMonthDayYearFormats_8; }
	inline void set_ParseMonthDayYearFormats_8(StringU5BU5D_t522578821* value)
	{
		___ParseMonthDayYearFormats_8 = value;
		Il2CppCodeGenWriteBarrier((&___ParseMonthDayYearFormats_8), value);
	}

	inline static int32_t get_offset_of_MonthDayShortFormats_9() { return static_cast<int32_t>(offsetof(DateTime_t507798353_StaticFields, ___MonthDayShortFormats_9)); }
	inline StringU5BU5D_t522578821* get_MonthDayShortFormats_9() const { return ___MonthDayShortFormats_9; }
	inline StringU5BU5D_t522578821** get_address_of_MonthDayShortFormats_9() { return &___MonthDayShortFormats_9; }
	inline void set_MonthDayShortFormats_9(StringU5BU5D_t522578821* value)
	{
		___MonthDayShortFormats_9 = value;
		Il2CppCodeGenWriteBarrier((&___MonthDayShortFormats_9), value);
	}

	inline static int32_t get_offset_of_DayMonthShortFormats_10() { return static_cast<int32_t>(offsetof(DateTime_t507798353_StaticFields, ___DayMonthShortFormats_10)); }
	inline StringU5BU5D_t522578821* get_DayMonthShortFormats_10() const { return ___DayMonthShortFormats_10; }
	inline StringU5BU5D_t522578821** get_address_of_DayMonthShortFormats_10() { return &___DayMonthShortFormats_10; }
	inline void set_DayMonthShortFormats_10(StringU5BU5D_t522578821* value)
	{
		___DayMonthShortFormats_10 = value;
		Il2CppCodeGenWriteBarrier((&___DayMonthShortFormats_10), value);
	}

	inline static int32_t get_offset_of_daysmonth_11() { return static_cast<int32_t>(offsetof(DateTime_t507798353_StaticFields, ___daysmonth_11)); }
	inline Int32U5BU5D_t2856113350* get_daysmonth_11() const { return ___daysmonth_11; }
	inline Int32U5BU5D_t2856113350** get_address_of_daysmonth_11() { return &___daysmonth_11; }
	inline void set_daysmonth_11(Int32U5BU5D_t2856113350* value)
	{
		___daysmonth_11 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonth_11), value);
	}

	inline static int32_t get_offset_of_daysmonthleap_12() { return static_cast<int32_t>(offsetof(DateTime_t507798353_StaticFields, ___daysmonthleap_12)); }
	inline Int32U5BU5D_t2856113350* get_daysmonthleap_12() const { return ___daysmonthleap_12; }
	inline Int32U5BU5D_t2856113350** get_address_of_daysmonthleap_12() { return &___daysmonthleap_12; }
	inline void set_daysmonthleap_12(Int32U5BU5D_t2856113350* value)
	{
		___daysmonthleap_12 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonthleap_12), value);
	}

	inline static int32_t get_offset_of_to_local_time_span_object_13() { return static_cast<int32_t>(offsetof(DateTime_t507798353_StaticFields, ___to_local_time_span_object_13)); }
	inline RuntimeObject * get_to_local_time_span_object_13() const { return ___to_local_time_span_object_13; }
	inline RuntimeObject ** get_address_of_to_local_time_span_object_13() { return &___to_local_time_span_object_13; }
	inline void set_to_local_time_span_object_13(RuntimeObject * value)
	{
		___to_local_time_span_object_13 = value;
		Il2CppCodeGenWriteBarrier((&___to_local_time_span_object_13), value);
	}

	inline static int32_t get_offset_of_last_now_14() { return static_cast<int32_t>(offsetof(DateTime_t507798353_StaticFields, ___last_now_14)); }
	inline int64_t get_last_now_14() const { return ___last_now_14; }
	inline int64_t* get_address_of_last_now_14() { return &___last_now_14; }
	inline void set_last_now_14(int64_t value)
	{
		___last_now_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T507798353_H
#ifndef PLAYABLEBINDING_T136667248_H
#define PLAYABLEBINDING_T136667248_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.PlayableBinding
struct  PlayableBinding_t136667248 
{
public:
	union
	{
		struct
		{
			// System.String UnityEngine.Playables.PlayableBinding::<streamName>k__BackingField
			String_t* ___U3CstreamNameU3Ek__BackingField_2;
			// UnityEngine.Playables.DataStreamType UnityEngine.Playables.PlayableBinding::<streamType>k__BackingField
			int32_t ___U3CstreamTypeU3Ek__BackingField_3;
			// UnityEngine.Object UnityEngine.Playables.PlayableBinding::<sourceObject>k__BackingField
			Object_t2461733472 * ___U3CsourceObjectU3Ek__BackingField_4;
			// System.Type UnityEngine.Playables.PlayableBinding::<sourceBindingType>k__BackingField
			Type_t * ___U3CsourceBindingTypeU3Ek__BackingField_5;
		};
		uint8_t PlayableBinding_t136667248__padding[1];
	};

public:
	inline static int32_t get_offset_of_U3CstreamNameU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(PlayableBinding_t136667248, ___U3CstreamNameU3Ek__BackingField_2)); }
	inline String_t* get_U3CstreamNameU3Ek__BackingField_2() const { return ___U3CstreamNameU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CstreamNameU3Ek__BackingField_2() { return &___U3CstreamNameU3Ek__BackingField_2; }
	inline void set_U3CstreamNameU3Ek__BackingField_2(String_t* value)
	{
		___U3CstreamNameU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CstreamNameU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CstreamTypeU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(PlayableBinding_t136667248, ___U3CstreamTypeU3Ek__BackingField_3)); }
	inline int32_t get_U3CstreamTypeU3Ek__BackingField_3() const { return ___U3CstreamTypeU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CstreamTypeU3Ek__BackingField_3() { return &___U3CstreamTypeU3Ek__BackingField_3; }
	inline void set_U3CstreamTypeU3Ek__BackingField_3(int32_t value)
	{
		___U3CstreamTypeU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CsourceObjectU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(PlayableBinding_t136667248, ___U3CsourceObjectU3Ek__BackingField_4)); }
	inline Object_t2461733472 * get_U3CsourceObjectU3Ek__BackingField_4() const { return ___U3CsourceObjectU3Ek__BackingField_4; }
	inline Object_t2461733472 ** get_address_of_U3CsourceObjectU3Ek__BackingField_4() { return &___U3CsourceObjectU3Ek__BackingField_4; }
	inline void set_U3CsourceObjectU3Ek__BackingField_4(Object_t2461733472 * value)
	{
		___U3CsourceObjectU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsourceObjectU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CsourceBindingTypeU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(PlayableBinding_t136667248, ___U3CsourceBindingTypeU3Ek__BackingField_5)); }
	inline Type_t * get_U3CsourceBindingTypeU3Ek__BackingField_5() const { return ___U3CsourceBindingTypeU3Ek__BackingField_5; }
	inline Type_t ** get_address_of_U3CsourceBindingTypeU3Ek__BackingField_5() { return &___U3CsourceBindingTypeU3Ek__BackingField_5; }
	inline void set_U3CsourceBindingTypeU3Ek__BackingField_5(Type_t * value)
	{
		___U3CsourceBindingTypeU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsourceBindingTypeU3Ek__BackingField_5), value);
	}
};

struct PlayableBinding_t136667248_StaticFields
{
public:
	// UnityEngine.Playables.PlayableBinding[] UnityEngine.Playables.PlayableBinding::None
	PlayableBindingU5BU5D_t207642577* ___None_0;
	// System.Double UnityEngine.Playables.PlayableBinding::DefaultDuration
	double ___DefaultDuration_1;

public:
	inline static int32_t get_offset_of_None_0() { return static_cast<int32_t>(offsetof(PlayableBinding_t136667248_StaticFields, ___None_0)); }
	inline PlayableBindingU5BU5D_t207642577* get_None_0() const { return ___None_0; }
	inline PlayableBindingU5BU5D_t207642577** get_address_of_None_0() { return &___None_0; }
	inline void set_None_0(PlayableBindingU5BU5D_t207642577* value)
	{
		___None_0 = value;
		Il2CppCodeGenWriteBarrier((&___None_0), value);
	}

	inline static int32_t get_offset_of_DefaultDuration_1() { return static_cast<int32_t>(offsetof(PlayableBinding_t136667248_StaticFields, ___DefaultDuration_1)); }
	inline double get_DefaultDuration_1() const { return ___DefaultDuration_1; }
	inline double* get_address_of_DefaultDuration_1() { return &___DefaultDuration_1; }
	inline void set_DefaultDuration_1(double value)
	{
		___DefaultDuration_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Playables.PlayableBinding
struct PlayableBinding_t136667248_marshaled_pinvoke
{
	union
	{
		struct
		{
			char* ___U3CstreamNameU3Ek__BackingField_2;
			int32_t ___U3CstreamTypeU3Ek__BackingField_3;
			Object_t2461733472_marshaled_pinvoke ___U3CsourceObjectU3Ek__BackingField_4;
			Type_t * ___U3CsourceBindingTypeU3Ek__BackingField_5;
		};
		uint8_t PlayableBinding_t136667248__padding[1];
	};
};
// Native definition for COM marshalling of UnityEngine.Playables.PlayableBinding
struct PlayableBinding_t136667248_marshaled_com
{
	union
	{
		struct
		{
			Il2CppChar* ___U3CstreamNameU3Ek__BackingField_2;
			int32_t ___U3CstreamTypeU3Ek__BackingField_3;
			Object_t2461733472_marshaled_com* ___U3CsourceObjectU3Ek__BackingField_4;
			Type_t * ___U3CsourceBindingTypeU3Ek__BackingField_5;
		};
		uint8_t PlayableBinding_t136667248__padding[1];
	};
};
#endif // PLAYABLEBINDING_T136667248_H
#ifndef ENUMERATOR_T767066857_H
#define ENUMERATOR_T767066857_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,System.Object>
struct  Enumerator_t767066857 
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator::dictionary
	Dictionary_2_t287235217 * ___dictionary_0;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::stamp
	int32_t ___stamp_2;
	// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator::current
	KeyValuePair_2_t2204764377  ___current_3;

public:
	inline static int32_t get_offset_of_dictionary_0() { return static_cast<int32_t>(offsetof(Enumerator_t767066857, ___dictionary_0)); }
	inline Dictionary_2_t287235217 * get_dictionary_0() const { return ___dictionary_0; }
	inline Dictionary_2_t287235217 ** get_address_of_dictionary_0() { return &___dictionary_0; }
	inline void set_dictionary_0(Dictionary_2_t287235217 * value)
	{
		___dictionary_0 = value;
		Il2CppCodeGenWriteBarrier((&___dictionary_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t767066857, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_stamp_2() { return static_cast<int32_t>(offsetof(Enumerator_t767066857, ___stamp_2)); }
	inline int32_t get_stamp_2() const { return ___stamp_2; }
	inline int32_t* get_address_of_stamp_2() { return &___stamp_2; }
	inline void set_stamp_2(int32_t value)
	{
		___stamp_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t767066857, ___current_3)); }
	inline KeyValuePair_2_t2204764377  get_current_3() const { return ___current_3; }
	inline KeyValuePair_2_t2204764377 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(KeyValuePair_2_t2204764377  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T767066857_H
#ifndef MONOPROPERTYINFO_T3452547385_H
#define MONOPROPERTYINFO_T3452547385_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MonoPropertyInfo
struct  MonoPropertyInfo_t3452547385 
{
public:
	// System.Type System.Reflection.MonoPropertyInfo::parent
	Type_t * ___parent_0;
	// System.String System.Reflection.MonoPropertyInfo::name
	String_t* ___name_1;
	// System.Reflection.MethodInfo System.Reflection.MonoPropertyInfo::get_method
	MethodInfo_t * ___get_method_2;
	// System.Reflection.MethodInfo System.Reflection.MonoPropertyInfo::set_method
	MethodInfo_t * ___set_method_3;
	// System.Reflection.PropertyAttributes System.Reflection.MonoPropertyInfo::attrs
	int32_t ___attrs_4;

public:
	inline static int32_t get_offset_of_parent_0() { return static_cast<int32_t>(offsetof(MonoPropertyInfo_t3452547385, ___parent_0)); }
	inline Type_t * get_parent_0() const { return ___parent_0; }
	inline Type_t ** get_address_of_parent_0() { return &___parent_0; }
	inline void set_parent_0(Type_t * value)
	{
		___parent_0 = value;
		Il2CppCodeGenWriteBarrier((&___parent_0), value);
	}

	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(MonoPropertyInfo_t3452547385, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier((&___name_1), value);
	}

	inline static int32_t get_offset_of_get_method_2() { return static_cast<int32_t>(offsetof(MonoPropertyInfo_t3452547385, ___get_method_2)); }
	inline MethodInfo_t * get_get_method_2() const { return ___get_method_2; }
	inline MethodInfo_t ** get_address_of_get_method_2() { return &___get_method_2; }
	inline void set_get_method_2(MethodInfo_t * value)
	{
		___get_method_2 = value;
		Il2CppCodeGenWriteBarrier((&___get_method_2), value);
	}

	inline static int32_t get_offset_of_set_method_3() { return static_cast<int32_t>(offsetof(MonoPropertyInfo_t3452547385, ___set_method_3)); }
	inline MethodInfo_t * get_set_method_3() const { return ___set_method_3; }
	inline MethodInfo_t ** get_address_of_set_method_3() { return &___set_method_3; }
	inline void set_set_method_3(MethodInfo_t * value)
	{
		___set_method_3 = value;
		Il2CppCodeGenWriteBarrier((&___set_method_3), value);
	}

	inline static int32_t get_offset_of_attrs_4() { return static_cast<int32_t>(offsetof(MonoPropertyInfo_t3452547385, ___attrs_4)); }
	inline int32_t get_attrs_4() const { return ___attrs_4; }
	inline int32_t* get_address_of_attrs_4() { return &___attrs_4; }
	inline void set_attrs_4(int32_t value)
	{
		___attrs_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Reflection.MonoPropertyInfo
struct MonoPropertyInfo_t3452547385_marshaled_pinvoke
{
	Type_t * ___parent_0;
	char* ___name_1;
	MethodInfo_t * ___get_method_2;
	MethodInfo_t * ___set_method_3;
	int32_t ___attrs_4;
};
// Native definition for COM marshalling of System.Reflection.MonoPropertyInfo
struct MonoPropertyInfo_t3452547385_marshaled_com
{
	Type_t * ___parent_0;
	Il2CppChar* ___name_1;
	MethodInfo_t * ___get_method_2;
	MethodInfo_t * ___set_method_3;
	int32_t ___attrs_4;
};
#endif // MONOPROPERTYINFO_T3452547385_H
#ifndef DATETIMEOFFSET_T2220763929_H
#define DATETIMEOFFSET_T2220763929_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTimeOffset
struct  DateTimeOffset_t2220763929 
{
public:
	// System.DateTime System.DateTimeOffset::dt
	DateTime_t507798353  ___dt_2;
	// System.TimeSpan System.DateTimeOffset::utc_offset
	TimeSpan_t2986675223  ___utc_offset_3;

public:
	inline static int32_t get_offset_of_dt_2() { return static_cast<int32_t>(offsetof(DateTimeOffset_t2220763929, ___dt_2)); }
	inline DateTime_t507798353  get_dt_2() const { return ___dt_2; }
	inline DateTime_t507798353 * get_address_of_dt_2() { return &___dt_2; }
	inline void set_dt_2(DateTime_t507798353  value)
	{
		___dt_2 = value;
	}

	inline static int32_t get_offset_of_utc_offset_3() { return static_cast<int32_t>(offsetof(DateTimeOffset_t2220763929, ___utc_offset_3)); }
	inline TimeSpan_t2986675223  get_utc_offset_3() const { return ___utc_offset_3; }
	inline TimeSpan_t2986675223 * get_address_of_utc_offset_3() { return &___utc_offset_3; }
	inline void set_utc_offset_3(TimeSpan_t2986675223  value)
	{
		___utc_offset_3 = value;
	}
};

struct DateTimeOffset_t2220763929_StaticFields
{
public:
	// System.DateTimeOffset System.DateTimeOffset::MaxValue
	DateTimeOffset_t2220763929  ___MaxValue_0;
	// System.DateTimeOffset System.DateTimeOffset::MinValue
	DateTimeOffset_t2220763929  ___MinValue_1;

public:
	inline static int32_t get_offset_of_MaxValue_0() { return static_cast<int32_t>(offsetof(DateTimeOffset_t2220763929_StaticFields, ___MaxValue_0)); }
	inline DateTimeOffset_t2220763929  get_MaxValue_0() const { return ___MaxValue_0; }
	inline DateTimeOffset_t2220763929 * get_address_of_MaxValue_0() { return &___MaxValue_0; }
	inline void set_MaxValue_0(DateTimeOffset_t2220763929  value)
	{
		___MaxValue_0 = value;
	}

	inline static int32_t get_offset_of_MinValue_1() { return static_cast<int32_t>(offsetof(DateTimeOffset_t2220763929_StaticFields, ___MinValue_1)); }
	inline DateTimeOffset_t2220763929  get_MinValue_1() const { return ___MinValue_1; }
	inline DateTimeOffset_t2220763929 * get_address_of_MinValue_1() { return &___MinValue_1; }
	inline void set_MinValue_1(DateTimeOffset_t2220763929  value)
	{
		___MinValue_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMEOFFSET_T2220763929_H
// System.Object[]
struct ObjectU5BU5D_t4290686858  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject * m_Items[1];

public:
	inline RuntimeObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline RuntimeObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Mono.Globalization.Unicode.Contraction[]
struct ContractionU5BU5D_t398144010  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Contraction_t3257534955 * m_Items[1];

public:
	inline Contraction_t3257534955 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Contraction_t3257534955 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Contraction_t3257534955 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Contraction_t3257534955 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Contraction_t3257534955 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Contraction_t3257534955 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Mono.Globalization.Unicode.Level2Map[]
struct Level2MapU5BU5D_t70716484  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Level2Map_t3905868985 * m_Items[1];

public:
	inline Level2Map_t3905868985 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Level2Map_t3905868985 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Level2Map_t3905868985 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Level2Map_t3905868985 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Level2Map_t3905868985 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Level2Map_t3905868985 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Byte[]
struct ByteU5BU5D_t1014014593  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) uint8_t m_Items[1];

public:
	inline uint8_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline uint8_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, uint8_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline uint8_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline uint8_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, uint8_t value)
	{
		m_Items[index] = value;
	}
};
// System.Runtime.Remoting.Messaging.Header[]
struct HeaderU5BU5D_t1468929288  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Header_t2445250629 * m_Items[1];

public:
	inline Header_t2445250629 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Header_t2445250629 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Header_t2445250629 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Header_t2445250629 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Header_t2445250629 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Header_t2445250629 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Char[]
struct CharU5BU5D_t2864810077  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Il2CppChar m_Items[1];

public:
	inline Il2CppChar GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Il2CppChar* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Il2CppChar value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Il2CppChar GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Il2CppChar* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Il2CppChar value)
	{
		m_Items[index] = value;
	}
};
// System.Int64[]
struct Int64U5BU5D_t3591546736  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) int64_t m_Items[1];

public:
	inline int64_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int64_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int64_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int64_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int64_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int64_t value)
	{
		m_Items[index] = value;
	}
};
// System.String[]
struct StringU5BU5D_t522578821  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) String_t* m_Items[1];

public:
	inline String_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline String_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, String_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline String_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline String_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, String_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.SocialPlatforms.Impl.UserProfile[]
struct UserProfileU5BU5D_t4189818181  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) UserProfile_t3211800844 * m_Items[1];

public:
	inline UserProfile_t3211800844 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline UserProfile_t3211800844 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, UserProfile_t3211800844 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline UserProfile_t3211800844 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline UserProfile_t3211800844 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, UserProfile_t3211800844 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Int32[]
struct Int32U5BU5D_t2856113350  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int32_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int32_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int32_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// System.Reflection.CustomAttributeNamedArgument[]
struct CustomAttributeNamedArgumentU5BU5D_t3127017202  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) CustomAttributeNamedArgument_t425730339  m_Items[1];

public:
	inline CustomAttributeNamedArgument_t425730339  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline CustomAttributeNamedArgument_t425730339 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, CustomAttributeNamedArgument_t425730339  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline CustomAttributeNamedArgument_t425730339  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline CustomAttributeNamedArgument_t425730339 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, CustomAttributeNamedArgument_t425730339  value)
	{
		m_Items[index] = value;
	}
};
// System.Reflection.CustomAttributeTypedArgument[]
struct CustomAttributeTypedArgumentU5BU5D_t2668173852  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) CustomAttributeTypedArgument_t2667702721  m_Items[1];

public:
	inline CustomAttributeTypedArgument_t2667702721  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline CustomAttributeTypedArgument_t2667702721 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, CustomAttributeTypedArgument_t2667702721  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline CustomAttributeTypedArgument_t2667702721  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline CustomAttributeTypedArgument_t2667702721 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, CustomAttributeTypedArgument_t2667702721  value)
	{
		m_Items[index] = value;
	}
};



void* RuntimeInvoker_Void_t1078098495 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, const RuntimeMethod* method);
	((Func)methodPointer)(obj, methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Boolean_t2059672899_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, RuntimeObject * p1, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_RuntimeObject_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, RuntimeObject * p1, RuntimeObject * p2, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], (RuntimeObject *)args[1], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, methodMetadata);
	return ret;
}

void* RuntimeInvoker_Int32_t2571135199_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RuntimeObject * p1, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_RuntimeObject_RuntimeObject_ObjectU5BU5DU26_t3608108294 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, RuntimeObject * p1, RuntimeObject * p2, ObjectU5BU5D_t4290686858** p3, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], (RuntimeObject *)args[1], (ObjectU5BU5D_t4290686858**)args[2], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int32_t2571135199_RuntimeObject_ObjectU5BU5DU26_t3608108294 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RuntimeObject * p1, ObjectU5BU5D_t4290686858** p2, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], (ObjectU5BU5D_t4290686858**)args[1], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Void_t1078098495_RuntimeObject_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RuntimeObject * p1, RuntimeObject * p2, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (RuntimeObject *)args[0], (RuntimeObject *)args[1], methodMetadata);
	return NULL;
}

void* RuntimeInvoker_RuntimeObject_RuntimeObject_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, RuntimeObject * p1, RuntimeObject * p2, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], (RuntimeObject *)args[1], methodMetadata);
	return ret;
}

void* RuntimeInvoker_RuntimeObject_RuntimeObject_RuntimeObject_SByte_t2530277047 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, RuntimeObject * p1, RuntimeObject * p2, int8_t p3, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], (RuntimeObject *)args[1], *((int8_t*)args[2]), methodMetadata);
	return ret;
}

void* RuntimeInvoker_Boolean_t2059672899_RuntimeObject_RuntimeObject_SByte_t2530277047 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, RuntimeObject * p1, RuntimeObject * p2, int8_t p3, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], (RuntimeObject *)args[1], *((int8_t*)args[2]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Byte_t880488320_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, RuntimeObject * p1, const RuntimeMethod* method);
	uint8_t ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Char_t3572527572_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef Il2CppChar (*Func)(void* obj, RuntimeObject * p1, const RuntimeMethod* method);
	Il2CppChar ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_DateTime_t507798353_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef DateTime_t507798353  (*Func)(void* obj, RuntimeObject * p1, const RuntimeMethod* method);
	DateTime_t507798353  ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Decimal_t2656901032_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef Decimal_t2656901032  (*Func)(void* obj, RuntimeObject * p1, const RuntimeMethod* method);
	Decimal_t2656901032  ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Double_t1454265465_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef double (*Func)(void* obj, RuntimeObject * p1, const RuntimeMethod* method);
	double ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int16_t769030278_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, RuntimeObject * p1, const RuntimeMethod* method);
	int16_t ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int64_t384086013_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, RuntimeObject * p1, const RuntimeMethod* method);
	int64_t ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_SByte_t2530277047_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, RuntimeObject * p1, const RuntimeMethod* method);
	int8_t ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Single_t496865882_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef float (*Func)(void* obj, RuntimeObject * p1, const RuntimeMethod* method);
	float ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_UInt16_t11709673_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, RuntimeObject * p1, const RuntimeMethod* method);
	uint16_t ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_UInt32_t2739056616_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, RuntimeObject * p1, const RuntimeMethod* method);
	uint32_t ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_UInt64_t2265270229_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, RuntimeObject * p1, const RuntimeMethod* method);
	uint64_t ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int32_t2571135199_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, *((int32_t*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, *((int32_t*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_SByte_t2530277047_RuntimeObject_Int32_t2571135199_ExceptionU26_t1670763973 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int8_t p1, RuntimeObject * p2, int32_t p3, Exception_t3361176243 ** p4, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, *((int8_t*)args[0]), (RuntimeObject *)args[1], *((int32_t*)args[2]), (Exception_t3361176243 **)args[3], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_RuntimeObject_SByte_t2530277047_Int32U26_t2596978809_ExceptionU26_t1670763973 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, RuntimeObject * p1, int8_t p2, int32_t* p3, Exception_t3361176243 ** p4, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int8_t*)args[1]), (int32_t*)args[2], (Exception_t3361176243 **)args[3], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int32_t2571135199_RuntimeObject_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RuntimeObject * p1, RuntimeObject * p2, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], (RuntimeObject *)args[1], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_Int32_t2571135199_SByte_t2530277047_ExceptionU26_t1670763973 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int8_t p2, Exception_t3361176243 ** p3, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, *((int32_t*)args[0]), *((int8_t*)args[1]), (Exception_t3361176243 **)args[2], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_Int32U26_t2596978809_RuntimeObject_SByte_t2530277047_SByte_t2530277047_ExceptionU26_t1670763973 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, RuntimeObject * p2, int8_t p3, int8_t p4, Exception_t3361176243 ** p5, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, (int32_t*)args[0], (RuntimeObject *)args[1], *((int8_t*)args[2]), *((int8_t*)args[3]), (Exception_t3361176243 **)args[4], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Void_t1078098495_Int32U26_t2596978809_RuntimeObject_RuntimeObject_BooleanU26_t495120821_BooleanU26_t495120821 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, RuntimeObject * p2, RuntimeObject * p3, bool* p4, bool* p5, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (int32_t*)args[0], (RuntimeObject *)args[1], (RuntimeObject *)args[2], (bool*)args[3], (bool*)args[4], methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_Int32U26_t2596978809_RuntimeObject_RuntimeObject_BooleanU26_t495120821 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, RuntimeObject * p2, RuntimeObject * p3, bool* p4, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (int32_t*)args[0], (RuntimeObject *)args[1], (RuntimeObject *)args[2], (bool*)args[3], methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Boolean_t2059672899_Int32U26_t2596978809_RuntimeObject_Int32U26_t2596978809_SByte_t2530277047_ExceptionU26_t1670763973 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, RuntimeObject * p2, int32_t* p3, int8_t p4, Exception_t3361176243 ** p5, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, (int32_t*)args[0], (RuntimeObject *)args[1], (int32_t*)args[2], *((int8_t*)args[3]), (Exception_t3361176243 **)args[4], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_Int32U26_t2596978809_RuntimeObject_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, RuntimeObject * p2, RuntimeObject * p3, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, (int32_t*)args[0], (RuntimeObject *)args[1], (RuntimeObject *)args[2], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_Int16_t769030278_SByte_t2530277047 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int16_t p1, int8_t p2, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, *((int16_t*)args[0]), *((int8_t*)args[1]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_RuntimeObject_Int32_t2571135199_RuntimeObject_SByte_t2530277047_Int32U26_t2596978809_ExceptionU26_t1670763973 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, RuntimeObject * p1, int32_t p2, RuntimeObject * p3, int8_t p4, int32_t* p5, Exception_t3361176243 ** p6, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), (RuntimeObject *)args[2], *((int8_t*)args[3]), (int32_t*)args[4], (Exception_t3361176243 **)args[5], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int32_t2571135199_RuntimeObject_Int32_t2571135199_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RuntimeObject * p1, int32_t p2, RuntimeObject * p3, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), (RuntimeObject *)args[2], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_RuntimeObject_Int32U26_t2596978809 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, RuntimeObject * p1, int32_t* p2, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], (int32_t*)args[1], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_RuntimeObject_Int32_t2571135199_RuntimeObject_Int32U26_t2596978809 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, RuntimeObject * p1, int32_t p2, RuntimeObject * p3, int32_t* p4, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), (RuntimeObject *)args[2], (int32_t*)args[3], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_RuntimeObject_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, RuntimeObject * p1, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], methodMetadata);
	return ret;
}

void* RuntimeInvoker_Void_t1078098495_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((int32_t*)args[0]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Boolean_t2059672899 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Void_t1078098495_SByte_t2530277047 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((int8_t*)args[0]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Int32_t2571135199_Int64_t384086013 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int64_t p1, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, *((int64_t*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_Int64_t384086013 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int64_t p1, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, *((int64_t*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_RuntimeObject_SByte_t2530277047_Int64U26_t4038210315_ExceptionU26_t1670763973 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, RuntimeObject * p1, int8_t p2, int64_t* p3, Exception_t3361176243 ** p4, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int8_t*)args[1]), (int64_t*)args[2], (Exception_t3361176243 **)args[3], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int64_t384086013_RuntimeObject_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, RuntimeObject * p1, RuntimeObject * p2, const RuntimeMethod* method);
	int64_t ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], (RuntimeObject *)args[1], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_RuntimeObject_Int32_t2571135199_RuntimeObject_SByte_t2530277047_Int64U26_t4038210315_ExceptionU26_t1670763973 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, RuntimeObject * p1, int32_t p2, RuntimeObject * p3, int8_t p4, int64_t* p5, Exception_t3361176243 ** p6, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), (RuntimeObject *)args[2], *((int8_t*)args[3]), (int64_t*)args[4], (Exception_t3361176243 **)args[5], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int64_t384086013_RuntimeObject_Int32_t2571135199_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, RuntimeObject * p1, int32_t p2, RuntimeObject * p3, const RuntimeMethod* method);
	int64_t ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), (RuntimeObject *)args[2], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_RuntimeObject_Int64U26_t4038210315 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, RuntimeObject * p1, int64_t* p2, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], (int64_t*)args[1], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_RuntimeObject_Int32_t2571135199_RuntimeObject_Int64U26_t4038210315 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, RuntimeObject * p1, int32_t p2, RuntimeObject * p3, int64_t* p4, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), (RuntimeObject *)args[2], (int64_t*)args[3], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_RuntimeObject_SByte_t2530277047_UInt32U26_t3679184984_ExceptionU26_t1670763973 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, RuntimeObject * p1, int8_t p2, uint32_t* p3, Exception_t3361176243 ** p4, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int8_t*)args[1]), (uint32_t*)args[2], (Exception_t3361176243 **)args[3], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_RuntimeObject_Int32_t2571135199_RuntimeObject_SByte_t2530277047_UInt32U26_t3679184984_ExceptionU26_t1670763973 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, RuntimeObject * p1, int32_t p2, RuntimeObject * p3, int8_t p4, uint32_t* p5, Exception_t3361176243 ** p6, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), (RuntimeObject *)args[2], *((int8_t*)args[3]), (uint32_t*)args[4], (Exception_t3361176243 **)args[5], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_UInt32_t2739056616_RuntimeObject_Int32_t2571135199_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, RuntimeObject * p1, int32_t p2, RuntimeObject * p3, const RuntimeMethod* method);
	uint32_t ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), (RuntimeObject *)args[2], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_UInt32_t2739056616_RuntimeObject_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, RuntimeObject * p1, RuntimeObject * p2, const RuntimeMethod* method);
	uint32_t ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], (RuntimeObject *)args[1], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_RuntimeObject_UInt32U26_t3679184984 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, RuntimeObject * p1, uint32_t* p2, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], (uint32_t*)args[1], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_RuntimeObject_Int32_t2571135199_RuntimeObject_UInt32U26_t3679184984 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, RuntimeObject * p1, int32_t p2, RuntimeObject * p3, uint32_t* p4, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), (RuntimeObject *)args[2], (uint32_t*)args[3], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_UInt64_t2265270229_RuntimeObject_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, RuntimeObject * p1, RuntimeObject * p2, const RuntimeMethod* method);
	uint64_t ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], (RuntimeObject *)args[1], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_RuntimeObject_Int32_t2571135199_RuntimeObject_SByte_t2530277047_UInt64U26_t3595557107_ExceptionU26_t1670763973 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, RuntimeObject * p1, int32_t p2, RuntimeObject * p3, int8_t p4, uint64_t* p5, Exception_t3361176243 ** p6, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), (RuntimeObject *)args[2], *((int8_t*)args[3]), (uint64_t*)args[4], (Exception_t3361176243 **)args[5], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_UInt64_t2265270229_RuntimeObject_Int32_t2571135199_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, RuntimeObject * p1, int32_t p2, RuntimeObject * p3, const RuntimeMethod* method);
	uint64_t ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), (RuntimeObject *)args[2], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_RuntimeObject_UInt64U26_t3595557107 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, RuntimeObject * p1, uint64_t* p2, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], (uint64_t*)args[1], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int32_t2571135199_SByte_t2530277047 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int8_t p1, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, *((int8_t*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_SByte_t2530277047 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int8_t p1, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, *((int8_t*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Byte_t880488320_RuntimeObject_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, RuntimeObject * p1, RuntimeObject * p2, const RuntimeMethod* method);
	uint8_t ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], (RuntimeObject *)args[1], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Byte_t880488320_RuntimeObject_Int32_t2571135199_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, RuntimeObject * p1, int32_t p2, RuntimeObject * p3, const RuntimeMethod* method);
	uint8_t ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), (RuntimeObject *)args[2], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_RuntimeObject_ByteU26_t3158801536 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, RuntimeObject * p1, uint8_t* p2, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], (uint8_t*)args[1], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_RuntimeObject_Int32_t2571135199_RuntimeObject_ByteU26_t3158801536 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, RuntimeObject * p1, int32_t p2, RuntimeObject * p3, uint8_t* p4, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), (RuntimeObject *)args[2], (uint8_t*)args[3], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_RuntimeObject_SByte_t2530277047_SByteU26_t1964499041_ExceptionU26_t1670763973 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, RuntimeObject * p1, int8_t p2, int8_t* p3, Exception_t3361176243 ** p4, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int8_t*)args[1]), (int8_t*)args[2], (Exception_t3361176243 **)args[3], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_SByte_t2530277047_RuntimeObject_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, RuntimeObject * p1, RuntimeObject * p2, const RuntimeMethod* method);
	int8_t ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], (RuntimeObject *)args[1], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_SByte_t2530277047_RuntimeObject_Int32_t2571135199_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, RuntimeObject * p1, int32_t p2, RuntimeObject * p3, const RuntimeMethod* method);
	int8_t ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), (RuntimeObject *)args[2], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_RuntimeObject_SByteU26_t1964499041 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, RuntimeObject * p1, int8_t* p2, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], (int8_t*)args[1], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int32_t2571135199_Int16_t769030278 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int16_t p1, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, *((int16_t*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_Int16_t769030278 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int16_t p1, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, *((int16_t*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_RuntimeObject_SByte_t2530277047_Int16U26_t2738771306_ExceptionU26_t1670763973 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, RuntimeObject * p1, int8_t p2, int16_t* p3, Exception_t3361176243 ** p4, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int8_t*)args[1]), (int16_t*)args[2], (Exception_t3361176243 **)args[3], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int16_t769030278_RuntimeObject_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, RuntimeObject * p1, RuntimeObject * p2, const RuntimeMethod* method);
	int16_t ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], (RuntimeObject *)args[1], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int16_t769030278_RuntimeObject_Int32_t2571135199_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, RuntimeObject * p1, int32_t p2, RuntimeObject * p3, const RuntimeMethod* method);
	int16_t ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), (RuntimeObject *)args[2], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_RuntimeObject_Int16U26_t2738771306 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, RuntimeObject * p1, int16_t* p2, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], (int16_t*)args[1], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_UInt16_t11709673_RuntimeObject_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, RuntimeObject * p1, RuntimeObject * p2, const RuntimeMethod* method);
	uint16_t ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], (RuntimeObject *)args[1], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_UInt16_t11709673_RuntimeObject_Int32_t2571135199_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, RuntimeObject * p1, int32_t p2, RuntimeObject * p3, const RuntimeMethod* method);
	uint16_t ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), (RuntimeObject *)args[2], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_RuntimeObject_UInt16U26_t1123581183 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, RuntimeObject * p1, uint16_t* p2, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], (uint16_t*)args[1], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_RuntimeObject_Int32_t2571135199_RuntimeObject_UInt16U26_t1123581183 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, RuntimeObject * p1, int32_t p2, RuntimeObject * p3, uint16_t* p4, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), (RuntimeObject *)args[2], (uint16_t*)args[3], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Void_t1078098495_ByteU2AU26_t1485094272_ByteU2AU26_t1485094272_DoubleU2AU26_t2972773821_UInt16U2AU26_t1250832877_UInt16U2AU26_t1250832877_UInt16U2AU26_t1250832877_UInt16U2AU26_t1250832877 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint8_t** p1, uint8_t** p2, double** p3, uint16_t** p4, uint16_t** p5, uint16_t** p6, uint16_t** p7, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (uint8_t**)args[0], (uint8_t**)args[1], (double**)args[2], (uint16_t**)args[3], (uint16_t**)args[4], (uint16_t**)args[5], (uint16_t**)args[6], methodMetadata);
	return NULL;
}

void* RuntimeInvoker_UnicodeCategory_t1362072295_Int16_t769030278 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int16_t p1, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, *((int16_t*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_RuntimeObject_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, RuntimeObject * p1, int32_t p2, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Void_t1078098495_RuntimeObject_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RuntimeObject * p1, int32_t p2, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Char_t3572527572_Int16_t769030278 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef Il2CppChar (*Func)(void* obj, int16_t p1, const RuntimeMethod* method);
	Il2CppChar ret = ((Func)methodPointer)(obj, *((int16_t*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Char_t3572527572_Int16_t769030278_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef Il2CppChar (*Func)(void* obj, int16_t p1, RuntimeObject * p2, const RuntimeMethod* method);
	Il2CppChar ret = ((Func)methodPointer)(obj, *((int16_t*)args[0]), (RuntimeObject *)args[1], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Void_t1078098495_RuntimeObject_Int32_t2571135199_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RuntimeObject * p1, int32_t p2, int32_t p3, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RuntimeObject * p1, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (RuntimeObject *)args[0], methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_Int16_t769030278_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t p1, int32_t p2, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((int16_t*)args[0]), *((int32_t*)args[1]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Char_t3572527572_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef Il2CppChar (*Func)(void* obj, int32_t p1, const RuntimeMethod* method);
	Il2CppChar ret = ((Func)methodPointer)(obj, *((int32_t*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Void_t1078098495_Int32_t2571135199_RuntimeObject_Int32_t2571135199_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, RuntimeObject * p2, int32_t p3, int32_t p4, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((int32_t*)args[0]), (RuntimeObject *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_RuntimeObject_Int32_t2571135199_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, int32_t p1, int32_t p2, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), methodMetadata);
	return ret;
}

void* RuntimeInvoker_RuntimeObject_RuntimeObject_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, RuntimeObject * p1, int32_t p2, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), methodMetadata);
	return ret;
}

void* RuntimeInvoker_RuntimeObject_RuntimeObject_Int32_t2571135199_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, RuntimeObject * p1, int32_t p2, int32_t p3, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), methodMetadata);
	return ret;
}

void* RuntimeInvoker_RuntimeObject_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, int32_t p1, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, *((int32_t*)args[0]), methodMetadata);
	return ret;
}

void* RuntimeInvoker_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, RuntimeObject * p4, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), (RuntimeObject *)args[3], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int32_t2571135199_RuntimeObject_RuntimeObject_SByte_t2530277047 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RuntimeObject * p1, RuntimeObject * p2, int8_t p3, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], (RuntimeObject *)args[1], *((int8_t*)args[2]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int32_t2571135199_RuntimeObject_RuntimeObject_SByte_t2530277047_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RuntimeObject * p1, RuntimeObject * p2, int8_t p3, RuntimeObject * p4, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], (RuntimeObject *)args[1], *((int8_t*)args[2]), (RuntimeObject *)args[3], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int32_t2571135199_RuntimeObject_Int32_t2571135199_RuntimeObject_Int32_t2571135199_Int32_t2571135199_SByte_t2530277047_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RuntimeObject * p1, int32_t p2, RuntimeObject * p3, int32_t p4, int32_t p5, int8_t p6, RuntimeObject * p7, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), (RuntimeObject *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), (RuntimeObject *)args[6], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int32_t2571135199_RuntimeObject_Int32_t2571135199_RuntimeObject_Int32_t2571135199_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RuntimeObject * p1, int32_t p2, RuntimeObject * p3, int32_t p4, int32_t p5, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), (RuntimeObject *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int32_t2571135199_RuntimeObject_Int32_t2571135199_Int32_t2571135199_RuntimeObject_Int32_t2571135199_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RuntimeObject * p1, int32_t p2, int32_t p3, RuntimeObject * p4, int32_t p5, int32_t p6, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (RuntimeObject *)args[3], *((int32_t*)args[4]), *((int32_t*)args[5]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int32_t2571135199_RuntimeObject_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RuntimeObject * p1, int32_t p2, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int32_t2571135199_RuntimeObject_Int32_t2571135199_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RuntimeObject * p1, int32_t p2, int32_t p3, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int32_t2571135199_RuntimeObject_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RuntimeObject * p1, int32_t p2, int32_t p3, int32_t p4, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int32_t2571135199_Int16_t769030278_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int16_t p1, int32_t p2, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, *((int16_t*)args[0]), *((int32_t*)args[1]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int32_t2571135199_Int16_t769030278_Int32_t2571135199_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int16_t p1, int32_t p2, int32_t p3, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, *((int16_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_RuntimeObject_Int32_t2571135199_Int16_t769030278 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, int32_t p1, int16_t p2, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, *((int32_t*)args[0]), *((int16_t*)args[1]), methodMetadata);
	return ret;
}

void* RuntimeInvoker_RuntimeObject_Int16_t769030278_Int16_t769030278 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, int16_t p1, int16_t p2, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, *((int16_t*)args[0]), *((int16_t*)args[1]), methodMetadata);
	return ret;
}

void* RuntimeInvoker_RuntimeObject_RuntimeObject_RuntimeObject_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, RuntimeObject * p1, RuntimeObject * p2, int32_t p3, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], (RuntimeObject *)args[1], *((int32_t*)args[2]), methodMetadata);
	return ret;
}

void* RuntimeInvoker_RuntimeObject_RuntimeObject_RuntimeObject_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, RuntimeObject * p1, RuntimeObject * p2, RuntimeObject * p3, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], (RuntimeObject *)args[1], (RuntimeObject *)args[2], methodMetadata);
	return ret;
}

void* RuntimeInvoker_RuntimeObject_RuntimeObject_RuntimeObject_RuntimeObject_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, RuntimeObject * p1, RuntimeObject * p2, RuntimeObject * p3, RuntimeObject * p4, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], (RuntimeObject *)args[1], (RuntimeObject *)args[2], (RuntimeObject *)args[3], methodMetadata);
	return ret;
}

void* RuntimeInvoker_RuntimeObject_Int32_t2571135199_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, int32_t p1, RuntimeObject * p2, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, *((int32_t*)args[0]), (RuntimeObject *)args[1], methodMetadata);
	return ret;
}

void* RuntimeInvoker_RuntimeObject_RuntimeObject_RuntimeObject_Int32_t2571135199_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, RuntimeObject * p1, RuntimeObject * p2, int32_t p3, int32_t p4, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], (RuntimeObject *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), methodMetadata);
	return ret;
}

void* RuntimeInvoker_Void_t1078098495_RuntimeObject_Int32U26_t2596978809_Int32U26_t2596978809_Int32U26_t2596978809_BooleanU26_t495120821_StringU26_t2155547156 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RuntimeObject * p1, int32_t* p2, int32_t* p3, int32_t* p4, bool* p5, String_t** p6, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (RuntimeObject *)args[0], (int32_t*)args[1], (int32_t*)args[2], (int32_t*)args[3], (bool*)args[4], (String_t**)args[5], methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Int32_t2571135199_RuntimeObject_Int32U26_t2596978809 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RuntimeObject * p1, int32_t* p2, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], (int32_t*)args[1], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Void_t1078098495_Int32_t2571135199_Int16_t769030278 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int16_t p2, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((int32_t*)args[0]), *((int16_t*)args[1]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_RuntimeObject_RuntimeObject_Int32_t2571135199_Int32_t2571135199_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, RuntimeObject * p1, int32_t p2, int32_t p3, RuntimeObject * p4, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (RuntimeObject *)args[3], methodMetadata);
	return ret;
}

void* RuntimeInvoker_RuntimeObject_Int16_t769030278_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, int16_t p1, int32_t p2, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, *((int16_t*)args[0]), *((int32_t*)args[1]), methodMetadata);
	return ret;
}

void* RuntimeInvoker_Void_t1078098495_RuntimeObject_RuntimeObject_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RuntimeObject * p1, RuntimeObject * p2, int32_t p3, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (RuntimeObject *)args[0], (RuntimeObject *)args[1], *((int32_t*)args[2]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_RuntimeObject_Int32_t2571135199_RuntimeObject_Int32_t2571135199_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RuntimeObject * p1, int32_t p2, RuntimeObject * p3, int32_t p4, int32_t p5, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), (RuntimeObject *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Int32_t2571135199_Single_t496865882 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, float p1, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, *((float*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_Single_t496865882 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, float p1, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, *((float*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Single_t496865882_RuntimeObject_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef float (*Func)(void* obj, RuntimeObject * p1, RuntimeObject * p2, const RuntimeMethod* method);
	float ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], (RuntimeObject *)args[1], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int32_t2571135199_Double_t1454265465 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, double p1, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, *((double*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_Double_t1454265465 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, double p1, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, *((double*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Double_t1454265465_RuntimeObject_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef double (*Func)(void* obj, RuntimeObject * p1, RuntimeObject * p2, const RuntimeMethod* method);
	double ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], (RuntimeObject *)args[1], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Double_t1454265465_RuntimeObject_Int32_t2571135199_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef double (*Func)(void* obj, RuntimeObject * p1, int32_t p2, RuntimeObject * p3, const RuntimeMethod* method);
	double ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), (RuntimeObject *)args[2], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_RuntimeObject_Int32_t2571135199_RuntimeObject_SByte_t2530277047_DoubleU26_t1440668655_ExceptionU26_t1670763973 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, RuntimeObject * p1, int32_t p2, RuntimeObject * p3, int8_t p4, double* p5, Exception_t3361176243 ** p6, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), (RuntimeObject *)args[2], *((int8_t*)args[3]), (double*)args[4], (Exception_t3361176243 **)args[5], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_RuntimeObject_RuntimeObject_Int32_t2571135199_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, RuntimeObject * p1, RuntimeObject * p2, int32_t p3, int32_t p4, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], (RuntimeObject *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_RuntimeObject_DoubleU26_t1440668655 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, RuntimeObject * p1, double* p2, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], (double*)args[1], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Void_t1078098495_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199_SByte_t2530277047_SByte_t2530277047 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int8_t p4, int8_t p5, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_Int64_t384086013 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((int64_t*)args[0]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_Single_t496865882 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((float*)args[0]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_Double_t1454265465 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, double p1, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((double*)args[0]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_RuntimeObject_Decimal_t2656901032 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, Decimal_t2656901032  p1, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, *((Decimal_t2656901032 *)args[0]), methodMetadata);
	return ret;
}

void* RuntimeInvoker_Decimal_t2656901032_Decimal_t2656901032_Decimal_t2656901032 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef Decimal_t2656901032  (*Func)(void* obj, Decimal_t2656901032  p1, Decimal_t2656901032  p2, const RuntimeMethod* method);
	Decimal_t2656901032  ret = ((Func)methodPointer)(obj, *((Decimal_t2656901032 *)args[0]), *((Decimal_t2656901032 *)args[1]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_UInt64_t2265270229_Decimal_t2656901032 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, Decimal_t2656901032  p1, const RuntimeMethod* method);
	uint64_t ret = ((Func)methodPointer)(obj, *((Decimal_t2656901032 *)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int64_t384086013_Decimal_t2656901032 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, Decimal_t2656901032  p1, const RuntimeMethod* method);
	int64_t ret = ((Func)methodPointer)(obj, *((Decimal_t2656901032 *)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_Decimal_t2656901032_Decimal_t2656901032 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Decimal_t2656901032  p1, Decimal_t2656901032  p2, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, *((Decimal_t2656901032 *)args[0]), *((Decimal_t2656901032 *)args[1]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Decimal_t2656901032_Decimal_t2656901032 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef Decimal_t2656901032  (*Func)(void* obj, Decimal_t2656901032  p1, const RuntimeMethod* method);
	Decimal_t2656901032  ret = ((Func)methodPointer)(obj, *((Decimal_t2656901032 *)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int32_t2571135199_Decimal_t2656901032_Decimal_t2656901032 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Decimal_t2656901032  p1, Decimal_t2656901032  p2, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, *((Decimal_t2656901032 *)args[0]), *((Decimal_t2656901032 *)args[1]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int32_t2571135199_Decimal_t2656901032 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Decimal_t2656901032  p1, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, *((Decimal_t2656901032 *)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_Decimal_t2656901032 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Decimal_t2656901032  p1, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, *((Decimal_t2656901032 *)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Decimal_t2656901032_RuntimeObject_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef Decimal_t2656901032  (*Func)(void* obj, RuntimeObject * p1, RuntimeObject * p2, const RuntimeMethod* method);
	Decimal_t2656901032  ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], (RuntimeObject *)args[1], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_RuntimeObject_RuntimeObject_Int32_t2571135199_RuntimeObject_Int32U26_t2596978809_BooleanU26_t495120821_BooleanU26_t495120821_Int32U26_t2596978809_SByte_t2530277047 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, RuntimeObject * p1, int32_t p2, RuntimeObject * p3, int32_t* p4, bool* p5, bool* p6, int32_t* p7, int8_t p8, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), (RuntimeObject *)args[2], (int32_t*)args[3], (bool*)args[4], (bool*)args[5], (int32_t*)args[6], *((int8_t*)args[7]), methodMetadata);
	return ret;
}

void* RuntimeInvoker_Decimal_t2656901032_RuntimeObject_Int32_t2571135199_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef Decimal_t2656901032  (*Func)(void* obj, RuntimeObject * p1, int32_t p2, RuntimeObject * p3, const RuntimeMethod* method);
	Decimal_t2656901032  ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), (RuntimeObject *)args[2], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_RuntimeObject_Int32_t2571135199_RuntimeObject_DecimalU26_t3838531224_SByte_t2530277047 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, RuntimeObject * p1, int32_t p2, RuntimeObject * p3, Decimal_t2656901032 * p4, int8_t p5, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), (RuntimeObject *)args[2], (Decimal_t2656901032 *)args[3], *((int8_t*)args[4]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int32_t2571135199_DecimalU26_t3838531224_UInt64U26_t3595557107 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Decimal_t2656901032 * p1, uint64_t* p2, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, (Decimal_t2656901032 *)args[0], (uint64_t*)args[1], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int32_t2571135199_DecimalU26_t3838531224_Int64U26_t4038210315 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Decimal_t2656901032 * p1, int64_t* p2, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, (Decimal_t2656901032 *)args[0], (int64_t*)args[1], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int32_t2571135199_DecimalU26_t3838531224_DecimalU26_t3838531224 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Decimal_t2656901032 * p1, Decimal_t2656901032 * p2, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, (Decimal_t2656901032 *)args[0], (Decimal_t2656901032 *)args[1], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int32_t2571135199_DecimalU26_t3838531224_RuntimeObject_Int32_t2571135199_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Decimal_t2656901032 * p1, RuntimeObject * p2, int32_t p3, int32_t p4, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, (Decimal_t2656901032 *)args[0], (RuntimeObject *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int32_t2571135199_DecimalU26_t3838531224_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Decimal_t2656901032 * p1, int32_t p2, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, (Decimal_t2656901032 *)args[0], *((int32_t*)args[1]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Double_t1454265465_DecimalU26_t3838531224 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef double (*Func)(void* obj, Decimal_t2656901032 * p1, const RuntimeMethod* method);
	double ret = ((Func)methodPointer)(obj, (Decimal_t2656901032 *)args[0], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Void_t1078098495_DecimalU26_t3838531224_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Decimal_t2656901032 * p1, int32_t p2, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (Decimal_t2656901032 *)args[0], *((int32_t*)args[1]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Int32_t2571135199_DecimalU26_t3838531224_DecimalU26_t3838531224_DecimalU26_t3838531224 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Decimal_t2656901032 * p1, Decimal_t2656901032 * p2, Decimal_t2656901032 * p3, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, (Decimal_t2656901032 *)args[0], (Decimal_t2656901032 *)args[1], (Decimal_t2656901032 *)args[2], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Byte_t880488320_Decimal_t2656901032 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, Decimal_t2656901032  p1, const RuntimeMethod* method);
	uint8_t ret = ((Func)methodPointer)(obj, *((Decimal_t2656901032 *)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_SByte_t2530277047_Decimal_t2656901032 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, Decimal_t2656901032  p1, const RuntimeMethod* method);
	int8_t ret = ((Func)methodPointer)(obj, *((Decimal_t2656901032 *)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int16_t769030278_Decimal_t2656901032 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, Decimal_t2656901032  p1, const RuntimeMethod* method);
	int16_t ret = ((Func)methodPointer)(obj, *((Decimal_t2656901032 *)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_UInt16_t11709673_Decimal_t2656901032 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Decimal_t2656901032  p1, const RuntimeMethod* method);
	uint16_t ret = ((Func)methodPointer)(obj, *((Decimal_t2656901032 *)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_UInt32_t2739056616_Decimal_t2656901032 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, Decimal_t2656901032  p1, const RuntimeMethod* method);
	uint32_t ret = ((Func)methodPointer)(obj, *((Decimal_t2656901032 *)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Decimal_t2656901032_SByte_t2530277047 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef Decimal_t2656901032  (*Func)(void* obj, int8_t p1, const RuntimeMethod* method);
	Decimal_t2656901032  ret = ((Func)methodPointer)(obj, *((int8_t*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Decimal_t2656901032_Int16_t769030278 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef Decimal_t2656901032  (*Func)(void* obj, int16_t p1, const RuntimeMethod* method);
	Decimal_t2656901032  ret = ((Func)methodPointer)(obj, *((int16_t*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Decimal_t2656901032_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef Decimal_t2656901032  (*Func)(void* obj, int32_t p1, const RuntimeMethod* method);
	Decimal_t2656901032  ret = ((Func)methodPointer)(obj, *((int32_t*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Decimal_t2656901032_Int64_t384086013 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef Decimal_t2656901032  (*Func)(void* obj, int64_t p1, const RuntimeMethod* method);
	Decimal_t2656901032  ret = ((Func)methodPointer)(obj, *((int64_t*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Decimal_t2656901032_Single_t496865882 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef Decimal_t2656901032  (*Func)(void* obj, float p1, const RuntimeMethod* method);
	Decimal_t2656901032  ret = ((Func)methodPointer)(obj, *((float*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Decimal_t2656901032_Double_t1454265465 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef Decimal_t2656901032  (*Func)(void* obj, double p1, const RuntimeMethod* method);
	Decimal_t2656901032  ret = ((Func)methodPointer)(obj, *((double*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Single_t496865882_Decimal_t2656901032 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Decimal_t2656901032  p1, const RuntimeMethod* method);
	float ret = ((Func)methodPointer)(obj, *((Decimal_t2656901032 *)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Double_t1454265465_Decimal_t2656901032 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef double (*Func)(void* obj, Decimal_t2656901032  p1, const RuntimeMethod* method);
	double ret = ((Func)methodPointer)(obj, *((Decimal_t2656901032 *)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Void_t1078098495_RuntimeObject_StreamingContext_t1316667400 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RuntimeObject * p1, StreamingContext_t1316667400  p2, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((StreamingContext_t1316667400 *)args[1]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Int64_t384086013 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, const RuntimeMethod* method);
	int64_t ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_IntPtr_t_IntPtr_t (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, intptr_t p1, intptr_t p2, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, *((intptr_t*)args[0]), *((intptr_t*)args[1]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_IntPtr_t_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef intptr_t (*Func)(void* obj, int32_t p1, const RuntimeMethod* method);
	intptr_t ret = ((Func)methodPointer)(obj, *((int32_t*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_IntPtr_t_Int64_t384086013 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef intptr_t (*Func)(void* obj, int64_t p1, const RuntimeMethod* method);
	intptr_t ret = ((Func)methodPointer)(obj, *((int64_t*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_IntPtr_t_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef intptr_t (*Func)(void* obj, RuntimeObject * p1, const RuntimeMethod* method);
	intptr_t ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int32_t2571135199_IntPtr_t (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, intptr_t p1, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, *((intptr_t*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_RuntimeObject_IntPtr_t (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, intptr_t p1, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, *((intptr_t*)args[0]), methodMetadata);
	return ret;
}

void* RuntimeInvoker_RuntimeObject_RuntimeObject_RuntimeObject_MulticastDelegateU26_t455666402 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, RuntimeObject * p1, RuntimeObject * p2, MulticastDelegate_t69367374 ** p3, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], (RuntimeObject *)args[1], (MulticastDelegate_t69367374 **)args[2], methodMetadata);
	return ret;
}

void* RuntimeInvoker_RuntimeObject_RuntimeObject_RuntimeObject_RuntimeObject_SByte_t2530277047 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, RuntimeObject * p1, RuntimeObject * p2, RuntimeObject * p3, int8_t p4, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], (RuntimeObject *)args[1], (RuntimeObject *)args[2], *((int8_t*)args[3]), methodMetadata);
	return ret;
}

void* RuntimeInvoker_TypeCode_t2052347577 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int32_t2571135199_RuntimeObject_RuntimeObject_RuntimeObject_SByte_t2530277047 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RuntimeObject * p1, RuntimeObject * p2, RuntimeObject * p3, int8_t p4, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], (RuntimeObject *)args[1], (RuntimeObject *)args[2], *((int8_t*)args[3]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_UInt64_t2265270229_RuntimeObject_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, RuntimeObject * p1, int32_t p2, const RuntimeMethod* method);
	uint64_t ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_RuntimeObject_RuntimeObject_SByte_t2530277047 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, RuntimeObject * p1, int8_t p2, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int8_t*)args[1]), methodMetadata);
	return ret;
}

void* RuntimeInvoker_RuntimeObject_RuntimeObject_Int16_t769030278 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, RuntimeObject * p1, int16_t p2, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int16_t*)args[1]), methodMetadata);
	return ret;
}

void* RuntimeInvoker_RuntimeObject_RuntimeObject_Int64_t384086013 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, RuntimeObject * p1, int64_t p2, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int64_t*)args[1]), methodMetadata);
	return ret;
}

void* RuntimeInvoker_Void_t1078098495_Int32_t2571135199_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, RuntimeObject * p2, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((int32_t*)args[0]), (RuntimeObject *)args[1], methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Int64_t384086013_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, int32_t p1, const RuntimeMethod* method);
	int64_t ret = ((Func)methodPointer)(obj, *((int32_t*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_RuntimeObject_Int32_t2571135199_RuntimeObject_Int32_t2571135199_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, RuntimeObject * p1, int32_t p2, RuntimeObject * p3, int32_t p4, int32_t p5, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), (RuntimeObject *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_RuntimeObject_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), methodMetadata);
	return ret;
}

void* RuntimeInvoker_RuntimeObject_Int64_t384086013 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, int64_t p1, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, *((int64_t*)args[0]), methodMetadata);
	return ret;
}

void* RuntimeInvoker_RuntimeObject_Int64_t384086013_Int64_t384086013 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, int64_t p1, int64_t p2, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, *((int64_t*)args[0]), *((int64_t*)args[1]), methodMetadata);
	return ret;
}

void* RuntimeInvoker_RuntimeObject_Int64_t384086013_Int64_t384086013_Int64_t384086013 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, int64_t p1, int64_t p2, int64_t p3, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, *((int64_t*)args[0]), *((int64_t*)args[1]), *((int64_t*)args[2]), methodMetadata);
	return ret;
}

void* RuntimeInvoker_Void_t1078098495_RuntimeObject_Int64_t384086013 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RuntimeObject * p1, int64_t p2, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int64_t*)args[1]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_RuntimeObject_Int64_t384086013_Int64_t384086013 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RuntimeObject * p1, int64_t p2, int64_t p3, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int64_t*)args[1]), *((int64_t*)args[2]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_RuntimeObject_Int64_t384086013_Int64_t384086013_Int64_t384086013 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RuntimeObject * p1, int64_t p2, int64_t p3, int64_t p4, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int64_t*)args[1]), *((int64_t*)args[2]), *((int64_t*)args[3]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_RuntimeObject_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RuntimeObject * p1, int32_t p2, int32_t p3, int32_t p4, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_RuntimeObject_RuntimeObject_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, RuntimeObject * p1, int32_t p2, int32_t p3, int32_t p4, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), methodMetadata);
	return ret;
}

void* RuntimeInvoker_Int32_t2571135199_RuntimeObject_RuntimeObject_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RuntimeObject * p1, RuntimeObject * p2, RuntimeObject * p3, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], (RuntimeObject *)args[1], (RuntimeObject *)args[2], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int32_t2571135199_RuntimeObject_Int32_t2571135199_Int32_t2571135199_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RuntimeObject * p1, int32_t p2, int32_t p3, RuntimeObject * p4, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (RuntimeObject *)args[3], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int32_t2571135199_RuntimeObject_Int32_t2571135199_Int32_t2571135199_RuntimeObject_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RuntimeObject * p1, int32_t p2, int32_t p3, RuntimeObject * p4, RuntimeObject * p5, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (RuntimeObject *)args[3], (RuntimeObject *)args[4], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Void_t1078098495_RuntimeObject_Int64_t384086013_RuntimeObject_Int64_t384086013_Int64_t384086013 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RuntimeObject * p1, int64_t p2, RuntimeObject * p3, int64_t p4, int64_t p5, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int64_t*)args[1]), (RuntimeObject *)args[2], *((int64_t*)args[3]), *((int64_t*)args[4]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_RuntimeObject_RuntimeObject_Int64_t384086013 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RuntimeObject * p1, RuntimeObject * p2, int64_t p3, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (RuntimeObject *)args[0], (RuntimeObject *)args[1], *((int64_t*)args[2]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Int32_t2571135199_RuntimeObject_RuntimeObject_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RuntimeObject * p1, RuntimeObject * p2, int32_t p3, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], (RuntimeObject *)args[1], *((int32_t*)args[2]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int32_t2571135199_RuntimeObject_RuntimeObject_Int32_t2571135199_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RuntimeObject * p1, RuntimeObject * p2, int32_t p3, int32_t p4, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], (RuntimeObject *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Void_t1078098495_RuntimeObject_RuntimeObject_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RuntimeObject * p1, RuntimeObject * p2, RuntimeObject * p3, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (RuntimeObject *)args[0], (RuntimeObject *)args[1], (RuntimeObject *)args[2], methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_RuntimeObject_RuntimeObject_Int32_t2571135199_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RuntimeObject * p1, RuntimeObject * p2, int32_t p3, int32_t p4, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (RuntimeObject *)args[0], (RuntimeObject *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_RuntimeObject_Int32_t2571135199_Int32_t2571135199_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RuntimeObject * p1, int32_t p2, int32_t p3, RuntimeObject * p4, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (RuntimeObject *)args[3], methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_RuntimeObject_RuntimeObject_Int32_t2571135199_Int32_t2571135199_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RuntimeObject * p1, RuntimeObject * p2, int32_t p3, int32_t p4, RuntimeObject * p5, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (RuntimeObject *)args[0], (RuntimeObject *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (RuntimeObject *)args[4], methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_Int32_t2571135199_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_RuntimeObject_IntPtr_t (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RuntimeObject * p1, intptr_t p2, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((intptr_t*)args[1]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_RuntimeObject_Int32_t2571135199_Int32_t2571135199_RuntimeObject_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, int32_t p1, int32_t p2, RuntimeObject * p3, RuntimeObject * p4, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (RuntimeObject *)args[2], (RuntimeObject *)args[3], methodMetadata);
	return ret;
}

void* RuntimeInvoker_TypeAttributes_t3122562390 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_MemberTypes_t2339173058 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_RuntimeTypeHandle_t637624852 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeTypeHandle_t637624852  (*Func)(void* obj, const RuntimeMethod* method);
	RuntimeTypeHandle_t637624852  ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_RuntimeObject_RuntimeObject_SByte_t2530277047_SByte_t2530277047 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, RuntimeObject * p1, int8_t p2, int8_t p3, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int8_t*)args[1]), *((int8_t*)args[2]), methodMetadata);
	return ret;
}

void* RuntimeInvoker_TypeCode_t2052347577_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RuntimeObject * p1, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_RuntimeObject_RuntimeTypeHandle_t637624852 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, RuntimeTypeHandle_t637624852  p1, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, *((RuntimeTypeHandle_t637624852 *)args[0]), methodMetadata);
	return ret;
}

void* RuntimeInvoker_RuntimeObject_RuntimeObject_Int32_t2571135199_RuntimeObject_RuntimeObject_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, RuntimeObject * p1, int32_t p2, RuntimeObject * p3, RuntimeObject * p4, RuntimeObject * p5, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), (RuntimeObject *)args[2], (RuntimeObject *)args[3], (RuntimeObject *)args[4], methodMetadata);
	return ret;
}

void* RuntimeInvoker_RuntimeObject_RuntimeObject_Int32_t2571135199_RuntimeObject_Int32_t2571135199_RuntimeObject_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, RuntimeObject * p1, int32_t p2, RuntimeObject * p3, int32_t p4, RuntimeObject * p5, RuntimeObject * p6, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), (RuntimeObject *)args[2], *((int32_t*)args[3]), (RuntimeObject *)args[4], (RuntimeObject *)args[5], methodMetadata);
	return ret;
}

void* RuntimeInvoker_RuntimeObject_RuntimeObject_Int32_t2571135199_RuntimeObject_RuntimeObject_RuntimeObject_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, RuntimeObject * p1, int32_t p2, RuntimeObject * p3, RuntimeObject * p4, RuntimeObject * p5, RuntimeObject * p6, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), (RuntimeObject *)args[2], (RuntimeObject *)args[3], (RuntimeObject *)args[4], (RuntimeObject *)args[5], methodMetadata);
	return ret;
}

void* RuntimeInvoker_RuntimeObject_Int32_t2571135199_RuntimeObject_Int32_t2571135199_RuntimeObject_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, int32_t p1, RuntimeObject * p2, int32_t p3, RuntimeObject * p4, RuntimeObject * p5, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, *((int32_t*)args[0]), (RuntimeObject *)args[1], *((int32_t*)args[2]), (RuntimeObject *)args[3], (RuntimeObject *)args[4], methodMetadata);
	return ret;
}

void* RuntimeInvoker_RuntimeObject_Int32_t2571135199_RuntimeObject_RuntimeObject_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, int32_t p1, RuntimeObject * p2, RuntimeObject * p3, RuntimeObject * p4, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, *((int32_t*)args[0]), (RuntimeObject *)args[1], (RuntimeObject *)args[2], (RuntimeObject *)args[3], methodMetadata);
	return ret;
}

void* RuntimeInvoker_RuntimeObject_RuntimeObject_Int32_t2571135199_RuntimeObject_RuntimeObject_RuntimeObject_RuntimeObject_RuntimeObject_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, RuntimeObject * p1, int32_t p2, RuntimeObject * p3, RuntimeObject * p4, RuntimeObject * p5, RuntimeObject * p6, RuntimeObject * p7, RuntimeObject * p8, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), (RuntimeObject *)args[2], (RuntimeObject *)args[3], (RuntimeObject *)args[4], (RuntimeObject *)args[5], (RuntimeObject *)args[6], (RuntimeObject *)args[7], methodMetadata);
	return ret;
}

void* RuntimeInvoker_Boolean_t2059672899_RuntimeObject_SByte_t2530277047 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, RuntimeObject * p1, int8_t p2, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int8_t*)args[1]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_RuntimeObject_SByte_t2530277047 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, int8_t p1, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, *((int8_t*)args[0]), methodMetadata);
	return ret;
}

void* RuntimeInvoker_IntPtr_t (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef intptr_t (*Func)(void* obj, const RuntimeMethod* method);
	intptr_t ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Void_t1078098495_RuntimeObject_SByte_t2530277047 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RuntimeObject * p1, int8_t p2, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int8_t*)args[1]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_SByte_t2530277047_SByte_t2530277047_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int8_t p2, int32_t p3, int32_t p4, int32_t p5, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_RuntimeObject_RuntimeFieldHandle_t1782795956 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RuntimeObject * p1, RuntimeFieldHandle_t1782795956  p2, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((RuntimeFieldHandle_t1782795956 *)args[1]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_IntPtr_t_SByte_t2530277047 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, intptr_t p1, int8_t p2, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((intptr_t*)args[0]), *((int8_t*)args[1]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199_SByte_t2530277047 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int8_t p4, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int8_t*)args[3]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_SByte_t2530277047_SByte_t2530277047 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int8_t p2, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_RuntimeObject_RuntimeObject_ContractionU5BU5DU26_t2904489606_Level2MapU5BU5DU26_t2963104860 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RuntimeObject * p1, RuntimeObject * p2, ContractionU5BU5D_t398144010** p3, Level2MapU5BU5D_t70716484** p4, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (RuntimeObject *)args[0], (RuntimeObject *)args[1], (ContractionU5BU5D_t398144010**)args[2], (Level2MapU5BU5D_t70716484**)args[3], methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_RuntimeObject_CodePointIndexerU26_t2204815176_ByteU2AU26_t1485094272_ByteU2AU26_t1485094272_CodePointIndexerU26_t2204815176_ByteU2AU26_t1485094272 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RuntimeObject * p1, CodePointIndexer_t1824690040 ** p2, uint8_t** p3, uint8_t** p4, CodePointIndexer_t1824690040 ** p5, uint8_t** p6, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (RuntimeObject *)args[0], (CodePointIndexer_t1824690040 **)args[1], (uint8_t**)args[2], (uint8_t**)args[3], (CodePointIndexer_t1824690040 **)args[4], (uint8_t**)args[5], methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Byte_t880488320_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, int32_t p1, const RuntimeMethod* method);
	uint8_t ret = ((Func)methodPointer)(obj, *((int32_t*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_Int32_t2571135199_SByte_t2530277047 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int8_t p2, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, *((int32_t*)args[0]), *((int8_t*)args[1]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_UInt32_t2739056616_RuntimeObject_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, RuntimeObject * p1, int32_t p2, const RuntimeMethod* method);
	uint32_t ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Byte_t880488320_Int32_t2571135199_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, int32_t p1, int32_t p2, const RuntimeMethod* method);
	uint8_t ret = ((Func)methodPointer)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_Int32_t2571135199_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int32_t p2, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, int32_t p2, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_ExtenderType_t1616581759_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, *((int32_t*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Void_t1078098495_RuntimeObject_Int32_t2571135199_Int32_t2571135199_RuntimeObject_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RuntimeObject * p1, int32_t p2, int32_t p3, RuntimeObject * p4, int32_t p5, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (RuntimeObject *)args[3], *((int32_t*)args[4]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_Int32_t2571135199_Int32_t2571135199_RuntimeObject_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, RuntimeObject * p3, int32_t p4, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (RuntimeObject *)args[2], *((int32_t*)args[3]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Int32_t2571135199_RuntimeObject_Int32_t2571135199_Int32_t2571135199_RuntimeObject_Int32_t2571135199_Int32_t2571135199_BooleanU26_t495120821_BooleanU26_t495120821_SByte_t2530277047 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RuntimeObject * p1, int32_t p2, int32_t p3, RuntimeObject * p4, int32_t p5, int32_t p6, bool* p7, bool* p8, int8_t p9, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (RuntimeObject *)args[3], *((int32_t*)args[4]), *((int32_t*)args[5]), (bool*)args[6], (bool*)args[7], *((int8_t*)args[8]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int32_t2571135199_RuntimeObject_Int32_t2571135199_Int32_t2571135199_RuntimeObject_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RuntimeObject * p1, int32_t p2, int32_t p3, RuntimeObject * p4, int32_t p5, int32_t p6, int32_t p7, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (RuntimeObject *)args[3], *((int32_t*)args[4]), *((int32_t*)args[5]), *((int32_t*)args[6]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_RuntimeObject_Int32_t2571135199_Int32_t2571135199_RuntimeObject_Int32_t2571135199_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, RuntimeObject * p1, int32_t p2, int32_t p3, RuntimeObject * p4, int32_t p5, int32_t p6, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (RuntimeObject *)args[3], *((int32_t*)args[4]), *((int32_t*)args[5]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int32_t2571135199_RuntimeObject_Int32_t2571135199_Int32_t2571135199_RuntimeObject_Int32_t2571135199_Int32_t2571135199_BooleanU26_t495120821_BooleanU26_t495120821_SByte_t2530277047_SByte_t2530277047_ContextU26_t831875511 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RuntimeObject * p1, int32_t p2, int32_t p3, RuntimeObject * p4, int32_t p5, int32_t p6, bool* p7, bool* p8, int8_t p9, int8_t p10, Context_t3303670129 * p11, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (RuntimeObject *)args[3], *((int32_t*)args[4]), *((int32_t*)args[5]), (bool*)args[6], (bool*)args[7], *((int8_t*)args[8]), *((int8_t*)args[9]), (Context_t3303670129 *)args[10], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int32_t2571135199_SByte_t2530277047_SByte_t2530277047 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int8_t p1, int8_t p2, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_RuntimeObject_RuntimeObject_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, RuntimeObject * p1, RuntimeObject * p2, int32_t p3, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], (RuntimeObject *)args[1], *((int32_t*)args[2]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_RuntimeObject_RuntimeObject_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, RuntimeObject * p1, RuntimeObject * p2, int32_t p3, int32_t p4, int32_t p5, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], (RuntimeObject *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_RuntimeObject_RuntimeObject_Int32_t2571135199_Int32_t2571135199_SByte_t2530277047_ContextU26_t831875511 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, RuntimeObject * p1, RuntimeObject * p2, int32_t p3, int32_t p4, int8_t p5, Context_t3303670129 * p6, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], (RuntimeObject *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int8_t*)args[4]), (Context_t3303670129 *)args[5], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int32_t2571135199_RuntimeObject_RuntimeObject_Int32_t2571135199_Int32_t2571135199_BooleanU26_t495120821 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RuntimeObject * p1, RuntimeObject * p2, int32_t p3, int32_t p4, bool* p5, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], (RuntimeObject *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (bool*)args[4], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int32_t2571135199_RuntimeObject_RuntimeObject_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RuntimeObject * p1, RuntimeObject * p2, int32_t p3, int32_t p4, int32_t p5, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], (RuntimeObject *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int32_t2571135199_RuntimeObject_Int32_t2571135199_Int32_t2571135199_RuntimeObject_Int16_t769030278_Int32_t2571135199_SByte_t2530277047_ContextU26_t831875511 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RuntimeObject * p1, int32_t p2, int32_t p3, RuntimeObject * p4, int16_t p5, int32_t p6, int8_t p7, Context_t3303670129 * p8, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (RuntimeObject *)args[3], *((int16_t*)args[4]), *((int32_t*)args[5]), *((int8_t*)args[6]), (Context_t3303670129 *)args[7], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int32_t2571135199_RuntimeObject_RuntimeObject_Int32_t2571135199_Int32_t2571135199_RuntimeObject_ContextU26_t831875511 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RuntimeObject * p1, RuntimeObject * p2, int32_t p3, int32_t p4, RuntimeObject * p5, Context_t3303670129 * p6, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], (RuntimeObject *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (RuntimeObject *)args[4], (Context_t3303670129 *)args[5], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int32_t2571135199_RuntimeObject_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199_RuntimeObject_Int32_t2571135199_SByte_t2530277047_ContextU26_t831875511 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RuntimeObject * p1, int32_t p2, int32_t p3, int32_t p4, RuntimeObject * p5, int32_t p6, int8_t p7, Context_t3303670129 * p8, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), (RuntimeObject *)args[4], *((int32_t*)args[5]), *((int8_t*)args[6]), (Context_t3303670129 *)args[7], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_RuntimeObject_Int32U26_t2596978809_Int32_t2571135199_Int32_t2571135199_RuntimeObject_SByte_t2530277047_ContextU26_t831875511 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, RuntimeObject * p1, int32_t* p2, int32_t p3, int32_t p4, RuntimeObject * p5, int8_t p6, Context_t3303670129 * p7, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (RuntimeObject *)args[4], *((int8_t*)args[5]), (Context_t3303670129 *)args[6], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_RuntimeObject_Int32U26_t2596978809_Int32_t2571135199_Int32_t2571135199_RuntimeObject_SByte_t2530277047_Int32_t2571135199_ContractionU26_t1846426445_ContextU26_t831875511 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, RuntimeObject * p1, int32_t* p2, int32_t p3, int32_t p4, RuntimeObject * p5, int8_t p6, int32_t p7, Contraction_t3257534955 ** p8, Context_t3303670129 * p9, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (RuntimeObject *)args[4], *((int8_t*)args[5]), *((int32_t*)args[6]), (Contraction_t3257534955 **)args[7], (Context_t3303670129 *)args[8], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_Int32_t2571135199_RuntimeObject_Int32_t2571135199_Int32_t2571135199_RuntimeObject_Int32_t2571135199_SByte_t2530277047 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, RuntimeObject * p2, int32_t p3, int32_t p4, RuntimeObject * p5, int32_t p6, int8_t p7, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, *((int32_t*)args[0]), (RuntimeObject *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (RuntimeObject *)args[4], *((int32_t*)args[5]), *((int8_t*)args[6]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_RuntimeObject_Int32U26_t2596978809_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199_RuntimeObject_SByte_t2530277047_ContextU26_t831875511 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, RuntimeObject * p1, int32_t* p2, int32_t p3, int32_t p4, int32_t p5, RuntimeObject * p6, int8_t p7, Context_t3303670129 * p8, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), (RuntimeObject *)args[5], *((int8_t*)args[6]), (Context_t3303670129 *)args[7], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_RuntimeObject_Int32U26_t2596978809_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199_RuntimeObject_SByte_t2530277047_Int32_t2571135199_ContractionU26_t1846426445_ContextU26_t831875511 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, RuntimeObject * p1, int32_t* p2, int32_t p3, int32_t p4, int32_t p5, RuntimeObject * p6, int8_t p7, int32_t p8, Contraction_t3257534955 ** p9, Context_t3303670129 * p10, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), (RuntimeObject *)args[5], *((int8_t*)args[6]), *((int32_t*)args[7]), (Contraction_t3257534955 **)args[8], (Context_t3303670129 *)args[9], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Void_t1078098495_Int32_t2571135199_RuntimeObject_RuntimeObject_RuntimeObject_RuntimeObject_RuntimeObject_SByte_t2530277047 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, RuntimeObject * p2, RuntimeObject * p3, RuntimeObject * p4, RuntimeObject * p5, RuntimeObject * p6, int8_t p7, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((int32_t*)args[0]), (RuntimeObject *)args[1], (RuntimeObject *)args[2], (RuntimeObject *)args[3], (RuntimeObject *)args[4], (RuntimeObject *)args[5], *((int8_t*)args[6]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_Int32_t2571135199_RuntimeObject_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, RuntimeObject * p2, int32_t p3, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((int32_t*)args[0]), (RuntimeObject *)args[1], *((int32_t*)args[2]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_Int32_t2571135199_RuntimeObject_RuntimeObject_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, RuntimeObject * p2, RuntimeObject * p3, int32_t p4, int32_t p5, int32_t p6, int32_t p7, int32_t p8, int32_t p9, int32_t p10, int32_t p11, int32_t p12, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((int32_t*)args[0]), (RuntimeObject *)args[1], (RuntimeObject *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), *((int32_t*)args[5]), *((int32_t*)args[6]), *((int32_t*)args[7]), *((int32_t*)args[8]), *((int32_t*)args[9]), *((int32_t*)args[10]), *((int32_t*)args[11]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_Int32_t2571135199_Int32_t2571135199_RuntimeObject_SByte_t2530277047 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, RuntimeObject * p3, int8_t p4, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (RuntimeObject *)args[2], *((int8_t*)args[3]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_SByte_t2530277047_SByte_t2530277047_SByte_t2530277047_SByte_t2530277047_SByte_t2530277047_SByte_t2530277047_SByte_t2530277047_SByte_t2530277047 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int8_t p2, int8_t p3, int8_t p4, int8_t p5, int8_t p6, int8_t p7, int8_t p8, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), *((int8_t*)args[6]), *((int8_t*)args[7]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_SByte_t2530277047_SByte_t2530277047_SByte_t2530277047_SByte_t2530277047 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int8_t p2, int8_t p3, int8_t p4, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_SByte_t2530277047_ByteU5BU5DU26_t321522471_Int32U26_t2596978809 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, ByteU5BU5D_t1014014593** p2, int32_t* p3, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((int8_t*)args[0]), (ByteU5BU5D_t1014014593**)args[1], (int32_t*)args[2], methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Int32_t2571135199_RuntimeObject_Int32_t2571135199_SByte_t2530277047 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RuntimeObject * p1, int32_t p2, int8_t p3, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_ConfidenceFactor_t3678829215 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Void_t1078098495_Int32_t2571135199_SByte_t2530277047 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int8_t p2, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((int32_t*)args[0]), *((int8_t*)args[1]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Sign_t3270271764_RuntimeObject_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RuntimeObject * p1, RuntimeObject * p2, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], (RuntimeObject *)args[1], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Void_t1078098495_RuntimeObject_Int32_t2571135199_Int32_t2571135199_RuntimeObject_Int32_t2571135199_Int32_t2571135199_RuntimeObject_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RuntimeObject * p1, int32_t p2, int32_t p3, RuntimeObject * p4, int32_t p5, int32_t p6, RuntimeObject * p7, int32_t p8, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (RuntimeObject *)args[3], *((int32_t*)args[4]), *((int32_t*)args[5]), (RuntimeObject *)args[6], *((int32_t*)args[7]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_RuntimeObject_Int32_t2571135199_Int32_t2571135199_RuntimeObject_Int32_t2571135199_Int32_t2571135199_RuntimeObject_Int32_t2571135199_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RuntimeObject * p1, int32_t p2, int32_t p3, RuntimeObject * p4, int32_t p5, int32_t p6, RuntimeObject * p7, int32_t p8, int32_t p9, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (RuntimeObject *)args[3], *((int32_t*)args[4]), *((int32_t*)args[5]), (RuntimeObject *)args[6], *((int32_t*)args[7]), *((int32_t*)args[8]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_DSAParameters_t2781005375_SByte_t2530277047 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef DSAParameters_t2781005375  (*Func)(void* obj, int8_t p1, const RuntimeMethod* method);
	DSAParameters_t2781005375  ret = ((Func)methodPointer)(obj, *((int8_t*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Void_t1078098495_DSAParameters_t2781005375 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, DSAParameters_t2781005375  p1, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((DSAParameters_t2781005375 *)args[0]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Boolean_t2059672899_RuntimeObject_RuntimeObject_RuntimeObject_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, RuntimeObject * p1, RuntimeObject * p2, RuntimeObject * p3, RuntimeObject * p4, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], (RuntimeObject *)args[1], (RuntimeObject *)args[2], (RuntimeObject *)args[3], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_RuntimeObject_RuntimeObject_RuntimeObject_RuntimeObject_SByte_t2530277047 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, RuntimeObject * p1, RuntimeObject * p2, RuntimeObject * p3, RuntimeObject * p4, int8_t p5, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], (RuntimeObject *)args[1], (RuntimeObject *)args[2], (RuntimeObject *)args[3], *((int8_t*)args[4]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_RuntimeObject_RuntimeObject_DSAParameters_t2781005375 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, RuntimeObject * p1, DSAParameters_t2781005375  p2, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((DSAParameters_t2781005375 *)args[1]), methodMetadata);
	return ret;
}

void* RuntimeInvoker_RSAParameters_t2749614896_SByte_t2530277047 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RSAParameters_t2749614896  (*Func)(void* obj, int8_t p1, const RuntimeMethod* method);
	RSAParameters_t2749614896  ret = ((Func)methodPointer)(obj, *((int8_t*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Void_t1078098495_RSAParameters_t2749614896 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RSAParameters_t2749614896  p1, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((RSAParameters_t2749614896 *)args[0]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_RuntimeObject_SByte_t2530277047_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RuntimeObject * p1, int8_t p2, RuntimeObject * p3, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int8_t*)args[1]), (RuntimeObject *)args[2], methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Int32_t2571135199_RuntimeObject_Int32_t2571135199_Int32_t2571135199_RuntimeObject_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RuntimeObject * p1, int32_t p2, int32_t p3, RuntimeObject * p4, int32_t p5, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (RuntimeObject *)args[3], *((int32_t*)args[4]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_RuntimeObject_RuntimeObject_RuntimeObject_Int32_t2571135199_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, RuntimeObject * p1, RuntimeObject * p2, int32_t p3, RuntimeObject * p4, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], (RuntimeObject *)args[1], *((int32_t*)args[2]), (RuntimeObject *)args[3], methodMetadata);
	return ret;
}

void* RuntimeInvoker_DSAParameters_t2781005375_BooleanU26_t495120821 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef DSAParameters_t2781005375  (*Func)(void* obj, bool* p1, const RuntimeMethod* method);
	DSAParameters_t2781005375  ret = ((Func)methodPointer)(obj, (bool*)args[0], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Void_t1078098495_RuntimeObject_Int32_t2571135199_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RuntimeObject * p1, int32_t p2, RuntimeObject * p3, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), (RuntimeObject *)args[2], methodMetadata);
	return NULL;
}

void* RuntimeInvoker_RuntimeObject_RuntimeObject_SByte_t2530277047_RuntimeObject_SByte_t2530277047 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, RuntimeObject * p1, int8_t p2, RuntimeObject * p3, int8_t p4, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int8_t*)args[1]), (RuntimeObject *)args[2], *((int8_t*)args[3]), methodMetadata);
	return ret;
}

void* RuntimeInvoker_Void_t1078098495_RuntimeObject_RuntimeObject_SByte_t2530277047 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RuntimeObject * p1, RuntimeObject * p2, int8_t p3, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (RuntimeObject *)args[0], (RuntimeObject *)args[1], *((int8_t*)args[2]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_DateTime_t507798353 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef DateTime_t507798353  (*Func)(void* obj, const RuntimeMethod* method);
	DateTime_t507798353  ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Void_t1078098495_SByte_t2530277047_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, RuntimeObject * p2, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((int8_t*)args[0]), (RuntimeObject *)args[1], methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Byte_t880488320 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, const RuntimeMethod* method);
	uint8_t ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Void_t1078098495_RuntimeObject_Int32U26_t2596978809_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RuntimeObject * p1, int32_t* p2, int32_t p3, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (RuntimeObject *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_RuntimeObject_Int32U26_t2596978809_ByteU26_t3158801536_Int32U26_t2596978809_ByteU5BU5DU26_t321522471 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RuntimeObject * p1, int32_t* p2, uint8_t* p3, int32_t* p4, ByteU5BU5D_t1014014593** p5, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (RuntimeObject *)args[0], (int32_t*)args[1], (uint8_t*)args[2], (int32_t*)args[3], (ByteU5BU5D_t1014014593**)args[4], methodMetadata);
	return NULL;
}

void* RuntimeInvoker_RuntimeObject_Int32_t2571135199_SByte_t2530277047 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, int32_t p1, int8_t p2, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, *((int32_t*)args[0]), *((int8_t*)args[1]), methodMetadata);
	return ret;
}

void* RuntimeInvoker_Int16_t769030278_RuntimeObject_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, RuntimeObject * p1, int32_t p2, const RuntimeMethod* method);
	int16_t ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Single_t496865882_RuntimeObject_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef float (*Func)(void* obj, RuntimeObject * p1, int32_t p2, const RuntimeMethod* method);
	float ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Double_t1454265465_RuntimeObject_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef double (*Func)(void* obj, RuntimeObject * p1, int32_t p2, const RuntimeMethod* method);
	double ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_RuntimeObject_Int16_t769030278_SByte_t2530277047 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, int16_t p1, int8_t p2, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, *((int16_t*)args[0]), *((int8_t*)args[1]), methodMetadata);
	return ret;
}

void* RuntimeInvoker_Void_t1078098495_Int32_t2571135199_RuntimeObject_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, RuntimeObject * p2, RuntimeObject * p3, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((int32_t*)args[0]), (RuntimeObject *)args[1], (RuntimeObject *)args[2], methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_Int32_t2571135199_Single_t496865882_RuntimeObject_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, float p2, RuntimeObject * p3, RuntimeObject * p4, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((int32_t*)args[0]), *((float*)args[1]), (RuntimeObject *)args[2], (RuntimeObject *)args[3], methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_Int32_t2571135199_Single_t496865882 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, float p2, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((int32_t*)args[0]), *((float*)args[1]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_RuntimeObject_Single_t496865882_RuntimeObject_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RuntimeObject * p1, float p2, RuntimeObject * p3, RuntimeObject * p4, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((float*)args[1]), (RuntimeObject *)args[2], (RuntimeObject *)args[3], methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_Int32_t2571135199_Single_t496865882_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, float p2, RuntimeObject * p3, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((int32_t*)args[0]), *((float*)args[1]), (RuntimeObject *)args[2], methodMetadata);
	return NULL;
}

void* RuntimeInvoker_DictionaryEntry_t1385909157 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef DictionaryEntry_t1385909157  (*Func)(void* obj, const RuntimeMethod* method);
	DictionaryEntry_t1385909157  ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_Int32_t2571135199_SByte_t2530277047_MethodBaseU26_t3663460261_Int32U26_t2596978809_Int32U26_t2596978809_StringU26_t2155547156_Int32U26_t2596978809_Int32U26_t2596978809 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int8_t p2, MethodBase_t674153939 ** p3, int32_t* p4, int32_t* p5, String_t** p6, int32_t* p7, int32_t* p8, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, *((int32_t*)args[0]), *((int8_t*)args[1]), (MethodBase_t674153939 **)args[2], (int32_t*)args[3], (int32_t*)args[4], (String_t**)args[5], (int32_t*)args[6], (int32_t*)args[7], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Void_t1078098495_RuntimeObject_Int32_t2571135199_SByte_t2530277047_SByte_t2530277047 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RuntimeObject * p1, int32_t p2, int8_t p3, int8_t p4, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_RuntimeObject_RuntimeObject_Int32_t2571135199_SByte_t2530277047 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, RuntimeObject * p1, int32_t p2, int8_t p3, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), methodMetadata);
	return ret;
}

void* RuntimeInvoker_Int32_t2571135199_DateTime_t507798353 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, DateTime_t507798353  p1, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, *((DateTime_t507798353 *)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_DayOfWeek_t61775933_DateTime_t507798353 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, DateTime_t507798353  p1, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, *((DateTime_t507798353 *)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int32_t2571135199_Int32U26_t2596978809_Int32_t2571135199_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t* p1, int32_t p2, int32_t p3, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, (int32_t*)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_DayOfWeek_t61775933_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, *((int32_t*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Void_t1078098495_Int32U26_t2596978809_Int32U26_t2596978809_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, int32_t* p2, int32_t p3, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (int32_t*)args[0], (int32_t*)args[1], *((int32_t*)args[2]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_Int32U26_t2596978809_Int32U26_t2596978809_Int32U26_t2596978809_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, int32_t* p2, int32_t* p3, int32_t p4, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (int32_t*)args[0], (int32_t*)args[1], (int32_t*)args[2], *((int32_t*)args[3]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Int32_t2571135199_RuntimeObject_Int32_t2571135199_Int32_t2571135199_RuntimeObject_Int32_t2571135199_SByte_t2530277047 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RuntimeObject * p1, int32_t p2, int32_t p3, RuntimeObject * p4, int32_t p5, int8_t p6, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (RuntimeObject *)args[3], *((int32_t*)args[4]), *((int8_t*)args[5]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Void_t1078098495_Int32_t2571135199_SByte_t2530277047_SByte_t2530277047 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int8_t p2, int8_t p3, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((int32_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_RuntimeObject_SByte_t2530277047_SByte_t2530277047 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RuntimeObject * p1, int8_t p2, int8_t p3, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int8_t*)args[1]), *((int8_t*)args[2]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_RuntimeObject_Int16_t769030278 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, int16_t p1, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, *((int16_t*)args[0]), methodMetadata);
	return ret;
}

void* RuntimeInvoker_Void_t1078098495_DateTime_t507798353_DateTime_t507798353_TimeSpan_t2986675223 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, DateTime_t507798353  p1, DateTime_t507798353  p2, TimeSpan_t2986675223  p3, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((DateTime_t507798353 *)args[0]), *((DateTime_t507798353 *)args[1]), *((TimeSpan_t2986675223 *)args[2]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_TimeSpan_t2986675223 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef TimeSpan_t2986675223  (*Func)(void* obj, const RuntimeMethod* method);
	TimeSpan_t2986675223  ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Void_t1078098495_RuntimeObject_Int32_t2571135199_RuntimeObject_SByte_t2530277047 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RuntimeObject * p1, int32_t p2, RuntimeObject * p3, int8_t p4, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), (RuntimeObject *)args[2], *((int8_t*)args[3]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Int32_t2571135199_RuntimeObject_Int32_t2571135199_Int32_t2571135199_Int32U26_t2596978809 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RuntimeObject * p1, int32_t p2, int32_t p3, int32_t* p4, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (int32_t*)args[3], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Char_t3572527572 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef Il2CppChar (*Func)(void* obj, const RuntimeMethod* method);
	Il2CppChar ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Decimal_t2656901032 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef Decimal_t2656901032  (*Func)(void* obj, const RuntimeMethod* method);
	Decimal_t2656901032  ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Double_t1454265465 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef double (*Func)(void* obj, const RuntimeMethod* method);
	double ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int16_t769030278 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, const RuntimeMethod* method);
	int16_t ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_SByte_t2530277047 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, const RuntimeMethod* method);
	int8_t ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Single_t496865882 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef float (*Func)(void* obj, const RuntimeMethod* method);
	float ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_UInt16_t11709673 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, const RuntimeMethod* method);
	uint16_t ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_UInt32_t2739056616 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, const RuntimeMethod* method);
	uint32_t ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_UInt64_t2265270229 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, const RuntimeMethod* method);
	uint64_t ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Void_t1078098495_IntPtr_t_Int32_t2571135199_SByte_t2530277047_Int32_t2571135199_SByte_t2530277047_SByte_t2530277047 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, intptr_t p1, int32_t p2, int8_t p3, int32_t p4, int8_t p5, int8_t p6, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((intptr_t*)args[0]), *((int32_t*)args[1]), *((int8_t*)args[2]), *((int32_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_RuntimeObject_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199_SByte_t2530277047_SByte_t2530277047 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RuntimeObject * p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, int8_t p6, int8_t p7, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), *((int8_t*)args[6]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_RuntimeObject_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199_SByte_t2530277047_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RuntimeObject * p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, int8_t p6, int32_t p7, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), *((int32_t*)args[6]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_RuntimeObject_RuntimeObject_Int32_t2571135199_Int32_t2571135199_RuntimeObject_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, RuntimeObject * p1, int32_t p2, int32_t p3, RuntimeObject * p4, RuntimeObject * p5, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (RuntimeObject *)args[3], (RuntimeObject *)args[4], methodMetadata);
	return ret;
}

void* RuntimeInvoker_Int64_t384086013_Int64_t384086013_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, int64_t p1, int32_t p2, const RuntimeMethod* method);
	int64_t ret = ((Func)methodPointer)(obj, *((int64_t*)args[0]), *((int32_t*)args[1]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int32_t2571135199_IntPtr_t_RuntimeObject_Int32_t2571135199_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, intptr_t p1, RuntimeObject * p2, int32_t p3, int32_t p4, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, *((intptr_t*)args[0]), (RuntimeObject *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Void_t1078098495_RuntimeObject_Int32_t2571135199_Int32_t2571135199_SByte_t2530277047_SByte_t2530277047 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RuntimeObject * p1, int32_t p2, int32_t p3, int8_t p4, int8_t p5, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Boolean_t2059672899_RuntimeObject_MonoIOErrorU26_t2040356602 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, RuntimeObject * p1, int32_t* p2, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], (int32_t*)args[1], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_RuntimeObject_RuntimeObject_RuntimeObject_Int32_t2571135199_Int32_t2571135199_MonoIOErrorU26_t2040356602 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, RuntimeObject * p1, RuntimeObject * p2, int32_t p3, int32_t p4, int32_t* p5, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], (RuntimeObject *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (int32_t*)args[4], methodMetadata);
	return ret;
}

void* RuntimeInvoker_RuntimeObject_MonoIOErrorU26_t2040356602 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, int32_t* p1, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, (int32_t*)args[0], methodMetadata);
	return ret;
}

void* RuntimeInvoker_FileAttributes_t1797260272_RuntimeObject_MonoIOErrorU26_t2040356602 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RuntimeObject * p1, int32_t* p2, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], (int32_t*)args[1], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_MonoFileType_t2271609547_IntPtr_t_MonoIOErrorU26_t2040356602 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, intptr_t p1, int32_t* p2, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, *((intptr_t*)args[0]), (int32_t*)args[1], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_RuntimeObject_MonoIOStatU26_t3133205914_MonoIOErrorU26_t2040356602 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, RuntimeObject * p1, MonoIOStat_t421315798 * p2, int32_t* p3, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], (MonoIOStat_t421315798 *)args[1], (int32_t*)args[2], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_IntPtr_t_RuntimeObject_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199_MonoIOErrorU26_t2040356602 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef intptr_t (*Func)(void* obj, RuntimeObject * p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, int32_t* p6, const RuntimeMethod* method);
	intptr_t ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), (int32_t*)args[5], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_IntPtr_t_MonoIOErrorU26_t2040356602 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, intptr_t p1, int32_t* p2, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, *((intptr_t*)args[0]), (int32_t*)args[1], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int32_t2571135199_IntPtr_t_RuntimeObject_Int32_t2571135199_Int32_t2571135199_MonoIOErrorU26_t2040356602 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, intptr_t p1, RuntimeObject * p2, int32_t p3, int32_t p4, int32_t* p5, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, *((intptr_t*)args[0]), (RuntimeObject *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (int32_t*)args[4], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int64_t384086013_IntPtr_t_Int64_t384086013_Int32_t2571135199_MonoIOErrorU26_t2040356602 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, intptr_t p1, int64_t p2, int32_t p3, int32_t* p4, const RuntimeMethod* method);
	int64_t ret = ((Func)methodPointer)(obj, *((intptr_t*)args[0]), *((int64_t*)args[1]), *((int32_t*)args[2]), (int32_t*)args[3], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int64_t384086013_IntPtr_t_MonoIOErrorU26_t2040356602 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, intptr_t p1, int32_t* p2, const RuntimeMethod* method);
	int64_t ret = ((Func)methodPointer)(obj, *((intptr_t*)args[0]), (int32_t*)args[1], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_IntPtr_t_Int64_t384086013_MonoIOErrorU26_t2040356602 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, intptr_t p1, int64_t p2, int32_t* p3, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, *((intptr_t*)args[0]), *((int64_t*)args[1]), (int32_t*)args[2], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_RuntimeObject_StringU26_t2155547156 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, RuntimeObject * p1, String_t** p2, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], (String_t**)args[1], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Void_t1078098495_RuntimeObject_RuntimeObject_SByte_t2530277047_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RuntimeObject * p1, RuntimeObject * p2, int8_t p3, int32_t p4, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (RuntimeObject *)args[0], (RuntimeObject *)args[1], *((int8_t*)args[2]), *((int32_t*)args[3]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_Int16_t769030278 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t p1, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((int16_t*)args[0]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_RuntimeObject_Int32_t2571135199_Int32_t2571135199_RuntimeObject_RuntimeObject_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RuntimeObject * p1, int32_t p2, int32_t p3, RuntimeObject * p4, RuntimeObject * p5, RuntimeObject * p6, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (RuntimeObject *)args[3], (RuntimeObject *)args[4], (RuntimeObject *)args[5], methodMetadata);
	return NULL;
}

void* RuntimeInvoker_CallingConventions_t1168634181 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_RuntimeMethodHandle_t2877888302 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeMethodHandle_t2877888302  (*Func)(void* obj, const RuntimeMethod* method);
	RuntimeMethodHandle_t2877888302  ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_MethodAttributes_t3855721989 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_MethodToken_t1430837147 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef MethodToken_t1430837147  (*Func)(void* obj, const RuntimeMethod* method);
	MethodToken_t1430837147  ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_FieldAttributes_t1107365117 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_RuntimeFieldHandle_t1782795956 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeFieldHandle_t1782795956  (*Func)(void* obj, const RuntimeMethod* method);
	RuntimeFieldHandle_t1782795956  ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Void_t1078098495_RuntimeObject_RuntimeObject_Int32_t2571135199_RuntimeObject_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RuntimeObject * p1, RuntimeObject * p2, int32_t p3, RuntimeObject * p4, RuntimeObject * p5, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (RuntimeObject *)args[0], (RuntimeObject *)args[1], *((int32_t*)args[2]), (RuntimeObject *)args[3], (RuntimeObject *)args[4], methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_OpCode_t376486760 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, OpCode_t376486760  p1, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((OpCode_t376486760 *)args[0]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_OpCode_t376486760_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, OpCode_t376486760  p1, RuntimeObject * p2, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((OpCode_t376486760 *)args[0]), (RuntimeObject *)args[1], methodMetadata);
	return NULL;
}

void* RuntimeInvoker_StackBehaviour_t1946418481 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_PropertyAttributes_t820793723 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Void_t1078098495_RuntimeObject_RuntimeObject_Int32_t2571135199_RuntimeObject_RuntimeObject_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RuntimeObject * p1, RuntimeObject * p2, int32_t p3, RuntimeObject * p4, RuntimeObject * p5, RuntimeObject * p6, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (RuntimeObject *)args[0], (RuntimeObject *)args[1], *((int32_t*)args[2]), (RuntimeObject *)args[3], (RuntimeObject *)args[4], (RuntimeObject *)args[5], methodMetadata);
	return NULL;
}

void* RuntimeInvoker_RuntimeObject_Int32_t2571135199_Int32_t2571135199_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, int32_t p1, int32_t p2, RuntimeObject * p3, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (RuntimeObject *)args[2], methodMetadata);
	return ret;
}

void* RuntimeInvoker_RuntimeObject_Int32_t2571135199_Int32_t2571135199_RuntimeObject_RuntimeObject_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, int32_t p1, int32_t p2, RuntimeObject * p3, RuntimeObject * p4, RuntimeObject * p5, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (RuntimeObject *)args[2], (RuntimeObject *)args[3], (RuntimeObject *)args[4], methodMetadata);
	return ret;
}

void* RuntimeInvoker_RuntimeObject_RuntimeObject_Int32_t2571135199_SByte_t2530277047_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, RuntimeObject * p1, int32_t p2, int8_t p3, RuntimeObject * p4, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), (RuntimeObject *)args[3], methodMetadata);
	return ret;
}

void* RuntimeInvoker_IntPtr_t_RuntimeObject_Int32U26_t2596978809_ModuleU26_t1352866505 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef intptr_t (*Func)(void* obj, RuntimeObject * p1, int32_t* p2, Module_t2594760207 ** p3, const RuntimeMethod* method);
	intptr_t ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], (int32_t*)args[1], (Module_t2594760207 **)args[2], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_RuntimeObject_RuntimeObject_RuntimeObject_SByte_t2530277047_SByte_t2530277047 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, RuntimeObject * p1, RuntimeObject * p2, int8_t p3, int8_t p4, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], (RuntimeObject *)args[1], *((int8_t*)args[2]), *((int8_t*)args[3]), methodMetadata);
	return ret;
}

void* RuntimeInvoker_AssemblyNameFlags_t899890916 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_RuntimeObject_Int32_t2571135199_RuntimeObject_ObjectU5BU5DU26_t3608108294_RuntimeObject_RuntimeObject_RuntimeObject_ObjectU26_t4136591053 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, int32_t p1, RuntimeObject * p2, ObjectU5BU5D_t4290686858** p3, RuntimeObject * p4, RuntimeObject * p5, RuntimeObject * p6, RuntimeObject ** p7, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, *((int32_t*)args[0]), (RuntimeObject *)args[1], (ObjectU5BU5D_t4290686858**)args[2], (RuntimeObject *)args[3], (RuntimeObject *)args[4], (RuntimeObject *)args[5], (RuntimeObject **)args[6], methodMetadata);
	return ret;
}

void* RuntimeInvoker_Void_t1078098495_ObjectU5BU5DU26_t3608108294_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ObjectU5BU5D_t4290686858** p1, RuntimeObject * p2, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (ObjectU5BU5D_t4290686858**)args[0], (RuntimeObject *)args[1], methodMetadata);
	return NULL;
}

void* RuntimeInvoker_RuntimeObject_Int32_t2571135199_RuntimeObject_RuntimeObject_RuntimeObject_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, int32_t p1, RuntimeObject * p2, RuntimeObject * p3, RuntimeObject * p4, RuntimeObject * p5, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, *((int32_t*)args[0]), (RuntimeObject *)args[1], (RuntimeObject *)args[2], (RuntimeObject *)args[3], (RuntimeObject *)args[4], methodMetadata);
	return ret;
}

void* RuntimeInvoker_Void_t1078098495_RuntimeObject_ObjectU5BU5DU26_t3608108294_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RuntimeObject * p1, ObjectU5BU5D_t4290686858** p2, RuntimeObject * p3, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (RuntimeObject *)args[0], (ObjectU5BU5D_t4290686858**)args[1], (RuntimeObject *)args[2], methodMetadata);
	return NULL;
}

void* RuntimeInvoker_RuntimeObject_Int32_t2571135199_RuntimeObject_RuntimeObject_RuntimeObject_SByte_t2530277047 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, int32_t p1, RuntimeObject * p2, RuntimeObject * p3, RuntimeObject * p4, int8_t p5, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, *((int32_t*)args[0]), (RuntimeObject *)args[1], (RuntimeObject *)args[2], (RuntimeObject *)args[3], *((int8_t*)args[4]), methodMetadata);
	return ret;
}

void* RuntimeInvoker_EventAttributes_t1621333428 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Void_t1078098495_RuntimeObject_RuntimeObject_RuntimeObject_RuntimeObject_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RuntimeObject * p1, RuntimeObject * p2, RuntimeObject * p3, RuntimeObject * p4, int32_t p5, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (RuntimeObject *)args[0], (RuntimeObject *)args[1], (RuntimeObject *)args[2], (RuntimeObject *)args[3], *((int32_t*)args[4]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_RuntimeObject_RuntimeObject_RuntimeObject_RuntimeObject_Int32_t2571135199_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RuntimeObject * p1, RuntimeObject * p2, RuntimeObject * p3, RuntimeObject * p4, int32_t p5, RuntimeObject * p6, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (RuntimeObject *)args[0], (RuntimeObject *)args[1], (RuntimeObject *)args[2], (RuntimeObject *)args[3], *((int32_t*)args[4]), (RuntimeObject *)args[5], methodMetadata);
	return NULL;
}

void* RuntimeInvoker_RuntimeObject_StreamingContext_t1316667400 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, StreamingContext_t1316667400  p1, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, *((StreamingContext_t1316667400 *)args[0]), methodMetadata);
	return ret;
}

void* RuntimeInvoker_Void_t1078098495_RuntimeObject_MonoEventInfoU26_t4282179039 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RuntimeObject * p1, MonoEventInfo_t802398473 * p2, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (RuntimeObject *)args[0], (MonoEventInfo_t802398473 *)args[1], methodMetadata);
	return NULL;
}

void* RuntimeInvoker_MonoEventInfo_t802398473_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef MonoEventInfo_t802398473  (*Func)(void* obj, RuntimeObject * p1, const RuntimeMethod* method);
	MonoEventInfo_t802398473  ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Void_t1078098495_IntPtr_t_MonoMethodInfoU26_t149925759 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, intptr_t p1, MonoMethodInfo_t1860908649 * p2, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((intptr_t*)args[0]), (MonoMethodInfo_t1860908649 *)args[1], methodMetadata);
	return NULL;
}

void* RuntimeInvoker_MonoMethodInfo_t1860908649_IntPtr_t (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef MonoMethodInfo_t1860908649  (*Func)(void* obj, intptr_t p1, const RuntimeMethod* method);
	MonoMethodInfo_t1860908649  ret = ((Func)methodPointer)(obj, *((intptr_t*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_MethodAttributes_t3855721989_IntPtr_t (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, intptr_t p1, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, *((intptr_t*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_CallingConventions_t1168634181_IntPtr_t (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, intptr_t p1, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, *((intptr_t*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_RuntimeObject_IntPtr_t_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, intptr_t p1, RuntimeObject * p2, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, *((intptr_t*)args[0]), (RuntimeObject *)args[1], methodMetadata);
	return ret;
}

void* RuntimeInvoker_RuntimeObject_RuntimeObject_RuntimeObject_ExceptionU26_t1670763973 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, RuntimeObject * p1, RuntimeObject * p2, Exception_t3361176243 ** p3, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], (RuntimeObject *)args[1], (Exception_t3361176243 **)args[2], methodMetadata);
	return ret;
}

void* RuntimeInvoker_Void_t1078098495_RuntimeObject_MonoPropertyInfoU26_t1411063087_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RuntimeObject * p1, MonoPropertyInfo_t3452547385 * p2, int32_t p3, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (RuntimeObject *)args[0], (MonoPropertyInfo_t3452547385 *)args[1], *((int32_t*)args[2]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_RuntimeObject_RuntimeObject_RuntimeObject_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RuntimeObject * p1, RuntimeObject * p2, RuntimeObject * p3, int32_t p4, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (RuntimeObject *)args[0], (RuntimeObject *)args[1], (RuntimeObject *)args[2], *((int32_t*)args[3]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_ParameterAttributes_t3383706087 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Void_t1078098495_Int64_t384086013_ResourceInfoU26_t4212590071 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, ResourceInfo_t1773833009 * p2, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((int64_t*)args[0]), (ResourceInfo_t1773833009 *)args[1], methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_RuntimeObject_Int64_t384086013_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RuntimeObject * p1, int64_t p2, int32_t p3, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int64_t*)args[1]), *((int32_t*)args[2]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_GCHandle_t4215464244_RuntimeObject_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef GCHandle_t4215464244  (*Func)(void* obj, RuntimeObject * p1, int32_t p2, const RuntimeMethod* method);
	GCHandle_t4215464244  ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Void_t1078098495_IntPtr_t_Int32_t2571135199_RuntimeObject_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, intptr_t p1, int32_t p2, RuntimeObject * p3, int32_t p4, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((intptr_t*)args[0]), *((int32_t*)args[1]), (RuntimeObject *)args[2], *((int32_t*)args[3]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_IntPtr_t_RuntimeObject_Int32_t2571135199_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, intptr_t p1, RuntimeObject * p2, int32_t p3, int32_t p4, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((intptr_t*)args[0]), (RuntimeObject *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Byte_t880488320_IntPtr_t_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, intptr_t p1, int32_t p2, const RuntimeMethod* method);
	uint8_t ret = ((Func)methodPointer)(obj, *((intptr_t*)args[0]), *((int32_t*)args[1]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Void_t1078098495_IntPtr_t_Int32_t2571135199_SByte_t2530277047 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, intptr_t p1, int32_t p2, int8_t p3, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((intptr_t*)args[0]), *((int32_t*)args[1]), *((int8_t*)args[2]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_BooleanU26_t495120821 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, bool* p1, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (bool*)args[0], methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_IntPtr_t (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, intptr_t p1, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((intptr_t*)args[0]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_RuntimeObject_RuntimeObject_RuntimeObject_StringU26_t2155547156 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, RuntimeObject * p1, RuntimeObject * p2, String_t** p3, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], (RuntimeObject *)args[1], (String_t**)args[2], methodMetadata);
	return ret;
}

void* RuntimeInvoker_RuntimeObject_RuntimeObject_RuntimeObject_RuntimeObject_StringU26_t2155547156 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, RuntimeObject * p1, RuntimeObject * p2, RuntimeObject * p3, String_t** p4, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], (RuntimeObject *)args[1], (RuntimeObject *)args[2], (String_t**)args[3], methodMetadata);
	return ret;
}

void* RuntimeInvoker_Boolean_t2059672899_RuntimeObject_RuntimeObject_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, RuntimeObject * p1, RuntimeObject * p2, RuntimeObject * p3, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], (RuntimeObject *)args[1], (RuntimeObject *)args[2], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Void_t1078098495_SByte_t2530277047_RuntimeObject_SByte_t2530277047_SByte_t2530277047 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, RuntimeObject * p2, int8_t p3, int8_t p4, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((int8_t*)args[0]), (RuntimeObject *)args[1], *((int8_t*)args[2]), *((int8_t*)args[3]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_TimeSpan_t2986675223 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, TimeSpan_t2986675223  p1, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((TimeSpan_t2986675223 *)args[0]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_RuntimeObject_Byte_t880488320 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RuntimeObject * p1, uint8_t p2, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((uint8_t*)args[1]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_RuntimeObject_RuntimeObject_SByte_t2530277047_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RuntimeObject * p1, RuntimeObject * p2, int8_t p3, RuntimeObject * p4, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (RuntimeObject *)args[0], (RuntimeObject *)args[1], *((int8_t*)args[2]), (RuntimeObject *)args[3], methodMetadata);
	return NULL;
}

void* RuntimeInvoker_RuntimeObject_RuntimeObject_RuntimeObject_StreamingContext_t1316667400_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, RuntimeObject * p1, RuntimeObject * p2, StreamingContext_t1316667400  p3, RuntimeObject * p4, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], (RuntimeObject *)args[1], *((StreamingContext_t1316667400 *)args[2]), (RuntimeObject *)args[3], methodMetadata);
	return ret;
}

void* RuntimeInvoker_RuntimeObject_RuntimeObject_StreamingContext_t1316667400_ISurrogateSelectorU26_t193635247 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, RuntimeObject * p1, StreamingContext_t1316667400  p2, RuntimeObject** p3, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((StreamingContext_t1316667400 *)args[1]), (RuntimeObject**)args[2], methodMetadata);
	return ret;
}

void* RuntimeInvoker_Void_t1078098495_RuntimeObject_IntPtr_t_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RuntimeObject * p1, intptr_t p2, RuntimeObject * p3, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((intptr_t*)args[1]), (RuntimeObject *)args[2], methodMetadata);
	return NULL;
}

void* RuntimeInvoker_TimeSpan_t2986675223_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef TimeSpan_t2986675223  (*Func)(void* obj, RuntimeObject * p1, const RuntimeMethod* method);
	TimeSpan_t2986675223  ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_RuntimeObject_StringU26_t2155547156 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, String_t** p1, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, (String_t**)args[0], methodMetadata);
	return ret;
}

void* RuntimeInvoker_RuntimeObject_RuntimeObject_RuntimeObject_ObjectU26_t4136591053 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, RuntimeObject * p1, RuntimeObject * p2, RuntimeObject ** p3, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], (RuntimeObject *)args[1], (RuntimeObject **)args[2], methodMetadata);
	return ret;
}

void* RuntimeInvoker_Boolean_t2059672899_RuntimeObject_StringU26_t2155547156_StringU26_t2155547156 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, RuntimeObject * p1, String_t** p2, String_t** p3, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], (String_t**)args[1], (String_t**)args[2], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_WellKnownObjectMode_t2386524380 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_StreamingContext_t1316667400 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef StreamingContext_t1316667400  (*Func)(void* obj, const RuntimeMethod* method);
	StreamingContext_t1316667400  ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_TypeFilterLevel_t3266183943 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Void_t1078098495_RuntimeObject_BooleanU26_t495120821 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RuntimeObject * p1, bool* p2, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (RuntimeObject *)args[0], (bool*)args[1], methodMetadata);
	return NULL;
}

void* RuntimeInvoker_RuntimeObject_Byte_t880488320_RuntimeObject_SByte_t2530277047_RuntimeObject_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, uint8_t p1, RuntimeObject * p2, int8_t p3, RuntimeObject * p4, RuntimeObject * p5, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, *((uint8_t*)args[0]), (RuntimeObject *)args[1], *((int8_t*)args[2]), (RuntimeObject *)args[3], (RuntimeObject *)args[4], methodMetadata);
	return ret;
}

void* RuntimeInvoker_RuntimeObject_Byte_t880488320_RuntimeObject_SByte_t2530277047_RuntimeObject_RuntimeObject_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, uint8_t p1, RuntimeObject * p2, int8_t p3, RuntimeObject * p4, RuntimeObject * p5, RuntimeObject * p6, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, *((uint8_t*)args[0]), (RuntimeObject *)args[1], *((int8_t*)args[2]), (RuntimeObject *)args[3], (RuntimeObject *)args[4], (RuntimeObject *)args[5], methodMetadata);
	return ret;
}

void* RuntimeInvoker_Void_t1078098495_RuntimeObject_SByte_t2530277047_ObjectU26_t4136591053_HeaderU5BU5DU26_t1438187576 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RuntimeObject * p1, int8_t p2, RuntimeObject ** p3, HeaderU5BU5D_t1468929288** p4, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int8_t*)args[1]), (RuntimeObject **)args[2], (HeaderU5BU5D_t1468929288**)args[3], methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_Byte_t880488320_RuntimeObject_SByte_t2530277047_ObjectU26_t4136591053_HeaderU5BU5DU26_t1438187576 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint8_t p1, RuntimeObject * p2, int8_t p3, RuntimeObject ** p4, HeaderU5BU5D_t1468929288** p5, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((uint8_t*)args[0]), (RuntimeObject *)args[1], *((int8_t*)args[2]), (RuntimeObject **)args[3], (HeaderU5BU5D_t1468929288**)args[4], methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Boolean_t2059672899_Byte_t880488320_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, uint8_t p1, RuntimeObject * p2, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, *((uint8_t*)args[0]), (RuntimeObject *)args[1], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Void_t1078098495_Byte_t880488320_RuntimeObject_Int64U26_t4038210315_ObjectU26_t4136591053_SerializationInfoU26_t114801638 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint8_t p1, RuntimeObject * p2, int64_t* p3, RuntimeObject ** p4, SerializationInfo_t3667883434 ** p5, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((uint8_t*)args[0]), (RuntimeObject *)args[1], (int64_t*)args[2], (RuntimeObject **)args[3], (SerializationInfo_t3667883434 **)args[4], methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_RuntimeObject_SByte_t2530277047_SByte_t2530277047_Int64U26_t4038210315_ObjectU26_t4136591053_SerializationInfoU26_t114801638 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RuntimeObject * p1, int8_t p2, int8_t p3, int64_t* p4, RuntimeObject ** p5, SerializationInfo_t3667883434 ** p6, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int8_t*)args[1]), *((int8_t*)args[2]), (int64_t*)args[3], (RuntimeObject **)args[4], (SerializationInfo_t3667883434 **)args[5], methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_RuntimeObject_Int64U26_t4038210315_ObjectU26_t4136591053_SerializationInfoU26_t114801638 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RuntimeObject * p1, int64_t* p2, RuntimeObject ** p3, SerializationInfo_t3667883434 ** p4, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (RuntimeObject *)args[0], (int64_t*)args[1], (RuntimeObject **)args[2], (SerializationInfo_t3667883434 **)args[3], methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_RuntimeObject_RuntimeObject_Int64_t384086013_ObjectU26_t4136591053_SerializationInfoU26_t114801638 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RuntimeObject * p1, RuntimeObject * p2, int64_t p3, RuntimeObject ** p4, SerializationInfo_t3667883434 ** p5, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (RuntimeObject *)args[0], (RuntimeObject *)args[1], *((int64_t*)args[2]), (RuntimeObject **)args[3], (SerializationInfo_t3667883434 **)args[4], methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_Int64_t384086013_RuntimeObject_RuntimeObject_Int64_t384086013_RuntimeObject_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, RuntimeObject * p2, RuntimeObject * p3, int64_t p4, RuntimeObject * p5, RuntimeObject * p6, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((int64_t*)args[0]), (RuntimeObject *)args[1], (RuntimeObject *)args[2], *((int64_t*)args[3]), (RuntimeObject *)args[4], (RuntimeObject *)args[5], methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_RuntimeObject_Int64U26_t4038210315_ObjectU26_t4136591053 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RuntimeObject * p1, int64_t* p2, RuntimeObject ** p3, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (RuntimeObject *)args[0], (int64_t*)args[1], (RuntimeObject **)args[2], methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_RuntimeObject_RuntimeObject_Int64U26_t4038210315_ObjectU26_t4136591053 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RuntimeObject * p1, RuntimeObject * p2, int64_t* p3, RuntimeObject ** p4, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (RuntimeObject *)args[0], (RuntimeObject *)args[1], (int64_t*)args[2], (RuntimeObject **)args[3], methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_RuntimeObject_RuntimeObject_Int64_t384086013_RuntimeObject_RuntimeObject_RuntimeObject_RuntimeObject_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RuntimeObject * p1, RuntimeObject * p2, int64_t p3, RuntimeObject * p4, RuntimeObject * p5, RuntimeObject * p6, RuntimeObject * p7, RuntimeObject * p8, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (RuntimeObject *)args[0], (RuntimeObject *)args[1], *((int64_t*)args[2]), (RuntimeObject *)args[3], (RuntimeObject *)args[4], (RuntimeObject *)args[5], (RuntimeObject *)args[6], (RuntimeObject *)args[7], methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_RuntimeObject_RuntimeObject_RuntimeObject_RuntimeObject_RuntimeObject_RuntimeObject_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RuntimeObject * p1, RuntimeObject * p2, RuntimeObject * p3, RuntimeObject * p4, RuntimeObject * p5, RuntimeObject * p6, RuntimeObject * p7, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (RuntimeObject *)args[0], (RuntimeObject *)args[1], (RuntimeObject *)args[2], (RuntimeObject *)args[3], (RuntimeObject *)args[4], (RuntimeObject *)args[5], (RuntimeObject *)args[6], methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_Int64_t384086013_Int64_t384086013_RuntimeObject_RuntimeObject_RuntimeObject_RuntimeObject_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, int64_t p2, RuntimeObject * p3, RuntimeObject * p4, RuntimeObject * p5, RuntimeObject * p6, RuntimeObject * p7, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((int64_t*)args[0]), *((int64_t*)args[1]), (RuntimeObject *)args[2], (RuntimeObject *)args[3], (RuntimeObject *)args[4], (RuntimeObject *)args[5], (RuntimeObject *)args[6], methodMetadata);
	return NULL;
}

void* RuntimeInvoker_RuntimeObject_Int64_t384086013_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, int64_t p1, RuntimeObject * p2, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, *((int64_t*)args[0]), (RuntimeObject *)args[1], methodMetadata);
	return ret;
}

void* RuntimeInvoker_RuntimeObject_RuntimeObject_Byte_t880488320 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, RuntimeObject * p1, uint8_t p2, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((uint8_t*)args[1]), methodMetadata);
	return ret;
}

void* RuntimeInvoker_Void_t1078098495_Int64_t384086013_Int32_t2571135199_Int64_t384086013 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, int32_t p2, int64_t p3, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((int64_t*)args[0]), *((int32_t*)args[1]), *((int64_t*)args[2]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_Int64_t384086013_RuntimeObject_Int64_t384086013 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, RuntimeObject * p2, int64_t p3, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((int64_t*)args[0]), (RuntimeObject *)args[1], *((int64_t*)args[2]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_RuntimeObject_Int64_t384086013_RuntimeObject_Int64_t384086013_RuntimeObject_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RuntimeObject * p1, int64_t p2, RuntimeObject * p3, int64_t p4, RuntimeObject * p5, RuntimeObject * p6, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int64_t*)args[1]), (RuntimeObject *)args[2], *((int64_t*)args[3]), (RuntimeObject *)args[4], (RuntimeObject *)args[5], methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Boolean_t2059672899_SByte_t2530277047_RuntimeObject_SByte_t2530277047 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int8_t p1, RuntimeObject * p2, int8_t p3, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, *((int8_t*)args[0]), (RuntimeObject *)args[1], *((int8_t*)args[2]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_RuntimeObject_RuntimeObject_StreamingContext_t1316667400 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, RuntimeObject * p1, RuntimeObject * p2, StreamingContext_t1316667400  p3, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], (RuntimeObject *)args[1], *((StreamingContext_t1316667400 *)args[2]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Void_t1078098495_RuntimeObject_RuntimeObject_StreamingContext_t1316667400 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RuntimeObject * p1, RuntimeObject * p2, StreamingContext_t1316667400  p3, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (RuntimeObject *)args[0], (RuntimeObject *)args[1], *((StreamingContext_t1316667400 *)args[2]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_StreamingContext_t1316667400 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, StreamingContext_t1316667400  p1, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((StreamingContext_t1316667400 *)args[0]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_RuntimeObject_StreamingContext_t1316667400_RuntimeObject_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, StreamingContext_t1316667400  p1, RuntimeObject * p2, RuntimeObject * p3, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, *((StreamingContext_t1316667400 *)args[0]), (RuntimeObject *)args[1], (RuntimeObject *)args[2], methodMetadata);
	return ret;
}

void* RuntimeInvoker_SerializationEntry_t4251740542 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef SerializationEntry_t4251740542  (*Func)(void* obj, const RuntimeMethod* method);
	SerializationEntry_t4251740542  ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_StreamingContextStates_t4185925513 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_CspProviderFlags_t2530719326 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Void_t1078098495_RuntimeObject_SByte_t2530277047_RuntimeObject_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RuntimeObject * p1, int8_t p2, RuntimeObject * p3, RuntimeObject * p4, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int8_t*)args[1]), (RuntimeObject *)args[2], (RuntimeObject *)args[3], methodMetadata);
	return NULL;
}

void* RuntimeInvoker_UInt32_t2739056616_Int32_t2571135199_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, int32_t p1, int32_t p2, const RuntimeMethod* method);
	uint32_t ret = ((Func)methodPointer)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Void_t1078098495_RuntimeObject_RuntimeObject_RuntimeObject_SByte_t2530277047 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RuntimeObject * p1, RuntimeObject * p2, RuntimeObject * p3, int8_t p4, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (RuntimeObject *)args[0], (RuntimeObject *)args[1], (RuntimeObject *)args[2], *((int8_t*)args[3]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_Int64_t384086013_RuntimeObject_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, RuntimeObject * p2, int32_t p3, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((int64_t*)args[0]), (RuntimeObject *)args[1], *((int32_t*)args[2]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_UInt32_t2739056616_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, const RuntimeMethod* method);
	uint32_t ret = ((Func)methodPointer)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Void_t1078098495_UInt32U26_t3679184984_Int32_t2571135199_UInt32U26_t3679184984_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint32_t* p1, int32_t p2, uint32_t* p3, int32_t p4, int32_t p5, int32_t p6, int32_t p7, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (uint32_t*)args[0], *((int32_t*)args[1]), (uint32_t*)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), *((int32_t*)args[5]), *((int32_t*)args[6]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_IntPtr_t_IntPtr_t_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef intptr_t (*Func)(void* obj, intptr_t p1, RuntimeObject * p2, const RuntimeMethod* method);
	intptr_t ret = ((Func)methodPointer)(obj, *((intptr_t*)args[0]), (RuntimeObject *)args[1], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_UInt32_t2739056616_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, int32_t p1, const RuntimeMethod* method);
	uint32_t ret = ((Func)methodPointer)(obj, *((int32_t*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Void_t1078098495_Int64_t384086013_Int64_t384086013 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, int64_t p2, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((int64_t*)args[0]), *((int64_t*)args[1]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_UInt64_t2265270229_Int64_t384086013_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, int64_t p1, int32_t p2, const RuntimeMethod* method);
	uint64_t ret = ((Func)methodPointer)(obj, *((int64_t*)args[0]), *((int32_t*)args[1]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_UInt64_t2265270229_Int64_t384086013_Int64_t384086013_Int64_t384086013 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, int64_t p1, int64_t p2, int64_t p3, const RuntimeMethod* method);
	uint64_t ret = ((Func)methodPointer)(obj, *((int64_t*)args[0]), *((int64_t*)args[1]), *((int64_t*)args[2]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_UInt64_t2265270229_Int64_t384086013 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, int64_t p1, const RuntimeMethod* method);
	uint64_t ret = ((Func)methodPointer)(obj, *((int64_t*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_CipherMode_t1257099160 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_PaddingMode_t218725588 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Void_t1078098495_StringBuilderU26_t287250414_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, StringBuilder_t2076519010 ** p1, int32_t p2, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (StringBuilder_t2076519010 **)args[0], *((int32_t*)args[1]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_RuntimeObject_IntPtr_t_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, intptr_t p1, int32_t p2, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, *((intptr_t*)args[0]), *((int32_t*)args[1]), methodMetadata);
	return ret;
}

void* RuntimeInvoker_Int32_t2571135199_RuntimeObject_Int32_t2571135199_Int32_t2571135199_RuntimeObject_Int32_t2571135199_EncoderFallbackBufferU26_t95289058_CharU5BU5DU26_t3270425003 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RuntimeObject * p1, int32_t p2, int32_t p3, RuntimeObject * p4, int32_t p5, EncoderFallbackBuffer_t523190862 ** p6, CharU5BU5D_t2864810077** p7, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (RuntimeObject *)args[3], *((int32_t*)args[4]), (EncoderFallbackBuffer_t523190862 **)args[5], (CharU5BU5D_t2864810077**)args[6], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int32_t2571135199_RuntimeObject_Int32_t2571135199_Int32_t2571135199_RuntimeObject_Int32_t2571135199_DecoderFallbackBufferU26_t2401674479 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RuntimeObject * p1, int32_t p2, int32_t p3, RuntimeObject * p4, int32_t p5, DecoderFallbackBuffer_t1939825529 ** p6, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (RuntimeObject *)args[3], *((int32_t*)args[4]), (DecoderFallbackBuffer_t1939825529 **)args[5], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int32_t2571135199_RuntimeObject_Int32_t2571135199_RuntimeObject_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RuntimeObject * p1, int32_t p2, RuntimeObject * p3, int32_t p4, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), (RuntimeObject *)args[2], *((int32_t*)args[3]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_Int16_t769030278_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int16_t p1, int32_t p2, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, *((int16_t*)args[0]), *((int32_t*)args[1]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_Int16_t769030278_Int16_t769030278_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int16_t p1, int16_t p2, int32_t p3, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, *((int16_t*)args[0]), *((int16_t*)args[1]), *((int32_t*)args[2]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Void_t1078098495_Int16_t769030278_Int16_t769030278_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t p1, int16_t p2, int32_t p3, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((int16_t*)args[0]), *((int16_t*)args[1]), *((int32_t*)args[2]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_RuntimeObject_Int32U26_t2596978809 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, int32_t* p1, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, (int32_t*)args[0], methodMetadata);
	return ret;
}

void* RuntimeInvoker_Void_t1078098495_RuntimeObject_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RuntimeObject * p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_RuntimeObject_Int32_t2571135199_RuntimeObject_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, int32_t p1, RuntimeObject * p2, int32_t p3, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, *((int32_t*)args[0]), (RuntimeObject *)args[1], *((int32_t*)args[2]), methodMetadata);
	return ret;
}

void* RuntimeInvoker_Void_t1078098495_SByte_t2530277047_SByte_t2530277047_SByte_t2530277047 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int8_t p2, int8_t p3, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Int32_t2571135199_RuntimeObject_Int32_t2571135199_Int32_t2571135199_SByte_t2530277047_Int32_t2571135199_SByte_t2530277047_SByte_t2530277047 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RuntimeObject * p1, int32_t p2, int32_t p3, int8_t p4, int32_t p5, int8_t p6, int8_t p7, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int8_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), *((int8_t*)args[6]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int32_t2571135199_RuntimeObject_Int32_t2571135199_Int32_t2571135199_RuntimeObject_Int32_t2571135199_SByte_t2530277047_Int32U26_t2596978809_BooleanU26_t495120821_SByte_t2530277047 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RuntimeObject * p1, int32_t p2, int32_t p3, RuntimeObject * p4, int32_t p5, int8_t p6, int32_t* p7, bool* p8, int8_t p9, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (RuntimeObject *)args[3], *((int32_t*)args[4]), *((int8_t*)args[5]), (int32_t*)args[6], (bool*)args[7], *((int8_t*)args[8]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int32_t2571135199_RuntimeObject_Int32_t2571135199_Int32_t2571135199_RuntimeObject_Int32_t2571135199_Int32U26_t2596978809 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RuntimeObject * p1, int32_t p2, int32_t p3, RuntimeObject * p4, int32_t p5, int32_t* p6, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (RuntimeObject *)args[3], *((int32_t*)args[4]), (int32_t*)args[5], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int32_t2571135199_RuntimeObject_Int32_t2571135199_Int32_t2571135199_CharU26_t2918757708_SByte_t2530277047 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RuntimeObject * p1, int32_t p2, int32_t p3, Il2CppChar* p4, int8_t p5, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Il2CppChar*)args[3], *((int8_t*)args[4]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int32_t2571135199_RuntimeObject_Int32_t2571135199_CharU26_t2918757708_SByte_t2530277047 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RuntimeObject * p1, int32_t p2, Il2CppChar* p3, int8_t p4, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), (Il2CppChar*)args[2], *((int8_t*)args[3]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int32_t2571135199_RuntimeObject_Int32_t2571135199_Int32_t2571135199_RuntimeObject_Int32_t2571135199_CharU26_t2918757708_SByte_t2530277047 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RuntimeObject * p1, int32_t p2, int32_t p3, RuntimeObject * p4, int32_t p5, Il2CppChar* p6, int8_t p7, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (RuntimeObject *)args[3], *((int32_t*)args[4]), (Il2CppChar*)args[5], *((int8_t*)args[6]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int32_t2571135199_RuntimeObject_Int32_t2571135199_RuntimeObject_Int32_t2571135199_CharU26_t2918757708_SByte_t2530277047 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RuntimeObject * p1, int32_t p2, RuntimeObject * p3, int32_t p4, Il2CppChar* p5, int8_t p6, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), (RuntimeObject *)args[2], *((int32_t*)args[3]), (Il2CppChar*)args[4], *((int8_t*)args[5]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int32_t2571135199_RuntimeObject_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199_RuntimeObject_DecoderFallbackBufferU26_t2401674479_ByteU5BU5DU26_t321522471_SByte_t2530277047 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RuntimeObject * p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, RuntimeObject * p6, DecoderFallbackBuffer_t1939825529 ** p7, ByteU5BU5D_t1014014593** p8, int8_t p9, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), (RuntimeObject *)args[5], (DecoderFallbackBuffer_t1939825529 **)args[6], (ByteU5BU5D_t1014014593**)args[7], *((int8_t*)args[8]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int32_t2571135199_RuntimeObject_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199_RuntimeObject_DecoderFallbackBufferU26_t2401674479_ByteU5BU5DU26_t321522471_SByte_t2530277047 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RuntimeObject * p1, int32_t p2, int32_t p3, int32_t p4, RuntimeObject * p5, DecoderFallbackBuffer_t1939825529 ** p6, ByteU5BU5D_t1014014593** p7, int8_t p8, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), (RuntimeObject *)args[4], (DecoderFallbackBuffer_t1939825529 **)args[5], (ByteU5BU5D_t1014014593**)args[6], *((int8_t*)args[7]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int32_t2571135199_RuntimeObject_DecoderFallbackBufferU26_t2401674479_ByteU5BU5DU26_t321522471_RuntimeObject_Int64_t384086013_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RuntimeObject * p1, DecoderFallbackBuffer_t1939825529 ** p2, ByteU5BU5D_t1014014593** p3, RuntimeObject * p4, int64_t p5, int32_t p6, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], (DecoderFallbackBuffer_t1939825529 **)args[1], (ByteU5BU5D_t1014014593**)args[2], (RuntimeObject *)args[3], *((int64_t*)args[4]), *((int32_t*)args[5]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Void_t1078098495_RuntimeObject_DecoderFallbackBufferU26_t2401674479_ByteU5BU5DU26_t321522471_RuntimeObject_Int64_t384086013_Int32_t2571135199_RuntimeObject_Int32U26_t2596978809 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RuntimeObject * p1, DecoderFallbackBuffer_t1939825529 ** p2, ByteU5BU5D_t1014014593** p3, RuntimeObject * p4, int64_t p5, int32_t p6, RuntimeObject * p7, int32_t* p8, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (RuntimeObject *)args[0], (DecoderFallbackBuffer_t1939825529 **)args[1], (ByteU5BU5D_t1014014593**)args[2], (RuntimeObject *)args[3], *((int64_t*)args[4]), *((int32_t*)args[5]), (RuntimeObject *)args[6], (int32_t*)args[7], methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Int32_t2571135199_RuntimeObject_Int32_t2571135199_Int32_t2571135199_RuntimeObject_Int32_t2571135199_UInt32U26_t3679184984_UInt32U26_t3679184984_RuntimeObject_DecoderFallbackBufferU26_t2401674479_ByteU5BU5DU26_t321522471_SByte_t2530277047 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RuntimeObject * p1, int32_t p2, int32_t p3, RuntimeObject * p4, int32_t p5, uint32_t* p6, uint32_t* p7, RuntimeObject * p8, DecoderFallbackBuffer_t1939825529 ** p9, ByteU5BU5D_t1014014593** p10, int8_t p11, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (RuntimeObject *)args[3], *((int32_t*)args[4]), (uint32_t*)args[5], (uint32_t*)args[6], (RuntimeObject *)args[7], (DecoderFallbackBuffer_t1939825529 **)args[8], (ByteU5BU5D_t1014014593**)args[9], *((int8_t*)args[10]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int32_t2571135199_RuntimeObject_Int32_t2571135199_RuntimeObject_Int32_t2571135199_UInt32U26_t3679184984_UInt32U26_t3679184984_RuntimeObject_DecoderFallbackBufferU26_t2401674479_ByteU5BU5DU26_t321522471_SByte_t2530277047 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RuntimeObject * p1, int32_t p2, RuntimeObject * p3, int32_t p4, uint32_t* p5, uint32_t* p6, RuntimeObject * p7, DecoderFallbackBuffer_t1939825529 ** p8, ByteU5BU5D_t1014014593** p9, int8_t p10, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), (RuntimeObject *)args[2], *((int32_t*)args[3]), (uint32_t*)args[4], (uint32_t*)args[5], (RuntimeObject *)args[6], (DecoderFallbackBuffer_t1939825529 **)args[7], (ByteU5BU5D_t1014014593**)args[8], *((int8_t*)args[9]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Void_t1078098495_RuntimeObject_RuntimeObject_Int32_t2571135199_SByte_t2530277047 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RuntimeObject * p1, RuntimeObject * p2, int32_t p3, int8_t p4, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (RuntimeObject *)args[0], (RuntimeObject *)args[1], *((int32_t*)args[2]), *((int8_t*)args[3]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_SByte_t2530277047_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int32_t p2, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((int8_t*)args[0]), *((int32_t*)args[1]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_IntPtr_t_SByte_t2530277047_RuntimeObject_BooleanU26_t495120821 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef intptr_t (*Func)(void* obj, int8_t p1, RuntimeObject * p2, bool* p3, const RuntimeMethod* method);
	intptr_t ret = ((Func)methodPointer)(obj, *((int8_t*)args[0]), (RuntimeObject *)args[1], (bool*)args[2], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_IntPtr_t (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, intptr_t p1, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, *((intptr_t*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_IntPtr_t_SByte_t2530277047_SByte_t2530277047_RuntimeObject_BooleanU26_t495120821 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef intptr_t (*Func)(void* obj, int8_t p1, int8_t p2, RuntimeObject * p3, bool* p4, const RuntimeMethod* method);
	intptr_t ret = ((Func)methodPointer)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), (RuntimeObject *)args[2], (bool*)args[3], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_TimeSpan_t2986675223_TimeSpan_t2986675223 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, TimeSpan_t2986675223  p1, TimeSpan_t2986675223  p2, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, *((TimeSpan_t2986675223 *)args[0]), *((TimeSpan_t2986675223 *)args[1]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_Int64_t384086013_Int64_t384086013_SByte_t2530277047 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int64_t p1, int64_t p2, int8_t p3, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, *((int64_t*)args[0]), *((int64_t*)args[1]), *((int8_t*)args[2]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_IntPtr_t_Int32_t2571135199_SByte_t2530277047 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, intptr_t p1, int32_t p2, int8_t p3, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, *((intptr_t*)args[0]), *((int32_t*)args[1]), *((int8_t*)args[2]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int64_t384086013_Double_t1454265465 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, double p1, const RuntimeMethod* method);
	int64_t ret = ((Func)methodPointer)(obj, *((double*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_RuntimeObject_Double_t1454265465 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, double p1, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, *((double*)args[0]), methodMetadata);
	return ret;
}

void* RuntimeInvoker_Int64_t384086013_RuntimeObject_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, RuntimeObject * p1, int32_t p2, const RuntimeMethod* method);
	int64_t ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_RuntimeObject_IntPtr_t_Int32_t2571135199_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, intptr_t p1, int32_t p2, int32_t p3, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, *((intptr_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), methodMetadata);
	return ret;
}

void* RuntimeInvoker_Byte_t880488320_SByte_t2530277047 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, int8_t p1, const RuntimeMethod* method);
	uint8_t ret = ((Func)methodPointer)(obj, *((int8_t*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Byte_t880488320_Int16_t769030278 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, int16_t p1, const RuntimeMethod* method);
	uint8_t ret = ((Func)methodPointer)(obj, *((int16_t*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Byte_t880488320_Double_t1454265465 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, double p1, const RuntimeMethod* method);
	uint8_t ret = ((Func)methodPointer)(obj, *((double*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Byte_t880488320_Single_t496865882 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, float p1, const RuntimeMethod* method);
	uint8_t ret = ((Func)methodPointer)(obj, *((float*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Byte_t880488320_Int64_t384086013 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, int64_t p1, const RuntimeMethod* method);
	uint8_t ret = ((Func)methodPointer)(obj, *((int64_t*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Char_t3572527572_SByte_t2530277047 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef Il2CppChar (*Func)(void* obj, int8_t p1, const RuntimeMethod* method);
	Il2CppChar ret = ((Func)methodPointer)(obj, *((int8_t*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Char_t3572527572_Int64_t384086013 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef Il2CppChar (*Func)(void* obj, int64_t p1, const RuntimeMethod* method);
	Il2CppChar ret = ((Func)methodPointer)(obj, *((int64_t*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Char_t3572527572_Single_t496865882 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef Il2CppChar (*Func)(void* obj, float p1, const RuntimeMethod* method);
	Il2CppChar ret = ((Func)methodPointer)(obj, *((float*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Char_t3572527572_RuntimeObject_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef Il2CppChar (*Func)(void* obj, RuntimeObject * p1, RuntimeObject * p2, const RuntimeMethod* method);
	Il2CppChar ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], (RuntimeObject *)args[1], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_DateTime_t507798353_RuntimeObject_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef DateTime_t507798353  (*Func)(void* obj, RuntimeObject * p1, RuntimeObject * p2, const RuntimeMethod* method);
	DateTime_t507798353  ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], (RuntimeObject *)args[1], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_DateTime_t507798353_Int16_t769030278 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef DateTime_t507798353  (*Func)(void* obj, int16_t p1, const RuntimeMethod* method);
	DateTime_t507798353  ret = ((Func)methodPointer)(obj, *((int16_t*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_DateTime_t507798353_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef DateTime_t507798353  (*Func)(void* obj, int32_t p1, const RuntimeMethod* method);
	DateTime_t507798353  ret = ((Func)methodPointer)(obj, *((int32_t*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_DateTime_t507798353_Int64_t384086013 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef DateTime_t507798353  (*Func)(void* obj, int64_t p1, const RuntimeMethod* method);
	DateTime_t507798353  ret = ((Func)methodPointer)(obj, *((int64_t*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_DateTime_t507798353_Single_t496865882 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef DateTime_t507798353  (*Func)(void* obj, float p1, const RuntimeMethod* method);
	DateTime_t507798353  ret = ((Func)methodPointer)(obj, *((float*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_DateTime_t507798353_SByte_t2530277047 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef DateTime_t507798353  (*Func)(void* obj, int8_t p1, const RuntimeMethod* method);
	DateTime_t507798353  ret = ((Func)methodPointer)(obj, *((int8_t*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Double_t1454265465_SByte_t2530277047 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef double (*Func)(void* obj, int8_t p1, const RuntimeMethod* method);
	double ret = ((Func)methodPointer)(obj, *((int8_t*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Double_t1454265465_Double_t1454265465 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef double (*Func)(void* obj, double p1, const RuntimeMethod* method);
	double ret = ((Func)methodPointer)(obj, *((double*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Double_t1454265465_Single_t496865882 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef double (*Func)(void* obj, float p1, const RuntimeMethod* method);
	double ret = ((Func)methodPointer)(obj, *((float*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Double_t1454265465_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef double (*Func)(void* obj, int32_t p1, const RuntimeMethod* method);
	double ret = ((Func)methodPointer)(obj, *((int32_t*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Double_t1454265465_Int64_t384086013 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef double (*Func)(void* obj, int64_t p1, const RuntimeMethod* method);
	double ret = ((Func)methodPointer)(obj, *((int64_t*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Double_t1454265465_Int16_t769030278 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef double (*Func)(void* obj, int16_t p1, const RuntimeMethod* method);
	double ret = ((Func)methodPointer)(obj, *((int16_t*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int16_t769030278_SByte_t2530277047 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, int8_t p1, const RuntimeMethod* method);
	int16_t ret = ((Func)methodPointer)(obj, *((int8_t*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int16_t769030278_Int16_t769030278 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, int16_t p1, const RuntimeMethod* method);
	int16_t ret = ((Func)methodPointer)(obj, *((int16_t*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int16_t769030278_Double_t1454265465 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, double p1, const RuntimeMethod* method);
	int16_t ret = ((Func)methodPointer)(obj, *((double*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int16_t769030278_Single_t496865882 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, float p1, const RuntimeMethod* method);
	int16_t ret = ((Func)methodPointer)(obj, *((float*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int16_t769030278_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, int32_t p1, const RuntimeMethod* method);
	int16_t ret = ((Func)methodPointer)(obj, *((int32_t*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int16_t769030278_Int64_t384086013 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, int64_t p1, const RuntimeMethod* method);
	int16_t ret = ((Func)methodPointer)(obj, *((int64_t*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int64_t384086013_SByte_t2530277047 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, int8_t p1, const RuntimeMethod* method);
	int64_t ret = ((Func)methodPointer)(obj, *((int8_t*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int64_t384086013_Int16_t769030278 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, int16_t p1, const RuntimeMethod* method);
	int64_t ret = ((Func)methodPointer)(obj, *((int16_t*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int64_t384086013_Single_t496865882 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, float p1, const RuntimeMethod* method);
	int64_t ret = ((Func)methodPointer)(obj, *((float*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int64_t384086013_Int64_t384086013 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, int64_t p1, const RuntimeMethod* method);
	int64_t ret = ((Func)methodPointer)(obj, *((int64_t*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_SByte_t2530277047_SByte_t2530277047 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, int8_t p1, const RuntimeMethod* method);
	int8_t ret = ((Func)methodPointer)(obj, *((int8_t*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_SByte_t2530277047_Int16_t769030278 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, int16_t p1, const RuntimeMethod* method);
	int8_t ret = ((Func)methodPointer)(obj, *((int16_t*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_SByte_t2530277047_Double_t1454265465 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, double p1, const RuntimeMethod* method);
	int8_t ret = ((Func)methodPointer)(obj, *((double*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_SByte_t2530277047_Single_t496865882 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, float p1, const RuntimeMethod* method);
	int8_t ret = ((Func)methodPointer)(obj, *((float*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_SByte_t2530277047_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, int32_t p1, const RuntimeMethod* method);
	int8_t ret = ((Func)methodPointer)(obj, *((int32_t*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_SByte_t2530277047_Int64_t384086013 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, int64_t p1, const RuntimeMethod* method);
	int8_t ret = ((Func)methodPointer)(obj, *((int64_t*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Single_t496865882_SByte_t2530277047 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef float (*Func)(void* obj, int8_t p1, const RuntimeMethod* method);
	float ret = ((Func)methodPointer)(obj, *((int8_t*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Single_t496865882_Double_t1454265465 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef float (*Func)(void* obj, double p1, const RuntimeMethod* method);
	float ret = ((Func)methodPointer)(obj, *((double*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Single_t496865882_Single_t496865882 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef float (*Func)(void* obj, float p1, const RuntimeMethod* method);
	float ret = ((Func)methodPointer)(obj, *((float*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Single_t496865882_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef float (*Func)(void* obj, int32_t p1, const RuntimeMethod* method);
	float ret = ((Func)methodPointer)(obj, *((int32_t*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Single_t496865882_Int64_t384086013 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef float (*Func)(void* obj, int64_t p1, const RuntimeMethod* method);
	float ret = ((Func)methodPointer)(obj, *((int64_t*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Single_t496865882_Int16_t769030278 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef float (*Func)(void* obj, int16_t p1, const RuntimeMethod* method);
	float ret = ((Func)methodPointer)(obj, *((int16_t*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_UInt16_t11709673_SByte_t2530277047 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int8_t p1, const RuntimeMethod* method);
	uint16_t ret = ((Func)methodPointer)(obj, *((int8_t*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_UInt16_t11709673_Int16_t769030278 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int16_t p1, const RuntimeMethod* method);
	uint16_t ret = ((Func)methodPointer)(obj, *((int16_t*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_UInt16_t11709673_Double_t1454265465 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, double p1, const RuntimeMethod* method);
	uint16_t ret = ((Func)methodPointer)(obj, *((double*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_UInt16_t11709673_Single_t496865882 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, float p1, const RuntimeMethod* method);
	uint16_t ret = ((Func)methodPointer)(obj, *((float*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_UInt16_t11709673_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int32_t p1, const RuntimeMethod* method);
	uint16_t ret = ((Func)methodPointer)(obj, *((int32_t*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_UInt16_t11709673_Int64_t384086013 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int64_t p1, const RuntimeMethod* method);
	uint16_t ret = ((Func)methodPointer)(obj, *((int64_t*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_UInt32_t2739056616_SByte_t2530277047 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, int8_t p1, const RuntimeMethod* method);
	uint32_t ret = ((Func)methodPointer)(obj, *((int8_t*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_UInt32_t2739056616_Int16_t769030278 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, int16_t p1, const RuntimeMethod* method);
	uint32_t ret = ((Func)methodPointer)(obj, *((int16_t*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_UInt32_t2739056616_Double_t1454265465 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, double p1, const RuntimeMethod* method);
	uint32_t ret = ((Func)methodPointer)(obj, *((double*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_UInt32_t2739056616_Single_t496865882 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, float p1, const RuntimeMethod* method);
	uint32_t ret = ((Func)methodPointer)(obj, *((float*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_UInt32_t2739056616_Int64_t384086013 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, int64_t p1, const RuntimeMethod* method);
	uint32_t ret = ((Func)methodPointer)(obj, *((int64_t*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_UInt64_t2265270229_SByte_t2530277047 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, int8_t p1, const RuntimeMethod* method);
	uint64_t ret = ((Func)methodPointer)(obj, *((int8_t*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_UInt64_t2265270229_Int16_t769030278 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, int16_t p1, const RuntimeMethod* method);
	uint64_t ret = ((Func)methodPointer)(obj, *((int16_t*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_UInt64_t2265270229_Double_t1454265465 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, double p1, const RuntimeMethod* method);
	uint64_t ret = ((Func)methodPointer)(obj, *((double*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_UInt64_t2265270229_Single_t496865882 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, float p1, const RuntimeMethod* method);
	uint64_t ret = ((Func)methodPointer)(obj, *((float*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_UInt64_t2265270229_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, int32_t p1, const RuntimeMethod* method);
	uint64_t ret = ((Func)methodPointer)(obj, *((int32_t*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Void_t1078098495_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, int32_t p6, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), *((int32_t*)args[5]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, int32_t p6, int32_t p7, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), *((int32_t*)args[5]), *((int32_t*)args[6]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_SByte_t2530277047_TimeSpan_t2986675223 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, TimeSpan_t2986675223  p2, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((int8_t*)args[0]), *((TimeSpan_t2986675223 *)args[1]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_Int64_t384086013_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, int32_t p2, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((int64_t*)args[0]), *((int32_t*)args[1]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_DayOfWeek_t61775933 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_DateTimeKind_t3511769276 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_DateTime_t507798353_TimeSpan_t2986675223 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef DateTime_t507798353  (*Func)(void* obj, TimeSpan_t2986675223  p1, const RuntimeMethod* method);
	DateTime_t507798353  ret = ((Func)methodPointer)(obj, *((TimeSpan_t2986675223 *)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_DateTime_t507798353_Double_t1454265465 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef DateTime_t507798353  (*Func)(void* obj, double p1, const RuntimeMethod* method);
	DateTime_t507798353  ret = ((Func)methodPointer)(obj, *((double*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int32_t2571135199_DateTime_t507798353_DateTime_t507798353 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, DateTime_t507798353  p1, DateTime_t507798353  p2, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, *((DateTime_t507798353 *)args[0]), *((DateTime_t507798353 *)args[1]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_DateTime_t507798353 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, DateTime_t507798353  p1, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, *((DateTime_t507798353 *)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_DateTime_t507798353_DateTime_t507798353_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef DateTime_t507798353  (*Func)(void* obj, DateTime_t507798353  p1, int32_t p2, const RuntimeMethod* method);
	DateTime_t507798353  ret = ((Func)methodPointer)(obj, *((DateTime_t507798353 *)args[0]), *((int32_t*)args[1]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_DateTime_t507798353_RuntimeObject_RuntimeObject_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef DateTime_t507798353  (*Func)(void* obj, RuntimeObject * p1, RuntimeObject * p2, int32_t p3, const RuntimeMethod* method);
	DateTime_t507798353  ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], (RuntimeObject *)args[1], *((int32_t*)args[2]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_RuntimeObject_RuntimeObject_Int32_t2571135199_DateTimeU26_t2764841687_DateTimeOffsetU26_t118455887_SByte_t2530277047_ExceptionU26_t1670763973 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, RuntimeObject * p1, RuntimeObject * p2, int32_t p3, DateTime_t507798353 * p4, DateTimeOffset_t2220763929 * p5, int8_t p6, Exception_t3361176243 ** p7, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], (RuntimeObject *)args[1], *((int32_t*)args[2]), (DateTime_t507798353 *)args[3], (DateTimeOffset_t2220763929 *)args[4], *((int8_t*)args[5]), (Exception_t3361176243 **)args[6], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_RuntimeObject_RuntimeObject_SByte_t2530277047_ExceptionU26_t1670763973 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, RuntimeObject * p1, int8_t p2, Exception_t3361176243 ** p3, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int8_t*)args[1]), (Exception_t3361176243 **)args[2], methodMetadata);
	return ret;
}

void* RuntimeInvoker_Int32_t2571135199_RuntimeObject_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199_SByte_t2530277047_SByte_t2530277047_Int32U26_t2596978809 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RuntimeObject * p1, int32_t p2, int32_t p3, int32_t p4, int8_t p5, int8_t p6, int32_t* p7, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), (int32_t*)args[6], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int32_t2571135199_RuntimeObject_Int32_t2571135199_RuntimeObject_RuntimeObject_SByte_t2530277047_Int32U26_t2596978809 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RuntimeObject * p1, int32_t p2, RuntimeObject * p3, RuntimeObject * p4, int8_t p5, int32_t* p6, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), (RuntimeObject *)args[2], (RuntimeObject *)args[3], *((int8_t*)args[4]), (int32_t*)args[5], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_RuntimeObject_Int32_t2571135199_Int32_t2571135199_RuntimeObject_Int32U26_t2596978809 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, RuntimeObject * p1, int32_t p2, int32_t p3, RuntimeObject * p4, int32_t* p5, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (RuntimeObject *)args[3], (int32_t*)args[4], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_RuntimeObject_Int32_t2571135199_Int32_t2571135199_RuntimeObject_SByte_t2530277047_Int32U26_t2596978809_Int32U26_t2596978809 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, RuntimeObject * p1, int32_t p2, int32_t p3, RuntimeObject * p4, int8_t p5, int32_t* p6, int32_t* p7, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (RuntimeObject *)args[3], *((int8_t*)args[4]), (int32_t*)args[5], (int32_t*)args[6], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_RuntimeObject_Int32_t2571135199_RuntimeObject_SByte_t2530277047_Int32U26_t2596978809 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, RuntimeObject * p1, int32_t p2, RuntimeObject * p3, int8_t p4, int32_t* p5, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), (RuntimeObject *)args[2], *((int8_t*)args[3]), (int32_t*)args[4], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_RuntimeObject_RuntimeObject_RuntimeObject_SByte_t2530277047_DateTimeU26_t2764841687_DateTimeOffsetU26_t118455887_RuntimeObject_Int32_t2571135199_SByte_t2530277047_BooleanU26_t495120821_BooleanU26_t495120821 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, RuntimeObject * p1, RuntimeObject * p2, RuntimeObject * p3, int8_t p4, DateTime_t507798353 * p5, DateTimeOffset_t2220763929 * p6, RuntimeObject * p7, int32_t p8, int8_t p9, bool* p10, bool* p11, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], (RuntimeObject *)args[1], (RuntimeObject *)args[2], *((int8_t*)args[3]), (DateTime_t507798353 *)args[4], (DateTimeOffset_t2220763929 *)args[5], (RuntimeObject *)args[6], *((int32_t*)args[7]), *((int8_t*)args[8]), (bool*)args[9], (bool*)args[10], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_DateTime_t507798353_RuntimeObject_RuntimeObject_RuntimeObject_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef DateTime_t507798353  (*Func)(void* obj, RuntimeObject * p1, RuntimeObject * p2, RuntimeObject * p3, int32_t p4, const RuntimeMethod* method);
	DateTime_t507798353  ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], (RuntimeObject *)args[1], (RuntimeObject *)args[2], *((int32_t*)args[3]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_RuntimeObject_RuntimeObject_RuntimeObject_Int32_t2571135199_DateTimeU26_t2764841687_SByte_t2530277047_BooleanU26_t495120821_SByte_t2530277047_ExceptionU26_t1670763973 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, RuntimeObject * p1, RuntimeObject * p2, RuntimeObject * p3, int32_t p4, DateTime_t507798353 * p5, int8_t p6, bool* p7, int8_t p8, Exception_t3361176243 ** p9, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], (RuntimeObject *)args[1], (RuntimeObject *)args[2], *((int32_t*)args[3]), (DateTime_t507798353 *)args[4], *((int8_t*)args[5]), (bool*)args[6], *((int8_t*)args[7]), (Exception_t3361176243 **)args[8], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_DateTime_t507798353_DateTime_t507798353_TimeSpan_t2986675223 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef DateTime_t507798353  (*Func)(void* obj, DateTime_t507798353  p1, TimeSpan_t2986675223  p2, const RuntimeMethod* method);
	DateTime_t507798353  ret = ((Func)methodPointer)(obj, *((DateTime_t507798353 *)args[0]), *((TimeSpan_t2986675223 *)args[1]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_DateTime_t507798353_DateTime_t507798353 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, DateTime_t507798353  p1, DateTime_t507798353  p2, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, *((DateTime_t507798353 *)args[0]), *((DateTime_t507798353 *)args[1]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Void_t1078098495_DateTime_t507798353 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, DateTime_t507798353  p1, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((DateTime_t507798353 *)args[0]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_DateTime_t507798353_TimeSpan_t2986675223 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, DateTime_t507798353  p1, TimeSpan_t2986675223  p2, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((DateTime_t507798353 *)args[0]), *((TimeSpan_t2986675223 *)args[1]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_Int64_t384086013_TimeSpan_t2986675223 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, TimeSpan_t2986675223  p2, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((int64_t*)args[0]), *((TimeSpan_t2986675223 *)args[1]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Int32_t2571135199_DateTimeOffset_t2220763929 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, DateTimeOffset_t2220763929  p1, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, *((DateTimeOffset_t2220763929 *)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_DateTimeOffset_t2220763929 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, DateTimeOffset_t2220763929  p1, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, *((DateTimeOffset_t2220763929 *)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int32_t2571135199_RuntimeObject_Int32_t2571135199_Int16_t769030278 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RuntimeObject * p1, int32_t p2, int16_t p3, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), *((int16_t*)args[2]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_RuntimeObject_Int16_t769030278_RuntimeObject_BooleanU26_t495120821_BooleanU26_t495120821 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, int16_t p1, RuntimeObject * p2, bool* p3, bool* p4, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, *((int16_t*)args[0]), (RuntimeObject *)args[1], (bool*)args[2], (bool*)args[3], methodMetadata);
	return ret;
}

void* RuntimeInvoker_RuntimeObject_Int16_t769030278_RuntimeObject_BooleanU26_t495120821_BooleanU26_t495120821_SByte_t2530277047 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, int16_t p1, RuntimeObject * p2, bool* p3, bool* p4, int8_t p5, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, *((int16_t*)args[0]), (RuntimeObject *)args[1], (bool*)args[2], (bool*)args[3], *((int8_t*)args[4]), methodMetadata);
	return ret;
}

void* RuntimeInvoker_RuntimeObject_DateTime_t507798353_RuntimeObject_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, DateTime_t507798353  p1, RuntimeObject * p2, RuntimeObject * p3, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, *((DateTime_t507798353 *)args[0]), (RuntimeObject *)args[1], (RuntimeObject *)args[2], methodMetadata);
	return ret;
}

void* RuntimeInvoker_RuntimeObject_DateTime_t507798353_Nullable_1_t950977049_RuntimeObject_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, DateTime_t507798353  p1, Nullable_1_t950977049  p2, RuntimeObject * p3, RuntimeObject * p4, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, *((DateTime_t507798353 *)args[0]), *((Nullable_1_t950977049 *)args[1]), (RuntimeObject *)args[2], (RuntimeObject *)args[3], methodMetadata);
	return ret;
}

void* RuntimeInvoker_Void_t1078098495_MonoEnumInfo_t776332500 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, MonoEnumInfo_t776332500  p1, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((MonoEnumInfo_t776332500 *)args[0]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_RuntimeObject_MonoEnumInfoU26_t1245821516 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RuntimeObject * p1, MonoEnumInfo_t776332500 * p2, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (RuntimeObject *)args[0], (MonoEnumInfo_t776332500 *)args[1], methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Int32_t2571135199_Int16_t769030278_Int16_t769030278 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int16_t p1, int16_t p2, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, *((int16_t*)args[0]), *((int16_t*)args[1]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int32_t2571135199_Int64_t384086013_Int64_t384086013 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int64_t p1, int64_t p2, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, *((int64_t*)args[0]), *((int64_t*)args[1]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_PlatformID_t2641264986 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Void_t1078098495_Int32_t2571135199_Int16_t769030278_Int16_t769030278_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int16_t p2, int16_t p3, RuntimeObject * p4, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((int32_t*)args[0]), *((int16_t*)args[1]), *((int16_t*)args[2]), (RuntimeObject *)args[3], methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_Int32_t2571135199_Int16_t769030278_Int16_t769030278_SByte_t2530277047_SByte_t2530277047_SByte_t2530277047_SByte_t2530277047_SByte_t2530277047_SByte_t2530277047_SByte_t2530277047_SByte_t2530277047 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int16_t p2, int16_t p3, int8_t p4, int8_t p5, int8_t p6, int8_t p7, int8_t p8, int8_t p9, int8_t p10, int8_t p11, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((int32_t*)args[0]), *((int16_t*)args[1]), *((int16_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), *((int8_t*)args[6]), *((int8_t*)args[7]), *((int8_t*)args[8]), *((int8_t*)args[9]), *((int8_t*)args[10]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Int32_t2571135199_Guid_t (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Guid_t  p1, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, *((Guid_t *)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_Guid_t (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Guid_t  p1, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, *((Guid_t *)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Guid_t (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef Guid_t  (*Func)(void* obj, const RuntimeMethod* method);
	Guid_t  ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Void_t1078098495_RuntimeObject_Int16_t769030278 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RuntimeObject * p1, int16_t p2, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int16_t*)args[1]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_RuntimeObject_SByte_t2530277047_SByte_t2530277047_SByte_t2530277047 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, int8_t p1, int8_t p2, int8_t p3, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), methodMetadata);
	return ret;
}

void* RuntimeInvoker_Boolean_t2059672899_Guid_t_Guid_t (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Guid_t  p1, Guid_t  p2, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, *((Guid_t *)args[0]), *((Guid_t *)args[1]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_UInt64_t2265270229_Int32_t2571135199_SByte_t2530277047 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, int32_t p1, int8_t p2, const RuntimeMethod* method);
	uint64_t ret = ((Func)methodPointer)(obj, *((int32_t*)args[0]), *((int8_t*)args[1]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Double_t1454265465_Double_t1454265465_Double_t1454265465 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef double (*Func)(void* obj, double p1, double p2, const RuntimeMethod* method);
	double ret = ((Func)methodPointer)(obj, *((double*)args[0]), *((double*)args[1]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_TypeAttributes_t3122562390_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RuntimeObject * p1, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_RuntimeObject_SByte_t2530277047_SByte_t2530277047 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, int8_t p1, int8_t p2, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), methodMetadata);
	return ret;
}

void* RuntimeInvoker_Void_t1078098495_UInt64U2AU26_t2732919049_Int32U2AU26_t3017422331_CharU2AU26_t2131013796_CharU2AU26_t2131013796_Int64U2AU26_t3036000465_Int32U2AU26_t3017422331 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint64_t** p1, int32_t** p2, Il2CppChar** p3, Il2CppChar** p4, int64_t** p5, int32_t** p6, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (uint64_t**)args[0], (int32_t**)args[1], (Il2CppChar**)args[2], (Il2CppChar**)args[3], (int64_t**)args[4], (int32_t**)args[5], methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_Int32_t2571135199_Int64_t384086013 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int64_t p2, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((int32_t*)args[0]), *((int64_t*)args[1]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_RuntimeObject_Double_t1454265465_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RuntimeObject * p1, double p2, int32_t p3, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((double*)args[1]), *((int32_t*)args[2]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_RuntimeObject_Decimal_t2656901032 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RuntimeObject * p1, Decimal_t2656901032  p2, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((Decimal_t2656901032 *)args[1]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_RuntimeObject_RuntimeObject_SByte_t2530277047_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, RuntimeObject * p1, int8_t p2, RuntimeObject * p3, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int8_t*)args[1]), (RuntimeObject *)args[2], methodMetadata);
	return ret;
}

void* RuntimeInvoker_RuntimeObject_RuntimeObject_Int16_t769030278_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, RuntimeObject * p1, int16_t p2, RuntimeObject * p3, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int16_t*)args[1]), (RuntimeObject *)args[2], methodMetadata);
	return ret;
}

void* RuntimeInvoker_RuntimeObject_RuntimeObject_Int32_t2571135199_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, RuntimeObject * p1, int32_t p2, RuntimeObject * p3, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), (RuntimeObject *)args[2], methodMetadata);
	return ret;
}

void* RuntimeInvoker_RuntimeObject_RuntimeObject_Int64_t384086013_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, RuntimeObject * p1, int64_t p2, RuntimeObject * p3, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int64_t*)args[1]), (RuntimeObject *)args[2], methodMetadata);
	return ret;
}

void* RuntimeInvoker_RuntimeObject_RuntimeObject_Single_t496865882_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, RuntimeObject * p1, float p2, RuntimeObject * p3, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((float*)args[1]), (RuntimeObject *)args[2], methodMetadata);
	return ret;
}

void* RuntimeInvoker_RuntimeObject_RuntimeObject_Double_t1454265465_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, RuntimeObject * p1, double p2, RuntimeObject * p3, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((double*)args[1]), (RuntimeObject *)args[2], methodMetadata);
	return ret;
}

void* RuntimeInvoker_RuntimeObject_RuntimeObject_Decimal_t2656901032_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, RuntimeObject * p1, Decimal_t2656901032  p2, RuntimeObject * p3, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((Decimal_t2656901032 *)args[1]), (RuntimeObject *)args[2], methodMetadata);
	return ret;
}

void* RuntimeInvoker_RuntimeObject_Single_t496865882_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, float p1, RuntimeObject * p2, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, *((float*)args[0]), (RuntimeObject *)args[1], methodMetadata);
	return ret;
}

void* RuntimeInvoker_RuntimeObject_Double_t1454265465_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, double p1, RuntimeObject * p2, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, *((double*)args[0]), (RuntimeObject *)args[1], methodMetadata);
	return ret;
}

void* RuntimeInvoker_Void_t1078098495_Int32_t2571135199_Int32_t2571135199_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, RuntimeObject * p3, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (RuntimeObject *)args[2], methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_RuntimeObject_BooleanU26_t495120821_SByte_t2530277047_Int32U26_t2596978809_Int32U26_t2596978809 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RuntimeObject * p1, bool* p2, int8_t p3, int32_t* p4, int32_t* p5, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (RuntimeObject *)args[0], (bool*)args[1], *((int8_t*)args[2]), (int32_t*)args[3], (int32_t*)args[4], methodMetadata);
	return NULL;
}

void* RuntimeInvoker_RuntimeObject_RuntimeObject_Int32_t2571135199_Int32_t2571135199_RuntimeObject_SByte_t2530277047_RuntimeObject_RuntimeObject_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, RuntimeObject * p1, int32_t p2, int32_t p3, RuntimeObject * p4, int8_t p5, RuntimeObject * p6, RuntimeObject * p7, RuntimeObject * p8, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (RuntimeObject *)args[3], *((int8_t*)args[4]), (RuntimeObject *)args[5], (RuntimeObject *)args[6], (RuntimeObject *)args[7], methodMetadata);
	return ret;
}

void* RuntimeInvoker_Void_t1078098495_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Int64_t384086013_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, const RuntimeMethod* method);
	int64_t ret = ((Func)methodPointer)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_TimeSpan_t2986675223_TimeSpan_t2986675223 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef TimeSpan_t2986675223  (*Func)(void* obj, TimeSpan_t2986675223  p1, const RuntimeMethod* method);
	TimeSpan_t2986675223  ret = ((Func)methodPointer)(obj, *((TimeSpan_t2986675223 *)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int32_t2571135199_TimeSpan_t2986675223_TimeSpan_t2986675223 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, TimeSpan_t2986675223  p1, TimeSpan_t2986675223  p2, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, *((TimeSpan_t2986675223 *)args[0]), *((TimeSpan_t2986675223 *)args[1]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int32_t2571135199_TimeSpan_t2986675223 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, TimeSpan_t2986675223  p1, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, *((TimeSpan_t2986675223 *)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_TimeSpan_t2986675223 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, TimeSpan_t2986675223  p1, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, *((TimeSpan_t2986675223 *)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_TimeSpan_t2986675223_Double_t1454265465 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef TimeSpan_t2986675223  (*Func)(void* obj, double p1, const RuntimeMethod* method);
	TimeSpan_t2986675223  ret = ((Func)methodPointer)(obj, *((double*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_TimeSpan_t2986675223_Double_t1454265465_Int64_t384086013 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef TimeSpan_t2986675223  (*Func)(void* obj, double p1, int64_t p2, const RuntimeMethod* method);
	TimeSpan_t2986675223  ret = ((Func)methodPointer)(obj, *((double*)args[0]), *((int64_t*)args[1]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_TimeSpan_t2986675223_TimeSpan_t2986675223_TimeSpan_t2986675223 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef TimeSpan_t2986675223  (*Func)(void* obj, TimeSpan_t2986675223  p1, TimeSpan_t2986675223  p2, const RuntimeMethod* method);
	TimeSpan_t2986675223  ret = ((Func)methodPointer)(obj, *((TimeSpan_t2986675223 *)args[0]), *((TimeSpan_t2986675223 *)args[1]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_TimeSpan_t2986675223_DateTime_t507798353 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef TimeSpan_t2986675223  (*Func)(void* obj, DateTime_t507798353  p1, const RuntimeMethod* method);
	TimeSpan_t2986675223  ret = ((Func)methodPointer)(obj, *((DateTime_t507798353 *)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_DateTime_t507798353_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, DateTime_t507798353  p1, RuntimeObject * p2, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, *((DateTime_t507798353 *)args[0]), (RuntimeObject *)args[1], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_DateTime_t507798353_DateTime_t507798353 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef DateTime_t507798353  (*Func)(void* obj, DateTime_t507798353  p1, const RuntimeMethod* method);
	DateTime_t507798353  ret = ((Func)methodPointer)(obj, *((DateTime_t507798353 *)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_TimeSpan_t2986675223_DateTime_t507798353_TimeSpan_t2986675223 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef TimeSpan_t2986675223  (*Func)(void* obj, DateTime_t507798353  p1, TimeSpan_t2986675223  p2, const RuntimeMethod* method);
	TimeSpan_t2986675223  ret = ((Func)methodPointer)(obj, *((DateTime_t507798353 *)args[0]), *((TimeSpan_t2986675223 *)args[1]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_Int32_t2571135199_Int64U5BU5DU26_t3921029648_StringU5BU5DU26_t4251177155 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, Int64U5BU5D_t3591546736** p2, StringU5BU5D_t522578821** p3, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, *((int32_t*)args[0]), (Int64U5BU5D_t3591546736**)args[1], (StringU5BU5D_t522578821**)args[2], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Void_t1078098495_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int32_t p4, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_RuntimeObject_RuntimeObject_Int32_t2571135199_RuntimeObject_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, RuntimeObject * p1, int32_t p2, RuntimeObject * p3, RuntimeObject * p4, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), (RuntimeObject *)args[2], (RuntimeObject *)args[3], methodMetadata);
	return ret;
}

void* RuntimeInvoker_RuntimeObject_RuntimeObject_DictionaryNodeU26_t2493955228 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, RuntimeObject * p1, DictionaryNode_t4253121028 ** p2, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], (DictionaryNode_t4253121028 **)args[1], methodMetadata);
	return ret;
}

void* RuntimeInvoker_Boolean_t2059672899_RuntimeObject_RuntimeObject_RuntimeObject_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, RuntimeObject * p1, RuntimeObject * p2, RuntimeObject * p3, int32_t p4, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], (RuntimeObject *)args[1], (RuntimeObject *)args[2], *((int32_t*)args[3]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_RuntimeObject_IPAddressU26_t2403452733 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, RuntimeObject * p1, IPAddress_t1846197371 ** p2, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], (IPAddress_t1846197371 **)args[1], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_AddressFamily_t4183665415 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int32_t p4, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_RuntimeObject_IPv6AddressU26_t126668491 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, RuntimeObject * p1, IPv6Address_t2439851581 ** p2, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], (IPv6Address_t2439851581 **)args[1], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_SecurityProtocolType_t26325249 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Void_t1078098495_SByte_t2530277047_SByte_t2530277047_Int32_t2571135199_SByte_t2530277047 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int8_t p2, int32_t p3, int8_t p4, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((int32_t*)args[2]), *((int8_t*)args[3]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_AsnDecodeStatus_t4190820181_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RuntimeObject * p1, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_RuntimeObject_Int32_t2571135199_RuntimeObject_SByte_t2530277047 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, int32_t p1, RuntimeObject * p2, int8_t p3, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, *((int32_t*)args[0]), (RuntimeObject *)args[1], *((int8_t*)args[2]), methodMetadata);
	return ret;
}

void* RuntimeInvoker_X509ChainStatusFlags_t2184184987_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RuntimeObject * p1, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_X509ChainStatusFlags_t2184184987_RuntimeObject_Int32_t2571135199_SByte_t2530277047 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RuntimeObject * p1, int32_t p2, int8_t p3, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_X509ChainStatusFlags_t2184184987_RuntimeObject_RuntimeObject_SByte_t2530277047 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RuntimeObject * p1, RuntimeObject * p2, int8_t p3, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], (RuntimeObject *)args[1], *((int8_t*)args[2]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_X509ChainStatusFlags_t2184184987 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Void_t1078098495_RuntimeObject_Int32U26_t2596978809_Int32_t2571135199_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RuntimeObject * p1, int32_t* p2, int32_t p3, int32_t p4, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (RuntimeObject *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_X509RevocationFlag_t3762606324 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_X509RevocationMode_t2217493564 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_X509VerificationFlags_t1372417108 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_X509KeyUsageFlags_t1189859534 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_X509KeyUsageFlags_t1189859534_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, *((int32_t*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Void_t1078098495_RuntimeObject_Int32_t2571135199_SByte_t2530277047 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RuntimeObject * p1, int32_t p2, int8_t p3, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Byte_t880488320_Int16_t769030278_Int16_t769030278 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, int16_t p1, int16_t p2, const RuntimeMethod* method);
	uint8_t ret = ((Func)methodPointer)(obj, *((int16_t*)args[0]), *((int16_t*)args[1]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Void_t1078098495_RuntimeObject_RuntimeObject_RuntimeObject_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RuntimeObject * p1, RuntimeObject * p2, RuntimeObject * p3, int32_t p4, int32_t p5, int32_t p6, int32_t p7, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (RuntimeObject *)args[0], (RuntimeObject *)args[1], (RuntimeObject *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), *((int32_t*)args[5]), *((int32_t*)args[6]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_RuntimeObject_RuntimeObject_RuntimeObject_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RuntimeObject * p1, RuntimeObject * p2, RuntimeObject * p3, int32_t p4, int32_t p5, int32_t p6, int32_t p7, int32_t p8, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (RuntimeObject *)args[0], (RuntimeObject *)args[1], (RuntimeObject *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), *((int32_t*)args[5]), *((int32_t*)args[6]), *((int32_t*)args[7]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Category_t2116501226_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, RuntimeObject * p1, const RuntimeMethod* method);
	uint16_t ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_UInt16_t11709673_Int16_t769030278 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, uint16_t p1, int16_t p2, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, *((uint16_t*)args[0]), *((int16_t*)args[1]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_Int32_t2571135199_Int16_t769030278 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int16_t p2, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, *((int32_t*)args[0]), *((int16_t*)args[1]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Void_t1078098495_Int16_t769030278_SByte_t2530277047_SByte_t2530277047_SByte_t2530277047 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t p1, int8_t p2, int8_t p3, int8_t p4, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((int16_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_UInt16_t11709673_SByte_t2530277047_SByte_t2530277047 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint16_t p1, int8_t p2, int8_t p3, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((uint16_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_Int16_t769030278_Int16_t769030278_SByte_t2530277047_SByte_t2530277047_SByte_t2530277047 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t p1, int16_t p2, int8_t p3, int8_t p4, int8_t p5, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((int16_t*)args[0]), *((int16_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_Int16_t769030278_RuntimeObject_SByte_t2530277047_SByte_t2530277047_SByte_t2530277047 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t p1, RuntimeObject * p2, int8_t p3, int8_t p4, int8_t p5, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((int16_t*)args[0]), (RuntimeObject *)args[1], *((int8_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_UInt16_t11709673 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint16_t p1, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((uint16_t*)args[0]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_Int32_t2571135199_Int32_t2571135199_SByte_t2530277047_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int8_t p3, RuntimeObject * p4, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int8_t*)args[2]), (RuntimeObject *)args[3], methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_SByte_t2530277047_Int32_t2571135199_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int32_t p2, RuntimeObject * p3, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((int8_t*)args[0]), *((int32_t*)args[1]), (RuntimeObject *)args[2], methodMetadata);
	return NULL;
}

void* RuntimeInvoker_UInt16_t11709673_UInt16_t11709673_UInt16_t11709673 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, uint16_t p1, uint16_t p2, const RuntimeMethod* method);
	uint16_t ret = ((Func)methodPointer)(obj, *((uint16_t*)args[0]), *((uint16_t*)args[1]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_OpFlags_t3924773783_SByte_t2530277047_SByte_t2530277047_SByte_t2530277047_SByte_t2530277047 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int8_t p1, int8_t p2, int8_t p3, int8_t p4, const RuntimeMethod* method);
	uint16_t ret = ((Func)methodPointer)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Void_t1078098495_UInt16_t11709673_UInt16_t11709673 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint16_t p1, uint16_t p2, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((uint16_t*)args[0]), *((uint16_t*)args[1]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Boolean_t2059672899_Int32_t2571135199_Int32U26_t2596978809_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int32_t* p2, int32_t p3, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, *((int32_t*)args[0]), (int32_t*)args[1], *((int32_t*)args[2]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_Int32_t2571135199_Int32U26_t2596978809_Int32U26_t2596978809_SByte_t2530277047 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int32_t* p2, int32_t* p3, int8_t p4, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, *((int32_t*)args[0]), (int32_t*)args[1], (int32_t*)args[2], *((int8_t*)args[3]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_Int32U26_t2596978809_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t p2, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, (int32_t*)args[0], *((int32_t*)args[1]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_UInt16_t11709673_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, uint16_t p1, int32_t p2, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, *((uint16_t*)args[0]), *((int32_t*)args[1]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_Int32_t2571135199_Int32_t2571135199_SByte_t2530277047_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int32_t p2, int8_t p3, int32_t p4, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int8_t*)args[2]), *((int32_t*)args[3]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Void_t1078098495_Int32_t2571135199_Int32U26_t2596978809_Int32U26_t2596978809 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, int32_t* p3, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((int32_t*)args[0]), (int32_t*)args[1], (int32_t*)args[2], methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_RuntimeObject_Int32_t2571135199_Int32_t2571135199_SByte_t2530277047_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RuntimeObject * p1, int32_t p2, int32_t p3, int8_t p4, int32_t p5, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int8_t*)args[3]), *((int32_t*)args[4]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Interval_t797373189 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef Interval_t797373189  (*Func)(void* obj, const RuntimeMethod* method);
	Interval_t797373189  ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_Interval_t797373189 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Interval_t797373189  p1, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, *((Interval_t797373189 *)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Void_t1078098495_Interval_t797373189 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Interval_t797373189  p1, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((Interval_t797373189 *)args[0]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Interval_t797373189_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef Interval_t797373189  (*Func)(void* obj, int32_t p1, const RuntimeMethod* method);
	Interval_t797373189  ret = ((Func)methodPointer)(obj, *((int32_t*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Void_t1078098495_Int32_t2571135199_Int32_t2571135199_RuntimeObject_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, RuntimeObject * p3, RuntimeObject * p4, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (RuntimeObject *)args[2], (RuntimeObject *)args[3], methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Double_t1454265465_Interval_t797373189 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef double (*Func)(void* obj, Interval_t797373189  p1, const RuntimeMethod* method);
	double ret = ((Func)methodPointer)(obj, *((Interval_t797373189 *)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_RuntimeObject_Interval_t797373189_RuntimeObject_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, Interval_t797373189  p1, RuntimeObject * p2, RuntimeObject * p3, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, *((Interval_t797373189 *)args[0]), (RuntimeObject *)args[1], (RuntimeObject *)args[2], methodMetadata);
	return ret;
}

void* RuntimeInvoker_Int32_t2571135199_RuntimeObject_Int32U26_t2596978809_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RuntimeObject * p1, int32_t* p2, int32_t p3, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int32_t2571135199_RuntimeObject_Int32U26_t2596978809_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RuntimeObject * p1, int32_t* p2, int32_t p3, int32_t p4, int32_t p5, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_RuntimeObject_RuntimeObject_Int32U26_t2596978809 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, RuntimeObject * p1, int32_t* p2, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], (int32_t*)args[1], methodMetadata);
	return ret;
}

void* RuntimeInvoker_RuntimeObject_RegexOptionsU26_t416869168 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, int32_t* p1, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, (int32_t*)args[0], methodMetadata);
	return ret;
}

void* RuntimeInvoker_Void_t1078098495_RegexOptionsU26_t416869168_SByte_t2530277047 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, int8_t p2, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (int32_t*)args[0], *((int8_t*)args[1]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Boolean_t2059672899_Int32U26_t2596978809_Int32U26_t2596978809_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t* p2, int32_t p3, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, (int32_t*)args[0], (int32_t*)args[1], *((int32_t*)args[2]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Category_t2116501226 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, const RuntimeMethod* method);
	uint16_t ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Void_t1078098495_Int32U26_t2596978809_Int32U26_t2596978809 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, int32_t* p2, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (int32_t*)args[0], (int32_t*)args[1], methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_Int32_t2571135199_Int32_t2571135199_SByte_t2530277047 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int8_t p3, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int8_t*)args[2]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_RuntimeObject_RuntimeObject_SByte_t2530277047_SByte_t2530277047 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RuntimeObject * p1, RuntimeObject * p2, int8_t p3, int8_t p4, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (RuntimeObject *)args[0], (RuntimeObject *)args[1], *((int8_t*)args[2]), *((int8_t*)args[3]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_UInt16_t11709673_SByte_t2530277047 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint16_t p1, int8_t p2, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((uint16_t*)args[0]), *((int8_t*)args[1]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_Int16_t769030278_Int16_t769030278 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t p1, int16_t p2, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((int16_t*)args[0]), *((int16_t*)args[1]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_RuntimeObject_Int32_t2571135199_Int32_t2571135199_RuntimeObject_SByte_t2530277047 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RuntimeObject * p1, int32_t p2, int32_t p3, RuntimeObject * p4, int8_t p5, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (RuntimeObject *)args[3], *((int8_t*)args[4]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_RuntimeObject_Int32_t2571135199_Int32_t2571135199_UInt16_t11709673 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RuntimeObject * p1, int32_t p2, int32_t p3, uint16_t p4, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((uint16_t*)args[3]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Position_t3652736430 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, const RuntimeMethod* method);
	uint16_t ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_UriHostNameType_t1837236037_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RuntimeObject * p1, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Void_t1078098495_StringU26_t2155547156 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, String_t** p1, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (String_t**)args[0], methodMetadata);
	return NULL;
}

void* RuntimeInvoker_RuntimeObject_RuntimeObject_SByte_t2530277047_SByte_t2530277047_SByte_t2530277047 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, RuntimeObject * p1, int8_t p2, int8_t p3, int8_t p4, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int8_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), methodMetadata);
	return ret;
}

void* RuntimeInvoker_Char_t3572527572_RuntimeObject_Int32U26_t2596978809_CharU26_t2918757708 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef Il2CppChar (*Func)(void* obj, RuntimeObject * p1, int32_t* p2, Il2CppChar* p3, const RuntimeMethod* method);
	Il2CppChar ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], (int32_t*)args[1], (Il2CppChar*)args[2], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Void_t1078098495_RuntimeObject_UriFormatExceptionU26_t3440399237 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RuntimeObject * p1, UriFormatException_t2325300979 ** p2, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (RuntimeObject *)args[0], (UriFormatException_t2325300979 **)args[1], methodMetadata);
	return NULL;
}

void* RuntimeInvoker_RuntimeObject_RuntimeObject_RuntimeObject_RuntimeObject_Int32_t2571135199_RuntimeObject_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, RuntimeObject * p1, RuntimeObject * p2, RuntimeObject * p3, int32_t p4, RuntimeObject * p5, RuntimeObject * p6, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], (RuntimeObject *)args[1], (RuntimeObject *)args[2], *((int32_t*)args[3]), (RuntimeObject *)args[4], (RuntimeObject *)args[5], methodMetadata);
	return ret;
}

void* RuntimeInvoker_Sign_t3270271765_RuntimeObject_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RuntimeObject * p1, RuntimeObject * p2, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], (RuntimeObject *)args[1], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_ConfidenceFactor_t3678829216 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_UInt32_t2739056616_Int32_t2571135199_SByte_t2530277047 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, int32_t p1, int8_t p2, const RuntimeMethod* method);
	uint32_t ret = ((Func)methodPointer)(obj, *((int32_t*)args[0]), *((int8_t*)args[1]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Void_t1078098495_UInt32U26_t3679184984_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199_SByte_t2530277047 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint32_t* p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, int8_t p6, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (uint32_t*)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_X509ChainStatusFlags_t991867662 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Void_t1078098495_Byte_t880488320 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint8_t p1, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((uint8_t*)args[0]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_Byte_t880488320_Byte_t880488320 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint8_t p1, uint8_t p2, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((uint8_t*)args[0]), *((uint8_t*)args[1]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_AlertLevel_t912398071 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, const RuntimeMethod* method);
	uint8_t ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_AlertDescription_t952063977 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, const RuntimeMethod* method);
	uint8_t ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_RuntimeObject_Byte_t880488320 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, uint8_t p1, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, *((uint8_t*)args[0]), methodMetadata);
	return ret;
}

void* RuntimeInvoker_Void_t1078098495_Int16_t769030278_RuntimeObject_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199_SByte_t2530277047_SByte_t2530277047_SByte_t2530277047_SByte_t2530277047_Int16_t769030278_SByte_t2530277047_SByte_t2530277047 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t p1, RuntimeObject * p2, int32_t p3, int32_t p4, int32_t p5, int8_t p6, int8_t p7, int8_t p8, int8_t p9, int16_t p10, int8_t p11, int8_t p12, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((int16_t*)args[0]), (RuntimeObject *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), *((int8_t*)args[6]), *((int8_t*)args[7]), *((int8_t*)args[8]), *((int16_t*)args[9]), *((int8_t*)args[10]), *((int8_t*)args[11]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_CipherAlgorithmType_t3078901768 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_HashAlgorithmType_t2406960816 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_ExchangeAlgorithmType_t608439419 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Void_t1078098495_RuntimeObject_Int32_t2571135199_Int16_t769030278 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RuntimeObject * p1, int32_t p2, int16_t p3, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), *((int16_t*)args[2]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_RuntimeObject_Int32_t2571135199_Int64_t384086013 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RuntimeObject * p1, int32_t p2, int64_t p3, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), *((int64_t*)args[2]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_RuntimeObject_ByteU5BU5DU26_t321522471_ByteU5BU5DU26_t321522471 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RuntimeObject * p1, ByteU5BU5D_t1014014593** p2, ByteU5BU5D_t1014014593** p3, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (RuntimeObject *)args[0], (ByteU5BU5D_t1014014593**)args[1], (ByteU5BU5D_t1014014593**)args[2], methodMetadata);
	return NULL;
}

void* RuntimeInvoker_RuntimeObject_Byte_t880488320_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, uint8_t p1, RuntimeObject * p2, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, *((uint8_t*)args[0]), (RuntimeObject *)args[1], methodMetadata);
	return ret;
}

void* RuntimeInvoker_RuntimeObject_RuntimeObject_RuntimeObject_RuntimeObject_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, RuntimeObject * p1, RuntimeObject * p2, RuntimeObject * p3, int32_t p4, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], (RuntimeObject *)args[1], (RuntimeObject *)args[2], *((int32_t*)args[3]), methodMetadata);
	return ret;
}

void* RuntimeInvoker_RuntimeObject_Int16_t769030278_RuntimeObject_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199_SByte_t2530277047_SByte_t2530277047_SByte_t2530277047_SByte_t2530277047_Int16_t769030278_SByte_t2530277047_SByte_t2530277047 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, int16_t p1, RuntimeObject * p2, int32_t p3, int32_t p4, int32_t p5, int8_t p6, int8_t p7, int8_t p8, int8_t p9, int16_t p10, int8_t p11, int8_t p12, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, *((int16_t*)args[0]), (RuntimeObject *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), *((int8_t*)args[6]), *((int8_t*)args[7]), *((int8_t*)args[8]), *((int16_t*)args[9]), *((int8_t*)args[10]), *((int8_t*)args[11]), methodMetadata);
	return ret;
}

void* RuntimeInvoker_Void_t1078098495_RuntimeObject_Int32_t2571135199_RuntimeObject_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RuntimeObject * p1, int32_t p2, RuntimeObject * p3, RuntimeObject * p4, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), (RuntimeObject *)args[2], (RuntimeObject *)args[3], methodMetadata);
	return NULL;
}

void* RuntimeInvoker_SecurityProtocolType_t4172395504 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_SecurityCompressionType_t1053362341 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_HandshakeType_t2678014388 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, const RuntimeMethod* method);
	uint8_t ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_HandshakeState_t3800758483 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_SecurityProtocolType_t4172395504_Int16_t769030278 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int16_t p1, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, *((int16_t*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Void_t1078098495_RuntimeObject_RuntimeObject_RuntimeObject_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RuntimeObject * p1, RuntimeObject * p2, RuntimeObject * p3, RuntimeObject * p4, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (RuntimeObject *)args[0], (RuntimeObject *)args[1], (RuntimeObject *)args[2], (RuntimeObject *)args[3], methodMetadata);
	return NULL;
}

void* RuntimeInvoker_RuntimeObject_Byte_t880488320_RuntimeObject_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, uint8_t p1, RuntimeObject * p2, RuntimeObject * p3, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, *((uint8_t*)args[0]), (RuntimeObject *)args[1], (RuntimeObject *)args[2], methodMetadata);
	return ret;
}

void* RuntimeInvoker_RuntimeObject_Byte_t880488320_RuntimeObject_RuntimeObject_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, uint8_t p1, RuntimeObject * p2, RuntimeObject * p3, RuntimeObject * p4, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, *((uint8_t*)args[0]), (RuntimeObject *)args[1], (RuntimeObject *)args[2], (RuntimeObject *)args[3], methodMetadata);
	return ret;
}

void* RuntimeInvoker_Void_t1078098495_Byte_t880488320_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint8_t p1, RuntimeObject * p2, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((uint8_t*)args[0]), (RuntimeObject *)args[1], methodMetadata);
	return NULL;
}

void* RuntimeInvoker_RuntimeObject_Byte_t880488320_RuntimeObject_Int32_t2571135199_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, uint8_t p1, RuntimeObject * p2, int32_t p3, int32_t p4, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, *((uint8_t*)args[0]), (RuntimeObject *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), methodMetadata);
	return ret;
}

void* RuntimeInvoker_Void_t1078098495_RuntimeObject_RuntimeObject_SByte_t2530277047_Int32_t2571135199_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RuntimeObject * p1, RuntimeObject * p2, int8_t p3, int32_t p4, RuntimeObject * p5, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (RuntimeObject *)args[0], (RuntimeObject *)args[1], *((int8_t*)args[2]), *((int32_t*)args[3]), (RuntimeObject *)args[4], methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_RuntimeObject_RuntimeObject_RuntimeObject_Int32_t2571135199_Int32_t2571135199_SByte_t2530277047_SByte_t2530277047 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RuntimeObject * p1, RuntimeObject * p2, RuntimeObject * p3, int32_t p4, int32_t p5, int8_t p6, int8_t p7, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (RuntimeObject *)args[0], (RuntimeObject *)args[1], (RuntimeObject *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), *((int8_t*)args[6]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_Byte_t880488320_Byte_t880488320_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint8_t p1, uint8_t p2, RuntimeObject * p3, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((uint8_t*)args[0]), *((uint8_t*)args[1]), (RuntimeObject *)args[2], methodMetadata);
	return NULL;
}

void* RuntimeInvoker_RSAParameters_t2749614896 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RSAParameters_t2749614896  (*Func)(void* obj, const RuntimeMethod* method);
	RSAParameters_t2749614896  ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Void_t1078098495_RuntimeObject_Byte_t880488320_Byte_t880488320 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RuntimeObject * p1, uint8_t p2, uint8_t p3, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((uint8_t*)args[1]), *((uint8_t*)args[2]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_RuntimeObject_Byte_t880488320_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RuntimeObject * p1, uint8_t p2, RuntimeObject * p3, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((uint8_t*)args[1]), (RuntimeObject *)args[2], methodMetadata);
	return NULL;
}

void* RuntimeInvoker_ContentType_t3977715531 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, const RuntimeMethod* method);
	uint8_t ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_RuntimeObject_RuntimeObject_RuntimeObject_RuntimeObject_RuntimeObject_RuntimeObject_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, RuntimeObject * p1, RuntimeObject * p2, RuntimeObject * p3, RuntimeObject * p4, RuntimeObject * p5, RuntimeObject * p6, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], (RuntimeObject *)args[1], (RuntimeObject *)args[2], (RuntimeObject *)args[3], (RuntimeObject *)args[4], (RuntimeObject *)args[5], methodMetadata);
	return ret;
}

void* RuntimeInvoker_RuntimeObject_RuntimeObject_RuntimeObject_Int32_t2571135199_RuntimeObject_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, RuntimeObject * p1, RuntimeObject * p2, int32_t p3, RuntimeObject * p4, RuntimeObject * p5, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], (RuntimeObject *)args[1], *((int32_t*)args[2]), (RuntimeObject *)args[3], (RuntimeObject *)args[4], methodMetadata);
	return ret;
}

void* RuntimeInvoker_Rect_t1111852907 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef Rect_t1111852907  (*Func)(void* obj, const RuntimeMethod* method);
	Rect_t1111852907  ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Void_t1078098495_RectU26_t2812713421 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t1111852907 * p1, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (Rect_t1111852907 *)args[0], methodMetadata);
	return NULL;
}

void* RuntimeInvoker_CameraClearFlags_t4003071365 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Ray_t1113537957_Vector3_t3239555143 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef Ray_t1113537957  (*Func)(void* obj, Vector3_t3239555143  p1, const RuntimeMethod* method);
	Ray_t1113537957  ret = ((Func)methodPointer)(obj, *((Vector3_t3239555143 *)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Void_t1078098495_RuntimeObject_Vector3U26_t865664337_RayU26_t3990586275 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RuntimeObject * p1, Vector3_t3239555143 * p2, Ray_t1113537957 * p3, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (RuntimeObject *)args[0], (Vector3_t3239555143 *)args[1], (Ray_t1113537957 *)args[2], methodMetadata);
	return NULL;
}

void* RuntimeInvoker_RuntimeObject_Ray_t1113537957_Single_t496865882_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, Ray_t1113537957  p1, float p2, int32_t p3, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, *((Ray_t1113537957 *)args[0]), *((float*)args[1]), *((int32_t*)args[2]), methodMetadata);
	return ret;
}

void* RuntimeInvoker_RuntimeObject_RuntimeObject_RayU26_t3990586275_Single_t496865882_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, RuntimeObject * p1, Ray_t1113537957 * p2, float p3, int32_t p4, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], (Ray_t1113537957 *)args[1], *((float*)args[2]), *((int32_t*)args[3]), methodMetadata);
	return ret;
}

void* RuntimeInvoker_Void_t1078098495_RuntimeObject_IntPtr_t_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RuntimeObject * p1, intptr_t p2, int32_t p3, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((intptr_t*)args[1]), *((int32_t*)args[2]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_CullingGroupEvent_t157813866 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, CullingGroupEvent_t157813866  p1, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((CullingGroupEvent_t157813866 *)args[0]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_RuntimeObject_CullingGroupEvent_t157813866_RuntimeObject_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, CullingGroupEvent_t157813866  p1, RuntimeObject * p2, RuntimeObject * p3, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, *((CullingGroupEvent_t157813866 *)args[0]), (RuntimeObject *)args[1], (RuntimeObject *)args[2], methodMetadata);
	return ret;
}

void* RuntimeInvoker_Void_t1078098495_Int32_t2571135199_RuntimeObject_RuntimeObject_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, RuntimeObject * p2, RuntimeObject * p3, RuntimeObject * p4, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((int32_t*)args[0]), (RuntimeObject *)args[1], (RuntimeObject *)args[2], (RuntimeObject *)args[3], methodMetadata);
	return NULL;
}

void* RuntimeInvoker_RuntimeObject_Vector3_t3239555143 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, Vector3_t3239555143  p1, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, *((Vector3_t3239555143 *)args[0]), methodMetadata);
	return ret;
}

void* RuntimeInvoker_RuntimeObject_RuntimeObject_Vector3U26_t865664337 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, RuntimeObject * p1, Vector3_t3239555143 * p2, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], (Vector3_t3239555143 *)args[1], methodMetadata);
	return ret;
}

void* RuntimeInvoker_Vector3_t3239555143 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef Vector3_t3239555143  (*Func)(void* obj, const RuntimeMethod* method);
	Vector3_t3239555143  ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Void_t1078098495_Vector3U26_t865664337 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t3239555143 * p1, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (Vector3_t3239555143 *)args[0], methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_Single_t496865882_Single_t496865882_Single_t496865882 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, float p2, float p3, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_Vector4_t4052901136_Vector4_t4052901136_Vector4_t4052901136_Vector4_t4052901136 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector4_t4052901136  p1, Vector4_t4052901136  p2, Vector4_t4052901136  p3, Vector4_t4052901136  p4, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((Vector4_t4052901136 *)args[0]), *((Vector4_t4052901136 *)args[1]), *((Vector4_t4052901136 *)args[2]), *((Vector4_t4052901136 *)args[3]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Vector4_t4052901136_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef Vector4_t4052901136  (*Func)(void* obj, int32_t p1, const RuntimeMethod* method);
	Vector4_t4052901136  ret = ((Func)methodPointer)(obj, *((int32_t*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Matrix4x4_t3534180298 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef Matrix4x4_t3534180298  (*Func)(void* obj, const RuntimeMethod* method);
	Matrix4x4_t3534180298  ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Single_t496865882_Single_t496865882_Single_t496865882 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef float (*Func)(void* obj, float p1, float p2, const RuntimeMethod* method);
	float ret = ((Func)methodPointer)(obj, *((float*)args[0]), *((float*)args[1]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_Single_t496865882_Single_t496865882 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, float p1, float p2, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, *((float*)args[0]), *((float*)args[1]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Void_t1078098495_RuntimeObject_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199_SByte_t2530277047_SByte_t2530277047_IntPtr_t (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RuntimeObject * p1, int32_t p2, int32_t p3, int32_t p4, int8_t p5, int8_t p6, intptr_t p7, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), *((intptr_t*)args[6]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_PlayableHandle_t3938927722 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef PlayableHandle_t3938927722  (*Func)(void* obj, const RuntimeMethod* method);
	PlayableHandle_t3938927722  ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_PlayableHandle_t3938927722_PlayableHandle_t3938927722 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, PlayableHandle_t3938927722  p1, PlayableHandle_t3938927722  p2, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, *((PlayableHandle_t3938927722 *)args[0]), *((PlayableHandle_t3938927722 *)args[1]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Void_t1078098495_PlayableHandle_t3938927722 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, PlayableHandle_t3938927722  p1, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((PlayableHandle_t3938927722 *)args[0]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Playable_t1760619938 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef Playable_t1760619938  (*Func)(void* obj, const RuntimeMethod* method);
	Playable_t1760619938  ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_Playable_t1760619938 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Playable_t1760619938  p1, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, *((Playable_t1760619938 *)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_PlayableOutputHandle_t2719521870 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef PlayableOutputHandle_t2719521870  (*Func)(void* obj, const RuntimeMethod* method);
	PlayableOutputHandle_t2719521870  ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_PlayableOutputHandle_t2719521870_PlayableOutputHandle_t2719521870 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, PlayableOutputHandle_t2719521870  p1, PlayableOutputHandle_t2719521870  p2, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, *((PlayableOutputHandle_t2719521870 *)args[0]), *((PlayableOutputHandle_t2719521870 *)args[1]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Void_t1078098495_PlayableOutputHandle_t2719521870 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, PlayableOutputHandle_t2719521870  p1, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((PlayableOutputHandle_t2719521870 *)args[0]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Boolean_t2059672899_PlayableOutput_t3840687608 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, PlayableOutput_t3840687608  p1, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, *((PlayableOutput_t3840687608 *)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Void_t1078098495_Scene_t3023997566_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Scene_t3023997566  p1, int32_t p2, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((Scene_t3023997566 *)args[0]), *((int32_t*)args[1]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_Scene_t3023997566 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Scene_t3023997566  p1, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((Scene_t3023997566 *)args[0]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_Scene_t3023997566_Scene_t3023997566 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Scene_t3023997566  p1, Scene_t3023997566  p2, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((Scene_t3023997566 *)args[0]), *((Scene_t3023997566 *)args[1]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_Single_t496865882_Single_t496865882_Single_t496865882_Single_t496865882 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, float p2, float p3, float p4, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), *((float*)args[3]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Color_t4024113822 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef Color_t4024113822  (*Func)(void* obj, const RuntimeMethod* method);
	Color_t4024113822  ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Vector4_t4052901136_Color_t4024113822 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef Vector4_t4052901136  (*Func)(void* obj, Color_t4024113822  p1, const RuntimeMethod* method);
	Vector4_t4052901136  ret = ((Func)methodPointer)(obj, *((Color_t4024113822 *)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Void_t1078098495_RuntimeObject_Int32_t2571135199_Int32_t2571135199_RuntimeObject_IntPtr_t (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RuntimeObject * p1, int32_t p2, int32_t p3, RuntimeObject * p4, intptr_t p5, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (RuntimeObject *)args[3], *((intptr_t*)args[4]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_Int64_t384086013_Int32_t2571135199_RuntimeObject_IntPtr_t (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, int32_t p2, RuntimeObject * p3, intptr_t p4, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((int64_t*)args[0]), *((int32_t*)args[1]), (RuntimeObject *)args[2], *((intptr_t*)args[3]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_RuntimeObject_Vector3_t3239555143_HitInfoU26_t2509763475 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RuntimeObject * p1, Vector3_t3239555143  p2, HitInfo_t184839221 * p3, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((Vector3_t3239555143 *)args[1]), (HitInfo_t184839221 *)args[2], methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_Int32_t2571135199_HitInfo_t184839221 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, HitInfo_t184839221  p2, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((int32_t*)args[0]), *((HitInfo_t184839221 *)args[1]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Boolean_t2059672899_HitInfo_t184839221 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, HitInfo_t184839221  p1, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, *((HitInfo_t184839221 *)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_HitInfo_t184839221_HitInfo_t184839221 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, HitInfo_t184839221  p1, HitInfo_t184839221  p2, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, *((HitInfo_t184839221 *)args[0]), *((HitInfo_t184839221 *)args[1]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_PropertyName_t666104458_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef PropertyName_t666104458  (*Func)(void* obj, RuntimeObject * p1, const RuntimeMethod* method);
	PropertyName_t666104458  ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Void_t1078098495_RuntimeObject_PropertyNameU26_t334041606 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RuntimeObject * p1, PropertyName_t666104458 * p2, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (RuntimeObject *)args[0], (PropertyName_t666104458 *)args[1], methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_PropertyName_t666104458 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, PropertyName_t666104458  p1, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((PropertyName_t666104458 *)args[0]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Boolean_t2059672899_PropertyName_t666104458_PropertyName_t666104458 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, PropertyName_t666104458  p1, PropertyName_t666104458  p2, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, *((PropertyName_t666104458 *)args[0]), *((PropertyName_t666104458 *)args[1]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_PropertyName_t666104458_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef PropertyName_t666104458  (*Func)(void* obj, int32_t p1, const RuntimeMethod* method);
	PropertyName_t666104458  ret = ((Func)methodPointer)(obj, *((int32_t*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_Vector3_t3239555143 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector3_t3239555143  p1, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, *((Vector3_t3239555143 *)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Void_t1078098495_RuntimeObject_StringU26_t2155547156_StringU26_t2155547156 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RuntimeObject * p1, String_t** p2, String_t** p3, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (RuntimeObject *)args[0], (String_t**)args[1], (String_t**)args[2], methodMetadata);
	return NULL;
}

void* RuntimeInvoker_PersistentListenerMode_t677115295 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Playable_t1760619938_PlayableGraph_t1897216728_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef Playable_t1760619938  (*Func)(void* obj, PlayableGraph_t1897216728  p1, RuntimeObject * p2, const RuntimeMethod* method);
	Playable_t1760619938  ret = ((Func)methodPointer)(obj, *((PlayableGraph_t1897216728 *)args[0]), (RuntimeObject *)args[1], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Void_t1078098495_RuntimeObject_PlayableGraph_t1897216728_RuntimeObject_IntPtr_t (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RuntimeObject * p1, PlayableGraph_t1897216728  p2, RuntimeObject * p3, intptr_t p4, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((PlayableGraph_t1897216728 *)args[1]), (RuntimeObject *)args[2], *((intptr_t*)args[3]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_LogType_t1259498875 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Void_t1078098495_IntPtr_t_Int64_t384086013_Int64_t384086013_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, intptr_t p1, int64_t p2, int64_t p3, RuntimeObject * p4, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((intptr_t*)args[0]), *((int64_t*)args[1]), *((int64_t*)args[2]), (RuntimeObject *)args[3], methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_Guid_t_RuntimeObject_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Guid_t  p1, RuntimeObject * p2, int32_t p3, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((Guid_t *)args[0]), (RuntimeObject *)args[1], *((int32_t*)args[2]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_ScriptableRenderContext_t1486166028_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ScriptableRenderContext_t1486166028  p1, RuntimeObject * p2, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((ScriptableRenderContext_t1486166028 *)args[0]), (RuntimeObject *)args[1], methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_RuntimeObject_RuntimeObject_IntPtr_t (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RuntimeObject * p1, RuntimeObject * p2, intptr_t p3, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (RuntimeObject *)args[0], (RuntimeObject *)args[1], *((intptr_t*)args[2]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_CSSSize_t2436738465_IntPtr_t_Single_t496865882_Int32_t2571135199_Single_t496865882_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef CSSSize_t2436738465  (*Func)(void* obj, intptr_t p1, float p2, int32_t p3, float p4, int32_t p5, const RuntimeMethod* method);
	CSSSize_t2436738465  ret = ((Func)methodPointer)(obj, *((intptr_t*)args[0]), *((float*)args[1]), *((int32_t*)args[2]), *((float*)args[3]), *((int32_t*)args[4]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_RuntimeObject_IntPtr_t_Single_t496865882_Int32_t2571135199_Single_t496865882_Int32_t2571135199_RuntimeObject_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, intptr_t p1, float p2, int32_t p3, float p4, int32_t p5, RuntimeObject * p6, RuntimeObject * p7, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, *((intptr_t*)args[0]), *((float*)args[1]), *((int32_t*)args[2]), *((float*)args[3]), *((int32_t*)args[4]), (RuntimeObject *)args[5], (RuntimeObject *)args[6], methodMetadata);
	return ret;
}

void* RuntimeInvoker_CSSSize_t2436738465_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef CSSSize_t2436738465  (*Func)(void* obj, RuntimeObject * p1, const RuntimeMethod* method);
	CSSSize_t2436738465  ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Void_t1078098495_IntPtr_t_Single_t496865882_Int32_t2571135199_Single_t496865882_Int32_t2571135199_IntPtr_t (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, intptr_t p1, float p2, int32_t p3, float p4, int32_t p5, intptr_t p6, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((intptr_t*)args[0]), *((float*)args[1]), *((int32_t*)args[2]), *((float*)args[3]), *((int32_t*)args[4]), *((intptr_t*)args[5]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_RuntimeObject_SByte_t2530277047_RuntimeObject_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, int8_t p1, RuntimeObject * p2, RuntimeObject * p3, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, *((int8_t*)args[0]), (RuntimeObject *)args[1], (RuntimeObject *)args[2], methodMetadata);
	return ret;
}

void* RuntimeInvoker_RuntimeObject_Int32_t2571135199_RuntimeObject_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, int32_t p1, RuntimeObject * p2, RuntimeObject * p3, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, *((int32_t*)args[0]), (RuntimeObject *)args[1], (RuntimeObject *)args[2], methodMetadata);
	return ret;
}

void* RuntimeInvoker_Void_t1078098495_RuntimeObject_Int32_t2571135199_PropertyNameU26_t334041606 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RuntimeObject * p1, int32_t p2, PropertyName_t666104458 * p3, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), (PropertyName_t666104458 *)args[2], methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Boolean_t2059672899_AudioMixerPlayable_t1017334687 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, AudioMixerPlayable_t1017334687  p1, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, *((AudioMixerPlayable_t1017334687 *)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_AudioClipPlayable_t3812371941 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, AudioClipPlayable_t3812371941  p1, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, *((AudioClipPlayable_t3812371941 *)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Void_t1078098495_PropertyName_t666104458_Single_t496865882 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, PropertyName_t666104458  p1, float p2, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((PropertyName_t666104458 *)args[0]), *((float*)args[1]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_RuntimeObject_Double_t1454265465_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RuntimeObject * p1, double p2, RuntimeObject * p3, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((double*)args[1]), (RuntimeObject *)args[2], methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_Int64_t384086013_RuntimeObject_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, RuntimeObject * p2, RuntimeObject * p3, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((int64_t*)args[0]), (RuntimeObject *)args[1], (RuntimeObject *)args[2], methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_GcAchievementDescriptionData_t2898970914_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, GcAchievementDescriptionData_t2898970914  p1, int32_t p2, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((GcAchievementDescriptionData_t2898970914 *)args[0]), *((int32_t*)args[1]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_GcUserProfileData_t3262991188_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, GcUserProfileData_t3262991188  p1, int32_t p2, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((GcUserProfileData_t3262991188 *)args[0]), *((int32_t*)args[1]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_UserProfileU5BU5DU26_t2890929411_RuntimeObject_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UserProfileU5BU5D_t4189818181** p1, RuntimeObject * p2, int32_t p3, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (UserProfileU5BU5D_t4189818181**)args[0], (RuntimeObject *)args[1], *((int32_t*)args[2]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_UserProfileU5BU5DU26_t2890929411_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UserProfileU5BU5D_t4189818181** p1, int32_t p2, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (UserProfileU5BU5D_t4189818181**)args[0], *((int32_t*)args[1]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_GcScoreData_t3955401213 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, GcScoreData_t3955401213  p1, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((GcScoreData_t3955401213 *)args[0]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_RuntimeObject_Int32_t2571135199_Int32_t2571135199_RuntimeObject_Int32_t2571135199_Int32_t2571135199_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RuntimeObject * p1, int32_t p2, int32_t p3, RuntimeObject * p4, int32_t p5, int32_t p6, RuntimeObject * p7, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (RuntimeObject *)args[3], *((int32_t*)args[4]), *((int32_t*)args[5]), (RuntimeObject *)args[6], methodMetadata);
	return NULL;
}

void* RuntimeInvoker_UserState_t707530005 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Void_t1078098495_RuntimeObject_Double_t1454265465_SByte_t2530277047_SByte_t2530277047_DateTime_t507798353 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RuntimeObject * p1, double p2, int8_t p3, int8_t p4, DateTime_t507798353  p5, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((double*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), *((DateTime_t507798353 *)args[4]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_RuntimeObject_Double_t1454265465 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RuntimeObject * p1, double p2, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((double*)args[1]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_RuntimeObject_RuntimeObject_RuntimeObject_RuntimeObject_RuntimeObject_SByte_t2530277047_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RuntimeObject * p1, RuntimeObject * p2, RuntimeObject * p3, RuntimeObject * p4, RuntimeObject * p5, int8_t p6, int32_t p7, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (RuntimeObject *)args[0], (RuntimeObject *)args[1], (RuntimeObject *)args[2], (RuntimeObject *)args[3], (RuntimeObject *)args[4], *((int8_t*)args[5]), *((int32_t*)args[6]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_RuntimeObject_Int64_t384086013_RuntimeObject_DateTime_t507798353_RuntimeObject_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RuntimeObject * p1, int64_t p2, RuntimeObject * p3, DateTime_t507798353  p4, RuntimeObject * p5, int32_t p6, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int64_t*)args[1]), (RuntimeObject *)args[2], *((DateTime_t507798353 *)args[3]), (RuntimeObject *)args[4], *((int32_t*)args[5]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_UserScope_t2912238712 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Range_t1266129112 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef Range_t1266129112  (*Func)(void* obj, const RuntimeMethod* method);
	Range_t1266129112  ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Void_t1078098495_Range_t1266129112 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Range_t1266129112  p1, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((Range_t1266129112 *)args[0]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_TimeScope_t1786414419 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_RuntimeObject_Int16_t769030278 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, RuntimeObject * p1, int16_t p2, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int16_t*)args[1]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_RuntimeObject_Int64_t384086013 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, RuntimeObject * p1, int64_t p2, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int64_t*)args[1]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_RuntimeObject_Double_t1454265465 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, RuntimeObject * p1, double p2, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((double*)args[1]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_RuntimeObject_Single_t496865882 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, RuntimeObject * p1, float p2, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((float*)args[1]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_RuntimeObject_Decimal_t2656901032 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, RuntimeObject * p1, Decimal_t2656901032  p2, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((Decimal_t2656901032 *)args[1]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_AnalyticsResult_t1606825939_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RuntimeObject * p1, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_AnalyticsResult_t1606825939_RuntimeObject_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RuntimeObject * p1, RuntimeObject * p2, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], (RuntimeObject *)args[1], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Void_t1078098495_Int32_t2571135199_Int64_t384086013_Int64_t384086013_SByte_t2530277047 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int64_t p2, int64_t p3, int8_t p4, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((int32_t*)args[0]), *((int64_t*)args[1]), *((int64_t*)args[2]), *((int8_t*)args[3]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_RuntimeObject_Int32_t2571135199_Int64_t384086013_Int64_t384086013_SByte_t2530277047_RuntimeObject_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, int32_t p1, int64_t p2, int64_t p3, int8_t p4, RuntimeObject * p5, RuntimeObject * p6, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, *((int32_t*)args[0]), *((int64_t*)args[1]), *((int64_t*)args[2]), *((int8_t*)args[3]), (RuntimeObject *)args[4], (RuntimeObject *)args[5], methodMetadata);
	return ret;
}

void* RuntimeInvoker_Void_t1078098495_Int32_t2571135199_ObjectU26_t4136591053 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, RuntimeObject ** p2, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((int32_t*)args[0]), (RuntimeObject **)args[1], methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_ObjectU5BU5DU26_t3608108294_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ObjectU5BU5D_t4290686858** p1, int32_t p2, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (ObjectU5BU5D_t4290686858**)args[0], *((int32_t*)args[1]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_ObjectU5BU5DU26_t3608108294_Int32_t2571135199_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ObjectU5BU5D_t4290686858** p1, int32_t p2, int32_t p3, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (ObjectU5BU5D_t4290686858**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_KeyValuePair_2_t3602437993 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t3602437993  p1, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((KeyValuePair_2_t3602437993 *)args[0]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Boolean_t2059672899_KeyValuePair_2_t3602437993 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t3602437993  p1, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, *((KeyValuePair_2_t3602437993 *)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t3602437993_RuntimeObject_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef KeyValuePair_2_t3602437993  (*Func)(void* obj, RuntimeObject * p1, RuntimeObject * p2, const RuntimeMethod* method);
	KeyValuePair_2_t3602437993  ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], (RuntimeObject *)args[1], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_RuntimeObject_ObjectU26_t4136591053 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, RuntimeObject * p1, RuntimeObject ** p2, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], (RuntimeObject **)args[1], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Enumerator_t2164740473 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef Enumerator_t2164740473  (*Func)(void* obj, const RuntimeMethod* method);
	Enumerator_t2164740473  ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_DictionaryEntry_t1385909157_RuntimeObject_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef DictionaryEntry_t1385909157  (*Func)(void* obj, RuntimeObject * p1, RuntimeObject * p2, const RuntimeMethod* method);
	DictionaryEntry_t1385909157  ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], (RuntimeObject *)args[1], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t3602437993 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef KeyValuePair_2_t3602437993  (*Func)(void* obj, const RuntimeMethod* method);
	KeyValuePair_2_t3602437993  ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Enumerator_t2934400376 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef Enumerator_t2934400376  (*Func)(void* obj, const RuntimeMethod* method);
	Enumerator_t2934400376  ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_RuntimeObject_ObjectU26_t4136591053_RuntimeObject_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, RuntimeObject ** p1, RuntimeObject * p2, RuntimeObject * p3, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, (RuntimeObject **)args[0], (RuntimeObject *)args[1], (RuntimeObject *)args[2], methodMetadata);
	return ret;
}

void* RuntimeInvoker_Enumerator_t4228099087 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef Enumerator_t4228099087  (*Func)(void* obj, const RuntimeMethod* method);
	Enumerator_t4228099087  ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Enumerator_t1041909981 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef Enumerator_t1041909981  (*Func)(void* obj, const RuntimeMethod* method);
	Enumerator_t1041909981  ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_RuntimeObject_RuntimeObject_RuntimeObject_RuntimeObject_RuntimeObject_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, RuntimeObject * p1, RuntimeObject * p2, RuntimeObject * p3, RuntimeObject * p4, RuntimeObject * p5, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], (RuntimeObject *)args[1], (RuntimeObject *)args[2], (RuntimeObject *)args[3], (RuntimeObject *)args[4], methodMetadata);
	return ret;
}

void* RuntimeInvoker_Boolean_t2059672899_IntPtr_t_ObjectU26_t4136591053 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, intptr_t p1, RuntimeObject ** p2, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, *((intptr_t*)args[0]), (RuntimeObject **)args[1], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Void_t1078098495_RuntimeObject_RuntimeObject_Single_t496865882 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RuntimeObject * p1, RuntimeObject * p2, float p3, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (RuntimeObject *)args[0], (RuntimeObject *)args[1], *((float*)args[2]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_WorkRequest_t2547429811 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef WorkRequest_t2547429811  (*Func)(void* obj, const RuntimeMethod* method);
	WorkRequest_t2547429811  ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_TableRange_t4125455382 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, TableRange_t4125455382  p1, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, *((TableRange_t4125455382 *)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_DictionaryEntry_t1385909157 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, DictionaryEntry_t1385909157  p1, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, *((DictionaryEntry_t1385909157 *)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_KeyValuePair_2_t2204764377 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t2204764377  p1, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, *((KeyValuePair_2_t2204764377 *)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_KeyValuePair_2_t2366093889 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t2366093889  p1, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, *((KeyValuePair_2_t2366093889 *)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_KeyValuePair_2_t2877556189 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t2877556189  p1, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, *((KeyValuePair_2_t2877556189 *)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_Link_t3525875035 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Link_t3525875035  p1, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, *((Link_t3525875035 *)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_Slot_t2102621701 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Slot_t2102621701  p1, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, *((Slot_t2102621701 *)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_Slot_t1141617207 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Slot_t1141617207  p1, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, *((Slot_t1141617207 *)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_CustomAttributeNamedArgument_t425730339 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, CustomAttributeNamedArgument_t425730339  p1, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, *((CustomAttributeNamedArgument_t425730339 *)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_CustomAttributeTypedArgument_t2667702721 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, CustomAttributeTypedArgument_t2667702721  p1, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, *((CustomAttributeTypedArgument_t2667702721 *)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_LabelData_t1059919410 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, LabelData_t1059919410  p1, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, *((LabelData_t1059919410 *)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_LabelFixup_t2685311059 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, LabelFixup_t2685311059  p1, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, *((LabelFixup_t2685311059 *)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_ILTokenInfo_t281295013 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, ILTokenInfo_t281295013  p1, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, *((ILTokenInfo_t281295013 *)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_MonoResource_t2965441204 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, MonoResource_t2965441204  p1, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, *((MonoResource_t2965441204 *)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_RefEmitPermissionSet_t945207422 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, RefEmitPermissionSet_t945207422  p1, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, *((RefEmitPermissionSet_t945207422 *)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_ParameterModifier_t969812002 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, ParameterModifier_t969812002  p1, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, *((ParameterModifier_t969812002 *)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_ResourceCacheItem_t3632973466 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, ResourceCacheItem_t3632973466  p1, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, *((ResourceCacheItem_t3632973466 *)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_ResourceInfo_t1773833009 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, ResourceInfo_t1773833009  p1, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, *((ResourceInfo_t1773833009 *)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_Byte_t880488320 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, uint8_t p1, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, *((uint8_t*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_X509ChainStatus_t177750891 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, X509ChainStatus_t177750891  p1, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, *((X509ChainStatus_t177750891 *)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_Mark_t4050795874 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Mark_t4050795874  p1, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, *((Mark_t4050795874 *)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_UriScheme_t2622429923 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, UriScheme_t2622429923  p1, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, *((UriScheme_t2622429923 *)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_Keyframe_t3396755990 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Keyframe_t3396755990  p1, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, *((Keyframe_t3396755990 *)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_PlayableBinding_t136667248 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, PlayableBinding_t136667248  p1, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, *((PlayableBinding_t136667248 *)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_GcAchievementData_t3167706508 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, GcAchievementData_t3167706508  p1, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, *((GcAchievementData_t3167706508 *)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_GcScoreData_t3955401213 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, GcScoreData_t3955401213  p1, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, *((GcScoreData_t3955401213 *)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_WorkRequest_t2547429811 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, WorkRequest_t2547429811  p1, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, *((WorkRequest_t2547429811 *)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int32_t2571135199_RuntimeObject_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RuntimeObject * p1, int32_t p2, int32_t p3, int32_t p4, RuntimeObject * p5, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), (RuntimeObject *)args[4], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int32_t2571135199_RuntimeObject_CustomAttributeNamedArgument_t425730339_Int32_t2571135199_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RuntimeObject * p1, CustomAttributeNamedArgument_t425730339  p2, int32_t p3, int32_t p4, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((CustomAttributeNamedArgument_t425730339 *)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int32_t2571135199_RuntimeObject_CustomAttributeNamedArgument_t425730339 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RuntimeObject * p1, CustomAttributeNamedArgument_t425730339  p2, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((CustomAttributeNamedArgument_t425730339 *)args[1]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int32_t2571135199_RuntimeObject_CustomAttributeTypedArgument_t2667702721_Int32_t2571135199_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RuntimeObject * p1, CustomAttributeTypedArgument_t2667702721  p2, int32_t p3, int32_t p4, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((CustomAttributeTypedArgument_t2667702721 *)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int32_t2571135199_RuntimeObject_CustomAttributeTypedArgument_t2667702721 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RuntimeObject * p1, CustomAttributeTypedArgument_t2667702721  p2, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((CustomAttributeTypedArgument_t2667702721 *)args[1]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int32_t2571135199_TableRange_t4125455382 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, TableRange_t4125455382  p1, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, *((TableRange_t4125455382 *)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int32_t2571135199_DictionaryEntry_t1385909157 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, DictionaryEntry_t1385909157  p1, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, *((DictionaryEntry_t1385909157 *)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int32_t2571135199_KeyValuePair_2_t2204764377 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, KeyValuePair_2_t2204764377  p1, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, *((KeyValuePair_2_t2204764377 *)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int32_t2571135199_KeyValuePair_2_t2366093889 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, KeyValuePair_2_t2366093889  p1, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, *((KeyValuePair_2_t2366093889 *)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int32_t2571135199_KeyValuePair_2_t2877556189 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, KeyValuePair_2_t2877556189  p1, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, *((KeyValuePair_2_t2877556189 *)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int32_t2571135199_KeyValuePair_2_t3602437993 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, KeyValuePair_2_t3602437993  p1, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, *((KeyValuePair_2_t3602437993 *)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int32_t2571135199_Link_t3525875035 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Link_t3525875035  p1, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, *((Link_t3525875035 *)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int32_t2571135199_Slot_t2102621701 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Slot_t2102621701  p1, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, *((Slot_t2102621701 *)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int32_t2571135199_Slot_t1141617207 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Slot_t1141617207  p1, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, *((Slot_t1141617207 *)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int32_t2571135199_CustomAttributeNamedArgument_t425730339 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, CustomAttributeNamedArgument_t425730339  p1, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, *((CustomAttributeNamedArgument_t425730339 *)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int32_t2571135199_CustomAttributeTypedArgument_t2667702721 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, CustomAttributeTypedArgument_t2667702721  p1, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, *((CustomAttributeTypedArgument_t2667702721 *)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int32_t2571135199_LabelData_t1059919410 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, LabelData_t1059919410  p1, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, *((LabelData_t1059919410 *)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int32_t2571135199_LabelFixup_t2685311059 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, LabelFixup_t2685311059  p1, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, *((LabelFixup_t2685311059 *)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int32_t2571135199_ILTokenInfo_t281295013 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, ILTokenInfo_t281295013  p1, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, *((ILTokenInfo_t281295013 *)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int32_t2571135199_MonoResource_t2965441204 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, MonoResource_t2965441204  p1, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, *((MonoResource_t2965441204 *)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int32_t2571135199_RefEmitPermissionSet_t945207422 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RefEmitPermissionSet_t945207422  p1, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, *((RefEmitPermissionSet_t945207422 *)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int32_t2571135199_ParameterModifier_t969812002 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, ParameterModifier_t969812002  p1, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, *((ParameterModifier_t969812002 *)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int32_t2571135199_ResourceCacheItem_t3632973466 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, ResourceCacheItem_t3632973466  p1, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, *((ResourceCacheItem_t3632973466 *)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int32_t2571135199_ResourceInfo_t1773833009 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, ResourceInfo_t1773833009  p1, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, *((ResourceInfo_t1773833009 *)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int32_t2571135199_Byte_t880488320 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, uint8_t p1, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, *((uint8_t*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int32_t2571135199_X509ChainStatus_t177750891 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, X509ChainStatus_t177750891  p1, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, *((X509ChainStatus_t177750891 *)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int32_t2571135199_Mark_t4050795874 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Mark_t4050795874  p1, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, *((Mark_t4050795874 *)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int32_t2571135199_UriScheme_t2622429923 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, UriScheme_t2622429923  p1, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, *((UriScheme_t2622429923 *)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int32_t2571135199_Keyframe_t3396755990 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Keyframe_t3396755990  p1, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, *((Keyframe_t3396755990 *)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int32_t2571135199_PlayableBinding_t136667248 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, PlayableBinding_t136667248  p1, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, *((PlayableBinding_t136667248 *)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int32_t2571135199_HitInfo_t184839221 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, HitInfo_t184839221  p1, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, *((HitInfo_t184839221 *)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int32_t2571135199_GcAchievementData_t3167706508 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, GcAchievementData_t3167706508  p1, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, *((GcAchievementData_t3167706508 *)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int32_t2571135199_GcScoreData_t3955401213 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, GcScoreData_t3955401213  p1, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, *((GcScoreData_t3955401213 *)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int32_t2571135199_WorkRequest_t2547429811 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, WorkRequest_t2547429811  p1, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, *((WorkRequest_t2547429811 *)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Void_t1078098495_TableRange_t4125455382 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, TableRange_t4125455382  p1, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((TableRange_t4125455382 *)args[0]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_DictionaryEntry_t1385909157 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, DictionaryEntry_t1385909157  p1, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((DictionaryEntry_t1385909157 *)args[0]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_KeyValuePair_2_t2204764377 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t2204764377  p1, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((KeyValuePair_2_t2204764377 *)args[0]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_KeyValuePair_2_t2366093889 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t2366093889  p1, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((KeyValuePair_2_t2366093889 *)args[0]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_KeyValuePair_2_t2877556189 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t2877556189  p1, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((KeyValuePair_2_t2877556189 *)args[0]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_Link_t3525875035 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Link_t3525875035  p1, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((Link_t3525875035 *)args[0]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_Slot_t2102621701 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Slot_t2102621701  p1, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((Slot_t2102621701 *)args[0]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_Slot_t1141617207 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Slot_t1141617207  p1, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((Slot_t1141617207 *)args[0]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_Decimal_t2656901032 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Decimal_t2656901032  p1, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((Decimal_t2656901032 *)args[0]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_CustomAttributeNamedArgument_t425730339 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, CustomAttributeNamedArgument_t425730339  p1, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((CustomAttributeNamedArgument_t425730339 *)args[0]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_CustomAttributeTypedArgument_t2667702721 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, CustomAttributeTypedArgument_t2667702721  p1, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((CustomAttributeTypedArgument_t2667702721 *)args[0]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_LabelData_t1059919410 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, LabelData_t1059919410  p1, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((LabelData_t1059919410 *)args[0]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_LabelFixup_t2685311059 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, LabelFixup_t2685311059  p1, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((LabelFixup_t2685311059 *)args[0]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_ILTokenInfo_t281295013 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ILTokenInfo_t281295013  p1, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((ILTokenInfo_t281295013 *)args[0]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_MonoResource_t2965441204 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, MonoResource_t2965441204  p1, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((MonoResource_t2965441204 *)args[0]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_RefEmitPermissionSet_t945207422 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RefEmitPermissionSet_t945207422  p1, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((RefEmitPermissionSet_t945207422 *)args[0]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_ParameterModifier_t969812002 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ParameterModifier_t969812002  p1, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((ParameterModifier_t969812002 *)args[0]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_ResourceCacheItem_t3632973466 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ResourceCacheItem_t3632973466  p1, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((ResourceCacheItem_t3632973466 *)args[0]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_ResourceInfo_t1773833009 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ResourceInfo_t1773833009  p1, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((ResourceInfo_t1773833009 *)args[0]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_X509ChainStatus_t177750891 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, X509ChainStatus_t177750891  p1, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((X509ChainStatus_t177750891 *)args[0]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_Mark_t4050795874 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Mark_t4050795874  p1, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((Mark_t4050795874 *)args[0]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_UriScheme_t2622429923 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UriScheme_t2622429923  p1, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((UriScheme_t2622429923 *)args[0]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_Keyframe_t3396755990 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Keyframe_t3396755990  p1, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((Keyframe_t3396755990 *)args[0]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_PlayableBinding_t136667248 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, PlayableBinding_t136667248  p1, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((PlayableBinding_t136667248 *)args[0]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_HitInfo_t184839221 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, HitInfo_t184839221  p1, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((HitInfo_t184839221 *)args[0]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_GcAchievementData_t3167706508 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, GcAchievementData_t3167706508  p1, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((GcAchievementData_t3167706508 *)args[0]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_WorkRequest_t2547429811 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, WorkRequest_t2547429811  p1, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((WorkRequest_t2547429811 *)args[0]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_Int32_t2571135199_TableRange_t4125455382 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, TableRange_t4125455382  p2, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((int32_t*)args[0]), *((TableRange_t4125455382 *)args[1]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_Int32_t2571135199_DictionaryEntry_t1385909157 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, DictionaryEntry_t1385909157  p2, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((int32_t*)args[0]), *((DictionaryEntry_t1385909157 *)args[1]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_Int32_t2571135199_KeyValuePair_2_t2204764377 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, KeyValuePair_2_t2204764377  p2, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((int32_t*)args[0]), *((KeyValuePair_2_t2204764377 *)args[1]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_Int32_t2571135199_KeyValuePair_2_t2366093889 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, KeyValuePair_2_t2366093889  p2, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((int32_t*)args[0]), *((KeyValuePair_2_t2366093889 *)args[1]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_Int32_t2571135199_KeyValuePair_2_t2877556189 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, KeyValuePair_2_t2877556189  p2, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((int32_t*)args[0]), *((KeyValuePair_2_t2877556189 *)args[1]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_Int32_t2571135199_KeyValuePair_2_t3602437993 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, KeyValuePair_2_t3602437993  p2, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((int32_t*)args[0]), *((KeyValuePair_2_t3602437993 *)args[1]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_Int32_t2571135199_Link_t3525875035 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Link_t3525875035  p2, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((int32_t*)args[0]), *((Link_t3525875035 *)args[1]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_Int32_t2571135199_Slot_t2102621701 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Slot_t2102621701  p2, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((int32_t*)args[0]), *((Slot_t2102621701 *)args[1]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_Int32_t2571135199_Slot_t1141617207 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Slot_t1141617207  p2, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((int32_t*)args[0]), *((Slot_t1141617207 *)args[1]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_Int32_t2571135199_DateTime_t507798353 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, DateTime_t507798353  p2, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((int32_t*)args[0]), *((DateTime_t507798353 *)args[1]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_Int32_t2571135199_Decimal_t2656901032 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Decimal_t2656901032  p2, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((int32_t*)args[0]), *((Decimal_t2656901032 *)args[1]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_Int32_t2571135199_Double_t1454265465 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, double p2, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((int32_t*)args[0]), *((double*)args[1]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_Int32_t2571135199_IntPtr_t (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, intptr_t p2, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((int32_t*)args[0]), *((intptr_t*)args[1]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_Int32_t2571135199_CustomAttributeNamedArgument_t425730339 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, CustomAttributeNamedArgument_t425730339  p2, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((int32_t*)args[0]), *((CustomAttributeNamedArgument_t425730339 *)args[1]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_Int32_t2571135199_CustomAttributeTypedArgument_t2667702721 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, CustomAttributeTypedArgument_t2667702721  p2, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((int32_t*)args[0]), *((CustomAttributeTypedArgument_t2667702721 *)args[1]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_Int32_t2571135199_LabelData_t1059919410 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, LabelData_t1059919410  p2, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((int32_t*)args[0]), *((LabelData_t1059919410 *)args[1]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_Int32_t2571135199_LabelFixup_t2685311059 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, LabelFixup_t2685311059  p2, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((int32_t*)args[0]), *((LabelFixup_t2685311059 *)args[1]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_Int32_t2571135199_ILTokenInfo_t281295013 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, ILTokenInfo_t281295013  p2, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((int32_t*)args[0]), *((ILTokenInfo_t281295013 *)args[1]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_Int32_t2571135199_MonoResource_t2965441204 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, MonoResource_t2965441204  p2, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((int32_t*)args[0]), *((MonoResource_t2965441204 *)args[1]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_Int32_t2571135199_RefEmitPermissionSet_t945207422 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, RefEmitPermissionSet_t945207422  p2, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((int32_t*)args[0]), *((RefEmitPermissionSet_t945207422 *)args[1]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_Int32_t2571135199_ParameterModifier_t969812002 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, ParameterModifier_t969812002  p2, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((int32_t*)args[0]), *((ParameterModifier_t969812002 *)args[1]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_Int32_t2571135199_ResourceCacheItem_t3632973466 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, ResourceCacheItem_t3632973466  p2, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((int32_t*)args[0]), *((ResourceCacheItem_t3632973466 *)args[1]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_Int32_t2571135199_ResourceInfo_t1773833009 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, ResourceInfo_t1773833009  p2, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((int32_t*)args[0]), *((ResourceInfo_t1773833009 *)args[1]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_Int32_t2571135199_Byte_t880488320 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, uint8_t p2, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((int32_t*)args[0]), *((uint8_t*)args[1]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_Int32_t2571135199_X509ChainStatus_t177750891 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, X509ChainStatus_t177750891  p2, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((int32_t*)args[0]), *((X509ChainStatus_t177750891 *)args[1]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_Int32_t2571135199_Mark_t4050795874 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Mark_t4050795874  p2, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((int32_t*)args[0]), *((Mark_t4050795874 *)args[1]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_Int32_t2571135199_TimeSpan_t2986675223 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, TimeSpan_t2986675223  p2, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((int32_t*)args[0]), *((TimeSpan_t2986675223 *)args[1]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_Int32_t2571135199_UriScheme_t2622429923 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, UriScheme_t2622429923  p2, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((int32_t*)args[0]), *((UriScheme_t2622429923 *)args[1]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_Int32_t2571135199_Keyframe_t3396755990 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Keyframe_t3396755990  p2, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((int32_t*)args[0]), *((Keyframe_t3396755990 *)args[1]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_Int32_t2571135199_PlayableBinding_t136667248 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, PlayableBinding_t136667248  p2, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((int32_t*)args[0]), *((PlayableBinding_t136667248 *)args[1]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_Int32_t2571135199_GcAchievementData_t3167706508 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, GcAchievementData_t3167706508  p2, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((int32_t*)args[0]), *((GcAchievementData_t3167706508 *)args[1]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_Int32_t2571135199_GcScoreData_t3955401213 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, GcScoreData_t3955401213  p2, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((int32_t*)args[0]), *((GcScoreData_t3955401213 *)args[1]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_Int32_t2571135199_WorkRequest_t2547429811 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, WorkRequest_t2547429811  p2, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((int32_t*)args[0]), *((WorkRequest_t2547429811 *)args[1]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_Int32U5BU5DU26_t463651626_Int32_t2571135199_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Int32U5BU5D_t2856113350** p1, int32_t p2, int32_t p3, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (Int32U5BU5D_t2856113350**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_Int32U5BU5DU26_t463651626_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Int32U5BU5D_t2856113350** p1, int32_t p2, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (Int32U5BU5D_t2856113350**)args[0], *((int32_t*)args[1]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_CustomAttributeNamedArgumentU5BU5DU26_t4027732446_Int32_t2571135199_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, CustomAttributeNamedArgumentU5BU5D_t3127017202** p1, int32_t p2, int32_t p3, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (CustomAttributeNamedArgumentU5BU5D_t3127017202**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_CustomAttributeNamedArgumentU5BU5DU26_t4027732446_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, CustomAttributeNamedArgumentU5BU5D_t3127017202** p1, int32_t p2, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (CustomAttributeNamedArgumentU5BU5D_t3127017202**)args[0], *((int32_t*)args[1]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_CustomAttributeTypedArgumentU5BU5DU26_t2921661508_Int32_t2571135199_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, CustomAttributeTypedArgumentU5BU5D_t2668173852** p1, int32_t p2, int32_t p3, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (CustomAttributeTypedArgumentU5BU5D_t2668173852**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Void_t1078098495_CustomAttributeTypedArgumentU5BU5DU26_t2921661508_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, CustomAttributeTypedArgumentU5BU5D_t2668173852** p1, int32_t p2, const RuntimeMethod* method);
	((Func)methodPointer)(obj, (CustomAttributeTypedArgumentU5BU5D_t2668173852**)args[0], *((int32_t*)args[1]), methodMetadata);
	return NULL;
}

void* RuntimeInvoker_TableRange_t4125455382_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef TableRange_t4125455382  (*Func)(void* obj, int32_t p1, const RuntimeMethod* method);
	TableRange_t4125455382  ret = ((Func)methodPointer)(obj, *((int32_t*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_ClientCertificateType_t194976790_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, *((int32_t*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_DictionaryEntry_t1385909157_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef DictionaryEntry_t1385909157  (*Func)(void* obj, int32_t p1, const RuntimeMethod* method);
	DictionaryEntry_t1385909157  ret = ((Func)methodPointer)(obj, *((int32_t*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t2204764377_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef KeyValuePair_2_t2204764377  (*Func)(void* obj, int32_t p1, const RuntimeMethod* method);
	KeyValuePair_2_t2204764377  ret = ((Func)methodPointer)(obj, *((int32_t*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t2366093889_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef KeyValuePair_2_t2366093889  (*Func)(void* obj, int32_t p1, const RuntimeMethod* method);
	KeyValuePair_2_t2366093889  ret = ((Func)methodPointer)(obj, *((int32_t*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t2877556189_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef KeyValuePair_2_t2877556189  (*Func)(void* obj, int32_t p1, const RuntimeMethod* method);
	KeyValuePair_2_t2877556189  ret = ((Func)methodPointer)(obj, *((int32_t*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t3602437993_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef KeyValuePair_2_t3602437993  (*Func)(void* obj, int32_t p1, const RuntimeMethod* method);
	KeyValuePair_2_t3602437993  ret = ((Func)methodPointer)(obj, *((int32_t*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Link_t3525875035_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef Link_t3525875035  (*Func)(void* obj, int32_t p1, const RuntimeMethod* method);
	Link_t3525875035  ret = ((Func)methodPointer)(obj, *((int32_t*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Slot_t2102621701_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef Slot_t2102621701  (*Func)(void* obj, int32_t p1, const RuntimeMethod* method);
	Slot_t2102621701  ret = ((Func)methodPointer)(obj, *((int32_t*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Slot_t1141617207_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef Slot_t1141617207  (*Func)(void* obj, int32_t p1, const RuntimeMethod* method);
	Slot_t1141617207  ret = ((Func)methodPointer)(obj, *((int32_t*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_CustomAttributeNamedArgument_t425730339_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef CustomAttributeNamedArgument_t425730339  (*Func)(void* obj, int32_t p1, const RuntimeMethod* method);
	CustomAttributeNamedArgument_t425730339  ret = ((Func)methodPointer)(obj, *((int32_t*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_CustomAttributeTypedArgument_t2667702721_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef CustomAttributeTypedArgument_t2667702721  (*Func)(void* obj, int32_t p1, const RuntimeMethod* method);
	CustomAttributeTypedArgument_t2667702721  ret = ((Func)methodPointer)(obj, *((int32_t*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_LabelData_t1059919410_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef LabelData_t1059919410  (*Func)(void* obj, int32_t p1, const RuntimeMethod* method);
	LabelData_t1059919410  ret = ((Func)methodPointer)(obj, *((int32_t*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_LabelFixup_t2685311059_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef LabelFixup_t2685311059  (*Func)(void* obj, int32_t p1, const RuntimeMethod* method);
	LabelFixup_t2685311059  ret = ((Func)methodPointer)(obj, *((int32_t*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_ILTokenInfo_t281295013_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef ILTokenInfo_t281295013  (*Func)(void* obj, int32_t p1, const RuntimeMethod* method);
	ILTokenInfo_t281295013  ret = ((Func)methodPointer)(obj, *((int32_t*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_MonoResource_t2965441204_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef MonoResource_t2965441204  (*Func)(void* obj, int32_t p1, const RuntimeMethod* method);
	MonoResource_t2965441204  ret = ((Func)methodPointer)(obj, *((int32_t*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_RefEmitPermissionSet_t945207422_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RefEmitPermissionSet_t945207422  (*Func)(void* obj, int32_t p1, const RuntimeMethod* method);
	RefEmitPermissionSet_t945207422  ret = ((Func)methodPointer)(obj, *((int32_t*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_ParameterModifier_t969812002_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef ParameterModifier_t969812002  (*Func)(void* obj, int32_t p1, const RuntimeMethod* method);
	ParameterModifier_t969812002  ret = ((Func)methodPointer)(obj, *((int32_t*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_ResourceCacheItem_t3632973466_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef ResourceCacheItem_t3632973466  (*Func)(void* obj, int32_t p1, const RuntimeMethod* method);
	ResourceCacheItem_t3632973466  ret = ((Func)methodPointer)(obj, *((int32_t*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_ResourceInfo_t1773833009_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef ResourceInfo_t1773833009  (*Func)(void* obj, int32_t p1, const RuntimeMethod* method);
	ResourceInfo_t1773833009  ret = ((Func)methodPointer)(obj, *((int32_t*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_TypeTag_t1137063594_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, int32_t p1, const RuntimeMethod* method);
	uint8_t ret = ((Func)methodPointer)(obj, *((int32_t*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_X509ChainStatus_t177750891_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef X509ChainStatus_t177750891  (*Func)(void* obj, int32_t p1, const RuntimeMethod* method);
	X509ChainStatus_t177750891  ret = ((Func)methodPointer)(obj, *((int32_t*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Mark_t4050795874_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef Mark_t4050795874  (*Func)(void* obj, int32_t p1, const RuntimeMethod* method);
	Mark_t4050795874  ret = ((Func)methodPointer)(obj, *((int32_t*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_TimeSpan_t2986675223_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef TimeSpan_t2986675223  (*Func)(void* obj, int32_t p1, const RuntimeMethod* method);
	TimeSpan_t2986675223  ret = ((Func)methodPointer)(obj, *((int32_t*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_UriScheme_t2622429923_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef UriScheme_t2622429923  (*Func)(void* obj, int32_t p1, const RuntimeMethod* method);
	UriScheme_t2622429923  ret = ((Func)methodPointer)(obj, *((int32_t*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Keyframe_t3396755990_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef Keyframe_t3396755990  (*Func)(void* obj, int32_t p1, const RuntimeMethod* method);
	Keyframe_t3396755990  ret = ((Func)methodPointer)(obj, *((int32_t*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_PlayableBinding_t136667248_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef PlayableBinding_t136667248  (*Func)(void* obj, int32_t p1, const RuntimeMethod* method);
	PlayableBinding_t136667248  ret = ((Func)methodPointer)(obj, *((int32_t*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_HitInfo_t184839221_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef HitInfo_t184839221  (*Func)(void* obj, int32_t p1, const RuntimeMethod* method);
	HitInfo_t184839221  ret = ((Func)methodPointer)(obj, *((int32_t*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_GcAchievementData_t3167706508_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef GcAchievementData_t3167706508  (*Func)(void* obj, int32_t p1, const RuntimeMethod* method);
	GcAchievementData_t3167706508  ret = ((Func)methodPointer)(obj, *((int32_t*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_GcScoreData_t3955401213_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef GcScoreData_t3955401213  (*Func)(void* obj, int32_t p1, const RuntimeMethod* method);
	GcScoreData_t3955401213  ret = ((Func)methodPointer)(obj, *((int32_t*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_WorkRequest_t2547429811_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef WorkRequest_t2547429811  (*Func)(void* obj, int32_t p1, const RuntimeMethod* method);
	WorkRequest_t2547429811  ret = ((Func)methodPointer)(obj, *((int32_t*)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_RuntimeObject_SByte_t2530277047_RuntimeObject_RuntimeObject_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, int8_t p1, RuntimeObject * p2, RuntimeObject * p3, RuntimeObject * p4, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, *((int8_t*)args[0]), (RuntimeObject *)args[1], (RuntimeObject *)args[2], (RuntimeObject *)args[3], methodMetadata);
	return ret;
}

void* RuntimeInvoker_CustomAttributeNamedArgument_t425730339 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef CustomAttributeNamedArgument_t425730339  (*Func)(void* obj, const RuntimeMethod* method);
	CustomAttributeNamedArgument_t425730339  ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_CustomAttributeTypedArgument_t2667702721 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef CustomAttributeTypedArgument_t2667702721  (*Func)(void* obj, const RuntimeMethod* method);
	CustomAttributeTypedArgument_t2667702721  ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_TableRange_t4125455382 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef TableRange_t4125455382  (*Func)(void* obj, const RuntimeMethod* method);
	TableRange_t4125455382  ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_ClientCertificateType_t194976790 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t2204764377 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef KeyValuePair_2_t2204764377  (*Func)(void* obj, const RuntimeMethod* method);
	KeyValuePair_2_t2204764377  ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t2366093889 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef KeyValuePair_2_t2366093889  (*Func)(void* obj, const RuntimeMethod* method);
	KeyValuePair_2_t2366093889  ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t2877556189 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef KeyValuePair_2_t2877556189  (*Func)(void* obj, const RuntimeMethod* method);
	KeyValuePair_2_t2877556189  ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Link_t3525875035 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef Link_t3525875035  (*Func)(void* obj, const RuntimeMethod* method);
	Link_t3525875035  ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Slot_t2102621701 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef Slot_t2102621701  (*Func)(void* obj, const RuntimeMethod* method);
	Slot_t2102621701  ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Slot_t1141617207 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef Slot_t1141617207  (*Func)(void* obj, const RuntimeMethod* method);
	Slot_t1141617207  ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_LabelData_t1059919410 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef LabelData_t1059919410  (*Func)(void* obj, const RuntimeMethod* method);
	LabelData_t1059919410  ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_LabelFixup_t2685311059 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef LabelFixup_t2685311059  (*Func)(void* obj, const RuntimeMethod* method);
	LabelFixup_t2685311059  ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_ILTokenInfo_t281295013 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef ILTokenInfo_t281295013  (*Func)(void* obj, const RuntimeMethod* method);
	ILTokenInfo_t281295013  ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_MonoResource_t2965441204 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef MonoResource_t2965441204  (*Func)(void* obj, const RuntimeMethod* method);
	MonoResource_t2965441204  ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_RefEmitPermissionSet_t945207422 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RefEmitPermissionSet_t945207422  (*Func)(void* obj, const RuntimeMethod* method);
	RefEmitPermissionSet_t945207422  ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_ParameterModifier_t969812002 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef ParameterModifier_t969812002  (*Func)(void* obj, const RuntimeMethod* method);
	ParameterModifier_t969812002  ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_ResourceCacheItem_t3632973466 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef ResourceCacheItem_t3632973466  (*Func)(void* obj, const RuntimeMethod* method);
	ResourceCacheItem_t3632973466  ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_ResourceInfo_t1773833009 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef ResourceInfo_t1773833009  (*Func)(void* obj, const RuntimeMethod* method);
	ResourceInfo_t1773833009  ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_TypeTag_t1137063594 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, const RuntimeMethod* method);
	uint8_t ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_X509ChainStatus_t177750891 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef X509ChainStatus_t177750891  (*Func)(void* obj, const RuntimeMethod* method);
	X509ChainStatus_t177750891  ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Mark_t4050795874 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef Mark_t4050795874  (*Func)(void* obj, const RuntimeMethod* method);
	Mark_t4050795874  ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_UriScheme_t2622429923 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef UriScheme_t2622429923  (*Func)(void* obj, const RuntimeMethod* method);
	UriScheme_t2622429923  ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Keyframe_t3396755990 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef Keyframe_t3396755990  (*Func)(void* obj, const RuntimeMethod* method);
	Keyframe_t3396755990  ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_PlayableBinding_t136667248 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef PlayableBinding_t136667248  (*Func)(void* obj, const RuntimeMethod* method);
	PlayableBinding_t136667248  ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_HitInfo_t184839221 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef HitInfo_t184839221  (*Func)(void* obj, const RuntimeMethod* method);
	HitInfo_t184839221  ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_GcAchievementData_t3167706508 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef GcAchievementData_t3167706508  (*Func)(void* obj, const RuntimeMethod* method);
	GcAchievementData_t3167706508  ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_GcScoreData_t3955401213 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef GcScoreData_t3955401213  (*Func)(void* obj, const RuntimeMethod* method);
	GcScoreData_t3955401213  ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int32_t2571135199_DateTimeOffset_t2220763929_DateTimeOffset_t2220763929 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, DateTimeOffset_t2220763929  p1, DateTimeOffset_t2220763929  p2, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, *((DateTimeOffset_t2220763929 *)args[0]), *((DateTimeOffset_t2220763929 *)args[1]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Int32_t2571135199_Guid_t_Guid_t (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Guid_t  p1, Guid_t  p2, const RuntimeMethod* method);
	int32_t ret = ((Func)methodPointer)(obj, *((Guid_t *)args[0]), *((Guid_t *)args[1]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_DictionaryEntry_t1385909157_IntPtr_t_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef DictionaryEntry_t1385909157  (*Func)(void* obj, intptr_t p1, RuntimeObject * p2, const RuntimeMethod* method);
	DictionaryEntry_t1385909157  ret = ((Func)methodPointer)(obj, *((intptr_t*)args[0]), (RuntimeObject *)args[1], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_RuntimeObject_IntPtr_t_RuntimeObject_RuntimeObject_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, intptr_t p1, RuntimeObject * p2, RuntimeObject * p3, RuntimeObject * p4, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, *((intptr_t*)args[0]), (RuntimeObject *)args[1], (RuntimeObject *)args[2], (RuntimeObject *)args[3], methodMetadata);
	return ret;
}

void* RuntimeInvoker_DictionaryEntry_t1385909157_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef DictionaryEntry_t1385909157  (*Func)(void* obj, RuntimeObject * p1, const RuntimeMethod* method);
	DictionaryEntry_t1385909157  ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t2204764377_IntPtr_t_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef KeyValuePair_2_t2204764377  (*Func)(void* obj, intptr_t p1, RuntimeObject * p2, const RuntimeMethod* method);
	KeyValuePair_2_t2204764377  ret = ((Func)methodPointer)(obj, *((intptr_t*)args[0]), (RuntimeObject *)args[1], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t2204764377_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef KeyValuePair_2_t2204764377  (*Func)(void* obj, RuntimeObject * p1, const RuntimeMethod* method);
	KeyValuePair_2_t2204764377  ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_DictionaryEntry_t1385909157_RuntimeObject_SByte_t2530277047 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef DictionaryEntry_t1385909157  (*Func)(void* obj, RuntimeObject * p1, int8_t p2, const RuntimeMethod* method);
	DictionaryEntry_t1385909157  ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int8_t*)args[1]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_RuntimeObject_RuntimeObject_SByte_t2530277047_RuntimeObject_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, RuntimeObject * p1, int8_t p2, RuntimeObject * p3, RuntimeObject * p4, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int8_t*)args[1]), (RuntimeObject *)args[2], (RuntimeObject *)args[3], methodMetadata);
	return ret;
}

void* RuntimeInvoker_KeyValuePair_2_t2366093889_RuntimeObject_SByte_t2530277047 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef KeyValuePair_2_t2366093889  (*Func)(void* obj, RuntimeObject * p1, int8_t p2, const RuntimeMethod* method);
	KeyValuePair_2_t2366093889  ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int8_t*)args[1]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t2366093889_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef KeyValuePair_2_t2366093889  (*Func)(void* obj, RuntimeObject * p1, const RuntimeMethod* method);
	KeyValuePair_2_t2366093889  ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_DictionaryEntry_t1385909157_RuntimeObject_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef DictionaryEntry_t1385909157  (*Func)(void* obj, RuntimeObject * p1, int32_t p2, const RuntimeMethod* method);
	DictionaryEntry_t1385909157  ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t2877556189_RuntimeObject_Int32_t2571135199 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef KeyValuePair_2_t2877556189  (*Func)(void* obj, RuntimeObject * p1, int32_t p2, const RuntimeMethod* method);
	KeyValuePair_2_t2877556189  ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], *((int32_t*)args[1]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t2877556189_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef KeyValuePair_2_t2877556189  (*Func)(void* obj, RuntimeObject * p1, const RuntimeMethod* method);
	KeyValuePair_2_t2877556189  ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t3602437993_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef KeyValuePair_2_t3602437993  (*Func)(void* obj, RuntimeObject * p1, const RuntimeMethod* method);
	KeyValuePair_2_t3602437993  ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Void_t1078098495_IntPtr_t_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef void (*Func)(void* obj, intptr_t p1, RuntimeObject * p2, const RuntimeMethod* method);
	((Func)methodPointer)(obj, *((intptr_t*)args[0]), (RuntimeObject *)args[1], methodMetadata);
	return NULL;
}

void* RuntimeInvoker_Enumerator_t767066857 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef Enumerator_t767066857  (*Func)(void* obj, const RuntimeMethod* method);
	Enumerator_t767066857  ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_RuntimeObject_BooleanU26_t495120821 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, RuntimeObject * p1, bool* p2, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], (bool*)args[1], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Enumerator_t928396369 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef Enumerator_t928396369  (*Func)(void* obj, const RuntimeMethod* method);
	Enumerator_t928396369  ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Enumerator_t1439858669 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef Enumerator_t1439858669  (*Func)(void* obj, const RuntimeMethod* method);
	Enumerator_t1439858669  ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_SByte_t2530277047_SByte_t2530277047 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int8_t p1, int8_t p2, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_DateTimeOffset_t2220763929_DateTimeOffset_t2220763929 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, DateTimeOffset_t2220763929  p1, DateTimeOffset_t2220763929  p2, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, *((DateTimeOffset_t2220763929 *)args[0]), *((DateTimeOffset_t2220763929 *)args[1]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_CustomAttributeNamedArgument_t425730339_CustomAttributeNamedArgument_t425730339 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, CustomAttributeNamedArgument_t425730339  p1, CustomAttributeNamedArgument_t425730339  p2, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, *((CustomAttributeNamedArgument_t425730339 *)args[0]), *((CustomAttributeNamedArgument_t425730339 *)args[1]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_CustomAttributeTypedArgument_t2667702721_CustomAttributeTypedArgument_t2667702721 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, CustomAttributeTypedArgument_t2667702721  p1, CustomAttributeTypedArgument_t2667702721  p2, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, *((CustomAttributeTypedArgument_t2667702721 *)args[0]), *((CustomAttributeTypedArgument_t2667702721 *)args[1]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Enumerator_t2209518572 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef Enumerator_t2209518572  (*Func)(void* obj, const RuntimeMethod* method);
	Enumerator_t2209518572  ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Enumerator_t64113712 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef Enumerator_t64113712  (*Func)(void* obj, const RuntimeMethod* method);
	Enumerator_t64113712  ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Enumerator_t2306086094 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef Enumerator_t2306086094  (*Func)(void* obj, const RuntimeMethod* method);
	Enumerator_t2306086094  ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Enumerator_t3479511895 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef Enumerator_t3479511895  (*Func)(void* obj, const RuntimeMethod* method);
	Enumerator_t3479511895  ret = ((Func)methodPointer)(obj, methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_CustomAttributeNamedArgument_t425730339_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef CustomAttributeNamedArgument_t425730339  (*Func)(void* obj, RuntimeObject * p1, const RuntimeMethod* method);
	CustomAttributeNamedArgument_t425730339  ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_CustomAttributeTypedArgument_t2667702721_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef CustomAttributeTypedArgument_t2667702721  (*Func)(void* obj, RuntimeObject * p1, const RuntimeMethod* method);
	CustomAttributeTypedArgument_t2667702721  ret = ((Func)methodPointer)(obj, (RuntimeObject *)args[0], methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_Boolean_t2059672899_Nullable_1_t950977049 (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Nullable_1_t950977049  p1, const RuntimeMethod* method);
	bool ret = ((Func)methodPointer)(obj, *((Nullable_1_t950977049 *)args[0]), methodMetadata);
	return Box(il2cpp_codegen_class_from_type (il2cpp_codegen_method_return_type(methodMetadata)), &ret);
}

void* RuntimeInvoker_RuntimeObject_Single_t496865882_RuntimeObject_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, float p1, RuntimeObject * p2, RuntimeObject * p3, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, *((float*)args[0]), (RuntimeObject *)args[1], (RuntimeObject *)args[2], methodMetadata);
	return ret;
}

void* RuntimeInvoker_RuntimeObject_Scene_t3023997566_RuntimeObject_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, Scene_t3023997566  p1, RuntimeObject * p2, RuntimeObject * p3, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, *((Scene_t3023997566 *)args[0]), (RuntimeObject *)args[1], (RuntimeObject *)args[2], methodMetadata);
	return ret;
}

void* RuntimeInvoker_RuntimeObject_Scene_t3023997566_Int32_t2571135199_RuntimeObject_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, Scene_t3023997566  p1, int32_t p2, RuntimeObject * p3, RuntimeObject * p4, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, *((Scene_t3023997566 *)args[0]), *((int32_t*)args[1]), (RuntimeObject *)args[2], (RuntimeObject *)args[3], methodMetadata);
	return ret;
}

void* RuntimeInvoker_RuntimeObject_Scene_t3023997566_Scene_t3023997566_RuntimeObject_RuntimeObject (Il2CppMethodPointer methodPointer, const RuntimeMethod* methodMetadata, void* obj, void** args)
{
	typedef RuntimeObject * (*Func)(void* obj, Scene_t3023997566  p1, Scene_t3023997566  p2, RuntimeObject * p3, RuntimeObject * p4, const RuntimeMethod* method);
	RuntimeObject * ret = ((Func)methodPointer)(obj, *((Scene_t3023997566 *)args[0]), *((Scene_t3023997566 *)args[1]), (RuntimeObject *)args[2], (RuntimeObject *)args[3], methodMetadata);
	return ret;
}

extern const InvokerMethod g_Il2CppInvokerPointers[1087] = 
{
	RuntimeInvoker_Void_t1078098495,
	RuntimeInvoker_Boolean_t2059672899_RuntimeObject,
	RuntimeInvoker_Boolean_t2059672899_RuntimeObject_RuntimeObject,
	RuntimeInvoker_Int32_t2571135199,
	RuntimeInvoker_RuntimeObject,
	RuntimeInvoker_Int32_t2571135199_RuntimeObject,
	RuntimeInvoker_Boolean_t2059672899_RuntimeObject_RuntimeObject_ObjectU5BU5DU26_t3608108294,
	RuntimeInvoker_Int32_t2571135199_RuntimeObject_ObjectU5BU5DU26_t3608108294,
	RuntimeInvoker_Void_t1078098495_RuntimeObject_RuntimeObject,
	RuntimeInvoker_RuntimeObject_RuntimeObject_RuntimeObject,
	RuntimeInvoker_RuntimeObject_RuntimeObject_RuntimeObject_SByte_t2530277047,
	RuntimeInvoker_Boolean_t2059672899_RuntimeObject_RuntimeObject_SByte_t2530277047,
	RuntimeInvoker_Byte_t880488320_RuntimeObject,
	RuntimeInvoker_Char_t3572527572_RuntimeObject,
	RuntimeInvoker_DateTime_t507798353_RuntimeObject,
	RuntimeInvoker_Decimal_t2656901032_RuntimeObject,
	RuntimeInvoker_Double_t1454265465_RuntimeObject,
	RuntimeInvoker_Int16_t769030278_RuntimeObject,
	RuntimeInvoker_Int64_t384086013_RuntimeObject,
	RuntimeInvoker_SByte_t2530277047_RuntimeObject,
	RuntimeInvoker_Single_t496865882_RuntimeObject,
	RuntimeInvoker_UInt16_t11709673_RuntimeObject,
	RuntimeInvoker_UInt32_t2739056616_RuntimeObject,
	RuntimeInvoker_UInt64_t2265270229_RuntimeObject,
	RuntimeInvoker_Int32_t2571135199_Int32_t2571135199,
	RuntimeInvoker_Boolean_t2059672899_Int32_t2571135199,
	RuntimeInvoker_Boolean_t2059672899_SByte_t2530277047_RuntimeObject_Int32_t2571135199_ExceptionU26_t1670763973,
	RuntimeInvoker_Boolean_t2059672899_RuntimeObject_SByte_t2530277047_Int32U26_t2596978809_ExceptionU26_t1670763973,
	RuntimeInvoker_Int32_t2571135199_RuntimeObject_RuntimeObject,
	RuntimeInvoker_Boolean_t2059672899_Int32_t2571135199_SByte_t2530277047_ExceptionU26_t1670763973,
	RuntimeInvoker_Boolean_t2059672899_Int32U26_t2596978809_RuntimeObject_SByte_t2530277047_SByte_t2530277047_ExceptionU26_t1670763973,
	RuntimeInvoker_Void_t1078098495_Int32U26_t2596978809_RuntimeObject_RuntimeObject_BooleanU26_t495120821_BooleanU26_t495120821,
	RuntimeInvoker_Void_t1078098495_Int32U26_t2596978809_RuntimeObject_RuntimeObject_BooleanU26_t495120821,
	RuntimeInvoker_Boolean_t2059672899_Int32U26_t2596978809_RuntimeObject_Int32U26_t2596978809_SByte_t2530277047_ExceptionU26_t1670763973,
	RuntimeInvoker_Boolean_t2059672899_Int32U26_t2596978809_RuntimeObject_RuntimeObject,
	RuntimeInvoker_Boolean_t2059672899_Int16_t769030278_SByte_t2530277047,
	RuntimeInvoker_Boolean_t2059672899_RuntimeObject_Int32_t2571135199_RuntimeObject_SByte_t2530277047_Int32U26_t2596978809_ExceptionU26_t1670763973,
	RuntimeInvoker_Int32_t2571135199_RuntimeObject_Int32_t2571135199_RuntimeObject,
	RuntimeInvoker_Boolean_t2059672899_RuntimeObject_Int32U26_t2596978809,
	RuntimeInvoker_Boolean_t2059672899_RuntimeObject_Int32_t2571135199_RuntimeObject_Int32U26_t2596978809,
	RuntimeInvoker_RuntimeObject_RuntimeObject,
	RuntimeInvoker_Void_t1078098495_Int32_t2571135199,
	RuntimeInvoker_Boolean_t2059672899,
	RuntimeInvoker_Void_t1078098495_SByte_t2530277047,
	RuntimeInvoker_Int32_t2571135199_Int64_t384086013,
	RuntimeInvoker_Boolean_t2059672899_Int64_t384086013,
	RuntimeInvoker_Boolean_t2059672899_RuntimeObject_SByte_t2530277047_Int64U26_t4038210315_ExceptionU26_t1670763973,
	RuntimeInvoker_Int64_t384086013_RuntimeObject_RuntimeObject,
	RuntimeInvoker_Boolean_t2059672899_RuntimeObject_Int32_t2571135199_RuntimeObject_SByte_t2530277047_Int64U26_t4038210315_ExceptionU26_t1670763973,
	RuntimeInvoker_Int64_t384086013_RuntimeObject_Int32_t2571135199_RuntimeObject,
	RuntimeInvoker_Boolean_t2059672899_RuntimeObject_Int64U26_t4038210315,
	RuntimeInvoker_Boolean_t2059672899_RuntimeObject_Int32_t2571135199_RuntimeObject_Int64U26_t4038210315,
	RuntimeInvoker_Boolean_t2059672899_RuntimeObject_SByte_t2530277047_UInt32U26_t3679184984_ExceptionU26_t1670763973,
	RuntimeInvoker_Boolean_t2059672899_RuntimeObject_Int32_t2571135199_RuntimeObject_SByte_t2530277047_UInt32U26_t3679184984_ExceptionU26_t1670763973,
	RuntimeInvoker_UInt32_t2739056616_RuntimeObject_Int32_t2571135199_RuntimeObject,
	RuntimeInvoker_UInt32_t2739056616_RuntimeObject_RuntimeObject,
	RuntimeInvoker_Boolean_t2059672899_RuntimeObject_UInt32U26_t3679184984,
	RuntimeInvoker_Boolean_t2059672899_RuntimeObject_Int32_t2571135199_RuntimeObject_UInt32U26_t3679184984,
	RuntimeInvoker_UInt64_t2265270229_RuntimeObject_RuntimeObject,
	RuntimeInvoker_Boolean_t2059672899_RuntimeObject_Int32_t2571135199_RuntimeObject_SByte_t2530277047_UInt64U26_t3595557107_ExceptionU26_t1670763973,
	RuntimeInvoker_UInt64_t2265270229_RuntimeObject_Int32_t2571135199_RuntimeObject,
	RuntimeInvoker_Boolean_t2059672899_RuntimeObject_UInt64U26_t3595557107,
	RuntimeInvoker_Int32_t2571135199_SByte_t2530277047,
	RuntimeInvoker_Boolean_t2059672899_SByte_t2530277047,
	RuntimeInvoker_Byte_t880488320_RuntimeObject_RuntimeObject,
	RuntimeInvoker_Byte_t880488320_RuntimeObject_Int32_t2571135199_RuntimeObject,
	RuntimeInvoker_Boolean_t2059672899_RuntimeObject_ByteU26_t3158801536,
	RuntimeInvoker_Boolean_t2059672899_RuntimeObject_Int32_t2571135199_RuntimeObject_ByteU26_t3158801536,
	RuntimeInvoker_Boolean_t2059672899_RuntimeObject_SByte_t2530277047_SByteU26_t1964499041_ExceptionU26_t1670763973,
	RuntimeInvoker_SByte_t2530277047_RuntimeObject_RuntimeObject,
	RuntimeInvoker_SByte_t2530277047_RuntimeObject_Int32_t2571135199_RuntimeObject,
	RuntimeInvoker_Boolean_t2059672899_RuntimeObject_SByteU26_t1964499041,
	RuntimeInvoker_Int32_t2571135199_Int16_t769030278,
	RuntimeInvoker_Boolean_t2059672899_Int16_t769030278,
	RuntimeInvoker_Boolean_t2059672899_RuntimeObject_SByte_t2530277047_Int16U26_t2738771306_ExceptionU26_t1670763973,
	RuntimeInvoker_Int16_t769030278_RuntimeObject_RuntimeObject,
	RuntimeInvoker_Int16_t769030278_RuntimeObject_Int32_t2571135199_RuntimeObject,
	RuntimeInvoker_Boolean_t2059672899_RuntimeObject_Int16U26_t2738771306,
	RuntimeInvoker_UInt16_t11709673_RuntimeObject_RuntimeObject,
	RuntimeInvoker_UInt16_t11709673_RuntimeObject_Int32_t2571135199_RuntimeObject,
	RuntimeInvoker_Boolean_t2059672899_RuntimeObject_UInt16U26_t1123581183,
	RuntimeInvoker_Boolean_t2059672899_RuntimeObject_Int32_t2571135199_RuntimeObject_UInt16U26_t1123581183,
	RuntimeInvoker_Void_t1078098495_ByteU2AU26_t1485094272_ByteU2AU26_t1485094272_DoubleU2AU26_t2972773821_UInt16U2AU26_t1250832877_UInt16U2AU26_t1250832877_UInt16U2AU26_t1250832877_UInt16U2AU26_t1250832877,
	RuntimeInvoker_UnicodeCategory_t1362072295_Int16_t769030278,
	RuntimeInvoker_Boolean_t2059672899_RuntimeObject_Int32_t2571135199,
	RuntimeInvoker_Void_t1078098495_RuntimeObject_Int32_t2571135199,
	RuntimeInvoker_Char_t3572527572_Int16_t769030278,
	RuntimeInvoker_Char_t3572527572_Int16_t769030278_RuntimeObject,
	RuntimeInvoker_Void_t1078098495_RuntimeObject_Int32_t2571135199_Int32_t2571135199,
	RuntimeInvoker_Void_t1078098495_RuntimeObject,
	RuntimeInvoker_Void_t1078098495_Int16_t769030278_Int32_t2571135199,
	RuntimeInvoker_Char_t3572527572_Int32_t2571135199,
	RuntimeInvoker_Void_t1078098495_Int32_t2571135199_RuntimeObject_Int32_t2571135199_Int32_t2571135199,
	RuntimeInvoker_RuntimeObject_Int32_t2571135199_Int32_t2571135199,
	RuntimeInvoker_RuntimeObject_RuntimeObject_Int32_t2571135199,
	RuntimeInvoker_RuntimeObject_RuntimeObject_Int32_t2571135199_Int32_t2571135199,
	RuntimeInvoker_RuntimeObject_Int32_t2571135199,
	RuntimeInvoker_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199,
	RuntimeInvoker_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199_RuntimeObject,
	RuntimeInvoker_Int32_t2571135199_RuntimeObject_RuntimeObject_SByte_t2530277047,
	RuntimeInvoker_Int32_t2571135199_RuntimeObject_RuntimeObject_SByte_t2530277047_RuntimeObject,
	RuntimeInvoker_Int32_t2571135199_RuntimeObject_Int32_t2571135199_RuntimeObject_Int32_t2571135199_Int32_t2571135199_SByte_t2530277047_RuntimeObject,
	RuntimeInvoker_Int32_t2571135199_RuntimeObject_Int32_t2571135199_RuntimeObject_Int32_t2571135199_Int32_t2571135199,
	RuntimeInvoker_Int32_t2571135199_RuntimeObject_Int32_t2571135199_Int32_t2571135199_RuntimeObject_Int32_t2571135199_Int32_t2571135199,
	RuntimeInvoker_Int32_t2571135199_RuntimeObject_Int32_t2571135199,
	RuntimeInvoker_Int32_t2571135199_RuntimeObject_Int32_t2571135199_Int32_t2571135199,
	RuntimeInvoker_Int32_t2571135199_RuntimeObject_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199,
	RuntimeInvoker_Int32_t2571135199_Int16_t769030278_Int32_t2571135199,
	RuntimeInvoker_Int32_t2571135199_Int16_t769030278_Int32_t2571135199_Int32_t2571135199,
	RuntimeInvoker_RuntimeObject_Int32_t2571135199_Int16_t769030278,
	RuntimeInvoker_RuntimeObject_Int16_t769030278_Int16_t769030278,
	RuntimeInvoker_RuntimeObject_RuntimeObject_RuntimeObject_Int32_t2571135199,
	RuntimeInvoker_RuntimeObject_RuntimeObject_RuntimeObject_RuntimeObject,
	RuntimeInvoker_RuntimeObject_RuntimeObject_RuntimeObject_RuntimeObject_RuntimeObject,
	RuntimeInvoker_RuntimeObject_Int32_t2571135199_RuntimeObject,
	RuntimeInvoker_RuntimeObject_RuntimeObject_RuntimeObject_Int32_t2571135199_Int32_t2571135199,
	RuntimeInvoker_Void_t1078098495_RuntimeObject_Int32U26_t2596978809_Int32U26_t2596978809_Int32U26_t2596978809_BooleanU26_t495120821_StringU26_t2155547156,
	RuntimeInvoker_Int32_t2571135199_RuntimeObject_Int32U26_t2596978809,
	RuntimeInvoker_Void_t1078098495_Int32_t2571135199_Int16_t769030278,
	RuntimeInvoker_RuntimeObject_RuntimeObject_Int32_t2571135199_Int32_t2571135199_RuntimeObject,
	RuntimeInvoker_RuntimeObject_Int16_t769030278_Int32_t2571135199,
	RuntimeInvoker_Void_t1078098495_RuntimeObject_RuntimeObject_Int32_t2571135199,
	RuntimeInvoker_Void_t1078098495_RuntimeObject_Int32_t2571135199_RuntimeObject_Int32_t2571135199_Int32_t2571135199,
	RuntimeInvoker_Int32_t2571135199_Single_t496865882,
	RuntimeInvoker_Boolean_t2059672899_Single_t496865882,
	RuntimeInvoker_Single_t496865882_RuntimeObject_RuntimeObject,
	RuntimeInvoker_Int32_t2571135199_Double_t1454265465,
	RuntimeInvoker_Boolean_t2059672899_Double_t1454265465,
	RuntimeInvoker_Double_t1454265465_RuntimeObject_RuntimeObject,
	RuntimeInvoker_Double_t1454265465_RuntimeObject_Int32_t2571135199_RuntimeObject,
	RuntimeInvoker_Boolean_t2059672899_RuntimeObject_Int32_t2571135199_RuntimeObject_SByte_t2530277047_DoubleU26_t1440668655_ExceptionU26_t1670763973,
	RuntimeInvoker_Boolean_t2059672899_RuntimeObject_RuntimeObject_Int32_t2571135199_Int32_t2571135199,
	RuntimeInvoker_Boolean_t2059672899_RuntimeObject_DoubleU26_t1440668655,
	RuntimeInvoker_Void_t1078098495_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199_SByte_t2530277047_SByte_t2530277047,
	RuntimeInvoker_Void_t1078098495_Int64_t384086013,
	RuntimeInvoker_Void_t1078098495_Single_t496865882,
	RuntimeInvoker_Void_t1078098495_Double_t1454265465,
	RuntimeInvoker_RuntimeObject_Decimal_t2656901032,
	RuntimeInvoker_Decimal_t2656901032_Decimal_t2656901032_Decimal_t2656901032,
	RuntimeInvoker_UInt64_t2265270229_Decimal_t2656901032,
	RuntimeInvoker_Int64_t384086013_Decimal_t2656901032,
	RuntimeInvoker_Boolean_t2059672899_Decimal_t2656901032_Decimal_t2656901032,
	RuntimeInvoker_Decimal_t2656901032_Decimal_t2656901032,
	RuntimeInvoker_Int32_t2571135199_Decimal_t2656901032_Decimal_t2656901032,
	RuntimeInvoker_Int32_t2571135199_Decimal_t2656901032,
	RuntimeInvoker_Boolean_t2059672899_Decimal_t2656901032,
	RuntimeInvoker_Decimal_t2656901032_RuntimeObject_RuntimeObject,
	RuntimeInvoker_RuntimeObject_RuntimeObject_Int32_t2571135199_RuntimeObject_Int32U26_t2596978809_BooleanU26_t495120821_BooleanU26_t495120821_Int32U26_t2596978809_SByte_t2530277047,
	RuntimeInvoker_Decimal_t2656901032_RuntimeObject_Int32_t2571135199_RuntimeObject,
	RuntimeInvoker_Boolean_t2059672899_RuntimeObject_Int32_t2571135199_RuntimeObject_DecimalU26_t3838531224_SByte_t2530277047,
	RuntimeInvoker_Int32_t2571135199_DecimalU26_t3838531224_UInt64U26_t3595557107,
	RuntimeInvoker_Int32_t2571135199_DecimalU26_t3838531224_Int64U26_t4038210315,
	RuntimeInvoker_Int32_t2571135199_DecimalU26_t3838531224_DecimalU26_t3838531224,
	RuntimeInvoker_Int32_t2571135199_DecimalU26_t3838531224_RuntimeObject_Int32_t2571135199_Int32_t2571135199,
	RuntimeInvoker_Int32_t2571135199_DecimalU26_t3838531224_Int32_t2571135199,
	RuntimeInvoker_Double_t1454265465_DecimalU26_t3838531224,
	RuntimeInvoker_Void_t1078098495_DecimalU26_t3838531224_Int32_t2571135199,
	RuntimeInvoker_Int32_t2571135199_DecimalU26_t3838531224_DecimalU26_t3838531224_DecimalU26_t3838531224,
	RuntimeInvoker_Byte_t880488320_Decimal_t2656901032,
	RuntimeInvoker_SByte_t2530277047_Decimal_t2656901032,
	RuntimeInvoker_Int16_t769030278_Decimal_t2656901032,
	RuntimeInvoker_UInt16_t11709673_Decimal_t2656901032,
	RuntimeInvoker_UInt32_t2739056616_Decimal_t2656901032,
	RuntimeInvoker_Decimal_t2656901032_SByte_t2530277047,
	RuntimeInvoker_Decimal_t2656901032_Int16_t769030278,
	RuntimeInvoker_Decimal_t2656901032_Int32_t2571135199,
	RuntimeInvoker_Decimal_t2656901032_Int64_t384086013,
	RuntimeInvoker_Decimal_t2656901032_Single_t496865882,
	RuntimeInvoker_Decimal_t2656901032_Double_t1454265465,
	RuntimeInvoker_Single_t496865882_Decimal_t2656901032,
	RuntimeInvoker_Double_t1454265465_Decimal_t2656901032,
	RuntimeInvoker_Void_t1078098495_RuntimeObject_StreamingContext_t1316667400,
	RuntimeInvoker_Int64_t384086013,
	RuntimeInvoker_Boolean_t2059672899_IntPtr_t_IntPtr_t,
	RuntimeInvoker_IntPtr_t_Int32_t2571135199,
	RuntimeInvoker_IntPtr_t_Int64_t384086013,
	RuntimeInvoker_IntPtr_t_RuntimeObject,
	RuntimeInvoker_Int32_t2571135199_IntPtr_t,
	RuntimeInvoker_RuntimeObject_IntPtr_t,
	RuntimeInvoker_RuntimeObject_RuntimeObject_RuntimeObject_MulticastDelegateU26_t455666402,
	RuntimeInvoker_RuntimeObject_RuntimeObject_RuntimeObject_RuntimeObject_SByte_t2530277047,
	RuntimeInvoker_TypeCode_t2052347577,
	RuntimeInvoker_Int32_t2571135199_RuntimeObject_RuntimeObject_RuntimeObject_SByte_t2530277047,
	RuntimeInvoker_UInt64_t2265270229_RuntimeObject_Int32_t2571135199,
	RuntimeInvoker_RuntimeObject_RuntimeObject_SByte_t2530277047,
	RuntimeInvoker_RuntimeObject_RuntimeObject_Int16_t769030278,
	RuntimeInvoker_RuntimeObject_RuntimeObject_Int64_t384086013,
	RuntimeInvoker_Void_t1078098495_Int32_t2571135199_RuntimeObject,
	RuntimeInvoker_Int64_t384086013_Int32_t2571135199,
	RuntimeInvoker_Boolean_t2059672899_RuntimeObject_Int32_t2571135199_RuntimeObject_Int32_t2571135199_Int32_t2571135199,
	RuntimeInvoker_RuntimeObject_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199,
	RuntimeInvoker_RuntimeObject_Int64_t384086013,
	RuntimeInvoker_RuntimeObject_Int64_t384086013_Int64_t384086013,
	RuntimeInvoker_RuntimeObject_Int64_t384086013_Int64_t384086013_Int64_t384086013,
	RuntimeInvoker_Void_t1078098495_RuntimeObject_Int64_t384086013,
	RuntimeInvoker_Void_t1078098495_RuntimeObject_Int64_t384086013_Int64_t384086013,
	RuntimeInvoker_Void_t1078098495_RuntimeObject_Int64_t384086013_Int64_t384086013_Int64_t384086013,
	RuntimeInvoker_Void_t1078098495_RuntimeObject_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199,
	RuntimeInvoker_RuntimeObject_RuntimeObject_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199,
	RuntimeInvoker_Int32_t2571135199_RuntimeObject_RuntimeObject_RuntimeObject,
	RuntimeInvoker_Int32_t2571135199_RuntimeObject_Int32_t2571135199_Int32_t2571135199_RuntimeObject,
	RuntimeInvoker_Int32_t2571135199_RuntimeObject_Int32_t2571135199_Int32_t2571135199_RuntimeObject_RuntimeObject,
	RuntimeInvoker_Void_t1078098495_RuntimeObject_Int64_t384086013_RuntimeObject_Int64_t384086013_Int64_t384086013,
	RuntimeInvoker_Void_t1078098495_RuntimeObject_RuntimeObject_Int64_t384086013,
	RuntimeInvoker_Int32_t2571135199_RuntimeObject_RuntimeObject_Int32_t2571135199,
	RuntimeInvoker_Int32_t2571135199_RuntimeObject_RuntimeObject_Int32_t2571135199_Int32_t2571135199,
	RuntimeInvoker_Void_t1078098495_RuntimeObject_RuntimeObject_RuntimeObject,
	RuntimeInvoker_Void_t1078098495_RuntimeObject_RuntimeObject_Int32_t2571135199_Int32_t2571135199,
	RuntimeInvoker_Void_t1078098495_RuntimeObject_Int32_t2571135199_Int32_t2571135199_RuntimeObject,
	RuntimeInvoker_Void_t1078098495_RuntimeObject_RuntimeObject_Int32_t2571135199_Int32_t2571135199_RuntimeObject,
	RuntimeInvoker_Void_t1078098495_Int32_t2571135199_Int32_t2571135199,
	RuntimeInvoker_Void_t1078098495_RuntimeObject_IntPtr_t,
	RuntimeInvoker_RuntimeObject_Int32_t2571135199_Int32_t2571135199_RuntimeObject_RuntimeObject,
	RuntimeInvoker_TypeAttributes_t3122562390,
	RuntimeInvoker_MemberTypes_t2339173058,
	RuntimeInvoker_RuntimeTypeHandle_t637624852,
	RuntimeInvoker_RuntimeObject_RuntimeObject_SByte_t2530277047_SByte_t2530277047,
	RuntimeInvoker_TypeCode_t2052347577_RuntimeObject,
	RuntimeInvoker_RuntimeObject_RuntimeTypeHandle_t637624852,
	RuntimeInvoker_RuntimeObject_RuntimeObject_Int32_t2571135199_RuntimeObject_RuntimeObject_RuntimeObject,
	RuntimeInvoker_RuntimeObject_RuntimeObject_Int32_t2571135199_RuntimeObject_Int32_t2571135199_RuntimeObject_RuntimeObject,
	RuntimeInvoker_RuntimeObject_RuntimeObject_Int32_t2571135199_RuntimeObject_RuntimeObject_RuntimeObject_RuntimeObject,
	RuntimeInvoker_RuntimeObject_Int32_t2571135199_RuntimeObject_Int32_t2571135199_RuntimeObject_RuntimeObject,
	RuntimeInvoker_RuntimeObject_Int32_t2571135199_RuntimeObject_RuntimeObject_RuntimeObject,
	RuntimeInvoker_RuntimeObject_RuntimeObject_Int32_t2571135199_RuntimeObject_RuntimeObject_RuntimeObject_RuntimeObject_RuntimeObject_RuntimeObject,
	RuntimeInvoker_Boolean_t2059672899_RuntimeObject_SByte_t2530277047,
	RuntimeInvoker_RuntimeObject_SByte_t2530277047,
	RuntimeInvoker_IntPtr_t,
	RuntimeInvoker_Void_t1078098495_RuntimeObject_SByte_t2530277047,
	RuntimeInvoker_Void_t1078098495_SByte_t2530277047_SByte_t2530277047_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199,
	RuntimeInvoker_Void_t1078098495_RuntimeObject_RuntimeFieldHandle_t1782795956,
	RuntimeInvoker_Void_t1078098495_IntPtr_t_SByte_t2530277047,
	RuntimeInvoker_Void_t1078098495_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199,
	RuntimeInvoker_Void_t1078098495_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199_SByte_t2530277047,
	RuntimeInvoker_Void_t1078098495_SByte_t2530277047_SByte_t2530277047,
	RuntimeInvoker_Void_t1078098495_RuntimeObject_RuntimeObject_ContractionU5BU5DU26_t2904489606_Level2MapU5BU5DU26_t2963104860,
	RuntimeInvoker_Void_t1078098495_RuntimeObject_CodePointIndexerU26_t2204815176_ByteU2AU26_t1485094272_ByteU2AU26_t1485094272_CodePointIndexerU26_t2204815176_ByteU2AU26_t1485094272,
	RuntimeInvoker_Byte_t880488320_Int32_t2571135199,
	RuntimeInvoker_Boolean_t2059672899_Int32_t2571135199_SByte_t2530277047,
	RuntimeInvoker_UInt32_t2739056616_RuntimeObject_Int32_t2571135199,
	RuntimeInvoker_Byte_t880488320_Int32_t2571135199_Int32_t2571135199,
	RuntimeInvoker_Boolean_t2059672899_Int32_t2571135199_Int32_t2571135199,
	RuntimeInvoker_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199,
	RuntimeInvoker_ExtenderType_t1616581759_Int32_t2571135199,
	RuntimeInvoker_Void_t1078098495_RuntimeObject_Int32_t2571135199_Int32_t2571135199_RuntimeObject_Int32_t2571135199,
	RuntimeInvoker_Void_t1078098495_Int32_t2571135199_Int32_t2571135199_RuntimeObject_Int32_t2571135199,
	RuntimeInvoker_Int32_t2571135199_RuntimeObject_Int32_t2571135199_Int32_t2571135199_RuntimeObject_Int32_t2571135199_Int32_t2571135199_BooleanU26_t495120821_BooleanU26_t495120821_SByte_t2530277047,
	RuntimeInvoker_Int32_t2571135199_RuntimeObject_Int32_t2571135199_Int32_t2571135199_RuntimeObject_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199,
	RuntimeInvoker_Boolean_t2059672899_RuntimeObject_Int32_t2571135199_Int32_t2571135199_RuntimeObject_Int32_t2571135199_Int32_t2571135199,
	RuntimeInvoker_Int32_t2571135199_RuntimeObject_Int32_t2571135199_Int32_t2571135199_RuntimeObject_Int32_t2571135199_Int32_t2571135199_BooleanU26_t495120821_BooleanU26_t495120821_SByte_t2530277047_SByte_t2530277047_ContextU26_t831875511,
	RuntimeInvoker_Int32_t2571135199_SByte_t2530277047_SByte_t2530277047,
	RuntimeInvoker_Boolean_t2059672899_RuntimeObject_RuntimeObject_Int32_t2571135199,
	RuntimeInvoker_Boolean_t2059672899_RuntimeObject_RuntimeObject_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199,
	RuntimeInvoker_Boolean_t2059672899_RuntimeObject_RuntimeObject_Int32_t2571135199_Int32_t2571135199_SByte_t2530277047_ContextU26_t831875511,
	RuntimeInvoker_Int32_t2571135199_RuntimeObject_RuntimeObject_Int32_t2571135199_Int32_t2571135199_BooleanU26_t495120821,
	RuntimeInvoker_Int32_t2571135199_RuntimeObject_RuntimeObject_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199,
	RuntimeInvoker_Int32_t2571135199_RuntimeObject_Int32_t2571135199_Int32_t2571135199_RuntimeObject_Int16_t769030278_Int32_t2571135199_SByte_t2530277047_ContextU26_t831875511,
	RuntimeInvoker_Int32_t2571135199_RuntimeObject_RuntimeObject_Int32_t2571135199_Int32_t2571135199_RuntimeObject_ContextU26_t831875511,
	RuntimeInvoker_Int32_t2571135199_RuntimeObject_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199_RuntimeObject_Int32_t2571135199_SByte_t2530277047_ContextU26_t831875511,
	RuntimeInvoker_Boolean_t2059672899_RuntimeObject_Int32U26_t2596978809_Int32_t2571135199_Int32_t2571135199_RuntimeObject_SByte_t2530277047_ContextU26_t831875511,
	RuntimeInvoker_Boolean_t2059672899_RuntimeObject_Int32U26_t2596978809_Int32_t2571135199_Int32_t2571135199_RuntimeObject_SByte_t2530277047_Int32_t2571135199_ContractionU26_t1846426445_ContextU26_t831875511,
	RuntimeInvoker_Boolean_t2059672899_Int32_t2571135199_RuntimeObject_Int32_t2571135199_Int32_t2571135199_RuntimeObject_Int32_t2571135199_SByte_t2530277047,
	RuntimeInvoker_Boolean_t2059672899_RuntimeObject_Int32U26_t2596978809_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199_RuntimeObject_SByte_t2530277047_ContextU26_t831875511,
	RuntimeInvoker_Boolean_t2059672899_RuntimeObject_Int32U26_t2596978809_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199_RuntimeObject_SByte_t2530277047_Int32_t2571135199_ContractionU26_t1846426445_ContextU26_t831875511,
	RuntimeInvoker_Void_t1078098495_Int32_t2571135199_RuntimeObject_RuntimeObject_RuntimeObject_RuntimeObject_RuntimeObject_SByte_t2530277047,
	RuntimeInvoker_Void_t1078098495_Int32_t2571135199_RuntimeObject_Int32_t2571135199,
	RuntimeInvoker_Void_t1078098495_Int32_t2571135199_RuntimeObject_RuntimeObject_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199,
	RuntimeInvoker_Void_t1078098495_Int32_t2571135199_Int32_t2571135199_RuntimeObject_SByte_t2530277047,
	RuntimeInvoker_Void_t1078098495_SByte_t2530277047_SByte_t2530277047_SByte_t2530277047_SByte_t2530277047_SByte_t2530277047_SByte_t2530277047_SByte_t2530277047_SByte_t2530277047,
	RuntimeInvoker_Void_t1078098495_SByte_t2530277047_SByte_t2530277047_SByte_t2530277047_SByte_t2530277047,
	RuntimeInvoker_Void_t1078098495_SByte_t2530277047_ByteU5BU5DU26_t321522471_Int32U26_t2596978809,
	RuntimeInvoker_Int32_t2571135199_RuntimeObject_Int32_t2571135199_SByte_t2530277047,
	RuntimeInvoker_ConfidenceFactor_t3678829215,
	RuntimeInvoker_Void_t1078098495_Int32_t2571135199_SByte_t2530277047,
	RuntimeInvoker_Sign_t3270271764_RuntimeObject_RuntimeObject,
	RuntimeInvoker_Void_t1078098495_RuntimeObject_Int32_t2571135199_Int32_t2571135199_RuntimeObject_Int32_t2571135199_Int32_t2571135199_RuntimeObject_Int32_t2571135199,
	RuntimeInvoker_Void_t1078098495_RuntimeObject_Int32_t2571135199_Int32_t2571135199_RuntimeObject_Int32_t2571135199_Int32_t2571135199_RuntimeObject_Int32_t2571135199_Int32_t2571135199,
	RuntimeInvoker_DSAParameters_t2781005375_SByte_t2530277047,
	RuntimeInvoker_Void_t1078098495_DSAParameters_t2781005375,
	RuntimeInvoker_Boolean_t2059672899_RuntimeObject_RuntimeObject_RuntimeObject_RuntimeObject,
	RuntimeInvoker_Boolean_t2059672899_RuntimeObject_RuntimeObject_RuntimeObject_RuntimeObject_SByte_t2530277047,
	RuntimeInvoker_RuntimeObject_RuntimeObject_DSAParameters_t2781005375,
	RuntimeInvoker_RSAParameters_t2749614896_SByte_t2530277047,
	RuntimeInvoker_Void_t1078098495_RSAParameters_t2749614896,
	RuntimeInvoker_Void_t1078098495_RuntimeObject_SByte_t2530277047_RuntimeObject,
	RuntimeInvoker_Int32_t2571135199_RuntimeObject_Int32_t2571135199_Int32_t2571135199_RuntimeObject_Int32_t2571135199,
	RuntimeInvoker_RuntimeObject_RuntimeObject_RuntimeObject_Int32_t2571135199_RuntimeObject,
	RuntimeInvoker_DSAParameters_t2781005375_BooleanU26_t495120821,
	RuntimeInvoker_Void_t1078098495_RuntimeObject_Int32_t2571135199_RuntimeObject,
	RuntimeInvoker_RuntimeObject_RuntimeObject_SByte_t2530277047_RuntimeObject_SByte_t2530277047,
	RuntimeInvoker_Void_t1078098495_RuntimeObject_RuntimeObject_SByte_t2530277047,
	RuntimeInvoker_DateTime_t507798353,
	RuntimeInvoker_Void_t1078098495_SByte_t2530277047_RuntimeObject,
	RuntimeInvoker_Byte_t880488320,
	RuntimeInvoker_Void_t1078098495_RuntimeObject_Int32U26_t2596978809_Int32_t2571135199,
	RuntimeInvoker_Void_t1078098495_RuntimeObject_Int32U26_t2596978809_ByteU26_t3158801536_Int32U26_t2596978809_ByteU5BU5DU26_t321522471,
	RuntimeInvoker_RuntimeObject_Int32_t2571135199_SByte_t2530277047,
	RuntimeInvoker_Int16_t769030278_RuntimeObject_Int32_t2571135199,
	RuntimeInvoker_Single_t496865882_RuntimeObject_Int32_t2571135199,
	RuntimeInvoker_Double_t1454265465_RuntimeObject_Int32_t2571135199,
	RuntimeInvoker_RuntimeObject_Int16_t769030278_SByte_t2530277047,
	RuntimeInvoker_Void_t1078098495_Int32_t2571135199_RuntimeObject_RuntimeObject,
	RuntimeInvoker_Void_t1078098495_Int32_t2571135199_Single_t496865882_RuntimeObject_RuntimeObject,
	RuntimeInvoker_Void_t1078098495_Int32_t2571135199_Single_t496865882,
	RuntimeInvoker_Void_t1078098495_RuntimeObject_Single_t496865882_RuntimeObject_RuntimeObject,
	RuntimeInvoker_Void_t1078098495_Int32_t2571135199_Single_t496865882_RuntimeObject,
	RuntimeInvoker_DictionaryEntry_t1385909157,
	RuntimeInvoker_Boolean_t2059672899_Int32_t2571135199_SByte_t2530277047_MethodBaseU26_t3663460261_Int32U26_t2596978809_Int32U26_t2596978809_StringU26_t2155547156_Int32U26_t2596978809_Int32U26_t2596978809,
	RuntimeInvoker_Void_t1078098495_RuntimeObject_Int32_t2571135199_SByte_t2530277047_SByte_t2530277047,
	RuntimeInvoker_RuntimeObject_RuntimeObject_Int32_t2571135199_SByte_t2530277047,
	RuntimeInvoker_Int32_t2571135199_DateTime_t507798353,
	RuntimeInvoker_DayOfWeek_t61775933_DateTime_t507798353,
	RuntimeInvoker_Int32_t2571135199_Int32U26_t2596978809_Int32_t2571135199_Int32_t2571135199,
	RuntimeInvoker_DayOfWeek_t61775933_Int32_t2571135199,
	RuntimeInvoker_Void_t1078098495_Int32U26_t2596978809_Int32U26_t2596978809_Int32_t2571135199,
	RuntimeInvoker_Void_t1078098495_Int32U26_t2596978809_Int32U26_t2596978809_Int32U26_t2596978809_Int32_t2571135199,
	RuntimeInvoker_Int32_t2571135199_RuntimeObject_Int32_t2571135199_Int32_t2571135199_RuntimeObject_Int32_t2571135199_SByte_t2530277047,
	RuntimeInvoker_Void_t1078098495_Int32_t2571135199_SByte_t2530277047_SByte_t2530277047,
	RuntimeInvoker_Void_t1078098495_RuntimeObject_SByte_t2530277047_SByte_t2530277047,
	RuntimeInvoker_RuntimeObject_Int16_t769030278,
	RuntimeInvoker_Void_t1078098495_DateTime_t507798353_DateTime_t507798353_TimeSpan_t2986675223,
	RuntimeInvoker_TimeSpan_t2986675223,
	RuntimeInvoker_Void_t1078098495_RuntimeObject_Int32_t2571135199_RuntimeObject_SByte_t2530277047,
	RuntimeInvoker_Int32_t2571135199_RuntimeObject_Int32_t2571135199_Int32_t2571135199_Int32U26_t2596978809,
	RuntimeInvoker_Char_t3572527572,
	RuntimeInvoker_Decimal_t2656901032,
	RuntimeInvoker_Double_t1454265465,
	RuntimeInvoker_Int16_t769030278,
	RuntimeInvoker_SByte_t2530277047,
	RuntimeInvoker_Single_t496865882,
	RuntimeInvoker_UInt16_t11709673,
	RuntimeInvoker_UInt32_t2739056616,
	RuntimeInvoker_UInt64_t2265270229,
	RuntimeInvoker_Void_t1078098495_IntPtr_t_Int32_t2571135199_SByte_t2530277047_Int32_t2571135199_SByte_t2530277047_SByte_t2530277047,
	RuntimeInvoker_Void_t1078098495_RuntimeObject_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199_SByte_t2530277047_SByte_t2530277047,
	RuntimeInvoker_Void_t1078098495_RuntimeObject_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199_SByte_t2530277047_Int32_t2571135199,
	RuntimeInvoker_RuntimeObject_RuntimeObject_Int32_t2571135199_Int32_t2571135199_RuntimeObject_RuntimeObject,
	RuntimeInvoker_Int64_t384086013_Int64_t384086013_Int32_t2571135199,
	RuntimeInvoker_Int32_t2571135199_IntPtr_t_RuntimeObject_Int32_t2571135199_Int32_t2571135199,
	RuntimeInvoker_Void_t1078098495_RuntimeObject_Int32_t2571135199_Int32_t2571135199_SByte_t2530277047_SByte_t2530277047,
	RuntimeInvoker_Boolean_t2059672899_RuntimeObject_MonoIOErrorU26_t2040356602,
	RuntimeInvoker_RuntimeObject_RuntimeObject_RuntimeObject_Int32_t2571135199_Int32_t2571135199_MonoIOErrorU26_t2040356602,
	RuntimeInvoker_RuntimeObject_MonoIOErrorU26_t2040356602,
	RuntimeInvoker_FileAttributes_t1797260272_RuntimeObject_MonoIOErrorU26_t2040356602,
	RuntimeInvoker_MonoFileType_t2271609547_IntPtr_t_MonoIOErrorU26_t2040356602,
	RuntimeInvoker_Boolean_t2059672899_RuntimeObject_MonoIOStatU26_t3133205914_MonoIOErrorU26_t2040356602,
	RuntimeInvoker_IntPtr_t_RuntimeObject_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199_MonoIOErrorU26_t2040356602,
	RuntimeInvoker_Boolean_t2059672899_IntPtr_t_MonoIOErrorU26_t2040356602,
	RuntimeInvoker_Int32_t2571135199_IntPtr_t_RuntimeObject_Int32_t2571135199_Int32_t2571135199_MonoIOErrorU26_t2040356602,
	RuntimeInvoker_Int64_t384086013_IntPtr_t_Int64_t384086013_Int32_t2571135199_MonoIOErrorU26_t2040356602,
	RuntimeInvoker_Int64_t384086013_IntPtr_t_MonoIOErrorU26_t2040356602,
	RuntimeInvoker_Boolean_t2059672899_IntPtr_t_Int64_t384086013_MonoIOErrorU26_t2040356602,
	RuntimeInvoker_Boolean_t2059672899_RuntimeObject_StringU26_t2155547156,
	RuntimeInvoker_Void_t1078098495_RuntimeObject_RuntimeObject_SByte_t2530277047_Int32_t2571135199,
	RuntimeInvoker_Void_t1078098495_Int16_t769030278,
	RuntimeInvoker_Void_t1078098495_RuntimeObject_Int32_t2571135199_Int32_t2571135199_RuntimeObject_RuntimeObject_RuntimeObject,
	RuntimeInvoker_CallingConventions_t1168634181,
	RuntimeInvoker_RuntimeMethodHandle_t2877888302,
	RuntimeInvoker_MethodAttributes_t3855721989,
	RuntimeInvoker_MethodToken_t1430837147,
	RuntimeInvoker_FieldAttributes_t1107365117,
	RuntimeInvoker_RuntimeFieldHandle_t1782795956,
	RuntimeInvoker_Void_t1078098495_RuntimeObject_RuntimeObject_Int32_t2571135199_RuntimeObject_RuntimeObject,
	RuntimeInvoker_Void_t1078098495_OpCode_t376486760,
	RuntimeInvoker_Void_t1078098495_OpCode_t376486760_RuntimeObject,
	RuntimeInvoker_StackBehaviour_t1946418481,
	RuntimeInvoker_PropertyAttributes_t820793723,
	RuntimeInvoker_Void_t1078098495_RuntimeObject_RuntimeObject_Int32_t2571135199_RuntimeObject_RuntimeObject_RuntimeObject,
	RuntimeInvoker_RuntimeObject_Int32_t2571135199_Int32_t2571135199_RuntimeObject,
	RuntimeInvoker_RuntimeObject_Int32_t2571135199_Int32_t2571135199_RuntimeObject_RuntimeObject_RuntimeObject,
	RuntimeInvoker_RuntimeObject_RuntimeObject_Int32_t2571135199_SByte_t2530277047_RuntimeObject,
	RuntimeInvoker_IntPtr_t_RuntimeObject_Int32U26_t2596978809_ModuleU26_t1352866505,
	RuntimeInvoker_RuntimeObject_RuntimeObject_RuntimeObject_SByte_t2530277047_SByte_t2530277047,
	RuntimeInvoker_AssemblyNameFlags_t899890916,
	RuntimeInvoker_RuntimeObject_Int32_t2571135199_RuntimeObject_ObjectU5BU5DU26_t3608108294_RuntimeObject_RuntimeObject_RuntimeObject_ObjectU26_t4136591053,
	RuntimeInvoker_Void_t1078098495_ObjectU5BU5DU26_t3608108294_RuntimeObject,
	RuntimeInvoker_RuntimeObject_Int32_t2571135199_RuntimeObject_RuntimeObject_RuntimeObject_RuntimeObject,
	RuntimeInvoker_Void_t1078098495_RuntimeObject_ObjectU5BU5DU26_t3608108294_RuntimeObject,
	RuntimeInvoker_RuntimeObject_Int32_t2571135199_RuntimeObject_RuntimeObject_RuntimeObject_SByte_t2530277047,
	RuntimeInvoker_EventAttributes_t1621333428,
	RuntimeInvoker_Void_t1078098495_RuntimeObject_RuntimeObject_RuntimeObject_RuntimeObject_Int32_t2571135199,
	RuntimeInvoker_Void_t1078098495_RuntimeObject_RuntimeObject_RuntimeObject_RuntimeObject_Int32_t2571135199_RuntimeObject,
	RuntimeInvoker_RuntimeObject_StreamingContext_t1316667400,
	RuntimeInvoker_Void_t1078098495_RuntimeObject_MonoEventInfoU26_t4282179039,
	RuntimeInvoker_MonoEventInfo_t802398473_RuntimeObject,
	RuntimeInvoker_Void_t1078098495_IntPtr_t_MonoMethodInfoU26_t149925759,
	RuntimeInvoker_MonoMethodInfo_t1860908649_IntPtr_t,
	RuntimeInvoker_MethodAttributes_t3855721989_IntPtr_t,
	RuntimeInvoker_CallingConventions_t1168634181_IntPtr_t,
	RuntimeInvoker_RuntimeObject_IntPtr_t_RuntimeObject,
	RuntimeInvoker_RuntimeObject_RuntimeObject_RuntimeObject_ExceptionU26_t1670763973,
	RuntimeInvoker_Void_t1078098495_RuntimeObject_MonoPropertyInfoU26_t1411063087_Int32_t2571135199,
	RuntimeInvoker_Void_t1078098495_RuntimeObject_RuntimeObject_RuntimeObject_Int32_t2571135199,
	RuntimeInvoker_ParameterAttributes_t3383706087,
	RuntimeInvoker_Void_t1078098495_Int64_t384086013_ResourceInfoU26_t4212590071,
	RuntimeInvoker_Void_t1078098495_RuntimeObject_Int64_t384086013_Int32_t2571135199,
	RuntimeInvoker_GCHandle_t4215464244_RuntimeObject_Int32_t2571135199,
	RuntimeInvoker_Void_t1078098495_IntPtr_t_Int32_t2571135199_RuntimeObject_Int32_t2571135199,
	RuntimeInvoker_Void_t1078098495_IntPtr_t_RuntimeObject_Int32_t2571135199_Int32_t2571135199,
	RuntimeInvoker_Byte_t880488320_IntPtr_t_Int32_t2571135199,
	RuntimeInvoker_Void_t1078098495_IntPtr_t_Int32_t2571135199_SByte_t2530277047,
	RuntimeInvoker_Void_t1078098495_BooleanU26_t495120821,
	RuntimeInvoker_Void_t1078098495_IntPtr_t,
	RuntimeInvoker_RuntimeObject_RuntimeObject_RuntimeObject_StringU26_t2155547156,
	RuntimeInvoker_RuntimeObject_RuntimeObject_RuntimeObject_RuntimeObject_StringU26_t2155547156,
	RuntimeInvoker_Boolean_t2059672899_RuntimeObject_RuntimeObject_RuntimeObject,
	RuntimeInvoker_Void_t1078098495_SByte_t2530277047_RuntimeObject_SByte_t2530277047_SByte_t2530277047,
	RuntimeInvoker_Void_t1078098495_TimeSpan_t2986675223,
	RuntimeInvoker_Void_t1078098495_RuntimeObject_Byte_t880488320,
	RuntimeInvoker_Void_t1078098495_RuntimeObject_RuntimeObject_SByte_t2530277047_RuntimeObject,
	RuntimeInvoker_RuntimeObject_RuntimeObject_RuntimeObject_StreamingContext_t1316667400_RuntimeObject,
	RuntimeInvoker_RuntimeObject_RuntimeObject_StreamingContext_t1316667400_ISurrogateSelectorU26_t193635247,
	RuntimeInvoker_Void_t1078098495_RuntimeObject_IntPtr_t_RuntimeObject,
	RuntimeInvoker_TimeSpan_t2986675223_RuntimeObject,
	RuntimeInvoker_RuntimeObject_StringU26_t2155547156,
	RuntimeInvoker_RuntimeObject_RuntimeObject_RuntimeObject_ObjectU26_t4136591053,
	RuntimeInvoker_Boolean_t2059672899_RuntimeObject_StringU26_t2155547156_StringU26_t2155547156,
	RuntimeInvoker_WellKnownObjectMode_t2386524380,
	RuntimeInvoker_StreamingContext_t1316667400,
	RuntimeInvoker_TypeFilterLevel_t3266183943,
	RuntimeInvoker_Void_t1078098495_RuntimeObject_BooleanU26_t495120821,
	RuntimeInvoker_RuntimeObject_Byte_t880488320_RuntimeObject_SByte_t2530277047_RuntimeObject_RuntimeObject,
	RuntimeInvoker_RuntimeObject_Byte_t880488320_RuntimeObject_SByte_t2530277047_RuntimeObject_RuntimeObject_RuntimeObject,
	RuntimeInvoker_Void_t1078098495_RuntimeObject_SByte_t2530277047_ObjectU26_t4136591053_HeaderU5BU5DU26_t1438187576,
	RuntimeInvoker_Void_t1078098495_Byte_t880488320_RuntimeObject_SByte_t2530277047_ObjectU26_t4136591053_HeaderU5BU5DU26_t1438187576,
	RuntimeInvoker_Boolean_t2059672899_Byte_t880488320_RuntimeObject,
	RuntimeInvoker_Void_t1078098495_Byte_t880488320_RuntimeObject_Int64U26_t4038210315_ObjectU26_t4136591053_SerializationInfoU26_t114801638,
	RuntimeInvoker_Void_t1078098495_RuntimeObject_SByte_t2530277047_SByte_t2530277047_Int64U26_t4038210315_ObjectU26_t4136591053_SerializationInfoU26_t114801638,
	RuntimeInvoker_Void_t1078098495_RuntimeObject_Int64U26_t4038210315_ObjectU26_t4136591053_SerializationInfoU26_t114801638,
	RuntimeInvoker_Void_t1078098495_RuntimeObject_RuntimeObject_Int64_t384086013_ObjectU26_t4136591053_SerializationInfoU26_t114801638,
	RuntimeInvoker_Void_t1078098495_Int64_t384086013_RuntimeObject_RuntimeObject_Int64_t384086013_RuntimeObject_RuntimeObject,
	RuntimeInvoker_Void_t1078098495_RuntimeObject_Int64U26_t4038210315_ObjectU26_t4136591053,
	RuntimeInvoker_Void_t1078098495_RuntimeObject_RuntimeObject_Int64U26_t4038210315_ObjectU26_t4136591053,
	RuntimeInvoker_Void_t1078098495_RuntimeObject_RuntimeObject_Int64_t384086013_RuntimeObject_RuntimeObject_RuntimeObject_RuntimeObject_RuntimeObject,
	RuntimeInvoker_Void_t1078098495_RuntimeObject_RuntimeObject_RuntimeObject_RuntimeObject_RuntimeObject_RuntimeObject_RuntimeObject,
	RuntimeInvoker_Void_t1078098495_Int64_t384086013_Int64_t384086013_RuntimeObject_RuntimeObject_RuntimeObject_RuntimeObject_RuntimeObject,
	RuntimeInvoker_RuntimeObject_Int64_t384086013_RuntimeObject,
	RuntimeInvoker_RuntimeObject_RuntimeObject_Byte_t880488320,
	RuntimeInvoker_Void_t1078098495_Int64_t384086013_Int32_t2571135199_Int64_t384086013,
	RuntimeInvoker_Void_t1078098495_Int64_t384086013_RuntimeObject_Int64_t384086013,
	RuntimeInvoker_Void_t1078098495_RuntimeObject_Int64_t384086013_RuntimeObject_Int64_t384086013_RuntimeObject_RuntimeObject,
	RuntimeInvoker_Boolean_t2059672899_SByte_t2530277047_RuntimeObject_SByte_t2530277047,
	RuntimeInvoker_Boolean_t2059672899_RuntimeObject_RuntimeObject_StreamingContext_t1316667400,
	RuntimeInvoker_Void_t1078098495_RuntimeObject_RuntimeObject_StreamingContext_t1316667400,
	RuntimeInvoker_Void_t1078098495_StreamingContext_t1316667400,
	RuntimeInvoker_RuntimeObject_StreamingContext_t1316667400_RuntimeObject_RuntimeObject,
	RuntimeInvoker_SerializationEntry_t4251740542,
	RuntimeInvoker_StreamingContextStates_t4185925513,
	RuntimeInvoker_CspProviderFlags_t2530719326,
	RuntimeInvoker_Void_t1078098495_RuntimeObject_SByte_t2530277047_RuntimeObject_RuntimeObject,
	RuntimeInvoker_UInt32_t2739056616_Int32_t2571135199_Int32_t2571135199,
	RuntimeInvoker_Void_t1078098495_RuntimeObject_RuntimeObject_RuntimeObject_SByte_t2530277047,
	RuntimeInvoker_Void_t1078098495_Int64_t384086013_RuntimeObject_Int32_t2571135199,
	RuntimeInvoker_UInt32_t2739056616_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199,
	RuntimeInvoker_Void_t1078098495_UInt32U26_t3679184984_Int32_t2571135199_UInt32U26_t3679184984_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199,
	RuntimeInvoker_IntPtr_t_IntPtr_t_RuntimeObject,
	RuntimeInvoker_UInt32_t2739056616_Int32_t2571135199,
	RuntimeInvoker_Void_t1078098495_Int64_t384086013_Int64_t384086013,
	RuntimeInvoker_UInt64_t2265270229_Int64_t384086013_Int32_t2571135199,
	RuntimeInvoker_UInt64_t2265270229_Int64_t384086013_Int64_t384086013_Int64_t384086013,
	RuntimeInvoker_UInt64_t2265270229_Int64_t384086013,
	RuntimeInvoker_CipherMode_t1257099160,
	RuntimeInvoker_PaddingMode_t218725588,
	RuntimeInvoker_Void_t1078098495_StringBuilderU26_t287250414_Int32_t2571135199,
	RuntimeInvoker_RuntimeObject_IntPtr_t_Int32_t2571135199,
	RuntimeInvoker_Int32_t2571135199_RuntimeObject_Int32_t2571135199_Int32_t2571135199_RuntimeObject_Int32_t2571135199_EncoderFallbackBufferU26_t95289058_CharU5BU5DU26_t3270425003,
	RuntimeInvoker_Int32_t2571135199_RuntimeObject_Int32_t2571135199_Int32_t2571135199_RuntimeObject_Int32_t2571135199_DecoderFallbackBufferU26_t2401674479,
	RuntimeInvoker_Int32_t2571135199_RuntimeObject_Int32_t2571135199_RuntimeObject_Int32_t2571135199,
	RuntimeInvoker_Boolean_t2059672899_Int16_t769030278_Int32_t2571135199,
	RuntimeInvoker_Boolean_t2059672899_Int16_t769030278_Int16_t769030278_Int32_t2571135199,
	RuntimeInvoker_Void_t1078098495_Int16_t769030278_Int16_t769030278_Int32_t2571135199,
	RuntimeInvoker_RuntimeObject_Int32U26_t2596978809,
	RuntimeInvoker_Void_t1078098495_RuntimeObject_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199,
	RuntimeInvoker_RuntimeObject_Int32_t2571135199_RuntimeObject_Int32_t2571135199,
	RuntimeInvoker_Void_t1078098495_SByte_t2530277047_SByte_t2530277047_SByte_t2530277047,
	RuntimeInvoker_Int32_t2571135199_RuntimeObject_Int32_t2571135199_Int32_t2571135199_SByte_t2530277047_Int32_t2571135199_SByte_t2530277047_SByte_t2530277047,
	RuntimeInvoker_Int32_t2571135199_RuntimeObject_Int32_t2571135199_Int32_t2571135199_RuntimeObject_Int32_t2571135199_SByte_t2530277047_Int32U26_t2596978809_BooleanU26_t495120821_SByte_t2530277047,
	RuntimeInvoker_Int32_t2571135199_RuntimeObject_Int32_t2571135199_Int32_t2571135199_RuntimeObject_Int32_t2571135199_Int32U26_t2596978809,
	RuntimeInvoker_Int32_t2571135199_RuntimeObject_Int32_t2571135199_Int32_t2571135199_CharU26_t2918757708_SByte_t2530277047,
	RuntimeInvoker_Int32_t2571135199_RuntimeObject_Int32_t2571135199_CharU26_t2918757708_SByte_t2530277047,
	RuntimeInvoker_Int32_t2571135199_RuntimeObject_Int32_t2571135199_Int32_t2571135199_RuntimeObject_Int32_t2571135199_CharU26_t2918757708_SByte_t2530277047,
	RuntimeInvoker_Int32_t2571135199_RuntimeObject_Int32_t2571135199_RuntimeObject_Int32_t2571135199_CharU26_t2918757708_SByte_t2530277047,
	RuntimeInvoker_Int32_t2571135199_RuntimeObject_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199_RuntimeObject_DecoderFallbackBufferU26_t2401674479_ByteU5BU5DU26_t321522471_SByte_t2530277047,
	RuntimeInvoker_Int32_t2571135199_RuntimeObject_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199_RuntimeObject_DecoderFallbackBufferU26_t2401674479_ByteU5BU5DU26_t321522471_SByte_t2530277047,
	RuntimeInvoker_Int32_t2571135199_RuntimeObject_DecoderFallbackBufferU26_t2401674479_ByteU5BU5DU26_t321522471_RuntimeObject_Int64_t384086013_Int32_t2571135199,
	RuntimeInvoker_Void_t1078098495_RuntimeObject_DecoderFallbackBufferU26_t2401674479_ByteU5BU5DU26_t321522471_RuntimeObject_Int64_t384086013_Int32_t2571135199_RuntimeObject_Int32U26_t2596978809,
	RuntimeInvoker_Int32_t2571135199_RuntimeObject_Int32_t2571135199_Int32_t2571135199_RuntimeObject_Int32_t2571135199_UInt32U26_t3679184984_UInt32U26_t3679184984_RuntimeObject_DecoderFallbackBufferU26_t2401674479_ByteU5BU5DU26_t321522471_SByte_t2530277047,
	RuntimeInvoker_Int32_t2571135199_RuntimeObject_Int32_t2571135199_RuntimeObject_Int32_t2571135199_UInt32U26_t3679184984_UInt32U26_t3679184984_RuntimeObject_DecoderFallbackBufferU26_t2401674479_ByteU5BU5DU26_t321522471_SByte_t2530277047,
	RuntimeInvoker_Void_t1078098495_RuntimeObject_RuntimeObject_Int32_t2571135199_SByte_t2530277047,
	RuntimeInvoker_Void_t1078098495_SByte_t2530277047_Int32_t2571135199,
	RuntimeInvoker_IntPtr_t_SByte_t2530277047_RuntimeObject_BooleanU26_t495120821,
	RuntimeInvoker_Boolean_t2059672899_IntPtr_t,
	RuntimeInvoker_IntPtr_t_SByte_t2530277047_SByte_t2530277047_RuntimeObject_BooleanU26_t495120821,
	RuntimeInvoker_Boolean_t2059672899_TimeSpan_t2986675223_TimeSpan_t2986675223,
	RuntimeInvoker_Boolean_t2059672899_Int64_t384086013_Int64_t384086013_SByte_t2530277047,
	RuntimeInvoker_Boolean_t2059672899_IntPtr_t_Int32_t2571135199_SByte_t2530277047,
	RuntimeInvoker_Int64_t384086013_Double_t1454265465,
	RuntimeInvoker_RuntimeObject_Double_t1454265465,
	RuntimeInvoker_Int64_t384086013_RuntimeObject_Int32_t2571135199,
	RuntimeInvoker_RuntimeObject_IntPtr_t_Int32_t2571135199_Int32_t2571135199,
	RuntimeInvoker_Byte_t880488320_SByte_t2530277047,
	RuntimeInvoker_Byte_t880488320_Int16_t769030278,
	RuntimeInvoker_Byte_t880488320_Double_t1454265465,
	RuntimeInvoker_Byte_t880488320_Single_t496865882,
	RuntimeInvoker_Byte_t880488320_Int64_t384086013,
	RuntimeInvoker_Char_t3572527572_SByte_t2530277047,
	RuntimeInvoker_Char_t3572527572_Int64_t384086013,
	RuntimeInvoker_Char_t3572527572_Single_t496865882,
	RuntimeInvoker_Char_t3572527572_RuntimeObject_RuntimeObject,
	RuntimeInvoker_DateTime_t507798353_RuntimeObject_RuntimeObject,
	RuntimeInvoker_DateTime_t507798353_Int16_t769030278,
	RuntimeInvoker_DateTime_t507798353_Int32_t2571135199,
	RuntimeInvoker_DateTime_t507798353_Int64_t384086013,
	RuntimeInvoker_DateTime_t507798353_Single_t496865882,
	RuntimeInvoker_DateTime_t507798353_SByte_t2530277047,
	RuntimeInvoker_Double_t1454265465_SByte_t2530277047,
	RuntimeInvoker_Double_t1454265465_Double_t1454265465,
	RuntimeInvoker_Double_t1454265465_Single_t496865882,
	RuntimeInvoker_Double_t1454265465_Int32_t2571135199,
	RuntimeInvoker_Double_t1454265465_Int64_t384086013,
	RuntimeInvoker_Double_t1454265465_Int16_t769030278,
	RuntimeInvoker_Int16_t769030278_SByte_t2530277047,
	RuntimeInvoker_Int16_t769030278_Int16_t769030278,
	RuntimeInvoker_Int16_t769030278_Double_t1454265465,
	RuntimeInvoker_Int16_t769030278_Single_t496865882,
	RuntimeInvoker_Int16_t769030278_Int32_t2571135199,
	RuntimeInvoker_Int16_t769030278_Int64_t384086013,
	RuntimeInvoker_Int64_t384086013_SByte_t2530277047,
	RuntimeInvoker_Int64_t384086013_Int16_t769030278,
	RuntimeInvoker_Int64_t384086013_Single_t496865882,
	RuntimeInvoker_Int64_t384086013_Int64_t384086013,
	RuntimeInvoker_SByte_t2530277047_SByte_t2530277047,
	RuntimeInvoker_SByte_t2530277047_Int16_t769030278,
	RuntimeInvoker_SByte_t2530277047_Double_t1454265465,
	RuntimeInvoker_SByte_t2530277047_Single_t496865882,
	RuntimeInvoker_SByte_t2530277047_Int32_t2571135199,
	RuntimeInvoker_SByte_t2530277047_Int64_t384086013,
	RuntimeInvoker_Single_t496865882_SByte_t2530277047,
	RuntimeInvoker_Single_t496865882_Double_t1454265465,
	RuntimeInvoker_Single_t496865882_Single_t496865882,
	RuntimeInvoker_Single_t496865882_Int32_t2571135199,
	RuntimeInvoker_Single_t496865882_Int64_t384086013,
	RuntimeInvoker_Single_t496865882_Int16_t769030278,
	RuntimeInvoker_UInt16_t11709673_SByte_t2530277047,
	RuntimeInvoker_UInt16_t11709673_Int16_t769030278,
	RuntimeInvoker_UInt16_t11709673_Double_t1454265465,
	RuntimeInvoker_UInt16_t11709673_Single_t496865882,
	RuntimeInvoker_UInt16_t11709673_Int32_t2571135199,
	RuntimeInvoker_UInt16_t11709673_Int64_t384086013,
	RuntimeInvoker_UInt32_t2739056616_SByte_t2530277047,
	RuntimeInvoker_UInt32_t2739056616_Int16_t769030278,
	RuntimeInvoker_UInt32_t2739056616_Double_t1454265465,
	RuntimeInvoker_UInt32_t2739056616_Single_t496865882,
	RuntimeInvoker_UInt32_t2739056616_Int64_t384086013,
	RuntimeInvoker_UInt64_t2265270229_SByte_t2530277047,
	RuntimeInvoker_UInt64_t2265270229_Int16_t769030278,
	RuntimeInvoker_UInt64_t2265270229_Double_t1454265465,
	RuntimeInvoker_UInt64_t2265270229_Single_t496865882,
	RuntimeInvoker_UInt64_t2265270229_Int32_t2571135199,
	RuntimeInvoker_Void_t1078098495_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199,
	RuntimeInvoker_Void_t1078098495_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199,
	RuntimeInvoker_Void_t1078098495_SByte_t2530277047_TimeSpan_t2986675223,
	RuntimeInvoker_Void_t1078098495_Int64_t384086013_Int32_t2571135199,
	RuntimeInvoker_DayOfWeek_t61775933,
	RuntimeInvoker_DateTimeKind_t3511769276,
	RuntimeInvoker_DateTime_t507798353_TimeSpan_t2986675223,
	RuntimeInvoker_DateTime_t507798353_Double_t1454265465,
	RuntimeInvoker_Int32_t2571135199_DateTime_t507798353_DateTime_t507798353,
	RuntimeInvoker_Boolean_t2059672899_DateTime_t507798353,
	RuntimeInvoker_DateTime_t507798353_DateTime_t507798353_Int32_t2571135199,
	RuntimeInvoker_DateTime_t507798353_RuntimeObject_RuntimeObject_Int32_t2571135199,
	RuntimeInvoker_Boolean_t2059672899_RuntimeObject_RuntimeObject_Int32_t2571135199_DateTimeU26_t2764841687_DateTimeOffsetU26_t118455887_SByte_t2530277047_ExceptionU26_t1670763973,
	RuntimeInvoker_RuntimeObject_RuntimeObject_SByte_t2530277047_ExceptionU26_t1670763973,
	RuntimeInvoker_Int32_t2571135199_RuntimeObject_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199_SByte_t2530277047_SByte_t2530277047_Int32U26_t2596978809,
	RuntimeInvoker_Int32_t2571135199_RuntimeObject_Int32_t2571135199_RuntimeObject_RuntimeObject_SByte_t2530277047_Int32U26_t2596978809,
	RuntimeInvoker_Boolean_t2059672899_RuntimeObject_Int32_t2571135199_Int32_t2571135199_RuntimeObject_Int32U26_t2596978809,
	RuntimeInvoker_Boolean_t2059672899_RuntimeObject_Int32_t2571135199_Int32_t2571135199_RuntimeObject_SByte_t2530277047_Int32U26_t2596978809_Int32U26_t2596978809,
	RuntimeInvoker_Boolean_t2059672899_RuntimeObject_Int32_t2571135199_RuntimeObject_SByte_t2530277047_Int32U26_t2596978809,
	RuntimeInvoker_Boolean_t2059672899_RuntimeObject_RuntimeObject_RuntimeObject_SByte_t2530277047_DateTimeU26_t2764841687_DateTimeOffsetU26_t118455887_RuntimeObject_Int32_t2571135199_SByte_t2530277047_BooleanU26_t495120821_BooleanU26_t495120821,
	RuntimeInvoker_DateTime_t507798353_RuntimeObject_RuntimeObject_RuntimeObject_Int32_t2571135199,
	RuntimeInvoker_Boolean_t2059672899_RuntimeObject_RuntimeObject_RuntimeObject_Int32_t2571135199_DateTimeU26_t2764841687_SByte_t2530277047_BooleanU26_t495120821_SByte_t2530277047_ExceptionU26_t1670763973,
	RuntimeInvoker_DateTime_t507798353_DateTime_t507798353_TimeSpan_t2986675223,
	RuntimeInvoker_Boolean_t2059672899_DateTime_t507798353_DateTime_t507798353,
	RuntimeInvoker_Void_t1078098495_DateTime_t507798353,
	RuntimeInvoker_Void_t1078098495_DateTime_t507798353_TimeSpan_t2986675223,
	RuntimeInvoker_Void_t1078098495_Int64_t384086013_TimeSpan_t2986675223,
	RuntimeInvoker_Int32_t2571135199_DateTimeOffset_t2220763929,
	RuntimeInvoker_Boolean_t2059672899_DateTimeOffset_t2220763929,
	RuntimeInvoker_Int32_t2571135199_RuntimeObject_Int32_t2571135199_Int16_t769030278,
	RuntimeInvoker_RuntimeObject_Int16_t769030278_RuntimeObject_BooleanU26_t495120821_BooleanU26_t495120821,
	RuntimeInvoker_RuntimeObject_Int16_t769030278_RuntimeObject_BooleanU26_t495120821_BooleanU26_t495120821_SByte_t2530277047,
	RuntimeInvoker_RuntimeObject_DateTime_t507798353_RuntimeObject_RuntimeObject,
	RuntimeInvoker_RuntimeObject_DateTime_t507798353_Nullable_1_t950977049_RuntimeObject_RuntimeObject,
	RuntimeInvoker_Void_t1078098495_MonoEnumInfo_t776332500,
	RuntimeInvoker_Void_t1078098495_RuntimeObject_MonoEnumInfoU26_t1245821516,
	RuntimeInvoker_Int32_t2571135199_Int16_t769030278_Int16_t769030278,
	RuntimeInvoker_Int32_t2571135199_Int64_t384086013_Int64_t384086013,
	RuntimeInvoker_PlatformID_t2641264986,
	RuntimeInvoker_Void_t1078098495_Int32_t2571135199_Int16_t769030278_Int16_t769030278_RuntimeObject,
	RuntimeInvoker_Void_t1078098495_Int32_t2571135199_Int16_t769030278_Int16_t769030278_SByte_t2530277047_SByte_t2530277047_SByte_t2530277047_SByte_t2530277047_SByte_t2530277047_SByte_t2530277047_SByte_t2530277047_SByte_t2530277047,
	RuntimeInvoker_Int32_t2571135199_Guid_t,
	RuntimeInvoker_Boolean_t2059672899_Guid_t,
	RuntimeInvoker_Guid_t,
	RuntimeInvoker_Void_t1078098495_RuntimeObject_Int16_t769030278,
	RuntimeInvoker_RuntimeObject_SByte_t2530277047_SByte_t2530277047_SByte_t2530277047,
	RuntimeInvoker_Boolean_t2059672899_Guid_t_Guid_t,
	RuntimeInvoker_UInt64_t2265270229_Int32_t2571135199_SByte_t2530277047,
	RuntimeInvoker_Double_t1454265465_Double_t1454265465_Double_t1454265465,
	RuntimeInvoker_TypeAttributes_t3122562390_RuntimeObject,
	RuntimeInvoker_RuntimeObject_SByte_t2530277047_SByte_t2530277047,
	RuntimeInvoker_Void_t1078098495_UInt64U2AU26_t2732919049_Int32U2AU26_t3017422331_CharU2AU26_t2131013796_CharU2AU26_t2131013796_Int64U2AU26_t3036000465_Int32U2AU26_t3017422331,
	RuntimeInvoker_Void_t1078098495_Int32_t2571135199_Int64_t384086013,
	RuntimeInvoker_Void_t1078098495_RuntimeObject_Double_t1454265465_Int32_t2571135199,
	RuntimeInvoker_Void_t1078098495_RuntimeObject_Decimal_t2656901032,
	RuntimeInvoker_RuntimeObject_RuntimeObject_SByte_t2530277047_RuntimeObject,
	RuntimeInvoker_RuntimeObject_RuntimeObject_Int16_t769030278_RuntimeObject,
	RuntimeInvoker_RuntimeObject_RuntimeObject_Int32_t2571135199_RuntimeObject,
	RuntimeInvoker_RuntimeObject_RuntimeObject_Int64_t384086013_RuntimeObject,
	RuntimeInvoker_RuntimeObject_RuntimeObject_Single_t496865882_RuntimeObject,
	RuntimeInvoker_RuntimeObject_RuntimeObject_Double_t1454265465_RuntimeObject,
	RuntimeInvoker_RuntimeObject_RuntimeObject_Decimal_t2656901032_RuntimeObject,
	RuntimeInvoker_RuntimeObject_Single_t496865882_RuntimeObject,
	RuntimeInvoker_RuntimeObject_Double_t1454265465_RuntimeObject,
	RuntimeInvoker_Void_t1078098495_Int32_t2571135199_Int32_t2571135199_RuntimeObject,
	RuntimeInvoker_Void_t1078098495_RuntimeObject_BooleanU26_t495120821_SByte_t2530277047_Int32U26_t2596978809_Int32U26_t2596978809,
	RuntimeInvoker_RuntimeObject_RuntimeObject_Int32_t2571135199_Int32_t2571135199_RuntimeObject_SByte_t2530277047_RuntimeObject_RuntimeObject_RuntimeObject,
	RuntimeInvoker_Void_t1078098495_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199,
	RuntimeInvoker_Int64_t384086013_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199,
	RuntimeInvoker_TimeSpan_t2986675223_TimeSpan_t2986675223,
	RuntimeInvoker_Int32_t2571135199_TimeSpan_t2986675223_TimeSpan_t2986675223,
	RuntimeInvoker_Int32_t2571135199_TimeSpan_t2986675223,
	RuntimeInvoker_Boolean_t2059672899_TimeSpan_t2986675223,
	RuntimeInvoker_TimeSpan_t2986675223_Double_t1454265465,
	RuntimeInvoker_TimeSpan_t2986675223_Double_t1454265465_Int64_t384086013,
	RuntimeInvoker_TimeSpan_t2986675223_TimeSpan_t2986675223_TimeSpan_t2986675223,
	RuntimeInvoker_TimeSpan_t2986675223_DateTime_t507798353,
	RuntimeInvoker_Boolean_t2059672899_DateTime_t507798353_RuntimeObject,
	RuntimeInvoker_DateTime_t507798353_DateTime_t507798353,
	RuntimeInvoker_TimeSpan_t2986675223_DateTime_t507798353_TimeSpan_t2986675223,
	RuntimeInvoker_Boolean_t2059672899_Int32_t2571135199_Int64U5BU5DU26_t3921029648_StringU5BU5DU26_t4251177155,
	RuntimeInvoker_Void_t1078098495_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199,
	RuntimeInvoker_RuntimeObject_RuntimeObject_Int32_t2571135199_RuntimeObject_RuntimeObject,
	RuntimeInvoker_RuntimeObject_RuntimeObject_DictionaryNodeU26_t2493955228,
	RuntimeInvoker_Boolean_t2059672899_RuntimeObject_RuntimeObject_RuntimeObject_Int32_t2571135199,
	RuntimeInvoker_Boolean_t2059672899_RuntimeObject_IPAddressU26_t2403452733,
	RuntimeInvoker_AddressFamily_t4183665415,
	RuntimeInvoker_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199,
	RuntimeInvoker_Boolean_t2059672899_RuntimeObject_IPv6AddressU26_t126668491,
	RuntimeInvoker_SecurityProtocolType_t26325249,
	RuntimeInvoker_Void_t1078098495_SByte_t2530277047_SByte_t2530277047_Int32_t2571135199_SByte_t2530277047,
	RuntimeInvoker_AsnDecodeStatus_t4190820181_RuntimeObject,
	RuntimeInvoker_RuntimeObject_Int32_t2571135199_RuntimeObject_SByte_t2530277047,
	RuntimeInvoker_X509ChainStatusFlags_t2184184987_RuntimeObject,
	RuntimeInvoker_X509ChainStatusFlags_t2184184987_RuntimeObject_Int32_t2571135199_SByte_t2530277047,
	RuntimeInvoker_X509ChainStatusFlags_t2184184987_RuntimeObject_RuntimeObject_SByte_t2530277047,
	RuntimeInvoker_X509ChainStatusFlags_t2184184987,
	RuntimeInvoker_Void_t1078098495_RuntimeObject_Int32U26_t2596978809_Int32_t2571135199_Int32_t2571135199,
	RuntimeInvoker_X509RevocationFlag_t3762606324,
	RuntimeInvoker_X509RevocationMode_t2217493564,
	RuntimeInvoker_X509VerificationFlags_t1372417108,
	RuntimeInvoker_X509KeyUsageFlags_t1189859534,
	RuntimeInvoker_X509KeyUsageFlags_t1189859534_Int32_t2571135199,
	RuntimeInvoker_Void_t1078098495_RuntimeObject_Int32_t2571135199_SByte_t2530277047,
	RuntimeInvoker_Byte_t880488320_Int16_t769030278_Int16_t769030278,
	RuntimeInvoker_Void_t1078098495_RuntimeObject_RuntimeObject_RuntimeObject_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199,
	RuntimeInvoker_Void_t1078098495_RuntimeObject_RuntimeObject_RuntimeObject_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199,
	RuntimeInvoker_Category_t2116501226_RuntimeObject,
	RuntimeInvoker_Boolean_t2059672899_UInt16_t11709673_Int16_t769030278,
	RuntimeInvoker_Boolean_t2059672899_Int32_t2571135199_Int16_t769030278,
	RuntimeInvoker_Void_t1078098495_Int16_t769030278_SByte_t2530277047_SByte_t2530277047_SByte_t2530277047,
	RuntimeInvoker_Void_t1078098495_UInt16_t11709673_SByte_t2530277047_SByte_t2530277047,
	RuntimeInvoker_Void_t1078098495_Int16_t769030278_Int16_t769030278_SByte_t2530277047_SByte_t2530277047_SByte_t2530277047,
	RuntimeInvoker_Void_t1078098495_Int16_t769030278_RuntimeObject_SByte_t2530277047_SByte_t2530277047_SByte_t2530277047,
	RuntimeInvoker_Void_t1078098495_UInt16_t11709673,
	RuntimeInvoker_Void_t1078098495_Int32_t2571135199_Int32_t2571135199_SByte_t2530277047_RuntimeObject,
	RuntimeInvoker_Void_t1078098495_SByte_t2530277047_Int32_t2571135199_RuntimeObject,
	RuntimeInvoker_UInt16_t11709673_UInt16_t11709673_UInt16_t11709673,
	RuntimeInvoker_OpFlags_t3924773783_SByte_t2530277047_SByte_t2530277047_SByte_t2530277047_SByte_t2530277047,
	RuntimeInvoker_Void_t1078098495_UInt16_t11709673_UInt16_t11709673,
	RuntimeInvoker_Boolean_t2059672899_Int32_t2571135199_Int32U26_t2596978809_Int32_t2571135199,
	RuntimeInvoker_Boolean_t2059672899_Int32_t2571135199_Int32U26_t2596978809_Int32U26_t2596978809_SByte_t2530277047,
	RuntimeInvoker_Boolean_t2059672899_Int32U26_t2596978809_Int32_t2571135199,
	RuntimeInvoker_Boolean_t2059672899_UInt16_t11709673_Int32_t2571135199,
	RuntimeInvoker_Boolean_t2059672899_Int32_t2571135199_Int32_t2571135199_SByte_t2530277047_Int32_t2571135199,
	RuntimeInvoker_Void_t1078098495_Int32_t2571135199_Int32U26_t2596978809_Int32U26_t2596978809,
	RuntimeInvoker_Void_t1078098495_RuntimeObject_Int32_t2571135199_Int32_t2571135199_SByte_t2530277047_Int32_t2571135199,
	RuntimeInvoker_Interval_t797373189,
	RuntimeInvoker_Boolean_t2059672899_Interval_t797373189,
	RuntimeInvoker_Void_t1078098495_Interval_t797373189,
	RuntimeInvoker_Interval_t797373189_Int32_t2571135199,
	RuntimeInvoker_Void_t1078098495_Int32_t2571135199_Int32_t2571135199_RuntimeObject_RuntimeObject,
	RuntimeInvoker_Double_t1454265465_Interval_t797373189,
	RuntimeInvoker_RuntimeObject_Interval_t797373189_RuntimeObject_RuntimeObject,
	RuntimeInvoker_Int32_t2571135199_RuntimeObject_Int32U26_t2596978809_Int32_t2571135199,
	RuntimeInvoker_Int32_t2571135199_RuntimeObject_Int32U26_t2596978809_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199,
	RuntimeInvoker_RuntimeObject_RuntimeObject_Int32U26_t2596978809,
	RuntimeInvoker_RuntimeObject_RegexOptionsU26_t416869168,
	RuntimeInvoker_Void_t1078098495_RegexOptionsU26_t416869168_SByte_t2530277047,
	RuntimeInvoker_Boolean_t2059672899_Int32U26_t2596978809_Int32U26_t2596978809_Int32_t2571135199,
	RuntimeInvoker_Category_t2116501226,
	RuntimeInvoker_Void_t1078098495_Int32U26_t2596978809_Int32U26_t2596978809,
	RuntimeInvoker_Void_t1078098495_Int32_t2571135199_Int32_t2571135199_SByte_t2530277047,
	RuntimeInvoker_Void_t1078098495_RuntimeObject_RuntimeObject_SByte_t2530277047_SByte_t2530277047,
	RuntimeInvoker_Void_t1078098495_UInt16_t11709673_SByte_t2530277047,
	RuntimeInvoker_Void_t1078098495_Int16_t769030278_Int16_t769030278,
	RuntimeInvoker_Void_t1078098495_RuntimeObject_Int32_t2571135199_Int32_t2571135199_RuntimeObject_SByte_t2530277047,
	RuntimeInvoker_Void_t1078098495_RuntimeObject_Int32_t2571135199_Int32_t2571135199_UInt16_t11709673,
	RuntimeInvoker_Position_t3652736430,
	RuntimeInvoker_UriHostNameType_t1837236037_RuntimeObject,
	RuntimeInvoker_Void_t1078098495_StringU26_t2155547156,
	RuntimeInvoker_RuntimeObject_RuntimeObject_SByte_t2530277047_SByte_t2530277047_SByte_t2530277047,
	RuntimeInvoker_Char_t3572527572_RuntimeObject_Int32U26_t2596978809_CharU26_t2918757708,
	RuntimeInvoker_Void_t1078098495_RuntimeObject_UriFormatExceptionU26_t3440399237,
	RuntimeInvoker_RuntimeObject_RuntimeObject_RuntimeObject_RuntimeObject_Int32_t2571135199_RuntimeObject_RuntimeObject,
	RuntimeInvoker_Sign_t3270271765_RuntimeObject_RuntimeObject,
	RuntimeInvoker_ConfidenceFactor_t3678829216,
	RuntimeInvoker_UInt32_t2739056616_Int32_t2571135199_SByte_t2530277047,
	RuntimeInvoker_Void_t1078098495_UInt32U26_t3679184984_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199_SByte_t2530277047,
	RuntimeInvoker_X509ChainStatusFlags_t991867662,
	RuntimeInvoker_Void_t1078098495_Byte_t880488320,
	RuntimeInvoker_Void_t1078098495_Byte_t880488320_Byte_t880488320,
	RuntimeInvoker_AlertLevel_t912398071,
	RuntimeInvoker_AlertDescription_t952063977,
	RuntimeInvoker_RuntimeObject_Byte_t880488320,
	RuntimeInvoker_Void_t1078098495_Int16_t769030278_RuntimeObject_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199_SByte_t2530277047_SByte_t2530277047_SByte_t2530277047_SByte_t2530277047_Int16_t769030278_SByte_t2530277047_SByte_t2530277047,
	RuntimeInvoker_CipherAlgorithmType_t3078901768,
	RuntimeInvoker_HashAlgorithmType_t2406960816,
	RuntimeInvoker_ExchangeAlgorithmType_t608439419,
	RuntimeInvoker_Void_t1078098495_RuntimeObject_Int32_t2571135199_Int16_t769030278,
	RuntimeInvoker_Void_t1078098495_RuntimeObject_Int32_t2571135199_Int64_t384086013,
	RuntimeInvoker_Void_t1078098495_RuntimeObject_ByteU5BU5DU26_t321522471_ByteU5BU5DU26_t321522471,
	RuntimeInvoker_RuntimeObject_Byte_t880488320_RuntimeObject,
	RuntimeInvoker_RuntimeObject_RuntimeObject_RuntimeObject_RuntimeObject_Int32_t2571135199,
	RuntimeInvoker_RuntimeObject_Int16_t769030278_RuntimeObject_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199_SByte_t2530277047_SByte_t2530277047_SByte_t2530277047_SByte_t2530277047_Int16_t769030278_SByte_t2530277047_SByte_t2530277047,
	RuntimeInvoker_Void_t1078098495_RuntimeObject_Int32_t2571135199_RuntimeObject_RuntimeObject,
	RuntimeInvoker_SecurityProtocolType_t4172395504,
	RuntimeInvoker_SecurityCompressionType_t1053362341,
	RuntimeInvoker_HandshakeType_t2678014388,
	RuntimeInvoker_HandshakeState_t3800758483,
	RuntimeInvoker_SecurityProtocolType_t4172395504_Int16_t769030278,
	RuntimeInvoker_Void_t1078098495_RuntimeObject_RuntimeObject_RuntimeObject_RuntimeObject,
	RuntimeInvoker_RuntimeObject_Byte_t880488320_RuntimeObject_RuntimeObject,
	RuntimeInvoker_RuntimeObject_Byte_t880488320_RuntimeObject_RuntimeObject_RuntimeObject,
	RuntimeInvoker_Void_t1078098495_Byte_t880488320_RuntimeObject,
	RuntimeInvoker_RuntimeObject_Byte_t880488320_RuntimeObject_Int32_t2571135199_Int32_t2571135199,
	RuntimeInvoker_Void_t1078098495_RuntimeObject_RuntimeObject_SByte_t2530277047_Int32_t2571135199_RuntimeObject,
	RuntimeInvoker_Void_t1078098495_RuntimeObject_RuntimeObject_RuntimeObject_Int32_t2571135199_Int32_t2571135199_SByte_t2530277047_SByte_t2530277047,
	RuntimeInvoker_Void_t1078098495_Byte_t880488320_Byte_t880488320_RuntimeObject,
	RuntimeInvoker_RSAParameters_t2749614896,
	RuntimeInvoker_Void_t1078098495_RuntimeObject_Byte_t880488320_Byte_t880488320,
	RuntimeInvoker_Void_t1078098495_RuntimeObject_Byte_t880488320_RuntimeObject,
	RuntimeInvoker_ContentType_t3977715531,
	RuntimeInvoker_RuntimeObject_RuntimeObject_RuntimeObject_RuntimeObject_RuntimeObject_RuntimeObject_RuntimeObject,
	RuntimeInvoker_RuntimeObject_RuntimeObject_RuntimeObject_Int32_t2571135199_RuntimeObject_RuntimeObject,
	RuntimeInvoker_Rect_t1111852907,
	RuntimeInvoker_Void_t1078098495_RectU26_t2812713421,
	RuntimeInvoker_CameraClearFlags_t4003071365,
	RuntimeInvoker_Ray_t1113537957_Vector3_t3239555143,
	RuntimeInvoker_Void_t1078098495_RuntimeObject_Vector3U26_t865664337_RayU26_t3990586275,
	RuntimeInvoker_RuntimeObject_Ray_t1113537957_Single_t496865882_Int32_t2571135199,
	RuntimeInvoker_RuntimeObject_RuntimeObject_RayU26_t3990586275_Single_t496865882_Int32_t2571135199,
	RuntimeInvoker_Void_t1078098495_RuntimeObject_IntPtr_t_Int32_t2571135199,
	RuntimeInvoker_Void_t1078098495_CullingGroupEvent_t157813866,
	RuntimeInvoker_RuntimeObject_CullingGroupEvent_t157813866_RuntimeObject_RuntimeObject,
	RuntimeInvoker_Void_t1078098495_Int32_t2571135199_RuntimeObject_RuntimeObject_RuntimeObject,
	RuntimeInvoker_RuntimeObject_Vector3_t3239555143,
	RuntimeInvoker_RuntimeObject_RuntimeObject_Vector3U26_t865664337,
	RuntimeInvoker_Vector3_t3239555143,
	RuntimeInvoker_Void_t1078098495_Vector3U26_t865664337,
	RuntimeInvoker_Void_t1078098495_Single_t496865882_Single_t496865882_Single_t496865882,
	RuntimeInvoker_Void_t1078098495_Vector4_t4052901136_Vector4_t4052901136_Vector4_t4052901136_Vector4_t4052901136,
	RuntimeInvoker_Vector4_t4052901136_Int32_t2571135199,
	RuntimeInvoker_Matrix4x4_t3534180298,
	RuntimeInvoker_Single_t496865882_Single_t496865882_Single_t496865882,
	RuntimeInvoker_Boolean_t2059672899_Single_t496865882_Single_t496865882,
	RuntimeInvoker_Void_t1078098495_RuntimeObject_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199_SByte_t2530277047_SByte_t2530277047_IntPtr_t,
	RuntimeInvoker_PlayableHandle_t3938927722,
	RuntimeInvoker_Boolean_t2059672899_PlayableHandle_t3938927722_PlayableHandle_t3938927722,
	RuntimeInvoker_Void_t1078098495_PlayableHandle_t3938927722,
	RuntimeInvoker_Playable_t1760619938,
	RuntimeInvoker_Boolean_t2059672899_Playable_t1760619938,
	RuntimeInvoker_PlayableOutputHandle_t2719521870,
	RuntimeInvoker_Boolean_t2059672899_PlayableOutputHandle_t2719521870_PlayableOutputHandle_t2719521870,
	RuntimeInvoker_Void_t1078098495_PlayableOutputHandle_t2719521870,
	RuntimeInvoker_Boolean_t2059672899_PlayableOutput_t3840687608,
	RuntimeInvoker_Void_t1078098495_Scene_t3023997566_Int32_t2571135199,
	RuntimeInvoker_Void_t1078098495_Scene_t3023997566,
	RuntimeInvoker_Void_t1078098495_Scene_t3023997566_Scene_t3023997566,
	RuntimeInvoker_Void_t1078098495_Single_t496865882_Single_t496865882_Single_t496865882_Single_t496865882,
	RuntimeInvoker_Color_t4024113822,
	RuntimeInvoker_Vector4_t4052901136_Color_t4024113822,
	RuntimeInvoker_Void_t1078098495_RuntimeObject_Int32_t2571135199_Int32_t2571135199_RuntimeObject_IntPtr_t,
	RuntimeInvoker_Void_t1078098495_Int64_t384086013_Int32_t2571135199_RuntimeObject_IntPtr_t,
	RuntimeInvoker_Void_t1078098495_RuntimeObject_Vector3_t3239555143_HitInfoU26_t2509763475,
	RuntimeInvoker_Void_t1078098495_Int32_t2571135199_HitInfo_t184839221,
	RuntimeInvoker_Boolean_t2059672899_HitInfo_t184839221,
	RuntimeInvoker_Boolean_t2059672899_HitInfo_t184839221_HitInfo_t184839221,
	RuntimeInvoker_PropertyName_t666104458_RuntimeObject,
	RuntimeInvoker_Void_t1078098495_RuntimeObject_PropertyNameU26_t334041606,
	RuntimeInvoker_Void_t1078098495_PropertyName_t666104458,
	RuntimeInvoker_Boolean_t2059672899_PropertyName_t666104458_PropertyName_t666104458,
	RuntimeInvoker_PropertyName_t666104458_Int32_t2571135199,
	RuntimeInvoker_Boolean_t2059672899_Vector3_t3239555143,
	RuntimeInvoker_Void_t1078098495_RuntimeObject_StringU26_t2155547156_StringU26_t2155547156,
	RuntimeInvoker_PersistentListenerMode_t677115295,
	RuntimeInvoker_Playable_t1760619938_PlayableGraph_t1897216728_RuntimeObject,
	RuntimeInvoker_Void_t1078098495_RuntimeObject_PlayableGraph_t1897216728_RuntimeObject_IntPtr_t,
	RuntimeInvoker_LogType_t1259498875,
	RuntimeInvoker_Void_t1078098495_IntPtr_t_Int64_t384086013_Int64_t384086013_RuntimeObject,
	RuntimeInvoker_Void_t1078098495_Guid_t_RuntimeObject_Int32_t2571135199,
	RuntimeInvoker_Void_t1078098495_ScriptableRenderContext_t1486166028_RuntimeObject,
	RuntimeInvoker_Void_t1078098495_RuntimeObject_RuntimeObject_IntPtr_t,
	RuntimeInvoker_CSSSize_t2436738465_IntPtr_t_Single_t496865882_Int32_t2571135199_Single_t496865882_Int32_t2571135199,
	RuntimeInvoker_RuntimeObject_IntPtr_t_Single_t496865882_Int32_t2571135199_Single_t496865882_Int32_t2571135199_RuntimeObject_RuntimeObject,
	RuntimeInvoker_CSSSize_t2436738465_RuntimeObject,
	RuntimeInvoker_Void_t1078098495_IntPtr_t_Single_t496865882_Int32_t2571135199_Single_t496865882_Int32_t2571135199_IntPtr_t,
	RuntimeInvoker_RuntimeObject_SByte_t2530277047_RuntimeObject_RuntimeObject,
	RuntimeInvoker_RuntimeObject_Int32_t2571135199_RuntimeObject_RuntimeObject,
	RuntimeInvoker_Void_t1078098495_RuntimeObject_Int32_t2571135199_PropertyNameU26_t334041606,
	RuntimeInvoker_Boolean_t2059672899_AudioMixerPlayable_t1017334687,
	RuntimeInvoker_Boolean_t2059672899_AudioClipPlayable_t3812371941,
	RuntimeInvoker_Void_t1078098495_PropertyName_t666104458_Single_t496865882,
	RuntimeInvoker_Void_t1078098495_RuntimeObject_Double_t1454265465_RuntimeObject,
	RuntimeInvoker_Void_t1078098495_Int64_t384086013_RuntimeObject_RuntimeObject,
	RuntimeInvoker_Void_t1078098495_GcAchievementDescriptionData_t2898970914_Int32_t2571135199,
	RuntimeInvoker_Void_t1078098495_GcUserProfileData_t3262991188_Int32_t2571135199,
	RuntimeInvoker_Void_t1078098495_UserProfileU5BU5DU26_t2890929411_RuntimeObject_Int32_t2571135199,
	RuntimeInvoker_Void_t1078098495_UserProfileU5BU5DU26_t2890929411_Int32_t2571135199,
	RuntimeInvoker_Void_t1078098495_GcScoreData_t3955401213,
	RuntimeInvoker_Void_t1078098495_RuntimeObject_Int32_t2571135199_Int32_t2571135199_RuntimeObject_Int32_t2571135199_Int32_t2571135199_RuntimeObject,
	RuntimeInvoker_UserState_t707530005,
	RuntimeInvoker_Void_t1078098495_RuntimeObject_Double_t1454265465_SByte_t2530277047_SByte_t2530277047_DateTime_t507798353,
	RuntimeInvoker_Void_t1078098495_RuntimeObject_Double_t1454265465,
	RuntimeInvoker_Void_t1078098495_RuntimeObject_RuntimeObject_RuntimeObject_RuntimeObject_RuntimeObject_SByte_t2530277047_Int32_t2571135199,
	RuntimeInvoker_Void_t1078098495_RuntimeObject_Int64_t384086013_RuntimeObject_DateTime_t507798353_RuntimeObject_Int32_t2571135199,
	RuntimeInvoker_UserScope_t2912238712,
	RuntimeInvoker_Range_t1266129112,
	RuntimeInvoker_Void_t1078098495_Range_t1266129112,
	RuntimeInvoker_TimeScope_t1786414419,
	RuntimeInvoker_Boolean_t2059672899_RuntimeObject_Int16_t769030278,
	RuntimeInvoker_Boolean_t2059672899_RuntimeObject_Int64_t384086013,
	RuntimeInvoker_Boolean_t2059672899_RuntimeObject_Double_t1454265465,
	RuntimeInvoker_Boolean_t2059672899_RuntimeObject_Single_t496865882,
	RuntimeInvoker_Boolean_t2059672899_RuntimeObject_Decimal_t2656901032,
	RuntimeInvoker_AnalyticsResult_t1606825939_RuntimeObject,
	RuntimeInvoker_AnalyticsResult_t1606825939_RuntimeObject_RuntimeObject,
	RuntimeInvoker_Void_t1078098495_Int32_t2571135199_Int64_t384086013_Int64_t384086013_SByte_t2530277047,
	RuntimeInvoker_RuntimeObject_Int32_t2571135199_Int64_t384086013_Int64_t384086013_SByte_t2530277047_RuntimeObject_RuntimeObject,
	RuntimeInvoker_Void_t1078098495_Int32_t2571135199_ObjectU26_t4136591053,
	RuntimeInvoker_Void_t1078098495_ObjectU5BU5DU26_t3608108294_Int32_t2571135199,
	RuntimeInvoker_Void_t1078098495_ObjectU5BU5DU26_t3608108294_Int32_t2571135199_Int32_t2571135199,
	RuntimeInvoker_Void_t1078098495_KeyValuePair_2_t3602437993,
	RuntimeInvoker_Boolean_t2059672899_KeyValuePair_2_t3602437993,
	RuntimeInvoker_KeyValuePair_2_t3602437993_RuntimeObject_RuntimeObject,
	RuntimeInvoker_Boolean_t2059672899_RuntimeObject_ObjectU26_t4136591053,
	RuntimeInvoker_Enumerator_t2164740473,
	RuntimeInvoker_DictionaryEntry_t1385909157_RuntimeObject_RuntimeObject,
	RuntimeInvoker_KeyValuePair_2_t3602437993,
	RuntimeInvoker_Enumerator_t2934400376,
	RuntimeInvoker_RuntimeObject_ObjectU26_t4136591053_RuntimeObject_RuntimeObject,
	RuntimeInvoker_Enumerator_t4228099087,
	RuntimeInvoker_Enumerator_t1041909981,
	RuntimeInvoker_RuntimeObject_RuntimeObject_RuntimeObject_RuntimeObject_RuntimeObject_RuntimeObject,
	RuntimeInvoker_Boolean_t2059672899_IntPtr_t_ObjectU26_t4136591053,
	RuntimeInvoker_Void_t1078098495_RuntimeObject_RuntimeObject_Single_t496865882,
	RuntimeInvoker_WorkRequest_t2547429811,
	RuntimeInvoker_Boolean_t2059672899_TableRange_t4125455382,
	RuntimeInvoker_Boolean_t2059672899_DictionaryEntry_t1385909157,
	RuntimeInvoker_Boolean_t2059672899_KeyValuePair_2_t2204764377,
	RuntimeInvoker_Boolean_t2059672899_KeyValuePair_2_t2366093889,
	RuntimeInvoker_Boolean_t2059672899_KeyValuePair_2_t2877556189,
	RuntimeInvoker_Boolean_t2059672899_Link_t3525875035,
	RuntimeInvoker_Boolean_t2059672899_Slot_t2102621701,
	RuntimeInvoker_Boolean_t2059672899_Slot_t1141617207,
	RuntimeInvoker_Boolean_t2059672899_CustomAttributeNamedArgument_t425730339,
	RuntimeInvoker_Boolean_t2059672899_CustomAttributeTypedArgument_t2667702721,
	RuntimeInvoker_Boolean_t2059672899_LabelData_t1059919410,
	RuntimeInvoker_Boolean_t2059672899_LabelFixup_t2685311059,
	RuntimeInvoker_Boolean_t2059672899_ILTokenInfo_t281295013,
	RuntimeInvoker_Boolean_t2059672899_MonoResource_t2965441204,
	RuntimeInvoker_Boolean_t2059672899_RefEmitPermissionSet_t945207422,
	RuntimeInvoker_Boolean_t2059672899_ParameterModifier_t969812002,
	RuntimeInvoker_Boolean_t2059672899_ResourceCacheItem_t3632973466,
	RuntimeInvoker_Boolean_t2059672899_ResourceInfo_t1773833009,
	RuntimeInvoker_Boolean_t2059672899_Byte_t880488320,
	RuntimeInvoker_Boolean_t2059672899_X509ChainStatus_t177750891,
	RuntimeInvoker_Boolean_t2059672899_Mark_t4050795874,
	RuntimeInvoker_Boolean_t2059672899_UriScheme_t2622429923,
	RuntimeInvoker_Boolean_t2059672899_Keyframe_t3396755990,
	RuntimeInvoker_Boolean_t2059672899_PlayableBinding_t136667248,
	RuntimeInvoker_Boolean_t2059672899_GcAchievementData_t3167706508,
	RuntimeInvoker_Boolean_t2059672899_GcScoreData_t3955401213,
	RuntimeInvoker_Boolean_t2059672899_WorkRequest_t2547429811,
	RuntimeInvoker_Int32_t2571135199_RuntimeObject_Int32_t2571135199_Int32_t2571135199_Int32_t2571135199_RuntimeObject,
	RuntimeInvoker_Int32_t2571135199_RuntimeObject_CustomAttributeNamedArgument_t425730339_Int32_t2571135199_Int32_t2571135199,
	RuntimeInvoker_Int32_t2571135199_RuntimeObject_CustomAttributeNamedArgument_t425730339,
	RuntimeInvoker_Int32_t2571135199_RuntimeObject_CustomAttributeTypedArgument_t2667702721_Int32_t2571135199_Int32_t2571135199,
	RuntimeInvoker_Int32_t2571135199_RuntimeObject_CustomAttributeTypedArgument_t2667702721,
	RuntimeInvoker_Int32_t2571135199_TableRange_t4125455382,
	RuntimeInvoker_Int32_t2571135199_DictionaryEntry_t1385909157,
	RuntimeInvoker_Int32_t2571135199_KeyValuePair_2_t2204764377,
	RuntimeInvoker_Int32_t2571135199_KeyValuePair_2_t2366093889,
	RuntimeInvoker_Int32_t2571135199_KeyValuePair_2_t2877556189,
	RuntimeInvoker_Int32_t2571135199_KeyValuePair_2_t3602437993,
	RuntimeInvoker_Int32_t2571135199_Link_t3525875035,
	RuntimeInvoker_Int32_t2571135199_Slot_t2102621701,
	RuntimeInvoker_Int32_t2571135199_Slot_t1141617207,
	RuntimeInvoker_Int32_t2571135199_CustomAttributeNamedArgument_t425730339,
	RuntimeInvoker_Int32_t2571135199_CustomAttributeTypedArgument_t2667702721,
	RuntimeInvoker_Int32_t2571135199_LabelData_t1059919410,
	RuntimeInvoker_Int32_t2571135199_LabelFixup_t2685311059,
	RuntimeInvoker_Int32_t2571135199_ILTokenInfo_t281295013,
	RuntimeInvoker_Int32_t2571135199_MonoResource_t2965441204,
	RuntimeInvoker_Int32_t2571135199_RefEmitPermissionSet_t945207422,
	RuntimeInvoker_Int32_t2571135199_ParameterModifier_t969812002,
	RuntimeInvoker_Int32_t2571135199_ResourceCacheItem_t3632973466,
	RuntimeInvoker_Int32_t2571135199_ResourceInfo_t1773833009,
	RuntimeInvoker_Int32_t2571135199_Byte_t880488320,
	RuntimeInvoker_Int32_t2571135199_X509ChainStatus_t177750891,
	RuntimeInvoker_Int32_t2571135199_Mark_t4050795874,
	RuntimeInvoker_Int32_t2571135199_UriScheme_t2622429923,
	RuntimeInvoker_Int32_t2571135199_Keyframe_t3396755990,
	RuntimeInvoker_Int32_t2571135199_PlayableBinding_t136667248,
	RuntimeInvoker_Int32_t2571135199_HitInfo_t184839221,
	RuntimeInvoker_Int32_t2571135199_GcAchievementData_t3167706508,
	RuntimeInvoker_Int32_t2571135199_GcScoreData_t3955401213,
	RuntimeInvoker_Int32_t2571135199_WorkRequest_t2547429811,
	RuntimeInvoker_Void_t1078098495_TableRange_t4125455382,
	RuntimeInvoker_Void_t1078098495_DictionaryEntry_t1385909157,
	RuntimeInvoker_Void_t1078098495_KeyValuePair_2_t2204764377,
	RuntimeInvoker_Void_t1078098495_KeyValuePair_2_t2366093889,
	RuntimeInvoker_Void_t1078098495_KeyValuePair_2_t2877556189,
	RuntimeInvoker_Void_t1078098495_Link_t3525875035,
	RuntimeInvoker_Void_t1078098495_Slot_t2102621701,
	RuntimeInvoker_Void_t1078098495_Slot_t1141617207,
	RuntimeInvoker_Void_t1078098495_Decimal_t2656901032,
	RuntimeInvoker_Void_t1078098495_CustomAttributeNamedArgument_t425730339,
	RuntimeInvoker_Void_t1078098495_CustomAttributeTypedArgument_t2667702721,
	RuntimeInvoker_Void_t1078098495_LabelData_t1059919410,
	RuntimeInvoker_Void_t1078098495_LabelFixup_t2685311059,
	RuntimeInvoker_Void_t1078098495_ILTokenInfo_t281295013,
	RuntimeInvoker_Void_t1078098495_MonoResource_t2965441204,
	RuntimeInvoker_Void_t1078098495_RefEmitPermissionSet_t945207422,
	RuntimeInvoker_Void_t1078098495_ParameterModifier_t969812002,
	RuntimeInvoker_Void_t1078098495_ResourceCacheItem_t3632973466,
	RuntimeInvoker_Void_t1078098495_ResourceInfo_t1773833009,
	RuntimeInvoker_Void_t1078098495_X509ChainStatus_t177750891,
	RuntimeInvoker_Void_t1078098495_Mark_t4050795874,
	RuntimeInvoker_Void_t1078098495_UriScheme_t2622429923,
	RuntimeInvoker_Void_t1078098495_Keyframe_t3396755990,
	RuntimeInvoker_Void_t1078098495_PlayableBinding_t136667248,
	RuntimeInvoker_Void_t1078098495_HitInfo_t184839221,
	RuntimeInvoker_Void_t1078098495_GcAchievementData_t3167706508,
	RuntimeInvoker_Void_t1078098495_WorkRequest_t2547429811,
	RuntimeInvoker_Void_t1078098495_Int32_t2571135199_TableRange_t4125455382,
	RuntimeInvoker_Void_t1078098495_Int32_t2571135199_DictionaryEntry_t1385909157,
	RuntimeInvoker_Void_t1078098495_Int32_t2571135199_KeyValuePair_2_t2204764377,
	RuntimeInvoker_Void_t1078098495_Int32_t2571135199_KeyValuePair_2_t2366093889,
	RuntimeInvoker_Void_t1078098495_Int32_t2571135199_KeyValuePair_2_t2877556189,
	RuntimeInvoker_Void_t1078098495_Int32_t2571135199_KeyValuePair_2_t3602437993,
	RuntimeInvoker_Void_t1078098495_Int32_t2571135199_Link_t3525875035,
	RuntimeInvoker_Void_t1078098495_Int32_t2571135199_Slot_t2102621701,
	RuntimeInvoker_Void_t1078098495_Int32_t2571135199_Slot_t1141617207,
	RuntimeInvoker_Void_t1078098495_Int32_t2571135199_DateTime_t507798353,
	RuntimeInvoker_Void_t1078098495_Int32_t2571135199_Decimal_t2656901032,
	RuntimeInvoker_Void_t1078098495_Int32_t2571135199_Double_t1454265465,
	RuntimeInvoker_Void_t1078098495_Int32_t2571135199_IntPtr_t,
	RuntimeInvoker_Void_t1078098495_Int32_t2571135199_CustomAttributeNamedArgument_t425730339,
	RuntimeInvoker_Void_t1078098495_Int32_t2571135199_CustomAttributeTypedArgument_t2667702721,
	RuntimeInvoker_Void_t1078098495_Int32_t2571135199_LabelData_t1059919410,
	RuntimeInvoker_Void_t1078098495_Int32_t2571135199_LabelFixup_t2685311059,
	RuntimeInvoker_Void_t1078098495_Int32_t2571135199_ILTokenInfo_t281295013,
	RuntimeInvoker_Void_t1078098495_Int32_t2571135199_MonoResource_t2965441204,
	RuntimeInvoker_Void_t1078098495_Int32_t2571135199_RefEmitPermissionSet_t945207422,
	RuntimeInvoker_Void_t1078098495_Int32_t2571135199_ParameterModifier_t969812002,
	RuntimeInvoker_Void_t1078098495_Int32_t2571135199_ResourceCacheItem_t3632973466,
	RuntimeInvoker_Void_t1078098495_Int32_t2571135199_ResourceInfo_t1773833009,
	RuntimeInvoker_Void_t1078098495_Int32_t2571135199_Byte_t880488320,
	RuntimeInvoker_Void_t1078098495_Int32_t2571135199_X509ChainStatus_t177750891,
	RuntimeInvoker_Void_t1078098495_Int32_t2571135199_Mark_t4050795874,
	RuntimeInvoker_Void_t1078098495_Int32_t2571135199_TimeSpan_t2986675223,
	RuntimeInvoker_Void_t1078098495_Int32_t2571135199_UriScheme_t2622429923,
	RuntimeInvoker_Void_t1078098495_Int32_t2571135199_Keyframe_t3396755990,
	RuntimeInvoker_Void_t1078098495_Int32_t2571135199_PlayableBinding_t136667248,
	RuntimeInvoker_Void_t1078098495_Int32_t2571135199_GcAchievementData_t3167706508,
	RuntimeInvoker_Void_t1078098495_Int32_t2571135199_GcScoreData_t3955401213,
	RuntimeInvoker_Void_t1078098495_Int32_t2571135199_WorkRequest_t2547429811,
	RuntimeInvoker_Void_t1078098495_Int32U5BU5DU26_t463651626_Int32_t2571135199_Int32_t2571135199,
	RuntimeInvoker_Void_t1078098495_Int32U5BU5DU26_t463651626_Int32_t2571135199,
	RuntimeInvoker_Void_t1078098495_CustomAttributeNamedArgumentU5BU5DU26_t4027732446_Int32_t2571135199_Int32_t2571135199,
	RuntimeInvoker_Void_t1078098495_CustomAttributeNamedArgumentU5BU5DU26_t4027732446_Int32_t2571135199,
	RuntimeInvoker_Void_t1078098495_CustomAttributeTypedArgumentU5BU5DU26_t2921661508_Int32_t2571135199_Int32_t2571135199,
	RuntimeInvoker_Void_t1078098495_CustomAttributeTypedArgumentU5BU5DU26_t2921661508_Int32_t2571135199,
	RuntimeInvoker_TableRange_t4125455382_Int32_t2571135199,
	RuntimeInvoker_ClientCertificateType_t194976790_Int32_t2571135199,
	RuntimeInvoker_DictionaryEntry_t1385909157_Int32_t2571135199,
	RuntimeInvoker_KeyValuePair_2_t2204764377_Int32_t2571135199,
	RuntimeInvoker_KeyValuePair_2_t2366093889_Int32_t2571135199,
	RuntimeInvoker_KeyValuePair_2_t2877556189_Int32_t2571135199,
	RuntimeInvoker_KeyValuePair_2_t3602437993_Int32_t2571135199,
	RuntimeInvoker_Link_t3525875035_Int32_t2571135199,
	RuntimeInvoker_Slot_t2102621701_Int32_t2571135199,
	RuntimeInvoker_Slot_t1141617207_Int32_t2571135199,
	RuntimeInvoker_CustomAttributeNamedArgument_t425730339_Int32_t2571135199,
	RuntimeInvoker_CustomAttributeTypedArgument_t2667702721_Int32_t2571135199,
	RuntimeInvoker_LabelData_t1059919410_Int32_t2571135199,
	RuntimeInvoker_LabelFixup_t2685311059_Int32_t2571135199,
	RuntimeInvoker_ILTokenInfo_t281295013_Int32_t2571135199,
	RuntimeInvoker_MonoResource_t2965441204_Int32_t2571135199,
	RuntimeInvoker_RefEmitPermissionSet_t945207422_Int32_t2571135199,
	RuntimeInvoker_ParameterModifier_t969812002_Int32_t2571135199,
	RuntimeInvoker_ResourceCacheItem_t3632973466_Int32_t2571135199,
	RuntimeInvoker_ResourceInfo_t1773833009_Int32_t2571135199,
	RuntimeInvoker_TypeTag_t1137063594_Int32_t2571135199,
	RuntimeInvoker_X509ChainStatus_t177750891_Int32_t2571135199,
	RuntimeInvoker_Mark_t4050795874_Int32_t2571135199,
	RuntimeInvoker_TimeSpan_t2986675223_Int32_t2571135199,
	RuntimeInvoker_UriScheme_t2622429923_Int32_t2571135199,
	RuntimeInvoker_Keyframe_t3396755990_Int32_t2571135199,
	RuntimeInvoker_PlayableBinding_t136667248_Int32_t2571135199,
	RuntimeInvoker_HitInfo_t184839221_Int32_t2571135199,
	RuntimeInvoker_GcAchievementData_t3167706508_Int32_t2571135199,
	RuntimeInvoker_GcScoreData_t3955401213_Int32_t2571135199,
	RuntimeInvoker_WorkRequest_t2547429811_Int32_t2571135199,
	RuntimeInvoker_RuntimeObject_SByte_t2530277047_RuntimeObject_RuntimeObject_RuntimeObject,
	RuntimeInvoker_CustomAttributeNamedArgument_t425730339,
	RuntimeInvoker_CustomAttributeTypedArgument_t2667702721,
	RuntimeInvoker_TableRange_t4125455382,
	RuntimeInvoker_ClientCertificateType_t194976790,
	RuntimeInvoker_KeyValuePair_2_t2204764377,
	RuntimeInvoker_KeyValuePair_2_t2366093889,
	RuntimeInvoker_KeyValuePair_2_t2877556189,
	RuntimeInvoker_Link_t3525875035,
	RuntimeInvoker_Slot_t2102621701,
	RuntimeInvoker_Slot_t1141617207,
	RuntimeInvoker_LabelData_t1059919410,
	RuntimeInvoker_LabelFixup_t2685311059,
	RuntimeInvoker_ILTokenInfo_t281295013,
	RuntimeInvoker_MonoResource_t2965441204,
	RuntimeInvoker_RefEmitPermissionSet_t945207422,
	RuntimeInvoker_ParameterModifier_t969812002,
	RuntimeInvoker_ResourceCacheItem_t3632973466,
	RuntimeInvoker_ResourceInfo_t1773833009,
	RuntimeInvoker_TypeTag_t1137063594,
	RuntimeInvoker_X509ChainStatus_t177750891,
	RuntimeInvoker_Mark_t4050795874,
	RuntimeInvoker_UriScheme_t2622429923,
	RuntimeInvoker_Keyframe_t3396755990,
	RuntimeInvoker_PlayableBinding_t136667248,
	RuntimeInvoker_HitInfo_t184839221,
	RuntimeInvoker_GcAchievementData_t3167706508,
	RuntimeInvoker_GcScoreData_t3955401213,
	RuntimeInvoker_Int32_t2571135199_DateTimeOffset_t2220763929_DateTimeOffset_t2220763929,
	RuntimeInvoker_Int32_t2571135199_Guid_t_Guid_t,
	RuntimeInvoker_DictionaryEntry_t1385909157_IntPtr_t_RuntimeObject,
	RuntimeInvoker_RuntimeObject_IntPtr_t_RuntimeObject_RuntimeObject_RuntimeObject,
	RuntimeInvoker_DictionaryEntry_t1385909157_RuntimeObject,
	RuntimeInvoker_KeyValuePair_2_t2204764377_IntPtr_t_RuntimeObject,
	RuntimeInvoker_KeyValuePair_2_t2204764377_RuntimeObject,
	RuntimeInvoker_DictionaryEntry_t1385909157_RuntimeObject_SByte_t2530277047,
	RuntimeInvoker_RuntimeObject_RuntimeObject_SByte_t2530277047_RuntimeObject_RuntimeObject,
	RuntimeInvoker_KeyValuePair_2_t2366093889_RuntimeObject_SByte_t2530277047,
	RuntimeInvoker_KeyValuePair_2_t2366093889_RuntimeObject,
	RuntimeInvoker_DictionaryEntry_t1385909157_RuntimeObject_Int32_t2571135199,
	RuntimeInvoker_KeyValuePair_2_t2877556189_RuntimeObject_Int32_t2571135199,
	RuntimeInvoker_KeyValuePair_2_t2877556189_RuntimeObject,
	RuntimeInvoker_KeyValuePair_2_t3602437993_RuntimeObject,
	RuntimeInvoker_Void_t1078098495_IntPtr_t_RuntimeObject,
	RuntimeInvoker_Enumerator_t767066857,
	RuntimeInvoker_Boolean_t2059672899_RuntimeObject_BooleanU26_t495120821,
	RuntimeInvoker_Enumerator_t928396369,
	RuntimeInvoker_Enumerator_t1439858669,
	RuntimeInvoker_Boolean_t2059672899_SByte_t2530277047_SByte_t2530277047,
	RuntimeInvoker_Boolean_t2059672899_DateTimeOffset_t2220763929_DateTimeOffset_t2220763929,
	RuntimeInvoker_Boolean_t2059672899_CustomAttributeNamedArgument_t425730339_CustomAttributeNamedArgument_t425730339,
	RuntimeInvoker_Boolean_t2059672899_CustomAttributeTypedArgument_t2667702721_CustomAttributeTypedArgument_t2667702721,
	RuntimeInvoker_Enumerator_t2209518572,
	RuntimeInvoker_Enumerator_t64113712,
	RuntimeInvoker_Enumerator_t2306086094,
	RuntimeInvoker_Enumerator_t3479511895,
	RuntimeInvoker_CustomAttributeNamedArgument_t425730339_RuntimeObject,
	RuntimeInvoker_CustomAttributeTypedArgument_t2667702721_RuntimeObject,
	RuntimeInvoker_Boolean_t2059672899_Nullable_1_t950977049,
	RuntimeInvoker_RuntimeObject_Single_t496865882_RuntimeObject_RuntimeObject,
	RuntimeInvoker_RuntimeObject_Scene_t3023997566_RuntimeObject_RuntimeObject,
	RuntimeInvoker_RuntimeObject_Scene_t3023997566_Int32_t2571135199_RuntimeObject_RuntimeObject,
	RuntimeInvoker_RuntimeObject_Scene_t3023997566_Scene_t3023997566_RuntimeObject_RuntimeObject,
};
