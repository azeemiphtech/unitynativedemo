﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Reflection.CustomAttributeTypedArgument>
struct DefaultComparer_t2145707355;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.TimeSpan>
struct DefaultComparer_t2464679857;
// System.Collections.Generic.EqualityComparer`1<System.Boolean>
struct EqualityComparer_1_t2592293618;
// System.Type
struct Type_t;
// System.Collections.Generic.EqualityComparer`1<System.DateTime>
struct EqualityComparer_1_t1040419072;
// System.Collections.Generic.EqualityComparer`1<System.DateTimeOffset>
struct EqualityComparer_1_t2753384648;
// System.Collections.Generic.EqualityComparer`1<System.Guid>
struct EqualityComparer_1_t499284453;
// System.Collections.Generic.EqualityComparer`1<System.Int32>
struct EqualityComparer_1_t3103755918;
// System.Collections.Generic.EqualityComparer`1<System.IntPtr>
struct EqualityComparer_1_t1845253994;
// System.Collections.Generic.EqualityComparer`1<System.Object>
struct EqualityComparer_1_t3828637722;
// System.Collections.Generic.EqualityComparer`1<System.Reflection.CustomAttributeNamedArgument>
struct EqualityComparer_1_t958351058;
// System.Collections.Generic.EqualityComparer`1<System.Reflection.CustomAttributeTypedArgument>
struct EqualityComparer_1_t3200323440;
// System.Collections.Generic.EqualityComparer`1<System.TimeSpan>
struct EqualityComparer_1_t3519295942;
// System.Collections.Generic.GenericComparer`1<System.DateTime>
struct GenericComparer_1_t2804608443;
// System.Collections.Generic.GenericComparer`1<System.DateTimeOffset>
struct GenericComparer_1_t222606723;
// System.Collections.Generic.GenericComparer`1<System.Guid>
struct GenericComparer_1_t2263473824;
// System.Collections.Generic.GenericComparer`1<System.Int32>
struct GenericComparer_1_t572977993;
// System.Collections.Generic.GenericComparer`1<System.Object>
struct GenericComparer_1_t1297859797;
// System.Collections.Generic.GenericComparer`1<System.TimeSpan>
struct GenericComparer_1_t988518017;
// System.Collections.Generic.GenericEqualityComparer`1<System.Boolean>
struct GenericEqualityComparer_1_t1903981024;
// System.Collections.Generic.GenericEqualityComparer`1<System.DateTime>
struct GenericEqualityComparer_1_t352106478;
// System.Collections.Generic.GenericEqualityComparer`1<System.DateTimeOffset>
struct GenericEqualityComparer_1_t2065072054;
// System.Collections.Generic.GenericEqualityComparer`1<System.Guid>
struct GenericEqualityComparer_1_t4105939155;
// System.Collections.Generic.GenericEqualityComparer`1<System.Int32>
struct GenericEqualityComparer_1_t2415443324;
// System.Collections.Generic.GenericEqualityComparer`1<System.Object>
struct GenericEqualityComparer_1_t3140325128;
// System.Collections.Generic.GenericEqualityComparer`1<System.TimeSpan>
struct GenericEqualityComparer_1_t2830983348;
// System.Void
struct Void_t1078098495;
// System.String
struct String_t;
// System.Security.Cryptography.RandomNumberGenerator
struct RandomNumberGenerator_t4107616314;
// System.Char[]
struct CharU5BU5D_t2864810077;
// System.Reflection.MemberInfo
struct MemberInfo_t;
// System.Type[]
struct TypeU5BU5D_t1830665827;
// System.Reflection.MemberFilter
struct MemberFilter_t1197335030;
// System.String[]
struct StringU5BU5D_t522578821;
// System.Int32[]
struct Int32U5BU5D_t2856113350;

extern const RuntimeType* GenericEqualityComparer_1_t2573898200_0_0_0_var;
extern RuntimeClass* Type_t_il2cpp_TypeInfo_var;
extern RuntimeClass* TypeU5BU5D_t1830665827_il2cpp_TypeInfo_var;
extern const uint32_t EqualityComparer_1__cctor_m1182025640_MetadataUsageId;
extern const uint32_t EqualityComparer_1__cctor_m1522028557_MetadataUsageId;
extern const uint32_t EqualityComparer_1__cctor_m1277617039_MetadataUsageId;
extern const uint32_t EqualityComparer_1__cctor_m3704847281_MetadataUsageId;
extern const uint32_t EqualityComparer_1__cctor_m3173561107_MetadataUsageId;
extern const uint32_t EqualityComparer_1__cctor_m3466349081_MetadataUsageId;
extern const uint32_t EqualityComparer_1__cctor_m3326013475_MetadataUsageId;
extern const uint32_t EqualityComparer_1__cctor_m3327302871_MetadataUsageId;
extern const uint32_t EqualityComparer_1__cctor_m7634007_MetadataUsageId;
extern const uint32_t EqualityComparer_1__cctor_m679490880_MetadataUsageId;

struct TypeU5BU5D_t1830665827;


#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef COMPARER_1_T2590470634_H
#define COMPARER_1_T2590470634_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Comparer`1<System.Guid>
struct  Comparer_1_t2590470634  : public RuntimeObject
{
public:

public:
};

struct Comparer_1_t2590470634_StaticFields
{
public:
	// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1::_default
	Comparer_1_t2590470634 * ____default_0;

public:
	inline static int32_t get_offset_of__default_0() { return static_cast<int32_t>(offsetof(Comparer_1_t2590470634_StaticFields, ____default_0)); }
	inline Comparer_1_t2590470634 * get__default_0() const { return ____default_0; }
	inline Comparer_1_t2590470634 ** get_address_of__default_0() { return &____default_0; }
	inline void set__default_0(Comparer_1_t2590470634 * value)
	{
		____default_0 = value;
		Il2CppCodeGenWriteBarrier((&____default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPARER_1_T2590470634_H
#ifndef EQUALITYCOMPARER_1_T2753384648_H
#define EQUALITYCOMPARER_1_T2753384648_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1<System.DateTimeOffset>
struct  EqualityComparer_1_t2753384648  : public RuntimeObject
{
public:

public:
};

struct EqualityComparer_1_t2753384648_StaticFields
{
public:
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1::_default
	EqualityComparer_1_t2753384648 * ____default_0;

public:
	inline static int32_t get_offset_of__default_0() { return static_cast<int32_t>(offsetof(EqualityComparer_1_t2753384648_StaticFields, ____default_0)); }
	inline EqualityComparer_1_t2753384648 * get__default_0() const { return ____default_0; }
	inline EqualityComparer_1_t2753384648 ** get_address_of__default_0() { return &____default_0; }
	inline void set__default_0(EqualityComparer_1_t2753384648 * value)
	{
		____default_0 = value;
		Il2CppCodeGenWriteBarrier((&____default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EQUALITYCOMPARER_1_T2753384648_H
#ifndef COMPARER_1_T1624856607_H
#define COMPARER_1_T1624856607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Comparer`1<System.Object>
struct  Comparer_1_t1624856607  : public RuntimeObject
{
public:

public:
};

struct Comparer_1_t1624856607_StaticFields
{
public:
	// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1::_default
	Comparer_1_t1624856607 * ____default_0;

public:
	inline static int32_t get_offset_of__default_0() { return static_cast<int32_t>(offsetof(Comparer_1_t1624856607_StaticFields, ____default_0)); }
	inline Comparer_1_t1624856607 * get__default_0() const { return ____default_0; }
	inline Comparer_1_t1624856607 ** get_address_of__default_0() { return &____default_0; }
	inline void set__default_0(Comparer_1_t1624856607 * value)
	{
		____default_0 = value;
		Il2CppCodeGenWriteBarrier((&____default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPARER_1_T1624856607_H
#ifndef EQUALITYCOMPARER_1_T958351058_H
#define EQUALITYCOMPARER_1_T958351058_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1<System.Reflection.CustomAttributeNamedArgument>
struct  EqualityComparer_1_t958351058  : public RuntimeObject
{
public:

public:
};

struct EqualityComparer_1_t958351058_StaticFields
{
public:
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1::_default
	EqualityComparer_1_t958351058 * ____default_0;

public:
	inline static int32_t get_offset_of__default_0() { return static_cast<int32_t>(offsetof(EqualityComparer_1_t958351058_StaticFields, ____default_0)); }
	inline EqualityComparer_1_t958351058 * get__default_0() const { return ____default_0; }
	inline EqualityComparer_1_t958351058 ** get_address_of__default_0() { return &____default_0; }
	inline void set__default_0(EqualityComparer_1_t958351058 * value)
	{
		____default_0 = value;
		Il2CppCodeGenWriteBarrier((&____default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EQUALITYCOMPARER_1_T958351058_H
#ifndef EQUALITYCOMPARER_1_T1040419072_H
#define EQUALITYCOMPARER_1_T1040419072_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1<System.DateTime>
struct  EqualityComparer_1_t1040419072  : public RuntimeObject
{
public:

public:
};

struct EqualityComparer_1_t1040419072_StaticFields
{
public:
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1::_default
	EqualityComparer_1_t1040419072 * ____default_0;

public:
	inline static int32_t get_offset_of__default_0() { return static_cast<int32_t>(offsetof(EqualityComparer_1_t1040419072_StaticFields, ____default_0)); }
	inline EqualityComparer_1_t1040419072 * get__default_0() const { return ____default_0; }
	inline EqualityComparer_1_t1040419072 ** get_address_of__default_0() { return &____default_0; }
	inline void set__default_0(EqualityComparer_1_t1040419072 * value)
	{
		____default_0 = value;
		Il2CppCodeGenWriteBarrier((&____default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EQUALITYCOMPARER_1_T1040419072_H
#ifndef COMPARER_1_T1315514827_H
#define COMPARER_1_T1315514827_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Comparer`1<System.TimeSpan>
struct  Comparer_1_t1315514827  : public RuntimeObject
{
public:

public:
};

struct Comparer_1_t1315514827_StaticFields
{
public:
	// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1::_default
	Comparer_1_t1315514827 * ____default_0;

public:
	inline static int32_t get_offset_of__default_0() { return static_cast<int32_t>(offsetof(Comparer_1_t1315514827_StaticFields, ____default_0)); }
	inline Comparer_1_t1315514827 * get__default_0() const { return ____default_0; }
	inline Comparer_1_t1315514827 ** get_address_of__default_0() { return &____default_0; }
	inline void set__default_0(Comparer_1_t1315514827 * value)
	{
		____default_0 = value;
		Il2CppCodeGenWriteBarrier((&____default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPARER_1_T1315514827_H
#ifndef EQUALITYCOMPARER_1_T3103755918_H
#define EQUALITYCOMPARER_1_T3103755918_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1<System.Int32>
struct  EqualityComparer_1_t3103755918  : public RuntimeObject
{
public:

public:
};

struct EqualityComparer_1_t3103755918_StaticFields
{
public:
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1::_default
	EqualityComparer_1_t3103755918 * ____default_0;

public:
	inline static int32_t get_offset_of__default_0() { return static_cast<int32_t>(offsetof(EqualityComparer_1_t3103755918_StaticFields, ____default_0)); }
	inline EqualityComparer_1_t3103755918 * get__default_0() const { return ____default_0; }
	inline EqualityComparer_1_t3103755918 ** get_address_of__default_0() { return &____default_0; }
	inline void set__default_0(EqualityComparer_1_t3103755918 * value)
	{
		____default_0 = value;
		Il2CppCodeGenWriteBarrier((&____default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EQUALITYCOMPARER_1_T3103755918_H
#ifndef EQUALITYCOMPARER_1_T2592293618_H
#define EQUALITYCOMPARER_1_T2592293618_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1<System.Boolean>
struct  EqualityComparer_1_t2592293618  : public RuntimeObject
{
public:

public:
};

struct EqualityComparer_1_t2592293618_StaticFields
{
public:
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1::_default
	EqualityComparer_1_t2592293618 * ____default_0;

public:
	inline static int32_t get_offset_of__default_0() { return static_cast<int32_t>(offsetof(EqualityComparer_1_t2592293618_StaticFields, ____default_0)); }
	inline EqualityComparer_1_t2592293618 * get__default_0() const { return ____default_0; }
	inline EqualityComparer_1_t2592293618 ** get_address_of__default_0() { return &____default_0; }
	inline void set__default_0(EqualityComparer_1_t2592293618 * value)
	{
		____default_0 = value;
		Il2CppCodeGenWriteBarrier((&____default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EQUALITYCOMPARER_1_T2592293618_H
#ifndef EQUALITYCOMPARER_1_T1845253994_H
#define EQUALITYCOMPARER_1_T1845253994_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1<System.IntPtr>
struct  EqualityComparer_1_t1845253994  : public RuntimeObject
{
public:

public:
};

struct EqualityComparer_1_t1845253994_StaticFields
{
public:
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1::_default
	EqualityComparer_1_t1845253994 * ____default_0;

public:
	inline static int32_t get_offset_of__default_0() { return static_cast<int32_t>(offsetof(EqualityComparer_1_t1845253994_StaticFields, ____default_0)); }
	inline EqualityComparer_1_t1845253994 * get__default_0() const { return ____default_0; }
	inline EqualityComparer_1_t1845253994 ** get_address_of__default_0() { return &____default_0; }
	inline void set__default_0(EqualityComparer_1_t1845253994 * value)
	{
		____default_0 = value;
		Il2CppCodeGenWriteBarrier((&____default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EQUALITYCOMPARER_1_T1845253994_H
#ifndef EQUALITYCOMPARER_1_T3519295942_H
#define EQUALITYCOMPARER_1_T3519295942_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1<System.TimeSpan>
struct  EqualityComparer_1_t3519295942  : public RuntimeObject
{
public:

public:
};

struct EqualityComparer_1_t3519295942_StaticFields
{
public:
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1::_default
	EqualityComparer_1_t3519295942 * ____default_0;

public:
	inline static int32_t get_offset_of__default_0() { return static_cast<int32_t>(offsetof(EqualityComparer_1_t3519295942_StaticFields, ____default_0)); }
	inline EqualityComparer_1_t3519295942 * get__default_0() const { return ____default_0; }
	inline EqualityComparer_1_t3519295942 ** get_address_of__default_0() { return &____default_0; }
	inline void set__default_0(EqualityComparer_1_t3519295942 * value)
	{
		____default_0 = value;
		Il2CppCodeGenWriteBarrier((&____default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EQUALITYCOMPARER_1_T3519295942_H
#ifndef COMPARER_1_T549603533_H
#define COMPARER_1_T549603533_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Comparer`1<System.DateTimeOffset>
struct  Comparer_1_t549603533  : public RuntimeObject
{
public:

public:
};

struct Comparer_1_t549603533_StaticFields
{
public:
	// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1::_default
	Comparer_1_t549603533 * ____default_0;

public:
	inline static int32_t get_offset_of__default_0() { return static_cast<int32_t>(offsetof(Comparer_1_t549603533_StaticFields, ____default_0)); }
	inline Comparer_1_t549603533 * get__default_0() const { return ____default_0; }
	inline Comparer_1_t549603533 ** get_address_of__default_0() { return &____default_0; }
	inline void set__default_0(Comparer_1_t549603533 * value)
	{
		____default_0 = value;
		Il2CppCodeGenWriteBarrier((&____default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPARER_1_T549603533_H
#ifndef EQUALITYCOMPARER_1_T3828637722_H
#define EQUALITYCOMPARER_1_T3828637722_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1<System.Object>
struct  EqualityComparer_1_t3828637722  : public RuntimeObject
{
public:

public:
};

struct EqualityComparer_1_t3828637722_StaticFields
{
public:
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1::_default
	EqualityComparer_1_t3828637722 * ____default_0;

public:
	inline static int32_t get_offset_of__default_0() { return static_cast<int32_t>(offsetof(EqualityComparer_1_t3828637722_StaticFields, ____default_0)); }
	inline EqualityComparer_1_t3828637722 * get__default_0() const { return ____default_0; }
	inline EqualityComparer_1_t3828637722 ** get_address_of__default_0() { return &____default_0; }
	inline void set__default_0(EqualityComparer_1_t3828637722 * value)
	{
		____default_0 = value;
		Il2CppCodeGenWriteBarrier((&____default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EQUALITYCOMPARER_1_T3828637722_H
#ifndef COMPARER_1_T3131605253_H
#define COMPARER_1_T3131605253_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Comparer`1<System.DateTime>
struct  Comparer_1_t3131605253  : public RuntimeObject
{
public:

public:
};

struct Comparer_1_t3131605253_StaticFields
{
public:
	// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1::_default
	Comparer_1_t3131605253 * ____default_0;

public:
	inline static int32_t get_offset_of__default_0() { return static_cast<int32_t>(offsetof(Comparer_1_t3131605253_StaticFields, ____default_0)); }
	inline Comparer_1_t3131605253 * get__default_0() const { return ____default_0; }
	inline Comparer_1_t3131605253 ** get_address_of__default_0() { return &____default_0; }
	inline void set__default_0(Comparer_1_t3131605253 * value)
	{
		____default_0 = value;
		Il2CppCodeGenWriteBarrier((&____default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPARER_1_T3131605253_H
#ifndef EQUALITYCOMPARER_1_T3200323440_H
#define EQUALITYCOMPARER_1_T3200323440_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1<System.Reflection.CustomAttributeTypedArgument>
struct  EqualityComparer_1_t3200323440  : public RuntimeObject
{
public:

public:
};

struct EqualityComparer_1_t3200323440_StaticFields
{
public:
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1::_default
	EqualityComparer_1_t3200323440 * ____default_0;

public:
	inline static int32_t get_offset_of__default_0() { return static_cast<int32_t>(offsetof(EqualityComparer_1_t3200323440_StaticFields, ____default_0)); }
	inline EqualityComparer_1_t3200323440 * get__default_0() const { return ____default_0; }
	inline EqualityComparer_1_t3200323440 ** get_address_of__default_0() { return &____default_0; }
	inline void set__default_0(EqualityComparer_1_t3200323440 * value)
	{
		____default_0 = value;
		Il2CppCodeGenWriteBarrier((&____default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EQUALITYCOMPARER_1_T3200323440_H
#ifndef VALUETYPE_T1920584095_H
#define VALUETYPE_T1920584095_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t1920584095  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t1920584095_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t1920584095_marshaled_com
{
};
#endif // VALUETYPE_T1920584095_H
#ifndef MEMBERINFO_T_H
#define MEMBERINFO_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MemberInfo
struct  MemberInfo_t  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERINFO_T_H
#ifndef COMPARER_1_T899974803_H
#define COMPARER_1_T899974803_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Comparer`1<System.Int32>
struct  Comparer_1_t899974803  : public RuntimeObject
{
public:

public:
};

struct Comparer_1_t899974803_StaticFields
{
public:
	// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1::_default
	Comparer_1_t899974803 * ____default_0;

public:
	inline static int32_t get_offset_of__default_0() { return static_cast<int32_t>(offsetof(Comparer_1_t899974803_StaticFields, ____default_0)); }
	inline Comparer_1_t899974803 * get__default_0() const { return ____default_0; }
	inline Comparer_1_t899974803 ** get_address_of__default_0() { return &____default_0; }
	inline void set__default_0(Comparer_1_t899974803 * value)
	{
		____default_0 = value;
		Il2CppCodeGenWriteBarrier((&____default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPARER_1_T899974803_H
#ifndef EQUALITYCOMPARER_1_T499284453_H
#define EQUALITYCOMPARER_1_T499284453_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1<System.Guid>
struct  EqualityComparer_1_t499284453  : public RuntimeObject
{
public:

public:
};

struct EqualityComparer_1_t499284453_StaticFields
{
public:
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1::_default
	EqualityComparer_1_t499284453 * ____default_0;

public:
	inline static int32_t get_offset_of__default_0() { return static_cast<int32_t>(offsetof(EqualityComparer_1_t499284453_StaticFields, ____default_0)); }
	inline EqualityComparer_1_t499284453 * get__default_0() const { return ____default_0; }
	inline EqualityComparer_1_t499284453 ** get_address_of__default_0() { return &____default_0; }
	inline void set__default_0(EqualityComparer_1_t499284453 * value)
	{
		____default_0 = value;
		Il2CppCodeGenWriteBarrier((&____default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EQUALITYCOMPARER_1_T499284453_H
#ifndef GENERICCOMPARER_1_T2263473824_H
#define GENERICCOMPARER_1_T2263473824_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.GenericComparer`1<System.Guid>
struct  GenericComparer_1_t2263473824  : public Comparer_1_t2590470634
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERICCOMPARER_1_T2263473824_H
#ifndef GENERICCOMPARER_1_T222606723_H
#define GENERICCOMPARER_1_T222606723_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.GenericComparer`1<System.DateTimeOffset>
struct  GenericComparer_1_t222606723  : public Comparer_1_t549603533
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERICCOMPARER_1_T222606723_H
#ifndef GENERICCOMPARER_1_T988518017_H
#define GENERICCOMPARER_1_T988518017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.GenericComparer`1<System.TimeSpan>
struct  GenericComparer_1_t988518017  : public Comparer_1_t1315514827
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERICCOMPARER_1_T988518017_H
#ifndef GENERICCOMPARER_1_T1297859797_H
#define GENERICCOMPARER_1_T1297859797_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.GenericComparer`1<System.Object>
struct  GenericComparer_1_t1297859797  : public Comparer_1_t1624856607
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERICCOMPARER_1_T1297859797_H
#ifndef GENERICEQUALITYCOMPARER_1_T1903981024_H
#define GENERICEQUALITYCOMPARER_1_T1903981024_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.GenericEqualityComparer`1<System.Boolean>
struct  GenericEqualityComparer_1_t1903981024  : public EqualityComparer_1_t2592293618
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERICEQUALITYCOMPARER_1_T1903981024_H
#ifndef GENERICEQUALITYCOMPARER_1_T352106478_H
#define GENERICEQUALITYCOMPARER_1_T352106478_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.GenericEqualityComparer`1<System.DateTime>
struct  GenericEqualityComparer_1_t352106478  : public EqualityComparer_1_t1040419072
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERICEQUALITYCOMPARER_1_T352106478_H
#ifndef GENERICEQUALITYCOMPARER_1_T2065072054_H
#define GENERICEQUALITYCOMPARER_1_T2065072054_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.GenericEqualityComparer`1<System.DateTimeOffset>
struct  GenericEqualityComparer_1_t2065072054  : public EqualityComparer_1_t2753384648
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERICEQUALITYCOMPARER_1_T2065072054_H
#ifndef GENERICEQUALITYCOMPARER_1_T4105939155_H
#define GENERICEQUALITYCOMPARER_1_T4105939155_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.GenericEqualityComparer`1<System.Guid>
struct  GenericEqualityComparer_1_t4105939155  : public EqualityComparer_1_t499284453
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERICEQUALITYCOMPARER_1_T4105939155_H
#ifndef GENERICEQUALITYCOMPARER_1_T2415443324_H
#define GENERICEQUALITYCOMPARER_1_T2415443324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.GenericEqualityComparer`1<System.Int32>
struct  GenericEqualityComparer_1_t2415443324  : public EqualityComparer_1_t3103755918
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERICEQUALITYCOMPARER_1_T2415443324_H
#ifndef GENERICEQUALITYCOMPARER_1_T3140325128_H
#define GENERICEQUALITYCOMPARER_1_T3140325128_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.GenericEqualityComparer`1<System.Object>
struct  GenericEqualityComparer_1_t3140325128  : public EqualityComparer_1_t3828637722
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERICEQUALITYCOMPARER_1_T3140325128_H
#ifndef GENERICEQUALITYCOMPARER_1_T2830983348_H
#define GENERICEQUALITYCOMPARER_1_T2830983348_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.GenericEqualityComparer`1<System.TimeSpan>
struct  GenericEqualityComparer_1_t2830983348  : public EqualityComparer_1_t3519295942
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERICEQUALITYCOMPARER_1_T2830983348_H
#ifndef GENERICCOMPARER_1_T572977993_H
#define GENERICCOMPARER_1_T572977993_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.GenericComparer`1<System.Int32>
struct  GenericComparer_1_t572977993  : public Comparer_1_t899974803
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERICCOMPARER_1_T572977993_H
#ifndef GENERICCOMPARER_1_T2804608443_H
#define GENERICCOMPARER_1_T2804608443_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.GenericComparer`1<System.DateTime>
struct  GenericComparer_1_t2804608443  : public Comparer_1_t3131605253
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERICCOMPARER_1_T2804608443_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef DEFAULTCOMPARER_T4198702269_H
#define DEFAULTCOMPARER_T4198702269_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Reflection.CustomAttributeNamedArgument>
struct  DefaultComparer_t4198702269  : public EqualityComparer_1_t958351058
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTCOMPARER_T4198702269_H
#ifndef DEFAULTCOMPARER_T2145707355_H
#define DEFAULTCOMPARER_T2145707355_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Reflection.CustomAttributeTypedArgument>
struct  DefaultComparer_t2145707355  : public EqualityComparer_1_t3200323440
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTCOMPARER_T2145707355_H
#ifndef VOID_T1078098495_H
#define VOID_T1078098495_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1078098495 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1078098495_H
#ifndef CUSTOMATTRIBUTETYPEDARGUMENT_T2667702721_H
#define CUSTOMATTRIBUTETYPEDARGUMENT_T2667702721_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.CustomAttributeTypedArgument
struct  CustomAttributeTypedArgument_t2667702721 
{
public:
	// System.Type System.Reflection.CustomAttributeTypedArgument::argumentType
	Type_t * ___argumentType_0;
	// System.Object System.Reflection.CustomAttributeTypedArgument::value
	RuntimeObject * ___value_1;

public:
	inline static int32_t get_offset_of_argumentType_0() { return static_cast<int32_t>(offsetof(CustomAttributeTypedArgument_t2667702721, ___argumentType_0)); }
	inline Type_t * get_argumentType_0() const { return ___argumentType_0; }
	inline Type_t ** get_address_of_argumentType_0() { return &___argumentType_0; }
	inline void set_argumentType_0(Type_t * value)
	{
		___argumentType_0 = value;
		Il2CppCodeGenWriteBarrier((&___argumentType_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(CustomAttributeTypedArgument_t2667702721, ___value_1)); }
	inline RuntimeObject * get_value_1() const { return ___value_1; }
	inline RuntimeObject ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(RuntimeObject * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Reflection.CustomAttributeTypedArgument
struct CustomAttributeTypedArgument_t2667702721_marshaled_pinvoke
{
	Type_t * ___argumentType_0;
	Il2CppIUnknown* ___value_1;
};
// Native definition for COM marshalling of System.Reflection.CustomAttributeTypedArgument
struct CustomAttributeTypedArgument_t2667702721_marshaled_com
{
	Type_t * ___argumentType_0;
	Il2CppIUnknown* ___value_1;
};
#endif // CUSTOMATTRIBUTETYPEDARGUMENT_T2667702721_H
#ifndef INT32_T2571135199_H
#define INT32_T2571135199_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2571135199 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t2571135199, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2571135199_H
#ifndef BOOLEAN_T2059672899_H
#define BOOLEAN_T2059672899_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t2059672899 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t2059672899, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t2059672899_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t2059672899_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t2059672899_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T2059672899_H
#ifndef DEFAULTCOMPARER_T2464679857_H
#define DEFAULTCOMPARER_T2464679857_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.TimeSpan>
struct  DefaultComparer_t2464679857  : public EqualityComparer_1_t3519295942
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTCOMPARER_T2464679857_H
#ifndef TIMESPAN_T2986675223_H
#define TIMESPAN_T2986675223_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TimeSpan
struct  TimeSpan_t2986675223 
{
public:
	// System.Int64 System.TimeSpan::_ticks
	int64_t ____ticks_3;

public:
	inline static int32_t get_offset_of__ticks_3() { return static_cast<int32_t>(offsetof(TimeSpan_t2986675223, ____ticks_3)); }
	inline int64_t get__ticks_3() const { return ____ticks_3; }
	inline int64_t* get_address_of__ticks_3() { return &____ticks_3; }
	inline void set__ticks_3(int64_t value)
	{
		____ticks_3 = value;
	}
};

struct TimeSpan_t2986675223_StaticFields
{
public:
	// System.TimeSpan System.TimeSpan::MaxValue
	TimeSpan_t2986675223  ___MaxValue_0;
	// System.TimeSpan System.TimeSpan::MinValue
	TimeSpan_t2986675223  ___MinValue_1;
	// System.TimeSpan System.TimeSpan::Zero
	TimeSpan_t2986675223  ___Zero_2;

public:
	inline static int32_t get_offset_of_MaxValue_0() { return static_cast<int32_t>(offsetof(TimeSpan_t2986675223_StaticFields, ___MaxValue_0)); }
	inline TimeSpan_t2986675223  get_MaxValue_0() const { return ___MaxValue_0; }
	inline TimeSpan_t2986675223 * get_address_of_MaxValue_0() { return &___MaxValue_0; }
	inline void set_MaxValue_0(TimeSpan_t2986675223  value)
	{
		___MaxValue_0 = value;
	}

	inline static int32_t get_offset_of_MinValue_1() { return static_cast<int32_t>(offsetof(TimeSpan_t2986675223_StaticFields, ___MinValue_1)); }
	inline TimeSpan_t2986675223  get_MinValue_1() const { return ___MinValue_1; }
	inline TimeSpan_t2986675223 * get_address_of_MinValue_1() { return &___MinValue_1; }
	inline void set_MinValue_1(TimeSpan_t2986675223  value)
	{
		___MinValue_1 = value;
	}

	inline static int32_t get_offset_of_Zero_2() { return static_cast<int32_t>(offsetof(TimeSpan_t2986675223_StaticFields, ___Zero_2)); }
	inline TimeSpan_t2986675223  get_Zero_2() const { return ___Zero_2; }
	inline TimeSpan_t2986675223 * get_address_of_Zero_2() { return &___Zero_2; }
	inline void set_Zero_2(TimeSpan_t2986675223  value)
	{
		___Zero_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESPAN_T2986675223_H
#ifndef DEFAULTCOMPARER_T4280770283_H
#define DEFAULTCOMPARER_T4280770283_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.DateTime>
struct  DefaultComparer_t4280770283  : public EqualityComparer_1_t1040419072
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTCOMPARER_T4280770283_H
#ifndef DEFAULTCOMPARER_T1537677533_H
#define DEFAULTCOMPARER_T1537677533_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Boolean>
struct  DefaultComparer_t1537677533  : public EqualityComparer_1_t2592293618
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTCOMPARER_T1537677533_H
#ifndef DEFAULTCOMPARER_T1698768563_H
#define DEFAULTCOMPARER_T1698768563_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.DateTimeOffset>
struct  DefaultComparer_t1698768563  : public EqualityComparer_1_t2753384648
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTCOMPARER_T1698768563_H
#ifndef DEFAULTCOMPARER_T3739635664_H
#define DEFAULTCOMPARER_T3739635664_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Guid>
struct  DefaultComparer_t3739635664  : public EqualityComparer_1_t499284453
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTCOMPARER_T3739635664_H
#ifndef GUID_T_H
#define GUID_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Guid
struct  Guid_t 
{
public:
	// System.Int32 System.Guid::_a
	int32_t ____a_0;
	// System.Int16 System.Guid::_b
	int16_t ____b_1;
	// System.Int16 System.Guid::_c
	int16_t ____c_2;
	// System.Byte System.Guid::_d
	uint8_t ____d_3;
	// System.Byte System.Guid::_e
	uint8_t ____e_4;
	// System.Byte System.Guid::_f
	uint8_t ____f_5;
	// System.Byte System.Guid::_g
	uint8_t ____g_6;
	// System.Byte System.Guid::_h
	uint8_t ____h_7;
	// System.Byte System.Guid::_i
	uint8_t ____i_8;
	// System.Byte System.Guid::_j
	uint8_t ____j_9;
	// System.Byte System.Guid::_k
	uint8_t ____k_10;

public:
	inline static int32_t get_offset_of__a_0() { return static_cast<int32_t>(offsetof(Guid_t, ____a_0)); }
	inline int32_t get__a_0() const { return ____a_0; }
	inline int32_t* get_address_of__a_0() { return &____a_0; }
	inline void set__a_0(int32_t value)
	{
		____a_0 = value;
	}

	inline static int32_t get_offset_of__b_1() { return static_cast<int32_t>(offsetof(Guid_t, ____b_1)); }
	inline int16_t get__b_1() const { return ____b_1; }
	inline int16_t* get_address_of__b_1() { return &____b_1; }
	inline void set__b_1(int16_t value)
	{
		____b_1 = value;
	}

	inline static int32_t get_offset_of__c_2() { return static_cast<int32_t>(offsetof(Guid_t, ____c_2)); }
	inline int16_t get__c_2() const { return ____c_2; }
	inline int16_t* get_address_of__c_2() { return &____c_2; }
	inline void set__c_2(int16_t value)
	{
		____c_2 = value;
	}

	inline static int32_t get_offset_of__d_3() { return static_cast<int32_t>(offsetof(Guid_t, ____d_3)); }
	inline uint8_t get__d_3() const { return ____d_3; }
	inline uint8_t* get_address_of__d_3() { return &____d_3; }
	inline void set__d_3(uint8_t value)
	{
		____d_3 = value;
	}

	inline static int32_t get_offset_of__e_4() { return static_cast<int32_t>(offsetof(Guid_t, ____e_4)); }
	inline uint8_t get__e_4() const { return ____e_4; }
	inline uint8_t* get_address_of__e_4() { return &____e_4; }
	inline void set__e_4(uint8_t value)
	{
		____e_4 = value;
	}

	inline static int32_t get_offset_of__f_5() { return static_cast<int32_t>(offsetof(Guid_t, ____f_5)); }
	inline uint8_t get__f_5() const { return ____f_5; }
	inline uint8_t* get_address_of__f_5() { return &____f_5; }
	inline void set__f_5(uint8_t value)
	{
		____f_5 = value;
	}

	inline static int32_t get_offset_of__g_6() { return static_cast<int32_t>(offsetof(Guid_t, ____g_6)); }
	inline uint8_t get__g_6() const { return ____g_6; }
	inline uint8_t* get_address_of__g_6() { return &____g_6; }
	inline void set__g_6(uint8_t value)
	{
		____g_6 = value;
	}

	inline static int32_t get_offset_of__h_7() { return static_cast<int32_t>(offsetof(Guid_t, ____h_7)); }
	inline uint8_t get__h_7() const { return ____h_7; }
	inline uint8_t* get_address_of__h_7() { return &____h_7; }
	inline void set__h_7(uint8_t value)
	{
		____h_7 = value;
	}

	inline static int32_t get_offset_of__i_8() { return static_cast<int32_t>(offsetof(Guid_t, ____i_8)); }
	inline uint8_t get__i_8() const { return ____i_8; }
	inline uint8_t* get_address_of__i_8() { return &____i_8; }
	inline void set__i_8(uint8_t value)
	{
		____i_8 = value;
	}

	inline static int32_t get_offset_of__j_9() { return static_cast<int32_t>(offsetof(Guid_t, ____j_9)); }
	inline uint8_t get__j_9() const { return ____j_9; }
	inline uint8_t* get_address_of__j_9() { return &____j_9; }
	inline void set__j_9(uint8_t value)
	{
		____j_9 = value;
	}

	inline static int32_t get_offset_of__k_10() { return static_cast<int32_t>(offsetof(Guid_t, ____k_10)); }
	inline uint8_t get__k_10() const { return ____k_10; }
	inline uint8_t* get_address_of__k_10() { return &____k_10; }
	inline void set__k_10(uint8_t value)
	{
		____k_10 = value;
	}
};

struct Guid_t_StaticFields
{
public:
	// System.Guid System.Guid::Empty
	Guid_t  ___Empty_11;
	// System.Object System.Guid::_rngAccess
	RuntimeObject * ____rngAccess_12;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_rng
	RandomNumberGenerator_t4107616314 * ____rng_13;

public:
	inline static int32_t get_offset_of_Empty_11() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ___Empty_11)); }
	inline Guid_t  get_Empty_11() const { return ___Empty_11; }
	inline Guid_t * get_address_of_Empty_11() { return &___Empty_11; }
	inline void set_Empty_11(Guid_t  value)
	{
		___Empty_11 = value;
	}

	inline static int32_t get_offset_of__rngAccess_12() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rngAccess_12)); }
	inline RuntimeObject * get__rngAccess_12() const { return ____rngAccess_12; }
	inline RuntimeObject ** get_address_of__rngAccess_12() { return &____rngAccess_12; }
	inline void set__rngAccess_12(RuntimeObject * value)
	{
		____rngAccess_12 = value;
		Il2CppCodeGenWriteBarrier((&____rngAccess_12), value);
	}

	inline static int32_t get_offset_of__rng_13() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rng_13)); }
	inline RandomNumberGenerator_t4107616314 * get__rng_13() const { return ____rng_13; }
	inline RandomNumberGenerator_t4107616314 ** get_address_of__rng_13() { return &____rng_13; }
	inline void set__rng_13(RandomNumberGenerator_t4107616314 * value)
	{
		____rng_13 = value;
		Il2CppCodeGenWriteBarrier((&____rng_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUID_T_H
#ifndef DEFAULTCOMPARER_T2049139833_H
#define DEFAULTCOMPARER_T2049139833_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Int32>
struct  DefaultComparer_t2049139833  : public EqualityComparer_1_t3103755918
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTCOMPARER_T2049139833_H
#ifndef DEFAULTCOMPARER_T790637909_H
#define DEFAULTCOMPARER_T790637909_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.IntPtr>
struct  DefaultComparer_t790637909  : public EqualityComparer_1_t1845253994
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTCOMPARER_T790637909_H
#ifndef DEFAULTCOMPARER_T2774021637_H
#define DEFAULTCOMPARER_T2774021637_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Object>
struct  DefaultComparer_t2774021637  : public EqualityComparer_1_t3828637722
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTCOMPARER_T2774021637_H
#ifndef ENUM_T750633987_H
#define ENUM_T750633987_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t750633987  : public ValueType_t1920584095
{
public:

public:
};

struct Enum_t750633987_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t2864810077* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t750633987_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t2864810077* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t2864810077** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t2864810077* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t750633987_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t750633987_marshaled_com
{
};
#endif // ENUM_T750633987_H
#ifndef CUSTOMATTRIBUTENAMEDARGUMENT_T425730339_H
#define CUSTOMATTRIBUTENAMEDARGUMENT_T425730339_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.CustomAttributeNamedArgument
struct  CustomAttributeNamedArgument_t425730339 
{
public:
	// System.Reflection.CustomAttributeTypedArgument System.Reflection.CustomAttributeNamedArgument::typedArgument
	CustomAttributeTypedArgument_t2667702721  ___typedArgument_0;
	// System.Reflection.MemberInfo System.Reflection.CustomAttributeNamedArgument::memberInfo
	MemberInfo_t * ___memberInfo_1;

public:
	inline static int32_t get_offset_of_typedArgument_0() { return static_cast<int32_t>(offsetof(CustomAttributeNamedArgument_t425730339, ___typedArgument_0)); }
	inline CustomAttributeTypedArgument_t2667702721  get_typedArgument_0() const { return ___typedArgument_0; }
	inline CustomAttributeTypedArgument_t2667702721 * get_address_of_typedArgument_0() { return &___typedArgument_0; }
	inline void set_typedArgument_0(CustomAttributeTypedArgument_t2667702721  value)
	{
		___typedArgument_0 = value;
	}

	inline static int32_t get_offset_of_memberInfo_1() { return static_cast<int32_t>(offsetof(CustomAttributeNamedArgument_t425730339, ___memberInfo_1)); }
	inline MemberInfo_t * get_memberInfo_1() const { return ___memberInfo_1; }
	inline MemberInfo_t ** get_address_of_memberInfo_1() { return &___memberInfo_1; }
	inline void set_memberInfo_1(MemberInfo_t * value)
	{
		___memberInfo_1 = value;
		Il2CppCodeGenWriteBarrier((&___memberInfo_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Reflection.CustomAttributeNamedArgument
struct CustomAttributeNamedArgument_t425730339_marshaled_pinvoke
{
	CustomAttributeTypedArgument_t2667702721_marshaled_pinvoke ___typedArgument_0;
	MemberInfo_t * ___memberInfo_1;
};
// Native definition for COM marshalling of System.Reflection.CustomAttributeNamedArgument
struct CustomAttributeNamedArgument_t425730339_marshaled_com
{
	CustomAttributeTypedArgument_t2667702721_marshaled_com ___typedArgument_0;
	MemberInfo_t * ___memberInfo_1;
};
#endif // CUSTOMATTRIBUTENAMEDARGUMENT_T425730339_H
#ifndef RUNTIMETYPEHANDLE_T637624852_H
#define RUNTIMETYPEHANDLE_T637624852_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.RuntimeTypeHandle
struct  RuntimeTypeHandle_t637624852 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_t637624852, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMETYPEHANDLE_T637624852_H
#ifndef DATETIMEKIND_T3511769276_H
#define DATETIMEKIND_T3511769276_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTimeKind
struct  DateTimeKind_t3511769276 
{
public:
	// System.Int32 System.DateTimeKind::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DateTimeKind_t3511769276, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMEKIND_T3511769276_H
#ifndef BINDINGFLAGS_T3268090012_H
#define BINDINGFLAGS_T3268090012_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.BindingFlags
struct  BindingFlags_t3268090012 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BindingFlags_t3268090012, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDINGFLAGS_T3268090012_H
#ifndef TYPE_T_H
#define TYPE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Type
struct  Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_t637624852  ____impl_1;

public:
	inline static int32_t get_offset_of__impl_1() { return static_cast<int32_t>(offsetof(Type_t, ____impl_1)); }
	inline RuntimeTypeHandle_t637624852  get__impl_1() const { return ____impl_1; }
	inline RuntimeTypeHandle_t637624852 * get_address_of__impl_1() { return &____impl_1; }
	inline void set__impl_1(RuntimeTypeHandle_t637624852  value)
	{
		____impl_1 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_2;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t1830665827* ___EmptyTypes_3;
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t1197335030 * ___FilterAttribute_4;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t1197335030 * ___FilterName_5;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t1197335030 * ___FilterNameIgnoreCase_6;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_7;

public:
	inline static int32_t get_offset_of_Delimiter_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_2)); }
	inline Il2CppChar get_Delimiter_2() const { return ___Delimiter_2; }
	inline Il2CppChar* get_address_of_Delimiter_2() { return &___Delimiter_2; }
	inline void set_Delimiter_2(Il2CppChar value)
	{
		___Delimiter_2 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_3)); }
	inline TypeU5BU5D_t1830665827* get_EmptyTypes_3() const { return ___EmptyTypes_3; }
	inline TypeU5BU5D_t1830665827** get_address_of_EmptyTypes_3() { return &___EmptyTypes_3; }
	inline void set_EmptyTypes_3(TypeU5BU5D_t1830665827* value)
	{
		___EmptyTypes_3 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyTypes_3), value);
	}

	inline static int32_t get_offset_of_FilterAttribute_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_4)); }
	inline MemberFilter_t1197335030 * get_FilterAttribute_4() const { return ___FilterAttribute_4; }
	inline MemberFilter_t1197335030 ** get_address_of_FilterAttribute_4() { return &___FilterAttribute_4; }
	inline void set_FilterAttribute_4(MemberFilter_t1197335030 * value)
	{
		___FilterAttribute_4 = value;
		Il2CppCodeGenWriteBarrier((&___FilterAttribute_4), value);
	}

	inline static int32_t get_offset_of_FilterName_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_5)); }
	inline MemberFilter_t1197335030 * get_FilterName_5() const { return ___FilterName_5; }
	inline MemberFilter_t1197335030 ** get_address_of_FilterName_5() { return &___FilterName_5; }
	inline void set_FilterName_5(MemberFilter_t1197335030 * value)
	{
		___FilterName_5 = value;
		Il2CppCodeGenWriteBarrier((&___FilterName_5), value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_6)); }
	inline MemberFilter_t1197335030 * get_FilterNameIgnoreCase_6() const { return ___FilterNameIgnoreCase_6; }
	inline MemberFilter_t1197335030 ** get_address_of_FilterNameIgnoreCase_6() { return &___FilterNameIgnoreCase_6; }
	inline void set_FilterNameIgnoreCase_6(MemberFilter_t1197335030 * value)
	{
		___FilterNameIgnoreCase_6 = value;
		Il2CppCodeGenWriteBarrier((&___FilterNameIgnoreCase_6), value);
	}

	inline static int32_t get_offset_of_Missing_7() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_7)); }
	inline RuntimeObject * get_Missing_7() const { return ___Missing_7; }
	inline RuntimeObject ** get_address_of_Missing_7() { return &___Missing_7; }
	inline void set_Missing_7(RuntimeObject * value)
	{
		___Missing_7 = value;
		Il2CppCodeGenWriteBarrier((&___Missing_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T_H
#ifndef DATETIME_T507798353_H
#define DATETIME_T507798353_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t507798353 
{
public:
	// System.TimeSpan System.DateTime::ticks
	TimeSpan_t2986675223  ___ticks_0;
	// System.DateTimeKind System.DateTime::kind
	int32_t ___kind_1;

public:
	inline static int32_t get_offset_of_ticks_0() { return static_cast<int32_t>(offsetof(DateTime_t507798353, ___ticks_0)); }
	inline TimeSpan_t2986675223  get_ticks_0() const { return ___ticks_0; }
	inline TimeSpan_t2986675223 * get_address_of_ticks_0() { return &___ticks_0; }
	inline void set_ticks_0(TimeSpan_t2986675223  value)
	{
		___ticks_0 = value;
	}

	inline static int32_t get_offset_of_kind_1() { return static_cast<int32_t>(offsetof(DateTime_t507798353, ___kind_1)); }
	inline int32_t get_kind_1() const { return ___kind_1; }
	inline int32_t* get_address_of_kind_1() { return &___kind_1; }
	inline void set_kind_1(int32_t value)
	{
		___kind_1 = value;
	}
};

struct DateTime_t507798353_StaticFields
{
public:
	// System.DateTime System.DateTime::MaxValue
	DateTime_t507798353  ___MaxValue_2;
	// System.DateTime System.DateTime::MinValue
	DateTime_t507798353  ___MinValue_3;
	// System.String[] System.DateTime::ParseTimeFormats
	StringU5BU5D_t522578821* ___ParseTimeFormats_4;
	// System.String[] System.DateTime::ParseYearDayMonthFormats
	StringU5BU5D_t522578821* ___ParseYearDayMonthFormats_5;
	// System.String[] System.DateTime::ParseYearMonthDayFormats
	StringU5BU5D_t522578821* ___ParseYearMonthDayFormats_6;
	// System.String[] System.DateTime::ParseDayMonthYearFormats
	StringU5BU5D_t522578821* ___ParseDayMonthYearFormats_7;
	// System.String[] System.DateTime::ParseMonthDayYearFormats
	StringU5BU5D_t522578821* ___ParseMonthDayYearFormats_8;
	// System.String[] System.DateTime::MonthDayShortFormats
	StringU5BU5D_t522578821* ___MonthDayShortFormats_9;
	// System.String[] System.DateTime::DayMonthShortFormats
	StringU5BU5D_t522578821* ___DayMonthShortFormats_10;
	// System.Int32[] System.DateTime::daysmonth
	Int32U5BU5D_t2856113350* ___daysmonth_11;
	// System.Int32[] System.DateTime::daysmonthleap
	Int32U5BU5D_t2856113350* ___daysmonthleap_12;
	// System.Object System.DateTime::to_local_time_span_object
	RuntimeObject * ___to_local_time_span_object_13;
	// System.Int64 System.DateTime::last_now
	int64_t ___last_now_14;

public:
	inline static int32_t get_offset_of_MaxValue_2() { return static_cast<int32_t>(offsetof(DateTime_t507798353_StaticFields, ___MaxValue_2)); }
	inline DateTime_t507798353  get_MaxValue_2() const { return ___MaxValue_2; }
	inline DateTime_t507798353 * get_address_of_MaxValue_2() { return &___MaxValue_2; }
	inline void set_MaxValue_2(DateTime_t507798353  value)
	{
		___MaxValue_2 = value;
	}

	inline static int32_t get_offset_of_MinValue_3() { return static_cast<int32_t>(offsetof(DateTime_t507798353_StaticFields, ___MinValue_3)); }
	inline DateTime_t507798353  get_MinValue_3() const { return ___MinValue_3; }
	inline DateTime_t507798353 * get_address_of_MinValue_3() { return &___MinValue_3; }
	inline void set_MinValue_3(DateTime_t507798353  value)
	{
		___MinValue_3 = value;
	}

	inline static int32_t get_offset_of_ParseTimeFormats_4() { return static_cast<int32_t>(offsetof(DateTime_t507798353_StaticFields, ___ParseTimeFormats_4)); }
	inline StringU5BU5D_t522578821* get_ParseTimeFormats_4() const { return ___ParseTimeFormats_4; }
	inline StringU5BU5D_t522578821** get_address_of_ParseTimeFormats_4() { return &___ParseTimeFormats_4; }
	inline void set_ParseTimeFormats_4(StringU5BU5D_t522578821* value)
	{
		___ParseTimeFormats_4 = value;
		Il2CppCodeGenWriteBarrier((&___ParseTimeFormats_4), value);
	}

	inline static int32_t get_offset_of_ParseYearDayMonthFormats_5() { return static_cast<int32_t>(offsetof(DateTime_t507798353_StaticFields, ___ParseYearDayMonthFormats_5)); }
	inline StringU5BU5D_t522578821* get_ParseYearDayMonthFormats_5() const { return ___ParseYearDayMonthFormats_5; }
	inline StringU5BU5D_t522578821** get_address_of_ParseYearDayMonthFormats_5() { return &___ParseYearDayMonthFormats_5; }
	inline void set_ParseYearDayMonthFormats_5(StringU5BU5D_t522578821* value)
	{
		___ParseYearDayMonthFormats_5 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearDayMonthFormats_5), value);
	}

	inline static int32_t get_offset_of_ParseYearMonthDayFormats_6() { return static_cast<int32_t>(offsetof(DateTime_t507798353_StaticFields, ___ParseYearMonthDayFormats_6)); }
	inline StringU5BU5D_t522578821* get_ParseYearMonthDayFormats_6() const { return ___ParseYearMonthDayFormats_6; }
	inline StringU5BU5D_t522578821** get_address_of_ParseYearMonthDayFormats_6() { return &___ParseYearMonthDayFormats_6; }
	inline void set_ParseYearMonthDayFormats_6(StringU5BU5D_t522578821* value)
	{
		___ParseYearMonthDayFormats_6 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearMonthDayFormats_6), value);
	}

	inline static int32_t get_offset_of_ParseDayMonthYearFormats_7() { return static_cast<int32_t>(offsetof(DateTime_t507798353_StaticFields, ___ParseDayMonthYearFormats_7)); }
	inline StringU5BU5D_t522578821* get_ParseDayMonthYearFormats_7() const { return ___ParseDayMonthYearFormats_7; }
	inline StringU5BU5D_t522578821** get_address_of_ParseDayMonthYearFormats_7() { return &___ParseDayMonthYearFormats_7; }
	inline void set_ParseDayMonthYearFormats_7(StringU5BU5D_t522578821* value)
	{
		___ParseDayMonthYearFormats_7 = value;
		Il2CppCodeGenWriteBarrier((&___ParseDayMonthYearFormats_7), value);
	}

	inline static int32_t get_offset_of_ParseMonthDayYearFormats_8() { return static_cast<int32_t>(offsetof(DateTime_t507798353_StaticFields, ___ParseMonthDayYearFormats_8)); }
	inline StringU5BU5D_t522578821* get_ParseMonthDayYearFormats_8() const { return ___ParseMonthDayYearFormats_8; }
	inline StringU5BU5D_t522578821** get_address_of_ParseMonthDayYearFormats_8() { return &___ParseMonthDayYearFormats_8; }
	inline void set_ParseMonthDayYearFormats_8(StringU5BU5D_t522578821* value)
	{
		___ParseMonthDayYearFormats_8 = value;
		Il2CppCodeGenWriteBarrier((&___ParseMonthDayYearFormats_8), value);
	}

	inline static int32_t get_offset_of_MonthDayShortFormats_9() { return static_cast<int32_t>(offsetof(DateTime_t507798353_StaticFields, ___MonthDayShortFormats_9)); }
	inline StringU5BU5D_t522578821* get_MonthDayShortFormats_9() const { return ___MonthDayShortFormats_9; }
	inline StringU5BU5D_t522578821** get_address_of_MonthDayShortFormats_9() { return &___MonthDayShortFormats_9; }
	inline void set_MonthDayShortFormats_9(StringU5BU5D_t522578821* value)
	{
		___MonthDayShortFormats_9 = value;
		Il2CppCodeGenWriteBarrier((&___MonthDayShortFormats_9), value);
	}

	inline static int32_t get_offset_of_DayMonthShortFormats_10() { return static_cast<int32_t>(offsetof(DateTime_t507798353_StaticFields, ___DayMonthShortFormats_10)); }
	inline StringU5BU5D_t522578821* get_DayMonthShortFormats_10() const { return ___DayMonthShortFormats_10; }
	inline StringU5BU5D_t522578821** get_address_of_DayMonthShortFormats_10() { return &___DayMonthShortFormats_10; }
	inline void set_DayMonthShortFormats_10(StringU5BU5D_t522578821* value)
	{
		___DayMonthShortFormats_10 = value;
		Il2CppCodeGenWriteBarrier((&___DayMonthShortFormats_10), value);
	}

	inline static int32_t get_offset_of_daysmonth_11() { return static_cast<int32_t>(offsetof(DateTime_t507798353_StaticFields, ___daysmonth_11)); }
	inline Int32U5BU5D_t2856113350* get_daysmonth_11() const { return ___daysmonth_11; }
	inline Int32U5BU5D_t2856113350** get_address_of_daysmonth_11() { return &___daysmonth_11; }
	inline void set_daysmonth_11(Int32U5BU5D_t2856113350* value)
	{
		___daysmonth_11 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonth_11), value);
	}

	inline static int32_t get_offset_of_daysmonthleap_12() { return static_cast<int32_t>(offsetof(DateTime_t507798353_StaticFields, ___daysmonthleap_12)); }
	inline Int32U5BU5D_t2856113350* get_daysmonthleap_12() const { return ___daysmonthleap_12; }
	inline Int32U5BU5D_t2856113350** get_address_of_daysmonthleap_12() { return &___daysmonthleap_12; }
	inline void set_daysmonthleap_12(Int32U5BU5D_t2856113350* value)
	{
		___daysmonthleap_12 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonthleap_12), value);
	}

	inline static int32_t get_offset_of_to_local_time_span_object_13() { return static_cast<int32_t>(offsetof(DateTime_t507798353_StaticFields, ___to_local_time_span_object_13)); }
	inline RuntimeObject * get_to_local_time_span_object_13() const { return ___to_local_time_span_object_13; }
	inline RuntimeObject ** get_address_of_to_local_time_span_object_13() { return &___to_local_time_span_object_13; }
	inline void set_to_local_time_span_object_13(RuntimeObject * value)
	{
		___to_local_time_span_object_13 = value;
		Il2CppCodeGenWriteBarrier((&___to_local_time_span_object_13), value);
	}

	inline static int32_t get_offset_of_last_now_14() { return static_cast<int32_t>(offsetof(DateTime_t507798353_StaticFields, ___last_now_14)); }
	inline int64_t get_last_now_14() const { return ___last_now_14; }
	inline int64_t* get_address_of_last_now_14() { return &___last_now_14; }
	inline void set_last_now_14(int64_t value)
	{
		___last_now_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T507798353_H
#ifndef DATETIMEOFFSET_T2220763929_H
#define DATETIMEOFFSET_T2220763929_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTimeOffset
struct  DateTimeOffset_t2220763929 
{
public:
	// System.DateTime System.DateTimeOffset::dt
	DateTime_t507798353  ___dt_2;
	// System.TimeSpan System.DateTimeOffset::utc_offset
	TimeSpan_t2986675223  ___utc_offset_3;

public:
	inline static int32_t get_offset_of_dt_2() { return static_cast<int32_t>(offsetof(DateTimeOffset_t2220763929, ___dt_2)); }
	inline DateTime_t507798353  get_dt_2() const { return ___dt_2; }
	inline DateTime_t507798353 * get_address_of_dt_2() { return &___dt_2; }
	inline void set_dt_2(DateTime_t507798353  value)
	{
		___dt_2 = value;
	}

	inline static int32_t get_offset_of_utc_offset_3() { return static_cast<int32_t>(offsetof(DateTimeOffset_t2220763929, ___utc_offset_3)); }
	inline TimeSpan_t2986675223  get_utc_offset_3() const { return ___utc_offset_3; }
	inline TimeSpan_t2986675223 * get_address_of_utc_offset_3() { return &___utc_offset_3; }
	inline void set_utc_offset_3(TimeSpan_t2986675223  value)
	{
		___utc_offset_3 = value;
	}
};

struct DateTimeOffset_t2220763929_StaticFields
{
public:
	// System.DateTimeOffset System.DateTimeOffset::MaxValue
	DateTimeOffset_t2220763929  ___MaxValue_0;
	// System.DateTimeOffset System.DateTimeOffset::MinValue
	DateTimeOffset_t2220763929  ___MinValue_1;

public:
	inline static int32_t get_offset_of_MaxValue_0() { return static_cast<int32_t>(offsetof(DateTimeOffset_t2220763929_StaticFields, ___MaxValue_0)); }
	inline DateTimeOffset_t2220763929  get_MaxValue_0() const { return ___MaxValue_0; }
	inline DateTimeOffset_t2220763929 * get_address_of_MaxValue_0() { return &___MaxValue_0; }
	inline void set_MaxValue_0(DateTimeOffset_t2220763929  value)
	{
		___MaxValue_0 = value;
	}

	inline static int32_t get_offset_of_MinValue_1() { return static_cast<int32_t>(offsetof(DateTimeOffset_t2220763929_StaticFields, ___MinValue_1)); }
	inline DateTimeOffset_t2220763929  get_MinValue_1() const { return ___MinValue_1; }
	inline DateTimeOffset_t2220763929 * get_address_of_MinValue_1() { return &___MinValue_1; }
	inline void set_MinValue_1(DateTimeOffset_t2220763929  value)
	{
		___MinValue_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMEOFFSET_T2220763929_H
// System.Type[]
struct TypeU5BU5D_t1830665827  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Type_t * m_Items[1];

public:
	inline Type_t * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Type_t ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Type_t * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Type_t * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Type_t ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Type_t * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};



// System.Int32 System.Reflection.CustomAttributeTypedArgument::GetHashCode()
extern "C"  int32_t CustomAttributeTypedArgument_GetHashCode_m1148730416 (CustomAttributeTypedArgument_t2667702721 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.CustomAttributeTypedArgument::Equals(System.Object)
extern "C"  bool CustomAttributeTypedArgument_Equals_m1480105269 (CustomAttributeTypedArgument_t2667702721 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.TimeSpan::GetHashCode()
extern "C"  int32_t TimeSpan_GetHashCode_m3640128864 (TimeSpan_t2986675223 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.TimeSpan::Equals(System.Object)
extern "C"  bool TimeSpan_Equals_m2659880825 (TimeSpan_t2986675223 * __this, RuntimeObject * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Object::.ctor()
extern "C"  void Object__ctor_m3446193457 (RuntimeObject * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Type System.Type::GetTypeFromHandle(System.RuntimeTypeHandle)
extern "C"  Type_t * Type_GetTypeFromHandle_m3171413529 (RuntimeObject * __this /* static, unused */, RuntimeTypeHandle_t637624852  ___handle0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Object System.Activator::CreateInstance(System.Type)
extern "C"  RuntimeObject * Activator_CreateInstance_m88608359 (RuntimeObject * __this /* static, unused */, Type_t * ___type0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.DateTime::CompareTo(System.DateTime)
extern "C"  int32_t DateTime_CompareTo_m2695414402 (DateTime_t507798353 * __this, DateTime_t507798353  ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.DateTimeOffset::CompareTo(System.DateTimeOffset)
extern "C"  int32_t DateTimeOffset_CompareTo_m1080840663 (DateTimeOffset_t2220763929 * __this, DateTimeOffset_t2220763929  ___other0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Guid::CompareTo(System.Guid)
extern "C"  int32_t Guid_CompareTo_m2483260021 (Guid_t * __this, Guid_t  ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Int32::CompareTo(System.Int32)
extern "C"  int32_t Int32_CompareTo_m960498275 (int32_t* __this, int32_t ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.TimeSpan::CompareTo(System.TimeSpan)
extern "C"  int32_t TimeSpan_CompareTo_m3198449526 (TimeSpan_t2986675223 * __this, TimeSpan_t2986675223  ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Boolean::GetHashCode()
extern "C"  int32_t Boolean_GetHashCode_m1880341182 (bool* __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Boolean::Equals(System.Boolean)
extern "C"  bool Boolean_Equals_m67531367 (bool* __this, bool ___obj0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.DateTime::GetHashCode()
extern "C"  int32_t DateTime_GetHashCode_m1398597643 (DateTime_t507798353 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.DateTime::Equals(System.DateTime)
extern "C"  bool DateTime_Equals_m2586536838 (DateTime_t507798353 * __this, DateTime_t507798353  ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.DateTimeOffset::GetHashCode()
extern "C"  int32_t DateTimeOffset_GetHashCode_m3032241378 (DateTimeOffset_t2220763929 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.DateTimeOffset::Equals(System.DateTimeOffset)
extern "C"  bool DateTimeOffset_Equals_m4119379754 (DateTimeOffset_t2220763929 * __this, DateTimeOffset_t2220763929  ___other0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Guid::GetHashCode()
extern "C"  int32_t Guid_GetHashCode_m3315775205 (Guid_t * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Guid::Equals(System.Guid)
extern "C"  bool Guid_Equals_m3913097454 (Guid_t * __this, Guid_t  ___g0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Int32::GetHashCode()
extern "C"  int32_t Int32_GetHashCode_m339244912 (int32_t* __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Int32::Equals(System.Int32)
extern "C"  bool Int32_Equals_m2128805555 (int32_t* __this, int32_t ___obj0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.TimeSpan::Equals(System.TimeSpan)
extern "C"  bool TimeSpan_Equals_m1555084785 (TimeSpan_t2986675223 * __this, TimeSpan_t2986675223  ___obj0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Reflection.CustomAttributeTypedArgument>::.ctor()
extern "C"  void DefaultComparer__ctor_m1384442037_gshared (DefaultComparer_t2145707355 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((EqualityComparer_1_t3200323440 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t3200323440 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t3200323440 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Reflection.CustomAttributeTypedArgument>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m1103173526_gshared (DefaultComparer_t2145707355 * __this, CustomAttributeTypedArgument_t2667702721  ___obj0, const RuntimeMethod* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t L_1 = CustomAttributeTypedArgument_GetHashCode_m1148730416((CustomAttributeTypedArgument_t2667702721 *)(&___obj0), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Reflection.CustomAttributeTypedArgument>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m3406131826_gshared (DefaultComparer_t2145707355 * __this, CustomAttributeTypedArgument_t2667702721  ___x0, CustomAttributeTypedArgument_t2667702721  ___y1, const RuntimeMethod* method)
{
	{
		goto IL_0015;
	}
	{
		CustomAttributeTypedArgument_t2667702721  L_1 = ___y1;
		CustomAttributeTypedArgument_t2667702721  L_2 = L_1;
		RuntimeObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((RuntimeObject*)(RuntimeObject *)L_3) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		CustomAttributeTypedArgument_t2667702721  L_4 = ___y1;
		CustomAttributeTypedArgument_t2667702721  L_5 = L_4;
		RuntimeObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_5);
		bool L_7 = CustomAttributeTypedArgument_Equals_m1480105269((CustomAttributeTypedArgument_t2667702721 *)(&___x0), (RuntimeObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.TimeSpan>::.ctor()
extern "C"  void DefaultComparer__ctor_m1340109020_gshared (DefaultComparer_t2464679857 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((EqualityComparer_1_t3519295942 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t3519295942 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t3519295942 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.TimeSpan>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m204635457_gshared (DefaultComparer_t2464679857 * __this, TimeSpan_t2986675223  ___obj0, const RuntimeMethod* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t L_1 = TimeSpan_GetHashCode_m3640128864((TimeSpan_t2986675223 *)(&___obj0), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.TimeSpan>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m2748320953_gshared (DefaultComparer_t2464679857 * __this, TimeSpan_t2986675223  ___x0, TimeSpan_t2986675223  ___y1, const RuntimeMethod* method)
{
	{
		goto IL_0015;
	}
	{
		TimeSpan_t2986675223  L_1 = ___y1;
		TimeSpan_t2986675223  L_2 = L_1;
		RuntimeObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((RuntimeObject*)(RuntimeObject *)L_3) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		TimeSpan_t2986675223  L_4 = ___y1;
		TimeSpan_t2986675223  L_5 = L_4;
		RuntimeObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_5);
		bool L_7 = TimeSpan_Equals_m2659880825((TimeSpan_t2986675223 *)(&___x0), (RuntimeObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<System.Boolean>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m414177633_gshared (EqualityComparer_1_t2592293618 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m3446193457((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<System.Boolean>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m1182025640_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m1182025640_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3171413529(NULL /*static, unused*/, (RuntimeTypeHandle_t637624852 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m3171413529(NULL /*static, unused*/, (RuntimeTypeHandle_t637624852 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(40 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m3171413529(NULL /*static, unused*/, (RuntimeTypeHandle_t637624852 )LoadTypeToken(GenericEqualityComparer_1_t2573898200_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1830665827* L_4 = (TypeU5BU5D_t1830665827*)((TypeU5BU5D_t1830665827*)SZArrayNew(TypeU5BU5D_t1830665827_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m3171413529(NULL /*static, unused*/, (RuntimeTypeHandle_t637624852 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1830665827* >::Invoke(79 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1830665827*)L_4);
		RuntimeObject * L_7 = Activator_CreateInstance_m88608359(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t2592293618_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(((EqualityComparer_1_t2592293618 *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t1537677533 * L_8 = (DefaultComparer_t1537677533 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t1537677533 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t2592293618_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.Boolean>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m3101194977_gshared (EqualityComparer_1_t2592293618 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t2592293618 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, bool >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.Boolean>::GetHashCode(T) */, (EqualityComparer_1_t2592293618 *)__this, (bool)((*(bool*)((bool*)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<System.Boolean>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m548228993_gshared (EqualityComparer_1_t2592293618 * __this, RuntimeObject * ___x0, RuntimeObject * ___y1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___x0;
		RuntimeObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t2592293618 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, bool, bool >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Boolean>::Equals(T,T) */, (EqualityComparer_1_t2592293618 *)__this, (bool)((*(bool*)((bool*)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (bool)((*(bool*)((bool*)UnBox(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.Boolean>::get_Default()
extern "C"  EqualityComparer_1_t2592293618 * EqualityComparer_1_get_Default_m2366011092_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t2592293618 * L_0 = ((EqualityComparer_1_t2592293618_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<System.DateTime>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m2727624630_gshared (EqualityComparer_1_t1040419072 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m3446193457((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<System.DateTime>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m1522028557_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m1522028557_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3171413529(NULL /*static, unused*/, (RuntimeTypeHandle_t637624852 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m3171413529(NULL /*static, unused*/, (RuntimeTypeHandle_t637624852 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(40 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m3171413529(NULL /*static, unused*/, (RuntimeTypeHandle_t637624852 )LoadTypeToken(GenericEqualityComparer_1_t2573898200_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1830665827* L_4 = (TypeU5BU5D_t1830665827*)((TypeU5BU5D_t1830665827*)SZArrayNew(TypeU5BU5D_t1830665827_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m3171413529(NULL /*static, unused*/, (RuntimeTypeHandle_t637624852 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1830665827* >::Invoke(79 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1830665827*)L_4);
		RuntimeObject * L_7 = Activator_CreateInstance_m88608359(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t1040419072_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(((EqualityComparer_1_t1040419072 *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t4280770283 * L_8 = (DefaultComparer_t4280770283 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t4280770283 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t1040419072_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.DateTime>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m2054268489_gshared (EqualityComparer_1_t1040419072 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t1040419072 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, DateTime_t507798353  >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.DateTime>::GetHashCode(T) */, (EqualityComparer_1_t1040419072 *)__this, (DateTime_t507798353 )((*(DateTime_t507798353 *)((DateTime_t507798353 *)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<System.DateTime>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m3838702662_gshared (EqualityComparer_1_t1040419072 * __this, RuntimeObject * ___x0, RuntimeObject * ___y1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___x0;
		RuntimeObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t1040419072 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, DateTime_t507798353 , DateTime_t507798353  >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.DateTime>::Equals(T,T) */, (EqualityComparer_1_t1040419072 *)__this, (DateTime_t507798353 )((*(DateTime_t507798353 *)((DateTime_t507798353 *)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (DateTime_t507798353 )((*(DateTime_t507798353 *)((DateTime_t507798353 *)UnBox(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.DateTime>::get_Default()
extern "C"  EqualityComparer_1_t1040419072 * EqualityComparer_1_get_Default_m1157086640_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t1040419072 * L_0 = ((EqualityComparer_1_t1040419072_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<System.DateTimeOffset>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m3215209271_gshared (EqualityComparer_1_t2753384648 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m3446193457((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<System.DateTimeOffset>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m1277617039_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m1277617039_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3171413529(NULL /*static, unused*/, (RuntimeTypeHandle_t637624852 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m3171413529(NULL /*static, unused*/, (RuntimeTypeHandle_t637624852 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(40 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m3171413529(NULL /*static, unused*/, (RuntimeTypeHandle_t637624852 )LoadTypeToken(GenericEqualityComparer_1_t2573898200_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1830665827* L_4 = (TypeU5BU5D_t1830665827*)((TypeU5BU5D_t1830665827*)SZArrayNew(TypeU5BU5D_t1830665827_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m3171413529(NULL /*static, unused*/, (RuntimeTypeHandle_t637624852 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1830665827* >::Invoke(79 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1830665827*)L_4);
		RuntimeObject * L_7 = Activator_CreateInstance_m88608359(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t2753384648_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(((EqualityComparer_1_t2753384648 *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t1698768563 * L_8 = (DefaultComparer_t1698768563 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t1698768563 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t2753384648_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.DateTimeOffset>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m1829365185_gshared (EqualityComparer_1_t2753384648 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t2753384648 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, DateTimeOffset_t2220763929  >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.DateTimeOffset>::GetHashCode(T) */, (EqualityComparer_1_t2753384648 *)__this, (DateTimeOffset_t2220763929 )((*(DateTimeOffset_t2220763929 *)((DateTimeOffset_t2220763929 *)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<System.DateTimeOffset>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m3595952617_gshared (EqualityComparer_1_t2753384648 * __this, RuntimeObject * ___x0, RuntimeObject * ___y1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___x0;
		RuntimeObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t2753384648 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, DateTimeOffset_t2220763929 , DateTimeOffset_t2220763929  >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.DateTimeOffset>::Equals(T,T) */, (EqualityComparer_1_t2753384648 *)__this, (DateTimeOffset_t2220763929 )((*(DateTimeOffset_t2220763929 *)((DateTimeOffset_t2220763929 *)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (DateTimeOffset_t2220763929 )((*(DateTimeOffset_t2220763929 *)((DateTimeOffset_t2220763929 *)UnBox(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.DateTimeOffset>::get_Default()
extern "C"  EqualityComparer_1_t2753384648 * EqualityComparer_1_get_Default_m498495543_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t2753384648 * L_0 = ((EqualityComparer_1_t2753384648_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<System.Guid>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m2592865134_gshared (EqualityComparer_1_t499284453 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m3446193457((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<System.Guid>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m3704847281_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m3704847281_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3171413529(NULL /*static, unused*/, (RuntimeTypeHandle_t637624852 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m3171413529(NULL /*static, unused*/, (RuntimeTypeHandle_t637624852 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(40 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m3171413529(NULL /*static, unused*/, (RuntimeTypeHandle_t637624852 )LoadTypeToken(GenericEqualityComparer_1_t2573898200_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1830665827* L_4 = (TypeU5BU5D_t1830665827*)((TypeU5BU5D_t1830665827*)SZArrayNew(TypeU5BU5D_t1830665827_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m3171413529(NULL /*static, unused*/, (RuntimeTypeHandle_t637624852 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1830665827* >::Invoke(79 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1830665827*)L_4);
		RuntimeObject * L_7 = Activator_CreateInstance_m88608359(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t499284453_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(((EqualityComparer_1_t499284453 *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t3739635664 * L_8 = (DefaultComparer_t3739635664 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t3739635664 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t499284453_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.Guid>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m362938331_gshared (EqualityComparer_1_t499284453 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t499284453 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, Guid_t  >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.Guid>::GetHashCode(T) */, (EqualityComparer_1_t499284453 *)__this, (Guid_t )((*(Guid_t *)((Guid_t *)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<System.Guid>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m424838771_gshared (EqualityComparer_1_t499284453 * __this, RuntimeObject * ___x0, RuntimeObject * ___y1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___x0;
		RuntimeObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t499284453 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, Guid_t , Guid_t  >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Guid>::Equals(T,T) */, (EqualityComparer_1_t499284453 *)__this, (Guid_t )((*(Guid_t *)((Guid_t *)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (Guid_t )((*(Guid_t *)((Guid_t *)UnBox(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.Guid>::get_Default()
extern "C"  EqualityComparer_1_t499284453 * EqualityComparer_1_get_Default_m2110497837_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t499284453 * L_0 = ((EqualityComparer_1_t499284453_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<System.Int32>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m554199480_gshared (EqualityComparer_1_t3103755918 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m3446193457((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<System.Int32>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m3173561107_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m3173561107_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3171413529(NULL /*static, unused*/, (RuntimeTypeHandle_t637624852 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m3171413529(NULL /*static, unused*/, (RuntimeTypeHandle_t637624852 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(40 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m3171413529(NULL /*static, unused*/, (RuntimeTypeHandle_t637624852 )LoadTypeToken(GenericEqualityComparer_1_t2573898200_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1830665827* L_4 = (TypeU5BU5D_t1830665827*)((TypeU5BU5D_t1830665827*)SZArrayNew(TypeU5BU5D_t1830665827_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m3171413529(NULL /*static, unused*/, (RuntimeTypeHandle_t637624852 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1830665827* >::Invoke(79 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1830665827*)L_4);
		RuntimeObject * L_7 = Activator_CreateInstance_m88608359(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t3103755918_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(((EqualityComparer_1_t3103755918 *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t2049139833 * L_8 = (DefaultComparer_t2049139833 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t2049139833 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t3103755918_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.Int32>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m3016813880_gshared (EqualityComparer_1_t3103755918 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t3103755918 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, int32_t >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.Int32>::GetHashCode(T) */, (EqualityComparer_1_t3103755918 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<System.Int32>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m1419391228_gshared (EqualityComparer_1_t3103755918 * __this, RuntimeObject * ___x0, RuntimeObject * ___y1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___x0;
		RuntimeObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t3103755918 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, int32_t, int32_t >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Int32>::Equals(T,T) */, (EqualityComparer_1_t3103755918 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (int32_t)((*(int32_t*)((int32_t*)UnBox(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.Int32>::get_Default()
extern "C"  EqualityComparer_1_t3103755918 * EqualityComparer_1_get_Default_m3415920396_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t3103755918 * L_0 = ((EqualityComparer_1_t3103755918_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<System.IntPtr>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m1989801558_gshared (EqualityComparer_1_t1845253994 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m3446193457((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<System.IntPtr>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m3466349081_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m3466349081_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3171413529(NULL /*static, unused*/, (RuntimeTypeHandle_t637624852 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m3171413529(NULL /*static, unused*/, (RuntimeTypeHandle_t637624852 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(40 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m3171413529(NULL /*static, unused*/, (RuntimeTypeHandle_t637624852 )LoadTypeToken(GenericEqualityComparer_1_t2573898200_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1830665827* L_4 = (TypeU5BU5D_t1830665827*)((TypeU5BU5D_t1830665827*)SZArrayNew(TypeU5BU5D_t1830665827_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m3171413529(NULL /*static, unused*/, (RuntimeTypeHandle_t637624852 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1830665827* >::Invoke(79 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1830665827*)L_4);
		RuntimeObject * L_7 = Activator_CreateInstance_m88608359(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t1845253994_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(((EqualityComparer_1_t1845253994 *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t790637909 * L_8 = (DefaultComparer_t790637909 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t790637909 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t1845253994_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.IntPtr>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m2049292064_gshared (EqualityComparer_1_t1845253994 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t1845253994 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, intptr_t >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.IntPtr>::GetHashCode(T) */, (EqualityComparer_1_t1845253994 *)__this, (intptr_t)((*(intptr_t*)((intptr_t*)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<System.IntPtr>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m3589862234_gshared (EqualityComparer_1_t1845253994 * __this, RuntimeObject * ___x0, RuntimeObject * ___y1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___x0;
		RuntimeObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t1845253994 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, intptr_t, intptr_t >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.IntPtr>::Equals(T,T) */, (EqualityComparer_1_t1845253994 *)__this, (intptr_t)((*(intptr_t*)((intptr_t*)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (intptr_t)((*(intptr_t*)((intptr_t*)UnBox(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.IntPtr>::get_Default()
extern "C"  EqualityComparer_1_t1845253994 * EqualityComparer_1_get_Default_m2580473628_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t1845253994 * L_0 = ((EqualityComparer_1_t1845253994_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<System.Object>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m1634940858_gshared (EqualityComparer_1_t3828637722 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m3446193457((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<System.Object>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m3326013475_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m3326013475_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3171413529(NULL /*static, unused*/, (RuntimeTypeHandle_t637624852 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m3171413529(NULL /*static, unused*/, (RuntimeTypeHandle_t637624852 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(40 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m3171413529(NULL /*static, unused*/, (RuntimeTypeHandle_t637624852 )LoadTypeToken(GenericEqualityComparer_1_t2573898200_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1830665827* L_4 = (TypeU5BU5D_t1830665827*)((TypeU5BU5D_t1830665827*)SZArrayNew(TypeU5BU5D_t1830665827_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m3171413529(NULL /*static, unused*/, (RuntimeTypeHandle_t637624852 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1830665827* >::Invoke(79 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1830665827*)L_4);
		RuntimeObject * L_7 = Activator_CreateInstance_m88608359(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t3828637722_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(((EqualityComparer_1_t3828637722 *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t2774021637 * L_8 = (DefaultComparer_t2774021637 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t2774021637 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t3828637722_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.Object>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m2886129788_gshared (EqualityComparer_1_t3828637722 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t3828637722 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, RuntimeObject * >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.Object>::GetHashCode(T) */, (EqualityComparer_1_t3828637722 *)__this, (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<System.Object>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m2631093730_gshared (EqualityComparer_1_t3828637722 * __this, RuntimeObject * ___x0, RuntimeObject * ___y1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___x0;
		RuntimeObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t3828637722 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, RuntimeObject *, RuntimeObject * >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Object>::Equals(T,T) */, (EqualityComparer_1_t3828637722 *)__this, (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))), (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.Object>::get_Default()
extern "C"  EqualityComparer_1_t3828637722 * EqualityComparer_1_get_Default_m1577725120_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t3828637722 * L_0 = ((EqualityComparer_1_t3828637722_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<System.Reflection.CustomAttributeNamedArgument>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m2100856524_gshared (EqualityComparer_1_t958351058 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m3446193457((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<System.Reflection.CustomAttributeNamedArgument>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m3327302871_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m3327302871_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3171413529(NULL /*static, unused*/, (RuntimeTypeHandle_t637624852 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m3171413529(NULL /*static, unused*/, (RuntimeTypeHandle_t637624852 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(40 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m3171413529(NULL /*static, unused*/, (RuntimeTypeHandle_t637624852 )LoadTypeToken(GenericEqualityComparer_1_t2573898200_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1830665827* L_4 = (TypeU5BU5D_t1830665827*)((TypeU5BU5D_t1830665827*)SZArrayNew(TypeU5BU5D_t1830665827_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m3171413529(NULL /*static, unused*/, (RuntimeTypeHandle_t637624852 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1830665827* >::Invoke(79 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1830665827*)L_4);
		RuntimeObject * L_7 = Activator_CreateInstance_m88608359(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t958351058_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(((EqualityComparer_1_t958351058 *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t4198702269 * L_8 = (DefaultComparer_t4198702269 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t4198702269 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t958351058_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m3339063844_gshared (EqualityComparer_1_t958351058 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t958351058 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, CustomAttributeNamedArgument_t425730339  >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.Reflection.CustomAttributeNamedArgument>::GetHashCode(T) */, (EqualityComparer_1_t958351058 *)__this, (CustomAttributeNamedArgument_t425730339 )((*(CustomAttributeNamedArgument_t425730339 *)((CustomAttributeNamedArgument_t425730339 *)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m1115593231_gshared (EqualityComparer_1_t958351058 * __this, RuntimeObject * ___x0, RuntimeObject * ___y1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___x0;
		RuntimeObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t958351058 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, CustomAttributeNamedArgument_t425730339 , CustomAttributeNamedArgument_t425730339  >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Reflection.CustomAttributeNamedArgument>::Equals(T,T) */, (EqualityComparer_1_t958351058 *)__this, (CustomAttributeNamedArgument_t425730339 )((*(CustomAttributeNamedArgument_t425730339 *)((CustomAttributeNamedArgument_t425730339 *)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (CustomAttributeNamedArgument_t425730339 )((*(CustomAttributeNamedArgument_t425730339 *)((CustomAttributeNamedArgument_t425730339 *)UnBox(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.Reflection.CustomAttributeNamedArgument>::get_Default()
extern "C"  EqualityComparer_1_t958351058 * EqualityComparer_1_get_Default_m2286589575_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t958351058 * L_0 = ((EqualityComparer_1_t958351058_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<System.Reflection.CustomAttributeTypedArgument>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m4048630488_gshared (EqualityComparer_1_t3200323440 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m3446193457((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<System.Reflection.CustomAttributeTypedArgument>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m7634007_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m7634007_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3171413529(NULL /*static, unused*/, (RuntimeTypeHandle_t637624852 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m3171413529(NULL /*static, unused*/, (RuntimeTypeHandle_t637624852 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(40 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m3171413529(NULL /*static, unused*/, (RuntimeTypeHandle_t637624852 )LoadTypeToken(GenericEqualityComparer_1_t2573898200_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1830665827* L_4 = (TypeU5BU5D_t1830665827*)((TypeU5BU5D_t1830665827*)SZArrayNew(TypeU5BU5D_t1830665827_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m3171413529(NULL /*static, unused*/, (RuntimeTypeHandle_t637624852 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1830665827* >::Invoke(79 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1830665827*)L_4);
		RuntimeObject * L_7 = Activator_CreateInstance_m88608359(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t3200323440_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(((EqualityComparer_1_t3200323440 *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t2145707355 * L_8 = (DefaultComparer_t2145707355 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t2145707355 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t3200323440_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m2856991176_gshared (EqualityComparer_1_t3200323440 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t3200323440 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, CustomAttributeTypedArgument_t2667702721  >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.Reflection.CustomAttributeTypedArgument>::GetHashCode(T) */, (EqualityComparer_1_t3200323440 *)__this, (CustomAttributeTypedArgument_t2667702721 )((*(CustomAttributeTypedArgument_t2667702721 *)((CustomAttributeTypedArgument_t2667702721 *)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m881765641_gshared (EqualityComparer_1_t3200323440 * __this, RuntimeObject * ___x0, RuntimeObject * ___y1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___x0;
		RuntimeObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t3200323440 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, CustomAttributeTypedArgument_t2667702721 , CustomAttributeTypedArgument_t2667702721  >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Reflection.CustomAttributeTypedArgument>::Equals(T,T) */, (EqualityComparer_1_t3200323440 *)__this, (CustomAttributeTypedArgument_t2667702721 )((*(CustomAttributeTypedArgument_t2667702721 *)((CustomAttributeTypedArgument_t2667702721 *)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (CustomAttributeTypedArgument_t2667702721 )((*(CustomAttributeTypedArgument_t2667702721 *)((CustomAttributeTypedArgument_t2667702721 *)UnBox(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.Reflection.CustomAttributeTypedArgument>::get_Default()
extern "C"  EqualityComparer_1_t3200323440 * EqualityComparer_1_get_Default_m3733560950_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t3200323440 * L_0 = ((EqualityComparer_1_t3200323440_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<System.TimeSpan>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m3577946941_gshared (EqualityComparer_1_t3519295942 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m3446193457((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<System.TimeSpan>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m679490880_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m679490880_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3171413529(NULL /*static, unused*/, (RuntimeTypeHandle_t637624852 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m3171413529(NULL /*static, unused*/, (RuntimeTypeHandle_t637624852 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(40 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m3171413529(NULL /*static, unused*/, (RuntimeTypeHandle_t637624852 )LoadTypeToken(GenericEqualityComparer_1_t2573898200_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1830665827* L_4 = (TypeU5BU5D_t1830665827*)((TypeU5BU5D_t1830665827*)SZArrayNew(TypeU5BU5D_t1830665827_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m3171413529(NULL /*static, unused*/, (RuntimeTypeHandle_t637624852 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1830665827* >::Invoke(79 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1830665827*)L_4);
		RuntimeObject * L_7 = Activator_CreateInstance_m88608359(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t3519295942_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(((EqualityComparer_1_t3519295942 *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t2464679857 * L_8 = (DefaultComparer_t2464679857 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t2464679857 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t3519295942_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.TimeSpan>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m2054883387_gshared (EqualityComparer_1_t3519295942 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t3519295942 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, TimeSpan_t2986675223  >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.TimeSpan>::GetHashCode(T) */, (EqualityComparer_1_t3519295942 *)__this, (TimeSpan_t2986675223 )((*(TimeSpan_t2986675223 *)((TimeSpan_t2986675223 *)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<System.TimeSpan>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m3113948850_gshared (EqualityComparer_1_t3519295942 * __this, RuntimeObject * ___x0, RuntimeObject * ___y1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___x0;
		RuntimeObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t3519295942 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, TimeSpan_t2986675223 , TimeSpan_t2986675223  >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.TimeSpan>::Equals(T,T) */, (EqualityComparer_1_t3519295942 *)__this, (TimeSpan_t2986675223 )((*(TimeSpan_t2986675223 *)((TimeSpan_t2986675223 *)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (TimeSpan_t2986675223 )((*(TimeSpan_t2986675223 *)((TimeSpan_t2986675223 *)UnBox(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.TimeSpan>::get_Default()
extern "C"  EqualityComparer_1_t3519295942 * EqualityComparer_1_get_Default_m938318304_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t3519295942 * L_0 = ((EqualityComparer_1_t3519295942_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.GenericComparer`1<System.DateTime>::.ctor()
extern "C"  void GenericComparer_1__ctor_m2661512804_gshared (GenericComparer_1_t2804608443 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((Comparer_1_t3131605253 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (Comparer_1_t3131605253 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Comparer_1_t3131605253 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.GenericComparer`1<System.DateTime>::Compare(T,T)
extern "C"  int32_t GenericComparer_1_Compare_m2985698215_gshared (GenericComparer_1_t2804608443 * __this, DateTime_t507798353  ___x0, DateTime_t507798353  ___y1, const RuntimeMethod* method)
{
	int32_t G_B4_0 = 0;
	{
		goto IL_001e;
	}
	{
		goto IL_001c;
	}
	{
		G_B4_0 = 0;
		goto IL_001d;
	}

IL_001c:
	{
		G_B4_0 = (-1);
	}

IL_001d:
	{
		return G_B4_0;
	}

IL_001e:
	{
		goto IL_002b;
	}
	{
		return 1;
	}

IL_002b:
	{
		DateTime_t507798353  L_3 = ___y1;
		int32_t L_4 = DateTime_CompareTo_m2695414402((DateTime_t507798353 *)(&___x0), (DateTime_t507798353 )L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Void System.Collections.Generic.GenericComparer`1<System.DateTimeOffset>::.ctor()
extern "C"  void GenericComparer_1__ctor_m421231889_gshared (GenericComparer_1_t222606723 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((Comparer_1_t549603533 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (Comparer_1_t549603533 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Comparer_1_t549603533 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.GenericComparer`1<System.DateTimeOffset>::Compare(T,T)
extern "C"  int32_t GenericComparer_1_Compare_m3852478759_gshared (GenericComparer_1_t222606723 * __this, DateTimeOffset_t2220763929  ___x0, DateTimeOffset_t2220763929  ___y1, const RuntimeMethod* method)
{
	int32_t G_B4_0 = 0;
	{
		goto IL_001e;
	}
	{
		goto IL_001c;
	}
	{
		G_B4_0 = 0;
		goto IL_001d;
	}

IL_001c:
	{
		G_B4_0 = (-1);
	}

IL_001d:
	{
		return G_B4_0;
	}

IL_001e:
	{
		goto IL_002b;
	}
	{
		return 1;
	}

IL_002b:
	{
		DateTimeOffset_t2220763929  L_3 = ___y1;
		int32_t L_4 = DateTimeOffset_CompareTo_m1080840663((DateTimeOffset_t2220763929 *)(&___x0), (DateTimeOffset_t2220763929 )L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Void System.Collections.Generic.GenericComparer`1<System.Guid>::.ctor()
extern "C"  void GenericComparer_1__ctor_m2944517658_gshared (GenericComparer_1_t2263473824 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((Comparer_1_t2590470634 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (Comparer_1_t2590470634 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Comparer_1_t2590470634 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.GenericComparer`1<System.Guid>::Compare(T,T)
extern "C"  int32_t GenericComparer_1_Compare_m335732564_gshared (GenericComparer_1_t2263473824 * __this, Guid_t  ___x0, Guid_t  ___y1, const RuntimeMethod* method)
{
	int32_t G_B4_0 = 0;
	{
		goto IL_001e;
	}
	{
		goto IL_001c;
	}
	{
		G_B4_0 = 0;
		goto IL_001d;
	}

IL_001c:
	{
		G_B4_0 = (-1);
	}

IL_001d:
	{
		return G_B4_0;
	}

IL_001e:
	{
		goto IL_002b;
	}
	{
		return 1;
	}

IL_002b:
	{
		Guid_t  L_3 = ___y1;
		int32_t L_4 = Guid_CompareTo_m2483260021((Guid_t *)(&___x0), (Guid_t )L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Void System.Collections.Generic.GenericComparer`1<System.Int32>::.ctor()
extern "C"  void GenericComparer_1__ctor_m1518215989_gshared (GenericComparer_1_t572977993 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((Comparer_1_t899974803 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (Comparer_1_t899974803 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Comparer_1_t899974803 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.GenericComparer`1<System.Int32>::Compare(T,T)
extern "C"  int32_t GenericComparer_1_Compare_m4046430417_gshared (GenericComparer_1_t572977993 * __this, int32_t ___x0, int32_t ___y1, const RuntimeMethod* method)
{
	int32_t G_B4_0 = 0;
	{
		goto IL_001e;
	}
	{
		goto IL_001c;
	}
	{
		G_B4_0 = 0;
		goto IL_001d;
	}

IL_001c:
	{
		G_B4_0 = (-1);
	}

IL_001d:
	{
		return G_B4_0;
	}

IL_001e:
	{
		goto IL_002b;
	}
	{
		return 1;
	}

IL_002b:
	{
		int32_t L_3 = ___y1;
		int32_t L_4 = Int32_CompareTo_m960498275((int32_t*)(&___x0), (int32_t)L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Void System.Collections.Generic.GenericComparer`1<System.Object>::.ctor()
extern "C"  void GenericComparer_1__ctor_m193693475_gshared (GenericComparer_1_t1297859797 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((Comparer_1_t1624856607 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (Comparer_1_t1624856607 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Comparer_1_t1624856607 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.GenericComparer`1<System.Object>::Compare(T,T)
extern "C"  int32_t GenericComparer_1_Compare_m3025673068_gshared (GenericComparer_1_t1297859797 * __this, RuntimeObject * ___x0, RuntimeObject * ___y1, const RuntimeMethod* method)
{
	int32_t G_B4_0 = 0;
	{
		RuntimeObject * L_0 = ___x0;
		if (L_0)
		{
			goto IL_001e;
		}
	}
	{
		RuntimeObject * L_1 = ___y1;
		if (L_1)
		{
			goto IL_001c;
		}
	}
	{
		G_B4_0 = 0;
		goto IL_001d;
	}

IL_001c:
	{
		G_B4_0 = (-1);
	}

IL_001d:
	{
		return G_B4_0;
	}

IL_001e:
	{
		RuntimeObject * L_2 = ___y1;
		if (L_2)
		{
			goto IL_002b;
		}
	}
	{
		return 1;
	}

IL_002b:
	{
		RuntimeObject * L_3 = ___y1;
		NullCheck((RuntimeObject*)(*(&___x0)));
		int32_t L_4 = InterfaceFuncInvoker1< int32_t, RuntimeObject * >::Invoke(0 /* System.Int32 System.IComparable`1<System.Object>::CompareTo(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (RuntimeObject*)(*(&___x0)), (RuntimeObject *)L_3);
		return L_4;
	}
}
// System.Void System.Collections.Generic.GenericComparer`1<System.TimeSpan>::.ctor()
extern "C"  void GenericComparer_1__ctor_m2743817692_gshared (GenericComparer_1_t988518017 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((Comparer_1_t1315514827 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (Comparer_1_t1315514827 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Comparer_1_t1315514827 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.GenericComparer`1<System.TimeSpan>::Compare(T,T)
extern "C"  int32_t GenericComparer_1_Compare_m4032603330_gshared (GenericComparer_1_t988518017 * __this, TimeSpan_t2986675223  ___x0, TimeSpan_t2986675223  ___y1, const RuntimeMethod* method)
{
	int32_t G_B4_0 = 0;
	{
		goto IL_001e;
	}
	{
		goto IL_001c;
	}
	{
		G_B4_0 = 0;
		goto IL_001d;
	}

IL_001c:
	{
		G_B4_0 = (-1);
	}

IL_001d:
	{
		return G_B4_0;
	}

IL_001e:
	{
		goto IL_002b;
	}
	{
		return 1;
	}

IL_002b:
	{
		TimeSpan_t2986675223  L_3 = ___y1;
		int32_t L_4 = TimeSpan_CompareTo_m3198449526((TimeSpan_t2986675223 *)(&___x0), (TimeSpan_t2986675223 )L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Void System.Collections.Generic.GenericEqualityComparer`1<System.Boolean>::.ctor()
extern "C"  void GenericEqualityComparer_1__ctor_m2867822148_gshared (GenericEqualityComparer_1_t1903981024 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((EqualityComparer_1_t2592293618 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t2592293618 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t2592293618 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.Boolean>::GetHashCode(T)
extern "C"  int32_t GenericEqualityComparer_1_GetHashCode_m1405931639_gshared (GenericEqualityComparer_1_t1903981024 * __this, bool ___obj0, const RuntimeMethod* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t L_1 = Boolean_GetHashCode_m1880341182((bool*)(&___obj0), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<System.Boolean>::Equals(T,T)
extern "C"  bool GenericEqualityComparer_1_Equals_m251542842_gshared (GenericEqualityComparer_1_t1903981024 * __this, bool ___x0, bool ___y1, const RuntimeMethod* method)
{
	{
		goto IL_0015;
	}
	{
		bool L_1 = ___y1;
		bool L_2 = L_1;
		RuntimeObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((RuntimeObject*)(RuntimeObject *)L_3) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		bool L_4 = ___y1;
		bool L_5 = Boolean_Equals_m67531367((bool*)(&___x0), (bool)L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Void System.Collections.Generic.GenericEqualityComparer`1<System.DateTime>::.ctor()
extern "C"  void GenericEqualityComparer_1__ctor_m3817170780_gshared (GenericEqualityComparer_1_t352106478 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((EqualityComparer_1_t1040419072 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t1040419072 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t1040419072 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.DateTime>::GetHashCode(T)
extern "C"  int32_t GenericEqualityComparer_1_GetHashCode_m3113379158_gshared (GenericEqualityComparer_1_t352106478 * __this, DateTime_t507798353  ___obj0, const RuntimeMethod* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t L_1 = DateTime_GetHashCode_m1398597643((DateTime_t507798353 *)(&___obj0), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<System.DateTime>::Equals(T,T)
extern "C"  bool GenericEqualityComparer_1_Equals_m3565912952_gshared (GenericEqualityComparer_1_t352106478 * __this, DateTime_t507798353  ___x0, DateTime_t507798353  ___y1, const RuntimeMethod* method)
{
	{
		goto IL_0015;
	}
	{
		DateTime_t507798353  L_1 = ___y1;
		DateTime_t507798353  L_2 = L_1;
		RuntimeObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((RuntimeObject*)(RuntimeObject *)L_3) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		DateTime_t507798353  L_4 = ___y1;
		bool L_5 = DateTime_Equals_m2586536838((DateTime_t507798353 *)(&___x0), (DateTime_t507798353 )L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Void System.Collections.Generic.GenericEqualityComparer`1<System.DateTimeOffset>::.ctor()
extern "C"  void GenericEqualityComparer_1__ctor_m1436146600_gshared (GenericEqualityComparer_1_t2065072054 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((EqualityComparer_1_t2753384648 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t2753384648 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t2753384648 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.DateTimeOffset>::GetHashCode(T)
extern "C"  int32_t GenericEqualityComparer_1_GetHashCode_m111943387_gshared (GenericEqualityComparer_1_t2065072054 * __this, DateTimeOffset_t2220763929  ___obj0, const RuntimeMethod* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t L_1 = DateTimeOffset_GetHashCode_m3032241378((DateTimeOffset_t2220763929 *)(&___obj0), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<System.DateTimeOffset>::Equals(T,T)
extern "C"  bool GenericEqualityComparer_1_Equals_m2662961069_gshared (GenericEqualityComparer_1_t2065072054 * __this, DateTimeOffset_t2220763929  ___x0, DateTimeOffset_t2220763929  ___y1, const RuntimeMethod* method)
{
	{
		goto IL_0015;
	}
	{
		DateTimeOffset_t2220763929  L_1 = ___y1;
		DateTimeOffset_t2220763929  L_2 = L_1;
		RuntimeObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((RuntimeObject*)(RuntimeObject *)L_3) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		DateTimeOffset_t2220763929  L_4 = ___y1;
		bool L_5 = DateTimeOffset_Equals_m4119379754((DateTimeOffset_t2220763929 *)(&___x0), (DateTimeOffset_t2220763929 )L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Void System.Collections.Generic.GenericEqualityComparer`1<System.Guid>::.ctor()
extern "C"  void GenericEqualityComparer_1__ctor_m1501211954_gshared (GenericEqualityComparer_1_t4105939155 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((EqualityComparer_1_t499284453 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t499284453 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t499284453 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.Guid>::GetHashCode(T)
extern "C"  int32_t GenericEqualityComparer_1_GetHashCode_m3872248138_gshared (GenericEqualityComparer_1_t4105939155 * __this, Guid_t  ___obj0, const RuntimeMethod* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t L_1 = Guid_GetHashCode_m3315775205((Guid_t *)(&___obj0), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<System.Guid>::Equals(T,T)
extern "C"  bool GenericEqualityComparer_1_Equals_m3314938700_gshared (GenericEqualityComparer_1_t4105939155 * __this, Guid_t  ___x0, Guid_t  ___y1, const RuntimeMethod* method)
{
	{
		goto IL_0015;
	}
	{
		Guid_t  L_1 = ___y1;
		Guid_t  L_2 = L_1;
		RuntimeObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((RuntimeObject*)(RuntimeObject *)L_3) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		Guid_t  L_4 = ___y1;
		bool L_5 = Guid_Equals_m3913097454((Guid_t *)(&___x0), (Guid_t )L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Void System.Collections.Generic.GenericEqualityComparer`1<System.Int32>::.ctor()
extern "C"  void GenericEqualityComparer_1__ctor_m1118245903_gshared (GenericEqualityComparer_1_t2415443324 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((EqualityComparer_1_t3103755918 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t3103755918 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t3103755918 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.Int32>::GetHashCode(T)
extern "C"  int32_t GenericEqualityComparer_1_GetHashCode_m1057208485_gshared (GenericEqualityComparer_1_t2415443324 * __this, int32_t ___obj0, const RuntimeMethod* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t L_1 = Int32_GetHashCode_m339244912((int32_t*)(&___obj0), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<System.Int32>::Equals(T,T)
extern "C"  bool GenericEqualityComparer_1_Equals_m1674825203_gshared (GenericEqualityComparer_1_t2415443324 * __this, int32_t ___x0, int32_t ___y1, const RuntimeMethod* method)
{
	{
		goto IL_0015;
	}
	{
		int32_t L_1 = ___y1;
		int32_t L_2 = L_1;
		RuntimeObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((RuntimeObject*)(RuntimeObject *)L_3) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		int32_t L_4 = ___y1;
		bool L_5 = Int32_Equals_m2128805555((int32_t*)(&___x0), (int32_t)L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Void System.Collections.Generic.GenericEqualityComparer`1<System.Object>::.ctor()
extern "C"  void GenericEqualityComparer_1__ctor_m3432859031_gshared (GenericEqualityComparer_1_t3140325128 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((EqualityComparer_1_t3828637722 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t3828637722 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t3828637722 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.Object>::GetHashCode(T)
extern "C"  int32_t GenericEqualityComparer_1_GetHashCode_m406234944_gshared (GenericEqualityComparer_1_t3140325128 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___obj0;
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		NullCheck((RuntimeObject *)(*(&___obj0)));
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (RuntimeObject *)(*(&___obj0)));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<System.Object>::Equals(T,T)
extern "C"  bool GenericEqualityComparer_1_Equals_m2363629631_gshared (GenericEqualityComparer_1_t3140325128 * __this, RuntimeObject * ___x0, RuntimeObject * ___y1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___x0;
		if (L_0)
		{
			goto IL_0015;
		}
	}
	{
		RuntimeObject * L_1 = ___y1;
		return (bool)((((RuntimeObject*)(RuntimeObject *)L_1) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		RuntimeObject * L_2 = ___y1;
		NullCheck((RuntimeObject*)(*(&___x0)));
		bool L_3 = InterfaceFuncInvoker1< bool, RuntimeObject * >::Invoke(0 /* System.Boolean System.IEquatable`1<System.Object>::Equals(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (RuntimeObject*)(*(&___x0)), (RuntimeObject *)L_2);
		return L_3;
	}
}
// System.Void System.Collections.Generic.GenericEqualityComparer`1<System.TimeSpan>::.ctor()
extern "C"  void GenericEqualityComparer_1__ctor_m2777857381_gshared (GenericEqualityComparer_1_t2830983348 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((EqualityComparer_1_t3519295942 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t3519295942 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t3519295942 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.TimeSpan>::GetHashCode(T)
extern "C"  int32_t GenericEqualityComparer_1_GetHashCode_m2641037285_gshared (GenericEqualityComparer_1_t2830983348 * __this, TimeSpan_t2986675223  ___obj0, const RuntimeMethod* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t L_1 = TimeSpan_GetHashCode_m3640128864((TimeSpan_t2986675223 *)(&___obj0), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<System.TimeSpan>::Equals(T,T)
extern "C"  bool GenericEqualityComparer_1_Equals_m2671234832_gshared (GenericEqualityComparer_1_t2830983348 * __this, TimeSpan_t2986675223  ___x0, TimeSpan_t2986675223  ___y1, const RuntimeMethod* method)
{
	{
		goto IL_0015;
	}
	{
		TimeSpan_t2986675223  L_1 = ___y1;
		TimeSpan_t2986675223  L_2 = L_1;
		RuntimeObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((RuntimeObject*)(RuntimeObject *)L_3) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		TimeSpan_t2986675223  L_4 = ___y1;
		bool L_5 = TimeSpan_Equals_m1555084785((TimeSpan_t2986675223 *)(&___x0), (TimeSpan_t2986675223 )L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
