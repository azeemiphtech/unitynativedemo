﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"




extern const Il2CppType RuntimeObject_0_0_0;
extern const Il2CppType Int32_t2571135199_0_0_0;
extern const Il2CppType Char_t3572527572_0_0_0;
extern const Il2CppType Int64_t384086013_0_0_0;
extern const Il2CppType UInt32_t2739056616_0_0_0;
extern const Il2CppType UInt64_t2265270229_0_0_0;
extern const Il2CppType Byte_t880488320_0_0_0;
extern const Il2CppType SByte_t2530277047_0_0_0;
extern const Il2CppType Int16_t769030278_0_0_0;
extern const Il2CppType UInt16_t11709673_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType IConvertible_t1224795492_0_0_0;
extern const Il2CppType IComparable_t63485110_0_0_0;
extern const Il2CppType IEnumerable_t1872631827_0_0_0;
extern const Il2CppType ICloneable_t3230708396_0_0_0;
extern const Il2CppType IComparable_1_t2260689833_0_0_0;
extern const Il2CppType IEquatable_1_t1012708249_0_0_0;
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType IReflect_t4184883566_0_0_0;
extern const Il2CppType _Type_t2913350559_0_0_0;
extern const Il2CppType MemberInfo_t_0_0_0;
extern const Il2CppType ICustomAttributeProvider_t4054400477_0_0_0;
extern const Il2CppType _MemberInfo_t1659175437_0_0_0;
extern const Il2CppType Single_t496865882_0_0_0;
extern const Il2CppType Double_t1454265465_0_0_0;
extern const Il2CppType Decimal_t2656901032_0_0_0;
extern const Il2CppType Boolean_t2059672899_0_0_0;
extern const Il2CppType Delegate_t96267039_0_0_0;
extern const Il2CppType ISerializable_t631501906_0_0_0;
extern const Il2CppType ParameterInfo_t3893964560_0_0_0;
extern const Il2CppType _ParameterInfo_t3152035282_0_0_0;
extern const Il2CppType FieldInfo_t_0_0_0;
extern const Il2CppType _FieldInfo_t3281021298_0_0_0;
extern const Il2CppType ParameterModifier_t969812002_0_0_0;
extern const Il2CppType MethodInfo_t_0_0_0;
extern const Il2CppType _MethodInfo_t643827776_0_0_0;
extern const Il2CppType MethodBase_t674153939_0_0_0;
extern const Il2CppType _MethodBase_t3352748864_0_0_0;
extern const Il2CppType ConstructorInfo_t3360520918_0_0_0;
extern const Il2CppType _ConstructorInfo_t1952808443_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
extern const Il2CppType TableRange_t4125455382_0_0_0;
extern const Il2CppType TailoringInfo_t3354707910_0_0_0;
extern const Il2CppType KeyValuePair_2_t2877556189_0_0_0;
extern const Il2CppType Link_t3525875035_0_0_0;
extern const Il2CppType DictionaryEntry_t1385909157_0_0_0;
extern const Il2CppType KeyValuePair_2_t3404415448_0_0_0;
extern const Il2CppType Contraction_t3257534955_0_0_0;
extern const Il2CppType Level2Map_t3905868985_0_0_0;
extern const Il2CppType BigInteger_t1715200596_0_0_0;
extern const Il2CppType KeySizes_t4273035850_0_0_0;
extern const Il2CppType KeyValuePair_2_t3602437993_0_0_0;
extern const Il2CppType Slot_t2102621701_0_0_0;
extern const Il2CppType Slot_t1141617207_0_0_0;
extern const Il2CppType StackFrame_t4156368350_0_0_0;
extern const Il2CppType Calendar_t585695848_0_0_0;
extern const Il2CppType ModuleBuilder_t671559400_0_0_0;
extern const Il2CppType _ModuleBuilder_t3149677118_0_0_0;
extern const Il2CppType Module_t2594760207_0_0_0;
extern const Il2CppType _Module_t494242506_0_0_0;
extern const Il2CppType ParameterBuilder_t2478802094_0_0_0;
extern const Il2CppType _ParameterBuilder_t3730072709_0_0_0;
extern const Il2CppType TypeU5BU5D_t1830665827_0_0_0;
extern const Il2CppType RuntimeArray_0_0_0;
extern const Il2CppType ICollection_t1881172908_0_0_0;
extern const Il2CppType IList_t2514494774_0_0_0;
extern const Il2CppType IList_1_t4288475132_0_0_0;
extern const Il2CppType ICollection_1_t3013691889_0_0_0;
extern const Il2CppType IEnumerable_1_t390880001_0_0_0;
extern const Il2CppType IList_1_t1904346532_0_0_0;
extern const Il2CppType ICollection_1_t629563289_0_0_0;
extern const Il2CppType IEnumerable_1_t2301718697_0_0_0;
extern const Il2CppType IList_1_t632813525_0_0_0;
extern const Il2CppType ICollection_1_t3652997578_0_0_0;
extern const Il2CppType IEnumerable_1_t1030185690_0_0_0;
extern const Il2CppType IList_1_t2432397382_0_0_0;
extern const Il2CppType ICollection_1_t1157614139_0_0_0;
extern const Il2CppType IEnumerable_1_t2829769547_0_0_0;
extern const Il2CppType IList_1_t1773863443_0_0_0;
extern const Il2CppType ICollection_1_t499080200_0_0_0;
extern const Il2CppType IEnumerable_1_t2171235608_0_0_0;
extern const Il2CppType IList_1_t3673605699_0_0_0;
extern const Il2CppType ICollection_1_t2398822456_0_0_0;
extern const Il2CppType IEnumerable_1_t4070977864_0_0_0;
extern const Il2CppType IList_1_t1015479969_0_0_0;
extern const Il2CppType ICollection_1_t4035664022_0_0_0;
extern const Il2CppType IEnumerable_1_t1412852134_0_0_0;
extern const Il2CppType ILTokenInfo_t281295013_0_0_0;
extern const Il2CppType LabelData_t1059919410_0_0_0;
extern const Il2CppType LabelFixup_t2685311059_0_0_0;
extern const Il2CppType GenericTypeParameterBuilder_t193954652_0_0_0;
extern const Il2CppType CustomAttributeBuilder_t1432176696_0_0_0;
extern const Il2CppType _CustomAttributeBuilder_t2651440002_0_0_0;
extern const Il2CppType RefEmitPermissionSet_t945207422_0_0_0;
extern const Il2CppType TypeBuilder_t1663068587_0_0_0;
extern const Il2CppType _TypeBuilder_t4136446867_0_0_0;
extern const Il2CppType MethodBuilder_t2964638190_0_0_0;
extern const Il2CppType _MethodBuilder_t195532601_0_0_0;
extern const Il2CppType FieldBuilder_t803025013_0_0_0;
extern const Il2CppType _FieldBuilder_t3486190427_0_0_0;
extern const Il2CppType MonoResource_t2965441204_0_0_0;
extern const Il2CppType ConstructorBuilder_t1596923965_0_0_0;
extern const Il2CppType _ConstructorBuilder_t3355697174_0_0_0;
extern const Il2CppType PropertyBuilder_t1258013571_0_0_0;
extern const Il2CppType _PropertyBuilder_t2876596643_0_0_0;
extern const Il2CppType PropertyInfo_t_0_0_0;
extern const Il2CppType _PropertyInfo_t2517621214_0_0_0;
extern const Il2CppType EventBuilder_t2300762438_0_0_0;
extern const Il2CppType _EventBuilder_t3169829690_0_0_0;
extern const Il2CppType CustomAttributeTypedArgument_t2667702721_0_0_0;
extern const Il2CppType CustomAttributeNamedArgument_t425730339_0_0_0;
extern const Il2CppType CustomAttributeData_t3408840380_0_0_0;
extern const Il2CppType ResourceInfo_t1773833009_0_0_0;
extern const Il2CppType ResourceCacheItem_t3632973466_0_0_0;
extern const Il2CppType IContextProperty_t2748911791_0_0_0;
extern const Il2CppType Header_t2445250629_0_0_0;
extern const Il2CppType ITrackingHandler_t2297406515_0_0_0;
extern const Il2CppType IContextAttribute_t632650489_0_0_0;
extern const Il2CppType DateTime_t507798353_0_0_0;
extern const Il2CppType TimeSpan_t2986675223_0_0_0;
extern const Il2CppType TypeTag_t1137063594_0_0_0;
extern const Il2CppType MonoType_t_0_0_0;
extern const Il2CppType StrongName_t848839738_0_0_0;
extern const Il2CppType IBuiltInEvidence_t1834666417_0_0_0;
extern const Il2CppType IIdentityPermissionFactory_t233160095_0_0_0;
extern const Il2CppType DateTimeOffset_t2220763929_0_0_0;
extern const Il2CppType Guid_t_0_0_0;
extern const Il2CppType Version_t900644674_0_0_0;
extern const Il2CppType KeyValuePair_2_t2366093889_0_0_0;
extern const Il2CppType KeyValuePair_2_t2892953148_0_0_0;
extern const Il2CppType X509Certificate_t3231070314_0_0_0;
extern const Il2CppType IDeserializationCallback_t1559034278_0_0_0;
extern const Il2CppType X509ChainStatus_t177750891_0_0_0;
extern const Il2CppType Capture_t1939465921_0_0_0;
extern const Il2CppType Group_t61132085_0_0_0;
extern const Il2CppType Mark_t4050795874_0_0_0;
extern const Il2CppType UriScheme_t2622429923_0_0_0;
extern const Il2CppType BigInteger_t1715200597_0_0_0;
extern const Il2CppType ByteU5BU5D_t1014014593_0_0_0;
extern const Il2CppType IList_1_t2894918582_0_0_0;
extern const Il2CppType ICollection_1_t1620135339_0_0_0;
extern const Il2CppType IEnumerable_1_t3292290747_0_0_0;
extern const Il2CppType ClientCertificateType_t194976790_0_0_0;
extern const Il2CppType AsyncOperation_t4233810744_0_0_0;
extern const Il2CppType Camera_t101201881_0_0_0;
extern const Il2CppType Behaviour_t4110946901_0_0_0;
extern const Il2CppType Component_t2160255836_0_0_0;
extern const Il2CppType Object_t2461733472_0_0_0;
extern const Il2CppType Display_t4002355288_0_0_0;
extern const Il2CppType Keyframe_t3396755990_0_0_0;
extern const Il2CppType Playable_t1760619938_0_0_0;
extern const Il2CppType PlayableOutput_t3840687608_0_0_0;
extern const Il2CppType Scene_t3023997566_0_0_0;
extern const Il2CppType LoadSceneMode_t524132339_0_0_0;
extern const Il2CppType SpriteAtlas_t1477815998_0_0_0;
extern const Il2CppType DisallowMultipleComponent_t2957315911_0_0_0;
extern const Il2CppType Attribute_t1659210826_0_0_0;
extern const Il2CppType _Attribute_t480711065_0_0_0;
extern const Il2CppType ExecuteInEditMode_t1836144016_0_0_0;
extern const Il2CppType RequireComponent_t597737523_0_0_0;
extern const Il2CppType HitInfo_t184839221_0_0_0;
extern const Il2CppType PersistentCall_t2687057912_0_0_0;
extern const Il2CppType BaseInvokableCall_t2920707967_0_0_0;
extern const Il2CppType WorkRequest_t2547429811_0_0_0;
extern const Il2CppType PlayableBinding_t136667248_0_0_0;
extern const Il2CppType MessageTypeSubscribers_t1657992506_0_0_0;
extern const Il2CppType MessageEventArgs_t947408028_0_0_0;
extern const Il2CppType WeakReference_t3998924468_0_0_0;
extern const Il2CppType KeyValuePair_2_t2204764377_0_0_0;
extern const Il2CppType KeyValuePair_2_t2907671842_0_0_0;
extern const Il2CppType AudioSpatializerExtensionDefinition_t4023306521_0_0_0;
extern const Il2CppType AudioAmbisonicExtensionDefinition_t139142167_0_0_0;
extern const Il2CppType AudioSourceExtension_t3778551013_0_0_0;
extern const Il2CppType ScriptableObject_t476655885_0_0_0;
extern const Il2CppType AudioMixerPlayable_t1017334687_0_0_0;
extern const Il2CppType AudioClipPlayable_t3812371941_0_0_0;
extern const Il2CppType AchievementDescription_t130800866_0_0_0;
extern const Il2CppType IAchievementDescription_t924957763_0_0_0;
extern const Il2CppType UserProfile_t3211800844_0_0_0;
extern const Il2CppType IUserProfile_t3226494754_0_0_0;
extern const Il2CppType GcLeaderboard_t617530396_0_0_0;
extern const Il2CppType IAchievementDescriptionU5BU5D_t2959897426_0_0_0;
extern const Il2CppType IAchievementU5BU5D_t1231928882_0_0_0;
extern const Il2CppType IAchievement_t3311330019_0_0_0;
extern const Il2CppType GcAchievementData_t3167706508_0_0_0;
extern const Il2CppType Achievement_t2517894267_0_0_0;
extern const Il2CppType IScoreU5BU5D_t624878879_0_0_0;
extern const Il2CppType IScore_t1996314298_0_0_0;
extern const Il2CppType GcScoreData_t3955401213_0_0_0;
extern const Il2CppType Score_t1135675058_0_0_0;
extern const Il2CppType IUserProfileU5BU5D_t3577724439_0_0_0;
extern const Il2CppType KeyValuePair_2_t4129297252_0_0_0;
extern const Il2CppType FieldWithTarget_t1883299082_0_0_0;
extern const Il2CppType IEnumerable_1_t2300816294_gp_0_0_0_0;
extern const Il2CppType Array_InternalArray__IEnumerable_GetEnumerator_m2280917461_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m2667677402_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m724484260_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m724484260_gp_1_0_0_0;
extern const Il2CppType Array_Sort_m3090697542_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m3597357548_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m3597357548_gp_1_0_0_0;
extern const Il2CppType Array_Sort_m1886451243_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m1562643584_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m1562643584_gp_1_0_0_0;
extern const Il2CppType Array_Sort_m2579296346_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m3476310768_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m3476310768_gp_1_0_0_0;
extern const Il2CppType Array_Sort_m3187056504_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m1246393659_gp_0_0_0_0;
extern const Il2CppType Array_qsort_m1307391834_gp_0_0_0_0;
extern const Il2CppType Array_qsort_m1307391834_gp_1_0_0_0;
extern const Il2CppType Array_compare_m3469659333_gp_0_0_0_0;
extern const Il2CppType Array_qsort_m172816524_gp_0_0_0_0;
extern const Il2CppType Array_Resize_m3981285257_gp_0_0_0_0;
extern const Il2CppType Array_TrueForAll_m1661083019_gp_0_0_0_0;
extern const Il2CppType Array_ForEach_m753603380_gp_0_0_0_0;
extern const Il2CppType Array_ConvertAll_m2612536962_gp_0_0_0_0;
extern const Il2CppType Array_ConvertAll_m2612536962_gp_1_0_0_0;
extern const Il2CppType Array_FindLastIndex_m1512572791_gp_0_0_0_0;
extern const Il2CppType Array_FindLastIndex_m1228596251_gp_0_0_0_0;
extern const Il2CppType Array_FindLastIndex_m3711907002_gp_0_0_0_0;
extern const Il2CppType Array_FindIndex_m2893558283_gp_0_0_0_0;
extern const Il2CppType Array_FindIndex_m3648911080_gp_0_0_0_0;
extern const Il2CppType Array_FindIndex_m3595353763_gp_0_0_0_0;
extern const Il2CppType Array_BinarySearch_m4248949575_gp_0_0_0_0;
extern const Il2CppType Array_BinarySearch_m1974805269_gp_0_0_0_0;
extern const Il2CppType Array_BinarySearch_m3819272786_gp_0_0_0_0;
extern const Il2CppType Array_BinarySearch_m1130814323_gp_0_0_0_0;
extern const Il2CppType Array_IndexOf_m2441478810_gp_0_0_0_0;
extern const Il2CppType Array_IndexOf_m4207036999_gp_0_0_0_0;
extern const Il2CppType Array_IndexOf_m3455639349_gp_0_0_0_0;
extern const Il2CppType Array_LastIndexOf_m2181488816_gp_0_0_0_0;
extern const Il2CppType Array_LastIndexOf_m4262341566_gp_0_0_0_0;
extern const Il2CppType Array_LastIndexOf_m395810534_gp_0_0_0_0;
extern const Il2CppType Array_FindAll_m1792123350_gp_0_0_0_0;
extern const Il2CppType Array_Exists_m4188561929_gp_0_0_0_0;
extern const Il2CppType Array_AsReadOnly_m547237041_gp_0_0_0_0;
extern const Il2CppType Array_Find_m4070178886_gp_0_0_0_0;
extern const Il2CppType Array_FindLast_m3836785593_gp_0_0_0_0;
extern const Il2CppType InternalEnumerator_1_t2351989714_gp_0_0_0_0;
extern const Il2CppType ArrayReadOnlyList_1_t3002341623_gp_0_0_0_0;
extern const Il2CppType U3CGetEnumeratorU3Ec__Iterator0_t4287845664_gp_0_0_0_0;
extern const Il2CppType IList_1_t2409468953_gp_0_0_0_0;
extern const Il2CppType ICollection_1_t2720146454_gp_0_0_0_0;
extern const Il2CppType Nullable_1_t2413029061_gp_0_0_0_0;
extern const Il2CppType Comparer_1_t1619863495_gp_0_0_0_0;
extern const Il2CppType DefaultComparer_t3604220391_gp_0_0_0_0;
extern const Il2CppType GenericComparer_1_t769309037_gp_0_0_0_0;
extern const Il2CppType Dictionary_2_t1922834018_gp_0_0_0_0;
extern const Il2CppType Dictionary_2_t1922834018_gp_1_0_0_0;
extern const Il2CppType KeyValuePair_2_t2834118370_0_0_0;
extern const Il2CppType Dictionary_2_Do_CopyTo_m1120866649_gp_0_0_0_0;
extern const Il2CppType Dictionary_2_Do_ICollectionCopyTo_m3964247330_gp_0_0_0_0;
extern const Il2CppType ShimEnumerator_t2216024862_gp_0_0_0_0;
extern const Il2CppType ShimEnumerator_t2216024862_gp_1_0_0_0;
extern const Il2CppType Enumerator_t601853994_gp_0_0_0_0;
extern const Il2CppType Enumerator_t601853994_gp_1_0_0_0;
extern const Il2CppType KeyValuePair_2_t2855505602_0_0_0;
extern const Il2CppType EqualityComparer_1_t2656680762_gp_0_0_0_0;
extern const Il2CppType DefaultComparer_t866827805_gp_0_0_0_0;
extern const Il2CppType GenericEqualityComparer_1_t2573898200_gp_0_0_0_0;
extern const Il2CppType KeyValuePair_2_t4172885178_0_0_0;
extern const Il2CppType IDictionary_2_t3413473948_gp_0_0_0_0;
extern const Il2CppType IDictionary_2_t3413473948_gp_1_0_0_0;
extern const Il2CppType KeyValuePair_2_t776898938_gp_0_0_0_0;
extern const Il2CppType KeyValuePair_2_t776898938_gp_1_0_0_0;
extern const Il2CppType List_1_t82844879_gp_0_0_0_0;
extern const Il2CppType Enumerator_t2210905544_gp_0_0_0_0;
extern const Il2CppType Collection_1_t3543120677_gp_0_0_0_0;
extern const Il2CppType ReadOnlyCollection_1_t891526187_gp_0_0_0_0;
extern const Il2CppType MonoProperty_GetterAdapterFrame_m880231002_gp_0_0_0_0;
extern const Il2CppType MonoProperty_GetterAdapterFrame_m880231002_gp_1_0_0_0;
extern const Il2CppType MonoProperty_StaticGetterAdapterFrame_m944017034_gp_0_0_0_0;
extern const Il2CppType Queue_1_t542587364_gp_0_0_0_0;
extern const Il2CppType Enumerator_t3782511927_gp_0_0_0_0;
extern const Il2CppType Stack_1_t2711020695_gp_0_0_0_0;
extern const Il2CppType Enumerator_t548431029_gp_0_0_0_0;
extern const Il2CppType Enumerable_Any_m1770862959_gp_0_0_0_0;
extern const Il2CppType Enumerable_Where_m2049181036_gp_0_0_0_0;
extern const Il2CppType Enumerable_CreateWhereIterator_m1295165955_gp_0_0_0_0;
extern const Il2CppType U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1910290221_gp_0_0_0_0;
extern const Il2CppType InvokableCall_1_t1604090555_gp_0_0_0_0;
extern const Il2CppType UnityAction_1_t3885734668_0_0_0;
extern const Il2CppType InvokableCall_2_t3976188983_gp_0_0_0_0;
extern const Il2CppType InvokableCall_2_t3976188983_gp_1_0_0_0;
extern const Il2CppType InvokableCall_3_t3917817895_gp_0_0_0_0;
extern const Il2CppType InvokableCall_3_t3917817895_gp_1_0_0_0;
extern const Il2CppType InvokableCall_3_t3917817895_gp_2_0_0_0;
extern const Il2CppType InvokableCall_4_t2387264047_gp_0_0_0_0;
extern const Il2CppType InvokableCall_4_t2387264047_gp_1_0_0_0;
extern const Il2CppType InvokableCall_4_t2387264047_gp_2_0_0_0;
extern const Il2CppType InvokableCall_4_t2387264047_gp_3_0_0_0;
extern const Il2CppType CachedInvokableCall_1_t4137603102_gp_0_0_0_0;
extern const Il2CppType UnityEvent_1_t4023850743_gp_0_0_0_0;
extern const Il2CppType UnityEvent_2_t1533051435_gp_0_0_0_0;
extern const Il2CppType UnityEvent_2_t1533051435_gp_1_0_0_0;
extern const Il2CppType UnityEvent_3_t4091795572_gp_0_0_0_0;
extern const Il2CppType UnityEvent_3_t4091795572_gp_1_0_0_0;
extern const Il2CppType UnityEvent_3_t4091795572_gp_2_0_0_0;
extern const Il2CppType UnityEvent_4_t1002767761_gp_0_0_0_0;
extern const Il2CppType UnityEvent_4_t1002767761_gp_1_0_0_0;
extern const Il2CppType UnityEvent_4_t1002767761_gp_2_0_0_0;
extern const Il2CppType UnityEvent_4_t1002767761_gp_3_0_0_0;
extern const Il2CppType DefaultExecutionOrder_t3862449190_0_0_0;
extern const Il2CppType PlayerConnection_t3559457672_0_0_0;
extern const Il2CppType GUILayer_t1929752731_0_0_0;




static const RuntimeType* GenInst_RuntimeObject_0_0_0_Types[] = { (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0 = { 1, GenInst_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2571135199_0_0_0_Types[] = { (&Int32_t2571135199_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2571135199_0_0_0 = { 1, GenInst_Int32_t2571135199_0_0_0_Types };
static const RuntimeType* GenInst_Char_t3572527572_0_0_0_Types[] = { (&Char_t3572527572_0_0_0) };
extern const Il2CppGenericInst GenInst_Char_t3572527572_0_0_0 = { 1, GenInst_Char_t3572527572_0_0_0_Types };
static const RuntimeType* GenInst_Int64_t384086013_0_0_0_Types[] = { (&Int64_t384086013_0_0_0) };
extern const Il2CppGenericInst GenInst_Int64_t384086013_0_0_0 = { 1, GenInst_Int64_t384086013_0_0_0_Types };
static const RuntimeType* GenInst_UInt32_t2739056616_0_0_0_Types[] = { (&UInt32_t2739056616_0_0_0) };
extern const Il2CppGenericInst GenInst_UInt32_t2739056616_0_0_0 = { 1, GenInst_UInt32_t2739056616_0_0_0_Types };
static const RuntimeType* GenInst_UInt64_t2265270229_0_0_0_Types[] = { (&UInt64_t2265270229_0_0_0) };
extern const Il2CppGenericInst GenInst_UInt64_t2265270229_0_0_0 = { 1, GenInst_UInt64_t2265270229_0_0_0_Types };
static const RuntimeType* GenInst_Byte_t880488320_0_0_0_Types[] = { (&Byte_t880488320_0_0_0) };
extern const Il2CppGenericInst GenInst_Byte_t880488320_0_0_0 = { 1, GenInst_Byte_t880488320_0_0_0_Types };
static const RuntimeType* GenInst_SByte_t2530277047_0_0_0_Types[] = { (&SByte_t2530277047_0_0_0) };
extern const Il2CppGenericInst GenInst_SByte_t2530277047_0_0_0 = { 1, GenInst_SByte_t2530277047_0_0_0_Types };
static const RuntimeType* GenInst_Int16_t769030278_0_0_0_Types[] = { (&Int16_t769030278_0_0_0) };
extern const Il2CppGenericInst GenInst_Int16_t769030278_0_0_0 = { 1, GenInst_Int16_t769030278_0_0_0_Types };
static const RuntimeType* GenInst_UInt16_t11709673_0_0_0_Types[] = { (&UInt16_t11709673_0_0_0) };
extern const Il2CppGenericInst GenInst_UInt16_t11709673_0_0_0 = { 1, GenInst_UInt16_t11709673_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_Types[] = { (&String_t_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0 = { 1, GenInst_String_t_0_0_0_Types };
static const RuntimeType* GenInst_IConvertible_t1224795492_0_0_0_Types[] = { (&IConvertible_t1224795492_0_0_0) };
extern const Il2CppGenericInst GenInst_IConvertible_t1224795492_0_0_0 = { 1, GenInst_IConvertible_t1224795492_0_0_0_Types };
static const RuntimeType* GenInst_IComparable_t63485110_0_0_0_Types[] = { (&IComparable_t63485110_0_0_0) };
extern const Il2CppGenericInst GenInst_IComparable_t63485110_0_0_0 = { 1, GenInst_IComparable_t63485110_0_0_0_Types };
static const RuntimeType* GenInst_IEnumerable_t1872631827_0_0_0_Types[] = { (&IEnumerable_t1872631827_0_0_0) };
extern const Il2CppGenericInst GenInst_IEnumerable_t1872631827_0_0_0 = { 1, GenInst_IEnumerable_t1872631827_0_0_0_Types };
static const RuntimeType* GenInst_ICloneable_t3230708396_0_0_0_Types[] = { (&ICloneable_t3230708396_0_0_0) };
extern const Il2CppGenericInst GenInst_ICloneable_t3230708396_0_0_0 = { 1, GenInst_ICloneable_t3230708396_0_0_0_Types };
static const RuntimeType* GenInst_IComparable_1_t2260689833_0_0_0_Types[] = { (&IComparable_1_t2260689833_0_0_0) };
extern const Il2CppGenericInst GenInst_IComparable_1_t2260689833_0_0_0 = { 1, GenInst_IComparable_1_t2260689833_0_0_0_Types };
static const RuntimeType* GenInst_IEquatable_1_t1012708249_0_0_0_Types[] = { (&IEquatable_1_t1012708249_0_0_0) };
extern const Il2CppGenericInst GenInst_IEquatable_1_t1012708249_0_0_0 = { 1, GenInst_IEquatable_1_t1012708249_0_0_0_Types };
static const RuntimeType* GenInst_Type_t_0_0_0_Types[] = { (&Type_t_0_0_0) };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0 = { 1, GenInst_Type_t_0_0_0_Types };
static const RuntimeType* GenInst_IReflect_t4184883566_0_0_0_Types[] = { (&IReflect_t4184883566_0_0_0) };
extern const Il2CppGenericInst GenInst_IReflect_t4184883566_0_0_0 = { 1, GenInst_IReflect_t4184883566_0_0_0_Types };
static const RuntimeType* GenInst__Type_t2913350559_0_0_0_Types[] = { (&_Type_t2913350559_0_0_0) };
extern const Il2CppGenericInst GenInst__Type_t2913350559_0_0_0 = { 1, GenInst__Type_t2913350559_0_0_0_Types };
static const RuntimeType* GenInst_MemberInfo_t_0_0_0_Types[] = { (&MemberInfo_t_0_0_0) };
extern const Il2CppGenericInst GenInst_MemberInfo_t_0_0_0 = { 1, GenInst_MemberInfo_t_0_0_0_Types };
static const RuntimeType* GenInst_ICustomAttributeProvider_t4054400477_0_0_0_Types[] = { (&ICustomAttributeProvider_t4054400477_0_0_0) };
extern const Il2CppGenericInst GenInst_ICustomAttributeProvider_t4054400477_0_0_0 = { 1, GenInst_ICustomAttributeProvider_t4054400477_0_0_0_Types };
static const RuntimeType* GenInst__MemberInfo_t1659175437_0_0_0_Types[] = { (&_MemberInfo_t1659175437_0_0_0) };
extern const Il2CppGenericInst GenInst__MemberInfo_t1659175437_0_0_0 = { 1, GenInst__MemberInfo_t1659175437_0_0_0_Types };
static const RuntimeType* GenInst_Single_t496865882_0_0_0_Types[] = { (&Single_t496865882_0_0_0) };
extern const Il2CppGenericInst GenInst_Single_t496865882_0_0_0 = { 1, GenInst_Single_t496865882_0_0_0_Types };
static const RuntimeType* GenInst_Double_t1454265465_0_0_0_Types[] = { (&Double_t1454265465_0_0_0) };
extern const Il2CppGenericInst GenInst_Double_t1454265465_0_0_0 = { 1, GenInst_Double_t1454265465_0_0_0_Types };
static const RuntimeType* GenInst_Decimal_t2656901032_0_0_0_Types[] = { (&Decimal_t2656901032_0_0_0) };
extern const Il2CppGenericInst GenInst_Decimal_t2656901032_0_0_0 = { 1, GenInst_Decimal_t2656901032_0_0_0_Types };
static const RuntimeType* GenInst_Boolean_t2059672899_0_0_0_Types[] = { (&Boolean_t2059672899_0_0_0) };
extern const Il2CppGenericInst GenInst_Boolean_t2059672899_0_0_0 = { 1, GenInst_Boolean_t2059672899_0_0_0_Types };
static const RuntimeType* GenInst_Delegate_t96267039_0_0_0_Types[] = { (&Delegate_t96267039_0_0_0) };
extern const Il2CppGenericInst GenInst_Delegate_t96267039_0_0_0 = { 1, GenInst_Delegate_t96267039_0_0_0_Types };
static const RuntimeType* GenInst_ISerializable_t631501906_0_0_0_Types[] = { (&ISerializable_t631501906_0_0_0) };
extern const Il2CppGenericInst GenInst_ISerializable_t631501906_0_0_0 = { 1, GenInst_ISerializable_t631501906_0_0_0_Types };
static const RuntimeType* GenInst_ParameterInfo_t3893964560_0_0_0_Types[] = { (&ParameterInfo_t3893964560_0_0_0) };
extern const Il2CppGenericInst GenInst_ParameterInfo_t3893964560_0_0_0 = { 1, GenInst_ParameterInfo_t3893964560_0_0_0_Types };
static const RuntimeType* GenInst__ParameterInfo_t3152035282_0_0_0_Types[] = { (&_ParameterInfo_t3152035282_0_0_0) };
extern const Il2CppGenericInst GenInst__ParameterInfo_t3152035282_0_0_0 = { 1, GenInst__ParameterInfo_t3152035282_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_FieldInfo_t_0_0_0_Types[] = { (&FieldInfo_t_0_0_0) };
extern const Il2CppGenericInst GenInst_FieldInfo_t_0_0_0 = { 1, GenInst_FieldInfo_t_0_0_0_Types };
static const RuntimeType* GenInst__FieldInfo_t3281021298_0_0_0_Types[] = { (&_FieldInfo_t3281021298_0_0_0) };
extern const Il2CppGenericInst GenInst__FieldInfo_t3281021298_0_0_0 = { 1, GenInst__FieldInfo_t3281021298_0_0_0_Types };
static const RuntimeType* GenInst_ParameterModifier_t969812002_0_0_0_Types[] = { (&ParameterModifier_t969812002_0_0_0) };
extern const Il2CppGenericInst GenInst_ParameterModifier_t969812002_0_0_0 = { 1, GenInst_ParameterModifier_t969812002_0_0_0_Types };
static const RuntimeType* GenInst_MethodInfo_t_0_0_0_Types[] = { (&MethodInfo_t_0_0_0) };
extern const Il2CppGenericInst GenInst_MethodInfo_t_0_0_0 = { 1, GenInst_MethodInfo_t_0_0_0_Types };
static const RuntimeType* GenInst__MethodInfo_t643827776_0_0_0_Types[] = { (&_MethodInfo_t643827776_0_0_0) };
extern const Il2CppGenericInst GenInst__MethodInfo_t643827776_0_0_0 = { 1, GenInst__MethodInfo_t643827776_0_0_0_Types };
static const RuntimeType* GenInst_MethodBase_t674153939_0_0_0_Types[] = { (&MethodBase_t674153939_0_0_0) };
extern const Il2CppGenericInst GenInst_MethodBase_t674153939_0_0_0 = { 1, GenInst_MethodBase_t674153939_0_0_0_Types };
static const RuntimeType* GenInst__MethodBase_t3352748864_0_0_0_Types[] = { (&_MethodBase_t3352748864_0_0_0) };
extern const Il2CppGenericInst GenInst__MethodBase_t3352748864_0_0_0 = { 1, GenInst__MethodBase_t3352748864_0_0_0_Types };
static const RuntimeType* GenInst_ConstructorInfo_t3360520918_0_0_0_Types[] = { (&ConstructorInfo_t3360520918_0_0_0) };
extern const Il2CppGenericInst GenInst_ConstructorInfo_t3360520918_0_0_0 = { 1, GenInst_ConstructorInfo_t3360520918_0_0_0_Types };
static const RuntimeType* GenInst__ConstructorInfo_t1952808443_0_0_0_Types[] = { (&_ConstructorInfo_t1952808443_0_0_0) };
extern const Il2CppGenericInst GenInst__ConstructorInfo_t1952808443_0_0_0 = { 1, GenInst__ConstructorInfo_t1952808443_0_0_0_Types };
static const RuntimeType* GenInst_IntPtr_t_0_0_0_Types[] = { (&IntPtr_t_0_0_0) };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0 = { 1, GenInst_IntPtr_t_0_0_0_Types };
static const RuntimeType* GenInst_TableRange_t4125455382_0_0_0_Types[] = { (&TableRange_t4125455382_0_0_0) };
extern const Il2CppGenericInst GenInst_TableRange_t4125455382_0_0_0 = { 1, GenInst_TableRange_t4125455382_0_0_0_Types };
static const RuntimeType* GenInst_TailoringInfo_t3354707910_0_0_0_Types[] = { (&TailoringInfo_t3354707910_0_0_0) };
extern const Il2CppGenericInst GenInst_TailoringInfo_t3354707910_0_0_0 = { 1, GenInst_TailoringInfo_t3354707910_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_Int32_t2571135199_0_0_0_Types[] = { (&String_t_0_0_0), (&Int32_t2571135199_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Int32_t2571135199_0_0_0 = { 2, GenInst_String_t_0_0_0_Int32_t2571135199_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_Int32_t2571135199_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&Int32_t2571135199_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_Int32_t2571135199_0_0_0 = { 2, GenInst_RuntimeObject_0_0_0_Int32_t2571135199_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t2877556189_0_0_0_Types[] = { (&KeyValuePair_2_t2877556189_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2877556189_0_0_0 = { 1, GenInst_KeyValuePair_2_t2877556189_0_0_0_Types };
static const RuntimeType* GenInst_Link_t3525875035_0_0_0_Types[] = { (&Link_t3525875035_0_0_0) };
extern const Il2CppGenericInst GenInst_Link_t3525875035_0_0_0 = { 1, GenInst_Link_t3525875035_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_Int32_t2571135199_0_0_0_DictionaryEntry_t1385909157_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&Int32_t2571135199_0_0_0), (&DictionaryEntry_t1385909157_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_Int32_t2571135199_0_0_0_DictionaryEntry_t1385909157_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_Int32_t2571135199_0_0_0_DictionaryEntry_t1385909157_0_0_0_Types };
static const RuntimeType* GenInst_DictionaryEntry_t1385909157_0_0_0_Types[] = { (&DictionaryEntry_t1385909157_0_0_0) };
extern const Il2CppGenericInst GenInst_DictionaryEntry_t1385909157_0_0_0 = { 1, GenInst_DictionaryEntry_t1385909157_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_Int32_t2571135199_0_0_0_KeyValuePair_2_t2877556189_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&Int32_t2571135199_0_0_0), (&KeyValuePair_2_t2877556189_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_Int32_t2571135199_0_0_0_KeyValuePair_2_t2877556189_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_Int32_t2571135199_0_0_0_KeyValuePair_2_t2877556189_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_Int32_t2571135199_0_0_0_DictionaryEntry_t1385909157_0_0_0_Types[] = { (&String_t_0_0_0), (&Int32_t2571135199_0_0_0), (&DictionaryEntry_t1385909157_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Int32_t2571135199_0_0_0_DictionaryEntry_t1385909157_0_0_0 = { 3, GenInst_String_t_0_0_0_Int32_t2571135199_0_0_0_DictionaryEntry_t1385909157_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3404415448_0_0_0_Types[] = { (&KeyValuePair_2_t3404415448_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3404415448_0_0_0 = { 1, GenInst_KeyValuePair_2_t3404415448_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_Int32_t2571135199_0_0_0_KeyValuePair_2_t3404415448_0_0_0_Types[] = { (&String_t_0_0_0), (&Int32_t2571135199_0_0_0), (&KeyValuePair_2_t3404415448_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Int32_t2571135199_0_0_0_KeyValuePair_2_t3404415448_0_0_0 = { 3, GenInst_String_t_0_0_0_Int32_t2571135199_0_0_0_KeyValuePair_2_t3404415448_0_0_0_Types };
static const RuntimeType* GenInst_Contraction_t3257534955_0_0_0_Types[] = { (&Contraction_t3257534955_0_0_0) };
extern const Il2CppGenericInst GenInst_Contraction_t3257534955_0_0_0 = { 1, GenInst_Contraction_t3257534955_0_0_0_Types };
static const RuntimeType* GenInst_Level2Map_t3905868985_0_0_0_Types[] = { (&Level2Map_t3905868985_0_0_0) };
extern const Il2CppGenericInst GenInst_Level2Map_t3905868985_0_0_0 = { 1, GenInst_Level2Map_t3905868985_0_0_0_Types };
static const RuntimeType* GenInst_BigInteger_t1715200596_0_0_0_Types[] = { (&BigInteger_t1715200596_0_0_0) };
extern const Il2CppGenericInst GenInst_BigInteger_t1715200596_0_0_0 = { 1, GenInst_BigInteger_t1715200596_0_0_0_Types };
static const RuntimeType* GenInst_KeySizes_t4273035850_0_0_0_Types[] = { (&KeySizes_t4273035850_0_0_0) };
extern const Il2CppGenericInst GenInst_KeySizes_t4273035850_0_0_0 = { 1, GenInst_KeySizes_t4273035850_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3602437993_0_0_0_Types[] = { (&KeyValuePair_2_t3602437993_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3602437993_0_0_0 = { 1, GenInst_KeyValuePair_2_t3602437993_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t1385909157_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&RuntimeObject_0_0_0), (&DictionaryEntry_t1385909157_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t1385909157_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t1385909157_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t3602437993_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&RuntimeObject_0_0_0), (&KeyValuePair_2_t3602437993_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t3602437993_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t3602437993_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&RuntimeObject_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_Slot_t2102621701_0_0_0_Types[] = { (&Slot_t2102621701_0_0_0) };
extern const Il2CppGenericInst GenInst_Slot_t2102621701_0_0_0 = { 1, GenInst_Slot_t2102621701_0_0_0_Types };
static const RuntimeType* GenInst_Slot_t1141617207_0_0_0_Types[] = { (&Slot_t1141617207_0_0_0) };
extern const Il2CppGenericInst GenInst_Slot_t1141617207_0_0_0 = { 1, GenInst_Slot_t1141617207_0_0_0_Types };
static const RuntimeType* GenInst_StackFrame_t4156368350_0_0_0_Types[] = { (&StackFrame_t4156368350_0_0_0) };
extern const Il2CppGenericInst GenInst_StackFrame_t4156368350_0_0_0 = { 1, GenInst_StackFrame_t4156368350_0_0_0_Types };
static const RuntimeType* GenInst_Calendar_t585695848_0_0_0_Types[] = { (&Calendar_t585695848_0_0_0) };
extern const Il2CppGenericInst GenInst_Calendar_t585695848_0_0_0 = { 1, GenInst_Calendar_t585695848_0_0_0_Types };
static const RuntimeType* GenInst_ModuleBuilder_t671559400_0_0_0_Types[] = { (&ModuleBuilder_t671559400_0_0_0) };
extern const Il2CppGenericInst GenInst_ModuleBuilder_t671559400_0_0_0 = { 1, GenInst_ModuleBuilder_t671559400_0_0_0_Types };
static const RuntimeType* GenInst__ModuleBuilder_t3149677118_0_0_0_Types[] = { (&_ModuleBuilder_t3149677118_0_0_0) };
extern const Il2CppGenericInst GenInst__ModuleBuilder_t3149677118_0_0_0 = { 1, GenInst__ModuleBuilder_t3149677118_0_0_0_Types };
static const RuntimeType* GenInst_Module_t2594760207_0_0_0_Types[] = { (&Module_t2594760207_0_0_0) };
extern const Il2CppGenericInst GenInst_Module_t2594760207_0_0_0 = { 1, GenInst_Module_t2594760207_0_0_0_Types };
static const RuntimeType* GenInst__Module_t494242506_0_0_0_Types[] = { (&_Module_t494242506_0_0_0) };
extern const Il2CppGenericInst GenInst__Module_t494242506_0_0_0 = { 1, GenInst__Module_t494242506_0_0_0_Types };
static const RuntimeType* GenInst_ParameterBuilder_t2478802094_0_0_0_Types[] = { (&ParameterBuilder_t2478802094_0_0_0) };
extern const Il2CppGenericInst GenInst_ParameterBuilder_t2478802094_0_0_0 = { 1, GenInst_ParameterBuilder_t2478802094_0_0_0_Types };
static const RuntimeType* GenInst__ParameterBuilder_t3730072709_0_0_0_Types[] = { (&_ParameterBuilder_t3730072709_0_0_0) };
extern const Il2CppGenericInst GenInst__ParameterBuilder_t3730072709_0_0_0 = { 1, GenInst__ParameterBuilder_t3730072709_0_0_0_Types };
static const RuntimeType* GenInst_TypeU5BU5D_t1830665827_0_0_0_Types[] = { (&TypeU5BU5D_t1830665827_0_0_0) };
extern const Il2CppGenericInst GenInst_TypeU5BU5D_t1830665827_0_0_0 = { 1, GenInst_TypeU5BU5D_t1830665827_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeArray_0_0_0_Types[] = { (&RuntimeArray_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeArray_0_0_0 = { 1, GenInst_RuntimeArray_0_0_0_Types };
static const RuntimeType* GenInst_ICollection_t1881172908_0_0_0_Types[] = { (&ICollection_t1881172908_0_0_0) };
extern const Il2CppGenericInst GenInst_ICollection_t1881172908_0_0_0 = { 1, GenInst_ICollection_t1881172908_0_0_0_Types };
static const RuntimeType* GenInst_IList_t2514494774_0_0_0_Types[] = { (&IList_t2514494774_0_0_0) };
extern const Il2CppGenericInst GenInst_IList_t2514494774_0_0_0 = { 1, GenInst_IList_t2514494774_0_0_0_Types };
static const RuntimeType* GenInst_IList_1_t4288475132_0_0_0_Types[] = { (&IList_1_t4288475132_0_0_0) };
extern const Il2CppGenericInst GenInst_IList_1_t4288475132_0_0_0 = { 1, GenInst_IList_1_t4288475132_0_0_0_Types };
static const RuntimeType* GenInst_ICollection_1_t3013691889_0_0_0_Types[] = { (&ICollection_1_t3013691889_0_0_0) };
extern const Il2CppGenericInst GenInst_ICollection_1_t3013691889_0_0_0 = { 1, GenInst_ICollection_1_t3013691889_0_0_0_Types };
static const RuntimeType* GenInst_IEnumerable_1_t390880001_0_0_0_Types[] = { (&IEnumerable_1_t390880001_0_0_0) };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t390880001_0_0_0 = { 1, GenInst_IEnumerable_1_t390880001_0_0_0_Types };
static const RuntimeType* GenInst_IList_1_t1904346532_0_0_0_Types[] = { (&IList_1_t1904346532_0_0_0) };
extern const Il2CppGenericInst GenInst_IList_1_t1904346532_0_0_0 = { 1, GenInst_IList_1_t1904346532_0_0_0_Types };
static const RuntimeType* GenInst_ICollection_1_t629563289_0_0_0_Types[] = { (&ICollection_1_t629563289_0_0_0) };
extern const Il2CppGenericInst GenInst_ICollection_1_t629563289_0_0_0 = { 1, GenInst_ICollection_1_t629563289_0_0_0_Types };
static const RuntimeType* GenInst_IEnumerable_1_t2301718697_0_0_0_Types[] = { (&IEnumerable_1_t2301718697_0_0_0) };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t2301718697_0_0_0 = { 1, GenInst_IEnumerable_1_t2301718697_0_0_0_Types };
static const RuntimeType* GenInst_IList_1_t632813525_0_0_0_Types[] = { (&IList_1_t632813525_0_0_0) };
extern const Il2CppGenericInst GenInst_IList_1_t632813525_0_0_0 = { 1, GenInst_IList_1_t632813525_0_0_0_Types };
static const RuntimeType* GenInst_ICollection_1_t3652997578_0_0_0_Types[] = { (&ICollection_1_t3652997578_0_0_0) };
extern const Il2CppGenericInst GenInst_ICollection_1_t3652997578_0_0_0 = { 1, GenInst_ICollection_1_t3652997578_0_0_0_Types };
static const RuntimeType* GenInst_IEnumerable_1_t1030185690_0_0_0_Types[] = { (&IEnumerable_1_t1030185690_0_0_0) };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t1030185690_0_0_0 = { 1, GenInst_IEnumerable_1_t1030185690_0_0_0_Types };
static const RuntimeType* GenInst_IList_1_t2432397382_0_0_0_Types[] = { (&IList_1_t2432397382_0_0_0) };
extern const Il2CppGenericInst GenInst_IList_1_t2432397382_0_0_0 = { 1, GenInst_IList_1_t2432397382_0_0_0_Types };
static const RuntimeType* GenInst_ICollection_1_t1157614139_0_0_0_Types[] = { (&ICollection_1_t1157614139_0_0_0) };
extern const Il2CppGenericInst GenInst_ICollection_1_t1157614139_0_0_0 = { 1, GenInst_ICollection_1_t1157614139_0_0_0_Types };
static const RuntimeType* GenInst_IEnumerable_1_t2829769547_0_0_0_Types[] = { (&IEnumerable_1_t2829769547_0_0_0) };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t2829769547_0_0_0 = { 1, GenInst_IEnumerable_1_t2829769547_0_0_0_Types };
static const RuntimeType* GenInst_IList_1_t1773863443_0_0_0_Types[] = { (&IList_1_t1773863443_0_0_0) };
extern const Il2CppGenericInst GenInst_IList_1_t1773863443_0_0_0 = { 1, GenInst_IList_1_t1773863443_0_0_0_Types };
static const RuntimeType* GenInst_ICollection_1_t499080200_0_0_0_Types[] = { (&ICollection_1_t499080200_0_0_0) };
extern const Il2CppGenericInst GenInst_ICollection_1_t499080200_0_0_0 = { 1, GenInst_ICollection_1_t499080200_0_0_0_Types };
static const RuntimeType* GenInst_IEnumerable_1_t2171235608_0_0_0_Types[] = { (&IEnumerable_1_t2171235608_0_0_0) };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t2171235608_0_0_0 = { 1, GenInst_IEnumerable_1_t2171235608_0_0_0_Types };
static const RuntimeType* GenInst_IList_1_t3673605699_0_0_0_Types[] = { (&IList_1_t3673605699_0_0_0) };
extern const Il2CppGenericInst GenInst_IList_1_t3673605699_0_0_0 = { 1, GenInst_IList_1_t3673605699_0_0_0_Types };
static const RuntimeType* GenInst_ICollection_1_t2398822456_0_0_0_Types[] = { (&ICollection_1_t2398822456_0_0_0) };
extern const Il2CppGenericInst GenInst_ICollection_1_t2398822456_0_0_0 = { 1, GenInst_ICollection_1_t2398822456_0_0_0_Types };
static const RuntimeType* GenInst_IEnumerable_1_t4070977864_0_0_0_Types[] = { (&IEnumerable_1_t4070977864_0_0_0) };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t4070977864_0_0_0 = { 1, GenInst_IEnumerable_1_t4070977864_0_0_0_Types };
static const RuntimeType* GenInst_IList_1_t1015479969_0_0_0_Types[] = { (&IList_1_t1015479969_0_0_0) };
extern const Il2CppGenericInst GenInst_IList_1_t1015479969_0_0_0 = { 1, GenInst_IList_1_t1015479969_0_0_0_Types };
static const RuntimeType* GenInst_ICollection_1_t4035664022_0_0_0_Types[] = { (&ICollection_1_t4035664022_0_0_0) };
extern const Il2CppGenericInst GenInst_ICollection_1_t4035664022_0_0_0 = { 1, GenInst_ICollection_1_t4035664022_0_0_0_Types };
static const RuntimeType* GenInst_IEnumerable_1_t1412852134_0_0_0_Types[] = { (&IEnumerable_1_t1412852134_0_0_0) };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t1412852134_0_0_0 = { 1, GenInst_IEnumerable_1_t1412852134_0_0_0_Types };
static const RuntimeType* GenInst_ILTokenInfo_t281295013_0_0_0_Types[] = { (&ILTokenInfo_t281295013_0_0_0) };
extern const Il2CppGenericInst GenInst_ILTokenInfo_t281295013_0_0_0 = { 1, GenInst_ILTokenInfo_t281295013_0_0_0_Types };
static const RuntimeType* GenInst_LabelData_t1059919410_0_0_0_Types[] = { (&LabelData_t1059919410_0_0_0) };
extern const Il2CppGenericInst GenInst_LabelData_t1059919410_0_0_0 = { 1, GenInst_LabelData_t1059919410_0_0_0_Types };
static const RuntimeType* GenInst_LabelFixup_t2685311059_0_0_0_Types[] = { (&LabelFixup_t2685311059_0_0_0) };
extern const Il2CppGenericInst GenInst_LabelFixup_t2685311059_0_0_0 = { 1, GenInst_LabelFixup_t2685311059_0_0_0_Types };
static const RuntimeType* GenInst_GenericTypeParameterBuilder_t193954652_0_0_0_Types[] = { (&GenericTypeParameterBuilder_t193954652_0_0_0) };
extern const Il2CppGenericInst GenInst_GenericTypeParameterBuilder_t193954652_0_0_0 = { 1, GenInst_GenericTypeParameterBuilder_t193954652_0_0_0_Types };
static const RuntimeType* GenInst_CustomAttributeBuilder_t1432176696_0_0_0_Types[] = { (&CustomAttributeBuilder_t1432176696_0_0_0) };
extern const Il2CppGenericInst GenInst_CustomAttributeBuilder_t1432176696_0_0_0 = { 1, GenInst_CustomAttributeBuilder_t1432176696_0_0_0_Types };
static const RuntimeType* GenInst__CustomAttributeBuilder_t2651440002_0_0_0_Types[] = { (&_CustomAttributeBuilder_t2651440002_0_0_0) };
extern const Il2CppGenericInst GenInst__CustomAttributeBuilder_t2651440002_0_0_0 = { 1, GenInst__CustomAttributeBuilder_t2651440002_0_0_0_Types };
static const RuntimeType* GenInst_RefEmitPermissionSet_t945207422_0_0_0_Types[] = { (&RefEmitPermissionSet_t945207422_0_0_0) };
extern const Il2CppGenericInst GenInst_RefEmitPermissionSet_t945207422_0_0_0 = { 1, GenInst_RefEmitPermissionSet_t945207422_0_0_0_Types };
static const RuntimeType* GenInst_TypeBuilder_t1663068587_0_0_0_Types[] = { (&TypeBuilder_t1663068587_0_0_0) };
extern const Il2CppGenericInst GenInst_TypeBuilder_t1663068587_0_0_0 = { 1, GenInst_TypeBuilder_t1663068587_0_0_0_Types };
static const RuntimeType* GenInst__TypeBuilder_t4136446867_0_0_0_Types[] = { (&_TypeBuilder_t4136446867_0_0_0) };
extern const Il2CppGenericInst GenInst__TypeBuilder_t4136446867_0_0_0 = { 1, GenInst__TypeBuilder_t4136446867_0_0_0_Types };
static const RuntimeType* GenInst_MethodBuilder_t2964638190_0_0_0_Types[] = { (&MethodBuilder_t2964638190_0_0_0) };
extern const Il2CppGenericInst GenInst_MethodBuilder_t2964638190_0_0_0 = { 1, GenInst_MethodBuilder_t2964638190_0_0_0_Types };
static const RuntimeType* GenInst__MethodBuilder_t195532601_0_0_0_Types[] = { (&_MethodBuilder_t195532601_0_0_0) };
extern const Il2CppGenericInst GenInst__MethodBuilder_t195532601_0_0_0 = { 1, GenInst__MethodBuilder_t195532601_0_0_0_Types };
static const RuntimeType* GenInst_FieldBuilder_t803025013_0_0_0_Types[] = { (&FieldBuilder_t803025013_0_0_0) };
extern const Il2CppGenericInst GenInst_FieldBuilder_t803025013_0_0_0 = { 1, GenInst_FieldBuilder_t803025013_0_0_0_Types };
static const RuntimeType* GenInst__FieldBuilder_t3486190427_0_0_0_Types[] = { (&_FieldBuilder_t3486190427_0_0_0) };
extern const Il2CppGenericInst GenInst__FieldBuilder_t3486190427_0_0_0 = { 1, GenInst__FieldBuilder_t3486190427_0_0_0_Types };
static const RuntimeType* GenInst_MonoResource_t2965441204_0_0_0_Types[] = { (&MonoResource_t2965441204_0_0_0) };
extern const Il2CppGenericInst GenInst_MonoResource_t2965441204_0_0_0 = { 1, GenInst_MonoResource_t2965441204_0_0_0_Types };
static const RuntimeType* GenInst_ConstructorBuilder_t1596923965_0_0_0_Types[] = { (&ConstructorBuilder_t1596923965_0_0_0) };
extern const Il2CppGenericInst GenInst_ConstructorBuilder_t1596923965_0_0_0 = { 1, GenInst_ConstructorBuilder_t1596923965_0_0_0_Types };
static const RuntimeType* GenInst__ConstructorBuilder_t3355697174_0_0_0_Types[] = { (&_ConstructorBuilder_t3355697174_0_0_0) };
extern const Il2CppGenericInst GenInst__ConstructorBuilder_t3355697174_0_0_0 = { 1, GenInst__ConstructorBuilder_t3355697174_0_0_0_Types };
static const RuntimeType* GenInst_PropertyBuilder_t1258013571_0_0_0_Types[] = { (&PropertyBuilder_t1258013571_0_0_0) };
extern const Il2CppGenericInst GenInst_PropertyBuilder_t1258013571_0_0_0 = { 1, GenInst_PropertyBuilder_t1258013571_0_0_0_Types };
static const RuntimeType* GenInst__PropertyBuilder_t2876596643_0_0_0_Types[] = { (&_PropertyBuilder_t2876596643_0_0_0) };
extern const Il2CppGenericInst GenInst__PropertyBuilder_t2876596643_0_0_0 = { 1, GenInst__PropertyBuilder_t2876596643_0_0_0_Types };
static const RuntimeType* GenInst_PropertyInfo_t_0_0_0_Types[] = { (&PropertyInfo_t_0_0_0) };
extern const Il2CppGenericInst GenInst_PropertyInfo_t_0_0_0 = { 1, GenInst_PropertyInfo_t_0_0_0_Types };
static const RuntimeType* GenInst__PropertyInfo_t2517621214_0_0_0_Types[] = { (&_PropertyInfo_t2517621214_0_0_0) };
extern const Il2CppGenericInst GenInst__PropertyInfo_t2517621214_0_0_0 = { 1, GenInst__PropertyInfo_t2517621214_0_0_0_Types };
static const RuntimeType* GenInst_EventBuilder_t2300762438_0_0_0_Types[] = { (&EventBuilder_t2300762438_0_0_0) };
extern const Il2CppGenericInst GenInst_EventBuilder_t2300762438_0_0_0 = { 1, GenInst_EventBuilder_t2300762438_0_0_0_Types };
static const RuntimeType* GenInst__EventBuilder_t3169829690_0_0_0_Types[] = { (&_EventBuilder_t3169829690_0_0_0) };
extern const Il2CppGenericInst GenInst__EventBuilder_t3169829690_0_0_0 = { 1, GenInst__EventBuilder_t3169829690_0_0_0_Types };
static const RuntimeType* GenInst_CustomAttributeTypedArgument_t2667702721_0_0_0_Types[] = { (&CustomAttributeTypedArgument_t2667702721_0_0_0) };
extern const Il2CppGenericInst GenInst_CustomAttributeTypedArgument_t2667702721_0_0_0 = { 1, GenInst_CustomAttributeTypedArgument_t2667702721_0_0_0_Types };
static const RuntimeType* GenInst_CustomAttributeNamedArgument_t425730339_0_0_0_Types[] = { (&CustomAttributeNamedArgument_t425730339_0_0_0) };
extern const Il2CppGenericInst GenInst_CustomAttributeNamedArgument_t425730339_0_0_0 = { 1, GenInst_CustomAttributeNamedArgument_t425730339_0_0_0_Types };
static const RuntimeType* GenInst_CustomAttributeData_t3408840380_0_0_0_Types[] = { (&CustomAttributeData_t3408840380_0_0_0) };
extern const Il2CppGenericInst GenInst_CustomAttributeData_t3408840380_0_0_0 = { 1, GenInst_CustomAttributeData_t3408840380_0_0_0_Types };
static const RuntimeType* GenInst_ResourceInfo_t1773833009_0_0_0_Types[] = { (&ResourceInfo_t1773833009_0_0_0) };
extern const Il2CppGenericInst GenInst_ResourceInfo_t1773833009_0_0_0 = { 1, GenInst_ResourceInfo_t1773833009_0_0_0_Types };
static const RuntimeType* GenInst_ResourceCacheItem_t3632973466_0_0_0_Types[] = { (&ResourceCacheItem_t3632973466_0_0_0) };
extern const Il2CppGenericInst GenInst_ResourceCacheItem_t3632973466_0_0_0 = { 1, GenInst_ResourceCacheItem_t3632973466_0_0_0_Types };
static const RuntimeType* GenInst_IContextProperty_t2748911791_0_0_0_Types[] = { (&IContextProperty_t2748911791_0_0_0) };
extern const Il2CppGenericInst GenInst_IContextProperty_t2748911791_0_0_0 = { 1, GenInst_IContextProperty_t2748911791_0_0_0_Types };
static const RuntimeType* GenInst_Header_t2445250629_0_0_0_Types[] = { (&Header_t2445250629_0_0_0) };
extern const Il2CppGenericInst GenInst_Header_t2445250629_0_0_0 = { 1, GenInst_Header_t2445250629_0_0_0_Types };
static const RuntimeType* GenInst_ITrackingHandler_t2297406515_0_0_0_Types[] = { (&ITrackingHandler_t2297406515_0_0_0) };
extern const Il2CppGenericInst GenInst_ITrackingHandler_t2297406515_0_0_0 = { 1, GenInst_ITrackingHandler_t2297406515_0_0_0_Types };
static const RuntimeType* GenInst_IContextAttribute_t632650489_0_0_0_Types[] = { (&IContextAttribute_t632650489_0_0_0) };
extern const Il2CppGenericInst GenInst_IContextAttribute_t632650489_0_0_0 = { 1, GenInst_IContextAttribute_t632650489_0_0_0_Types };
static const RuntimeType* GenInst_DateTime_t507798353_0_0_0_Types[] = { (&DateTime_t507798353_0_0_0) };
extern const Il2CppGenericInst GenInst_DateTime_t507798353_0_0_0 = { 1, GenInst_DateTime_t507798353_0_0_0_Types };
static const RuntimeType* GenInst_TimeSpan_t2986675223_0_0_0_Types[] = { (&TimeSpan_t2986675223_0_0_0) };
extern const Il2CppGenericInst GenInst_TimeSpan_t2986675223_0_0_0 = { 1, GenInst_TimeSpan_t2986675223_0_0_0_Types };
static const RuntimeType* GenInst_TypeTag_t1137063594_0_0_0_Types[] = { (&TypeTag_t1137063594_0_0_0) };
extern const Il2CppGenericInst GenInst_TypeTag_t1137063594_0_0_0 = { 1, GenInst_TypeTag_t1137063594_0_0_0_Types };
static const RuntimeType* GenInst_MonoType_t_0_0_0_Types[] = { (&MonoType_t_0_0_0) };
extern const Il2CppGenericInst GenInst_MonoType_t_0_0_0 = { 1, GenInst_MonoType_t_0_0_0_Types };
static const RuntimeType* GenInst_StrongName_t848839738_0_0_0_Types[] = { (&StrongName_t848839738_0_0_0) };
extern const Il2CppGenericInst GenInst_StrongName_t848839738_0_0_0 = { 1, GenInst_StrongName_t848839738_0_0_0_Types };
static const RuntimeType* GenInst_IBuiltInEvidence_t1834666417_0_0_0_Types[] = { (&IBuiltInEvidence_t1834666417_0_0_0) };
extern const Il2CppGenericInst GenInst_IBuiltInEvidence_t1834666417_0_0_0 = { 1, GenInst_IBuiltInEvidence_t1834666417_0_0_0_Types };
static const RuntimeType* GenInst_IIdentityPermissionFactory_t233160095_0_0_0_Types[] = { (&IIdentityPermissionFactory_t233160095_0_0_0) };
extern const Il2CppGenericInst GenInst_IIdentityPermissionFactory_t233160095_0_0_0 = { 1, GenInst_IIdentityPermissionFactory_t233160095_0_0_0_Types };
static const RuntimeType* GenInst_DateTimeOffset_t2220763929_0_0_0_Types[] = { (&DateTimeOffset_t2220763929_0_0_0) };
extern const Il2CppGenericInst GenInst_DateTimeOffset_t2220763929_0_0_0 = { 1, GenInst_DateTimeOffset_t2220763929_0_0_0_Types };
static const RuntimeType* GenInst_Guid_t_0_0_0_Types[] = { (&Guid_t_0_0_0) };
extern const Il2CppGenericInst GenInst_Guid_t_0_0_0 = { 1, GenInst_Guid_t_0_0_0_Types };
static const RuntimeType* GenInst_Version_t900644674_0_0_0_Types[] = { (&Version_t900644674_0_0_0) };
extern const Il2CppGenericInst GenInst_Version_t900644674_0_0_0 = { 1, GenInst_Version_t900644674_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_Boolean_t2059672899_0_0_0_Types[] = { (&String_t_0_0_0), (&Boolean_t2059672899_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Boolean_t2059672899_0_0_0 = { 2, GenInst_String_t_0_0_0_Boolean_t2059672899_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_Boolean_t2059672899_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&Boolean_t2059672899_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_Boolean_t2059672899_0_0_0 = { 2, GenInst_RuntimeObject_0_0_0_Boolean_t2059672899_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t2366093889_0_0_0_Types[] = { (&KeyValuePair_2_t2366093889_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2366093889_0_0_0 = { 1, GenInst_KeyValuePair_2_t2366093889_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_Boolean_t2059672899_0_0_0_DictionaryEntry_t1385909157_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&Boolean_t2059672899_0_0_0), (&DictionaryEntry_t1385909157_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_Boolean_t2059672899_0_0_0_DictionaryEntry_t1385909157_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_Boolean_t2059672899_0_0_0_DictionaryEntry_t1385909157_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_Boolean_t2059672899_0_0_0_KeyValuePair_2_t2366093889_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&Boolean_t2059672899_0_0_0), (&KeyValuePair_2_t2366093889_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_Boolean_t2059672899_0_0_0_KeyValuePair_2_t2366093889_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_Boolean_t2059672899_0_0_0_KeyValuePair_2_t2366093889_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_Boolean_t2059672899_0_0_0_DictionaryEntry_t1385909157_0_0_0_Types[] = { (&String_t_0_0_0), (&Boolean_t2059672899_0_0_0), (&DictionaryEntry_t1385909157_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Boolean_t2059672899_0_0_0_DictionaryEntry_t1385909157_0_0_0 = { 3, GenInst_String_t_0_0_0_Boolean_t2059672899_0_0_0_DictionaryEntry_t1385909157_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t2892953148_0_0_0_Types[] = { (&KeyValuePair_2_t2892953148_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2892953148_0_0_0 = { 1, GenInst_KeyValuePair_2_t2892953148_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_Boolean_t2059672899_0_0_0_KeyValuePair_2_t2892953148_0_0_0_Types[] = { (&String_t_0_0_0), (&Boolean_t2059672899_0_0_0), (&KeyValuePair_2_t2892953148_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Boolean_t2059672899_0_0_0_KeyValuePair_2_t2892953148_0_0_0 = { 3, GenInst_String_t_0_0_0_Boolean_t2059672899_0_0_0_KeyValuePair_2_t2892953148_0_0_0_Types };
static const RuntimeType* GenInst_X509Certificate_t3231070314_0_0_0_Types[] = { (&X509Certificate_t3231070314_0_0_0) };
extern const Il2CppGenericInst GenInst_X509Certificate_t3231070314_0_0_0 = { 1, GenInst_X509Certificate_t3231070314_0_0_0_Types };
static const RuntimeType* GenInst_IDeserializationCallback_t1559034278_0_0_0_Types[] = { (&IDeserializationCallback_t1559034278_0_0_0) };
extern const Il2CppGenericInst GenInst_IDeserializationCallback_t1559034278_0_0_0 = { 1, GenInst_IDeserializationCallback_t1559034278_0_0_0_Types };
static const RuntimeType* GenInst_X509ChainStatus_t177750891_0_0_0_Types[] = { (&X509ChainStatus_t177750891_0_0_0) };
extern const Il2CppGenericInst GenInst_X509ChainStatus_t177750891_0_0_0 = { 1, GenInst_X509ChainStatus_t177750891_0_0_0_Types };
static const RuntimeType* GenInst_Capture_t1939465921_0_0_0_Types[] = { (&Capture_t1939465921_0_0_0) };
extern const Il2CppGenericInst GenInst_Capture_t1939465921_0_0_0 = { 1, GenInst_Capture_t1939465921_0_0_0_Types };
static const RuntimeType* GenInst_Group_t61132085_0_0_0_Types[] = { (&Group_t61132085_0_0_0) };
extern const Il2CppGenericInst GenInst_Group_t61132085_0_0_0 = { 1, GenInst_Group_t61132085_0_0_0_Types };
static const RuntimeType* GenInst_Mark_t4050795874_0_0_0_Types[] = { (&Mark_t4050795874_0_0_0) };
extern const Il2CppGenericInst GenInst_Mark_t4050795874_0_0_0 = { 1, GenInst_Mark_t4050795874_0_0_0_Types };
static const RuntimeType* GenInst_UriScheme_t2622429923_0_0_0_Types[] = { (&UriScheme_t2622429923_0_0_0) };
extern const Il2CppGenericInst GenInst_UriScheme_t2622429923_0_0_0 = { 1, GenInst_UriScheme_t2622429923_0_0_0_Types };
static const RuntimeType* GenInst_BigInteger_t1715200597_0_0_0_Types[] = { (&BigInteger_t1715200597_0_0_0) };
extern const Il2CppGenericInst GenInst_BigInteger_t1715200597_0_0_0 = { 1, GenInst_BigInteger_t1715200597_0_0_0_Types };
static const RuntimeType* GenInst_ByteU5BU5D_t1014014593_0_0_0_Types[] = { (&ByteU5BU5D_t1014014593_0_0_0) };
extern const Il2CppGenericInst GenInst_ByteU5BU5D_t1014014593_0_0_0 = { 1, GenInst_ByteU5BU5D_t1014014593_0_0_0_Types };
static const RuntimeType* GenInst_IList_1_t2894918582_0_0_0_Types[] = { (&IList_1_t2894918582_0_0_0) };
extern const Il2CppGenericInst GenInst_IList_1_t2894918582_0_0_0 = { 1, GenInst_IList_1_t2894918582_0_0_0_Types };
static const RuntimeType* GenInst_ICollection_1_t1620135339_0_0_0_Types[] = { (&ICollection_1_t1620135339_0_0_0) };
extern const Il2CppGenericInst GenInst_ICollection_1_t1620135339_0_0_0 = { 1, GenInst_ICollection_1_t1620135339_0_0_0_Types };
static const RuntimeType* GenInst_IEnumerable_1_t3292290747_0_0_0_Types[] = { (&IEnumerable_1_t3292290747_0_0_0) };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t3292290747_0_0_0 = { 1, GenInst_IEnumerable_1_t3292290747_0_0_0_Types };
static const RuntimeType* GenInst_ClientCertificateType_t194976790_0_0_0_Types[] = { (&ClientCertificateType_t194976790_0_0_0) };
extern const Il2CppGenericInst GenInst_ClientCertificateType_t194976790_0_0_0 = { 1, GenInst_ClientCertificateType_t194976790_0_0_0_Types };
static const RuntimeType* GenInst_AsyncOperation_t4233810744_0_0_0_Types[] = { (&AsyncOperation_t4233810744_0_0_0) };
extern const Il2CppGenericInst GenInst_AsyncOperation_t4233810744_0_0_0 = { 1, GenInst_AsyncOperation_t4233810744_0_0_0_Types };
static const RuntimeType* GenInst_Camera_t101201881_0_0_0_Types[] = { (&Camera_t101201881_0_0_0) };
extern const Il2CppGenericInst GenInst_Camera_t101201881_0_0_0 = { 1, GenInst_Camera_t101201881_0_0_0_Types };
static const RuntimeType* GenInst_Behaviour_t4110946901_0_0_0_Types[] = { (&Behaviour_t4110946901_0_0_0) };
extern const Il2CppGenericInst GenInst_Behaviour_t4110946901_0_0_0 = { 1, GenInst_Behaviour_t4110946901_0_0_0_Types };
static const RuntimeType* GenInst_Component_t2160255836_0_0_0_Types[] = { (&Component_t2160255836_0_0_0) };
extern const Il2CppGenericInst GenInst_Component_t2160255836_0_0_0 = { 1, GenInst_Component_t2160255836_0_0_0_Types };
static const RuntimeType* GenInst_Object_t2461733472_0_0_0_Types[] = { (&Object_t2461733472_0_0_0) };
extern const Il2CppGenericInst GenInst_Object_t2461733472_0_0_0 = { 1, GenInst_Object_t2461733472_0_0_0_Types };
static const RuntimeType* GenInst_Display_t4002355288_0_0_0_Types[] = { (&Display_t4002355288_0_0_0) };
extern const Il2CppGenericInst GenInst_Display_t4002355288_0_0_0 = { 1, GenInst_Display_t4002355288_0_0_0_Types };
static const RuntimeType* GenInst_Keyframe_t3396755990_0_0_0_Types[] = { (&Keyframe_t3396755990_0_0_0) };
extern const Il2CppGenericInst GenInst_Keyframe_t3396755990_0_0_0 = { 1, GenInst_Keyframe_t3396755990_0_0_0_Types };
static const RuntimeType* GenInst_Playable_t1760619938_0_0_0_Types[] = { (&Playable_t1760619938_0_0_0) };
extern const Il2CppGenericInst GenInst_Playable_t1760619938_0_0_0 = { 1, GenInst_Playable_t1760619938_0_0_0_Types };
static const RuntimeType* GenInst_PlayableOutput_t3840687608_0_0_0_Types[] = { (&PlayableOutput_t3840687608_0_0_0) };
extern const Il2CppGenericInst GenInst_PlayableOutput_t3840687608_0_0_0 = { 1, GenInst_PlayableOutput_t3840687608_0_0_0_Types };
static const RuntimeType* GenInst_Scene_t3023997566_0_0_0_LoadSceneMode_t524132339_0_0_0_Types[] = { (&Scene_t3023997566_0_0_0), (&LoadSceneMode_t524132339_0_0_0) };
extern const Il2CppGenericInst GenInst_Scene_t3023997566_0_0_0_LoadSceneMode_t524132339_0_0_0 = { 2, GenInst_Scene_t3023997566_0_0_0_LoadSceneMode_t524132339_0_0_0_Types };
static const RuntimeType* GenInst_Scene_t3023997566_0_0_0_Types[] = { (&Scene_t3023997566_0_0_0) };
extern const Il2CppGenericInst GenInst_Scene_t3023997566_0_0_0 = { 1, GenInst_Scene_t3023997566_0_0_0_Types };
static const RuntimeType* GenInst_Scene_t3023997566_0_0_0_Scene_t3023997566_0_0_0_Types[] = { (&Scene_t3023997566_0_0_0), (&Scene_t3023997566_0_0_0) };
extern const Il2CppGenericInst GenInst_Scene_t3023997566_0_0_0_Scene_t3023997566_0_0_0 = { 2, GenInst_Scene_t3023997566_0_0_0_Scene_t3023997566_0_0_0_Types };
static const RuntimeType* GenInst_SpriteAtlas_t1477815998_0_0_0_Types[] = { (&SpriteAtlas_t1477815998_0_0_0) };
extern const Il2CppGenericInst GenInst_SpriteAtlas_t1477815998_0_0_0 = { 1, GenInst_SpriteAtlas_t1477815998_0_0_0_Types };
static const RuntimeType* GenInst_DisallowMultipleComponent_t2957315911_0_0_0_Types[] = { (&DisallowMultipleComponent_t2957315911_0_0_0) };
extern const Il2CppGenericInst GenInst_DisallowMultipleComponent_t2957315911_0_0_0 = { 1, GenInst_DisallowMultipleComponent_t2957315911_0_0_0_Types };
static const RuntimeType* GenInst_Attribute_t1659210826_0_0_0_Types[] = { (&Attribute_t1659210826_0_0_0) };
extern const Il2CppGenericInst GenInst_Attribute_t1659210826_0_0_0 = { 1, GenInst_Attribute_t1659210826_0_0_0_Types };
static const RuntimeType* GenInst__Attribute_t480711065_0_0_0_Types[] = { (&_Attribute_t480711065_0_0_0) };
extern const Il2CppGenericInst GenInst__Attribute_t480711065_0_0_0 = { 1, GenInst__Attribute_t480711065_0_0_0_Types };
static const RuntimeType* GenInst_ExecuteInEditMode_t1836144016_0_0_0_Types[] = { (&ExecuteInEditMode_t1836144016_0_0_0) };
extern const Il2CppGenericInst GenInst_ExecuteInEditMode_t1836144016_0_0_0 = { 1, GenInst_ExecuteInEditMode_t1836144016_0_0_0_Types };
static const RuntimeType* GenInst_RequireComponent_t597737523_0_0_0_Types[] = { (&RequireComponent_t597737523_0_0_0) };
extern const Il2CppGenericInst GenInst_RequireComponent_t597737523_0_0_0 = { 1, GenInst_RequireComponent_t597737523_0_0_0_Types };
static const RuntimeType* GenInst_HitInfo_t184839221_0_0_0_Types[] = { (&HitInfo_t184839221_0_0_0) };
extern const Il2CppGenericInst GenInst_HitInfo_t184839221_0_0_0 = { 1, GenInst_HitInfo_t184839221_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&RuntimeObject_0_0_0), (&RuntimeObject_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0 = { 4, GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_PersistentCall_t2687057912_0_0_0_Types[] = { (&PersistentCall_t2687057912_0_0_0) };
extern const Il2CppGenericInst GenInst_PersistentCall_t2687057912_0_0_0 = { 1, GenInst_PersistentCall_t2687057912_0_0_0_Types };
static const RuntimeType* GenInst_BaseInvokableCall_t2920707967_0_0_0_Types[] = { (&BaseInvokableCall_t2920707967_0_0_0) };
extern const Il2CppGenericInst GenInst_BaseInvokableCall_t2920707967_0_0_0 = { 1, GenInst_BaseInvokableCall_t2920707967_0_0_0_Types };
static const RuntimeType* GenInst_WorkRequest_t2547429811_0_0_0_Types[] = { (&WorkRequest_t2547429811_0_0_0) };
extern const Il2CppGenericInst GenInst_WorkRequest_t2547429811_0_0_0 = { 1, GenInst_WorkRequest_t2547429811_0_0_0_Types };
static const RuntimeType* GenInst_PlayableBinding_t136667248_0_0_0_Types[] = { (&PlayableBinding_t136667248_0_0_0) };
extern const Il2CppGenericInst GenInst_PlayableBinding_t136667248_0_0_0 = { 1, GenInst_PlayableBinding_t136667248_0_0_0_Types };
static const RuntimeType* GenInst_MessageTypeSubscribers_t1657992506_0_0_0_Types[] = { (&MessageTypeSubscribers_t1657992506_0_0_0) };
extern const Il2CppGenericInst GenInst_MessageTypeSubscribers_t1657992506_0_0_0 = { 1, GenInst_MessageTypeSubscribers_t1657992506_0_0_0_Types };
static const RuntimeType* GenInst_MessageTypeSubscribers_t1657992506_0_0_0_Boolean_t2059672899_0_0_0_Types[] = { (&MessageTypeSubscribers_t1657992506_0_0_0), (&Boolean_t2059672899_0_0_0) };
extern const Il2CppGenericInst GenInst_MessageTypeSubscribers_t1657992506_0_0_0_Boolean_t2059672899_0_0_0 = { 2, GenInst_MessageTypeSubscribers_t1657992506_0_0_0_Boolean_t2059672899_0_0_0_Types };
static const RuntimeType* GenInst_MessageEventArgs_t947408028_0_0_0_Types[] = { (&MessageEventArgs_t947408028_0_0_0) };
extern const Il2CppGenericInst GenInst_MessageEventArgs_t947408028_0_0_0 = { 1, GenInst_MessageEventArgs_t947408028_0_0_0_Types };
static const RuntimeType* GenInst_IntPtr_t_0_0_0_WeakReference_t3998924468_0_0_0_Types[] = { (&IntPtr_t_0_0_0), (&WeakReference_t3998924468_0_0_0) };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_WeakReference_t3998924468_0_0_0 = { 2, GenInst_IntPtr_t_0_0_0_WeakReference_t3998924468_0_0_0_Types };
static const RuntimeType* GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_Types[] = { (&IntPtr_t_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t2204764377_0_0_0_Types[] = { (&KeyValuePair_2_t2204764377_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2204764377_0_0_0 = { 1, GenInst_KeyValuePair_2_t2204764377_0_0_0_Types };
static const RuntimeType* GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t1385909157_0_0_0_Types[] = { (&IntPtr_t_0_0_0), (&RuntimeObject_0_0_0), (&DictionaryEntry_t1385909157_0_0_0) };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t1385909157_0_0_0 = { 3, GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t1385909157_0_0_0_Types };
static const RuntimeType* GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t2204764377_0_0_0_Types[] = { (&IntPtr_t_0_0_0), (&RuntimeObject_0_0_0), (&KeyValuePair_2_t2204764377_0_0_0) };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t2204764377_0_0_0 = { 3, GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t2204764377_0_0_0_Types };
static const RuntimeType* GenInst_WeakReference_t3998924468_0_0_0_Types[] = { (&WeakReference_t3998924468_0_0_0) };
extern const Il2CppGenericInst GenInst_WeakReference_t3998924468_0_0_0 = { 1, GenInst_WeakReference_t3998924468_0_0_0_Types };
static const RuntimeType* GenInst_IntPtr_t_0_0_0_WeakReference_t3998924468_0_0_0_DictionaryEntry_t1385909157_0_0_0_Types[] = { (&IntPtr_t_0_0_0), (&WeakReference_t3998924468_0_0_0), (&DictionaryEntry_t1385909157_0_0_0) };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_WeakReference_t3998924468_0_0_0_DictionaryEntry_t1385909157_0_0_0 = { 3, GenInst_IntPtr_t_0_0_0_WeakReference_t3998924468_0_0_0_DictionaryEntry_t1385909157_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t2907671842_0_0_0_Types[] = { (&KeyValuePair_2_t2907671842_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2907671842_0_0_0 = { 1, GenInst_KeyValuePair_2_t2907671842_0_0_0_Types };
static const RuntimeType* GenInst_IntPtr_t_0_0_0_WeakReference_t3998924468_0_0_0_KeyValuePair_2_t2907671842_0_0_0_Types[] = { (&IntPtr_t_0_0_0), (&WeakReference_t3998924468_0_0_0), (&KeyValuePair_2_t2907671842_0_0_0) };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_WeakReference_t3998924468_0_0_0_KeyValuePair_2_t2907671842_0_0_0 = { 3, GenInst_IntPtr_t_0_0_0_WeakReference_t3998924468_0_0_0_KeyValuePair_2_t2907671842_0_0_0_Types };
static const RuntimeType* GenInst_AudioSpatializerExtensionDefinition_t4023306521_0_0_0_Types[] = { (&AudioSpatializerExtensionDefinition_t4023306521_0_0_0) };
extern const Il2CppGenericInst GenInst_AudioSpatializerExtensionDefinition_t4023306521_0_0_0 = { 1, GenInst_AudioSpatializerExtensionDefinition_t4023306521_0_0_0_Types };
static const RuntimeType* GenInst_AudioAmbisonicExtensionDefinition_t139142167_0_0_0_Types[] = { (&AudioAmbisonicExtensionDefinition_t139142167_0_0_0) };
extern const Il2CppGenericInst GenInst_AudioAmbisonicExtensionDefinition_t139142167_0_0_0 = { 1, GenInst_AudioAmbisonicExtensionDefinition_t139142167_0_0_0_Types };
static const RuntimeType* GenInst_AudioSourceExtension_t3778551013_0_0_0_Types[] = { (&AudioSourceExtension_t3778551013_0_0_0) };
extern const Il2CppGenericInst GenInst_AudioSourceExtension_t3778551013_0_0_0 = { 1, GenInst_AudioSourceExtension_t3778551013_0_0_0_Types };
static const RuntimeType* GenInst_ScriptableObject_t476655885_0_0_0_Types[] = { (&ScriptableObject_t476655885_0_0_0) };
extern const Il2CppGenericInst GenInst_ScriptableObject_t476655885_0_0_0 = { 1, GenInst_ScriptableObject_t476655885_0_0_0_Types };
static const RuntimeType* GenInst_AudioMixerPlayable_t1017334687_0_0_0_Types[] = { (&AudioMixerPlayable_t1017334687_0_0_0) };
extern const Il2CppGenericInst GenInst_AudioMixerPlayable_t1017334687_0_0_0 = { 1, GenInst_AudioMixerPlayable_t1017334687_0_0_0_Types };
static const RuntimeType* GenInst_AudioClipPlayable_t3812371941_0_0_0_Types[] = { (&AudioClipPlayable_t3812371941_0_0_0) };
extern const Il2CppGenericInst GenInst_AudioClipPlayable_t3812371941_0_0_0 = { 1, GenInst_AudioClipPlayable_t3812371941_0_0_0_Types };
static const RuntimeType* GenInst_Boolean_t2059672899_0_0_0_String_t_0_0_0_Types[] = { (&Boolean_t2059672899_0_0_0), (&String_t_0_0_0) };
extern const Il2CppGenericInst GenInst_Boolean_t2059672899_0_0_0_String_t_0_0_0 = { 2, GenInst_Boolean_t2059672899_0_0_0_String_t_0_0_0_Types };
static const RuntimeType* GenInst_Boolean_t2059672899_0_0_0_RuntimeObject_0_0_0_Types[] = { (&Boolean_t2059672899_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_Boolean_t2059672899_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_Boolean_t2059672899_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_AchievementDescription_t130800866_0_0_0_Types[] = { (&AchievementDescription_t130800866_0_0_0) };
extern const Il2CppGenericInst GenInst_AchievementDescription_t130800866_0_0_0 = { 1, GenInst_AchievementDescription_t130800866_0_0_0_Types };
static const RuntimeType* GenInst_IAchievementDescription_t924957763_0_0_0_Types[] = { (&IAchievementDescription_t924957763_0_0_0) };
extern const Il2CppGenericInst GenInst_IAchievementDescription_t924957763_0_0_0 = { 1, GenInst_IAchievementDescription_t924957763_0_0_0_Types };
static const RuntimeType* GenInst_UserProfile_t3211800844_0_0_0_Types[] = { (&UserProfile_t3211800844_0_0_0) };
extern const Il2CppGenericInst GenInst_UserProfile_t3211800844_0_0_0 = { 1, GenInst_UserProfile_t3211800844_0_0_0_Types };
static const RuntimeType* GenInst_IUserProfile_t3226494754_0_0_0_Types[] = { (&IUserProfile_t3226494754_0_0_0) };
extern const Il2CppGenericInst GenInst_IUserProfile_t3226494754_0_0_0 = { 1, GenInst_IUserProfile_t3226494754_0_0_0_Types };
static const RuntimeType* GenInst_GcLeaderboard_t617530396_0_0_0_Types[] = { (&GcLeaderboard_t617530396_0_0_0) };
extern const Il2CppGenericInst GenInst_GcLeaderboard_t617530396_0_0_0 = { 1, GenInst_GcLeaderboard_t617530396_0_0_0_Types };
static const RuntimeType* GenInst_IAchievementDescriptionU5BU5D_t2959897426_0_0_0_Types[] = { (&IAchievementDescriptionU5BU5D_t2959897426_0_0_0) };
extern const Il2CppGenericInst GenInst_IAchievementDescriptionU5BU5D_t2959897426_0_0_0 = { 1, GenInst_IAchievementDescriptionU5BU5D_t2959897426_0_0_0_Types };
static const RuntimeType* GenInst_IAchievementU5BU5D_t1231928882_0_0_0_Types[] = { (&IAchievementU5BU5D_t1231928882_0_0_0) };
extern const Il2CppGenericInst GenInst_IAchievementU5BU5D_t1231928882_0_0_0 = { 1, GenInst_IAchievementU5BU5D_t1231928882_0_0_0_Types };
static const RuntimeType* GenInst_IAchievement_t3311330019_0_0_0_Types[] = { (&IAchievement_t3311330019_0_0_0) };
extern const Il2CppGenericInst GenInst_IAchievement_t3311330019_0_0_0 = { 1, GenInst_IAchievement_t3311330019_0_0_0_Types };
static const RuntimeType* GenInst_GcAchievementData_t3167706508_0_0_0_Types[] = { (&GcAchievementData_t3167706508_0_0_0) };
extern const Il2CppGenericInst GenInst_GcAchievementData_t3167706508_0_0_0 = { 1, GenInst_GcAchievementData_t3167706508_0_0_0_Types };
static const RuntimeType* GenInst_Achievement_t2517894267_0_0_0_Types[] = { (&Achievement_t2517894267_0_0_0) };
extern const Il2CppGenericInst GenInst_Achievement_t2517894267_0_0_0 = { 1, GenInst_Achievement_t2517894267_0_0_0_Types };
static const RuntimeType* GenInst_IScoreU5BU5D_t624878879_0_0_0_Types[] = { (&IScoreU5BU5D_t624878879_0_0_0) };
extern const Il2CppGenericInst GenInst_IScoreU5BU5D_t624878879_0_0_0 = { 1, GenInst_IScoreU5BU5D_t624878879_0_0_0_Types };
static const RuntimeType* GenInst_IScore_t1996314298_0_0_0_Types[] = { (&IScore_t1996314298_0_0_0) };
extern const Il2CppGenericInst GenInst_IScore_t1996314298_0_0_0 = { 1, GenInst_IScore_t1996314298_0_0_0_Types };
static const RuntimeType* GenInst_GcScoreData_t3955401213_0_0_0_Types[] = { (&GcScoreData_t3955401213_0_0_0) };
extern const Il2CppGenericInst GenInst_GcScoreData_t3955401213_0_0_0 = { 1, GenInst_GcScoreData_t3955401213_0_0_0_Types };
static const RuntimeType* GenInst_Score_t1135675058_0_0_0_Types[] = { (&Score_t1135675058_0_0_0) };
extern const Il2CppGenericInst GenInst_Score_t1135675058_0_0_0 = { 1, GenInst_Score_t1135675058_0_0_0_Types };
static const RuntimeType* GenInst_IUserProfileU5BU5D_t3577724439_0_0_0_Types[] = { (&IUserProfileU5BU5D_t3577724439_0_0_0) };
extern const Il2CppGenericInst GenInst_IUserProfileU5BU5D_t3577724439_0_0_0 = { 1, GenInst_IUserProfileU5BU5D_t3577724439_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_RuntimeObject_0_0_0_Types[] = { (&String_t_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_String_t_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t4129297252_0_0_0_Types[] = { (&KeyValuePair_2_t4129297252_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4129297252_0_0_0 = { 1, GenInst_KeyValuePair_2_t4129297252_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t1385909157_0_0_0_Types[] = { (&String_t_0_0_0), (&RuntimeObject_0_0_0), (&DictionaryEntry_t1385909157_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t1385909157_0_0_0 = { 3, GenInst_String_t_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t1385909157_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t4129297252_0_0_0_Types[] = { (&String_t_0_0_0), (&RuntimeObject_0_0_0), (&KeyValuePair_2_t4129297252_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t4129297252_0_0_0 = { 3, GenInst_String_t_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t4129297252_0_0_0_Types };
static const RuntimeType* GenInst_FieldWithTarget_t1883299082_0_0_0_Types[] = { (&FieldWithTarget_t1883299082_0_0_0) };
extern const Il2CppGenericInst GenInst_FieldWithTarget_t1883299082_0_0_0 = { 1, GenInst_FieldWithTarget_t1883299082_0_0_0_Types };
static const RuntimeType* GenInst_IEnumerable_1_t2300816294_gp_0_0_0_0_Types[] = { (&IEnumerable_1_t2300816294_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t2300816294_gp_0_0_0_0 = { 1, GenInst_IEnumerable_1_t2300816294_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m2280917461_gp_0_0_0_0_Types[] = { (&Array_InternalArray__IEnumerable_GetEnumerator_m2280917461_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m2280917461_gp_0_0_0_0 = { 1, GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m2280917461_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m2667677402_gp_0_0_0_0_Array_Sort_m2667677402_gp_0_0_0_0_Types[] = { (&Array_Sort_m2667677402_gp_0_0_0_0), (&Array_Sort_m2667677402_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m2667677402_gp_0_0_0_0_Array_Sort_m2667677402_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m2667677402_gp_0_0_0_0_Array_Sort_m2667677402_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m724484260_gp_0_0_0_0_Array_Sort_m724484260_gp_1_0_0_0_Types[] = { (&Array_Sort_m724484260_gp_0_0_0_0), (&Array_Sort_m724484260_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m724484260_gp_0_0_0_0_Array_Sort_m724484260_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m724484260_gp_0_0_0_0_Array_Sort_m724484260_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m3090697542_gp_0_0_0_0_Types[] = { (&Array_Sort_m3090697542_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m3090697542_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m3090697542_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m3090697542_gp_0_0_0_0_Array_Sort_m3090697542_gp_0_0_0_0_Types[] = { (&Array_Sort_m3090697542_gp_0_0_0_0), (&Array_Sort_m3090697542_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m3090697542_gp_0_0_0_0_Array_Sort_m3090697542_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m3090697542_gp_0_0_0_0_Array_Sort_m3090697542_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m3597357548_gp_0_0_0_0_Types[] = { (&Array_Sort_m3597357548_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m3597357548_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m3597357548_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m3597357548_gp_0_0_0_0_Array_Sort_m3597357548_gp_1_0_0_0_Types[] = { (&Array_Sort_m3597357548_gp_0_0_0_0), (&Array_Sort_m3597357548_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m3597357548_gp_0_0_0_0_Array_Sort_m3597357548_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m3597357548_gp_0_0_0_0_Array_Sort_m3597357548_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m1886451243_gp_0_0_0_0_Array_Sort_m1886451243_gp_0_0_0_0_Types[] = { (&Array_Sort_m1886451243_gp_0_0_0_0), (&Array_Sort_m1886451243_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m1886451243_gp_0_0_0_0_Array_Sort_m1886451243_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m1886451243_gp_0_0_0_0_Array_Sort_m1886451243_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m1562643584_gp_0_0_0_0_Array_Sort_m1562643584_gp_1_0_0_0_Types[] = { (&Array_Sort_m1562643584_gp_0_0_0_0), (&Array_Sort_m1562643584_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m1562643584_gp_0_0_0_0_Array_Sort_m1562643584_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m1562643584_gp_0_0_0_0_Array_Sort_m1562643584_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m2579296346_gp_0_0_0_0_Types[] = { (&Array_Sort_m2579296346_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m2579296346_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m2579296346_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m2579296346_gp_0_0_0_0_Array_Sort_m2579296346_gp_0_0_0_0_Types[] = { (&Array_Sort_m2579296346_gp_0_0_0_0), (&Array_Sort_m2579296346_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m2579296346_gp_0_0_0_0_Array_Sort_m2579296346_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m2579296346_gp_0_0_0_0_Array_Sort_m2579296346_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m3476310768_gp_0_0_0_0_Types[] = { (&Array_Sort_m3476310768_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m3476310768_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m3476310768_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m3476310768_gp_1_0_0_0_Types[] = { (&Array_Sort_m3476310768_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m3476310768_gp_1_0_0_0 = { 1, GenInst_Array_Sort_m3476310768_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m3476310768_gp_0_0_0_0_Array_Sort_m3476310768_gp_1_0_0_0_Types[] = { (&Array_Sort_m3476310768_gp_0_0_0_0), (&Array_Sort_m3476310768_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m3476310768_gp_0_0_0_0_Array_Sort_m3476310768_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m3476310768_gp_0_0_0_0_Array_Sort_m3476310768_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m3187056504_gp_0_0_0_0_Types[] = { (&Array_Sort_m3187056504_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m3187056504_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m3187056504_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m1246393659_gp_0_0_0_0_Types[] = { (&Array_Sort_m1246393659_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m1246393659_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m1246393659_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_qsort_m1307391834_gp_0_0_0_0_Types[] = { (&Array_qsort_m1307391834_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_qsort_m1307391834_gp_0_0_0_0 = { 1, GenInst_Array_qsort_m1307391834_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_qsort_m1307391834_gp_0_0_0_0_Array_qsort_m1307391834_gp_1_0_0_0_Types[] = { (&Array_qsort_m1307391834_gp_0_0_0_0), (&Array_qsort_m1307391834_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_qsort_m1307391834_gp_0_0_0_0_Array_qsort_m1307391834_gp_1_0_0_0 = { 2, GenInst_Array_qsort_m1307391834_gp_0_0_0_0_Array_qsort_m1307391834_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_Array_compare_m3469659333_gp_0_0_0_0_Types[] = { (&Array_compare_m3469659333_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_compare_m3469659333_gp_0_0_0_0 = { 1, GenInst_Array_compare_m3469659333_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_qsort_m172816524_gp_0_0_0_0_Types[] = { (&Array_qsort_m172816524_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_qsort_m172816524_gp_0_0_0_0 = { 1, GenInst_Array_qsort_m172816524_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_Resize_m3981285257_gp_0_0_0_0_Types[] = { (&Array_Resize_m3981285257_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Resize_m3981285257_gp_0_0_0_0 = { 1, GenInst_Array_Resize_m3981285257_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_TrueForAll_m1661083019_gp_0_0_0_0_Types[] = { (&Array_TrueForAll_m1661083019_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_TrueForAll_m1661083019_gp_0_0_0_0 = { 1, GenInst_Array_TrueForAll_m1661083019_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_ForEach_m753603380_gp_0_0_0_0_Types[] = { (&Array_ForEach_m753603380_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_ForEach_m753603380_gp_0_0_0_0 = { 1, GenInst_Array_ForEach_m753603380_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_ConvertAll_m2612536962_gp_0_0_0_0_Array_ConvertAll_m2612536962_gp_1_0_0_0_Types[] = { (&Array_ConvertAll_m2612536962_gp_0_0_0_0), (&Array_ConvertAll_m2612536962_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_ConvertAll_m2612536962_gp_0_0_0_0_Array_ConvertAll_m2612536962_gp_1_0_0_0 = { 2, GenInst_Array_ConvertAll_m2612536962_gp_0_0_0_0_Array_ConvertAll_m2612536962_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_Array_FindLastIndex_m1512572791_gp_0_0_0_0_Types[] = { (&Array_FindLastIndex_m1512572791_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_FindLastIndex_m1512572791_gp_0_0_0_0 = { 1, GenInst_Array_FindLastIndex_m1512572791_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_FindLastIndex_m1228596251_gp_0_0_0_0_Types[] = { (&Array_FindLastIndex_m1228596251_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_FindLastIndex_m1228596251_gp_0_0_0_0 = { 1, GenInst_Array_FindLastIndex_m1228596251_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_FindLastIndex_m3711907002_gp_0_0_0_0_Types[] = { (&Array_FindLastIndex_m3711907002_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_FindLastIndex_m3711907002_gp_0_0_0_0 = { 1, GenInst_Array_FindLastIndex_m3711907002_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_FindIndex_m2893558283_gp_0_0_0_0_Types[] = { (&Array_FindIndex_m2893558283_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_FindIndex_m2893558283_gp_0_0_0_0 = { 1, GenInst_Array_FindIndex_m2893558283_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_FindIndex_m3648911080_gp_0_0_0_0_Types[] = { (&Array_FindIndex_m3648911080_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_FindIndex_m3648911080_gp_0_0_0_0 = { 1, GenInst_Array_FindIndex_m3648911080_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_FindIndex_m3595353763_gp_0_0_0_0_Types[] = { (&Array_FindIndex_m3595353763_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_FindIndex_m3595353763_gp_0_0_0_0 = { 1, GenInst_Array_FindIndex_m3595353763_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_BinarySearch_m4248949575_gp_0_0_0_0_Types[] = { (&Array_BinarySearch_m4248949575_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m4248949575_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m4248949575_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_BinarySearch_m1974805269_gp_0_0_0_0_Types[] = { (&Array_BinarySearch_m1974805269_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m1974805269_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m1974805269_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_BinarySearch_m3819272786_gp_0_0_0_0_Types[] = { (&Array_BinarySearch_m3819272786_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m3819272786_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m3819272786_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_BinarySearch_m1130814323_gp_0_0_0_0_Types[] = { (&Array_BinarySearch_m1130814323_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m1130814323_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m1130814323_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_IndexOf_m2441478810_gp_0_0_0_0_Types[] = { (&Array_IndexOf_m2441478810_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_IndexOf_m2441478810_gp_0_0_0_0 = { 1, GenInst_Array_IndexOf_m2441478810_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_IndexOf_m4207036999_gp_0_0_0_0_Types[] = { (&Array_IndexOf_m4207036999_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_IndexOf_m4207036999_gp_0_0_0_0 = { 1, GenInst_Array_IndexOf_m4207036999_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_IndexOf_m3455639349_gp_0_0_0_0_Types[] = { (&Array_IndexOf_m3455639349_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_IndexOf_m3455639349_gp_0_0_0_0 = { 1, GenInst_Array_IndexOf_m3455639349_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_LastIndexOf_m2181488816_gp_0_0_0_0_Types[] = { (&Array_LastIndexOf_m2181488816_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_LastIndexOf_m2181488816_gp_0_0_0_0 = { 1, GenInst_Array_LastIndexOf_m2181488816_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_LastIndexOf_m4262341566_gp_0_0_0_0_Types[] = { (&Array_LastIndexOf_m4262341566_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_LastIndexOf_m4262341566_gp_0_0_0_0 = { 1, GenInst_Array_LastIndexOf_m4262341566_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_LastIndexOf_m395810534_gp_0_0_0_0_Types[] = { (&Array_LastIndexOf_m395810534_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_LastIndexOf_m395810534_gp_0_0_0_0 = { 1, GenInst_Array_LastIndexOf_m395810534_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_FindAll_m1792123350_gp_0_0_0_0_Types[] = { (&Array_FindAll_m1792123350_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_FindAll_m1792123350_gp_0_0_0_0 = { 1, GenInst_Array_FindAll_m1792123350_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_Exists_m4188561929_gp_0_0_0_0_Types[] = { (&Array_Exists_m4188561929_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Exists_m4188561929_gp_0_0_0_0 = { 1, GenInst_Array_Exists_m4188561929_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_AsReadOnly_m547237041_gp_0_0_0_0_Types[] = { (&Array_AsReadOnly_m547237041_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_AsReadOnly_m547237041_gp_0_0_0_0 = { 1, GenInst_Array_AsReadOnly_m547237041_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_Find_m4070178886_gp_0_0_0_0_Types[] = { (&Array_Find_m4070178886_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Find_m4070178886_gp_0_0_0_0 = { 1, GenInst_Array_Find_m4070178886_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_FindLast_m3836785593_gp_0_0_0_0_Types[] = { (&Array_FindLast_m3836785593_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_FindLast_m3836785593_gp_0_0_0_0 = { 1, GenInst_Array_FindLast_m3836785593_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_InternalEnumerator_1_t2351989714_gp_0_0_0_0_Types[] = { (&InternalEnumerator_1_t2351989714_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_InternalEnumerator_1_t2351989714_gp_0_0_0_0 = { 1, GenInst_InternalEnumerator_1_t2351989714_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_ArrayReadOnlyList_1_t3002341623_gp_0_0_0_0_Types[] = { (&ArrayReadOnlyList_1_t3002341623_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_ArrayReadOnlyList_1_t3002341623_gp_0_0_0_0 = { 1, GenInst_ArrayReadOnlyList_1_t3002341623_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_U3CGetEnumeratorU3Ec__Iterator0_t4287845664_gp_0_0_0_0_Types[] = { (&U3CGetEnumeratorU3Ec__Iterator0_t4287845664_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_U3CGetEnumeratorU3Ec__Iterator0_t4287845664_gp_0_0_0_0 = { 1, GenInst_U3CGetEnumeratorU3Ec__Iterator0_t4287845664_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_IList_1_t2409468953_gp_0_0_0_0_Types[] = { (&IList_1_t2409468953_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_IList_1_t2409468953_gp_0_0_0_0 = { 1, GenInst_IList_1_t2409468953_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_ICollection_1_t2720146454_gp_0_0_0_0_Types[] = { (&ICollection_1_t2720146454_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_ICollection_1_t2720146454_gp_0_0_0_0 = { 1, GenInst_ICollection_1_t2720146454_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Nullable_1_t2413029061_gp_0_0_0_0_Types[] = { (&Nullable_1_t2413029061_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Nullable_1_t2413029061_gp_0_0_0_0 = { 1, GenInst_Nullable_1_t2413029061_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Comparer_1_t1619863495_gp_0_0_0_0_Types[] = { (&Comparer_1_t1619863495_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Comparer_1_t1619863495_gp_0_0_0_0 = { 1, GenInst_Comparer_1_t1619863495_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_DefaultComparer_t3604220391_gp_0_0_0_0_Types[] = { (&DefaultComparer_t3604220391_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_DefaultComparer_t3604220391_gp_0_0_0_0 = { 1, GenInst_DefaultComparer_t3604220391_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_GenericComparer_1_t769309037_gp_0_0_0_0_Types[] = { (&GenericComparer_1_t769309037_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_GenericComparer_1_t769309037_gp_0_0_0_0 = { 1, GenInst_GenericComparer_1_t769309037_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Dictionary_2_t1922834018_gp_0_0_0_0_Types[] = { (&Dictionary_2_t1922834018_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Dictionary_2_t1922834018_gp_0_0_0_0 = { 1, GenInst_Dictionary_2_t1922834018_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Dictionary_2_t1922834018_gp_0_0_0_0_Dictionary_2_t1922834018_gp_1_0_0_0_Types[] = { (&Dictionary_2_t1922834018_gp_0_0_0_0), (&Dictionary_2_t1922834018_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Dictionary_2_t1922834018_gp_0_0_0_0_Dictionary_2_t1922834018_gp_1_0_0_0 = { 2, GenInst_Dictionary_2_t1922834018_gp_0_0_0_0_Dictionary_2_t1922834018_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t2834118370_0_0_0_Types[] = { (&KeyValuePair_2_t2834118370_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2834118370_0_0_0 = { 1, GenInst_KeyValuePair_2_t2834118370_0_0_0_Types };
static const RuntimeType* GenInst_Dictionary_2_t1922834018_gp_0_0_0_0_Dictionary_2_t1922834018_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m1120866649_gp_0_0_0_0_Types[] = { (&Dictionary_2_t1922834018_gp_0_0_0_0), (&Dictionary_2_t1922834018_gp_1_0_0_0), (&Dictionary_2_Do_CopyTo_m1120866649_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Dictionary_2_t1922834018_gp_0_0_0_0_Dictionary_2_t1922834018_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m1120866649_gp_0_0_0_0 = { 3, GenInst_Dictionary_2_t1922834018_gp_0_0_0_0_Dictionary_2_t1922834018_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m1120866649_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Dictionary_2_t1922834018_gp_0_0_0_0_Dictionary_2_t1922834018_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m3964247330_gp_0_0_0_0_Types[] = { (&Dictionary_2_t1922834018_gp_0_0_0_0), (&Dictionary_2_t1922834018_gp_1_0_0_0), (&Dictionary_2_Do_ICollectionCopyTo_m3964247330_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Dictionary_2_t1922834018_gp_0_0_0_0_Dictionary_2_t1922834018_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m3964247330_gp_0_0_0_0 = { 3, GenInst_Dictionary_2_t1922834018_gp_0_0_0_0_Dictionary_2_t1922834018_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m3964247330_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Dictionary_2_Do_ICollectionCopyTo_m3964247330_gp_0_0_0_0_RuntimeObject_0_0_0_Types[] = { (&Dictionary_2_Do_ICollectionCopyTo_m3964247330_gp_0_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_Dictionary_2_Do_ICollectionCopyTo_m3964247330_gp_0_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_Dictionary_2_Do_ICollectionCopyTo_m3964247330_gp_0_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_Dictionary_2_t1922834018_gp_0_0_0_0_Dictionary_2_t1922834018_gp_1_0_0_0_DictionaryEntry_t1385909157_0_0_0_Types[] = { (&Dictionary_2_t1922834018_gp_0_0_0_0), (&Dictionary_2_t1922834018_gp_1_0_0_0), (&DictionaryEntry_t1385909157_0_0_0) };
extern const Il2CppGenericInst GenInst_Dictionary_2_t1922834018_gp_0_0_0_0_Dictionary_2_t1922834018_gp_1_0_0_0_DictionaryEntry_t1385909157_0_0_0 = { 3, GenInst_Dictionary_2_t1922834018_gp_0_0_0_0_Dictionary_2_t1922834018_gp_1_0_0_0_DictionaryEntry_t1385909157_0_0_0_Types };
static const RuntimeType* GenInst_ShimEnumerator_t2216024862_gp_0_0_0_0_ShimEnumerator_t2216024862_gp_1_0_0_0_Types[] = { (&ShimEnumerator_t2216024862_gp_0_0_0_0), (&ShimEnumerator_t2216024862_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_ShimEnumerator_t2216024862_gp_0_0_0_0_ShimEnumerator_t2216024862_gp_1_0_0_0 = { 2, GenInst_ShimEnumerator_t2216024862_gp_0_0_0_0_ShimEnumerator_t2216024862_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_Enumerator_t601853994_gp_0_0_0_0_Enumerator_t601853994_gp_1_0_0_0_Types[] = { (&Enumerator_t601853994_gp_0_0_0_0), (&Enumerator_t601853994_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerator_t601853994_gp_0_0_0_0_Enumerator_t601853994_gp_1_0_0_0 = { 2, GenInst_Enumerator_t601853994_gp_0_0_0_0_Enumerator_t601853994_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t2855505602_0_0_0_Types[] = { (&KeyValuePair_2_t2855505602_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2855505602_0_0_0 = { 1, GenInst_KeyValuePair_2_t2855505602_0_0_0_Types };
static const RuntimeType* GenInst_DictionaryEntry_t1385909157_0_0_0_DictionaryEntry_t1385909157_0_0_0_Types[] = { (&DictionaryEntry_t1385909157_0_0_0), (&DictionaryEntry_t1385909157_0_0_0) };
extern const Il2CppGenericInst GenInst_DictionaryEntry_t1385909157_0_0_0_DictionaryEntry_t1385909157_0_0_0 = { 2, GenInst_DictionaryEntry_t1385909157_0_0_0_DictionaryEntry_t1385909157_0_0_0_Types };
static const RuntimeType* GenInst_Dictionary_2_t1922834018_gp_0_0_0_0_Dictionary_2_t1922834018_gp_1_0_0_0_KeyValuePair_2_t2834118370_0_0_0_Types[] = { (&Dictionary_2_t1922834018_gp_0_0_0_0), (&Dictionary_2_t1922834018_gp_1_0_0_0), (&KeyValuePair_2_t2834118370_0_0_0) };
extern const Il2CppGenericInst GenInst_Dictionary_2_t1922834018_gp_0_0_0_0_Dictionary_2_t1922834018_gp_1_0_0_0_KeyValuePair_2_t2834118370_0_0_0 = { 3, GenInst_Dictionary_2_t1922834018_gp_0_0_0_0_Dictionary_2_t1922834018_gp_1_0_0_0_KeyValuePair_2_t2834118370_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t2834118370_0_0_0_KeyValuePair_2_t2834118370_0_0_0_Types[] = { (&KeyValuePair_2_t2834118370_0_0_0), (&KeyValuePair_2_t2834118370_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2834118370_0_0_0_KeyValuePair_2_t2834118370_0_0_0 = { 2, GenInst_KeyValuePair_2_t2834118370_0_0_0_KeyValuePair_2_t2834118370_0_0_0_Types };
static const RuntimeType* GenInst_Dictionary_2_t1922834018_gp_1_0_0_0_Types[] = { (&Dictionary_2_t1922834018_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Dictionary_2_t1922834018_gp_1_0_0_0 = { 1, GenInst_Dictionary_2_t1922834018_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_EqualityComparer_1_t2656680762_gp_0_0_0_0_Types[] = { (&EqualityComparer_1_t2656680762_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_EqualityComparer_1_t2656680762_gp_0_0_0_0 = { 1, GenInst_EqualityComparer_1_t2656680762_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_DefaultComparer_t866827805_gp_0_0_0_0_Types[] = { (&DefaultComparer_t866827805_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_DefaultComparer_t866827805_gp_0_0_0_0 = { 1, GenInst_DefaultComparer_t866827805_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_GenericEqualityComparer_1_t2573898200_gp_0_0_0_0_Types[] = { (&GenericEqualityComparer_1_t2573898200_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_GenericEqualityComparer_1_t2573898200_gp_0_0_0_0 = { 1, GenInst_GenericEqualityComparer_1_t2573898200_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t4172885178_0_0_0_Types[] = { (&KeyValuePair_2_t4172885178_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4172885178_0_0_0 = { 1, GenInst_KeyValuePair_2_t4172885178_0_0_0_Types };
static const RuntimeType* GenInst_IDictionary_2_t3413473948_gp_0_0_0_0_IDictionary_2_t3413473948_gp_1_0_0_0_Types[] = { (&IDictionary_2_t3413473948_gp_0_0_0_0), (&IDictionary_2_t3413473948_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_IDictionary_2_t3413473948_gp_0_0_0_0_IDictionary_2_t3413473948_gp_1_0_0_0 = { 2, GenInst_IDictionary_2_t3413473948_gp_0_0_0_0_IDictionary_2_t3413473948_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t776898938_gp_0_0_0_0_KeyValuePair_2_t776898938_gp_1_0_0_0_Types[] = { (&KeyValuePair_2_t776898938_gp_0_0_0_0), (&KeyValuePair_2_t776898938_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t776898938_gp_0_0_0_0_KeyValuePair_2_t776898938_gp_1_0_0_0 = { 2, GenInst_KeyValuePair_2_t776898938_gp_0_0_0_0_KeyValuePair_2_t776898938_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t82844879_gp_0_0_0_0_Types[] = { (&List_1_t82844879_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t82844879_gp_0_0_0_0 = { 1, GenInst_List_1_t82844879_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Enumerator_t2210905544_gp_0_0_0_0_Types[] = { (&Enumerator_t2210905544_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerator_t2210905544_gp_0_0_0_0 = { 1, GenInst_Enumerator_t2210905544_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Collection_1_t3543120677_gp_0_0_0_0_Types[] = { (&Collection_1_t3543120677_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Collection_1_t3543120677_gp_0_0_0_0 = { 1, GenInst_Collection_1_t3543120677_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_ReadOnlyCollection_1_t891526187_gp_0_0_0_0_Types[] = { (&ReadOnlyCollection_1_t891526187_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_ReadOnlyCollection_1_t891526187_gp_0_0_0_0 = { 1, GenInst_ReadOnlyCollection_1_t891526187_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_MonoProperty_GetterAdapterFrame_m880231002_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m880231002_gp_1_0_0_0_Types[] = { (&MonoProperty_GetterAdapterFrame_m880231002_gp_0_0_0_0), (&MonoProperty_GetterAdapterFrame_m880231002_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_MonoProperty_GetterAdapterFrame_m880231002_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m880231002_gp_1_0_0_0 = { 2, GenInst_MonoProperty_GetterAdapterFrame_m880231002_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m880231002_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_MonoProperty_StaticGetterAdapterFrame_m944017034_gp_0_0_0_0_Types[] = { (&MonoProperty_StaticGetterAdapterFrame_m944017034_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_MonoProperty_StaticGetterAdapterFrame_m944017034_gp_0_0_0_0 = { 1, GenInst_MonoProperty_StaticGetterAdapterFrame_m944017034_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Queue_1_t542587364_gp_0_0_0_0_Types[] = { (&Queue_1_t542587364_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Queue_1_t542587364_gp_0_0_0_0 = { 1, GenInst_Queue_1_t542587364_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Enumerator_t3782511927_gp_0_0_0_0_Types[] = { (&Enumerator_t3782511927_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerator_t3782511927_gp_0_0_0_0 = { 1, GenInst_Enumerator_t3782511927_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Stack_1_t2711020695_gp_0_0_0_0_Types[] = { (&Stack_1_t2711020695_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Stack_1_t2711020695_gp_0_0_0_0 = { 1, GenInst_Stack_1_t2711020695_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Enumerator_t548431029_gp_0_0_0_0_Types[] = { (&Enumerator_t548431029_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerator_t548431029_gp_0_0_0_0 = { 1, GenInst_Enumerator_t548431029_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Enumerable_Any_m1770862959_gp_0_0_0_0_Types[] = { (&Enumerable_Any_m1770862959_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerable_Any_m1770862959_gp_0_0_0_0 = { 1, GenInst_Enumerable_Any_m1770862959_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Enumerable_Where_m2049181036_gp_0_0_0_0_Types[] = { (&Enumerable_Where_m2049181036_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerable_Where_m2049181036_gp_0_0_0_0 = { 1, GenInst_Enumerable_Where_m2049181036_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Enumerable_Where_m2049181036_gp_0_0_0_0_Boolean_t2059672899_0_0_0_Types[] = { (&Enumerable_Where_m2049181036_gp_0_0_0_0), (&Boolean_t2059672899_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerable_Where_m2049181036_gp_0_0_0_0_Boolean_t2059672899_0_0_0 = { 2, GenInst_Enumerable_Where_m2049181036_gp_0_0_0_0_Boolean_t2059672899_0_0_0_Types };
static const RuntimeType* GenInst_Enumerable_CreateWhereIterator_m1295165955_gp_0_0_0_0_Types[] = { (&Enumerable_CreateWhereIterator_m1295165955_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerable_CreateWhereIterator_m1295165955_gp_0_0_0_0 = { 1, GenInst_Enumerable_CreateWhereIterator_m1295165955_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Enumerable_CreateWhereIterator_m1295165955_gp_0_0_0_0_Boolean_t2059672899_0_0_0_Types[] = { (&Enumerable_CreateWhereIterator_m1295165955_gp_0_0_0_0), (&Boolean_t2059672899_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerable_CreateWhereIterator_m1295165955_gp_0_0_0_0_Boolean_t2059672899_0_0_0 = { 2, GenInst_Enumerable_CreateWhereIterator_m1295165955_gp_0_0_0_0_Boolean_t2059672899_0_0_0_Types };
static const RuntimeType* GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1910290221_gp_0_0_0_0_Types[] = { (&U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1910290221_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1910290221_gp_0_0_0_0 = { 1, GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1910290221_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1910290221_gp_0_0_0_0_Boolean_t2059672899_0_0_0_Types[] = { (&U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1910290221_gp_0_0_0_0), (&Boolean_t2059672899_0_0_0) };
extern const Il2CppGenericInst GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1910290221_gp_0_0_0_0_Boolean_t2059672899_0_0_0 = { 2, GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1910290221_gp_0_0_0_0_Boolean_t2059672899_0_0_0_Types };
static const RuntimeType* GenInst_InvokableCall_1_t1604090555_gp_0_0_0_0_Types[] = { (&InvokableCall_1_t1604090555_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_InvokableCall_1_t1604090555_gp_0_0_0_0 = { 1, GenInst_InvokableCall_1_t1604090555_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_UnityAction_1_t3885734668_0_0_0_Types[] = { (&UnityAction_1_t3885734668_0_0_0) };
extern const Il2CppGenericInst GenInst_UnityAction_1_t3885734668_0_0_0 = { 1, GenInst_UnityAction_1_t3885734668_0_0_0_Types };
static const RuntimeType* GenInst_InvokableCall_2_t3976188983_gp_0_0_0_0_InvokableCall_2_t3976188983_gp_1_0_0_0_Types[] = { (&InvokableCall_2_t3976188983_gp_0_0_0_0), (&InvokableCall_2_t3976188983_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_InvokableCall_2_t3976188983_gp_0_0_0_0_InvokableCall_2_t3976188983_gp_1_0_0_0 = { 2, GenInst_InvokableCall_2_t3976188983_gp_0_0_0_0_InvokableCall_2_t3976188983_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_InvokableCall_2_t3976188983_gp_0_0_0_0_Types[] = { (&InvokableCall_2_t3976188983_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_InvokableCall_2_t3976188983_gp_0_0_0_0 = { 1, GenInst_InvokableCall_2_t3976188983_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_InvokableCall_2_t3976188983_gp_1_0_0_0_Types[] = { (&InvokableCall_2_t3976188983_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_InvokableCall_2_t3976188983_gp_1_0_0_0 = { 1, GenInst_InvokableCall_2_t3976188983_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_InvokableCall_3_t3917817895_gp_0_0_0_0_InvokableCall_3_t3917817895_gp_1_0_0_0_InvokableCall_3_t3917817895_gp_2_0_0_0_Types[] = { (&InvokableCall_3_t3917817895_gp_0_0_0_0), (&InvokableCall_3_t3917817895_gp_1_0_0_0), (&InvokableCall_3_t3917817895_gp_2_0_0_0) };
extern const Il2CppGenericInst GenInst_InvokableCall_3_t3917817895_gp_0_0_0_0_InvokableCall_3_t3917817895_gp_1_0_0_0_InvokableCall_3_t3917817895_gp_2_0_0_0 = { 3, GenInst_InvokableCall_3_t3917817895_gp_0_0_0_0_InvokableCall_3_t3917817895_gp_1_0_0_0_InvokableCall_3_t3917817895_gp_2_0_0_0_Types };
static const RuntimeType* GenInst_InvokableCall_3_t3917817895_gp_0_0_0_0_Types[] = { (&InvokableCall_3_t3917817895_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_InvokableCall_3_t3917817895_gp_0_0_0_0 = { 1, GenInst_InvokableCall_3_t3917817895_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_InvokableCall_3_t3917817895_gp_1_0_0_0_Types[] = { (&InvokableCall_3_t3917817895_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_InvokableCall_3_t3917817895_gp_1_0_0_0 = { 1, GenInst_InvokableCall_3_t3917817895_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_InvokableCall_3_t3917817895_gp_2_0_0_0_Types[] = { (&InvokableCall_3_t3917817895_gp_2_0_0_0) };
extern const Il2CppGenericInst GenInst_InvokableCall_3_t3917817895_gp_2_0_0_0 = { 1, GenInst_InvokableCall_3_t3917817895_gp_2_0_0_0_Types };
static const RuntimeType* GenInst_InvokableCall_4_t2387264047_gp_0_0_0_0_InvokableCall_4_t2387264047_gp_1_0_0_0_InvokableCall_4_t2387264047_gp_2_0_0_0_InvokableCall_4_t2387264047_gp_3_0_0_0_Types[] = { (&InvokableCall_4_t2387264047_gp_0_0_0_0), (&InvokableCall_4_t2387264047_gp_1_0_0_0), (&InvokableCall_4_t2387264047_gp_2_0_0_0), (&InvokableCall_4_t2387264047_gp_3_0_0_0) };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t2387264047_gp_0_0_0_0_InvokableCall_4_t2387264047_gp_1_0_0_0_InvokableCall_4_t2387264047_gp_2_0_0_0_InvokableCall_4_t2387264047_gp_3_0_0_0 = { 4, GenInst_InvokableCall_4_t2387264047_gp_0_0_0_0_InvokableCall_4_t2387264047_gp_1_0_0_0_InvokableCall_4_t2387264047_gp_2_0_0_0_InvokableCall_4_t2387264047_gp_3_0_0_0_Types };
static const RuntimeType* GenInst_InvokableCall_4_t2387264047_gp_0_0_0_0_Types[] = { (&InvokableCall_4_t2387264047_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t2387264047_gp_0_0_0_0 = { 1, GenInst_InvokableCall_4_t2387264047_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_InvokableCall_4_t2387264047_gp_1_0_0_0_Types[] = { (&InvokableCall_4_t2387264047_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t2387264047_gp_1_0_0_0 = { 1, GenInst_InvokableCall_4_t2387264047_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_InvokableCall_4_t2387264047_gp_2_0_0_0_Types[] = { (&InvokableCall_4_t2387264047_gp_2_0_0_0) };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t2387264047_gp_2_0_0_0 = { 1, GenInst_InvokableCall_4_t2387264047_gp_2_0_0_0_Types };
static const RuntimeType* GenInst_InvokableCall_4_t2387264047_gp_3_0_0_0_Types[] = { (&InvokableCall_4_t2387264047_gp_3_0_0_0) };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t2387264047_gp_3_0_0_0 = { 1, GenInst_InvokableCall_4_t2387264047_gp_3_0_0_0_Types };
static const RuntimeType* GenInst_CachedInvokableCall_1_t4137603102_gp_0_0_0_0_Types[] = { (&CachedInvokableCall_1_t4137603102_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_CachedInvokableCall_1_t4137603102_gp_0_0_0_0 = { 1, GenInst_CachedInvokableCall_1_t4137603102_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_UnityEvent_1_t4023850743_gp_0_0_0_0_Types[] = { (&UnityEvent_1_t4023850743_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_UnityEvent_1_t4023850743_gp_0_0_0_0 = { 1, GenInst_UnityEvent_1_t4023850743_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_UnityEvent_2_t1533051435_gp_0_0_0_0_UnityEvent_2_t1533051435_gp_1_0_0_0_Types[] = { (&UnityEvent_2_t1533051435_gp_0_0_0_0), (&UnityEvent_2_t1533051435_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_UnityEvent_2_t1533051435_gp_0_0_0_0_UnityEvent_2_t1533051435_gp_1_0_0_0 = { 2, GenInst_UnityEvent_2_t1533051435_gp_0_0_0_0_UnityEvent_2_t1533051435_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_UnityEvent_3_t4091795572_gp_0_0_0_0_UnityEvent_3_t4091795572_gp_1_0_0_0_UnityEvent_3_t4091795572_gp_2_0_0_0_Types[] = { (&UnityEvent_3_t4091795572_gp_0_0_0_0), (&UnityEvent_3_t4091795572_gp_1_0_0_0), (&UnityEvent_3_t4091795572_gp_2_0_0_0) };
extern const Il2CppGenericInst GenInst_UnityEvent_3_t4091795572_gp_0_0_0_0_UnityEvent_3_t4091795572_gp_1_0_0_0_UnityEvent_3_t4091795572_gp_2_0_0_0 = { 3, GenInst_UnityEvent_3_t4091795572_gp_0_0_0_0_UnityEvent_3_t4091795572_gp_1_0_0_0_UnityEvent_3_t4091795572_gp_2_0_0_0_Types };
static const RuntimeType* GenInst_UnityEvent_4_t1002767761_gp_0_0_0_0_UnityEvent_4_t1002767761_gp_1_0_0_0_UnityEvent_4_t1002767761_gp_2_0_0_0_UnityEvent_4_t1002767761_gp_3_0_0_0_Types[] = { (&UnityEvent_4_t1002767761_gp_0_0_0_0), (&UnityEvent_4_t1002767761_gp_1_0_0_0), (&UnityEvent_4_t1002767761_gp_2_0_0_0), (&UnityEvent_4_t1002767761_gp_3_0_0_0) };
extern const Il2CppGenericInst GenInst_UnityEvent_4_t1002767761_gp_0_0_0_0_UnityEvent_4_t1002767761_gp_1_0_0_0_UnityEvent_4_t1002767761_gp_2_0_0_0_UnityEvent_4_t1002767761_gp_3_0_0_0 = { 4, GenInst_UnityEvent_4_t1002767761_gp_0_0_0_0_UnityEvent_4_t1002767761_gp_1_0_0_0_UnityEvent_4_t1002767761_gp_2_0_0_0_UnityEvent_4_t1002767761_gp_3_0_0_0_Types };
static const RuntimeType* GenInst_DefaultExecutionOrder_t3862449190_0_0_0_Types[] = { (&DefaultExecutionOrder_t3862449190_0_0_0) };
extern const Il2CppGenericInst GenInst_DefaultExecutionOrder_t3862449190_0_0_0 = { 1, GenInst_DefaultExecutionOrder_t3862449190_0_0_0_Types };
static const RuntimeType* GenInst_PlayerConnection_t3559457672_0_0_0_Types[] = { (&PlayerConnection_t3559457672_0_0_0) };
extern const Il2CppGenericInst GenInst_PlayerConnection_t3559457672_0_0_0 = { 1, GenInst_PlayerConnection_t3559457672_0_0_0_Types };
static const RuntimeType* GenInst_GUILayer_t1929752731_0_0_0_Types[] = { (&GUILayer_t1929752731_0_0_0) };
extern const Il2CppGenericInst GenInst_GUILayer_t1929752731_0_0_0 = { 1, GenInst_GUILayer_t1929752731_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t2204764377_0_0_0_KeyValuePair_2_t2204764377_0_0_0_Types[] = { (&KeyValuePair_2_t2204764377_0_0_0), (&KeyValuePair_2_t2204764377_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2204764377_0_0_0_KeyValuePair_2_t2204764377_0_0_0 = { 2, GenInst_KeyValuePair_2_t2204764377_0_0_0_KeyValuePair_2_t2204764377_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t2204764377_0_0_0_RuntimeObject_0_0_0_Types[] = { (&KeyValuePair_2_t2204764377_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2204764377_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t2204764377_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t2366093889_0_0_0_KeyValuePair_2_t2366093889_0_0_0_Types[] = { (&KeyValuePair_2_t2366093889_0_0_0), (&KeyValuePair_2_t2366093889_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2366093889_0_0_0_KeyValuePair_2_t2366093889_0_0_0 = { 2, GenInst_KeyValuePair_2_t2366093889_0_0_0_KeyValuePair_2_t2366093889_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t2366093889_0_0_0_RuntimeObject_0_0_0_Types[] = { (&KeyValuePair_2_t2366093889_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2366093889_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t2366093889_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t2877556189_0_0_0_KeyValuePair_2_t2877556189_0_0_0_Types[] = { (&KeyValuePair_2_t2877556189_0_0_0), (&KeyValuePair_2_t2877556189_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2877556189_0_0_0_KeyValuePair_2_t2877556189_0_0_0 = { 2, GenInst_KeyValuePair_2_t2877556189_0_0_0_KeyValuePair_2_t2877556189_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t2877556189_0_0_0_RuntimeObject_0_0_0_Types[] = { (&KeyValuePair_2_t2877556189_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2877556189_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t2877556189_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3602437993_0_0_0_KeyValuePair_2_t3602437993_0_0_0_Types[] = { (&KeyValuePair_2_t3602437993_0_0_0), (&KeyValuePair_2_t3602437993_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3602437993_0_0_0_KeyValuePair_2_t3602437993_0_0_0 = { 2, GenInst_KeyValuePair_2_t3602437993_0_0_0_KeyValuePair_2_t3602437993_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3602437993_0_0_0_RuntimeObject_0_0_0_Types[] = { (&KeyValuePair_2_t3602437993_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3602437993_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t3602437993_0_0_0_RuntimeObject_0_0_0_Types };
extern const Il2CppGenericInst* const g_Il2CppGenericInstTable[345] = 
{
	&GenInst_RuntimeObject_0_0_0,
	&GenInst_Int32_t2571135199_0_0_0,
	&GenInst_Char_t3572527572_0_0_0,
	&GenInst_Int64_t384086013_0_0_0,
	&GenInst_UInt32_t2739056616_0_0_0,
	&GenInst_UInt64_t2265270229_0_0_0,
	&GenInst_Byte_t880488320_0_0_0,
	&GenInst_SByte_t2530277047_0_0_0,
	&GenInst_Int16_t769030278_0_0_0,
	&GenInst_UInt16_t11709673_0_0_0,
	&GenInst_String_t_0_0_0,
	&GenInst_IConvertible_t1224795492_0_0_0,
	&GenInst_IComparable_t63485110_0_0_0,
	&GenInst_IEnumerable_t1872631827_0_0_0,
	&GenInst_ICloneable_t3230708396_0_0_0,
	&GenInst_IComparable_1_t2260689833_0_0_0,
	&GenInst_IEquatable_1_t1012708249_0_0_0,
	&GenInst_Type_t_0_0_0,
	&GenInst_IReflect_t4184883566_0_0_0,
	&GenInst__Type_t2913350559_0_0_0,
	&GenInst_MemberInfo_t_0_0_0,
	&GenInst_ICustomAttributeProvider_t4054400477_0_0_0,
	&GenInst__MemberInfo_t1659175437_0_0_0,
	&GenInst_Single_t496865882_0_0_0,
	&GenInst_Double_t1454265465_0_0_0,
	&GenInst_Decimal_t2656901032_0_0_0,
	&GenInst_Boolean_t2059672899_0_0_0,
	&GenInst_Delegate_t96267039_0_0_0,
	&GenInst_ISerializable_t631501906_0_0_0,
	&GenInst_ParameterInfo_t3893964560_0_0_0,
	&GenInst__ParameterInfo_t3152035282_0_0_0,
	&GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0,
	&GenInst_FieldInfo_t_0_0_0,
	&GenInst__FieldInfo_t3281021298_0_0_0,
	&GenInst_ParameterModifier_t969812002_0_0_0,
	&GenInst_MethodInfo_t_0_0_0,
	&GenInst__MethodInfo_t643827776_0_0_0,
	&GenInst_MethodBase_t674153939_0_0_0,
	&GenInst__MethodBase_t3352748864_0_0_0,
	&GenInst_ConstructorInfo_t3360520918_0_0_0,
	&GenInst__ConstructorInfo_t1952808443_0_0_0,
	&GenInst_IntPtr_t_0_0_0,
	&GenInst_TableRange_t4125455382_0_0_0,
	&GenInst_TailoringInfo_t3354707910_0_0_0,
	&GenInst_String_t_0_0_0_Int32_t2571135199_0_0_0,
	&GenInst_RuntimeObject_0_0_0_Int32_t2571135199_0_0_0,
	&GenInst_KeyValuePair_2_t2877556189_0_0_0,
	&GenInst_Link_t3525875035_0_0_0,
	&GenInst_RuntimeObject_0_0_0_Int32_t2571135199_0_0_0_DictionaryEntry_t1385909157_0_0_0,
	&GenInst_DictionaryEntry_t1385909157_0_0_0,
	&GenInst_RuntimeObject_0_0_0_Int32_t2571135199_0_0_0_KeyValuePair_2_t2877556189_0_0_0,
	&GenInst_String_t_0_0_0_Int32_t2571135199_0_0_0_DictionaryEntry_t1385909157_0_0_0,
	&GenInst_KeyValuePair_2_t3404415448_0_0_0,
	&GenInst_String_t_0_0_0_Int32_t2571135199_0_0_0_KeyValuePair_2_t3404415448_0_0_0,
	&GenInst_Contraction_t3257534955_0_0_0,
	&GenInst_Level2Map_t3905868985_0_0_0,
	&GenInst_BigInteger_t1715200596_0_0_0,
	&GenInst_KeySizes_t4273035850_0_0_0,
	&GenInst_KeyValuePair_2_t3602437993_0_0_0,
	&GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t1385909157_0_0_0,
	&GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t3602437993_0_0_0,
	&GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0,
	&GenInst_Slot_t2102621701_0_0_0,
	&GenInst_Slot_t1141617207_0_0_0,
	&GenInst_StackFrame_t4156368350_0_0_0,
	&GenInst_Calendar_t585695848_0_0_0,
	&GenInst_ModuleBuilder_t671559400_0_0_0,
	&GenInst__ModuleBuilder_t3149677118_0_0_0,
	&GenInst_Module_t2594760207_0_0_0,
	&GenInst__Module_t494242506_0_0_0,
	&GenInst_ParameterBuilder_t2478802094_0_0_0,
	&GenInst__ParameterBuilder_t3730072709_0_0_0,
	&GenInst_TypeU5BU5D_t1830665827_0_0_0,
	&GenInst_RuntimeArray_0_0_0,
	&GenInst_ICollection_t1881172908_0_0_0,
	&GenInst_IList_t2514494774_0_0_0,
	&GenInst_IList_1_t4288475132_0_0_0,
	&GenInst_ICollection_1_t3013691889_0_0_0,
	&GenInst_IEnumerable_1_t390880001_0_0_0,
	&GenInst_IList_1_t1904346532_0_0_0,
	&GenInst_ICollection_1_t629563289_0_0_0,
	&GenInst_IEnumerable_1_t2301718697_0_0_0,
	&GenInst_IList_1_t632813525_0_0_0,
	&GenInst_ICollection_1_t3652997578_0_0_0,
	&GenInst_IEnumerable_1_t1030185690_0_0_0,
	&GenInst_IList_1_t2432397382_0_0_0,
	&GenInst_ICollection_1_t1157614139_0_0_0,
	&GenInst_IEnumerable_1_t2829769547_0_0_0,
	&GenInst_IList_1_t1773863443_0_0_0,
	&GenInst_ICollection_1_t499080200_0_0_0,
	&GenInst_IEnumerable_1_t2171235608_0_0_0,
	&GenInst_IList_1_t3673605699_0_0_0,
	&GenInst_ICollection_1_t2398822456_0_0_0,
	&GenInst_IEnumerable_1_t4070977864_0_0_0,
	&GenInst_IList_1_t1015479969_0_0_0,
	&GenInst_ICollection_1_t4035664022_0_0_0,
	&GenInst_IEnumerable_1_t1412852134_0_0_0,
	&GenInst_ILTokenInfo_t281295013_0_0_0,
	&GenInst_LabelData_t1059919410_0_0_0,
	&GenInst_LabelFixup_t2685311059_0_0_0,
	&GenInst_GenericTypeParameterBuilder_t193954652_0_0_0,
	&GenInst_CustomAttributeBuilder_t1432176696_0_0_0,
	&GenInst__CustomAttributeBuilder_t2651440002_0_0_0,
	&GenInst_RefEmitPermissionSet_t945207422_0_0_0,
	&GenInst_TypeBuilder_t1663068587_0_0_0,
	&GenInst__TypeBuilder_t4136446867_0_0_0,
	&GenInst_MethodBuilder_t2964638190_0_0_0,
	&GenInst__MethodBuilder_t195532601_0_0_0,
	&GenInst_FieldBuilder_t803025013_0_0_0,
	&GenInst__FieldBuilder_t3486190427_0_0_0,
	&GenInst_MonoResource_t2965441204_0_0_0,
	&GenInst_ConstructorBuilder_t1596923965_0_0_0,
	&GenInst__ConstructorBuilder_t3355697174_0_0_0,
	&GenInst_PropertyBuilder_t1258013571_0_0_0,
	&GenInst__PropertyBuilder_t2876596643_0_0_0,
	&GenInst_PropertyInfo_t_0_0_0,
	&GenInst__PropertyInfo_t2517621214_0_0_0,
	&GenInst_EventBuilder_t2300762438_0_0_0,
	&GenInst__EventBuilder_t3169829690_0_0_0,
	&GenInst_CustomAttributeTypedArgument_t2667702721_0_0_0,
	&GenInst_CustomAttributeNamedArgument_t425730339_0_0_0,
	&GenInst_CustomAttributeData_t3408840380_0_0_0,
	&GenInst_ResourceInfo_t1773833009_0_0_0,
	&GenInst_ResourceCacheItem_t3632973466_0_0_0,
	&GenInst_IContextProperty_t2748911791_0_0_0,
	&GenInst_Header_t2445250629_0_0_0,
	&GenInst_ITrackingHandler_t2297406515_0_0_0,
	&GenInst_IContextAttribute_t632650489_0_0_0,
	&GenInst_DateTime_t507798353_0_0_0,
	&GenInst_TimeSpan_t2986675223_0_0_0,
	&GenInst_TypeTag_t1137063594_0_0_0,
	&GenInst_MonoType_t_0_0_0,
	&GenInst_StrongName_t848839738_0_0_0,
	&GenInst_IBuiltInEvidence_t1834666417_0_0_0,
	&GenInst_IIdentityPermissionFactory_t233160095_0_0_0,
	&GenInst_DateTimeOffset_t2220763929_0_0_0,
	&GenInst_Guid_t_0_0_0,
	&GenInst_Version_t900644674_0_0_0,
	&GenInst_String_t_0_0_0_Boolean_t2059672899_0_0_0,
	&GenInst_RuntimeObject_0_0_0_Boolean_t2059672899_0_0_0,
	&GenInst_KeyValuePair_2_t2366093889_0_0_0,
	&GenInst_RuntimeObject_0_0_0_Boolean_t2059672899_0_0_0_DictionaryEntry_t1385909157_0_0_0,
	&GenInst_RuntimeObject_0_0_0_Boolean_t2059672899_0_0_0_KeyValuePair_2_t2366093889_0_0_0,
	&GenInst_String_t_0_0_0_Boolean_t2059672899_0_0_0_DictionaryEntry_t1385909157_0_0_0,
	&GenInst_KeyValuePair_2_t2892953148_0_0_0,
	&GenInst_String_t_0_0_0_Boolean_t2059672899_0_0_0_KeyValuePair_2_t2892953148_0_0_0,
	&GenInst_X509Certificate_t3231070314_0_0_0,
	&GenInst_IDeserializationCallback_t1559034278_0_0_0,
	&GenInst_X509ChainStatus_t177750891_0_0_0,
	&GenInst_Capture_t1939465921_0_0_0,
	&GenInst_Group_t61132085_0_0_0,
	&GenInst_Mark_t4050795874_0_0_0,
	&GenInst_UriScheme_t2622429923_0_0_0,
	&GenInst_BigInteger_t1715200597_0_0_0,
	&GenInst_ByteU5BU5D_t1014014593_0_0_0,
	&GenInst_IList_1_t2894918582_0_0_0,
	&GenInst_ICollection_1_t1620135339_0_0_0,
	&GenInst_IEnumerable_1_t3292290747_0_0_0,
	&GenInst_ClientCertificateType_t194976790_0_0_0,
	&GenInst_AsyncOperation_t4233810744_0_0_0,
	&GenInst_Camera_t101201881_0_0_0,
	&GenInst_Behaviour_t4110946901_0_0_0,
	&GenInst_Component_t2160255836_0_0_0,
	&GenInst_Object_t2461733472_0_0_0,
	&GenInst_Display_t4002355288_0_0_0,
	&GenInst_Keyframe_t3396755990_0_0_0,
	&GenInst_Playable_t1760619938_0_0_0,
	&GenInst_PlayableOutput_t3840687608_0_0_0,
	&GenInst_Scene_t3023997566_0_0_0_LoadSceneMode_t524132339_0_0_0,
	&GenInst_Scene_t3023997566_0_0_0,
	&GenInst_Scene_t3023997566_0_0_0_Scene_t3023997566_0_0_0,
	&GenInst_SpriteAtlas_t1477815998_0_0_0,
	&GenInst_DisallowMultipleComponent_t2957315911_0_0_0,
	&GenInst_Attribute_t1659210826_0_0_0,
	&GenInst__Attribute_t480711065_0_0_0,
	&GenInst_ExecuteInEditMode_t1836144016_0_0_0,
	&GenInst_RequireComponent_t597737523_0_0_0,
	&GenInst_HitInfo_t184839221_0_0_0,
	&GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0,
	&GenInst_PersistentCall_t2687057912_0_0_0,
	&GenInst_BaseInvokableCall_t2920707967_0_0_0,
	&GenInst_WorkRequest_t2547429811_0_0_0,
	&GenInst_PlayableBinding_t136667248_0_0_0,
	&GenInst_MessageTypeSubscribers_t1657992506_0_0_0,
	&GenInst_MessageTypeSubscribers_t1657992506_0_0_0_Boolean_t2059672899_0_0_0,
	&GenInst_MessageEventArgs_t947408028_0_0_0,
	&GenInst_IntPtr_t_0_0_0_WeakReference_t3998924468_0_0_0,
	&GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0,
	&GenInst_KeyValuePair_2_t2204764377_0_0_0,
	&GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t1385909157_0_0_0,
	&GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t2204764377_0_0_0,
	&GenInst_WeakReference_t3998924468_0_0_0,
	&GenInst_IntPtr_t_0_0_0_WeakReference_t3998924468_0_0_0_DictionaryEntry_t1385909157_0_0_0,
	&GenInst_KeyValuePair_2_t2907671842_0_0_0,
	&GenInst_IntPtr_t_0_0_0_WeakReference_t3998924468_0_0_0_KeyValuePair_2_t2907671842_0_0_0,
	&GenInst_AudioSpatializerExtensionDefinition_t4023306521_0_0_0,
	&GenInst_AudioAmbisonicExtensionDefinition_t139142167_0_0_0,
	&GenInst_AudioSourceExtension_t3778551013_0_0_0,
	&GenInst_ScriptableObject_t476655885_0_0_0,
	&GenInst_AudioMixerPlayable_t1017334687_0_0_0,
	&GenInst_AudioClipPlayable_t3812371941_0_0_0,
	&GenInst_Boolean_t2059672899_0_0_0_String_t_0_0_0,
	&GenInst_Boolean_t2059672899_0_0_0_RuntimeObject_0_0_0,
	&GenInst_AchievementDescription_t130800866_0_0_0,
	&GenInst_IAchievementDescription_t924957763_0_0_0,
	&GenInst_UserProfile_t3211800844_0_0_0,
	&GenInst_IUserProfile_t3226494754_0_0_0,
	&GenInst_GcLeaderboard_t617530396_0_0_0,
	&GenInst_IAchievementDescriptionU5BU5D_t2959897426_0_0_0,
	&GenInst_IAchievementU5BU5D_t1231928882_0_0_0,
	&GenInst_IAchievement_t3311330019_0_0_0,
	&GenInst_GcAchievementData_t3167706508_0_0_0,
	&GenInst_Achievement_t2517894267_0_0_0,
	&GenInst_IScoreU5BU5D_t624878879_0_0_0,
	&GenInst_IScore_t1996314298_0_0_0,
	&GenInst_GcScoreData_t3955401213_0_0_0,
	&GenInst_Score_t1135675058_0_0_0,
	&GenInst_IUserProfileU5BU5D_t3577724439_0_0_0,
	&GenInst_String_t_0_0_0_RuntimeObject_0_0_0,
	&GenInst_KeyValuePair_2_t4129297252_0_0_0,
	&GenInst_String_t_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t1385909157_0_0_0,
	&GenInst_String_t_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t4129297252_0_0_0,
	&GenInst_FieldWithTarget_t1883299082_0_0_0,
	&GenInst_IEnumerable_1_t2300816294_gp_0_0_0_0,
	&GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m2280917461_gp_0_0_0_0,
	&GenInst_Array_Sort_m2667677402_gp_0_0_0_0_Array_Sort_m2667677402_gp_0_0_0_0,
	&GenInst_Array_Sort_m724484260_gp_0_0_0_0_Array_Sort_m724484260_gp_1_0_0_0,
	&GenInst_Array_Sort_m3090697542_gp_0_0_0_0,
	&GenInst_Array_Sort_m3090697542_gp_0_0_0_0_Array_Sort_m3090697542_gp_0_0_0_0,
	&GenInst_Array_Sort_m3597357548_gp_0_0_0_0,
	&GenInst_Array_Sort_m3597357548_gp_0_0_0_0_Array_Sort_m3597357548_gp_1_0_0_0,
	&GenInst_Array_Sort_m1886451243_gp_0_0_0_0_Array_Sort_m1886451243_gp_0_0_0_0,
	&GenInst_Array_Sort_m1562643584_gp_0_0_0_0_Array_Sort_m1562643584_gp_1_0_0_0,
	&GenInst_Array_Sort_m2579296346_gp_0_0_0_0,
	&GenInst_Array_Sort_m2579296346_gp_0_0_0_0_Array_Sort_m2579296346_gp_0_0_0_0,
	&GenInst_Array_Sort_m3476310768_gp_0_0_0_0,
	&GenInst_Array_Sort_m3476310768_gp_1_0_0_0,
	&GenInst_Array_Sort_m3476310768_gp_0_0_0_0_Array_Sort_m3476310768_gp_1_0_0_0,
	&GenInst_Array_Sort_m3187056504_gp_0_0_0_0,
	&GenInst_Array_Sort_m1246393659_gp_0_0_0_0,
	&GenInst_Array_qsort_m1307391834_gp_0_0_0_0,
	&GenInst_Array_qsort_m1307391834_gp_0_0_0_0_Array_qsort_m1307391834_gp_1_0_0_0,
	&GenInst_Array_compare_m3469659333_gp_0_0_0_0,
	&GenInst_Array_qsort_m172816524_gp_0_0_0_0,
	&GenInst_Array_Resize_m3981285257_gp_0_0_0_0,
	&GenInst_Array_TrueForAll_m1661083019_gp_0_0_0_0,
	&GenInst_Array_ForEach_m753603380_gp_0_0_0_0,
	&GenInst_Array_ConvertAll_m2612536962_gp_0_0_0_0_Array_ConvertAll_m2612536962_gp_1_0_0_0,
	&GenInst_Array_FindLastIndex_m1512572791_gp_0_0_0_0,
	&GenInst_Array_FindLastIndex_m1228596251_gp_0_0_0_0,
	&GenInst_Array_FindLastIndex_m3711907002_gp_0_0_0_0,
	&GenInst_Array_FindIndex_m2893558283_gp_0_0_0_0,
	&GenInst_Array_FindIndex_m3648911080_gp_0_0_0_0,
	&GenInst_Array_FindIndex_m3595353763_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m4248949575_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m1974805269_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m3819272786_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m1130814323_gp_0_0_0_0,
	&GenInst_Array_IndexOf_m2441478810_gp_0_0_0_0,
	&GenInst_Array_IndexOf_m4207036999_gp_0_0_0_0,
	&GenInst_Array_IndexOf_m3455639349_gp_0_0_0_0,
	&GenInst_Array_LastIndexOf_m2181488816_gp_0_0_0_0,
	&GenInst_Array_LastIndexOf_m4262341566_gp_0_0_0_0,
	&GenInst_Array_LastIndexOf_m395810534_gp_0_0_0_0,
	&GenInst_Array_FindAll_m1792123350_gp_0_0_0_0,
	&GenInst_Array_Exists_m4188561929_gp_0_0_0_0,
	&GenInst_Array_AsReadOnly_m547237041_gp_0_0_0_0,
	&GenInst_Array_Find_m4070178886_gp_0_0_0_0,
	&GenInst_Array_FindLast_m3836785593_gp_0_0_0_0,
	&GenInst_InternalEnumerator_1_t2351989714_gp_0_0_0_0,
	&GenInst_ArrayReadOnlyList_1_t3002341623_gp_0_0_0_0,
	&GenInst_U3CGetEnumeratorU3Ec__Iterator0_t4287845664_gp_0_0_0_0,
	&GenInst_IList_1_t2409468953_gp_0_0_0_0,
	&GenInst_ICollection_1_t2720146454_gp_0_0_0_0,
	&GenInst_Nullable_1_t2413029061_gp_0_0_0_0,
	&GenInst_Comparer_1_t1619863495_gp_0_0_0_0,
	&GenInst_DefaultComparer_t3604220391_gp_0_0_0_0,
	&GenInst_GenericComparer_1_t769309037_gp_0_0_0_0,
	&GenInst_Dictionary_2_t1922834018_gp_0_0_0_0,
	&GenInst_Dictionary_2_t1922834018_gp_0_0_0_0_Dictionary_2_t1922834018_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t2834118370_0_0_0,
	&GenInst_Dictionary_2_t1922834018_gp_0_0_0_0_Dictionary_2_t1922834018_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m1120866649_gp_0_0_0_0,
	&GenInst_Dictionary_2_t1922834018_gp_0_0_0_0_Dictionary_2_t1922834018_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m3964247330_gp_0_0_0_0,
	&GenInst_Dictionary_2_Do_ICollectionCopyTo_m3964247330_gp_0_0_0_0_RuntimeObject_0_0_0,
	&GenInst_Dictionary_2_t1922834018_gp_0_0_0_0_Dictionary_2_t1922834018_gp_1_0_0_0_DictionaryEntry_t1385909157_0_0_0,
	&GenInst_ShimEnumerator_t2216024862_gp_0_0_0_0_ShimEnumerator_t2216024862_gp_1_0_0_0,
	&GenInst_Enumerator_t601853994_gp_0_0_0_0_Enumerator_t601853994_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t2855505602_0_0_0,
	&GenInst_DictionaryEntry_t1385909157_0_0_0_DictionaryEntry_t1385909157_0_0_0,
	&GenInst_Dictionary_2_t1922834018_gp_0_0_0_0_Dictionary_2_t1922834018_gp_1_0_0_0_KeyValuePair_2_t2834118370_0_0_0,
	&GenInst_KeyValuePair_2_t2834118370_0_0_0_KeyValuePair_2_t2834118370_0_0_0,
	&GenInst_Dictionary_2_t1922834018_gp_1_0_0_0,
	&GenInst_EqualityComparer_1_t2656680762_gp_0_0_0_0,
	&GenInst_DefaultComparer_t866827805_gp_0_0_0_0,
	&GenInst_GenericEqualityComparer_1_t2573898200_gp_0_0_0_0,
	&GenInst_KeyValuePair_2_t4172885178_0_0_0,
	&GenInst_IDictionary_2_t3413473948_gp_0_0_0_0_IDictionary_2_t3413473948_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t776898938_gp_0_0_0_0_KeyValuePair_2_t776898938_gp_1_0_0_0,
	&GenInst_List_1_t82844879_gp_0_0_0_0,
	&GenInst_Enumerator_t2210905544_gp_0_0_0_0,
	&GenInst_Collection_1_t3543120677_gp_0_0_0_0,
	&GenInst_ReadOnlyCollection_1_t891526187_gp_0_0_0_0,
	&GenInst_MonoProperty_GetterAdapterFrame_m880231002_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m880231002_gp_1_0_0_0,
	&GenInst_MonoProperty_StaticGetterAdapterFrame_m944017034_gp_0_0_0_0,
	&GenInst_Queue_1_t542587364_gp_0_0_0_0,
	&GenInst_Enumerator_t3782511927_gp_0_0_0_0,
	&GenInst_Stack_1_t2711020695_gp_0_0_0_0,
	&GenInst_Enumerator_t548431029_gp_0_0_0_0,
	&GenInst_Enumerable_Any_m1770862959_gp_0_0_0_0,
	&GenInst_Enumerable_Where_m2049181036_gp_0_0_0_0,
	&GenInst_Enumerable_Where_m2049181036_gp_0_0_0_0_Boolean_t2059672899_0_0_0,
	&GenInst_Enumerable_CreateWhereIterator_m1295165955_gp_0_0_0_0,
	&GenInst_Enumerable_CreateWhereIterator_m1295165955_gp_0_0_0_0_Boolean_t2059672899_0_0_0,
	&GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1910290221_gp_0_0_0_0,
	&GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1910290221_gp_0_0_0_0_Boolean_t2059672899_0_0_0,
	&GenInst_InvokableCall_1_t1604090555_gp_0_0_0_0,
	&GenInst_UnityAction_1_t3885734668_0_0_0,
	&GenInst_InvokableCall_2_t3976188983_gp_0_0_0_0_InvokableCall_2_t3976188983_gp_1_0_0_0,
	&GenInst_InvokableCall_2_t3976188983_gp_0_0_0_0,
	&GenInst_InvokableCall_2_t3976188983_gp_1_0_0_0,
	&GenInst_InvokableCall_3_t3917817895_gp_0_0_0_0_InvokableCall_3_t3917817895_gp_1_0_0_0_InvokableCall_3_t3917817895_gp_2_0_0_0,
	&GenInst_InvokableCall_3_t3917817895_gp_0_0_0_0,
	&GenInst_InvokableCall_3_t3917817895_gp_1_0_0_0,
	&GenInst_InvokableCall_3_t3917817895_gp_2_0_0_0,
	&GenInst_InvokableCall_4_t2387264047_gp_0_0_0_0_InvokableCall_4_t2387264047_gp_1_0_0_0_InvokableCall_4_t2387264047_gp_2_0_0_0_InvokableCall_4_t2387264047_gp_3_0_0_0,
	&GenInst_InvokableCall_4_t2387264047_gp_0_0_0_0,
	&GenInst_InvokableCall_4_t2387264047_gp_1_0_0_0,
	&GenInst_InvokableCall_4_t2387264047_gp_2_0_0_0,
	&GenInst_InvokableCall_4_t2387264047_gp_3_0_0_0,
	&GenInst_CachedInvokableCall_1_t4137603102_gp_0_0_0_0,
	&GenInst_UnityEvent_1_t4023850743_gp_0_0_0_0,
	&GenInst_UnityEvent_2_t1533051435_gp_0_0_0_0_UnityEvent_2_t1533051435_gp_1_0_0_0,
	&GenInst_UnityEvent_3_t4091795572_gp_0_0_0_0_UnityEvent_3_t4091795572_gp_1_0_0_0_UnityEvent_3_t4091795572_gp_2_0_0_0,
	&GenInst_UnityEvent_4_t1002767761_gp_0_0_0_0_UnityEvent_4_t1002767761_gp_1_0_0_0_UnityEvent_4_t1002767761_gp_2_0_0_0_UnityEvent_4_t1002767761_gp_3_0_0_0,
	&GenInst_DefaultExecutionOrder_t3862449190_0_0_0,
	&GenInst_PlayerConnection_t3559457672_0_0_0,
	&GenInst_GUILayer_t1929752731_0_0_0,
	&GenInst_KeyValuePair_2_t2204764377_0_0_0_KeyValuePair_2_t2204764377_0_0_0,
	&GenInst_KeyValuePair_2_t2204764377_0_0_0_RuntimeObject_0_0_0,
	&GenInst_KeyValuePair_2_t2366093889_0_0_0_KeyValuePair_2_t2366093889_0_0_0,
	&GenInst_KeyValuePair_2_t2366093889_0_0_0_RuntimeObject_0_0_0,
	&GenInst_KeyValuePair_2_t2877556189_0_0_0_KeyValuePair_2_t2877556189_0_0_0,
	&GenInst_KeyValuePair_2_t2877556189_0_0_0_RuntimeObject_0_0_0,
	&GenInst_KeyValuePair_2_t3602437993_0_0_0_KeyValuePair_2_t3602437993_0_0_0,
	&GenInst_KeyValuePair_2_t3602437993_0_0_0_RuntimeObject_0_0_0,
};
