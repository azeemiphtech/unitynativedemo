//
//  AppDelegate.m
//  App
//
//  Created by iphtech-28 on 6/5/18.
//  Copyright © 2018 iphtech-28. All rights reserved.
//

#import "AppDelegate.h"
#import <Launcher/Launcher.h>
@interface AppDelegate ()
@property (nonatomic,strong) Launch *unityLauncher;
@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    self->_unityLauncher = [[Launch alloc] init];
    [self->_unityLauncher application:application didFinishLaunchingWithOptions:launchOptions];
    
    UIViewController *controller = [[UIViewController alloc] init];
    self.window = [[UIWindow alloc] initWithFrame:UIScreen.mainScreen.bounds];
    self.window.rootViewController = controller;
    [self.window makeKeyAndVisible];
    [self.unityLauncher start];
    [controller.view addSubview:self.unityLauncher.view];
    [self.unityLauncher.view setHidden:YES];
    [NSLayoutConstraint activateConstraints:@[
                                              [self.unityLauncher.view.leftAnchor constraintEqualToAnchor:controller.view.leftAnchor],
                                              [self.unityLauncher.view.rightAnchor constraintEqualToAnchor:controller.view.rightAnchor],
                                              [self.unityLauncher.view.topAnchor constraintEqualToAnchor:controller.view.topAnchor],
                                              [self.unityLauncher.view.bottomAnchor constraintEqualToAnchor:controller.view.bottomAnchor]]];
    
    UIButton *button = [[ UIButton alloc] init];
    [button setTitle:@"Launch Unity" forState:UIControlStateNormal];
    [button setBackgroundColor:UIColor.blueColor];
    
    
    [controller.view addSubview:button];
    [button.leftAnchor constraintEqualToAnchor:controller.view.leftAnchor constant:25.0].active = YES;
    [button.rightAnchor constraintEqualToAnchor:controller.view.rightAnchor constant:-25.0].active = YES;
    [button.centerYAnchor constraintEqualToAnchor:controller.view.centerYAnchor].active = YES;
    [button.heightAnchor constraintEqualToConstant:45.0].active = YES;
    [button setTranslatesAutoresizingMaskIntoConstraints:NO];
    [button addTarget:self action:@selector(onLaunchUnity:) forControlEvents:UIControlEventTouchUpInside];
    
    return YES;
}
-(void)onLaunchUnity:(UIButton *)button{
    [button setHidden:YES];
    [self.unityLauncher.view setHidden:NO];
    
}

- (void)applicationWillResignActive:(UIApplication *)application {
    [self->_unityLauncher  applicationWillResignActive:application];
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    [self->_unityLauncher  applicationDidEnterBackground:application];
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    [self->_unityLauncher  applicationWillEnterForeground:application];
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    [self->_unityLauncher  applicationDidBecomeActive:application];
}


- (void)applicationWillTerminate:(UIApplication *)application {
    [self->_unityLauncher  applicationWillTerminate:application];
}
- (void)applicationDidReceiveMemoryWarning:(UIApplication*)application{
    
    [self->_unityLauncher  applicationDidReceiveMemoryWarning:application];
}
- (NSUInteger)application:(UIApplication*)application supportedInterfaceOrientationsForWindow:(UIWindow*)window{
    
    return  [self->_unityLauncher application:application supportedInterfaceOrientationsForWindow:window];
}

- (void)application:(UIApplication*)application didReceiveLocalNotification:(UILocalNotification*)notification{
    return  [self->_unityLauncher application:application didReceiveLocalNotification:notification];
}

- (void)application:(UIApplication*)application didReceiveRemoteNotification:(NSDictionary*)userInfo{
    return  [self->_unityLauncher application:application didReceiveRemoteNotification:userInfo];
}
- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken{
    return  [self->_unityLauncher application:application didRegisterForRemoteNotificationsWithDeviceToken:deviceToken];
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult result))handler{
    return  [self->_unityLauncher application:application didReceiveRemoteNotification:userInfo fetchCompletionHandler:handler];
}

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error{
    return  [self->_unityLauncher application:application didFailToRegisterForRemoteNotificationsWithError:error];
}

- (BOOL)application:(UIApplication*)application openURL:(NSURL*)url sourceApplication:(NSString*)sourceApplication annotation:(id)annotation{
    return  [self->_unityLauncher application:application openURL:url sourceApplication:sourceApplication annotation:annotation];
    
}
- (BOOL)application:(UIApplication*)application willFinishLaunchingWithOptions:(NSDictionary*)launchOptions{
    return  [self->_unityLauncher application:application willFinishLaunchingWithOptions:launchOptions];
}



@end
