//
//  main.m
//  App
//
//  Created by iphtech-28 on 6/5/18.
//  Copyright © 2018 iphtech-28. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import <Launcher/Launcher.h>

int main(int argc, char * argv[]) {
    @autoreleasepool {
          return [Launch initializeUnity:argc argv:argv appDelegateName:NSStringFromClass([AppDelegate class]) ];
    }
}
