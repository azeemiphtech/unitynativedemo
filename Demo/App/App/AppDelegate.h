//
//  AppDelegate.h
//  App
//
//  Created by iphtech-28 on 6/5/18.
//  Copyright © 2018 iphtech-28. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

