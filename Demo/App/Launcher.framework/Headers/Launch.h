//
//  Launch.h
//  Launcher
//
//  Created by iphtech-28 on 6/5/18.
//  Copyright © 2018 iphtech-28. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Launch : NSObject
+ (int)initializeUnity:(int)argc argv:(char**)argv appDelegateName:(NSString *)appDelegateName;
- (void)start ;
- (void)stop ;

- (UIView *)view ;

- (NSUInteger)application:(UIApplication*)application supportedInterfaceOrientationsForWindow:(UIWindow*)window;

- (void)application:(UIApplication*)application didReceiveLocalNotification:(UILocalNotification*)notification;

- (void)application:(UIApplication*)application didReceiveRemoteNotification:(NSDictionary*)userInfo;
- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken;

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult result))handler;

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error;

- (BOOL)application:(UIApplication*)application openURL:(NSURL*)url sourceApplication:(NSString*)sourceApplication annotation:(id)annotation;
- (BOOL)application:(UIApplication*)application willFinishLaunchingWithOptions:(NSDictionary*)launchOptions;

- (BOOL)application:(UIApplication*)application didFinishLaunchingWithOptions:(NSDictionary*)launchOptions;

- (void)applicationDidEnterBackground:(UIApplication*)application;
- (void)applicationWillEnterForeground:(UIApplication*)application;
- (void)applicationDidBecomeActive:(UIApplication*)application;

- (void)applicationWillResignActive:(UIApplication*)application;

- (void)applicationDidReceiveMemoryWarning:(UIApplication*)application;

- (void)applicationWillTerminate:(UIApplication*)application;
@end
